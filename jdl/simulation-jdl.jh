



entity User {}

entity Dataset {}

entity Application {}

entity SshServer {}

entity Simulation {
	name String required,
    timestamp ZonedDateTime required,
    description String required
}

entity ErrorType {
	/** 0: MSE, 1: MAE */
	name String required maxlength(50)
}

entity ProblemType {
	/** 0: AnnFixedGlia, 1: AngnSearchGlia, 2: nuSVM */
	name String required maxlength(100),
	outputFeatureConstraint Integer,
	numPopulation Integer required,
}

entity SimulationParam {
	/** value's name */
	name String required,
    value String required
}

relationship ManyToOne {
	Simulation{belong} to User
}

relationship ManyToOne {
	Simulation{use} to Dataset
}

relationship ManyToOne {
	Simulation{exe} to Application
}

relationship ManyToOne {
	Simulation{problem} to ProblemType
}

relationship ManyToOne {
	Simulation{minimize} to ErrorType
}

relationship ManyToOne {
	Simulation{launch} to SshServer
}

relationship ManyToOne {
	SimulationParam{belong} to Simulation
}

// This relationship is to show which errors are minimize for a specific problem
relationship ManyToMany {
	ProblemType{minimize} to ErrorType{solve}
}

dto ProblemType, ErrorType, Simulation, SimulationParam with mapstruct

paginate ProblemType, ErrorType, Simulation, SimulationParam with pagination

service ProblemType, ErrorType, Simulation, SimulationParam with serviceClass