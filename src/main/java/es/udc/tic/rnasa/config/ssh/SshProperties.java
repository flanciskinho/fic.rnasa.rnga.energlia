/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.config.ssh;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by flanciskinho on 16/5/16.
 *
 * Properties to ssh server
 *
 */
@Component
@ConfigurationProperties(prefix = "ssh", ignoreUnknownFields = true)
public class SshProperties {

    private final Config config = new Config();
    private final Server server = new Server();

    public Config getConfig() {
        return config;
    }

    public Server getServer() {
        return server;
    }

    public static class Config {
        private String sharedKey;
        private Integer timeout;

        public String getSharedKey() {
            return sharedKey;
        }

        public void setSharedKey(String sharedKey) {
            this.sharedKey = sharedKey;
        }

        public Integer getTimeout() {
            return timeout;
        }

        public void setTimeout(Integer timeout) {
            this.timeout = timeout;
        }
    }

    public static class Server {
        private List<String> username;
        private String key;
        private Integer port;
        private String host;
        private String fingerprint;

        public List<String> getUsername() {
            return username;
        }

        public void setUsername(List<String> username) {
            this.username = username;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getPort() {
            return port;
        }

        public void setPort(Integer port) {
            this.port = port;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getFingerprint() {
            return fingerprint;
        }

        public void setFingerprint(String fingerprint) {
            this.fingerprint = fingerprint;
        }
    }


}
