/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ArchType.
 */
@Entity
@Table(name = "arch_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "archtype")
public class ArchType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * To indicate if this still working. Cesga update CPUs and sometimes remove some architectures
     *
     */
    @ApiModelProperty(value = ""
        + "To indicate if this still working. Cesga update CPUs and sometimes remove some architectures"
        + "")
    @NotNull
    @Column(name = "activated", nullable = false)
    private Boolean activated;

    @ManyToOne
    private SshServer belong;

    public ArchType() {}

    public ArchType(String name, Boolean activated, SshServer belong) {
        this.name = name;
        this.activated = activated;
        this.belong = belong;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public SshServer getBelong() {
        return belong;
    }

    public void setBelong(SshServer sshServer) {
        this.belong = sshServer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArchType archType = (ArchType) o;
        if(archType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, archType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ArchType{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", activated='" + activated + "'" +
            '}';
    }
}
