/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CompiledApp.
 */
@Entity
@Table(name = "compiled_app")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "compiledapp")
public class CompiledApp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * app compiled for a specific server                                      
     * 
     */
    @ApiModelProperty(value = ""
        + "app compiled for a specific server                                 "
        + "")
    @NotNull
    @Lob
    @Column(name = "executable", nullable = false)
    private byte[] executable;

    @Column(name = "executable_content_type", nullable = false)    
    private String executableContentType;

    @ManyToOne
    private Application belong;

    @ManyToOne
    private SshServer use;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getExecutable() {
        return executable;
    }

    public void setExecutable(byte[] executable) {
        this.executable = executable;
    }

    public String getExecutableContentType() {
        return executableContentType;
    }

    public void setExecutableContentType(String executableContentType) {
        this.executableContentType = executableContentType;
    }

    public Application getBelong() {
        return belong;
    }

    public void setBelong(Application application) {
        this.belong = application;
    }

    public SshServer getUse() {
        return use;
    }

    public void setUse(SshServer sshServer) {
        this.use = sshServer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompiledApp compiledApp = (CompiledApp) o;
        if(compiledApp.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, compiledApp.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CompiledApp{" +
            "id=" + id +
            ", executable='" + executable + "'" +
            ", executableContentType='" + executableContentType + "'" +
            '}';
    }
}
