/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Dataset.
 */
@Entity
@Table(name = "dataset")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataset")
public class Dataset implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @Column(name = "num_pattern")
    private Integer numPattern;

    @Column(name = "output_feature")
    private Integer outputFeature;

    @Column(name = "input_feature")
    private Integer inputFeature;

    @Column(name = "num_combination")
    private Integer numCombination;

    @Column(name = "num_class")
    private Integer numClass;

    @ManyToOne
    private User owner;

    @ManyToOne
    private AnalyzeType analyze;


    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "dataset_share",
        joinColumns = @JoinColumn(name="datasets_id", referencedColumnName="ID"),
        inverseJoinColumns = @JoinColumn(name="shares_id", referencedColumnName="ID"))
    private Set<User> shares = new HashSet<>();

    public Dataset() {
        super();
    }

    public Dataset(String name, String description, Integer numPattern, Integer outputFeature, Integer inputFeature, Integer numCombination, User owner, AnalyzeType analyze, Integer numClass) {
        super();

        this.timestamp = ZonedDateTime.now();

        this.name = name;
        this.description = description;

        this.numPattern = numPattern;
        this.outputFeature = outputFeature;
        this.inputFeature = inputFeature;
        this.numCombination = numCombination;
        this.owner = owner;
        this.analyze = analyze;
        this.numClass = numClass;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getNumPattern() {
        return numPattern;
    }

    public void setNumPattern(Integer numPattern) {
        this.numPattern = numPattern;
    }

    public Integer getOutputFeature() {
        return outputFeature;
    }

    public void setOutputFeature(Integer outputFeature) {
        this.outputFeature = outputFeature;
    }

    public Integer getInputFeature() {
        return inputFeature;
    }

    public void setInputFeature(Integer inputFeature) {
        this.inputFeature = inputFeature;
    }

    public Integer getNumCombination() {
        return numCombination;
    }

    public void setNumCombination(Integer numCombination) {
        this.numCombination = numCombination;
    }

    public Integer getNumClass() {
        return numClass;
    }

    public void setNumClass(Integer numClass) {
        this.numClass = numClass;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User user) {
        this.owner = user;
    }

    public AnalyzeType getAnalyze() {
        return analyze;
    }

    public void setAnalyze(AnalyzeType analyze) {
        this.analyze = analyze;
    }

    public Set<User> getShares() {
        return shares;
    }

    public void setShares(Set<User> shares) {
        this.shares = shares;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dataset dataset = (Dataset) o;
        if(dataset.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, dataset.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Dataset{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", timestamp='" + timestamp + "'" +
            ", numPattern='" + numPattern + "'" +
            ", outputFeature='" + outputFeature + "'" +
            ", inputFeature='" + inputFeature + "'" +
            ", numCombination='" + numCombination + "'" +
            ", numClass='" + ((numClass == null)?"-":numClass) + "'" +
            ", owner='" + owner + "'" +
            ", analyze='" + analyze + "'" +
            "}";
    }
}
