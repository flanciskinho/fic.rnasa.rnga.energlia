/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DatasetCombination.
 */
@Entity
@Table(name = "dataset_combination")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datasetcombination")
public class DatasetCombination implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "index_combination", nullable = false)
    private Integer indexCombination;

    @NotNull
    @Lob
    @Column(name = "trainfilein", nullable = false)
    private byte[] trainfilein;

    @Column(name = "trainfilein_content_type", nullable = false)    
    private String trainfileinContentType;

    @NotNull
    @Lob
    @Column(name = "trainfileout", nullable = false)
    private byte[] trainfileout;

    @Column(name = "trainfileout_content_type", nullable = false)    
    private String trainfileoutContentType;

    @Lob
    @Column(name = "validationfilein")
    private byte[] validationfilein;

    @Column(name = "validationfilein_content_type")    
    private String validationfileinContentType;

    @Lob
    @Column(name = "validationfileout")
    private byte[] validationfileout;

    @Column(name = "validationfileout_content_type")    
    private String validationfileoutContentType;

    @Lob
    @Column(name = "testfilein")
    private byte[] testfilein;

    @Column(name = "testfilein_content_type")    
    private String testfileinContentType;

    @Lob
    @Column(name = "testfileout")
    private byte[] testfileout;

    @Column(name = "testfileout_content_type")    
    private String testfileoutContentType;

    @ManyToOne
    private Dataset belong;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndexCombination() {
        return indexCombination;
    }

    public void setIndexCombination(Integer indexCombination) {
        this.indexCombination = indexCombination;
    }

    public byte[] getTrainfilein() {
        return trainfilein;
    }

    public void setTrainfilein(byte[] trainfilein) {
        this.trainfilein = trainfilein;
    }

    public String getTrainfileinContentType() {
        return trainfileinContentType;
    }

    public void setTrainfileinContentType(String trainfileinContentType) {
        this.trainfileinContentType = trainfileinContentType;
    }

    public byte[] getTrainfileout() {
        return trainfileout;
    }

    public void setTrainfileout(byte[] trainfileout) {
        this.trainfileout = trainfileout;
    }

    public String getTrainfileoutContentType() {
        return trainfileoutContentType;
    }

    public void setTrainfileoutContentType(String trainfileoutContentType) {
        this.trainfileoutContentType = trainfileoutContentType;
    }

    public byte[] getValidationfilein() {
        return validationfilein;
    }

    public void setValidationfilein(byte[] validationfilein) {
        this.validationfilein = validationfilein;
    }

    public String getValidationfileinContentType() {
        return validationfileinContentType;
    }

    public void setValidationfileinContentType(String validationfileinContentType) {
        this.validationfileinContentType = validationfileinContentType;
    }

    public byte[] getValidationfileout() {
        return validationfileout;
    }

    public void setValidationfileout(byte[] validationfileout) {
        this.validationfileout = validationfileout;
    }

    public String getValidationfileoutContentType() {
        return validationfileoutContentType;
    }

    public void setValidationfileoutContentType(String validationfileoutContentType) {
        this.validationfileoutContentType = validationfileoutContentType;
    }

    public byte[] getTestfilein() {
        return testfilein;
    }

    public void setTestfilein(byte[] testfilein) {
        this.testfilein = testfilein;
    }

    public String getTestfileinContentType() {
        return testfileinContentType;
    }

    public void setTestfileinContentType(String testfileinContentType) {
        this.testfileinContentType = testfileinContentType;
    }

    public byte[] getTestfileout() {
        return testfileout;
    }

    public void setTestfileout(byte[] testfileout) {
        this.testfileout = testfileout;
    }

    public String getTestfileoutContentType() {
        return testfileoutContentType;
    }

    public void setTestfileoutContentType(String testfileoutContentType) {
        this.testfileoutContentType = testfileoutContentType;
    }

    public Dataset getBelong() {
        return belong;
    }

    public void setBelong(Dataset dataset) {
        this.belong = dataset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatasetCombination datasetCombination = (DatasetCombination) o;
        if(datasetCombination.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, datasetCombination.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DatasetCombination{" +
            "id=" + id +
            ", indexCombination='" + indexCombination + "'" +
            ", trainfilein='" + trainfilein + "'" +
            ", trainfileinContentType='" + trainfileinContentType + "'" +
            ", trainfileout='" + trainfileout + "'" +
            ", trainfileoutContentType='" + trainfileoutContentType + "'" +
            ", validationfilein='" + validationfilein + "'" +
            ", validationfileinContentType='" + validationfileinContentType + "'" +
            ", validationfileout='" + validationfileout + "'" +
            ", validationfileoutContentType='" + validationfileoutContentType + "'" +
            ", testfilein='" + testfilein + "'" +
            ", testfileinContentType='" + testfileinContentType + "'" +
            ", testfileout='" + testfileout + "'" +
            ", testfileoutContentType='" + testfileoutContentType + "'" +
            '}';
    }
}
