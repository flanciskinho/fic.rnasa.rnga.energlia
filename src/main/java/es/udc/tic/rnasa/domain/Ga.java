/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Ga.
 */
@Entity
@Table(name = "ga")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ga")
public class Ga implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String GA_VERSION = "1.0";

    @Transient
    public String getGaVersion() {
        return GA_VERSION;
    }

    public Ga() {
        this.version = new String(GA_VERSION);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "version", nullable = false)
    private String version;

    @OneToOne
    @JoinColumn(unique = true)
    private Simulation belong;

    @ManyToOne
    private GaType use;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Simulation getBelong() {
        return belong;
    }

    public void setBelong(Simulation simulation) {
        this.belong = simulation;
    }

    public GaType getUse() {
        return use;
    }

    public void setUse(GaType gaType) {
        this.use = gaType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ga ga = (Ga) o;
        if(ga.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, ga.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Ga{" +
            "id=" + id +
            ", version='" + version + '\'' +
            ", belong=" + belong +
            ", use=" + use +
            '}';
    }
}
