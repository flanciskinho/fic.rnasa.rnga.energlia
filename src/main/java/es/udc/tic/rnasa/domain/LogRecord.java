/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A LogRecord.
 */
@Entity
@Table(name = "log_record")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "logrecord")
@BatchSize(size = 10)

public class LogRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "index_record", nullable = false)
    private Long indexRecord;

    @NotNull
    @Column(name = "timestamp", nullable = false)
    private Long timestamp;

    @NotNull
    @Column(name = "error_train", nullable = false)
    private Double errorTrain;

    @Column(name = "error_validation")
    private Double errorValidation;

    @Column(name = "error_test")
    private Double errorTest;

    @ManyToOne
    private SpecificSimulation belong;

    public LogRecord() {}

    public LogRecord(Long indexRecord, Long timestamp, Double errorTrain, Double errorValidation, Double errorTest, SpecificSimulation belong) {
        this.indexRecord = indexRecord;
        this.timestamp = timestamp;
        this.errorTrain = errorTrain;
        this.errorValidation = errorValidation;
        this.errorTest = errorTest;
        this.belong = belong;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIndexRecord() {
        return indexRecord;
    }

    public void setIndexRecord(Long indexRecord) {
        this.indexRecord = indexRecord;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getErrorTrain() {
        return errorTrain;
    }

    public void setErrorTrain(Double errorTrain) {
        this.errorTrain = errorTrain;
    }

    public Double getErrorValidation() {
        return errorValidation;
    }

    public void setErrorValidation(Double errorValidation) {
        this.errorValidation = errorValidation;
    }

    public Double getErrorTest() {
        return errorTest;
    }

    public void setErrorTest(Double errorTest) {
        this.errorTest = errorTest;
    }

    public SpecificSimulation getBelong() {
        return belong;
    }

    public void setBelong(SpecificSimulation specificSimulation) {
        this.belong = specificSimulation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LogRecord logRecord = (LogRecord) o;
        if(logRecord.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, logRecord.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "LogRecord{" +
            "id=" + id +
            ",  belongId='" + (belong == null? "null":belong.getId()) + "'" +
            ", indexRecord='" + indexRecord + "'" +
            ", timestamp='" + timestamp + "'" +
            ", errorTrain='" + errorTrain + "'" +
            ", errorValidation='" + errorValidation + "'" +
            ", errorTest='" + errorTest + "'" +
            '}';
    }
}
