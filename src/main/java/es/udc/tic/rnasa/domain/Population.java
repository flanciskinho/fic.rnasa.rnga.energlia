/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Population.
 */
@Entity
@Table(name = "population")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "population")
public class Population implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @Column(name = "num_individual")
    private Integer numIndividual;

    @Column(name = "num_combination")
    private Integer numCombination;

    @Column(name = "size_genotype")
    private Integer sizeGenotype;

    @ManyToOne
    private User owner;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "population_share",
        joinColumns = @JoinColumn(name="populations_id", referencedColumnName="ID"),
        inverseJoinColumns = @JoinColumn(name="shares_id", referencedColumnName="ID"))
    private Set<User> shares = new HashSet<>();

    public Population() {
        super();
    }

    public Population(String name, String description, Integer numIndividual, Integer numCombination, Integer sizeGenotype, User owner) {
        this.name = name;
        this.description = description;
        this.timestamp = ZonedDateTime.now();
        this.numIndividual = numIndividual;
        this.numCombination = numCombination;
        this.sizeGenotype = sizeGenotype;
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getNumIndividual() {
        return numIndividual;
    }

    public void setNumIndividual(Integer numIndividual) {
        this.numIndividual = numIndividual;
    }

    public Integer getNumCombination() {
        return numCombination;
    }

    public void setNumCombination(Integer numCombination) {
        this.numCombination = numCombination;
    }

    public Integer getSizeGenotype() {
        return sizeGenotype;
    }

    public void setSizeGenotype(Integer sizeGenotype) {
        this.sizeGenotype = sizeGenotype;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User user) {
        this.owner = user;
    }

    public Set<User> getShares() {
        return shares;
    }

    public void setShares(Set<User> shares) {
        this.shares = shares;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Population population = (Population) o;
        if(population.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, population.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Population{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", timestamp='" + timestamp + "'" +
            ", numIndividual='" + numIndividual + "'" +
            ", numCombination='" + numCombination + "'" +
            ", sizeGenotype='" + sizeGenotype + "'" +
            '}';
    }
}
