/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PopulationCombinationOrder.
 */
@Entity
@Table(name = "population_combination_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "populationcombinationorder")
public class PopulationCombinationOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "index_order", nullable = false)
    private Integer indexOrder;

    @ManyToOne
    private SpecificSimulation belong;

    @ManyToOne
    private PopulationCombination use;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public void setIndexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
    }

    public SpecificSimulation getBelong() {
        return belong;
    }

    public void setBelong(SpecificSimulation specificSimulation) {
        this.belong = specificSimulation;
    }

    public PopulationCombination getUse() {
        return use;
    }

    public void setUse(PopulationCombination populationCombination) {
        this.use = populationCombination;
    }

    public PopulationCombinationOrder() {}

    public PopulationCombinationOrder(Integer indexOrder, SpecificSimulation belong, PopulationCombination use) {
        this.indexOrder = indexOrder;
        this.belong = belong;
        this.use = use;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PopulationCombinationOrder populationCombinationOrder = (PopulationCombinationOrder) o;
        if(populationCombinationOrder.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, populationCombinationOrder.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PopulationCombinationOrder{" +
            "id=" + id +
            ", indexOrder='" + indexOrder + "'" +
            ", belongId" +
            "='" + belong.getId() + "'" +
            '}';
    }
}
