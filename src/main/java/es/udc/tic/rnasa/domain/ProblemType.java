/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A ProblemType.
 */
@Entity
@Table(name = "problem_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "problemtype")
public class ProblemType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * 0: AnnFixedGlia, 1: AngnSearchGlia, 2: nuSVM
     *
     */
    @ApiModelProperty(value = ""
        + "0: AnnFixedGlia, 1: AngnSearchGlia, 2: nuSVM")
    @NotNull
    @Size(max = 100)
    @Column(name = "name", length = 100, unique = true, nullable = false)
    private String name;

    @Column(name = "output_feature_constraint")
    private Integer outputFeatureConstraint;

    @Column(name = "num_population")
    private Integer numPopulation;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "problem_type_minimize",
        joinColumns = @JoinColumn(name="problem_types_id", referencedColumnName="ID"),
        inverseJoinColumns = @JoinColumn(name="minimizes_id", referencedColumnName="ID"))
    private Set<ErrorType> minimizes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOutputFeatureConstraint() {
        return outputFeatureConstraint;
    }

    public void setOutputFeatureConstraint(Integer outputFeatureConstraint) {
        this.outputFeatureConstraint = outputFeatureConstraint;
    }

    public Integer getNumPopulation() {
        return numPopulation;
    }

    public void setNumPopulation(Integer numPopulation) {
        this.numPopulation = numPopulation;
    }

    public Set<ErrorType> getMinimizes() {
        return minimizes;
    }

    public void setMinimizes(Set<ErrorType> errorTypes) {
        this.minimizes = errorTypes;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProblemType problemType = (ProblemType) o;
        if(problemType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, problemType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProblemType{" +
            "id=" + id + "'" +
            ", name='" + name + "'" +
            ", outputFeatureConstraint=" + outputFeatureConstraint + "'" +
            ", numPopulation=" + numPopulation + "'" +
            '}';
    }
}
