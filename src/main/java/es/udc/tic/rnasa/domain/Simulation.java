/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Simulation.
 */
@Entity
@Table(name = "simulation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "simulation")
public class Simulation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "timestamp")
    private ZonedDateTime timestamp = ZonedDateTime.now();

    @NotNull
    @Column(name = "description", nullable = true)
    private String description;

    @NotNull
    @Column(name = "task_total", nullable = false)
    private Integer taskTotal;

    @NotNull
    @Column(name = "task_done", nullable = false)
    private Integer taskDone = 0;

    @NotNull
    @Column(name = "task_fail", nullable = false)
    private Integer taskFail = 0;

    @NotNull
    @Column(name = "task_queue", nullable = false)
    private Integer taskQueue = 0;

    @NotNull
    @Column(name = "task_server", nullable = false)
    private Integer taskServer = 0;

    @ManyToOne
    private User belong;

    @ManyToOne
    private Dataset use;

    @ManyToOne
    private Application exe;

    @ManyToOne
    private ProblemType problem;

    @ManyToOne
    private ErrorType minimize;

    @ManyToOne
    private SshServer launch;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
            this.timestamp = timestamp != null? timestamp: ZonedDateTime.now();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description != null? description: "";
    }

    public Integer getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Integer taskTotal) {
        this.taskTotal = taskTotal;
    }

    public Integer getTaskDone() {
        return taskDone;
    }

    public void setTaskDone(Integer taskDone) {
        this.taskDone = taskDone;
    }

    public Integer getTaskFail() {
        return taskFail;
    }

    public void setTaskFail(Integer taskFail) {
        this.taskFail = taskFail;
    }

    public Integer getTaskQueue() {
        return taskQueue;
    }

    public void setTaskQueue(Integer taskQueue) {
        this.taskQueue = taskQueue;
    }

    public Integer getTaskServer() {
        return taskServer;
    }

    public void setTaskServer(Integer taskServer) {
        this.taskServer = taskServer;
    }

    public User getBelong() {
        return belong;
    }

    public void setBelong(User user) {
        this.belong = user;
    }

    public Dataset getUse() {
        return use;
    }

    public void setUse(Dataset dataset) {
        this.use = dataset;
    }

    public Application getExe() {
        return exe;
    }

    public void setExe(Application application) {
        this.exe = application;
    }

    public ProblemType getProblem() {
        return problem;
    }

    public void setProblem(ProblemType problemType) {
        this.problem = problemType;
    }

    public ErrorType getMinimize() {
        return minimize;
    }

    public void setMinimize(ErrorType errorType) {
        this.minimize = errorType;
    }

    public SshServer getLaunch() {
        return launch;
    }

    public void setLaunch(SshServer launch) {
        this.launch = launch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Simulation simulation = (Simulation) o;
        if(simulation.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, simulation.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Simulation{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", login='" + (belong == null? "null": belong.getLogin()) + "'" +
            ", timestamp='" + timestamp + "'" +
            ", description='" + description + "'" +
            ", taskTotal='"+taskTotal+"'"+
            ", taskDone='"+taskDone+"'"+
            ", taskFail='"+taskFail+"'"+
            ", taskQueue='"+taskQueue+"'"+
            ", taskServer='"+taskServer+"'"+
            ", problem='" + (problem == null? "null":problem.getName()) + "'" +
            ", minimize='" + (minimize == null? "null":minimize.getName()) + "'" +
            '}';
    }
}
