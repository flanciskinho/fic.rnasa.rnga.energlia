/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SimulationStatusType.
 */
@Entity
@Table(name = "simulation_status_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "simulationstatustype")
public class SimulationStatusType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * 0:WAITING_FOR_QUEUE, 1:WAITING_ON_QUEUE, 2:RUNNING, 3:FINISHED_SUCCESS, 4: FINISHED_FAILURE
     *
     */
    @ApiModelProperty(value = ""
        + "0:WAITING_FOR_QUEUE, 1:WAITING_ON_QUEUE, 2:RUNNING, 3:FINISHED_SUCCESS, 4: FINISHED_FAILURE"
        + "")
    @NotNull
    @Column(name = "name", unique = true, nullable = false)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimulationStatusType simulationStatusType = (SimulationStatusType) o;
        if(simulationStatusType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, simulationStatusType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SimulationStatusType{" +
            "id=" + id +
            ", name='" + name + "'" +
            '}';
    }
}
