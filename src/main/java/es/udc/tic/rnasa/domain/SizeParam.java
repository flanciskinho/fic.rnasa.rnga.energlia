/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SizeParam.
 */
@Entity
@Table(name = "size_param")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sizeparam")
public class SizeParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "size", nullable = false)
    private Integer size;

    @ManyToOne
    private Population belong;

    @ManyToOne
    private SizeType use;

    public SizeParam(Integer size, Population belong, SizeType use) {
        super();
        this.size = size;
        this.belong = belong;
        this.use = use;
    }

    public SizeParam() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Population getBelong() {
        return belong;
    }

    public void setBelong(Population population) {
        this.belong = population;
    }

    public SizeType getUse() {
        return use;
    }

    public void setUse(SizeType sizeType) {
        this.use = sizeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SizeParam sizeParam = (SizeParam) o;
        if(sizeParam.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, sizeParam.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SizeParam{" +
            "id=" + id +
            ", size='" + size + "'" +
            '}';
    }
}
