/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SpecificSimulation.
 */
@Entity
@Table(name = "specific_simulation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "specificsimulation")
public class SpecificSimulation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * version log file
     *
     */
    @ApiModelProperty(value = ""
        + "version log file                                                   "
        + "")
    @Column(name = "log_version")
    private String logVersion;

    @Lob
    @Column(name = "filelog")
    private byte[] filelog;

    @Column(name = "filelog_content_type")
    private String filelogContentType;

    /**
     * version individual
     *
     */
    @ApiModelProperty(value = ""
        + "version individual                                                 "
        + "")
    @Column(name = "node_version")
    private String nodeVersion;

    /**
     * jobid assigned by server where the job is launched
     *
     */
    @ApiModelProperty(value = ""
        + "jobid assigned by server where the job is launched                 "
        + "")
    @Column(name = "jobid")
    private Long jobid;

    @ManyToOne
    private SimulationStatusType status;

    @ManyToOne
    private Simulation belong;

    @ManyToOne
    private SshAccount launchBy;

    @ManyToOne
    private DatasetCombination useData;

    public SpecificSimulation() {}

    public SpecificSimulation(SimulationStatusType status, Simulation belong, DatasetCombination useData) {
        this.status = status;
        this.belong = belong;
        this.useData = useData;
    }

    public SpecificSimulation(SpecificSimulation another) {
        this.status  = another.status;
        this.belong  = another.belong;
        this.useData = another.useData;

        this.jobid = another.jobid;
        this.nodeVersion = another.nodeVersion;
        this.status = another.status;

        this.launchBy = another.launchBy;

        this.logVersion = another.logVersion;

        this.filelog = another.filelog;
        this.filelogContentType = another.filelogContentType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogVersion() {
        return logVersion;
    }

    public void setLogVersion(String logVersion) {
        this.logVersion = logVersion;
    }

    public byte[] getFilelog() {
        return filelog;
    }

    public void setFilelog(byte[] filelog) {
        this.filelog = filelog;
    }

    public String getFilelogContentType() {
        return filelogContentType;
    }

    public void setFilelogContentType(String filelogContentType) {
        this.filelogContentType = filelogContentType;
    }

    public String getNodeVersion() {
        return nodeVersion;
    }

    public void setNodeVersion(String nodeVersion) {
        this.nodeVersion = nodeVersion;
    }

    public Long getJobid() {
        return jobid;
    }

    public void setJobid(Long jobid) {
        this.jobid = jobid;
    }

    public SimulationStatusType getStatus() {
        return status;
    }

    public void setStatus(SimulationStatusType simulationStatusType) {
        this.status = simulationStatusType;
    }

    public Simulation getBelong() {
        return belong;
    }

    public void setBelong(Simulation simulation) {
        this.belong = simulation;
    }

    public SshAccount getLaunchBy() {
        return launchBy;
    }

    public void setLaunchBy(SshAccount sshAccount) {
        this.launchBy = sshAccount;
    }

    public DatasetCombination getUseData() {
        return useData;
    }

    public void setUseData(DatasetCombination datasetCombination) {
        this.useData = datasetCombination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpecificSimulation specificSimulation = (SpecificSimulation) o;
        if(specificSimulation.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, specificSimulation.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SpecificSimulation{" +
            "id=" + id +
            ((useData == null)?"":(", dc{id='"+useData.getId()+"', belongId='"+useData.getBelong().getId()+"', index='"+useData.getIndexCombination()+"}")) +
            ((launchBy == null)? "": (", launchBy{id='"+launchBy.getId()+"', username='"+launchBy.getUsername()+"'}")) +
            ", status='" + (status == null ? "null" : status.getName()) + "'" +
            ", logVersion='" + logVersion + "'" +
            ", filelog='" + filelog + "'" +
            ", nodeVersion='" + nodeVersion + "'" +
            ", jobid='" + jobid + "'" +
            '}';
    }
}
