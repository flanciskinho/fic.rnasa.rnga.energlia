/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SshAccount.
 */
@Entity
@Table(name = "ssh_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sshaccount")
public class SshAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "username", nullable = false)
    private String username;

    /**
     * rsa key
     *
     */
    @ApiModelProperty(value = "rsa key")
    @NotNull
    @Size(max = 2264)
    @Column(name = "keyaccount", nullable = false)
    private String key;

    @NotNull
    @Column(name = "salt", nullable = false, length = 50)
    private String salt;

    @ApiModelProperty(value = "current number of jobs on its queue")
    @NotNull
    @Column(name = "num_job", nullable = false)
    private Integer numJob;


    /**
     * don't use this account if activated is false
     *
     */
    @ApiModelProperty(value = "don't use this account if activated is false")
    @NotNull
    @Column(name = "activated", nullable = false)
    private Boolean activated;

    @Version
    @Column(name="version")
    private long version = 0l;

    @ManyToOne
    private SshServer belong;


    public SshAccount() {
        this.numJob = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Integer getNumJob() {
        return numJob;
    }

    public void setNumJob(Integer numJob) {
        this.numJob = numJob;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Boolean isActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public SshServer getBelong() {
        return belong;
    }

    public void setBelong(SshServer sshServer) {
        this.belong = sshServer;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SshAccount sshAccount = (SshAccount) o;
        if(sshAccount.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, sshAccount.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SshAccount{" +
            "id=" + id +
            ", username='" + username + "'" +
            //", key='" + key + "'" +
            ", salt='" + salt + "'" +
            ", numJob='" + numJob + "'" +
            ", activated='" + activated + "'" +
            '}';
    }
}
