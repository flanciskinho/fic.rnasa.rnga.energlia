/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SshServer.
 */
@Entity
@Table(name = "ssh_server")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sshserver")
public class SshServer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "dnsname", unique = true, nullable = false)
    private String dnsname;

    @NotNull
    @Column(name = "port", nullable = false)
    private Integer port;

    @NotNull
    @Size(min = 47, max = 47)
    @Column(name = "fingerprint", length = 47, nullable = false)
    private String fingerprint;

    /**
     * max jobs for queue
     *
     */
    @ApiModelProperty(value = ""
        + "max jobs for queue                                                 "
        + "")
    @NotNull
    @Column(name = "max_job", nullable = false)
    private Integer maxJob;

    /**
     * max procs to use when job is launched
     *
     */
    @ApiModelProperty(value = "max procs to use when job is launched")
    @NotNull
    @Column(name = "max_proc", nullable = false)
    private Integer maxProc;

    /**
     * max time in minutes to execute a job
     *
     */
    @ApiModelProperty(value = "max time in minutes to execute a job")
    @NotNull
    @Column(name = "max_time", nullable = false)
    private Long maxTime;


    /**
     * max space for account in bytes
     *
     */
    @ApiModelProperty(value = ""
        + "max space for account in bytes                                     "
        + "")
    @NotNull
    @Column(name = "max_space", nullable = false)
    private Long maxSpace;

    /**
     * max memory to use when job is launched
     *
     */
    @ApiModelProperty(value = ""
        + "max memory to use when job is launched                             "
        + "")
    @NotNull
    @Column(name = "max_memory", nullable = false)
    private Long maxMemory;

    /**
     * don't use this server if activated is false
     *
     */
    @ApiModelProperty(value = ""
        + "don't use this server if activated is false                        "
        + "")
    @NotNull
    @Column(name = "activated", nullable = false)
    private Boolean activated;

    /**
     * to be polite with the server
     *
     */
    @ApiModelProperty(value = "to be polite with the server")
    @NotNull
    @Column(name="max_connection", nullable = false)
    private Integer maxConnection;

    /**
     * current connections with the server
     *
     */
    @ApiModelProperty(value = "current connections with the server")
    @NotNull
    @Column(name="num_connection", nullable = false)
    private Integer numConnection;


    @Version
    @Column(name="version")
    private long version = 0l;

    @ManyToOne
    private QueueType use;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDnsname() {
        return dnsname;
    }

    public void setDnsname(String dnsname) {
        this.dnsname = dnsname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public Integer getMaxJob() {
        return maxJob;
    }

    public void setMaxJob(Integer maxJob) {
        this.maxJob = maxJob;
    }

    public Integer getMaxProc() {
        return maxProc;
    }

    public void setMaxProc(Integer maxProc) {
        this.maxProc = maxProc;
    }

    public Long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(Long maxTime) {
        this.maxTime = maxTime;
    }

    public Long getMaxSpace() {
        return maxSpace;
    }

    public void setMaxSpace(Long maxSpace) {
        this.maxSpace = maxSpace;
    }

    public Long getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(Long maxMemory) {
        this.maxMemory = maxMemory;
    }

    public Boolean isActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Integer getMaxConnection() {
        return maxConnection;
    }

    public void setMaxConnection(Integer maxConnection) {
        this.maxConnection = maxConnection;
    }

    public Integer getNumConnection() {
        return numConnection;
    }

    public void setNumConnection(Integer numConnection) {
        this.numConnection = numConnection == null || numConnection < 0? 0: numConnection;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public QueueType getUse() {
        return use;
    }

    public void setUse(QueueType queueType) {
        this.use = queueType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SshServer sshServer = (SshServer) o;
        if(sshServer.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, sshServer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SshServer{" +
            "id=" + id +
            ", dnsname='" + dnsname + '\'' +
            ", port=" + port +
            ", fingerprint='" + fingerprint + '\'' +
            ", maxJob=" + maxJob +
            ", maxProc=" + maxProc +
            ", maxTime=" + maxTime +
            ", maxSpace=" + maxSpace +
            ", maxMemory=" + maxMemory +
            ", activated=" + activated +
            ", maxConnection=" + maxConnection +
            ", numConnection=" + numConnection +
            ", version=" + version +
            ", use=" + use +
            '}';
    }
}
