/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.repository;


import es.udc.tic.rnasa.domain.Dataset;
import es.udc.tic.rnasa.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Dataset entity.
 */
public interface DatasetRepository extends JpaRepository<Dataset,Long> {

    @Query("select dataset from Dataset dataset where dataset.owner.login = ?#{principal.username}")
    List<Dataset> findByOwnerIsCurrentUser();

//    @Query("select dataset from Dataset dataset where dataset.owner.login = :login")
//    Page<Dataset> findAllByUserLogin(Pageable pageable, @Param("login") String login);

    @Query (
        "select d " +
        "from Dataset d " +
        "where d.owner = :userProfile " +
           "or :userProfile in elements(d.shares)"
    )
    Page<Dataset> findAllByUserLogin(Pageable pageable, @Param("userProfile") User userProfile);
}
