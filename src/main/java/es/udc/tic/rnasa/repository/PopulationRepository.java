/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.repository;

import es.udc.tic.rnasa.domain.Population;
import es.udc.tic.rnasa.domain.SizeType;
import es.udc.tic.rnasa.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Population entity.
 */
public interface PopulationRepository extends JpaRepository<Population,Long> {

    @Query("select population from Population population where population.owner.login = ?#{principal.username}")
    List<Population> findByOwnerIsCurrentUser();

/*
    @Query("select population from Population population where population.owner.login = :login")
    Page<Population> findAllByUserLogin(Pageable pageable, @Param("login") String login);
*/

    @Query (
        "select p " +
        "from Population p " +
        "where p.owner = :userProfile " +
           "or :userProfile in elements(p.shares) "
    )
    Page<Population> findAllByUserLogin(Pageable pageable, @Param("userProfile") User userProfile);

    @Query(
        "select distinct sizeParam.belong " +
        "from SizeParam sizeParam " +
        "where " +
           "   (     (sizeParam.size = :sf and sizeParam.use = :tf)" +
           "      or (sizeParam.size = :ss and sizeParam.use = :ts)" +
           "      or (sizeParam.size = :sb and sizeParam.use = :tb)" +
           "   )" +
          "and sizeParam.belong.sizeGenotype = (:ss + :sf + :sb)"
    )
    Page<Population> findAllBySize(Pageable pageable,
                                   @Param("ss") Integer size_short, @Param("ts") SizeType type_short,
                                   @Param("sf") Integer size_float, @Param("tf") SizeType type_float,
                                   @Param("sb") Integer size_bool , @Param("tb") SizeType type_bool);


    @Query(
        "select distinct sizeParam.belong " +
            "from SizeParam sizeParam " +
            "where " +
            "   (     (sizeParam.size = :sf and sizeParam.use = :tf) " +
            "      or (sizeParam.size = :ss and sizeParam.use = :ts) " +
            "      or (sizeParam.size = :sb and sizeParam.use = :tb) " +
            "   )" +
            "and sizeParam.belong.sizeGenotype = (:ss + :sf + :sb) " +
            "and ( " +
                    "sizeParam.belong.owner = :userProfile " +
                "or " +
                    ":userProfile in elements(sizeParam.belong.shares) " +
                ") "
    )
    Page<Population> findAllBySizeAndUserLogin(Pageable pageable,
                                               @Param("ss") Integer size_short, @Param("ts") SizeType type_short,
                                               @Param("sf") Integer size_float, @Param("tf") SizeType type_float,
                                               @Param("sb") Integer size_bool , @Param("tb") SizeType type_bool,
                                               @Param("userProfile") User userProfile);


    List<Population> findByIdIn(List<Long> populationId);


    @Query(
        "select po.use " +
            "from PopulationOrder po " +
            "where po.belong.belong.id = :simulationId " +
            "order by po.indexOrder")
    List<Population> findAllBySimulationId(@Param("simulationId") Long simulationId);
}
