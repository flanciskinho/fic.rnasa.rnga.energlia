/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.repository;

import es.udc.tic.rnasa.domain.ProblemType;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the ProblemType entity.
 */
public interface ProblemTypeRepository extends JpaRepository<ProblemType,Long> {

    @Query("select distinct problemType from ProblemType problemType left join fetch problemType.minimizes")
    List<ProblemType> findAllWithEagerRelationships();

    @Query("select problemType from ProblemType problemType left join fetch problemType.minimizes where problemType.id =:id")
    ProblemType findOneWithEagerRelationships(@Param("id") Long id);

    ProblemType findOneByName(String name);
}
