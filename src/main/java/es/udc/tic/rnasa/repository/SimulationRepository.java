/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.repository;

import es.udc.tic.rnasa.domain.Population;
import es.udc.tic.rnasa.domain.Simulation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Simulation entity.
 */
public interface SimulationRepository extends JpaRepository<Simulation,Long> {

    @Query("select simulation from Simulation simulation where simulation.belong.login = ?#{principal.username}")
    List<Simulation> findByBelongIsCurrentUser();

    @Query("select simulation from Simulation simulation where simulation.belong.login = :login")
    Page<Simulation> findAllByUserLogin(Pageable pageable, @Param("login") String login);

    @Query("select po.use from PopulationOrder po where po.belong.belong.id = :simulationId")
    List<Population> findAllPopulations(@Param("simulationId") Long simulationId);
}
