/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.security.aes;

/**
 * Created by flanciskinho on 29/4/16.
 */
public enum AesLengthConstants {
    AES_128(128),
    AES_192(192),
    AES_256(256);

    private int length;
    AesLengthConstants(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }
}
