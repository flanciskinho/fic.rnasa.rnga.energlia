/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.security.random;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by flanciskinho on 17/5/16.
 */
public class IntegerRandom {

    private static Random rnd = new SecureRandom();

    public static String getSecureRandomInteger(int length) {
        StringBuffer buf = new StringBuffer();

        buf.append(rnd.nextInt(9)+1);
        for (int cnt = 0; cnt < (length-1) /2; cnt++)
            buf.append(String.format("%02d", rnd.nextInt(100)));

        if (length != buf.length())
            buf.append(rnd.nextInt(10));

        return buf.toString();
    }
}
