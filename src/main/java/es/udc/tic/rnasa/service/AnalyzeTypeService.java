/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.AnalyzeType;
import es.udc.tic.rnasa.repository.AnalyzeTypeRepository;
import es.udc.tic.rnasa.repository.search.AnalyzeTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.AnalyzeTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.AnalyzeTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing AnalyzeType.
 */
@Service
@Transactional
public class AnalyzeTypeService {

    private final Logger log = LoggerFactory.getLogger(AnalyzeTypeService.class);

    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    @Inject
    private AnalyzeTypeMapper analyzeTypeMapper;

    @Inject
    private AnalyzeTypeSearchRepository analyzeTypeSearchRepository;

    private static AnalyzeType CLASSIFICATION = null;
    private static AnalyzeType REGRESSION = null;


    @Transactional(readOnly = true)
    public AnalyzeType getCLASSIFICATION() {
        if (CLASSIFICATION == null) {
            Optional<AnalyzeType> optional = analyzeTypeRepository.findOneByName("CLASSIFICATION");

            CLASSIFICATION = optional.orElse(null);
        }
        return CLASSIFICATION;
    }

    @Transactional(readOnly = true)
    public AnalyzeType getREGRESSION() {
        if (REGRESSION == null) {
            Optional<AnalyzeType> optional = analyzeTypeRepository.findOneByName("REGRESSION");
            REGRESSION = optional.orElse(null);
        }
        return REGRESSION;
    }

    /**
     * Save a analizeType.
     *
     * @param analyzeTypeDTO the entity to save
     * @return the persisted entity
     */
    public AnalyzeTypeDTO save(AnalyzeTypeDTO analyzeTypeDTO) {
        log.debug("Request to save AnalyzeType : {}", analyzeTypeDTO);
        AnalyzeType analyzeType = analyzeTypeMapper.analyzeTypeDTOToAnalyzeType(analyzeTypeDTO);
        analyzeType = analyzeTypeRepository.save(analyzeType);
        AnalyzeTypeDTO result = analyzeTypeMapper.analyzeTypeToAnalyzeTypeDTO(analyzeType);
        analyzeTypeSearchRepository.save(analyzeType);
        return result;
    }

    /**
     *  Get all the analizeTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AnalyzeType> findAll(Pageable pageable) {
        log.debug("Request to get all AnalizeTypes");
        Page<AnalyzeType> result = analyzeTypeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one analizeType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AnalyzeTypeDTO findOne(Long id) {
        log.debug("Request to get AnalyzeType : {}", id);
        AnalyzeType analyzeType = analyzeTypeRepository.findOne(id);
        AnalyzeTypeDTO analyzeTypeDTO = analyzeTypeMapper.analyzeTypeToAnalyzeTypeDTO(analyzeType);
        return analyzeTypeDTO;
    }

    /**
     *  Delete the  analizeType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AnalyzeType : {}", id);
        analyzeTypeRepository.delete(id);
        analyzeTypeSearchRepository.delete(id);
    }

    /**
     * Search for the analizeType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AnalyzeType> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AnalizeTypes for query {}", query);
        return analyzeTypeSearchRepository.search(queryStringQuery(query), pageable);
    }
}
