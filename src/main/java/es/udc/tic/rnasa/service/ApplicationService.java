/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.Application;
import es.udc.tic.rnasa.repository.ApplicationRepository;
import es.udc.tic.rnasa.repository.search.ApplicationSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ApplicationDTO;
import es.udc.tic.rnasa.web.rest.mapper.ApplicationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Application.
 */
@Service
@Transactional
public class ApplicationService {

    private final Logger log = LoggerFactory.getLogger(ApplicationService.class);

    private final static String APP_PATHNAME = ".energlia/exe";

    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private ApplicationMapper applicationMapper;

    @Inject
    private ApplicationSearchRepository applicationSearchRepository;

    @Transactional(readOnly = true)
    public String getAppPathname() {
        return APP_PATHNAME;
    }

    /**
     * Save a application.
     *
     * @param applicationDTO the entity to save
     * @return the persisted entity
     */
    public ApplicationDTO save(ApplicationDTO applicationDTO) {
        log.debug("Request to save Application : {}", applicationDTO);
        applicationDTO.setTimestamp(ZonedDateTime.now());
        Application application = applicationMapper.applicationDTOToApplication(applicationDTO);
        application = applicationRepository.save(application);
        ApplicationDTO result = applicationMapper.applicationToApplicationDTO(application);
        applicationSearchRepository.save(application);
        return result;
    }

    /**
     *  Get all the applications.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Application> findAll(Pageable pageable) {
        log.debug("Request to get all Applications");
        Page<Application> result = applicationRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one application by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ApplicationDTO findOne(Long id) {
        log.debug("Request to get Application : {}", id);
        Application application = applicationRepository.findOne(id);
        ApplicationDTO applicationDTO = applicationMapper.applicationToApplicationDTO(application);
        return applicationDTO;
    }

    /**
     *  Delete the  application by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Application : {}", id);
        applicationRepository.delete(id);
        applicationSearchRepository.delete(id);
    }

    /**
     * Search for the application corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Application> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Applications for query {}", query);
        return applicationSearchRepository.search(queryStringQuery(query), pageable);
    }
}
