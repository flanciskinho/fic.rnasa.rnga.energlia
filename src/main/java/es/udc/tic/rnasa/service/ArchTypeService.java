/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.ArchType;
import es.udc.tic.rnasa.repository.ArchTypeRepository;
import es.udc.tic.rnasa.repository.search.ArchTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ArchTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ArchTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing ArchType.
 */
@Service
@Transactional
public class ArchTypeService {

    private final Logger log = LoggerFactory.getLogger(ArchTypeService.class);

    @Inject
    private ArchTypeRepository archTypeRepository;

    @Inject
    private ArchTypeMapper archTypeMapper;

    @Inject
    private ArchTypeSearchRepository archTypeSearchRepository;

    /**
     * Save a archType.
     *
     * @param archTypeDTO the entity to save
     * @return the persisted entity
     */
    public ArchTypeDTO save(ArchTypeDTO archTypeDTO) {
        log.debug("Request to save ArchType : {}", archTypeDTO);
        ArchType archType = archTypeMapper.archTypeDTOToArchType(archTypeDTO);
        archType = archTypeRepository.save(archType);
        ArchTypeDTO result = archTypeMapper.archTypeToArchTypeDTO(archType);
        archTypeSearchRepository.save(archType);
        return result;
    }

    /**
     *  Get all the archTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ArchType> findAll(Pageable pageable) {
        log.debug("Request to get all ArchTypes");
        Page<ArchType> result = archTypeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one archType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ArchTypeDTO findOne(Long id) {
        log.debug("Request to get ArchType : {}", id);
        ArchType archType = archTypeRepository.findOne(id);
        ArchTypeDTO archTypeDTO = archTypeMapper.archTypeToArchTypeDTO(archType);
        return archTypeDTO;
    }

    /**
     *  Delete the  archType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ArchType : {}", id);
        archTypeRepository.delete(id);
        archTypeSearchRepository.delete(id);
    }

    /**
     * Search for the archType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ArchType> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ArchTypes for query {}", query);
        return archTypeSearchRepository.search(queryStringQuery(query), pageable);
    }

    public List<ArchTypeDTO> findAllByBelongIdAndActivatedIsTrue(Long id) {
        log.debug("Request to get all ArchType by belong id {}", id);

        List<ArchType> list = archTypeRepository.findAllByBelongIdAndActivatedIsTrue(id);
        List<ArchTypeDTO> result = archTypeMapper.archTypesToArchTypeDTOs(list);

        return result;
    }

}
