/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.CompiledApp;
import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.repository.CompiledAppRepository;
import es.udc.tic.rnasa.repository.search.CompiledAppSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotTransferConnectionException;
import es.udc.tic.rnasa.service.exceptions.NoDataException;
import es.udc.tic.rnasa.web.rest.dto.CompiledAppDTO;
import es.udc.tic.rnasa.web.rest.mapper.CompiledAppMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing CompiledApp.
 */
@Service
// This anotation doesn't work with AuthoritiesConstants.ADMIN
@Transactional
public class CompiledAppService {

    private final Logger log = LoggerFactory.getLogger(CompiledAppService.class);

    @Inject
    private ApplicationService applicationService;

    @Inject
    private CompiledAppRepository compiledAppRepository;

    @Inject
    private CompiledAppMapper compiledAppMapper;

    @Inject
    private CompiledAppSearchRepository compiledAppSearchRepository;

    @Inject
    private SshAccountService sshAccountService;

    /**
     * Save a compiledApp.
     *
     * @param compiledAppDTO the entity to save
     * @return the persisted entity
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public CompiledAppDTO save(CompiledAppDTO compiledAppDTO) {
        log.debug("Request to save CompiledApp : {}", compiledAppDTO);
        CompiledApp compiledApp = compiledAppMapper.compiledAppDTOToCompiledApp(compiledAppDTO);
        compiledApp = compiledAppRepository.save(compiledApp);
        CompiledAppDTO result = compiledAppMapper.compiledAppToCompiledAppDTO(compiledApp);
        compiledAppSearchRepository.save(compiledApp);
        return result;
    }

    /**
     *  Get all the compiledApps.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Secured(AuthoritiesConstants.ADMIN)
    @Transactional(readOnly = true)
    public Page<CompiledApp> findAll(Pageable pageable) {
        log.debug("Request to get all CompiledApps");
        Page<CompiledApp> result = compiledAppRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one compiledApp by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Secured(AuthoritiesConstants.ADMIN)
    @Transactional(readOnly = true)
    public CompiledAppDTO findOne(Long id) {
        log.debug("Request to get CompiledApp : {}", id);
        CompiledApp compiledApp = compiledAppRepository.findOne(id);
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(compiledApp);
        return compiledAppDTO;
    }

    /**
     *  Delete the  compiledApp by id.
     *
     *  @param id the id of the entity
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void delete(Long id) {
        log.debug("Request to delete CompiledApp : {}", id);
        compiledAppRepository.delete(id);
        compiledAppSearchRepository.delete(id);
    }

    /**
     * Search for the compiledApp corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Page<CompiledApp> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CompiledApps for query {}", query);
        return compiledAppSearchRepository.search(queryStringQuery(query), pageable);
    }


    public CompiledApp sendCompiledApp(SshServer sshServer, ExecConnectionServer exec, TransferConnectionServer transfer) throws NoDataException, CannotTransferConnectionException, CannotExecConnectionException {
        CompiledApp compiledApp = compiledAppRepository.findFirstByUseIdAndBelongActivatedIsTrueOrderByBelongIdDesc(sshServer.getId());
        if (compiledApp == null)
            throw new NoDataException(String.format("No compiled App for %s", sshServer.getDnsname()), this.getClass());

        if (!sshAccountService.exists(exec, applicationService.getAppPathname()))
            sshAccountService.launchCommand(exec, String.format("mkdir -p %s", applicationService.getAppPathname()));

        String pathname = String.format("%s/%d.out", applicationService.getAppPathname(), compiledApp.getId());
        if (!sshAccountService.exists(exec, pathname)) {
            transfer.sendData(compiledApp.getExecutable(), pathname);
            exec.exec("chmod +x "+pathname);
        }



        return compiledApp;
    }
}
