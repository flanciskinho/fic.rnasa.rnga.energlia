/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.Dataset;
import es.udc.tic.rnasa.domain.DatasetCombination;
import es.udc.tic.rnasa.repository.DatasetCombinationRepository;
import es.udc.tic.rnasa.repository.search.DatasetCombinationSearchRepository;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotTransferConnectionException;
import es.udc.tic.rnasa.service.exceptions.NoDataException;
import es.udc.tic.rnasa.web.rest.dto.DatasetCombinationDTO;
import es.udc.tic.rnasa.web.rest.mapper.DatasetCombinationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing DatasetCombination.
 */
@Service
@Transactional
public class DatasetCombinationService {

    private final Logger log = LoggerFactory.getLogger(DatasetCombinationService.class);

    private static final String DATASET_PATHNAME = ".energlia/dataset/%d";

    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private DatasetCombinationMapper datasetCombinationMapper;

    @Inject
    private DatasetCombinationSearchRepository datasetCombinationSearchRepository;

    @Inject
    private SshAccountService sshAccountService;

    public String getDatasetPathname() {
        return DATASET_PATHNAME;
    }

    /**
     * Save a datasetCombination.
     *
     * @param datasetCombinationDTO the entity to save
     * @return the persisted entity
     */
    public DatasetCombinationDTO save(DatasetCombinationDTO datasetCombinationDTO) {
        log.debug("Request to save DatasetCombination : {}", datasetCombinationDTO);
        DatasetCombination datasetCombination = datasetCombinationMapper.datasetCombinationDTOToDatasetCombination(datasetCombinationDTO);
        datasetCombination = datasetCombinationRepository.save(datasetCombination);
        DatasetCombinationDTO result = datasetCombinationMapper.datasetCombinationToDatasetCombinationDTO(datasetCombination);
        datasetCombinationSearchRepository.save(datasetCombination);
        return result;
    }

    /**
     *  Get all the datasetCombinations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DatasetCombination> findAll(Pageable pageable) {
        log.debug("Request to get all DatasetCombinations");
        Page<DatasetCombination> result = datasetCombinationRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one datasetCombination by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DatasetCombinationDTO findOne(Long id) {
        log.debug("Request to get DatasetCombination : {}", id);
        DatasetCombination datasetCombination = datasetCombinationRepository.findOne(id);
        DatasetCombinationDTO datasetCombinationDTO = datasetCombinationMapper.datasetCombinationToDatasetCombinationDTO(datasetCombination);
        return datasetCombinationDTO;
    }

    /**
     *  Delete the  datasetCombination by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DatasetCombination : {}", id);
        datasetCombinationRepository.delete(id);
        datasetCombinationSearchRepository.delete(id);
    }

    /**
     * Search for the datasetCombination corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DatasetCombination> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DatasetCombinations for query {}", query);
        return datasetCombinationSearchRepository.search(queryStringQuery(query), pageable);
    }


    private void sendData(byte[] content, String filename, ExecConnectionServer exec, TransferConnectionServer transfer) throws CannotExecConnectionException, CannotTransferConnectionException {
        if (content == null)
            return;
        if (!sshAccountService.exists(exec, filename)) {
            transfer.sendData(new String(content), filename);
        }
    }

    private void sendData(DatasetCombination datasetCombination, ExecConnectionServer exec, TransferConnectionServer transfer) throws CannotExecConnectionException, CannotTransferConnectionException {
        sendData(datasetCombination.getTrainfilein(),  String.format("%s/%d.train.in" , String.format(DATASET_PATHNAME, datasetCombination.getBelong().getId()), datasetCombination.getId()), exec, transfer);
        sendData(datasetCombination.getTrainfileout(), String.format("%s/%d.train.out", String.format(DATASET_PATHNAME, datasetCombination.getBelong().getId()), datasetCombination.getId()), exec, transfer);

        sendData(datasetCombination.getValidationfilein(),  String.format("%s/%d.validation.in" , String.format(DATASET_PATHNAME, datasetCombination.getBelong().getId()), datasetCombination.getId()), exec, transfer);
        sendData(datasetCombination.getValidationfileout(), String.format("%s/%d.validation.out", String.format(DATASET_PATHNAME, datasetCombination.getBelong().getId()), datasetCombination.getId()), exec, transfer);

        sendData(datasetCombination.getTestfilein(),  String.format("%s/%d.test.in" , String.format(DATASET_PATHNAME, datasetCombination.getBelong().getId()), datasetCombination.getId()), exec, transfer);
        sendData(datasetCombination.getTestfileout(), String.format("%s/%d.test.out", String.format(DATASET_PATHNAME, datasetCombination.getBelong().getId()), datasetCombination.getId()), exec, transfer);
    }

/*
    @Transactional(readOnly = true)
    public void sendData(Dataset dataset, SshAccount sshAccount) throws NoDataException, TooMuchConnectionException, CannotOpenConnectionException, CannotExecConnectionException, CannotTransferConnectionException {
        List<DatasetCombination> datasetCombinationList = datasetCombinationRepository.findAllByBelongId(dataset.getId());
        if (datasetCombinationList.size() == 0)
            throw new NoDataException(String.format("dataset with out combinations (id: %d, name: %s)", dataset.getId(), dataset.getName()), this.getClass());


        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;
        try {
            exec     = (ExecConnectionServer)     sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);
            transfer = (TransferConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.SFTP);

            if (!sshAccountService.exists(exec, String.format(DATASET_PATHNAME, dataset.getId())))
                sshAccountService.launchCommand(exec, String.format("mkdir -p %s", String.format(DATASET_PATHNAME, dataset.getId())));

            for (DatasetCombination dc: datasetCombinationList)
                sendData(dc, exec, transfer);
        } catch (CannotTransferConnectionException | TooMuchConnectionException | CannotOpenConnectionException | CannotExecConnectionException e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
            if (transfer != null)
                sshAccountService.closeConnection(transfer);

            throw e;
        }

    }
*/

    public void sendData(Dataset dataset, ExecConnectionServer exec, TransferConnectionServer transfer) throws CannotExecConnectionException, NoDataException, CannotTransferConnectionException {
        List<DatasetCombination> datasetCombinationList = datasetCombinationRepository.findAllByBelongId(dataset.getId());
        if (datasetCombinationList.size() == 0)
            throw new NoDataException(String.format("dataset with out combinations (id: %d, name: %s)", dataset.getId(), dataset.getName()), this.getClass());

        if (!sshAccountService.exists(exec, String.format(DATASET_PATHNAME, dataset.getId())))
            sshAccountService.launchCommand(exec, String.format("mkdir -p %s", String.format(DATASET_PATHNAME, dataset.getId())));

        for (DatasetCombination dc: datasetCombinationList)
            sendData(dc, exec, transfer);
    }
}
