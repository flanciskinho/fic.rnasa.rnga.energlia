/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.repository.search.DatasetSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.security.SecurityUtils;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.service.util.CheckPermission;
import es.udc.tic.rnasa.service.util.RegexUtil;
import es.udc.tic.rnasa.web.rest.dto.CustomDescriptionDTO;
import es.udc.tic.rnasa.web.rest.dto.DatasetDTO;
import es.udc.tic.rnasa.web.rest.mapper.DatasetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Dataset.
 */
@Service
@Transactional
public class DatasetService {

    private final Logger log = LoggerFactory.getLogger(DatasetService.class);

    @Inject
    private DatasetRepository datasetRepository;

    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private DatasetMapper datasetMapper;

    @Inject
    private DatasetSearchRepository datasetSearchRepository;

    @Inject
    private AnalyzeTypeService analyzeTypeService;
    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    private static final String TEXTPLAIN = "text/plain";

    public DatasetDTO share(Long datasetId, String login) throws HttpExceptionDetails{
        log.debug("Request to share Dataset: {}", datasetId);

        Dataset dataset = datasetRepository.findOne(datasetId);
        if (dataset == null)
            throw new HttpExceptionDetails(HttpStatus.NOT_FOUND, "Dataset not found");

        Optional<User> userOptional = userRepository.findOneByLogin(login);
        if (!userOptional.isPresent())
            throw new HttpExceptionDetails(HttpStatus.NOT_FOUND, "User not found");

        User user = userOptional.get();

        Set<User> shares = dataset.getShares();

        log.debug("{} share with:", dataset.getName());
        shares.stream().forEach(u -> log.debug("{} share with: {}", dataset.getName(), u.getLogin()));

        if (shares.contains(user))
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "User already has access");

        shares.add(user);
        dataset.setShares(shares);

        datasetRepository.save(dataset);

        return datasetMapper.datasetToDatasetDTO(dataset);
    }

    /**
     * Save a dataset.
     *
     * @param datasetDTO the entity to save
     * @return the persisted entity
     */
    public DatasetDTO save(DatasetDTO datasetDTO) throws HttpExceptionDetails {
        log.debug("Request to save Dataset : {}", datasetDTO);

        new CheckPermission(userRepository).checkPermission(datasetDTO);

        Dataset dataset = datasetMapper.datasetDTOToDataset(datasetDTO);
        dataset.setAnalyze(analyzeTypeRepository.findOne(datasetDTO.getAnalyzeId()));
        dataset = datasetRepository.save(dataset);
        DatasetDTO result = datasetMapper.datasetToDatasetDTO(dataset);
        datasetSearchRepository.save(dataset);
        return result;
    }

    /**
     *  Get all the datasets.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Dataset> findAll(Pageable pageable) throws HttpExceptionDetails{
        log.debug("Request to get all Datasets");

        Page<Dataset> result;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))
            result  = datasetRepository.findAll(pageable);
        else {
            Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
            if (!user.isPresent())
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "invalid user");
            result = datasetRepository.findAllByUserLogin(pageable, user.get());
        }

        return result;
    }

    /**
     *  Get one dataset by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DatasetDTO findOne(Long id) throws HttpExceptionDetails {
        log.debug("Request to get Dataset : {}", id);
        Dataset dataset = datasetRepository.findOne(id);
        DatasetDTO datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);

        Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        if (user.isPresent() && dataset != null) {
            if (dataset.getOwner().equals(user.get()))
                return datasetDTO;
            if (dataset.getShares().contains(user.get()))
                return datasetDTO;
        }

        new CheckPermission(userRepository).checkPermission(datasetDTO);

        return datasetDTO;
    }

    /**
     *  Delete the  dataset by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) throws HttpExceptionDetails {
        log.debug("Request to delete Dataset : {}", id);
        Dataset dataset = datasetRepository.findOne(id);
        if (dataset == null)
            throw new HttpExceptionDetails(HttpStatus.NOT_FOUND, "not found", this.getClass());
        DatasetDTO datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);

        new CheckPermission(userRepository).checkPermission(datasetDTO);

        datasetRepository.delete(id);
        datasetSearchRepository.delete(id);
    }

    /**
     * Search for the dataset corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Dataset> search(String query, Pageable pageable) throws HttpExceptionDetails {
        log.debug("Request to search for a page of Datasets for query {}", query);
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))
            throw new HttpExceptionDetails(HttpStatus.NOT_IMPLEMENTED, "no implemented", this.getClass());

        return datasetSearchRepository.search(queryStringQuery(query), pageable);
    }


    ////
    //// custom services below this point
    ////

    /**
     * Method to check if the number of files are the same for input and output
     *
     * @param input   file for input patterns
     * @param output  file for output patterns
     * @param type    train, validation, test
     * @throws HttpExceptionDetails throw if the size of list are different
     */
    private void checkNumFiles(List<byte []> input, List<byte []> output, String type)
        throws HttpExceptionDetails {
        if (input.size() != output.size())
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, String.format("different number of files for %s", type), this.getClass());
    }

    private void checkNumFiles(List<byte []> trainInput,      List<byte []> trainOutput,
                               List<byte []> validationInput, List<byte []> validationOutput,
                               List<byte []> testInput,       List<byte []> testOutput)
        throws HttpExceptionDetails {
        if (trainInput.size() == 0)
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "no files for train", this.getClass());

        checkNumFiles(trainInput,      trainOutput,      "train");
        checkNumFiles(validationInput, validationOutput, "validation");
        checkNumFiles(testInput,       testOutput,       "test");

        // Only dataset for train
        if (validationInput.size() == 0 && testInput.size() == 0)
            return;

        // Dataset for train and validation
        if (trainInput.size() != validationInput.size())
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "different number of files", this.getClass());

        // Dataset for train, validaion and test
        if (testInput.size() != 0) {
            if (trainInput.size() != testInput.size())
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "different number of files", this.getClass());
        }



    }

    /**
     * Method to check if the number of patterns for input and output files are the same
     *
     * @param input
     * @param output
     * @param type
     * @throws HttpExceptionDetails
     */
    private void checkNumInputOutputPatterns(List<byte []> input, List<byte []> output, String type)
        throws HttpExceptionDetails {

        String in;
        String out;

        for (int cnt = 0; cnt < input.size(); cnt++) {
            in = new String(input.get(cnt));
            out = new String(output.get(cnt));

            if (in.split(RegexUtil.regexEndOfLine).length != out.split(RegexUtil.regexEndOfLine).length)
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, String.format("different number of patterns for combination %d to %s", cnt, type), this.getClass());
        }
    }

    private void checkNumInputOutputPatterns(List<byte[]> trainInput, List<byte[]> trainOutput, List<byte[]> validationInput, List<byte[]> validationOutput, List<byte[]> testInput, List<byte[]> testOutput) throws HttpExceptionDetails {

        checkNumInputOutputPatterns(trainInput,      trainOutput,      "train");
        checkNumInputOutputPatterns(validationInput, validationOutput, "validation");
        checkNumInputOutputPatterns(testInput,       testOutput,       "test");
    }

    /**
     * Method to check if the number of input set (train, validation and test) is the same than output set
     *
     * @param trainInput
     * @param trainOutput
     * @param validationInput
     * @param validationOutput
     * @param testInput
     * @param testOutput
     * @throws HttpExceptionDetails
     */
    private void checkNumSetPatterns(List<byte[]> trainInput, List<byte[]> trainOutput, List<byte[]> validationInput, List<byte[]> validationOutput, List<byte[]> testInput, List<byte[]> testOutput) throws HttpExceptionDetails {
        String trin, trout, vain="", vaout="", tein="", teout="";
        int in, out;
        for (int cnt = 0; cnt < trainInput.size(); cnt++) {
            trin  = new String(trainInput.get(cnt));
            trout = new String(trainOutput.get(cnt));
            if (!validationInput.isEmpty()) {
                vain  = new String(validationInput.get(cnt));
                vaout = new String(validationOutput.get(cnt));
            }
            if (!testInput.isEmpty()) {
                tein  = new String(testInput.get(cnt));
                teout = new String(testOutput.get(cnt));
            }

            in  = trin.split(RegexUtil.regexEndOfLine).length +
                  vain.split(RegexUtil.regexEndOfLine).length +
                  tein.split(RegexUtil.regexEndOfLine).length;
            out = trout.split(RegexUtil.regexEndOfLine).length +
                  vaout.split(RegexUtil.regexEndOfLine).length +
                  teout.split(RegexUtil.regexEndOfLine).length;

            if (in != out)
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, String.format("different number of patterns for set combination %d", cnt), this.getClass());

        }

    }

    private List<String> getPatterns(byte[] trainInput, byte[] trainOutput, byte[] validationInput, byte[] validationOutput, byte[] testInput, byte[] testOutput) {
        String trin, trout, vain="", vaout="", tein="", teout="";
        trin  = new String(trainInput);
        trout = new String(trainOutput);
        if (validationInput.length != 0) {
            vain  = new String(validationInput);
            vaout = new String(validationOutput);
        }
        if (testInput.length != 0) {
            tein  = new String(testInput);
            teout = new String(testOutput);
        }

        int cnt;
        List<String> patterns = new ArrayList<>();
        String tmp;

        String[] tmpin  = trin. split(RegexUtil.regexEndOfLine);
        String[] tmpout = trout.split(RegexUtil.regexEndOfLine);
        for (cnt = 0; cnt < tmpin.length; cnt++) {
            tmp = (tmpin[cnt] + tmpout[cnt]).replaceAll(RegexUtil.regexSpace, "").trim();
            if (!tmp.isEmpty())
                patterns.add(tmp);
        }

        tmpin  = vain. split(RegexUtil.regexEndOfLine);
        tmpout = vaout.split(RegexUtil.regexEndOfLine);
        for (cnt = 0; cnt < tmpin.length; cnt++) {
            tmp = (tmpin[cnt] + tmpout[cnt]).replaceAll(RegexUtil.regexSpace, "").trim();
            if (!tmp.isEmpty())
                patterns.add(tmp);
        }

        tmpin  = tein. split(RegexUtil.regexEndOfLine);
        tmpout = teout.split(RegexUtil.regexEndOfLine);
        for (cnt = 0; cnt < tmpin.length; cnt++) {
            tmp = (tmpin[cnt] + tmpout[cnt]).replaceAll(RegexUtil.regexSpace, "").trim();
            if (!tmp.isEmpty())
                patterns.add(tmp);
        }

        Collections.sort(patterns);

        return patterns;

    }

    private int checkAllHaveSamePatterns(List<byte[]> trainInput, List<byte[]> trainOutput, List<byte[]> validationInput, List<byte[]> validationOutput, List<byte[]> testInput, List<byte[]> testOutput) throws HttpExceptionDetails {
        List<String> firstPatterns = getPatterns(trainInput.get(0), trainOutput.get(0),
            validationInput.size()  != 0?  validationInput.get(0): "".getBytes(),
            validationOutput.size() != 0? validationOutput.get(0): "".getBytes(),
            testInput.size()  != 0?  testInput.get(0): "".getBytes(),
            testOutput.size() != 0? testOutput.get(0): "".getBytes());


        int tmp;
        List<String> patterns;
        for (int cnt = 1; cnt < trainInput.size(); cnt++) {
            patterns = getPatterns(trainInput.get(cnt), trainOutput.get(cnt),
                validationInput.size()  != 0?  validationInput.get(cnt): "".getBytes(),
                validationOutput.size() != 0? validationOutput.get(cnt): "".getBytes(),
                testInput.size()  != 0?  testInput.get(cnt): "".getBytes(),
                testOutput.size() != 0? testOutput.get(cnt): "".getBytes());

            for (tmp = 0; tmp < firstPatterns.size(); tmp++) {
                if (!firstPatterns.get(tmp).equals(patterns.get(tmp))) {
log.info("set {} line {}:\n<{}>\n<{}>", cnt, tmp, firstPatterns.get(tmp), patterns.get(tmp));
                    throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "different patterns found", this.getClass());
                }
            }
        }

        return firstPatterns.size();
    }

    private int checkNumFeatures(byte[] file) throws HttpExceptionDetails {
        if (file.length == 0)
            return 0;

        String aux = new String(file);
        if (aux.trim().isEmpty())
            return 0;

        String[] lines = aux.split(RegexUtil.regexEndOfLine);

        int num = lines[0].split(RegexUtil.regexSpace).length;

        for (int cnt = 1; cnt < lines.length; cnt++) {
            if (lines[cnt].split(RegexUtil.regexSpace).length != num)
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, String.format("Different number of patterns (line %d)", cnt), this.getClass());
        }

        return num;
    }

    private int checkNumFeatures(List<byte[]> train, List<byte[]> validation, List<byte[]> test) throws HttpExceptionDetails {
        int num = checkNumFeatures(train.get(0));
        int cnt;

        for (cnt = 1; cnt < train.size(); cnt++) {
            if (num != checkNumFeatures(train.get(cnt)))
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, String.format("train %d different number of patterns"), this.getClass());
        }


        for (cnt = 0; cnt < validation.size(); cnt++) {
            if (num != checkNumFeatures(validation.get(cnt)))
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, String.format("validation %d different number of patterns"), this.getClass());
        }


        for (cnt = 0; cnt < test.size(); cnt++) {
            if (num != checkNumFeatures(test.get(cnt)))
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, String.format("test %d different number of patterns"), this.getClass());
        }

        return num;
    }

    private Integer getNumClass(byte [] trainOutput, byte [] validationOutput, byte [] testOutput) {
        int cnt = 0;
        Set<String> set = new HashSet<>();
        String[] tmp;

        String tr, va, te;
        tr = new String(trainOutput);
        if (validationOutput == null)
            va = "";
        else
            va = new String(validationOutput);

        if (testOutput == null)
            te = "";
        else
            te = new String(testOutput);


        tmp = tr.split(RegexUtil.regexEndOfLine);
        for (cnt = 0; cnt < tmp.length; cnt++) {
            tmp[cnt] = tmp[cnt].replaceAll(RegexUtil.regexSpace, "").trim();
            if (!tmp[cnt].isEmpty())
                set.add(tmp[cnt]);
        }

        tmp = va.split(RegexUtil.regexEndOfLine);
        for (cnt = 0; cnt < tmp.length; cnt++) {
            tmp[cnt] = tmp[cnt].replaceAll(RegexUtil.regexSpace, "").trim();
            if (!tmp[cnt].isEmpty())
                set.add(tmp[cnt]);
        }

        tmp = te.split(RegexUtil.regexEndOfLine);
        for (cnt = 0; cnt < tmp.length; cnt++) {
            tmp[cnt] = tmp[cnt].replaceAll(RegexUtil.regexSpace, "").trim();
            if (!tmp[cnt].isEmpty())
                set.add(tmp[cnt]);
        }

        return set.size();
    }

    // BAD_REQUEST
    @PreAuthorize(
        value = "authenticated"
    )
    public Dataset createDataset(@NotNull User owner, @NotNull String name, String description, @NotNull AnalyzeType analyzeType,
                                 @NotNull List<byte []> trainInput,      @NotNull List<byte []> trainOutput,
                                 @NotNull List<byte []> validationInput, @NotNull List<byte []> validationOutput,
                                 @NotNull List<byte []> testInput,       @NotNull List<byte []> testOutput)
        throws HttpExceptionDetails {

        if (name == null || name.trim().isEmpty())
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "dataset must be a name", this.getClass());

        checkNumFiles(trainInput, trainOutput, validationInput, validationOutput, testInput, testOutput);

        checkNumInputOutputPatterns(trainInput, trainOutput, validationInput, validationOutput, testInput, testOutput);

        checkNumSetPatterns(trainInput, trainOutput, validationInput, validationOutput, testInput, testOutput);

        int numPatterns = checkAllHaveSamePatterns(trainInput, trainOutput, validationInput, validationOutput, testInput, testOutput);

        int numInputFeatures  = checkNumFeatures(trainInput,  validationInput,  testInput);
        int numOutputFeatures = checkNumFeatures(trainOutput, validationOutput, testOutput);

        Integer numClass = null;
        if (analyzeType.equals(analyzeTypeService.getCLASSIFICATION())) {
            numClass = getNumClass(trainOutput.get(0),
                validationOutput.size() == 0?null:validationOutput.get(0),
                testOutput.size() == 0?null:testOutput.get(0));
        }

        // Now it's the moment to store info
        Dataset dataset = new Dataset(name, description, numPatterns, numOutputFeatures, numInputFeatures, trainInput.size(), owner, analyzeType, numClass);


        // Save dataset
        new CheckPermission(userRepository).checkPermission(datasetMapper.datasetToDatasetDTO(dataset));
        dataset = datasetRepository.save(dataset);
        datasetSearchRepository.save(dataset);

        int cnt = 0;
        DatasetCombination datasetCombination;
        List<DatasetCombination> listDatsetCombination = new ArrayList<>();
        // Save datasetCombinations
        for (cnt = 0; cnt < trainInput.size(); cnt++) {
            datasetCombination = new DatasetCombination();
            datasetCombination.setBelong(dataset);
            datasetCombination.setIndexCombination(cnt);

            datasetCombination.setTrainfilein (trainInput. get(cnt));
            datasetCombination.setTrainfileout(trainOutput.get(cnt));
            datasetCombination.setTrainfileinContentType(TEXTPLAIN);
            datasetCombination.setTrainfileoutContentType(TEXTPLAIN);

            datasetCombination.setValidationfilein (validationInput.size()  != 0? validationInput. get(cnt): null);
            datasetCombination.setValidationfileout(validationOutput.size() != 0?validationOutput.get(cnt): null);
            datasetCombination.setValidationfileinContentType(TEXTPLAIN);
            datasetCombination.setValidationfileoutContentType(TEXTPLAIN);

            datasetCombination.setTestfilein (testInput.size()  != 0? testInput. get(cnt): null);
            datasetCombination.setTestfileout(testOutput.size() != 0?testOutput.get(cnt): null);
            datasetCombination.setTestfileinContentType(TEXTPLAIN);
            datasetCombination.setTestfileoutContentType(TEXTPLAIN);

            listDatsetCombination.add(datasetCombinationRepository.save(datasetCombination));
        }

        return dataset;
    }



    public DatasetDTO updateDataset(CustomDescriptionDTO customDescriptionDTO) throws HttpExceptionDetails {
        Dataset dataset = datasetRepository.findOne(customDescriptionDTO.getId());
        DatasetDTO datasetDTO = null;
        if (dataset != null)
            datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);

        new CheckPermission(userRepository).checkPermission(datasetDTO);

        dataset.setDescription(customDescriptionDTO.getDescription());

        dataset = datasetRepository.save(dataset);
        DatasetDTO result = datasetMapper.datasetToDatasetDTO(dataset);
        datasetSearchRepository.save(dataset);
        return result;
    }
}
