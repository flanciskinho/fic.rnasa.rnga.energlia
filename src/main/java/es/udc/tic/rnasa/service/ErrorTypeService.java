/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.ErrorType;
import es.udc.tic.rnasa.repository.ErrorTypeRepository;
import es.udc.tic.rnasa.repository.search.ErrorTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ErrorTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ErrorTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ErrorType.
 */
@Service
@Transactional
public class ErrorTypeService {

    private final Logger log = LoggerFactory.getLogger(ErrorTypeService.class);

    private static ErrorType MAE = null;
    private static ErrorType MSE = null;


    @Inject
    private ErrorTypeRepository errorTypeRepository;

    @Inject
    private ErrorTypeMapper errorTypeMapper;

    @Inject
    private ErrorTypeSearchRepository errorTypeSearchRepository;

    @Transactional(readOnly = true)
    public ErrorType getMAE() {
        if (MAE == null) {
            Optional<ErrorType> optional = errorTypeRepository.findOneByName("MAE");
            MAE = optional.orElse(null);
        }

        return MAE;
    }

    @Transactional(readOnly = true)
    public ErrorType getMSE() {
        if (MSE == null) {
            Optional<ErrorType> optional = errorTypeRepository.findOneByName("MSE");
            MSE = optional.orElse(null);
        }

        return MSE;
    }

/*
@Transactional(readOnly = true)
    public PatternType getOUTPUT() {
        if (OUTPUT == null) {
            Optional<PatternType> optional = patternTypeRepository.findOneByName("OUTPUT");
            OUTPUT = optional.orElse(null);
        }
        return OUTPUT;
    }
 */

    /**
     * Save a errorType.
     *
     * @param errorTypeDTO the entity to save
     * @return the persisted entity
     */
    public ErrorTypeDTO save(ErrorTypeDTO errorTypeDTO) {
        log.debug("Request to save ErrorType : {}", errorTypeDTO);
        ErrorType errorType = errorTypeMapper.errorTypeDTOToErrorType(errorTypeDTO);
        errorType = errorTypeRepository.save(errorType);
        ErrorTypeDTO result = errorTypeMapper.errorTypeToErrorTypeDTO(errorType);
        errorTypeSearchRepository.save(errorType);
        return result;
    }

    /**
     *  Get all the errorTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ErrorType> findAll(Pageable pageable) {
        log.debug("Request to get all ErrorTypes");
        Page<ErrorType> result = errorTypeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one errorType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ErrorTypeDTO findOne(Long id) {
        log.debug("Request to get ErrorType : {}", id);
        ErrorType errorType = errorTypeRepository.findOne(id);
        ErrorTypeDTO errorTypeDTO = errorTypeMapper.errorTypeToErrorTypeDTO(errorType);
        return errorTypeDTO;
    }

    /**
     *  Delete the  errorType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ErrorType : {}", id);
        errorTypeRepository.delete(id);
        errorTypeSearchRepository.delete(id);
    }

    /**
     * Search for the errorType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ErrorType> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ErrorTypes for query {}", query);
        return errorTypeSearchRepository.search(queryStringQuery(query), pageable);
    }
}
