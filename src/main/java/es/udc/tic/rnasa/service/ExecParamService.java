/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.ExecParam;
import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.repository.ExecParamRepository;
import es.udc.tic.rnasa.repository.search.ExecParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ExecParamDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.SimExecDTO;
import es.udc.tic.rnasa.web.rest.mapper.ExecParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing ExecParam.
 */
@Service
@Transactional
public class ExecParamService {

    private final Logger log = LoggerFactory.getLogger(ExecParamService.class);

    @Inject
    private ExecParamRepository execParamRepository;

    @Inject
    private ExecParamMapper execParamMapper;

    @Inject
    private ExecParamSearchRepository execParamSearchRepository;

    private ExecParam getExecParam(Simulation simulation, String name, String value) {
        ExecParam tmp = new ExecParam();
        tmp.setBelong(simulation);
        tmp.setName(name);
        tmp.setValue(value.substring(0, Math.min(value.length(), 250)));

        return tmp;
    }

    public void save(Simulation simulation, SimExecDTO simExecDTO) {
        List<ExecParam> params = new ArrayList<>();

        params.add(getExecParam(simulation, "arch",   simExecDTO.getArch()));
        params.add(getExecParam(simulation, "proc",   simExecDTO.getProc().toString()));
        params.add(getExecParam(simulation, "time",   simExecDTO.getTime().toString()));
        params.add(getExecParam(simulation, "memory", simExecDTO.getMemory().toString()));

        execParamRepository.save(params);
        execParamSearchRepository.save(params);
    }

    /**
     * Save a execParam.
     *
     * @param execParamDTO the entity to save
     * @return the persisted entity
     */
    public ExecParamDTO save(ExecParamDTO execParamDTO) {
        log.debug("Request to save ExecParam : {}", execParamDTO);
        ExecParam execParam = execParamMapper.execParamDTOToExecParam(execParamDTO);
        execParam = execParamRepository.save(execParam);
        ExecParamDTO result = execParamMapper.execParamToExecParamDTO(execParam);
        execParamSearchRepository.save(execParam);
        return result;
    }

    /**
     *  Get all the execParams.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ExecParam> findAll(Pageable pageable) {
        log.debug("Request to get all ExecParams");
        Page<ExecParam> result = execParamRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one execParam by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ExecParamDTO findOne(Long id) {
        log.debug("Request to get ExecParam : {}", id);
        ExecParam execParam = execParamRepository.findOne(id);
        ExecParamDTO execParamDTO = execParamMapper.execParamToExecParamDTO(execParam);
        return execParamDTO;
    }

    /**
     *  Delete the  execParam by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ExecParam : {}", id);
        execParamRepository.delete(id);
        execParamSearchRepository.delete(id);
    }

    /**
     * Search for the execParam corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ExecParam> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExecParams for query {}", query);
        return execParamSearchRepository.search(queryStringQuery(query), pageable);
    }
}
