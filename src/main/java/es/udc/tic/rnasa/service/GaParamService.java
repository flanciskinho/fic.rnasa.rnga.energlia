/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.Ga;
import es.udc.tic.rnasa.domain.GaParam;
import es.udc.tic.rnasa.repository.GaParamRepository;
import es.udc.tic.rnasa.repository.search.GaParamSearchRepository;
import es.udc.tic.rnasa.type.GeneticAlgorithm.SelectAlgorithm;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.SimGaBasicDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.SimGaDTO;
import es.udc.tic.rnasa.web.rest.dto.GaParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing GaParam.
 */
@Service
@Transactional
public class GaParamService {

    private final Logger log = LoggerFactory.getLogger(GaParamService.class);

    @Inject
    private GaParamRepository gaParamRepository;

    @Inject
    private GaParamMapper gaParamMapper;

    @Inject
    private GaParamSearchRepository gaParamSearchRepository;

    private GaParam getGaParam(Ga ga, String name, String value) {
        GaParam tmp = new GaParam();
        tmp.setBelong(ga);
        tmp.setName(name);
        tmp.setValue(value.substring(0, Math.min(value.length(), 250)));

        return tmp;
    }

    private void save(Ga ga, String fitnessAlgorithm,
                     Short best, Short random, Short select,
                     String algorithm, Short window, Float prob) {
        List<GaParam> params = new ArrayList<>();

        params.add(getGaParam(ga, "fitness.algorithm", fitnessAlgorithm.toString()));
        params.add(getGaParam(ga, "fitness.best.size", best.toString()));
        params.add(getGaParam(ga, "fitness.random.size", random.toString()));
        params.add(getGaParam(ga, "fitness.select.size", select.toString()));

        if (select > 0) {
            params.add(getGaParam(ga, "fitness.select.algorithm", algorithm.toString()));

            if (algorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getName()) || algorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getName())) {
                params.add(getGaParam(ga, "fitness.select.window", window.toString()));
                if (algorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getName()))
                    params.add(getGaParam(ga, "fitness.select.probability", prob.toString()));
            }
        }

        gaParamRepository.save(params);
        gaParamSearchRepository.save(params);
    }

    public void save(Ga ga, SimGaDTO g) {
        if (g.getFunction() > 0) {
            this.save(ga, g.getAlgorithm(),
                g.getBest(), g.getRandom(), g.getFunction(),
                g.getSelect().getAlgorithm(), g.getSelect().getWindow(), g.getSelect().getProbability());
        } else {
            this.save(ga, g.getAlgorithm(),
                g.getBest(), g.getRandom(), g.getFunction(),
                null, null, null);

        }
    }

    public void save(Ga ga, SimGaBasicDTO g) {
        List<GaParam> params = new ArrayList<>();

        params.add(getGaParam(ga, "generation", g.getGeneration().toString()));
        params.add(getGaParam(ga, "goal", g.getGoal().toString()));

        gaParamRepository.save(params);
        gaParamSearchRepository.save(params);
    }



    /**
     * Save a gaParam.
     *
     * @param gaParamDTO the entity to save
     * @return the persisted entity
     */
    public GaParamDTO save(GaParamDTO gaParamDTO) {
        log.debug("Request to save GaParam : {}", gaParamDTO);
        GaParam gaParam = gaParamMapper.gaParamDTOToGaParam(gaParamDTO);
        gaParam = gaParamRepository.save(gaParam);
        GaParamDTO result = gaParamMapper.gaParamToGaParamDTO(gaParam);
        gaParamSearchRepository.save(gaParam);
        return result;
    }

    /**
     *  Get all the gaParams.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GaParam> findAll(Pageable pageable) {
        log.debug("Request to get all GaParams");
        Page<GaParam> result = gaParamRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one gaParam by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public GaParamDTO findOne(Long id) {
        log.debug("Request to get GaParam : {}", id);
        GaParam gaParam = gaParamRepository.findOne(id);
        GaParamDTO gaParamDTO = gaParamMapper.gaParamToGaParamDTO(gaParam);
        return gaParamDTO;
    }

    /**
     *  Delete the  gaParam by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GaParam : {}", id);
        gaParamRepository.delete(id);
        gaParamSearchRepository.delete(id);
    }

    /**
     * Search for the gaParam corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GaParam> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of GaParams for query {}", query);
        return gaParamSearchRepository.search(queryStringQuery(query), pageable);
    }


}
