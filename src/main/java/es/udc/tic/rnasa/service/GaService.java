/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.Ga;
import es.udc.tic.rnasa.repository.GaRepository;
import es.udc.tic.rnasa.repository.search.GaSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.GaDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Ga.
 */
@Service
@Transactional
public class GaService {

    private final Logger log = LoggerFactory.getLogger(GaService.class);
    
    @Inject
    private GaRepository gaRepository;
    
    @Inject
    private GaMapper gaMapper;
    
    @Inject
    private GaSearchRepository gaSearchRepository;
    
    /**
     * Save a ga.
     * 
     * @param gaDTO the entity to save
     * @return the persisted entity
     */
    public GaDTO save(GaDTO gaDTO) {
        log.debug("Request to save Ga : {}", gaDTO);
        Ga ga = gaMapper.gaDTOToGa(gaDTO);
        ga = gaRepository.save(ga);
        GaDTO result = gaMapper.gaToGaDTO(ga);
        gaSearchRepository.save(ga);
        return result;
    }

    /**
     *  Get all the gas.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Ga> findAll(Pageable pageable) {
        log.debug("Request to get all Gas");
        Page<Ga> result = gaRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one ga by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public GaDTO findOne(Long id) {
        log.debug("Request to get Ga : {}", id);
        Ga ga = gaRepository.findOne(id);
        GaDTO gaDTO = gaMapper.gaToGaDTO(ga);
        return gaDTO;
    }

    /**
     *  Delete the  ga by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Ga : {}", id);
        gaRepository.delete(id);
        gaSearchRepository.delete(id);
    }

    /**
     * Search for the ga corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Ga> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Gas for query {}", query);
        return gaSearchRepository.search(queryStringQuery(query), pageable);
    }
}
