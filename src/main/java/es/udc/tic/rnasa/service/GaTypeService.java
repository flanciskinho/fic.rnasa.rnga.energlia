/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.GaType;
import es.udc.tic.rnasa.repository.GaTypeRepository;
import es.udc.tic.rnasa.repository.search.GaTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.GaTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing GaType.
 */
@Service
@Transactional
public class GaTypeService {

    private final Logger log = LoggerFactory.getLogger(GaTypeService.class);

    @Inject
    private GaTypeRepository gaTypeRepository;

    @Inject
    private GaTypeMapper gaTypeMapper;

    @Inject
    private GaTypeSearchRepository gaTypeSearchRepository;

    private static GaType STEADY_STATE = null;
    private static GaType COOPERATIVE_COEVOLUTIVE_STEADY_STATE = null;

    private GaType getEnum(String name) {
        Optional<GaType> optional = gaTypeRepository.findOneByName(name);
        if (!optional.isPresent())
            log.debug("not found '{}' on GaTypeRepository", name);

        return optional.get();
    }

    public GaType getSteadyState() {
        if (STEADY_STATE == null)
            STEADY_STATE = getEnum("STEADY_STATE");

        return STEADY_STATE;
    }

    public GaType getCooperativeCoevolutiveSteadyState() {
        if (COOPERATIVE_COEVOLUTIVE_STEADY_STATE == null)
            COOPERATIVE_COEVOLUTIVE_STEADY_STATE = getEnum("COOPERATIVE_COEVOLUTIVE_STEADY_STATE");

        return COOPERATIVE_COEVOLUTIVE_STEADY_STATE;
    }


    /**
     * Save a gaType.
     *
     * @param gaTypeDTO the entity to save
     * @return the persisted entity
     */
    public GaTypeDTO save(GaTypeDTO gaTypeDTO) {
        log.debug("Request to save GaType : {}", gaTypeDTO);
        GaType gaType = gaTypeMapper.gaTypeDTOToGaType(gaTypeDTO);
        gaType = gaTypeRepository.save(gaType);
        GaTypeDTO result = gaTypeMapper.gaTypeToGaTypeDTO(gaType);
        gaTypeSearchRepository.save(gaType);
        return result;
    }

    /**
     *  Get all the gaTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GaType> findAll(Pageable pageable) {
        log.debug("Request to get all GaTypes");
        Page<GaType> result = gaTypeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one gaType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public GaTypeDTO findOne(Long id) {
        log.debug("Request to get GaType : {}", id);
        GaType gaType = gaTypeRepository.findOne(id);
        GaTypeDTO gaTypeDTO = gaTypeMapper.gaTypeToGaTypeDTO(gaType);
        return gaTypeDTO;
    }

    /**
     *  Delete the  gaType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GaType : {}", id);
        gaTypeRepository.delete(id);
        gaTypeSearchRepository.delete(id);
    }

    /**
     * Search for the gaType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GaType> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of GaTypes for query {}", query);
        return gaTypeSearchRepository.search(queryStringQuery(query), pageable);
    }
}
