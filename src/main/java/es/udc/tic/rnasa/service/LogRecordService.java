/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.LogRecord;
import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.repository.LogRecordRepository;
import es.udc.tic.rnasa.repository.SimulationRepository;
import es.udc.tic.rnasa.repository.SpecificSimulationRepository;
import es.udc.tic.rnasa.repository.TechniqueParamRepository;
import es.udc.tic.rnasa.repository.search.LogRecordSearchRepository;
import es.udc.tic.rnasa.repository.search.TechniqueParamSearchRepository;
import es.udc.tic.rnasa.service.readLogService.ReadLogFactory;
import es.udc.tic.rnasa.web.rest.dto.ForLogRecordDTO.LogRecordAveragePointDTO;
import es.udc.tic.rnasa.web.rest.dto.ForLogRecordDTO.LogRecordPointDTO;
import es.udc.tic.rnasa.web.rest.dto.LogRecordDTO;
import es.udc.tic.rnasa.web.rest.mapper.LogRecordMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import javax.inject.Inject;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing LogRecord.
 */
@Service
@Transactional
public class LogRecordService {

    private final Logger log = LoggerFactory.getLogger(LogRecordService.class);

    @Inject
    private LogRecordRepository logRecordRepository;

    @Inject
    private LogRecordMapper logRecordMapper;

    @Inject
    private LogRecordSearchRepository logRecordSearchRepository;

    @Inject
    private SimulationRepository simulationRepository;

    @Inject
    private SpecificSimulationRepository specificSimulationRepository;

    @Inject
    private TechniqueParamRepository techniqueParamRepository;
    @Inject
    private TechniqueParamSearchRepository techniqueParamSearchRepository;

    /**
     * Save a logRecord.
     *
     * @param logRecordDTO the entity to save
     * @return the persisted entity
     */
    public LogRecordDTO save(LogRecordDTO logRecordDTO) {
        log.debug("Request to save LogRecord : {}", logRecordDTO);
        LogRecord logRecord = logRecordMapper.logRecordDTOToLogRecord(logRecordDTO);
        logRecord = logRecordRepository.save(logRecord);
        LogRecordDTO result = logRecordMapper.logRecordToLogRecordDTO(logRecord);
        logRecordSearchRepository.save(logRecord);
        return result;
    }

    /**
     *  Get all the logRecords.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LogRecord> findAll(Pageable pageable) {
        log.debug("Request to get all LogRecords");
        Page<LogRecord> result = logRecordRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one logRecord by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public LogRecordDTO findOne(Long id) {
        log.debug("Request to get LogRecord : {}", id);
        LogRecord logRecord = logRecordRepository.findOne(id);
        LogRecordDTO logRecordDTO = logRecordMapper.logRecordToLogRecordDTO(logRecord);
        return logRecordDTO;
    }

    /**
     *  Delete the  logRecord by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LogRecord : {}", id);
        logRecordRepository.delete(id);
        logRecordSearchRepository.delete(id);
    }

    /**
     * Search for the logRecord corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LogRecord> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of LogRecords for query {}", query);
        return logRecordSearchRepository.search(queryStringQuery(query), pageable);
    }

    public void saveLogs(SpecificSimulation specificSimulation, Document document) {
        log.debug("Request to save logs");

        ReadLogFactory log = new ReadLogFactory(document, specificSimulation);

        logRecordRepository.save(log.getReadLog().getLogRecords());
        logRecordSearchRepository.save(log.getReadLog().getLogRecords());

        techniqueParamRepository.save(log.getReadLog().getTechniqueParams());
        techniqueParamSearchRepository.save(log.getReadLog().getTechniqueParams());

        specificSimulation.setLogVersion(log.getLogVersion());
        specificSimulation.setNodeVersion(log.getNodeVersion());
    }

    public List<LogRecordPointDTO> getLogsAverage(Simulation simulation, int steps) {
        return getLogsAverage(simulation.getId(), steps);
    }

    public List<LogRecordPointDTO> getLogsAverage(Long simulationId, int steps) {

        Map<Long, List<LogRecord>> records = new HashMap<>();

        List<SpecificSimulation> specificSimulations = specificSimulationRepository.findAllByBelongId(simulationId);

        specificSimulations.stream().forEach(ss -> records.put(ss.getId(), logRecordRepository.findAllByBelongIdOrderByIndexRecord(ss.getId())));


        // Get Max value
        Long maxRecord = 0l, tmp;
        for (Map.Entry<Long, List<LogRecord>> entry: records.entrySet()) {
            tmp = entry.getValue().stream().mapToLong(LogRecord::getIndexRecord).max().orElse(0);
            if (tmp > maxRecord)
                maxRecord = tmp;
        }

        long point, sizeStep = maxRecord/(steps-1);
        int cnt;

        Map<Long, List<LogRecordAveragePointDTO>> aux = new HashMap();
        List<LogRecordAveragePointDTO> list;
        for (Map.Entry<Long, List<LogRecord>> entry: records.entrySet()) {
            for (cnt = 0; cnt < steps; cnt++) {
                point = cnt*sizeStep;
                if (!aux.containsKey(point)) {
                    aux.put(point, new ArrayList<>());
                }

                list = aux.get(point);
                list.add(interpolate(point, entry.getValue()));
                aux.put(point, list);
            }
        }


        List<LogRecordPointDTO> rtn = getGlobalAverage(aux);
        rtn.sort( (o1, o2) -> o1.getIndexRecord().compareTo(o2.getIndexRecord()) );
        return rtn;
    }

    private List<LogRecordPointDTO> getGlobalAverage(Map<Long, List<LogRecordAveragePointDTO>> map) {
        long tmpTimestamp;
        long desTimestamp;
        Double tmpErrorTrain, tmpErrorValidation, tmpErrorTest;
        Double desTrain, desValidation, desTest;
        int cnt;
        List<LogRecordAveragePointDTO> list;

        List<LogRecordPointDTO> interpolates = new ArrayList();

        for (Map.Entry<Long, List<LogRecordAveragePointDTO>> entry: map.entrySet()) {
            desTimestamp = tmpTimestamp = 0;
            desTest = tmpErrorTest = 0.0;
            desValidation = tmpErrorValidation = 0.0;
            desTrain = tmpErrorTrain = 0.0;

            list = entry.getValue();
            for (cnt = 0; cnt < list.size(); cnt++) {
                tmpTimestamp += list.get(cnt).getTimestamp();

                if (list.get(cnt).getErrorTrain() != null) {
                    tmpErrorTrain += list.get(cnt).getErrorTrain();
                } else
                    tmpErrorTrain = null;

                if (list.get(cnt).getErrorValidation() != null) {
                    tmpErrorValidation += list.get(cnt).getErrorValidation();
                } else
                    tmpErrorValidation = null;

                if (list.get(cnt).getErrorTest() != null) {
                    tmpErrorTest += list.get(cnt).getErrorTest();
                } else
                    tmpErrorTest = null;

            }
//Average
            tmpTimestamp = tmpTimestamp / list.size();
            if (tmpErrorTrain != null)
                tmpErrorTrain = tmpErrorTrain / list.size();
            if (tmpErrorValidation != null)
                tmpErrorValidation = tmpErrorValidation / list.size();
            if (tmpErrorTest != null)
                tmpErrorTest = tmpErrorTest / list.size();

            for (cnt = 0; cnt < list.size(); cnt++) {
                desTimestamp += Math.abs(list.get(cnt).getTimestamp() - tmpTimestamp);

                if (list.get(cnt).getErrorTrain() != null) {
                    desTrain += Math.abs(list.get(cnt).getErrorTrain() -tmpErrorTrain);
                } else
                    desTrain = null;

                if (list.get(cnt).getErrorValidation() != null) {
                    desValidation += Math.abs(list.get(cnt).getErrorValidation());
                } else
                    desValidation = null;

                if (list.get(cnt).getErrorTest() != null) {
                    desTest += Math.pow(list.get(cnt).getErrorTest(), 2);
                } else
                    desTest = null;

            }

            desTimestamp = desTimestamp / list.size();
            if (desTrain != null)
                desTrain = desTrain / list.size();
            if (desValidation != null)
                desValidation = desValidation / list.size();
            if (tmpErrorTest != null)
                desTest = desTest / list.size();

            interpolates.add(new LogRecordPointDTO(list.get(0).getIndexRecord(), tmpTimestamp, tmpErrorTrain, tmpErrorValidation, tmpErrorTest, desTimestamp, desTrain, desValidation, desTest));
        }

        return interpolates;
    }

    private LogRecordAveragePointDTO interpolate(long point, List<LogRecord> records) {
        Optional<LogRecord> opLogRecord =  records.stream().filter(lr -> lr.getIndexRecord() == point).findFirst();

        // Not interpolate
        if (opLogRecord.isPresent()) {
            LogRecord aux = opLogRecord.get();
            return new LogRecordAveragePointDTO(point, aux.getTimestamp(), aux.getErrorTrain(), aux.getErrorValidation(), aux.getErrorTest());
        }

        // Need interpolate
        LogRecord p1, p2;
        int cnt, index1 = 0;

        for (cnt = 0; cnt < records.size(); cnt++) {
            if (records.get(cnt).getIndexRecord() < point)
                index1 = cnt;
            else
                break;
        }

        p1 = records.get(index1);
        // Don't have the value after the point
        if (index1 == records.size()-1) {
            return new LogRecordAveragePointDTO(point, p1.getTimestamp(), p1.getErrorTrain(), p1.getErrorValidation(), p1.getErrorTest());
        }

        p2 = records.get(index1+1);


        long pointX1 = point-p1.getIndexRecord();
        long x2x1 = p2.getIndexRecord()-p1.getIndexRecord();


        return new LogRecordAveragePointDTO(point,
            linealInterpolateLong(pointX1, x2x1, p1.getTimestamp(), p2.getTimestamp()),
            linealInterpolateDouble(pointX1, x2x1, p1.getErrorTrain(), p2.getErrorTrain()),
            linealInterpolateDouble(pointX1, x2x1, p1.getErrorTrain(), p2.getErrorValidation()),
            linealInterpolateDouble(pointX1, x2x1, p1.getErrorTrain(), p2.getErrorTest())
        );

    }

    private Long linealInterpolateLong(long pointX1, long x2x1, long y1, long y2) {
        return y1 + pointX1 * ( (y2-y1)/ x2x1 );
    }

    private Double linealInterpolateDouble(long pointX1, long x2x1, Double y1, Double y2) {
        if (y1 == null || y2 == null)
            return null;

        return y1 + pointX1 * ( (y2-y1)/ (x2x1) );
    }
}
