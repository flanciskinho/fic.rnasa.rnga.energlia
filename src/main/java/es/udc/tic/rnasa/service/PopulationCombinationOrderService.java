/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.PopulationCombinationOrder;
import es.udc.tic.rnasa.repository.PopulationCombinationOrderRepository;
import es.udc.tic.rnasa.repository.search.PopulationCombinationOrderSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.PopulationCombinationOrderDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationCombinationOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PopulationCombinationOrder.
 */
@Service
@Transactional
public class PopulationCombinationOrderService {

    private final Logger log = LoggerFactory.getLogger(PopulationCombinationOrderService.class);
    
    @Inject
    private PopulationCombinationOrderRepository populationCombinationOrderRepository;
    
    @Inject
    private PopulationCombinationOrderMapper populationCombinationOrderMapper;
    
    @Inject
    private PopulationCombinationOrderSearchRepository populationCombinationOrderSearchRepository;
    
    /**
     * Save a populationCombinationOrder.
     * 
     * @param populationCombinationOrderDTO the entity to save
     * @return the persisted entity
     */
    public PopulationCombinationOrderDTO save(PopulationCombinationOrderDTO populationCombinationOrderDTO) {
        log.debug("Request to save PopulationCombinationOrder : {}", populationCombinationOrderDTO);
        PopulationCombinationOrder populationCombinationOrder = populationCombinationOrderMapper.populationCombinationOrderDTOToPopulationCombinationOrder(populationCombinationOrderDTO);
        populationCombinationOrder = populationCombinationOrderRepository.save(populationCombinationOrder);
        PopulationCombinationOrderDTO result = populationCombinationOrderMapper.populationCombinationOrderToPopulationCombinationOrderDTO(populationCombinationOrder);
        populationCombinationOrderSearchRepository.save(populationCombinationOrder);
        return result;
    }

    /**
     *  Get all the populationCombinationOrders.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PopulationCombinationOrder> findAll(Pageable pageable) {
        log.debug("Request to get all PopulationCombinationOrders");
        Page<PopulationCombinationOrder> result = populationCombinationOrderRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one populationCombinationOrder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PopulationCombinationOrderDTO findOne(Long id) {
        log.debug("Request to get PopulationCombinationOrder : {}", id);
        PopulationCombinationOrder populationCombinationOrder = populationCombinationOrderRepository.findOne(id);
        PopulationCombinationOrderDTO populationCombinationOrderDTO = populationCombinationOrderMapper.populationCombinationOrderToPopulationCombinationOrderDTO(populationCombinationOrder);
        return populationCombinationOrderDTO;
    }

    /**
     *  Delete the  populationCombinationOrder by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PopulationCombinationOrder : {}", id);
        populationCombinationOrderRepository.delete(id);
        populationCombinationOrderSearchRepository.delete(id);
    }

    /**
     * Search for the populationCombinationOrder corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PopulationCombinationOrder> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PopulationCombinationOrders for query {}", query);
        return populationCombinationOrderSearchRepository.search(queryStringQuery(query), pageable);
    }
}
