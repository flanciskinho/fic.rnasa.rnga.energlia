/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.Population;
import es.udc.tic.rnasa.domain.PopulationCombination;
import es.udc.tic.rnasa.repository.PopulationCombinationRepository;
import es.udc.tic.rnasa.repository.search.PopulationCombinationSearchRepository;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotTransferConnectionException;
import es.udc.tic.rnasa.service.exceptions.NoDataException;
import es.udc.tic.rnasa.web.rest.dto.PopulationCombinationDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationCombinationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing PopulationCombination.
 */
@Service
@Transactional
public class PopulationCombinationService {

    private final Logger log = LoggerFactory.getLogger(PopulationCombinationService.class);

    private final static String POPULATION_PATHNAME = ".energlia/population/%d";

    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @Inject
    private PopulationCombinationMapper populationCombinationMapper;

    @Inject
    private PopulationCombinationSearchRepository populationCombinationSearchRepository;

    @Inject
    private SshAccountService sshAccountService;

    public String getPopulationPathname() {
        return POPULATION_PATHNAME;
    }

    /**
     * Save a populationCombination.
     *
     * @param populationCombinationDTO the entity to save
     * @return the persisted entity
     */
    public PopulationCombinationDTO save(PopulationCombinationDTO populationCombinationDTO) {
        log.debug("Request to save PopulationCombination : {}", populationCombinationDTO);
        PopulationCombination populationCombination = populationCombinationMapper.populationCombinationDTOToPopulationCombination(populationCombinationDTO);
        populationCombination = populationCombinationRepository.save(populationCombination);
        PopulationCombinationDTO result = populationCombinationMapper.populationCombinationToPopulationCombinationDTO(populationCombination);
        populationCombinationSearchRepository.save(populationCombination);
        return result;
    }

    /**
     *  Get all the populationCombinations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PopulationCombination> findAll(Pageable pageable) {
        log.debug("Request to get all PopulationCombinations");
        Page<PopulationCombination> result = populationCombinationRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one populationCombination by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PopulationCombinationDTO findOne(Long id) {
        log.debug("Request to get PopulationCombination : {}", id);
        PopulationCombination populationCombination = populationCombinationRepository.findOne(id);
        PopulationCombinationDTO populationCombinationDTO = populationCombinationMapper.populationCombinationToPopulationCombinationDTO(populationCombination);
        return populationCombinationDTO;
    }

    /**
     *  Delete the  populationCombination by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PopulationCombination : {}", id);
        populationCombinationRepository.delete(id);
        populationCombinationSearchRepository.delete(id);
    }

    /**
     * Search for the populationCombination corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PopulationCombination> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PopulationCombinations for query {}", query);
        return populationCombinationSearchRepository.search(queryStringQuery(query), pageable);
    }

    private void sendData(PopulationCombination pc, ExecConnectionServer exec, TransferConnectionServer transfer) throws CannotExecConnectionException, CannotTransferConnectionException {
        String pathname = String.format("%s/%d.xml", String.format(POPULATION_PATHNAME, pc.getBelong().getId()), pc.getId());

        if (!sshAccountService.exists(exec, pathname)) {
            transfer.sendData(new String(pc.getFile()), pathname);
        }
    }

/*
    @Transactional(readOnly = true)
    public void sendData(Population population, SshAccount sshAccount) throws NoDataException, TooMuchConnectionException, CannotOpenConnectionException, CannotExecConnectionException, CannotTransferConnectionException {
        List<PopulationCombination> populationCombinationList = populationCombinationRepository.findAllByBelongId(population.getId());
        if (populationCombinationList.size() == 0)
            throw new NoDataException(String.format("population with out combinations (id: %d, name: %s)", population.getId(), population.getName()), this.getClass());


        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;
        try {
            exec     = (ExecConnectionServer)     sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);
            transfer = (TransferConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.SFTP);

            if (!sshAccountService.exists(exec, String.format(POPULATION_PATHNAME, population.getId())))
                sshAccountService.launchCommand(exec, String.format("mkdir -p %s", String.format(POPULATION_PATHNAME, population.getId())));

            for (PopulationCombination pc: populationCombinationList)
                sendData(pc, exec, transfer);
        } catch (CannotTransferConnectionException | TooMuchConnectionException | CannotOpenConnectionException | CannotExecConnectionException e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
            if (transfer != null)
                sshAccountService.closeConnection(transfer);

            throw e;
        }

    }
*/

    public void sendData(Population population, ExecConnectionServer exec, TransferConnectionServer transfer) throws NoDataException, CannotExecConnectionException, CannotTransferConnectionException {
        List<PopulationCombination> populationCombinationList = populationCombinationRepository.findAllByBelongId(population.getId());
        if (populationCombinationList.size() == 0)
            throw new NoDataException(String.format("population with out combinations (id: %d, name: %s)", population.getId(), population.getName()), this.getClass());

        if (!sshAccountService.exists(exec, String.format(POPULATION_PATHNAME, population.getId())))
            sshAccountService.launchCommand(exec, String.format("mkdir -p %s", String.format(POPULATION_PATHNAME, population.getId())));

        for (PopulationCombination pc: populationCombinationList)
            sendData(pc, exec, transfer);
    }
}
