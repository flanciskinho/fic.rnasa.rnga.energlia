/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.PopulationOrder;
import es.udc.tic.rnasa.repository.PopulationOrderRepository;
import es.udc.tic.rnasa.repository.search.PopulationOrderSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.PopulationOrderDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PopulationOrder.
 */
@Service
@Transactional
public class PopulationOrderService {

    private final Logger log = LoggerFactory.getLogger(PopulationOrderService.class);
    
    @Inject
    private PopulationOrderRepository populationOrderRepository;
    
    @Inject
    private PopulationOrderMapper populationOrderMapper;
    
    @Inject
    private PopulationOrderSearchRepository populationOrderSearchRepository;
    
    /**
     * Save a populationOrder.
     * 
     * @param populationOrderDTO the entity to save
     * @return the persisted entity
     */
    public PopulationOrderDTO save(PopulationOrderDTO populationOrderDTO) {
        log.debug("Request to save PopulationOrder : {}", populationOrderDTO);
        PopulationOrder populationOrder = populationOrderMapper.populationOrderDTOToPopulationOrder(populationOrderDTO);
        populationOrder = populationOrderRepository.save(populationOrder);
        PopulationOrderDTO result = populationOrderMapper.populationOrderToPopulationOrderDTO(populationOrder);
        populationOrderSearchRepository.save(populationOrder);
        return result;
    }

    /**
     *  Get all the populationOrders.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PopulationOrder> findAll(Pageable pageable) {
        log.debug("Request to get all PopulationOrders");
        Page<PopulationOrder> result = populationOrderRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one populationOrder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PopulationOrderDTO findOne(Long id) {
        log.debug("Request to get PopulationOrder : {}", id);
        PopulationOrder populationOrder = populationOrderRepository.findOne(id);
        PopulationOrderDTO populationOrderDTO = populationOrderMapper.populationOrderToPopulationOrderDTO(populationOrder);
        return populationOrderDTO;
    }

    /**
     *  Delete the  populationOrder by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PopulationOrder : {}", id);
        populationOrderRepository.delete(id);
        populationOrderSearchRepository.delete(id);
    }

    /**
     * Search for the populationOrder corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PopulationOrder> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PopulationOrders for query {}", query);
        return populationOrderSearchRepository.search(queryStringQuery(query), pageable);
    }
}
