/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.PopulationOrder;
import es.udc.tic.rnasa.domain.PopulationParam;
import es.udc.tic.rnasa.repository.PopulationParamRepository;
import es.udc.tic.rnasa.repository.search.PopulationParamSearchRepository;
import es.udc.tic.rnasa.type.GeneticAlgorithm.CrossoverAlgorithm;
import es.udc.tic.rnasa.type.GeneticAlgorithm.SelectAlgorithm;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.SimPopulationDTO;
import es.udc.tic.rnasa.web.rest.dto.PopulationParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing PopulationParam.
 */
@Service
@Transactional
public class PopulationParamService {

    private final Logger log = LoggerFactory.getLogger(PopulationParamService.class);

    @Inject
    private PopulationParamRepository populationParamRepository;

    @Inject
    private PopulationParamMapper populationParamMapper;

    @Inject
    private PopulationParamSearchRepository populationParamSearchRepository;


    private PopulationParam getPopulationParam(PopulationOrder po, String name, String value) {
        PopulationParam tmp = new PopulationParam();
        tmp.setBelong(po);
        tmp.setName(name);
        tmp.setValue(value.substring(0, Math.min(value.length(), 250)));

        return tmp;
    }

    public void save(PopulationOrder populationOrder, SimPopulationDTO p) {
        this.save(populationOrder,
            p.getMother().getAlgorithm(), p.getMother().getWindow(), p.getMother().getProbability(),
            p.getFather().getAlgorithm(), p.getFather().getWindow(), p.getFather().getProbability(),
            p.getMutation().getAlgorithm(), p.getMutation().getRate(),
            p.getCrossover().getAlgorithm(), p.getCrossover().getRate(), p.getCrossover().getMu(),
            p.getReplace().getAlgorithm());
    }

    private void save(PopulationOrder populationOrder,
                     String motherAlgorithm, Short motherWindow, Float motherProb,
                     String fatherAlgorithm, Short fatherWindow, Float fatherProb,
                     String mutationAlgorithm, Float mutationRate,
                     String crossoverAlgorithm, Float crossoverRate, Float crossoverMu,
                     String replaceAlgorithm) {

        List<PopulationParam> params = new ArrayList<>();

        // mother
        params.add(getPopulationParam(populationOrder, "mother.algorithm", motherAlgorithm.toString()));
        if (motherAlgorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getName()) || motherAlgorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getName())) {
            params.add(getPopulationParam(populationOrder, "mother.window", motherWindow.toString()));
            if (motherAlgorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getName()))
                params.add(getPopulationParam(populationOrder, "mother.probability", motherProb.toString()));
        }
        // father
        params.add(getPopulationParam(populationOrder, "father.algorithm", fatherAlgorithm.toString()));
        if (fatherAlgorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getName()) || fatherAlgorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getName())) {
            params.add(getPopulationParam(populationOrder, "father.window", fatherWindow.toString()));
            if (fatherAlgorithm.equals(SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getName()))
                params.add(getPopulationParam(populationOrder, "father.probability", fatherProb.toString()));
        }

        // mutation
        params.add(getPopulationParam(populationOrder, "mutation.algorithm", mutationAlgorithm.toString()));
        params.add(getPopulationParam(populationOrder, "mutation.rate", mutationRate.toString()));

        // crossover
        params.add(getPopulationParam(populationOrder, "crossover.algorithm", crossoverAlgorithm.toString()));
        params.add(getPopulationParam(populationOrder, "crossover.rate", crossoverRate.toString()));
        if (crossoverAlgorithm.equals(CrossoverAlgorithm.REAL_SBX.getName()))
            params.add(getPopulationParam(populationOrder, "crossover.mu", crossoverMu.toString()));

        // replace
        params.add(getPopulationParam(populationOrder, "replace.algorithm", replaceAlgorithm.toString()));

        populationParamRepository.save(params);
        populationParamSearchRepository.save(params);
    }

    /**
     * Save a populationParam.
     *
     * @param populationParamDTO the entity to save
     * @return the persisted entity
     */
    public PopulationParamDTO save(PopulationParamDTO populationParamDTO) {
        log.debug("Request to save PopulationParam : {}", populationParamDTO);
        PopulationParam populationParam = populationParamMapper.populationParamDTOToPopulationParam(populationParamDTO);
        populationParam = populationParamRepository.save(populationParam);
        PopulationParamDTO result = populationParamMapper.populationParamToPopulationParamDTO(populationParam);
        populationParamSearchRepository.save(populationParam);
        return result;
    }

    /**
     *  Get all the populationParams.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PopulationParam> findAll(Pageable pageable) {
        log.debug("Request to get all PopulationParams");
        Page<PopulationParam> result = populationParamRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one populationParam by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PopulationParamDTO findOne(Long id) {
        log.debug("Request to get PopulationParam : {}", id);
        PopulationParam populationParam = populationParamRepository.findOne(id);
        PopulationParamDTO populationParamDTO = populationParamMapper.populationParamToPopulationParamDTO(populationParam);
        return populationParamDTO;
    }

    /**
     *  Delete the  populationParam by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PopulationParam : {}", id);
        populationParamRepository.delete(id);
        populationParamSearchRepository.delete(id);
    }

    /**
     * Search for the populationParam corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PopulationParam> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PopulationParams for query {}", query);
        return populationParamSearchRepository.search(queryStringQuery(query), pageable);
    }

}
