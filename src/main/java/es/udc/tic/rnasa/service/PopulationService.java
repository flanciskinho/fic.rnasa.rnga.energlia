/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.Population;
import es.udc.tic.rnasa.domain.PopulationCombination;
import es.udc.tic.rnasa.domain.SizeParam;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.PopulationCombinationRepository;
import es.udc.tic.rnasa.repository.PopulationRepository;
import es.udc.tic.rnasa.repository.SizeParamRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.repository.search.PopulationSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.security.SecurityUtils;
import es.udc.tic.rnasa.service.exceptions.DifferentEncodesException;
import es.udc.tic.rnasa.service.exceptions.DifferentVersionsException;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.service.exceptions.NoXmlAttributeException;
import es.udc.tic.rnasa.service.readIndividualService.ReadIndividual;
import es.udc.tic.rnasa.service.readIndividualService.ReadIndividualFactory;
import es.udc.tic.rnasa.service.readPopulationService.ReadPopulation;
import es.udc.tic.rnasa.service.readPopulationService.ReadPopulationFactory;
import es.udc.tic.rnasa.service.util.CheckPermission;
import es.udc.tic.rnasa.web.rest.dto.CustomDescriptionDTO;
import es.udc.tic.rnasa.web.rest.dto.PopulationDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Population.
 */
@Service
@Transactional
public class PopulationService {

    private final Logger log = LoggerFactory.getLogger(PopulationService.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private PopulationRepository populationRepository;

    @Inject
    private SizeParamRepository sizeParamRepository;

    @Inject
    private SizeTypeService sizeTypeService;

    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @Inject
    private PopulationMapper populationMapper;

    @Inject
    private PopulationSearchRepository populationSearchRepository;

    private static final String TEXTXML = "text/xml";

    public PopulationDTO share(Long populationId, String login) throws HttpExceptionDetails{
        log.debug("Request to share Population: {}", populationId);

        Population population = populationRepository.findOne(populationId);
        if (population == null)
            throw new HttpExceptionDetails(HttpStatus.NOT_FOUND, "Population not found");

        Optional<User> userOptional = userRepository.findOneByLogin(login);
        if (!userOptional.isPresent())
            throw new HttpExceptionDetails(HttpStatus.NOT_FOUND, "User not found");

        User user = userOptional.get();

        Set<User> shares = population.getShares();

        if (shares.contains(user))
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "User already has access");

        shares.add(user);
        population.setShares(shares);

        populationRepository.save(population);

        return populationMapper.populationToPopulationDTO(population);
    }

    /**
     * Save a population.
     *
     * @param populationDTO the entity to save
     * @return the persisted entity
     */
    public PopulationDTO save(PopulationDTO populationDTO) throws HttpExceptionDetails {
        log.debug("Request to save Population : {}", populationDTO);

        new CheckPermission(userRepository).checkPermission(populationDTO);

        Population population = populationMapper.populationDTOToPopulation(populationDTO);
        population = populationRepository.save(population);
        PopulationDTO result = populationMapper.populationToPopulationDTO(population);
        populationSearchRepository.save(population);
        return result;
    }

    /**
     *  Get all the populations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Population> findAll(Pageable pageable) throws HttpExceptionDetails {
        log.debug("Request to get all Populations");

        Page<Population> result;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            result = populationRepository.findAll(pageable);
        } else {
            Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
            if (!user.isPresent())
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "invalid user");
            result = populationRepository.findAllByUserLogin(pageable, user.get());
        }

        return result;
    }

    /**
     *  Get one population by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PopulationDTO findOne(Long id) throws HttpExceptionDetails {
        log.debug("Request to get Population : {}", id);
        Population population = populationRepository.findOne(id);
        PopulationDTO populationDTO = populationMapper.populationToPopulationDTO(population);

        Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());

        if (user.isPresent() && population != null) {
            if (population.getOwner().equals(user.get()))
                return populationDTO;
            if (population.getShares().contains(user.get()))
                return populationDTO;
        }

        new CheckPermission(userRepository).checkPermission(populationDTO);

        return populationDTO;
    }

    /**
     *  Delete the  population by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) throws HttpExceptionDetails {
        log.debug("Request to delete Population : {}", id);

        Population population = populationRepository.findOne(id);
        if (population == null)
            throw new HttpExceptionDetails(HttpStatus.NOT_FOUND, "not found", this.getClass());

        PopulationDTO populationDTO = populationMapper.populationToPopulationDTO(population);

        new CheckPermission(userRepository).checkPermission(populationDTO);

        populationRepository.delete(id);
        populationSearchRepository.delete(id);
    }

    /**
     * Search for the population corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Population> search(String query, Pageable pageable) throws HttpExceptionDetails {
        log.debug("Request to search for a page of Populations for query {}", query);

        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))
            throw new HttpExceptionDetails(HttpStatus.NOT_IMPLEMENTED, "no implemented", this.getClass());

        return populationSearchRepository.search(queryStringQuery(query), pageable);
    }


    ////
    //// custom services below this point
    ////



    @PreAuthorize(value = "authenticated")
    public Population createPopulation(@NotNull User owner, @NotNull String name, String description,
                                       @NotNull List<byte []> files) throws HttpExceptionDetails, DifferentEncodesException, DifferentVersionsException, NoXmlAttributeException {

        if (name == null || name.trim().isEmpty())
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "dataset must be a name", this.getClass());

        ReadPopulationFactory readPopulationFactory = null;
        ReadPopulation readPopulation = null;
log.info("(createPopulation) Locale {}", Locale.getDefault());
        try {
            readPopulationFactory = new ReadPopulationFactory(files);
            readPopulation = readPopulationFactory.getReadPopulation();
        } catch (Exception e) {
            log.debug("(createPopulation) msg = {}",e.getMessage());
            log.debug(e.getClass().getName());
            if (e.getCause() != null)
                log.debug(e.getCause().getMessage());
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "bad format", this.getClass());
        }



        Population population = new Population(name, description, readPopulationFactory.getNumIndividuals(),
            readPopulationFactory.getNumCombination(), readPopulation.getSizeGenotype(),
            owner);

        new CheckPermission(userRepository).checkPermission(populationMapper.populationToPopulationDTO(population));

        // Save Population
        population = populationRepository.save(population);
        populationSearchRepository.save(population);

        // Save PopulationCombination
        PopulationCombination populationCombination;
        ReadIndividualFactory readIndividualFactory = null;
        ReadIndividual readIndividual;

        for (int cnt = 0; cnt < files.size(); cnt++) {
//log.debug(">>>To save combination= '{}'\n\nfile={}\n\n\ncontent-type= {}\npopulation = {}\n\n", cnt, new String(files.get(cnt)), TEXTXML, population );
            populationCombination = new PopulationCombination(cnt, files.get(cnt), TEXTXML, population);
            populationCombinationRepository.save(populationCombination);

            // Save individuals
            readIndividualFactory = new ReadIndividualFactory(readPopulationFactory.getXmlFiles().get(cnt));
            readIndividual = readIndividualFactory.getReadIndividual();


        }

        readIndividual= new ReadIndividualFactory(readPopulationFactory.getXmlFiles().get(0)).getReadIndividual();
        if (readIndividual.getGenoBinary().size() != 0)
            if (readIndividual.getGenoBinary().size()*readIndividual.getGenoBinary().get(0).size() != 0)
                sizeParamRepository.save(new SizeParam(
                    readIndividual.getGenoBinary().get(0).size(),
                    population, sizeTypeService.getBINARY()));
        if (readIndividual.getGenoShort().size() != 0)
            if (readIndividual.getGenoShort().size()*readIndividual.getGenoShort().get(0).size() != 0)
                sizeParamRepository.save(new SizeParam(
                    readIndividual.getGenoShort().get(0).size(),
                    population, sizeTypeService.getSHORT()));
        if (readIndividual.getGenoFloat().size() != 0)
            if (readIndividual.getGenoFloat().size()*readIndividual.getGenoFloat().get(0).size() != 0)
                sizeParamRepository.save(new SizeParam(
                    readIndividual.getGenoFloat().get(0).size(),
                    population, sizeTypeService.getREAL()));


        return population;

    }


    public PopulationDTO update(CustomDescriptionDTO customDescriptionDTO) throws HttpExceptionDetails {
        Population population = populationRepository.findOne(customDescriptionDTO.getId());
        PopulationDTO populationDTO = null;

        if (population != null)
            populationDTO = populationMapper.populationToPopulationDTO(population);

        new CheckPermission(userRepository).checkPermission(populationDTO);

        population.setDescription(customDescriptionDTO.getDescription());

        population = populationRepository.save(population);
        PopulationDTO result = populationMapper.populationToPopulationDTO(population);
        populationSearchRepository.save(population);
        return result;
    }


    @Transactional(readOnly = true)
    public Page<Population> findAllBySize(Pageable pageable, Integer size_short, Integer size_float, Integer size_bool) throws HttpExceptionDetails {
        log.debug("Request to get all Populations");

        Page<Population> result;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            result = populationRepository.findAllBySize(pageable,
                size_short, sizeTypeService.getSHORT(),
                size_float, sizeTypeService.getREAL(),
                size_bool , sizeTypeService.getBINARY());
        } else {
            Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
            if (!user.isPresent())
                throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "invalid user");

            result = populationRepository.findAllBySizeAndUserLogin(pageable,
                size_short, sizeTypeService.getSHORT(),
                size_float, sizeTypeService.getREAL(),
                size_bool , sizeTypeService.getBINARY(),
                user.get()
            );

        }

        return result;
    }
}
