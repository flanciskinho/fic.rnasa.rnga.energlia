/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.ProblemType;
import es.udc.tic.rnasa.repository.ProblemTypeRepository;
import es.udc.tic.rnasa.repository.search.ProblemTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ProblemTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ProblemTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing ProblemType.
 */
@Service
@Transactional
public class ProblemTypeService {

    private final Logger log = LoggerFactory.getLogger(ProblemTypeService.class);

    @Inject
    private ProblemTypeRepository problemTypeRepository;

    @Inject
    private ProblemTypeMapper problemTypeMapper;

    @Inject
    private ProblemTypeSearchRepository problemTypeSearchRepository;

    /**
     * Save a problemType.
     *
     * @param problemTypeDTO the entity to save
     * @return the persisted entity
     */
    public ProblemTypeDTO save(ProblemTypeDTO problemTypeDTO) {
        log.debug("Request to save ProblemType : {}", problemTypeDTO);
        ProblemType problemType = problemTypeMapper.problemTypeDTOToProblemType(problemTypeDTO);
        problemType = problemTypeRepository.save(problemType);
        ProblemTypeDTO result = problemTypeMapper.problemTypeToProblemTypeDTO(problemType);
        problemTypeSearchRepository.save(problemType);
        return result;
    }

    /**
     *  Get all the problemTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProblemType> findAll(Pageable pageable) {
        log.debug("Request to get all ProblemTypes");
        Page<ProblemType> result = problemTypeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one problemType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProblemTypeDTO findOne(Long id) {
        log.debug("Request to get ProblemType : {}", id);
        ProblemType problemType = problemTypeRepository.findOneWithEagerRelationships(id);
        ProblemTypeDTO problemTypeDTO = problemTypeMapper.problemTypeToProblemTypeDTO(problemType);
        return problemTypeDTO;
    }

    /**
     *  Delete the  problemType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ProblemType : {}", id);
        problemTypeRepository.delete(id);
        problemTypeSearchRepository.delete(id);
    }

    /**
     * Search for the problemType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProblemType> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ProblemTypes for query {}", query);
        return problemTypeSearchRepository.search(queryStringQuery(query), pageable);
    }
}
