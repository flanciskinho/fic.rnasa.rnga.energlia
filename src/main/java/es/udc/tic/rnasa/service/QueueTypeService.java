/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.QueueType;
import es.udc.tic.rnasa.repository.QueueTypeRepository;
import es.udc.tic.rnasa.repository.search.QueueTypeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing QueueType.
 */
@Service
// This anotation doesn't work with AuthoritiesConstants.ADMIN
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@Transactional
public class QueueTypeService {

    private final Logger log = LoggerFactory.getLogger(QueueTypeService.class);

    @Inject
    private QueueTypeRepository queueTypeRepository;

    @Inject
    private QueueTypeSearchRepository queueTypeSearchRepository;

    /**
     * Save a queueType.
     *
     * @param queueType the entity to save
     * @return the persisted entity
     */
    public QueueType save(QueueType queueType) {
        log.debug("Request to save QueueType : {}", queueType);
        QueueType result = queueTypeRepository.save(queueType);
        queueTypeSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the queueTypes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<QueueType> findAll() {
        log.debug("Request to get all QueueTypes");
        List<QueueType> result = queueTypeRepository.findAll();
        return result;
    }

    /**
     *  Get one queueType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public QueueType findOne(Long id) {
        log.debug("Request to get QueueType : {}", id);
        QueueType queueType = queueTypeRepository.findOne(id);
        return queueType;
    }

    /**
     *  Delete the  queueType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete QueueType : {}", id);
        queueTypeRepository.delete(id);
        queueTypeSearchRepository.delete(id);
    }

    /**
     * Search for the queueType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<QueueType> search(String query) {
        log.debug("Request to search QueueTypes for query {}", query);
        return StreamSupport
            .stream(queueTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
