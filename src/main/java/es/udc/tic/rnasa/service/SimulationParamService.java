/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.domain.SimulationParam;
import es.udc.tic.rnasa.repository.SimulationParamRepository;
import es.udc.tic.rnasa.repository.search.SimulationParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.CustomAngnSearchGliaDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.CustomAnnFixedGliaDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.CustomNuSvmDTO;
import es.udc.tic.rnasa.web.rest.dto.SimulationParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing SimulationParam.
 */
@Service
@Transactional
public class SimulationParamService {

    private final Logger log = LoggerFactory.getLogger(SimulationParamService.class);

    @Inject
    private SimulationParamRepository simulationParamRepository;

    @Inject
    private SimulationParamMapper simulationParamMapper;

    @Inject
    private SimulationParamSearchRepository simulationParamSearchRepository;

    private SimulationParam getSimulationParam(Simulation sim, String name, String value) {
        SimulationParam tmp = new SimulationParam();
        tmp.setBelong(sim);
        tmp.setName(name);
        tmp.setValue(value.substring(0, Math.min(value.length(), 250)));

        return tmp;
    }

    public void save(Simulation simulation, CustomNuSvmDTO customNuSvmDTO) {
        List<SimulationParam> params = new ArrayList<>();

        params.add(getSimulationParam(simulation, "constraint.nu.min", customNuSvmDTO.getCoNu().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.nu.max", customNuSvmDTO.getCoNu().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.tolerance.min", customNuSvmDTO.getCoTolerance().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.tolerance.max", customNuSvmDTO.getCoTolerance().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.degree.min", customNuSvmDTO.getCoDegree().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.degree.max", customNuSvmDTO.getCoDegree().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.gamma.min", customNuSvmDTO.getCoGamma().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.gamma.max", customNuSvmDTO.getCoGamma().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.coef.min", customNuSvmDTO.getCoCoef().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.coef.max", customNuSvmDTO.getCoCoef().get(1).toString()));

        simulationParamRepository.save(params);
        simulationParamSearchRepository.save(params);
    }


    public void save(Simulation simulation, CustomAnnFixedGliaDTO customAnnFixedGliaDTO) {
        List<SimulationParam> params = new ArrayList<>();

        params.add(getSimulationParam(simulation, "layer",      customAnnFixedGliaDTO.getLayer().toString()));
        params.add(getSimulationParam(simulation, "iteration",  customAnnFixedGliaDTO.getIteration().toString()));
        params.add(getSimulationParam(simulation, "activation", customAnnFixedGliaDTO.getActivation().toString()));

        for (int cnt = 0; cnt < customAnnFixedGliaDTO.getLayer(); cnt++) {
            params.add(getSimulationParam(simulation, String.format("layer[%d].neuron", cnt), customAnnFixedGliaDTO.getNeuron().get(cnt).toString()));
            params.add(getSimulationParam(simulation, String.format("layer[%d].activationFunction", cnt), customAnnFixedGliaDTO.getActivationFunction().get(cnt).toString()));
            params.add(getSimulationParam(simulation, String.format("layer[%d].gliaAlgorithm", cnt), customAnnFixedGliaDTO.getGliaAlgorithm().get(cnt).toString()));
            params.add(getSimulationParam(simulation, String.format("layer[%d].increase", cnt), customAnnFixedGliaDTO.getIncrease().get(cnt).toString()));
            params.add(getSimulationParam(simulation, String.format("layer[%d].decrease", cnt), customAnnFixedGliaDTO.getDecrease().get(cnt).toString()));
        }

        params.add(getSimulationParam(simulation, "constraint.weight.min", customAnnFixedGliaDTO.getCoWeight().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.weight.max", customAnnFixedGliaDTO.getCoWeight().get(1).toString()));

        simulationParamRepository.save(params);
        simulationParamSearchRepository.save(params);
    }

    public void save(Simulation simulation, CustomAngnSearchGliaDTO customAngnSearchGliaDTO) {
        List<SimulationParam> params = new ArrayList<>();

        params.add(getSimulationParam(simulation, "layer",      customAngnSearchGliaDTO.getLayer().toString()));

        for (int cnt = 0; cnt < customAngnSearchGliaDTO.getLayer(); cnt++) {
            params.add(getSimulationParam(simulation, String.format("layer[%d].neuron", cnt), customAngnSearchGliaDTO.getNeuron().get(cnt).toString()));
            params.add(getSimulationParam(simulation, String.format("layer[%d].activationFunction", cnt), customAngnSearchGliaDTO.getActivationFunction().get(cnt).toString()));
        }

        params.add(getSimulationParam(simulation, "constraint.weight.min", customAngnSearchGliaDTO.getCoWeight().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.weight.max", customAngnSearchGliaDTO.getCoWeight().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.weight.min",      customAngnSearchGliaDTO.getCoWeight().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.weight.max",      customAngnSearchGliaDTO.getCoWeight().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.iteration.min",   customAngnSearchGliaDTO.getCoIteration().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.iteration.max",   customAngnSearchGliaDTO.getCoIteration().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.activation.min",  customAngnSearchGliaDTO.getCoActivation().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.activation.max",  customAngnSearchGliaDTO.getCoActivation().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.increase.min",    customAngnSearchGliaDTO.getCoIncrease().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.increase.max",    customAngnSearchGliaDTO.getCoIncrease().get(1).toString()));
        params.add(getSimulationParam(simulation, "constraint.decrease.min",    customAngnSearchGliaDTO.getCoDecrease().get(0).toString()));
        params.add(getSimulationParam(simulation, "constraint.decrease.max",    customAngnSearchGliaDTO.getCoDecrease().get(1).toString()));

        params.add(getSimulationParam(simulation, "constraint.sameGlia", customAngnSearchGliaDTO.getSameGliaAlgorithm().toString()));
        params.add(getSimulationParam(simulation, "constraint.decreaseGtIncrease", customAngnSearchGliaDTO.getDecreaseGreatherThanIncrease().toString()));

        simulationParamRepository.save(params);
        simulationParamSearchRepository.save(params);
    }

    /**
     * Save a simulationParam.
     *
     * @param simulationParamDTO the entity to save
     * @return the persisted entity
     */
    public SimulationParamDTO save(SimulationParamDTO simulationParamDTO) {
        log.debug("Request to save SimulationParam : {}", simulationParamDTO);
        SimulationParam simulationParam = simulationParamMapper.simulationParamDTOToSimulationParam(simulationParamDTO);
        simulationParam = simulationParamRepository.save(simulationParam);
        SimulationParamDTO result = simulationParamMapper.simulationParamToSimulationParamDTO(simulationParam);
        simulationParamSearchRepository.save(simulationParam);
        return result;
    }

    /**
     *  Get all the simulationParams.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SimulationParam> findAll(Pageable pageable) {
        log.debug("Request to get all SimulationParams");
        Page<SimulationParam> result = simulationParamRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one simulationParam by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SimulationParamDTO findOne(Long id) {
        log.debug("Request to get SimulationParam : {}", id);
        SimulationParam simulationParam = simulationParamRepository.findOne(id);
        SimulationParamDTO simulationParamDTO = simulationParamMapper.simulationParamToSimulationParamDTO(simulationParam);
        return simulationParamDTO;
    }

    /**
     *  Delete the  simulationParam by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SimulationParam : {}", id);
        simulationParamRepository.delete(id);
        simulationParamSearchRepository.delete(id);
    }

    /**
     * Search for the simulationParam corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SimulationParam> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SimulationParams for query {}", query);
        return simulationParamSearchRepository.search(queryStringQuery(query), pageable);
    }
}
