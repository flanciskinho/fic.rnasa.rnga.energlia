/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.repository.search.SimulationSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.security.SecurityUtils;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotOpenConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotTransferConnectionException;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.service.exceptions.NoDataException;
import es.udc.tic.rnasa.service.exceptions.TooMuchConnectionException;
import es.udc.tic.rnasa.service.util.CheckPermission;
import es.udc.tic.rnasa.web.rest.dto.CustomDescriptionDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.*;
import es.udc.tic.rnasa.web.rest.dto.SimulationDTO;
import es.udc.tic.rnasa.web.rest.dto.SimulationDetailDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Simulation.
 */
@Service
@Transactional
public class SimulationService {

    private final Logger log = LoggerFactory.getLogger(SimulationService.class);

    private static final String SCRIPT_PATHNAME = ".energlia/simulation/%d/script";
    private static final String CONFIG_PATHNAME = ".energlia/simulation/%d/config";
    private static final String RESULT_PATHNAME = ".energlia/simulation/%d/result";

    @Inject
    private PopulationCombinationService populationCombinationService;
    @Inject
    private DatasetCombinationService datasetCombinationService;
    @Inject
    private CompiledAppService compiledAppService;

    @Inject
    private DatasetRepository datasetRepository;

    @Inject
    private SshAccountService sshAccountService;
    @Inject
    private SshAccountRepository sshAccountRepository;

    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private GaParamService gaParamService;

    @Inject
    private PopulationParamService populationParamService;

    @Inject
    private PopulationRepository populationRepository;

    @Inject
    private PopulationOrderRepository populationOrderRepository;

    @Inject
    private SimulationParamService simulationParamService;

    @Inject
    private GaTypeService gaTypeService;

    @Inject
    private ExecParamService execParamService;

    @Inject
    private GaRepository gaRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private SimulationRepository simulationRepository;

    @Inject
    private SimulationMapper simulationMapper;

    @Inject
    private SimulationSearchRepository simulationSearchRepository;

    @Inject
    private SpecificSimulationService specificSimulationService;

    @Inject
    private ErrorTypeRepository errorTypeRepository;

    @Inject
    private ProblemTypeRepository problemTypeRepository;

    @Inject
    private SshServerRepository sshServerRepository;

    @Transactional(readOnly = true)
    public String getScriptPathname() {
        return SCRIPT_PATHNAME;
    }
    @Transactional(readOnly = true)
    public String getConfigPathname() {
        return CONFIG_PATHNAME;
    }
    @Transactional(readOnly = true)
    public String getResultPathname() {
        return RESULT_PATHNAME;
    }

    /**
     * Save a simulation.
     *
     * @param simulationDTO the entity to save
     * @return the persisted entity
     */
    public SimulationDTO save(SimulationDTO simulationDTO) throws HttpExceptionDetails {
        log.debug("Request to save Simulation : {}", simulationDTO);

        String currentUserLogin = SecurityUtils.getCurrentUserLogin();

        simulationDTO.setBelongLogin(currentUserLogin);
        Optional<User> owner = userRepository.findOneByLogin(currentUserLogin);
        if (owner.isPresent())
            simulationDTO.setBelongId(owner.get().getId());

        simulationDTO.setTimestamp(ZonedDateTime.now());

        new CheckPermission(userRepository).checkPermission(simulationDTO);

        Simulation simulation = simulationMapper.simulationDTOToSimulation(simulationDTO);
        simulation = simulationRepository.save(simulation);
        SimulationDTO result = simulationMapper.simulationToSimulationDTO(simulation);
        simulationSearchRepository.save(simulation);
        return result;
    }



    private List<Population> getPopulationsFromIds(SimPopulationDTO []popId) {
        List<Long> populationsId = new ArrayList<>();
        for (SimPopulationDTO p: popId)
            populationsId.add(p.getPopulationId());

        List<Population> populations = populationRepository.findByIdIn(populationsId);

        return populations;
    }

    private Simulation getNewSimulationDTO(CommonSimulationDTO detail) {
        String currentUserLogin = SecurityUtils.getCurrentUserLogin();
        Long currentUserId = null;
        Optional<User> owner = userRepository.findOneByLogin(currentUserLogin);
        if (owner.isPresent())
            currentUserId = owner.get().getId();

        SimulationDTO simulationDTO = new SimulationDTO(null,
            detail.getName(),
            ZonedDateTime.now(),
            detail.getDescription(),
            currentUserId, currentUserLogin,
            detail.getDatasetId(),
            null,
            detail.getProblemId(),
            detail.getErrorId(),
            detail.getServerId());

        Simulation simulation = simulationMapper.simulationDTOToSimulation(simulationDTO);
log.debug("simulation {}", simulation);

        simulation = simulationRepository.save(simulation);
        simulationSearchRepository.save(simulation);

        return simulation;
    }

    private Ga saveGaParams(Simulation simulation, SimGaDTO simGaDTO, SimGaBasicDTO gaBasicDTO) {
        Ga ga = new Ga();
        ga.setUse(gaTypeService.getCooperativeCoevolutiveSteadyState());
        ga.setBelong(simulation);
        gaRepository.save(ga);

        // Save GA params
        if (simGaDTO != null)
            gaParamService.save(ga, simGaDTO);

        if (gaBasicDTO != null)
            gaParamService.save(ga, gaBasicDTO);

        return ga;
    }

    private void savePopulationParams(Ga ga, List<Population> populations, SimPopulationDTO []simPopulationDTO) {
        PopulationOrder populationOrder;
        for (int cnt = 0; cnt < populations.size(); cnt++) {
            populationOrder = new PopulationOrder(cnt, ga, populations.get(cnt));
            populationOrder = populationOrderRepository.save(populationOrder);

            // Save PopulationParam
            populationParamService.save(populationOrder,
                simPopulationDTO[cnt]);
        }
    }

    private boolean checkNumPopulations(CustomSimulationVariablesDTO simulation, int size) throws HttpExceptionDetails {
        boolean result = true;
        if (simulation instanceof CustomNuSvmDTO || simulation instanceof CustomAnnFixedGliaDTO)
            result = (size == 1);
        if (simulation instanceof CustomAngnSearchGliaDTO)
            result = (size == 2);

        if (!result)
            throw new HttpExceptionDetails(HttpStatus.BAD_REQUEST, "invalid number of populations ("+simulation.getPopulations()+")", this.getClass());

        return result;
    }

    private void saveSimulationParam(Simulation simulation, CustomSimulationVariablesDTO variables) {
        if (variables instanceof CustomNuSvmDTO) {
            simulationParamService.save(simulation, (CustomNuSvmDTO) variables);
        }
        if (variables instanceof CustomAnnFixedGliaDTO) {
            simulationParamService.save(simulation, (CustomAnnFixedGliaDTO) variables);
        }
        if (variables instanceof CustomAngnSearchGliaDTO) {
            simulationParamService.save(simulation, (CustomAngnSearchGliaDTO) variables);
        }
    }

    public SimulationDTO save(CustomSimulationVariablesDTO customSimulationVariablesDTO) throws HttpExceptionDetails {
        List<Population> populations = getPopulationsFromIds(customSimulationVariablesDTO.getPopulations());

        populations.sort(new Comparator<Population>() {
            private Integer getIndex(Population o1) {
                for (SimPopulationDTO simPopulationDTO: customSimulationVariablesDTO.getPopulations()) {
                    if (simPopulationDTO.getPopulationId() == o1.getId())
                        return simPopulationDTO.getIndexOrder();
                }
                return new Integer(-1);
            }

            @Override
            public int compare(Population o1, Population o2) {
                return this.getIndex(o1).compareTo(this.getIndex(o2));
            }
        });

        // Check num populations
        checkNumPopulations(customSimulationVariablesDTO, populations.size());

        Simulation simulation = getNewSimulationDTO(customSimulationVariablesDTO.getDetail());
        customSimulationVariablesDTO.setId(simulation.getId());

        Ga ga = saveGaParams(simulation, customSimulationVariablesDTO.getGa(), customSimulationVariablesDTO.getGaBasic());

        savePopulationParams(ga, populations, customSimulationVariablesDTO.getPopulations());

        // Save params
        saveSimulationParam(simulation, customSimulationVariablesDTO);

        execParamService.save(simulation, customSimulationVariablesDTO.getExecution());

log.debug("User {}", SecurityUtils.getCurrentUserLogin());
        simulation.setBelong(userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get());
        simulation.setUse(datasetRepository.findOne(customSimulationVariablesDTO.getDetail().getDatasetId()));
        simulation.setExe(applicationRepository.findFirstByActivatedTrueOrderByTimestamp());
        simulation.setLaunch(sshServerRepository.findOne(customSimulationVariablesDTO.getDetail().getServerId()));
        simulation.setMinimize(errorTypeRepository.findOne(customSimulationVariablesDTO.getDetail().getErrorId()));
        simulation.setProblem(problemTypeRepository.findOne(customSimulationVariablesDTO.getDetail().getProblemId()));

        int numTask = simulation.getUse().getNumCombination();
        if (customSimulationVariablesDTO.getPopulations()[0] != null)
            numTask *= populationRepository.findOne(customSimulationVariablesDTO.getPopulations()[0].getPopulationId()).getNumCombination();

        simulation.setTaskTotal(numTask);
        simulation.setTaskQueue(numTask);

        simulation = simulationRepository.save(simulation);
        SimulationDTO result = simulationMapper.simulationToSimulationDTO(simulation);
        result.setBelongLogin(SecurityUtils.getCurrentUserLogin());

        specificSimulationService.createSpecificSimulation(simulation);

        return result;
    }

    /**
     *  Get all the simulations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Simulation> findAll(Pageable pageable) {
        log.debug("Request to get all Simulations");
        Page<Simulation> result;

        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))
            result = simulationRepository.findAll(pageable);
        else
            result = simulationRepository.findAllByUserLogin(pageable, SecurityUtils.getCurrentUserLogin());

        return result;
    }

    /**
     *  Get one simulation by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SimulationDTO findOne(Long id) throws HttpExceptionDetails {
        log.debug("Request to get Simulation : {}", id);
        Simulation simulation = simulationRepository.findOne(id);
        SimulationDTO simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);

        new CheckPermission(userRepository).checkPermission(simulationDTO);

        return simulationDTO;
    }

    /**
     *  Delete the  simulation by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) throws HttpExceptionDetails {
        log.debug("Request to delete Simulation : {}", id);

        Simulation simulation = simulationRepository.findOne(id);
        SimulationDTO simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);

        new CheckPermission(userRepository).checkPermission(simulationDTO);

        simulationRepository.delete(id);
        simulationSearchRepository.delete(id);
    }

    /**
     * Search for the simulation corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Simulation> search(String query, Pageable pageable) throws HttpExceptionDetails {
        log.debug("Request to search for a page of Simulations for query {}", query);

        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))
            throw new HttpExceptionDetails(HttpStatus.NOT_IMPLEMENTED, "no implemented", this.getClass());

        return simulationSearchRepository.search(queryStringQuery(query), pageable);
    }



    public SimulationDTO updateSimulation(CustomDescriptionDTO customDescriptionDTO) throws HttpExceptionDetails {
        Simulation simulation = simulationRepository.findOne(customDescriptionDTO.getId());
        SimulationDTO simulationDTO = null;
        if (simulation != null)
            simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);

        new CheckPermission(userRepository).checkPermission(simulationDTO);

        simulation.setDescription(customDescriptionDTO.getDescription());

        simulation = simulationRepository.save(simulation);
        SimulationDTO result = simulationMapper.simulationToSimulationDTO(simulation);
        simulationSearchRepository.save(simulation);
        return result;
    }

    public void createDirSimulation(Simulation simulation, SshAccount sshAccount) throws TooMuchConnectionException, CannotOpenConnectionException, CannotExecConnectionException, NoDataException, CannotTransferConnectionException {
        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;
        try {
            exec     = (ExecConnectionServer)     sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);
            transfer = (TransferConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.SFTP);

            String dirs[] = {SCRIPT_PATHNAME, CONFIG_PATHNAME, RESULT_PATHNAME};

            for (String tmp: dirs) {
                if (!sshAccountService.exists(exec, String.format(tmp, simulation.getId())))
                    sshAccountService.launchCommand(exec, String.format("mkdir -p %s", String.format(tmp, simulation.getId())));
            }

            // Send data
            datasetCombinationService.sendData(datasetRepository.findOne(simulation.getUse().getId()), exec, transfer);

            // Send population
            List<Population> populationList = simulationRepository.findAllPopulations(simulation.getId());
            for (Population p: populationList)
                populationCombinationService.sendData(p, exec, transfer);

            // Send compiled app
            CompiledApp compiledApp = compiledAppService.sendCompiledApp(sshAccount.getBelong(), exec, transfer);
            simulation.setExe(compiledApp.getBelong());

            sshAccountService.closeConnection(exec);
            exec = null;
            sshAccountService.closeConnection(transfer);
            transfer = null;
        } catch (TooMuchConnectionException | CannotOpenConnectionException | CannotExecConnectionException e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
            if (transfer != null)
                sshAccountService.closeConnection(transfer);

            throw e;
        }
    }

    public void addTaskServer(Simulation simulation) {
        simulation.setTaskServer(simulation.getTaskServer()+1);
        simulation.setTaskQueue(simulation.getTaskQueue()-1);

        simulationRepository.save(simulation);
    }

    public void addTaskDone(Simulation simulation) {
        simulation.setTaskDone(simulation.getTaskDone()+1);
        simulation.setTaskServer(simulation.getTaskServer()-1);

        simulationRepository.save(simulation);
    }

    public void addTaskFail(Simulation simulation) {
        simulation.setTaskFail(simulation.getTaskFail()+1);
        simulation.setTaskServer(simulation.getTaskServer()-1);

        simulationRepository.save(simulation);
    }


    public SimulationDetailDTO getSimulationDetail(Long simulationId) {
        return getSimulationDetail(simulationRepository.findOne(simulationId));
    }

    public SimulationDetailDTO getSimulationDetail (Simulation simulation) {
        SimulationDetailDTO detail = new SimulationDetailDTO();
        detail.setSimulation(simulation);

        detail.setDataset(datasetRepository.findOne(simulation.getUse().getId()));
        detail.setExec(sshAccountRepository.findOne(simulation.getLaunch().getId()).getBelong());
        detail.setError(errorTypeRepository.findOne(simulation.getMinimize().getId()));



        return detail;
    }
}
