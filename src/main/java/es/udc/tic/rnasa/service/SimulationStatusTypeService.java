/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;


import es.udc.tic.rnasa.domain.SimulationStatusType;
import es.udc.tic.rnasa.repository.SimulationStatusTypeRepository;
import es.udc.tic.rnasa.repository.search.SimulationStatusTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.SimulationStatusTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationStatusTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing SimulationStatusType.
 */
@Service
@Transactional
public class SimulationStatusTypeService {

    private final Logger log = LoggerFactory.getLogger(SimulationStatusTypeService.class);

    @Inject
    private SimulationStatusTypeRepository simulationStatusTypeRepository;

    @Inject
    private SimulationStatusTypeMapper simulationStatusTypeMapper;

    @Inject
    private SimulationStatusTypeSearchRepository simulationStatusTypeSearchRepository;


    private static SimulationStatusType WAITING_FOR_QUEUE = null;
    private static SimulationStatusType WAITING_ON_QUEUE = null;
    private static SimulationStatusType RUNNING = null;
    private static SimulationStatusType FINISHED_SUCCESS = null;
    private static SimulationStatusType FINISHED_FAILURE = null;

    @Transactional(readOnly = true)
    public SimulationStatusType getWAITING_FOR_QUEUE() {
        if (WAITING_FOR_QUEUE == null) {
            Optional<SimulationStatusType> optional = simulationStatusTypeRepository.findOneByName("WAITING_FOR_QUEUE");
            WAITING_FOR_QUEUE = optional.orElse(null);
        }
        return WAITING_FOR_QUEUE;
    }

    @Transactional(readOnly = true)
    public SimulationStatusType getWAITING_ON_QUEUE() {
        if (WAITING_ON_QUEUE == null) {
            Optional<SimulationStatusType> optional = simulationStatusTypeRepository.findOneByName("WAITING_ON_QUEUE");
            WAITING_ON_QUEUE = optional.orElse(null);
        }
        return WAITING_ON_QUEUE;
    }

    @Transactional(readOnly = true)
    public SimulationStatusType getRUNNING() {
        if (RUNNING == null) {
            Optional<SimulationStatusType> optional = simulationStatusTypeRepository.findOneByName("RUNNING");
            RUNNING = optional.orElse(null);
        }
        return RUNNING;
    }

    @Transactional(readOnly = true)
    public SimulationStatusType getFINISHED_SUCCESS() {
        if (FINISHED_SUCCESS == null) {
            Optional<SimulationStatusType> optional = simulationStatusTypeRepository.findOneByName("FINISHED_SUCCESS");
            FINISHED_SUCCESS = optional.orElse(null);
        }
        return FINISHED_SUCCESS;
    }

    @Transactional(readOnly = true)
    public SimulationStatusType getFINISHED_FAILURE() {
        if (FINISHED_FAILURE == null) {
            Optional<SimulationStatusType> optional = simulationStatusTypeRepository.findOneByName("FINISHED_FAILURE");
            FINISHED_FAILURE = optional.orElse(null);
        }
        return FINISHED_FAILURE;
    }


    /**
     * Save a simulationStatusType.
     *
     * @param simulationStatusTypeDTO the entity to save
     * @return the persisted entity
     */
    public SimulationStatusTypeDTO save(SimulationStatusTypeDTO simulationStatusTypeDTO) {
        log.debug("Request to save SimulationStatusType : {}", simulationStatusTypeDTO);
        SimulationStatusType simulationStatusType = simulationStatusTypeMapper.simulationStatusTypeDTOToSimulationStatusType(simulationStatusTypeDTO);
        simulationStatusType = simulationStatusTypeRepository.save(simulationStatusType);
        SimulationStatusTypeDTO result = simulationStatusTypeMapper.simulationStatusTypeToSimulationStatusTypeDTO(simulationStatusType);
        simulationStatusTypeSearchRepository.save(simulationStatusType);
        return result;
    }

    /**
     *  Get all the simulationStatusTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SimulationStatusType> findAll(Pageable pageable) {
        log.debug("Request to get all SimulationStatusTypes");
        Page<SimulationStatusType> result = simulationStatusTypeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one simulationStatusType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SimulationStatusTypeDTO findOne(Long id) {
        log.debug("Request to get SimulationStatusType : {}", id);
        SimulationStatusType simulationStatusType = simulationStatusTypeRepository.findOne(id);
        SimulationStatusTypeDTO simulationStatusTypeDTO = simulationStatusTypeMapper.simulationStatusTypeToSimulationStatusTypeDTO(simulationStatusType);
        return simulationStatusTypeDTO;
    }

    /**
     *  Delete the  simulationStatusType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SimulationStatusType : {}", id);
        simulationStatusTypeRepository.delete(id);
        simulationStatusTypeSearchRepository.delete(id);
    }

    /**
     * Search for the simulationStatusType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SimulationStatusType> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SimulationStatusTypes for query {}", query);
        return simulationStatusTypeSearchRepository.search(queryStringQuery(query), pageable);
    }
}
