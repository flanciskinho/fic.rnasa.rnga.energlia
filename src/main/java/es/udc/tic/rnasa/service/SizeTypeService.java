/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.SizeType;
import es.udc.tic.rnasa.repository.SizeTypeRepository;
import es.udc.tic.rnasa.repository.search.SizeTypeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SizeType.
 */
@Service
@Transactional
public class SizeTypeService {

    private final Logger log = LoggerFactory.getLogger(SizeTypeService.class);

    @Inject
    private SizeTypeRepository sizeTypeRepository;

    @Inject
    private SizeTypeSearchRepository sizeTypeSearchRepository;

    private static SizeType BINARY = null;
    private static SizeType SHORT  = null;
    private static SizeType REAL   = null;

    private SizeType getEnum(String name) {
        Optional<SizeType> optional = sizeTypeRepository.findOneByName(name);
        if (!optional.isPresent())
            log.debug("not found '{}' on SizeTypeRepository", name);

        return optional.get();
    }

    public SizeType getBINARY() {
        if (BINARY == null)
            BINARY = getEnum("BINARY");

        return BINARY;
    }

    public SizeType getSHORT() {
        if (SHORT == null)
            SHORT = getEnum("SHORT");

        return SHORT;
    }

    public SizeType getREAL() {
        if (REAL == null)
            REAL = getEnum("REAL");

        return REAL;
    }

    /**
     * Save a sizeType.
     *
     * @param sizeType the entity to save
     * @return the persisted entity
     */
    public SizeType save(SizeType sizeType) {
        log.debug("Request to save SizeType : {}", sizeType);
        SizeType result = sizeTypeRepository.save(sizeType);
        sizeTypeSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the sizeTypes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SizeType> findAll() {
        log.debug("Request to get all SizeTypes");
        List<SizeType> result = sizeTypeRepository.findAll();
        return result;
    }

    /**
     *  Get one sizeType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SizeType findOne(Long id) {
        log.debug("Request to get SizeType : {}", id);
        SizeType sizeType = sizeTypeRepository.findOne(id);
        return sizeType;
    }

    /**
     *  Delete the  sizeType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SizeType : {}", id);
        sizeTypeRepository.delete(id);
        sizeTypeSearchRepository.delete(id);
    }

    /**
     * Search for the sizeType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SizeType> search(String query) {
        log.debug("Request to search SizeTypes for query {}", query);
        return StreamSupport
            .stream(sizeTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
