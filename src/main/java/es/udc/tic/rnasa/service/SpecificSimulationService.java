/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.repository.search.SpecificSimulationSearchRepository;
import es.udc.tic.rnasa.service.bashLaunch.BashLaunchFactory;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotOpenConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotTransferConnectionException;
import es.udc.tic.rnasa.service.exceptions.NoDataException;
import es.udc.tic.rnasa.service.exceptions.ParseException;
import es.udc.tic.rnasa.service.exceptions.TooMuchConnectionException;
import es.udc.tic.rnasa.service.queueOperations.QueueOperation;
import es.udc.tic.rnasa.service.queueServer.QueueFactory;
import es.udc.tic.rnasa.service.util.ParseUtil;
import es.udc.tic.rnasa.service.xmlParameter.XmlParameter;
import es.udc.tic.rnasa.web.rest.dto.SpecificSimulationDTO;
import es.udc.tic.rnasa.web.rest.mapper.SpecificSimulationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing SpecificSimulation.
 */
@Service
@Transactional
public class SpecificSimulationService {

    private final Logger log = LoggerFactory.getLogger(SpecificSimulationService.class);

    private final static int SIZE_PAGE = 5;

    @Inject
    private SpecificSimulationRepository specificSimulationRepository;

    @Inject
    private SpecificSimulationMapper specificSimulationMapper;

    @Inject
    private SpecificSimulationSearchRepository specificSimulationSearchRepository;

    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;

    @Inject
    private PopulationRepository populationRepository;

    @Inject
    private PopulationCombinationOrderRepository populationCombinationOrderRepository;

    @Inject
    private SshAccountRepository sshAccountRepository;

    @Inject
    private SshAccountService sshAccountService;

    @Inject
    private SimulationService simulationService;

    @Inject
    private XmlParameter xmlParameter;

    @Inject
    private QueueFactory queueFactory;

    @Inject
    private BashLaunchFactory bashLaunchFactory;

    @Inject
    private LogRecordService logRecordService;

    public void createSpecificSimulation(Simulation simulation) {
//log.debug("Create specific simulations for {}", simulation);
        int cnt, aux, tmp;
        List<PopulationCombinationOrder> populationCombinationOrderList = new ArrayList<>();
        List<SpecificSimulation> specificSimulationList = new ArrayList<>();

        // get all combination data set
//log.debug("Try to get all combinations where dataset id = {}", simulation.getUse().getId());
        List<DatasetCombination> datasetCombinationList = datasetCombinationRepository.findAllByBelongId(simulation.getUse().getId());
        datasetCombinationList.stream().forEach(dc -> log.debug("DC {}:{}", dc.getIndexCombination(), dc.getBelong().getName()));

        // get all populationCombinations
        List<Population> populationList = populationRepository.findAllBySimulationId(simulation.getId());
        List<List<PopulationCombination>> populationCombination = new ArrayList<>();

        if (!populationList.isEmpty()) {
            for (cnt = 0; cnt < populationList.size(); cnt++) {
                populationCombination.add(populationCombinationRepository.findAllByBelongId(populationList.get(cnt).getId()));
//log.debug("populationCombination {} = {}", cnt, populationCombination.get(cnt));
            }
        }

        SpecificSimulation specificSimulation, entity;
        PopulationCombinationOrder populationCombinationOrder;
        for (cnt = 0; cnt < datasetCombinationList.size(); cnt++) {


            if (populationList.isEmpty()) {
                specificSimulation = new SpecificSimulation(
                    simulationStatusTypeService.getWAITING_FOR_QUEUE(), simulation,
                    datasetCombinationList.get(cnt));
                specificSimulationList.add(specificSimulation);
            } else {
                for (tmp = 0; tmp < populationCombination.get(0).size(); tmp++) {
                    specificSimulation = new SpecificSimulation(
                        simulationStatusTypeService.getWAITING_FOR_QUEUE(), simulation,
                        datasetCombinationList.get(cnt));
                        specificSimulationList.add(specificSimulation);
                    for (aux = 0; aux < populationCombination.size(); aux++) {
                        populationCombinationOrderList.add(new PopulationCombinationOrder(aux, specificSimulation, populationCombination.get(aux).get(tmp)));
                    }
                }
            }
        }

        specificSimulationRepository.save(specificSimulationList);
        if (!populationCombinationOrderList.isEmpty())
            populationCombinationOrderRepository.save(populationCombinationOrderList);


        specificSimulationList.stream().forEach(ss -> log.debug("ss: {}", ss));
        populationCombinationOrderList.stream().forEach(pco -> log.debug("pco: {}", pco));
    }


    /**
     * Save a specificSimulation.
     *
     * @param specificSimulationDTO the entity to save
     * @return the persisted entity
     */
    public SpecificSimulationDTO save(SpecificSimulationDTO specificSimulationDTO) {
        log.debug("Request to save SpecificSimulation : {}", specificSimulationDTO);
        SpecificSimulation specificSimulation = specificSimulationMapper.specificSimulationDTOToSpecificSimulation(specificSimulationDTO);

        if (specificSimulationDTO.getLaunchById() != null)
            specificSimulation.setLaunchBy(sshAccountRepository.findOne(specificSimulationDTO.getLaunchById()));

        specificSimulation = specificSimulationRepository.save(specificSimulation);
        SpecificSimulationDTO result = specificSimulationMapper.specificSimulationToSpecificSimulationDTO(specificSimulation);
        specificSimulationSearchRepository.save(specificSimulation);
        return result;
    }

    /**
     *  Get all the specificSimulations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SpecificSimulation> findAll(Pageable pageable) {
        log.debug("Request to get all SpecificSimulations");
        Page<SpecificSimulation> result = specificSimulationRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one specificSimulation by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SpecificSimulationDTO findOne(Long id) {
        log.debug("Request to get SpecificSimulation : {}", id);
        SpecificSimulation specificSimulation = specificSimulationRepository.findOne(id);
        SpecificSimulationDTO specificSimulationDTO = specificSimulationMapper.specificSimulationToSpecificSimulationDTO(specificSimulation);
        return specificSimulationDTO;
    }

    /**
     *  Delete the  specificSimulation by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SpecificSimulation : {}", id);
        specificSimulationRepository.delete(id);
        specificSimulationSearchRepository.delete(id);
    }

    /**
     * Search for the specificSimulation corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SpecificSimulation> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SpecificSimulations for query {}", query);
        return specificSimulationSearchRepository.search(queryStringQuery(query), pageable);
    }

    public Page<SpecificSimulation> findAllByStatusIdOrderById(Long id, Pageable pageable) {
        log.debug("Request to get all SpecificSimulations filter by status {}", id);
        Page<SpecificSimulation> result = specificSimulationRepository.findAllByStatusIdOrderById(id, pageable);
        return result;
    }


    private boolean getResult(SpecificSimulation specificSimulation, ExecConnectionServer exec) throws CannotTransferConnectionException, CannotExecConnectionException, TooMuchConnectionException, CannotOpenConnectionException {
        String pathname = String.format("%s/%d.xml",
            String.format(simulationService.getResultPathname(), specificSimulation.getBelong().getId()),
                specificSimulation.getId());
        log.debug("check file {} on {}@{}", pathname, specificSimulation.getLaunchBy().getUsername(), specificSimulation.getLaunchBy().getBelong().getDnsname());

        if (!sshAccountService.exists(exec, pathname)) {
            log.debug("file {} doesn't exist", pathname);
            return false;
        }

        TransferConnectionServer transfer = null;
        String str = null;
        try {
            transfer = (TransferConnectionServer) sshAccountService.openConnection(specificSimulation.getLaunchBy().getId(), ChannelType.SFTP);
            str = transfer.receiveData(pathname);

        } catch (CannotOpenConnectionException | TooMuchConnectionException | CannotTransferConnectionException e) {
            if (transfer != null)
                sshAccountService.closeConnection(transfer);
            return false;
        }

        if (transfer != null)
            sshAccountService.closeConnection(transfer);


        try {
            Document d = ParseUtil.StringToXmlDocument(str);

            logRecordService.saveLogs(specificSimulation, d);

            specificSimulation.setFilelog(str.getBytes());
            specificSimulation.setFilelogContentType("text/xml");
            specificSimulation.setStatus(simulationStatusTypeService.getFINISHED_SUCCESS());
            simulationService.addTaskDone(specificSimulation.getBelong());

            specificSimulationRepository.save(specificSimulation);

        } catch (ParseException e) {
            log.debug("Failed to parse pathname {}", pathname);
            return false;
        }


        return true;
    }

    @Scheduled(initialDelay = 45000, fixedDelay = 120000) // Wait 20 seconds to start and repeat every two minutes
    public void checkJobOnQueue() {
        int numPage = 0;
        boolean repeat = false;
        int pageSize = 5;

        List<Long> statusOnQueue = new ArrayList(2);
        statusOnQueue.add(simulationStatusTypeService.getWAITING_ON_QUEUE().getId());
        statusOnQueue.add(simulationStatusTypeService.getRUNNING().getId());

        SshAccount sshAccount = null;
        ExecConnectionServer exec = null;
        QueueOperation queue = null;
        Map<Long, SimulationStatusType> map = null;

        List<SpecificSimulation> list = specificSimulationRepository.findAllByStatusIdIn(statusOnQueue, new PageRequest(numPage, pageSize));

        while (!list.isEmpty()) {
            list.sort((o1, o2) -> o1.getLaunchBy().getId().compareTo(o2.getLaunchBy().getId()));

            exec = null;
            sshAccount = null;
            try {
                for (SpecificSimulation ss : list) {
                    if (!ss.getLaunchBy().equals(sshAccount)) {
                        //Cerramos la conexion anterior
                        if (exec != null)
                            sshAccountService.closeConnection(exec);
                        exec = (ExecConnectionServer) sshAccountService.openConnection(ss.getLaunchBy().getId(), ChannelType.EXEC);
                        queue = queueFactory.getQueueOperation(ss.getLaunchBy().getBelong().getUse().getName(), exec);
                        queue.setExecConnectionServer(exec);
                        map = queue.getStatus();
                    }


                    if (!map.containsKey(ss.getJobid())) {
                        log.debug("job {} isn't on queue", ss.getJobid());
                        try {
                            if (!getResult(ss, exec)) {
                                log.debug("job {} doesn't generate a file", ss.getJobid());
                                ss.setStatus(simulationStatusTypeService.getFINISHED_FAILURE());
                                simulationService.addTaskFail(ss.getBelong());
                                repeat = true;
                            }
                        } catch (Exception e) {
                            log.debug("Get Exception", ss.getJobid());
                            ss.setStatus(simulationStatusTypeService.getFINISHED_FAILURE());
                            simulationService.addTaskFail(ss.getBelong());
                            repeat = true;
                        }
                    }

                }
            } catch (Exception e) {
                if (exec != null)
                    sshAccountService.closeConnection(exec);
            }

            if (exec != null)
                sshAccountService.closeConnection(exec);

            if (!repeat)
                numPage++;
            list = specificSimulationRepository.findAllByStatusIdIn(statusOnQueue, new PageRequest(numPage, pageSize));
            repeat = false;
        }

    }

    private void updateStatus(Map<Long, SimulationStatusType> map) {
        List<Long> jobIds = new ArrayList<>(map.size());
        jobIds.addAll(map.keySet());

        List<SpecificSimulation> specificSimulations = specificSimulationRepository.findAllByJobidIn(jobIds);

        List<SpecificSimulation> update = new ArrayList<>();
        specificSimulations.stream().filter(specificSimulation -> !specificSimulation.getStatus().equals(map.get(specificSimulation.getJobid()))).collect(Collectors.toCollection(() -> update));

        for (SpecificSimulation ss: update) {
log.debug("UpdateJob job {} (id {}) to {}", ss.getJobid(), ss.getId(), map.get(ss.getJobid()));
            ss.setStatus(map.get(ss.getJobid()));
            specificSimulationRepository.save(ss);
        }

    }

    @Scheduled(initialDelay = 30000, fixedDelay = 120000) // Wait 30 seconds to start and repeat every two minutes
    public void checkSimulationStatus() {
        log.debug("Request to check simulation status");

        int numPage = 0;
        Page<SshAccount> page = sshAccountRepository.findAllByActivatedIsTrueOrderById(new PageRequest(numPage, SIZE_PAGE));

        QueueOperation queue;
        List<SshAccount> list;
        ExecConnectionServer exec = null;


        while (page.getNumberOfElements() != 0) {
            list = new ArrayList<>();
            page.iterator().forEachRemaining(list::add);

            for (SshAccount account: list) {
                exec = null;
                try {
                    exec = (ExecConnectionServer) sshAccountService.openConnection(account.getId(), ChannelType.EXEC);

                    queue = queueFactory.getQueueOperation(account.getBelong().getUse().getName(), exec);
                    queue.setExecConnectionServer(exec);

                    updateStatus(queue.getStatus());

                    sshAccountService.closeConnection(exec);
                } catch (Exception e) {
                    if (exec != null)
                        sshAccountService.closeConnection(exec);
                }

            }

            numPage++;
            page = sshAccountRepository.findAllByActivatedIsTrueOrderById(new PageRequest(numPage, SIZE_PAGE));
        }


    }

    /**
     * Service to launch simulations
     */
    @Scheduled(initialDelay = 15000, fixedDelay = 300000) // Wait 15 seconds to start and repeat every five minutes
    public void launchSpecificSimulation() {
        log.debug("Request to launch specific simulations");

        int numPage = 0;
        Page<SpecificSimulation> page = specificSimulationRepository.findAllByStatusIdOrderById(
            simulationStatusTypeService.getWAITING_FOR_QUEUE().getId(),
            new PageRequest(numPage, SIZE_PAGE));


        if (page.getTotalElements() == 0) {
            log.debug("virtual queue's empty");
            return;
        }

        Simulation simulation = null;
        SshAccount sshAccount = null;
        List<SpecificSimulation> list;
        int cnt;
        long jobid;

        while (page.getNumberOfElements() != 0) {
            list = new ArrayList<>();
            page.iterator().forEachRemaining(list::add);
            list.sort((o1, o2) -> o1.getBelong().getLaunch().getId().compareTo(o2.getBelong().getLaunch().getId()));

//String tmpStr="";for(SpecificSimulation ss: list) tmpStr = tmpStr + ss.getId()+"(s:"+ss.getBelong().getLaunch().getDnsname()+"), ";
//log.debug("numPage {}>{}", numPage, tmpStr);

            for (cnt = 0; cnt < list.size(); cnt++) {
                SpecificSimulation ss = list.get(cnt);
                // Intentamos coger en la misma pagina de specific simulations la misma cuenta ssh
                if (sshAccount == null || !sshAccount.getBelong().equals(ss.getBelong().getLaunch()))
                    sshAccount = sshAccountRepository.findFirstByBelongIdAndActivatedIsTrueOrderByNumJobAsc(ss.getBelong().getLaunch().getId());

                // Comprobamos que no se llenara y si es así pillamos el siguiente
                if (sshAccount.getNumJob() >= sshAccount.getBelong().getMaxJob())
                    sshAccount = sshAccountRepository.findFirstByBelongIdAndActivatedIsTrueOrderByNumJobAsc(ss.getBelong().getLaunch().getId());

                // No hay cuentas con espacio disponible, así que aumentamos la página para ver si pillamos otras simulaciones para lanzar en otro sitio
                if (sshAccount.getNumJob() >= sshAccount.getBelong().getMaxJob()) {
                    if (cnt == 0) // Si no cogió ninguno de esta página intenta en la siguiente
                        numPage++;
                    //break;
                    continue;
                }

                log.debug("Specific simulation {}: dc {} (status {}) launched by  {}>(numJobs {}/{})", ss.getId(), ss.getUseData().getId(), ss.getStatus().getName(), sshAccount.getUsername(), sshAccount.getNumJob(), sshAccount.getBelong().getMaxJob());


                try {
                    simulationService.createDirSimulation(ss.getBelong(), sshAccount);
                    createSpecificSimulationFiles(ss, sshAccount);
                } catch (TooMuchConnectionException | CannotOpenConnectionException | CannotExecConnectionException | NoDataException | CannotTransferConnectionException e) {
                    log.debug("Too many connections {} {}", sshAccount.getBelong().getDnsname(), sshAccount.getBelong().getNumConnection());
                    return; // Too many connections. wait for the next time to try again
                }

                try {
                    jobid = sendToQueue(sshAccount, ss);
                } catch (Exception e) {
                    if (cnt == 0) // Si no cogió ninguno de esta página intenta en la siguiente
                        numPage++;
                    continue;
                }

                sshAccount.setNumJob(sshAccount.getNumJob() + 1);
                ss.setLaunchBy(sshAccount);
                ss.setJobid(jobid);
                ss.setStatus(simulationStatusTypeService.getWAITING_ON_QUEUE());
                simulationService.addTaskServer(ss.getBelong());

                sshAccountRepository.save(sshAccount);
                specificSimulationRepository.save(ss);
            }

            sshAccount = null; // para renovar las siguientes páginas

            page = specificSimulationRepository.findAllByStatusIdOrderById(
                simulationStatusTypeService.getWAITING_FOR_QUEUE().getId(),
                new PageRequest(numPage, SIZE_PAGE));
        }

    }


    /**
     *
     * @param sshAccount
     * @param specificSimulation
     * @return jobid
     */
    private Long sendToQueue(SshAccount sshAccount, SpecificSimulation specificSimulation) {
        ExecConnectionServer exec = null;

        Long jobid = null;
        try {
            exec = (ExecConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);

            QueueOperation queue = queueFactory.getQueueOperation(specificSimulation.getBelong().getLaunch().getUse().getName(), exec);
            queue.setExecConnectionServer(exec);

            jobid = queue.launchJob(specificSimulation);

            if (exec != null)
                sshAccountService.closeConnection(exec);

        } catch (Exception e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
        }

        return jobid;
    }

    public void createSpecificSimulationFiles(SpecificSimulation specificSimulation, SshAccount sshAccount) throws TooMuchConnectionException, CannotOpenConnectionException, CannotExecConnectionException, CannotTransferConnectionException {
        String xmlConfig = xmlParameter.getXmlParameter(specificSimulation);
        String bash = bashLaunchFactory.getBashLaunch(specificSimulation.getBelong().getLaunch().getUse().getName()).getScript(specificSimulation);

        String configPath = String.format("%s/%d.xml", String.format(simulationService.getConfigPathname(), specificSimulation.getBelong().getId()), specificSimulation.getId());
        String bashPath   = getBashPahtname(specificSimulation);

        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;
        try {
            exec = (ExecConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);
            transfer = (TransferConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.SFTP);

            if (!sshAccountService.exists(exec, configPath))
                transfer.sendData(xmlConfig, configPath);

            if (!sshAccountService.exists(exec, bashPath))
                transfer.sendData(bash, bashPath);

            sshAccountService.closeConnection(exec);
            exec = null;
            sshAccountService.closeConnection(transfer);
            transfer = null;
        } catch (TooMuchConnectionException | CannotOpenConnectionException | CannotExecConnectionException | CannotTransferConnectionException e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
            if (transfer != null)
                sshAccountService.closeConnection(transfer);

            throw e;
        }

    }


    public String getBashPahtname(SpecificSimulation specificSimulation) {
        return String.format("%s/energlia%d.sh" , String.format(simulationService.getScriptPathname(), specificSimulation.getBelong().getId()), specificSimulation.getId());
    }
}
