/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.domain.SshAccount;
import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.repository.CompiledAppRepository;
import es.udc.tic.rnasa.repository.SshAccountRepository;
import es.udc.tic.rnasa.repository.SshServerRepository;
import es.udc.tic.rnasa.repository.search.SshAccountSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.security.aes.Aes;
import es.udc.tic.rnasa.security.aes.AesLengthConstants;
import es.udc.tic.rnasa.security.random.IntegerRandom;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotOpenConnectionException;
import es.udc.tic.rnasa.service.exceptions.TooMuchConnectionException;
import es.udc.tic.rnasa.service.queueOperations.QueueOperation;
import es.udc.tic.rnasa.service.queueServer.QueueFactory;
import es.udc.tic.rnasa.web.rest.dto.SshAccountDTO;
import es.udc.tic.rnasa.web.rest.mapper.SshAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing SshAccount.
 */
@Service
@Transactional
public class SshAccountService {

    private final Logger log = LoggerFactory.getLogger(SshAccountService.class);

    private final static int SIZE_PAGE = 10;

    @Inject
    private ApplicationService applicationService;

    @Inject
    private SshServerRepository sshServerRepository;

    @Inject
    private SshAccountRepository sshAccountRepository;

    @Inject
    private SshAccountMapper sshAccountMapper;

    @Inject
    private SshAccountSearchRepository sshAccountSearchRepository;

    @Inject
    private CompiledAppRepository compiledAppRepository;

    @Inject
    private SshProperties sshProperties;

    @Inject
    private QueueFactory queueFactory;


    private static final AesLengthConstants keyLenght = AesLengthConstants.AES_256;

    private Aes getAes(SshAccountDTO accountDTO, String salt) throws Exception {
        SshServer server = sshServerRepository.findOne(accountDTO.getBelongId());
        return new Aes(salt,
            sshProperties.getConfig().getSharedKey() + accountDTO.getUsername() + server.getId(),
            keyLenght);
    }

    public String getEncryptKey(SshAccountDTO accountDTO, String salt) throws Exception {
        return getAes(accountDTO, salt).encrypt(accountDTO.getKey());
    }

    public String getDecryptKey(SshAccountDTO accountDTO, String salt) throws Exception {
        return getAes(accountDTO, salt).decrypt(accountDTO.getKey());
    }

    public String getDecryptKey(SshAccount account) throws Exception {
        return getDecryptKey(sshAccountMapper.sshAccountToSshAccountDTO(account), account.getSalt());
    }

    /**
     * Save a sshAccount.
     *
     * @param sshAccountDTO the entity to save
     * @return the persisted entity
     */
    // This anotation doesn't work with AuthoritiesConstants.ADMIN
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public SshAccountDTO save(SshAccountDTO sshAccountDTO) {
        log.debug("Request to save SshAccount : {}", sshAccountDTO);

        if (sshAccountDTO.getNumJob() == null)
            sshAccountDTO.setNumJob(0);

        SshServer server = sshServerRepository.findOne(sshAccountDTO.getBelongId());

        String encryptKey = "";
        String salt = IntegerRandom.getSecureRandomInteger(50);
        try {
            encryptKey = getEncryptKey(sshAccountDTO, salt);
        } catch (Exception e) {
            log.debug("Cannot encrypt password {} for username {} for server <{}>", sshAccountDTO.getKey(), sshAccountDTO.getUsername(), server);
            return null;
        }

//log.debug("encryptKey.size() = {}", encryptKey.length());

        sshAccountDTO.setKey(encryptKey);

        SshAccount sshAccount = sshAccountMapper.sshAccountDTOToSshAccount(sshAccountDTO);

        sshAccount.setBelong(server);
        sshAccount.setSalt(salt);

        sshAccount = sshAccountRepository.save(sshAccount);
        SshAccountDTO result = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);
        sshAccountSearchRepository.save(sshAccount);
        return result;
    }

    /**
     *  Get all the sshAccounts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Page<SshAccount> findAll(Pageable pageable) {
        log.debug("Request to get all SshAccounts");
        Page<SshAccount> result = sshAccountRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one sshAccount by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public SshAccountDTO findOne(Long id) {
        log.debug("Request to get SshAccount : {}", id);
        SshAccount sshAccount = sshAccountRepository.findOne(id);
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);
        return sshAccountDTO;
    }

    /**
     *  Delete the  sshAccount by id.
     *
     *  @param id the id of the entity
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void delete(Long id) {
        log.debug("Request to delete SshAccount : {}", id);
        sshAccountRepository.delete(id);
        sshAccountSearchRepository.delete(id);
    }

    /**
     * Search for the sshAccount corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Page<SshAccount> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SshAccounts for query {}", query);
        return sshAccountSearchRepository.search(queryStringQuery(query), pageable);
    }

    ///
    /// custom services below this point
    ///


    public ConnectionServer openConnection(Long id, ChannelType channelType) throws TooMuchConnectionException, CannotOpenConnectionException {
        SshAccount sshAccount = sshAccountRepository.getOne(id);
        SshServer sshServer = sshAccount.getBelong();

        if (sshServer.getNumConnection() >= sshServer.getMaxConnection()) {
            throw new TooMuchConnectionException(
                String.format("Cannot established connection with %s because server %s has a lot of connections %d",
                    sshAccount.getUsername(), sshServer.getDnsname(), sshServer.getNumConnection()));
        }

        Aes aes = null;
        String password = "";
        try {
            aes = this.getAes(sshAccountMapper.sshAccountToSshAccountDTO(sshAccount), sshAccount.getSalt());
            password = aes.decrypt(sshAccount.getKey());
        } catch (Exception e) {
            log.debug("Cannot get AES");
            throw new CannotOpenConnectionException();
        }

        ConnectionServer connectionServer = null;
        if (channelType.equals(ChannelType.EXEC)) {
            connectionServer = new ExecConnectionServer(
                sshAccount.getUsername(), password,
                sshServer.getDnsname(),
                sshServer.getFingerprint(),
                sshServer.getPort(),
                sshProperties.getConfig().getTimeout()
            );
        } else if (channelType.equals(ChannelType.SFTP)) {
            connectionServer = new TransferConnectionServer(
                sshAccount.getUsername(), password,
                sshServer.getDnsname(),
                sshServer.getFingerprint(),
                sshServer.getPort(),
                sshProperties.getConfig().getTimeout()
            );
        }

        try {
            connectionServer.open();

            sshServer.setNumConnection(sshServer.getNumConnection() + 1);
            sshServerRepository.saveAndFlush(sshServer);
        } catch (CannotOpenConnectionException e) {
            throw e;
        }


        log.debug("Open connection {} on server {} with user {} ({}/{})", channelType.getType(), sshServer.getDnsname(), sshAccount.getUsername(), sshServer.getNumConnection(), sshServer.getMaxConnection());
        return connectionServer;
    }

    public void closeConnection(ConnectionServer connection) {
        String dnsname = connection.getHostname();

        SshServer sshServer = sshServerRepository.findOneByDnsname(dnsname);

        sshServer.setNumConnection(sshServer.getNumConnection() - 1);
        sshServerRepository.saveAndFlush(sshServer);

        log.debug("Close connection on server {} with user {} ({}/{})", sshServer.getDnsname(), connection.getUsername(), sshServer.getNumConnection(), sshServer.getMaxConnection());
    }

    public String launchCommand(ExecConnectionServer connection, String command) throws CannotExecConnectionException {
        return connection.exec(command);
    }

    public boolean exists(ExecConnectionServer connection, String pathname) throws CannotExecConnectionException {
        return connection.exec(String.format("test -e %s; echo $?", pathname)).trim().equals("0");
    }



    ////////////
    //// Cron
    ////////////
    private void setUser() {
        String loginname="system";
        String password="system";
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.USER));
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(loginname, password, authorities);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    /**
     * Service to update de number of jobs automatically
     */
    @Scheduled(initialDelay = 10000, fixedDelay = 60000) // Wait 10 seconds to start and repeat every minute
    public void updateNumJob() {
        log.debug("{} update num job", ZonedDateTime.now());

        setUser();

        int cnt = 0;
        Page<SshAccount> page = sshAccountRepository.findAllByActivatedIsTrueOrderById(new PageRequest(cnt, SIZE_PAGE));

        SshAccount sshAccount;
        Iterator<SshAccount> iterator;
        ExecConnectionServer exec = null;
        QueueOperation queueOperation;
        Long numJob;
        while (page.getNumberOfElements() > 0) {
            iterator = page.iterator();

            while (iterator.hasNext()) {
                sshAccount = iterator.next();
                log.debug("update num job for account {}", sshAccount.getUsername());
                try {
                    exec = (ExecConnectionServer) openConnection(sshAccount.getId(), ChannelType.EXEC);

                    queueOperation = queueFactory.getQueueOperation(sshAccount.getBelong().getUse().getName(), exec);

                    numJob = queueOperation.getCurrentJobs();
                    closeConnection(exec);
                    exec = null;

                    log.debug("{} jobs for {}", numJob, sshAccount.getUsername());

                    sshAccount.setNumJob(numJob.intValue());
                    //sshAccountRepository.save(sshAccount);
                } catch (Exception e) {
                    if (exec != null) {
                        closeConnection(exec);
                        exec = null;
                    }
                    log.debug("exception: {}", e.getMessage());

                }
                if (exec != null) {
                    closeConnection(exec);
                    exec = null;
                }
            }

            cnt++;
            page = sshAccountRepository.findAllByActivatedIsTrueOrderById(new PageRequest(cnt, SIZE_PAGE));
        }

        SecurityContextHolder.clearContext();

    }

}
