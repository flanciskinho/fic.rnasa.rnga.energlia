/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.repository.SshServerRepository;
import es.udc.tic.rnasa.repository.search.SshServerSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.SshServerDTO;
import es.udc.tic.rnasa.web.rest.mapper.SshServerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing SshServer.
 */
@Service
@Transactional
public class SshServerService {

    private final Logger log = LoggerFactory.getLogger(SshServerService.class);

    @Inject
    private SshServerRepository sshServerRepository;

    @Inject
    private SshServerMapper sshServerMapper;

    @Inject
    private SshServerSearchRepository sshServerSearchRepository;

    /**
     * Save a sshServer.
     *
     * @param sshServerDTO the entity to save
     * @return the persisted entity
     */
    // This anotation doesn't work with AuthoritiesConstants.ADMIN
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public SshServerDTO save(SshServerDTO sshServerDTO) {
        log.debug("Request to save SshServer : {}", sshServerDTO);
        SshServer sshServer = sshServerMapper.sshServerDTOToSshServer(sshServerDTO);

        if (sshServerDTO.getId() == null) {
            sshServer.setNumConnection(0);
        } else {
            SshServer tmp = sshServerRepository.findOne(sshServerDTO.getId());
            sshServer.setNumConnection(tmp != null? tmp.getNumConnection(): 0);
        }

        sshServer = sshServerRepository.save(sshServer);
        SshServerDTO result = sshServerMapper.sshServerToSshServerDTO(sshServer);
        sshServerSearchRepository.save(sshServer);
        return result;
    }

    /**
     *  Get all the sshServers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SshServer> findAll(Pageable pageable) {
        log.debug("Request to get all SshServers");
        Page<SshServer> result = sshServerRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get all active sshServers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SshServer> findAllByActiveIsTrue(Pageable pageable) {
        log.debug("Request to get all SshServers");
        Page<SshServer> result = sshServerRepository.findAllByActivatedIsTrue(pageable);
        return result;
    }



    /**
     *  Get one sshServer by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SshServerDTO findOne(Long id) {
        log.debug("Request to get SshServer : {}", id);
        SshServer sshServer = sshServerRepository.findOne(id);
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);
        return sshServerDTO;
    }

    /**
     *  Delete the  sshServer by id.
     *
     *  @param id the id of the entity
     */
    // This anotation doesn't work with AuthoritiesConstants.ADMIN
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void delete(Long id) {
        log.debug("Request to delete SshServer : {}", id);
        sshServerRepository.delete(id);
        sshServerSearchRepository.delete(id);
    }

    /**
     * Search for the sshServer corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SshServer> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SshServers for query {}", query);
        return sshServerSearchRepository.search(queryStringQuery(query), pageable);
    }
}
