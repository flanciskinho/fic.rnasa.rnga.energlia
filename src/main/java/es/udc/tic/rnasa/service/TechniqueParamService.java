/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.domain.TechniqueParam;
import es.udc.tic.rnasa.repository.TechniqueParamRepository;
import es.udc.tic.rnasa.repository.search.TechniqueParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.TechniqueParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.TechniqueParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TechniqueParam.
 */
@Service
@Transactional
public class TechniqueParamService {

    private final Logger log = LoggerFactory.getLogger(TechniqueParamService.class);
    
    @Inject
    private TechniqueParamRepository techniqueParamRepository;
    
    @Inject
    private TechniqueParamMapper techniqueParamMapper;
    
    @Inject
    private TechniqueParamSearchRepository techniqueParamSearchRepository;
    
    /**
     * Save a techniqueParam.
     * 
     * @param techniqueParamDTO the entity to save
     * @return the persisted entity
     */
    public TechniqueParamDTO save(TechniqueParamDTO techniqueParamDTO) {
        log.debug("Request to save TechniqueParam : {}", techniqueParamDTO);
        TechniqueParam techniqueParam = techniqueParamMapper.techniqueParamDTOToTechniqueParam(techniqueParamDTO);
        techniqueParam = techniqueParamRepository.save(techniqueParam);
        TechniqueParamDTO result = techniqueParamMapper.techniqueParamToTechniqueParamDTO(techniqueParam);
        techniqueParamSearchRepository.save(techniqueParam);
        return result;
    }

    /**
     *  Get all the techniqueParams.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<TechniqueParam> findAll(Pageable pageable) {
        log.debug("Request to get all TechniqueParams");
        Page<TechniqueParam> result = techniqueParamRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one techniqueParam by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public TechniqueParamDTO findOne(Long id) {
        log.debug("Request to get TechniqueParam : {}", id);
        TechniqueParam techniqueParam = techniqueParamRepository.findOne(id);
        TechniqueParamDTO techniqueParamDTO = techniqueParamMapper.techniqueParamToTechniqueParamDTO(techniqueParam);
        return techniqueParamDTO;
    }

    /**
     *  Delete the  techniqueParam by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TechniqueParam : {}", id);
        techniqueParamRepository.delete(id);
        techniqueParamSearchRepository.delete(id);
    }

    /**
     * Search for the techniqueParam corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TechniqueParam> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TechniqueParams for query {}", query);
        return techniqueParamSearchRepository.search(queryStringQuery(query), pageable);
    }
}
