/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.bashLaunch;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 3/8/16.
 */
@Component
public class BashLaunchFactory {

    @Inject
    @Qualifier("nqsBash")
    private BashLaunch nqsLaunch;

    public BashLaunch getBashLaunch(String type) {
        BashLaunch bashLaunch = null;

        if (type.equals("NQS")) {
            bashLaunch = nqsLaunch;
        } else
            new NotImplementedException("No implemented bash script for '" + type + "'");

        return bashLaunch;
    }

}
