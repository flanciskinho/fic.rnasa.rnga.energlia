/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.bashLaunch;

import es.udc.tic.rnasa.domain.CompiledApp;
import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.repository.CompiledAppRepository;
import es.udc.tic.rnasa.service.ApplicationService;
import es.udc.tic.rnasa.service.SimulationService;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 1/8/16.
 */
@Component
public class NqsBash implements BashLaunch {

    private final Logger log = LoggerFactory.getLogger(NqsBash.class);

    private static String SCRIPT_HEADER = "#!/bin/bash\n\n";

    // 1 - prog       - (%s/%d.out), (applicationService.getAppPathname(), compiledAppId)
    // 2 - problem    - ann_fixed_glia, angn_search_glia, mu_svm
    // 3 - config     - (%s/energlia%d.xml), (getScriptPathname(), simulationId), specificSimulationId)
    // 4 - log        - (%s/%d.xml), (getResultPathname(), simulationId), specificSimulationId)
    private static String SCRIPT_BODY = "%s %s --config=%s --out=%s";

    @Inject
    private CompiledAppRepository compiledAppRepository;

    @Inject
    private ApplicationService applicationService;
    @Inject
    private SimulationService simulationService;

    public String getScript(SpecificSimulation specificSimulation) {
        CompiledApp compiledApp = compiledAppRepository.findFirstByUseIdAndBelongActivatedIsTrueOrderByBelongIdDesc(specificSimulation.getBelong().getLaunch().getId());

        String exePath = String.format("%s/%d.out", applicationService.getAppPathname(), compiledApp.getId());
        String problem = getProblem(specificSimulation.getBelong());
        String config  = String.format("%s/%d.xml", String.format(simulationService.getConfigPathname(), specificSimulation.getBelong().getId()), specificSimulation.getId());
        String result  = String.format("%s/%d.xml", String.format(simulationService.getResultPathname(), specificSimulation.getBelong().getId()), specificSimulation.getId());

        return String.format("%s%s", SCRIPT_HEADER,
            String.format(SCRIPT_BODY, exePath, problem, config, result));
    }


    private String getProblem(Simulation simulation) {
        String tmp  = simulation.getProblem().getName();

        if (tmp.equals("AnnFixedGlia")) {
            return "ann_fixed_glia";
        } else if (tmp.equals("AngnSearchGlia")) {
            return "angn_search_glia";
        } else if (tmp.equals("nuSVM")) {
            return "mu_svm";
        } else {
            throw new NotImplementedException("Not implemented '" + tmp + "' problem");
        }
    }
}
