/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.connectionServer;

import com.jcraft.jsch.*;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotOpenConnectionException;
import org.slf4j.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by flanciskinho on 3/5/16.
 */
public abstract class ConnectionServer {

    protected final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());

    protected static final int BUFFER_SIZE = 1024*4;

    private String username;
    private String password;
    private String hostname;
    private String fingerprint;
    private int    port;

    private int timeout;


    private JSch jsch;

    private Session session;

    private File keyFile;


    public ConnectionServer(String username, String password, String hostname, String fingerprint,
                            int port, int timeout) {
        super();
        this.username = username;
        this.password = password;
        this.hostname = hostname;
        this.fingerprint = fingerprint;
        this.port     = port;
        this.timeout  = timeout;

        jsch = new JSch();
    }

    public abstract void open() throws CannotOpenConnectionException;

    protected Channel open(ChannelType type) throws JSchException {
        JSch jsch = this.getJsch();

        try {
            keyFile = File.createTempFile("key",".pem");
            keyFile.deleteOnExit();

            BufferedWriter bw = new BufferedWriter(new FileWriter(keyFile));
            bw.write(this.getPassword());
            bw.write("\n");
            bw.close();

            jsch.addIdentity(keyFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        session = jsch.getSession(this.getUsername(), this.getHostname(), this.getPort());

        session.setConfig("PreferredAuthentications", "publickey");
        session.setConfig("StrictHostKeyChecking", "ask");

        UserInfo ui = new ConnectionUserInfo(this.getFingerprint(), this.getJsch(), this.getSession());
        session.setUserInfo(ui);

        session.connect(this.getTimeout());

        return session.openChannel(type.getType());
    }

    public abstract void close() throws Exception;

    protected void deleteKeyFile() {
        try {
            jsch.removeIdentity(keyFile.getAbsolutePath());
        } catch (Exception e) {}
        keyFile.delete();
    }

    protected JSch getJsch() {
        return jsch;
    }

    protected Session getSession() {
        return session;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

}
