/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.connectionServer;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by flanciskinho on 3/5/16.
 */
public class ConnectionUserInfo implements UserInfo {

    private final Logger log = LoggerFactory.getLogger(ConnectionUserInfo.class);

    private String password;

    private String fingerprint;
    private JSch jsch;
    private Session session;

    public ConnectionUserInfo(String fingerprint, JSch jsch, Session session) {
        this.fingerprint = fingerprint.toLowerCase();
        this.jsch = jsch;
        this.session = session;
    }

    @Override
    public String getPassphrase() {
        // TODO Auto-generated method stub
        log.debug("getPassphrase");
        return "";
    }

    @Override
    public String getPassword() {
        // TODO Auto-generated method stub
        //return password;
        return null;
    }

    @Override
    public boolean promptPassphrase(String arg0) {
        // TODO Auto-generated method stub
        log.debug("promptPassphrase: "+ arg0);
        return false;
    }

    @Override
    public boolean promptPassword(String arg0) {
        // TODO Auto-generated method stub
        log.debug("promptPassword: "+ arg0);
        return true;
        //return false;
    }

    @Override
    public boolean promptYesNo(String arg0) {
        // TODO Auto-generated method stub
        log.debug("promptYesNo: "+ arg0);
        // Check if fingerprint is the right
        boolean result = session.getHostKey().getFingerPrint(jsch).equals(fingerprint);
        log.debug("fingerprint {}", result);
        return result;
        //return true;
        //return false;
    }

    @Override
    public void showMessage(String arg0) {
        // TODO Auto-generated method stub
        log.debug("showMessage: "+ arg0);
    }

}
