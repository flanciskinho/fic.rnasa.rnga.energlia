/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.connectionServer;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotOpenConnectionException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by flanciskinho on 3/5/16.
 */
public class ExecConnectionServer extends ConnectionServer {

    private Channel channel;

    public ExecConnectionServer(String username, String password,
                                String hostname, String fingerprint,
                                int port, int timeout) {
        super(username, password, hostname, fingerprint, port, timeout);
    }



    @Override
    public void open() throws CannotOpenConnectionException {
        try {
            channel = this.open(ChannelType.EXEC);
            channel.disconnect();
        } catch (JSchException e) {
            throw new CannotOpenConnectionException("Cannot open connection on "+getHostname()+" with user "+getUsername());
        }
    }

    public String exec(String command) throws CannotExecConnectionException {
        Channel channel = null;
        InputStream in;
        try {
            channel = getSession().openChannel(ChannelType.EXEC.getType());
        } catch (JSchException e) {
            throw new CannotExecConnectionException("Cannot open "+ChannelType.EXEC.getType()+" channel");
        }

        ((ChannelExec) channel).setCommand(command);

        try {
            channel.setInputStream(null);
            in = channel.getInputStream();
            ((ChannelExec) channel).setErrStream(System.err);
        } catch (IOException ioe) {
            throw new CannotExecConnectionException("Cannot match streams");
        }

        try {
            channel.connect();
        } catch (JSchException je) {
            throw new CannotExecConnectionException("Cannot connect channel");
        }

        StringBuffer buffer = new StringBuffer();
        byte[] tmp = new byte[BUFFER_SIZE];

        try {
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, BUFFER_SIZE);
                    if (i < 0)
                        break;
                    if (i == 0)
                        continue;

                    buffer.append(new String(tmp, 0, i));

                }
                if (channel.isClosed()) {
                    if (in.available() > 0)
                        continue;
                    log.debug("exit-status: " + channel.getExitStatus());
                    break;
                }
            }
        } catch (IOException ioe) {
            throw new CannotExecConnectionException("Cannot get answer");
        }

        channel.disconnect();

        return buffer.toString();
    }

    @Override
    public void close() {
        getSession().disconnect();
        super.deleteKeyFile();
    }

}
