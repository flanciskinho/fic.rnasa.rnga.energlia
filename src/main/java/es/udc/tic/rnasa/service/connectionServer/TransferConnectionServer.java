/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.connectionServer;

/**
 * Created by flanciskinho on 3/5/16.
 */
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotOpenConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotTransferConnectionException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


public class TransferConnectionServer extends ConnectionServer {

    private Channel channel;
    private ChannelSftp channelSftp;

    public TransferConnectionServer(String username, String password,
                                    String hostname, String fingerprint,
                                    int port, int timeout) {
        super(username, password, hostname, fingerprint, port, timeout);
    }



    @Override
    public void open() throws CannotOpenConnectionException {
        try {
            channel = this.open(ChannelType.SFTP);
            channel.connect();
            channelSftp = (ChannelSftp) channel;
        } catch (JSchException je) {
            throw new CannotOpenConnectionException("Cannot open connection on "+getHostname()+" with user "+getUsername());
        }
    }

    public void sendData(byte [] content, String dest) throws CannotTransferConnectionException {
        InputStream stream   = new ByteArrayInputStream(content);

        try {
            channelSftp.put(stream, dest);
        } catch (SftpException e) {
            throw new CannotTransferConnectionException("Cannot send file to folder "+dest+" on server "+getHostname()+" with user "+getUsername());
        }
    }

    public void sendData(String content, String dest) throws CannotTransferConnectionException {
        InputStream stream   = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));

        try {
            channelSftp.put(stream, dest);
        } catch (SftpException e) {
            throw new CannotTransferConnectionException("Cannot send file to folder "+dest+" on server "+getHostname()+" with user "+getUsername());
        }
    }

    public String receiveData(String source) throws CannotTransferConnectionException {

        byte[] buffer = new byte[BUFFER_SIZE];
        BufferedInputStream bis;
        StringBuffer tmp = new StringBuffer();

        try {
            bis = new BufferedInputStream(channelSftp.get(source));
        } catch (SftpException e) {
            throw new CannotTransferConnectionException("Cannot receive file from "+source+" on server "+getHostname()+" with user "+getUsername());
        }

        try {
            int readCount;
            while ((readCount = bis.read(buffer, 0, BUFFER_SIZE)) > 0) {
                tmp.append(new String(buffer, 0, readCount));
            }
            bis.close();
        } catch (IOException ioe) {
            throw new CannotTransferConnectionException("Cannot get answer");
        }

        return tmp.toString();
    }

    @Override
    public void close() {
        channelSftp.exit();
        channel.disconnect();
        this.getSession().disconnect();
        super.deleteKeyFile();
    }

}
