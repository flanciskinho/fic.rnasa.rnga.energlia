/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by flanciskinho on 26/7/16.
 */
public class NoDataException extends Exception{

    private final Logger log = LoggerFactory.getLogger(NoDataException.class);

    private String message;
    private Class tClass;

    public NoDataException(String message, Class tClass) {
        super(message);

        this.message = message;
        this.tClass = tClass;

        log.debug("{} caused a exception: '{}'", tClass.getName(),  message);
    }

    public NoDataException(String message) {
        super(message);

        this.message = message;

        log.debug("exception: '{}'",  message);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Class gettClass() {
        return tClass;
    }
}
