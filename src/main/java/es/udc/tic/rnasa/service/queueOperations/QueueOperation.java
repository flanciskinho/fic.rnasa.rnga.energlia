/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.queueOperations;

import es.udc.tic.rnasa.domain.ExecParam;
import es.udc.tic.rnasa.domain.SimulationStatusType;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by flanciskinho on 14/7/16.
 */
@Component
public interface QueueOperation {

    void setExecConnectionServer(ExecConnectionServer exec);

    long getCurrentJobs() throws CannotExecConnectionException;

    long launchJob(Integer procs, Long minit, Long memory, String arch, String script) throws CannotExecConnectionException;

    long launchJob(SpecificSimulation specificSimulation) throws CannotExecConnectionException;

    Map<Long, SimulationStatusType> getStatus() throws CannotExecConnectionException;


    default String getExecParameterValue(List<ExecParam> list, String parameter) {
        Optional<ExecParam> op = list.stream().filter(p -> p.getName().equals(parameter)).findFirst();

        return op.isPresent() ? op.get().getValue() : "";
    }
}
