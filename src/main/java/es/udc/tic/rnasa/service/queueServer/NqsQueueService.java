/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.queueServer;

import com.google.gson.Gson;
import es.udc.tic.rnasa.domain.ExecParam;
import es.udc.tic.rnasa.domain.SimulationStatusType;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.repository.ExecParamRepository;
import es.udc.tic.rnasa.service.SimulationStatusTypeService;
import es.udc.tic.rnasa.service.SpecificSimulationService;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.queueOperations.QueueOperation;
import es.udc.tic.rnasa.service.queueOperations.queueEntity.CurrentJobQueueEntity;
import es.udc.tic.rnasa.service.queueOperations.queueEntity.JobIdQueueEntity;
import es.udc.tic.rnasa.service.queueOperations.queueEntity.JobStatQueueEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by flanciskinho on 14/7/16.
 */
@Component
public class NqsQueueService implements QueueOperation {

    private final Logger log = LoggerFactory.getLogger(NqsQueueService.class);

    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;

    @Inject
    private SpecificSimulationService specificSimulationService;

    @Inject
    private ExecParamRepository execParamRepository;

    //private final static String COMMAND_CURRENT_JOBS = "qstat -U `whoami` | tail -n +3  | wc -l | awk '{print \"{\\\"jobs\\\":\\\"\"$1\"\\\"}\"}'";
    private final static String COMMAND_CURRENT_JOBS = "qstat | tail -n +3  | wc -l | awk '{print \"{\\\"jobs\\\":\\\"\"$1\"\\\"}\"}'";

    private final static String COMMAND_LAUNCH_JOB = "qsub -l num_proc=%s,s_rt=%s,s_vmem=%s,h_fsize=2G,arch=%s -cwd %s | sed -n 1p | awk '{print $3}' | sed -n '/^[[:digit:]]*$/p' | awk '{print \"{\\\"jobid\\\":\\\"\"$1\"\\\"}\"}'";

    private final static String COMMAND_STATUS_JOB = "qstat | tail -n +3 | awk 'BEGIN {print \"[{}\"}{print \", {\\\"jobid\\\":\"\"\\\"\"$1\"\\\", \\\"status\\\":\\\"\"$5\"\\\"}\"}END{print \"]\"}'";

    private final static double kb2gb = 1024.0 * 1024 * 1024;

    private ExecConnectionServer execConnectionServer;

    public void setExecConnectionServer(ExecConnectionServer exec) {
        this.execConnectionServer = exec;
    }


    private String launchCommand(String command) throws CannotExecConnectionException{
        log.debug("exec: {}", command);
        String json = execConnectionServer.exec(command);
        log.debug("receive: {}", json);
        return json;
    }

    public long getCurrentJobs() throws CannotExecConnectionException {
        String str = this.launchCommand(COMMAND_CURRENT_JOBS);

        Gson gson = new Gson();
        CurrentJobQueueEntity jobs = gson.fromJson(str, CurrentJobQueueEntity.class);

        return jobs.getJobs();
    }

    public String getCommandCurrentJobs() {
        return COMMAND_CURRENT_JOBS;
    }

    public long launchJob(Integer procs, Long minit, Long memory, String arch, String script) throws CannotExecConnectionException {
        String str = String.format(
            COMMAND_LAUNCH_JOB,
            procs.toString(),
            String.format("%d:%02d:00", minit/60, minit % 60),
            String.format("%dG", new Double(Math.ceil((1+memory)/(kb2gb))).longValue()),
            arch, script);

        String json = launchCommand(str);
//log.debug("send command {} getJson {}", str, json);

        return new Gson().fromJson(json, JobIdQueueEntity.class).getJobid();
    }

    public long launchJob(SpecificSimulation specificSimulation) throws CannotExecConnectionException {
        String filename = specificSimulationService.getBashPahtname(specificSimulation);

        List<ExecParam> execParams = execParamRepository.findAllByBelongId(specificSimulation.getBelong().getId());

        String proc   = this.getExecParameterValue(execParams, "proc");
        String minit  = this.getExecParameterValue(execParams, "time");
        String memory = this.getExecParameterValue(execParams, "memory");
        String arch   = this.getExecParameterValue(execParams, "arch");

        return launchJob(new Integer(proc), new Long(minit), new Long(memory), arch, filename);
    }

    public Map<Long, SimulationStatusType> getStatus() throws CannotExecConnectionException {
        String json = launchCommand(COMMAND_STATUS_JOB);

        JobStatQueueEntity[] array = new Gson().fromJson(json, JobStatQueueEntity[].class);

        Map<Long, SimulationStatusType> map = new HashMap<>();

        for (JobStatQueueEntity stat : array) {
            if (stat == null || stat.isEmpty())
                continue;

            switch (stat.getStatus().charAt(0)) {
                case 'r':
                    map.put(stat.getJobid(), simulationStatusTypeService.getRUNNING());
                    break;
                default:
                    map.put(stat.getJobid(), simulationStatusTypeService.getWAITING_ON_QUEUE());
            }
        }

        return map;
    }
}
