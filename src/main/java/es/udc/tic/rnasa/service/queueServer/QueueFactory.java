/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.queueServer;

import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.queueOperations.QueueOperation;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 18/7/16.
 */
@Component
public class QueueFactory {

    @Inject @Qualifier("nqsQueueService")
    private QueueOperation nqsQueueService;

    public QueueOperation getQueueOperation(String queueType, ExecConnectionServer exec) {
        QueueOperation queueOperation = null;

        if (queueType.equals("NQS")) {
            queueOperation = nqsQueueService;
        } else {
            throw new NotImplementedException("No implemented operation for queue '"+queueType+"'");
        }

        queueOperation.setExecConnectionServer(exec);

        return queueOperation;
    }
}
