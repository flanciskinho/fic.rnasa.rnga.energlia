/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readIndividualService;

import es.udc.tic.rnasa.service.exceptions.NoXmlAttributeException;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by flanciskinho on 22/4/16.
 */
public interface ReadIndividual {

    default List<Map<Integer,Boolean>> getGenoBinary() throws NoXmlAttributeException {
        return new ArrayList<>();
    }

    default List<Map<Integer,Number>> getGenoShort() throws NoXmlAttributeException {
        return new ArrayList<>();
    }

    default List<Map<Integer,Number>> getGenoFloat() throws NoXmlAttributeException {
        return new ArrayList<>();
    }
}
