/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readIndividualService;

import es.udc.tic.rnasa.service.exceptions.NoXmlAttributeException;
import es.udc.tic.rnasa.service.util.XmlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by flanciskinho on 22/4/16.
 */
public class ReadIndividualBinaryv1_0 implements ReadIndividual {

    private final Logger log = LoggerFactory.getLogger(ReadIndividualBinaryv1_0.class);

    private static final String XPATH_INDIVIDUAL = "/population/individual";

    private static final String ELEMENT_BOOL = "genobool";

    private static final String ATT_ID = "id";
    private static final String ATT_VALUE = "value";

    private List<Map<Integer, Boolean>> genoBinary;

    private Document file;

    public ReadIndividualBinaryv1_0(Document file) throws NoXmlAttributeException {
        this.file = file;

        genoBinary = getGeno();
    }

    @Override
    public List<Map<Integer, Boolean>> getGenoBinary() {
        return genoBinary;
    }

    private List<Map<Integer, Boolean>> getGeno() throws NoXmlAttributeException {
        ArrayList<Map<Integer,Boolean>> individuals = new ArrayList<>();

        NodeList nl = XmlUtil.getNodeList(file, XPATH_INDIVIDUAL);
        NodeList childs;
        Element element;
        HashMap<Integer, Boolean> map;

        int cnt, tmp;
        for (cnt = 0; cnt < nl.getLength(); cnt++) {
            childs = nl.item(cnt).getChildNodes();

            map = new HashMap<>();
            for (tmp = 0; tmp < childs.getLength(); tmp++) {
                if (!childs.item(tmp).getNodeName().equals(ELEMENT_BOOL))
                    continue;

                element = (Element) childs.item(tmp);

                if (element.getAttribute(ATT_ID).isEmpty())
                    throw new NoXmlAttributeException(ATT_ID);

                if (element.getAttribute(ATT_VALUE).isEmpty())
                    throw new NoXmlAttributeException(ATT_VALUE);

                map.put(new Integer(element.getAttribute(ATT_ID)), new Boolean(element.getAttribute(ATT_VALUE).equals("1")?true:false));
            }
            individuals.add(map);
        }

        return individuals;
    }

}
