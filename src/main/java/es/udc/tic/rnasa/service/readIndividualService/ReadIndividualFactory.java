/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readIndividualService;

import es.udc.tic.rnasa.service.exceptions.DifferentEncodesException;
import es.udc.tic.rnasa.service.exceptions.DifferentVersionsException;
import es.udc.tic.rnasa.service.exceptions.NoXmlAttributeException;
import es.udc.tic.rnasa.service.util.XmlUtil;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import java.util.List;

/**
 * Created by flanciskinho on 22/4/16.
 */
public class ReadIndividualFactory {

    private final Logger log = LoggerFactory.getLogger(ReadIndividualFactory.class);

    private Document file;

    private static final String XPATH_VERSION = "/population/individual/@version";
    private static final String ITEM_VERSION  = "version";

    private static final String XPATH_ENCODE  = "/population/individual/@encode";
    private static final String ITEM_ENCODE   = "encode";

    private static final String REAL = "2";
    private static final String BINARY = "1";
    private static final String V1_0 = "1.0";

    private static final String REAL_V1_0 = REAL+V1_0;
    private static final String BINARY_V1_0 = BINARY+V1_0;

    private ReadIndividual readIndividual;

    public ReadIndividualFactory(Document file) throws DifferentVersionsException, DifferentEncodesException, NoXmlAttributeException {
        this.file = file;

        List<String> list = XmlUtil.getNodeAttribute(file, XPATH_VERSION, ITEM_VERSION);
        for (String str: list) {
            if (!str.equals(list.get(0)))
                throw new DifferentVersionsException();
        }
        String version = list.isEmpty()? "": list.get(0);

        list = XmlUtil.getNodeAttribute(file, XPATH_ENCODE, ITEM_ENCODE);
        for (String str: list) {
            if (!str.equals(list.get(0)))
                throw new DifferentEncodesException();
        }
        String encode  = list.isEmpty()? "": list.get(0);;

        readIndividual = getReadIndividual(version, encode);
    }

    public ReadIndividual getReadIndividual() {
        return readIndividual;
    }

    private ReadIndividual getReadIndividual(String version, String encode) throws NoXmlAttributeException{
        String key = String.format("%s%s", encode  == null?"":encode, version == null?"":version).toUpperCase();

        if (key.equals(REAL_V1_0)) {
            return new ReadIndividualRealv1_0(file);
        } else if (key.equals(BINARY_V1_0)){
            return new ReadIndividualBinaryv1_0(file);
        } else {
            throw new NotImplementedException("No implemented the way to read population for encode='"+encode+"' and version='"+version+"'");
        }

    }

}
