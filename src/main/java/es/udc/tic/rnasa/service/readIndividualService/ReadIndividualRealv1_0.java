/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readIndividualService;

import es.udc.tic.rnasa.service.exceptions.NoXmlAttributeException;
import es.udc.tic.rnasa.service.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by flanciskinho on 22/4/16.
 */
public class ReadIndividualRealv1_0 implements ReadIndividual {

    private Document file;

    private static final String XPATH_INDIVIDUAL = "/population/individual";

    private static final String ELEMENT_SHORT = "genoshort";
    private static final String ELEMENT_FLOAT = "genofloat";

    private static final String ATT_ID = "id";
    private static final String ATT_VALUE = "value";

    private List<Map<Integer, Number>> genoShort;
    private List<Map<Integer, Number>> genoFloat;

    public ReadIndividualRealv1_0(Document file) throws NoXmlAttributeException {
        this.file = file;

        genoShort = getGeno(ELEMENT_SHORT, Short.class);
        genoFloat = getGeno(ELEMENT_FLOAT, Float.class);


    }

    @Override
    public List<Map<Integer, Number>> getGenoShort() {
        return genoShort;
    }

    @Override
    public List<Map<Integer, Number>> getGenoFloat() {
        return genoFloat;
    }

    /*
    public List<Map<Long, Short>> getGenoShort() {
        ArrayList<Map<Long,Short>> individuals = new ArrayList<>();

        NodeList nl = XmlUtil.getNodeList(file, XPATH_INDIVIDUAL);
        NodeList childs;
        Element element;
        HashMap<Long, Short> map;

        int cnt, tmp;
        for (cnt = 0; cnt < nl.getLength(); cnt++) {
            childs = nl.item(cnt).getChildNodes();
            map = new HashMap<>();
            for (tmp = 0; tmp < childs.getLength(); cnt++) {
                element = (Element) childs.item(tmp);
                if (!element.getNodeName().equals(ELEMENT_SHORT))
                    continue;
                map.put(new Long(element.getAttribute(ATT_ID)), new Short(element.getAttribute(ATT_VALUE)));
            }
            individuals.add(map);
        }

        return individuals;
    }
    */

    private List<Map<Integer, Number>> getGeno(String element_type, Class cl) throws NoXmlAttributeException {
        ArrayList<Map<Integer, Number>> individuals = new ArrayList<>();

        NodeList nl = XmlUtil.getNodeList(file, XPATH_INDIVIDUAL);
        NodeList childs;
        Element element;
        HashMap<Integer, Number> map;

        try {
            int cnt, tmp;
            for (cnt = 0; cnt < nl.getLength(); cnt++) {
                childs = nl.item(cnt).getChildNodes();
                map = new HashMap<>();
                for (tmp = 0; tmp < childs.getLength(); tmp++) {
                    if (!childs.item(tmp).getNodeName().equals(element_type))
                        continue;

                    element = (Element) childs.item(tmp);

                    if (element.getAttribute(ATT_ID).isEmpty())
                        throw new NoXmlAttributeException(ATT_ID);

                    if (element.getAttribute(ATT_VALUE).isEmpty())
                        throw new NoXmlAttributeException(ATT_VALUE);

                    map.put(new Integer(element.getAttribute(ATT_ID)),
                        (Number) cl.getConstructor(String.class).newInstance(new Object[] {element.getAttribute(ATT_VALUE)})
                    );
                }
                individuals.add(map);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return individuals;
    }
}
