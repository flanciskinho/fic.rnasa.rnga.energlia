/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readLogService;

import es.udc.tic.rnasa.domain.LogRecord;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.domain.TechniqueParam;
import es.udc.tic.rnasa.service.util.XmlUtil;
import es.udc.tic.rnasa.type.NeuralNetwork.ActivationFunction;
import es.udc.tic.rnasa.type.NeuralNetwork.GliaAlgorithm;
import es.udc.tic.rnasa.type.NeuralNetwork.OutputNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by flanciskinho on 10/8/16.
 */
public class ReadLogAngnv1_0 implements ReadLog {

    private final Logger log = LoggerFactory.getLogger(ReadLogAngnv1_0.class);

    private static final String XPATH_LOG = "/logs/log";
    private static final String NODE_ANGN = "angn";

    private static final String NODE_OUTPUT = "output";
    private static final String ATT_OUPTUT_TYPE = "type";

    private static final String NODE_CLASSIFY = "classify";
    private static final String ATT_STEP = "step";
    private static final String ATT_CLASS = "class";

    private static final String NODE_COMPETITIVE = "competitive";
    private static final String ATT_YES = "yes";
    private static final String ATT_NO = "no";

    private static final String NODE_NEURON = "neuron";

    private static final String NODE_LAYERS = "layers";
    private static final String NODE_LAYER  = "layer";

    private static final String NODE_WEIGHTS = "weights";
    private static final String NODE_WEIGHT  = "weight";

    private static final String ATT_ID = "id";
    private static final String ATT_VALUE = "value";

    private static final String ATT_LAYER = "layer";
    private static final String ATT_ACT_FUNCTION = "activation_function";
    private static final String ATT_INCREASE = "per_increase";
    private static final String ATT_DECREASE = "per_decrease";
    private static final String ATT_ALGORITHM = "algorithm";


    private static final String ATT_MS = "ms";
    private static final String ATT_GENERATION = "generation";
    private static final String ATT_TRAIN = "train";
    private static final String ATT_VALIDATION = "validation";
    private static final String ATT_TEST = "test";

    private static final String ATT_TYPE_NETWORK = "type";
    private static final String ATT_NUM_LAYER = "num_layers";
    private static final String ATT_ITERATION = "iterations";
    private static final String ATT_ACTIVATION = "activations";

    private List<LogRecord> logRecords;
    private List<TechniqueParam> techniqueParams;
    private SpecificSimulation specificSimulation;

    @Override
    public List<LogRecord> getLogRecords() {
        return logRecords;
    }

    @Override
    public List<TechniqueParam> getTechniqueParams() {
        return techniqueParams;
    }

    public ReadLogAngnv1_0(Document document, SpecificSimulation specificSimulation) {
        logRecords = new ArrayList();
        techniqueParams = new ArrayList();
        this.specificSimulation = specificSimulation;
        this.specificSimulation = specificSimulation;

        NodeList nodes = XmlUtil.getNodeList(document, XPATH_LOG);
        NodeList models;

        int tmp, cnt = 0;
        Node node = nodes.item(cnt);
        LogRecord logRecord;
        for (; node != null; cnt++, node = nodes.item(cnt)) {
            logRecord = createLogRecord((Element) node);
            logRecords.add(logRecord);

            models = ((Element) node).getElementsByTagName(NODE_ANGN);

            for (tmp = 0; models.item(tmp) != null; tmp++) {
                if (models.item(tmp).getNodeName().equals(NODE_ANGN))
                    createTechniqueParams((Element) models.item(tmp), logRecord);
            }
        }

    }

    private void createTechniqueParams(Element e, LogRecord logRecord) {


        Map<String, String> map = new HashMap();

        map.put("type", e.getAttribute(ATT_TYPE_NETWORK));
        map.put("layer", e.getAttribute(ATT_NUM_LAYER));
        map.put("iteration", e.getAttribute(ATT_ITERATION));
        map.put("activation", e.getAttribute(ATT_ACTIVATION));


        NodeList nodeList, tmpList;
        Node node, tmp;
        Element element;
        int cnt, aux;
        String id;

        // Neurons
        nodeList = e.getElementsByTagName(NODE_NEURON);
        for (cnt = 0, node = nodeList.item(cnt); node != null; cnt++, node = nodeList.item(cnt)) {
            if (!node.getNodeName().equals(NODE_NEURON))
                continue;

            element = (Element) node;
            id = element.getAttribute(ATT_ID);

            map.put(String.format("neuron[%s].increase", id), element.getAttribute(ATT_LAYER));
            map.put(String.format("neuron[%s].activationFunction", id), ActivationFunction.getById(new Integer(element.getAttribute(ATT_ACT_FUNCTION))).getName());
            map.put(String.format("neuron[%s].gliaAlgorithm", id), GliaAlgorithm.getById(new Integer(element.getAttribute(ATT_ALGORITHM))).getName());
            map.put(String.format("neuron[%s].increase", id), element.getAttribute(ATT_INCREASE));
            map.put(String.format("neuron[%s].decrease", id), element.getAttribute(ATT_DECREASE));
        }

        // OUTPUT TYPE
        nodeList = e.getElementsByTagName(NODE_OUTPUT);
        for (cnt = 0, node = nodeList.item(cnt); node != null; cnt++, node = nodeList.item(cnt)) {
            if (!node.getNodeName().equals(NODE_OUTPUT))
                continue;

            element = (Element) node;
            OutputNetwork outputNetwork = OutputNetwork.getById(new Integer(element.getAttribute(ATT_OUPTUT_TYPE)));
            map.put("output", outputNetwork.getName());
            switch (outputNetwork) {
                case OUTPUT_TYPE_CLASSIFY:
                    tmpList = ((Element) node).getElementsByTagName(NODE_CLASSIFY);
                    for (aux = 0, tmp = tmpList.item(aux); tmp != null; aux++, tmp = tmpList.item(aux) ) {
                        if (!tmp.getNodeName().equals(NODE_CLASSIFY))
                            continue;

                        map.put("output.step", ((Element) tmp).getAttribute(ATT_STEP));
                        map.put("output.class", ((Element) tmp).getAttribute(ATT_CLASS));
                    }
                    break;
                case OUTPUT_TYPE_COMPETITIVE:
                    tmpList = ((Element) node).getElementsByTagName(NODE_COMPETITIVE);
                    for (aux = 0, tmp = tmpList.item(aux); tmp != null; aux++, tmp = tmpList.item(aux) ) {
                        if (!tmp.getNodeName().equals(NODE_COMPETITIVE))
                            continue;

                        map.put("output.yes", ((Element) tmp).getAttribute(ATT_YES));
                        map.put("output.no", ((Element) tmp).getAttribute(ATT_NO));
                    }
                    break;
            }
        }

        // LAYERS
        nodeList = e.getElementsByTagName(NODE_LAYERS);
        for (cnt = 0, node = nodeList.item(cnt); node != null; cnt++, node = nodeList.item(cnt)) {
            if (!node.getNodeName().equals(NODE_LAYERS))
                continue;

            tmpList = ((Element) node).getElementsByTagName(NODE_LAYER);
            for (aux = 0, tmp = tmpList.item(aux); tmp != null; aux++, tmp = tmpList.item(aux) ) {
                if (!tmp.getNodeName().equals(NODE_LAYER))
                    continue;

                element = (Element) tmp;
                map.put(
                    String.format("layer[%s].neuron", element.getAttribute(ATT_ID)),
                    element.getAttribute(ATT_VALUE)
                );
            }
        }

        // WEIGHTS
        nodeList = e.getElementsByTagName(NODE_WEIGHTS);
        for (cnt = 0, node = nodeList.item(cnt); node != null; cnt++, node = nodeList.item(cnt)) {
            if (!node.getNodeName().equals(NODE_WEIGHTS))
                continue;

            tmpList = ((Element) node).getElementsByTagName(NODE_WEIGHT);
            for (aux = 0, tmp = tmpList.item(aux); tmp != null; aux++, tmp = tmpList.item(aux) ) {
                if (!tmp.getNodeName().equals(NODE_WEIGHT))
                    continue;

                element = (Element) tmp;
                map.put(
                    String.format("weight[%s]", element.getAttribute(ATT_ID)),
                    element.getAttribute(ATT_VALUE)
                );
            }
        }




        TechniqueParam techniqueParam;
        for (Map.Entry<String, String> entry: map.entrySet()) {
            techniqueParam = new TechniqueParam();
            techniqueParam.setBelong(logRecord);
            techniqueParam.setName(entry.getKey());
            techniqueParam.setValue(entry.getValue());

            techniqueParams.add(techniqueParam);
        }
    }

    private LogRecord createLogRecord(Element e) {
        LogRecord logRecord = new LogRecord();
        logRecord.setBelong(specificSimulation);

        logRecord.setIndexRecord(new Long(e.getAttribute(ATT_GENERATION)));
        logRecord.setTimestamp(new Double(e.getAttribute(ATT_MS)).longValue());
        logRecord.setErrorTrain(new Double(e.getAttribute(ATT_TRAIN)));

        String tmp;

        tmp = e.getAttribute(ATT_VALIDATION);
        logRecord.setErrorValidation((tmp == null || tmp.trim().isEmpty())? null: new Double(tmp));

        tmp = e.getAttribute(ATT_TEST);
        logRecord.setErrorTest((tmp == null || tmp.trim().isEmpty())? null: new Double(tmp));

        return logRecord;
    }
}
