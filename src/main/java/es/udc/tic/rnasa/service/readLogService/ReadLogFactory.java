/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readLogService;

import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.service.util.XmlUtil;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.util.Locale;

/**
 * Created by flanciskinho on 9/8/16.
 */
public class ReadLogFactory {

    private final Logger log = LoggerFactory.getLogger(ReadLogFactory.class);

    private static final String XPATH_VERSION     = "/logs/@version";

    private static final String XPATH_NODE_VERSION = "/logs/log[1]//@version";

    private static final String XPATH_LOG = "/logs/log";

    private static final String XPATH_SVM  = "/logs/log/svm_model";
    private static final String XPATH_ANGN = "/logs/log/angn";


    private static final String ANGN_NODENAME = "angn";
    private static final String SVM_NODENAME = "svm_model";

    private Document document;
    private String logVersion = null;
    private String nodeVersion = null;
    private SpecificSimulation specificSimulation;

    private ReadLog readLog = null;

    public Document getDocument() {
        return document;
    }

    public String getLogVersion() {
        return logVersion;
    }

    public String getNodeVersion() {
        return nodeVersion;
    }

    public ReadLog getReadLog() {
        return readLog;
    }

    public ReadLogFactory(Document document, SpecificSimulation specificSimulation) {
        Locale tmp = Locale.getDefault();
        Locale.setDefault(Locale.ENGLISH);

        this.document = document;
        this.specificSimulation = specificSimulation;
        NodeList nodeList;

        logVersion = XmlUtil.getRootAttribute(document, XPATH_VERSION).trim();
        nodeVersion = XmlUtil.getRootAttribute(document, XPATH_NODE_VERSION).trim();

        nodeList = XmlUtil.getNodeList(document, XPATH_ANGN);
        if (nodeList.item(0) == null) {
            nodeList = XmlUtil.getNodeList(document, XPATH_SVM);
        }
        if (nodeList.item(0) == null) {
            return;
        }

        String nodeName = nodeList.item(0).getNodeName().trim();

        log.debug("Read log for type:{} version:{}", nodeName, nodeVersion);
        if (nodeName.equals(ANGN_NODENAME)) {
            readLog = getReadLogAngn(nodeName, nodeVersion);
        } else if (nodeName.equals(SVM_NODENAME)) {
            readLog = getReadLogSvm(nodeName, nodeVersion);
        } else {
            throw new NotImplementedException("Not implement how to read logs for "+nodeName+" problem");
        }


        Locale.setDefault(tmp);
    }

    private ReadLog getReadLogSvm(String nodeName, String nodeVersion) {
        if (nodeVersion.equals("1.0")) {
            return new ReadLogSvmv1_0(document, specificSimulation);
        } else {
            throw new NotImplementedException("Not implement the way to read svm log for version " + nodeVersion);
        }
    }

    private ReadLog getReadLogAngn(String nodeName, String nodeVersion) {
        if (nodeVersion.equals("1.0")) {
            return new ReadLogAngnv1_0(document, specificSimulation);
        } else {
            throw new NotImplementedException("Not implement the way to read angn log for version " + nodeVersion);
        }
    }
}
