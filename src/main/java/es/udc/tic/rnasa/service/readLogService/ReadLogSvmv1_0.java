/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readLogService;

import es.udc.tic.rnasa.domain.LogRecord;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.domain.TechniqueParam;
import es.udc.tic.rnasa.service.util.XmlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flanciskinho on 10/8/16.
 */
public class ReadLogSvmv1_0 implements ReadLog {

    private final Logger log = LoggerFactory.getLogger(ReadLogSvmv1_0.class);

    private List<LogRecord> logRecords;
    private List<TechniqueParam> techniqueParams;
    private SpecificSimulation specificSimulation;

    private static final String XPATH_LOG = "/logs/log";
    private static final String NODE_SVM = "svm_model";

    private static final String ATT_MS = "ms";
    private static final String ATT_GENERATION = "generation";
    private static final String ATT_TRAIN = "train";
    private static final String ATT_VALIDATION = "validation";
    private static final String ATT_TEST = "test";

    @Override
    public List<LogRecord> getLogRecords() {
        return logRecords;
    }

    @Override
    public List<TechniqueParam> getTechniqueParams() {
        return techniqueParams;
    }

    public ReadLogSvmv1_0(Document document, SpecificSimulation specificSimulation) {
        logRecords = new ArrayList();
        techniqueParams = new ArrayList();
        this.specificSimulation = specificSimulation;

        NodeList nodes = XmlUtil.getNodeList(document, XPATH_LOG);
        NodeList models;

        int tmp, cnt = 0;
        Node node = nodes.item(cnt);
        LogRecord logRecord;
        for (; node != null; cnt++, node = nodes.item(cnt)) {
            log.debug("Node name {}", node.getNodeName());
            logRecord = createLogRecord((Element) node);
            logRecords.add(logRecord);

            models = ((Element) node).getElementsByTagName(NODE_SVM);

            for (tmp = 0; models.item(tmp) != null; tmp++) {
                if (models.item(tmp).getNodeName().equals(NODE_SVM))
                    createTechniqueParams((Element) models.item(tmp), logRecord);
            }
        }

    }

    private void createTechniqueParams(Element e, LogRecord logRecord) {
        String svmModel = e.getTextContent();
        String svmParam = svmModel.substring(0, svmModel.indexOf("\nSV"));
        String svmSv    = svmModel.substring(svmModel.indexOf("SV\n")+"SV\n".length());


        TechniqueParam techniqueParam;
        String tmp[];

        for (String line: svmParam.split("\n")) {
            tmp = line.split(" ");
            if (tmp.length != 2) {
                log.debug("Bad split for {}", line);
                continue;
            }

            techniqueParam = new TechniqueParam();
            techniqueParam.setBelong(logRecord);
            techniqueParam.setName(tmp[0]);
            techniqueParam.setValue(tmp[1]);

            techniqueParams.add(techniqueParam);
        }

        int row = 0;
        int col;
        for (String line: svmSv.split("\n")) {
            col = 0;
            for (String v: line.split(" ")) {
                techniqueParam = new TechniqueParam();
                techniqueParam.setBelong(logRecord);
                techniqueParam.setName(String.format("SV[%d][%d]", row, col));
                techniqueParam.setValue(v);

                techniqueParams.add(techniqueParam);
                col++;
            }

            row++;
        }


    }

    private LogRecord createLogRecord(Element e) {
        LogRecord logRecord = new LogRecord();
        logRecord.setBelong(specificSimulation);

        logRecord.setIndexRecord(new Long(e.getAttribute(ATT_GENERATION)));
        logRecord.setTimestamp(new Double(e.getAttribute(ATT_MS)).longValue());
        logRecord.setErrorTrain(new Double(e.getAttribute(ATT_TRAIN)));

        String tmp;

        tmp = e.getAttribute(ATT_VALIDATION);
        logRecord.setErrorValidation((tmp == null || tmp.trim().isEmpty())? null: new Double(tmp));

        tmp = e.getAttribute(ATT_TEST);
        logRecord.setErrorTest((tmp == null || tmp.trim().isEmpty())? null: new Double(tmp));

        return logRecord;
    }

}
