/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readPopulationService;

import org.w3c.dom.Document;

import java.util.List;
import java.util.Map;

/**
 * Created by flanciskinho on 22/4/16.
 */
public interface ReadPopulation {

    Integer getSizeGenotype();

    /**
     * The key is the identifier of SizeType (binary, short, float)
     * and the value is the size for this datatype
     * @return
     */
    Map<Long, Integer> getSizeParamMap();

    List<Document> getXmlFiles();
}
