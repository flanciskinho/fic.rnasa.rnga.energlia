/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readPopulationService;

import es.udc.tic.rnasa.service.exceptions.DifferentEncodesException;
import es.udc.tic.rnasa.service.exceptions.DifferentGenosizeException;
import es.udc.tic.rnasa.service.exceptions.DifferentNumberOfIndividualsException;
import es.udc.tic.rnasa.service.exceptions.DifferentVersionsException;
import es.udc.tic.rnasa.service.util.ParseUtil;
import es.udc.tic.rnasa.service.util.XmlUtil;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by flanciskinho on 22/4/16.
 */
public class ReadPopulationFactory {

    private final Logger log = LoggerFactory.getLogger(ReadPopulationFactory.class);

    private static final String REAL = "2";
    private static final String BINARY = "1";
    private static final String V1_0 = "1.0";

    private static final String REAL_V1_0 = REAL+V1_0;
    private static final String BINARY_V1_0 = BINARY+V1_0;

    private static final String XPATH_VERSION     = "/population/@version";
    private static final String XPATH_ENCODE      = "/population/@encode";
    private static final String XPATH_INDIVIDUALS = "/population/@num_individuals";

    private ReadPopulation readPopulation;
    private List<Document> xmlFiles;


    private Integer numCombination, numIndividuals;

    public Integer getNumCombination() {
        return numCombination;
    }

    public Integer getNumIndividuals() {
        return numIndividuals;
    }

    public ReadPopulation getReadPopulation() {
        return readPopulation;
    }

    public List<Document> getXmlFiles() {
        return xmlFiles;
    }

    public ReadPopulationFactory(List<byte []> files) throws DifferentNumberOfIndividualsException, DifferentEncodesException, DifferentVersionsException, ParseException, es.udc.tic.rnasa.service.exceptions.ParseException, DifferentGenosizeException {
        xmlFiles = new ArrayList<>(files.size());

        for (byte[] tmp: files) {
            xmlFiles.add(ParseUtil.byteToXmlDocument(tmp));
        }


        xmlFiles.get(0);

        String version = XmlUtil.getRootAttribute(xmlFiles.get(0), XPATH_VERSION);
        String encode = XmlUtil.getRootAttribute(xmlFiles.get(0), XPATH_ENCODE);
        String individuals = XmlUtil.getRootAttribute(xmlFiles.get(0), XPATH_INDIVIDUALS);


        for (int cnt = 1; cnt < xmlFiles.size(); cnt++) {
            if (!version.equals(XmlUtil.getRootAttribute(xmlFiles.get(cnt), XPATH_VERSION)))
                throw new DifferentVersionsException();
            if (!encode.equals(XmlUtil.getRootAttribute(xmlFiles.get(cnt), XPATH_ENCODE)))
                throw new DifferentEncodesException();
            if (!individuals.equals(XmlUtil.getRootAttribute(xmlFiles.get(cnt), XPATH_INDIVIDUALS)))
                throw new DifferentNumberOfIndividualsException();
        }

        numCombination = files.size();
        numIndividuals = new Integer(individuals);

        readPopulation = getReadPopulation(version, encode, numIndividuals);

    }

    private ReadPopulation getReadPopulation(String version, String encode, int numIndividuals) throws DifferentGenosizeException {
        String key = String.format("%s%s", encode  == null?"":encode, version == null?"":version).toUpperCase();

        if (key.equals(REAL_V1_0)) {
            return new ReadPopulationRealv1_0(xmlFiles, numIndividuals);
        } else if (key.equals(BINARY_V1_0)){
            return new ReadPopulationBinaryv1_0(xmlFiles, numIndividuals);
        } else {
            throw new NotImplementedException("No implemented the way to read population for encode='"+encode+"' and version='"+version+"'");
        }
    }
}
