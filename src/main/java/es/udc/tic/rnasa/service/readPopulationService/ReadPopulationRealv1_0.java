/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.readPopulationService;

import es.udc.tic.rnasa.service.SizeTypeService;
import es.udc.tic.rnasa.service.exceptions.DifferentGenosizeException;
import es.udc.tic.rnasa.service.util.XmlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by flanciskinho on 22/4/16.
 */
public class ReadPopulationRealv1_0 implements ReadPopulation {

    private final Logger log = LoggerFactory.getLogger(ReadPopulationRealv1_0.class);

    private static final String XPATH_SIZE_SHORT = "/population/@s_genoshort";
    private static final String XPATH_SIZE_FLOAT = "/population/@s_genofloat";

    private List<Document> xmlFiles;

    private Integer sizeShort, sizeFloat;

    private Integer numIndividual;

    @Inject
    private SizeTypeService sizeTypeService;

    @Override
    public Integer getSizeGenotype() {
        return (sizeShort+ sizeFloat)/numIndividual;
    }

    @Override
    public Map<Long, Integer> getSizeParamMap() {
        Map<Long, Integer> map = new HashMap<>(2);

        map.put(sizeTypeService.getREAL().getId(),  sizeFloat/numIndividual);
        map.put(sizeTypeService.getSHORT().getId(), sizeShort/numIndividual);

        return map;
    }

    @Override
    public List<Document> getXmlFiles() {
        return xmlFiles;
    }


    public ReadPopulationRealv1_0(List<Document> xmlFiles, Integer numIndividual) throws DifferentGenosizeException {
        this.xmlFiles = xmlFiles;
        this.numIndividual = numIndividual;

        String strSizeShort = XmlUtil.getRootAttribute(xmlFiles.get(0), XPATH_SIZE_SHORT);
        String strSizeFloat = XmlUtil.getRootAttribute(xmlFiles.get(0), XPATH_SIZE_FLOAT);

        if (strSizeFloat.isEmpty())
            throw new DifferentGenosizeException("expression '"+XPATH_SIZE_FLOAT+"' does not return a value");
        if (strSizeShort.isEmpty())
            throw new DifferentGenosizeException("expression '"+XPATH_SIZE_SHORT+"' does not return a value");

        for (int cnt = 1; cnt < xmlFiles.size(); cnt++) {

            if (!strSizeShort.equals(XmlUtil.getRootAttribute(xmlFiles.get(cnt), XPATH_SIZE_SHORT))) {
                throw new DifferentGenosizeException("short");
            }
            if (!strSizeFloat.equals(XmlUtil.getRootAttribute(xmlFiles.get(cnt), XPATH_SIZE_FLOAT))) {
                throw new DifferentGenosizeException("float");
            }

        }

        sizeFloat = new Integer(strSizeFloat);
        sizeShort = new Integer(strSizeShort);
    }

}
