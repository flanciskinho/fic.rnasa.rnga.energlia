/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.util;

import es.udc.tic.rnasa.config.JHipsterProperties;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.security.SecurityUtils;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

/**
 * Created by flanciskinho on 12/4/16.
 */
public class CheckPermission {

    private final Logger log = LoggerFactory.getLogger(CheckPermission.class);

    private UserRepository userRepository;

    public CheckPermission(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private String getLogin(Long userId) {
        return userRepository.findOne(userId).getLogin();
    }

    public boolean checkPermission(HasOwnerToPrivacy entity) throws HttpExceptionDetails {
        log.debug("Check privacy for '{}'", entity);
        log.debug("user authenticated '{}'", SecurityUtils.getCurrentUserLogin());

        if (!SecurityUtils.isAuthenticated()) {
            log.debug("No user authenticated");
            throw new HttpExceptionDetails(HttpStatus.UNAUTHORIZED, "no user authenticated", entity.getClass());
            //return false;
        }

        if (entity == null) {
            log.error("No entity to check privacy");
            throw new HttpExceptionDetails(HttpStatus.NOT_FOUND, "no entity");
        }

        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            if (entity.getOwnerId() == null) {
                log.error("Entity without user {}", entity);
                throw new HttpExceptionDetails(HttpStatus.FORBIDDEN, "entity without owner", entity.getClass());
            }

            if (!SecurityUtils.getCurrentUserLogin().equals(getLogin(entity.getOwnerId()))) {
                log.warn("Current user aren't the entity owner. Current user {}. Owner {}. Entity {}", SecurityUtils.getCurrentUserLogin(), entity.getOwnerId(), entity);
                throw new HttpExceptionDetails(HttpStatus.FORBIDDEN, "Current user doesn't own the entity", entity.getClass());
            }
        }


        return true;

    }
}
