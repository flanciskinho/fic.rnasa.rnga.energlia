/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.util;

import es.udc.tic.rnasa.service.exceptions.ParseException;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by flanciskinho on 22/4/16.
 */
public class ParseUtil {

    /**
     * Convert raw file allocate in <i>document</i> to xml document to parse
     * @param document raw file
     * @return org.w3c.dom.Document
     * @throws ParseException
     */
    public static Document byteToXmlDocument(byte[] document) throws ParseException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(new ByteArrayInputStream(document));
        } catch (Exception e) {
            throw new ParseException();
        }
    }

    public static Document StringToXmlDocument(String document) throws ParseException {
        return byteToXmlDocument(document.getBytes());
    }

    public static byte[] getXmlDocumentToByte(Document xmlFile) throws ParseException {
        Source source = new DOMSource(xmlFile);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Result result = new StreamResult(out);
        TransformerFactory factory = TransformerFactory.newInstance();

        try {
            Transformer transformer = factory.newTransformer();
            transformer.transform(source, result);
            byte[] xml = out.toByteArray();
            return xml;
        } catch (Exception e) {
            throw new ParseException();
        }
    }
}
