/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by flanciskinho on 22/4/16.
 */
public class XmlUtil {

    private static final Logger log = LoggerFactory.getLogger(XmlUtil.class);

    public static String getRootAttribute(Document document, String expr) {
        try {
            return XPathFactory.newInstance().newXPath().compile(expr).evaluate(document, XPathConstants.STRING).toString();
        } catch (XPathExpressionException xpee) {
            log.debug(xpee.getMessage());
            log.debug(xpee.getLocalizedMessage());
            return "";
        }
    }

    public static List<String> getNodeAttribute(Document document, String expr, String item) {
        List<String> list = new ArrayList<>();
        try {
            NodeList nl = (NodeList) XPathFactory.newInstance().newXPath().compile(expr).evaluate(document, XPathConstants.NODESET);
            for (int cnt = 0; cnt < nl.getLength(); cnt++) {
                list.add( nl.item(cnt).getNodeValue() );
            }

        } catch (XPathExpressionException xpee) {
            System.err.println(xpee.getMessage());
            System.err.println(xpee.getLocalizedMessage());
        }

        return list;
    }

    public static NodeList getNodeList(Document document, String expr) {
        NodeList nl = null;

        try {
            nl = (NodeList) XPathFactory.newInstance().newXPath().compile(expr).evaluate(document, XPathConstants.NODESET);
        } catch (XPathExpressionException xpee) {
            System.err.println(xpee.getMessage());
            System.err.println(xpee.getLocalizedMessage());
        }

        return nl;
    }
}
