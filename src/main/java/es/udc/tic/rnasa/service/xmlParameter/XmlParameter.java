/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter;

import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.service.xmlParameter.datasetParameter.DatasetParameterFactory;
import es.udc.tic.rnasa.service.xmlParameter.gaParameter.GaParameterFactory;
import es.udc.tic.rnasa.service.xmlParameter.xmlParameter.ProblemParameterFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Locale;

/**
 * Created by flanciskinho on 28/7/16.
 */
@Component
public class XmlParameter {

    @Inject
    private DatasetParameterFactory datasetParameterFactory;

    @Inject
    private GaParameterFactory gaParameterFactory;

    @Inject
    private ProblemParameterFactory problemParameterFactory;

    public String getXmlParameter(SpecificSimulation specificSimulation) {
        Locale tmp = Locale.getDefault();
        Locale.setDefault(Locale.ENGLISH);

        String dataset = datasetParameterFactory.getLastDatasetParameter().getDatasetNode(specificSimulation);
        String ga      = gaParameterFactory.getLastGaParameter().getGaNode(specificSimulation);
        String problem = problemParameterFactory.getProblemParameter(specificSimulation.getBelong().getProblem().getName())
            .getLastProblemParameter().getProblemParameter(specificSimulation.getBelong());

        String xml = String.format("<parameters>%s%s%s</parameters>", dataset, ga, problem);
        Locale.setDefault(tmp);

        return xml;
    }
}
