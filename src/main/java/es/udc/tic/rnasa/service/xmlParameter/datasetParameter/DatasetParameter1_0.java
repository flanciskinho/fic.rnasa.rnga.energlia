/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.datasetParameter;

import es.udc.tic.rnasa.domain.DatasetCombination;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.service.DatasetCombinationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 27/7/16.
 */
@Component

public class DatasetParameter1_0 implements DatasetParameter {

    @Inject
    private DatasetCombinationService datasetCombinationService;

    private final Logger log = LoggerFactory.getLogger(DatasetParameter1_0.class);

    private final static String DATASET_NODE_1_0 = "<dataset version='1.0'>%s</dataset>";
    private final static String TRAIN_NODE = "<train inputs='%s' outputs='%s'/>";
    private final static String VALIDATION_NODE = "<validation inputs='%s' outputs='%s'/>";
    private final static String TEST_NODE = "<test inputs='%s' outputs='%s'/>";

    public String getDatasetNode(SpecificSimulation specificSimulation) {


        DatasetCombination datasetCombination = specificSimulation.getUseData();
        Long datasetId = datasetCombination.getBelong().getId();

        StringBuffer buffer = new StringBuffer();

        String mainPath = String.format(datasetCombinationService.getDatasetPathname(), datasetId);

        buffer.append(String.format(TRAIN_NODE,
            String.format("%s/%d.train.in", mainPath, datasetCombination.getId()),
            String.format("%s/%d.train.out", mainPath, datasetCombination.getId())));

        if (datasetCombination.getValidationfilein() != null) {
            buffer.append(String.format(VALIDATION_NODE,
                String.format("%s/%d.validation.in", mainPath, datasetCombination.getId()),
                String.format("%s/%d.validation.out", mainPath, datasetCombination.getId())));

            if (datasetCombination.getTestfilein() != null) {
                buffer.append(String.format(TEST_NODE,
                    String.format("%s/%d.test.in", mainPath, datasetCombination.getId()),
                    String.format("%s/%d.test.out", mainPath, datasetCombination.getId())));

            }
        }

        String xmlNode = String.format(DATASET_NODE_1_0, buffer.toString());

        //log.debug("datasetNode for SpecificSimulation {} = {}", specificSimulation, xmlNode);
        return xmlNode;

    }
}
