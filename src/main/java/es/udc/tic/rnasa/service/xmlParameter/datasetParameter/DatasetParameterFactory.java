/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.datasetParameter;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 27/7/16.
 */
@Component
public class DatasetParameterFactory {

    private final Logger log = LoggerFactory.getLogger(DatasetParameterFactory.class);

    private static final String LAST_VERSION = "1.0";

    @Inject @Qualifier("datasetParameter1_0")
    private DatasetParameter datasetParameter1_0;

    public DatasetParameter getDatasetParameter(String version) {
        DatasetParameter datasetParameter = null;

        if (version.equals("1.0")) {
            datasetParameter = datasetParameter1_0;
        } else
            throw new NotImplementedException("No implemented the way to create dataset node for version='"+version+"'");

        return datasetParameter;
    }

    public DatasetParameter getLastDatasetParameter() {
        return getDatasetParameter(LAST_VERSION);
    }

}
