/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.gaParameter;

import es.udc.tic.rnasa.domain.GaParam;
import es.udc.tic.rnasa.domain.PopulationParam;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.type.GeneticAlgorithm.*;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;


/**
 * Created by flanciskinho on 27/7/16.
 */
public interface GaParameter {
    Logger log = LoggerFactory.getLogger(GaParameter.class);

    String getGaNode(SpecificSimulation specificSimulation);



    default String getGeneticParameterValue(List<GaParam> list, String parameter) {
        Optional<GaParam> op =  list.stream().filter(p -> p.getName().equals(parameter)).findFirst();

        return op.isPresent()? op.get().getValue(): "";
    }

    default String getParameterValue(List<PopulationParam> list, String parameter) {
        Optional<PopulationParam> op =  list.stream().filter(p -> p.getName().equals(parameter)).findFirst();

        return op.isPresent()? op.get().getValue(): "";
    }



    default int getSelectionAlgorithm(String name) {
        SelectAlgorithm tmp = SelectAlgorithm.getByName(name);
        if (tmp == null) {
            String str = "Not implemented selection algorithm with name '" + name + "'";
            log.warn(str);
            throw new NotImplementedException(str);
        }

        return tmp.getId();
    }

    default int getCrossoverAlgorithm(String name) {
        CrossoverAlgorithm tmp = CrossoverAlgorithm.getByName(name);
        if (tmp == null) {
            String str = "Not implemented crossover algorithm with name '" + name + "'";
            log.warn(str);
            throw new NotImplementedException(str);
        }

        return tmp.getId();
    }

    default int getMutationAlgorithm(String name) {
        MutationAlgorithm tmp = MutationAlgorithm.getByName(name);
        if (tmp == null) {
            String str = "Not implemented mutation algorithm with name '" + name + "'";
            log.warn(str);
            throw new NotImplementedException(str);
        }

        return tmp.getId();
    }

    default int getReplaceAlgorithm(String name) {
        ReplaceAlgorithm tmp = ReplaceAlgorithm.getByName(name);
        if (tmp == null) {
            String str = "Not implemented replace algorithm with name '" + name + "'";
            log.warn(str);
            throw new NotImplementedException(str);
        }

        return tmp.getId();
    }

    default int getFitnessAlgorithm(String name) {
        FitnessAlgorithm tmp = FitnessAlgorithm.getByName(name);
        if (tmp == null) {
            String str = "Not implemented fitness algorithm with name '" + name + "'";
            throw new NotImplementedException(str);
        }

        return tmp.getId();
    }

}
