/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.gaParameter;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.GaParamRepository;
import es.udc.tic.rnasa.repository.PopulationCombinationOrderRepository;
import es.udc.tic.rnasa.repository.PopulationParamRepository;
import es.udc.tic.rnasa.service.PopulationCombinationService;
import es.udc.tic.rnasa.type.GeneticAlgorithm.CrossoverAlgorithm;
import es.udc.tic.rnasa.type.GeneticAlgorithm.SelectAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by flanciskinho on 27/7/16.
 */
@Component
public class GaParameter1_0 implements GaParameter {

    private final Logger log = LoggerFactory.getLogger(GaParameter1_0.class);

    private static final String GA_NODE = "<ga version='1.0' generations='%d' goal='%f'>%s</ga>";
    private static final String SS =
        "<steadystate version='1.0'>" +
            "<population pathname='%s' encode='2'>" +
                "%s" +
            "</population>" +
        "</steadystate>";

    private static final String SELECT_MOTHER   = "<select_mother algorithm='%d'%s/>";
    private static final String SELECT_FATHER   = "<select_father algorithm='%d'%s/>";
    private static final String SELECT_FUNCTION = "<select_function algorithm='%d'%s/>";
    private static final String SELECT_PARAM_WINDOW = " window='%d'";
    private static final String SELECT_PARAM_WINDOW_PROB = " window='%d' prob='%f'";

    private static final String CROSSOVER = "<crossover algorithm='%d' rate='%f' %s/>";
    private static final String CROSS_SBX = " mu='%f'";

    private static final String MUTATION = "<mutation algorithm='%d' rate='%f'/>";

    private static final String REPLACE = "<replace algorithm='%d'/>";


    private static final String CC =
        "<cooperativecoevolutivesteadystate version='1.0' num_population='%d'>" +
            "<selectfitness best='%d' random='%d' function='%d'>%s</selectfitness>" +
            "<fitness algorithm='%d'/>" +
            "%s" +
        "</cooperativecoevolutivesteadystate>";

    private static final String CC_POPULATION = "<population id='%d' pathname='%s' encode='2'>%s</population>";



    @Inject
    private GaParamRepository gaParamRepository;

    @Inject
    private PopulationCombinationOrderRepository populationCombinationOrderRepository;

    @Inject
    private PopulationParamRepository populationParamRepository;

    @Inject
    private PopulationCombinationService populationCombinationService;

    private String getGaNodeCooperative(SpecificSimulation specificSimulation, int generation, float goal) {
        long simulationId = specificSimulation.getBelong().getId();

        List<PopulationCombinationOrder> populationCombinationOrders = populationCombinationOrderRepository.findAllByBelongIdOrderByIndexOrder(specificSimulation.getId());

        int index;
        List<PopulationParam> populationParams;
        StringBuffer buffer = new StringBuffer();
        for (PopulationCombinationOrder pco : populationCombinationOrders) {
            index = pco.getIndexOrder();
            populationParams = populationParamRepository.findAllByBelongBelongBelongIdAndBelongIndexOrder(simulationId, index);

            buffer.append(
                String.format(CC_POPULATION, index,
                    //String.format(populationCombinationService.getPopulationPathname(), pco.getBelong().getId()),
                    String.format("%s/%d.xml",
                        String.format(populationCombinationService.getPopulationPathname(), pco.getUse().getBelong().getId()),
                        pco.getUse().getId()),
                    getGeneticOperations(populationParams)
                )
            );
//log.debug("getGeneticOperations {}", getGeneticOperations(populationParams));
        }

        List<GaParam> gaParams = gaParamRepository.findAllByBelongBelongId(simulationId);

        int best   = new Integer(getGeneticParameterValue(gaParams, "fitness.best.size"));
        int random = new Integer(getGeneticParameterValue(gaParams, "fitness.random.size"));
        int select = new Integer(getGeneticParameterValue(gaParams, "fitness.select.size"));

        String selectNode = "";
        String tmp = "";
        if (select > 0) {
            int selectAlgorithm = getSelectionAlgorithm(getGeneticParameterValue(gaParams, "fitness.select.algorithm"));
            if (selectAlgorithm == 3) {
                tmp = String.format(SELECT_PARAM_WINDOW, new Integer(getGeneticParameterValue(gaParams, "fitness.select.window")));
            } else if (selectAlgorithm == 4) {
                tmp = String.format(SELECT_PARAM_WINDOW_PROB,
                    new Integer(getGeneticParameterValue(gaParams, "fitness.select.window")),
                    new Float(getGeneticParameterValue(gaParams, "fitness.select.probability")));
            }
            selectNode = String.format(SELECT_FUNCTION, selectAlgorithm, tmp);
        }

        tmp = String.format(CC,
            populationCombinationOrders.size(), best, random, select, selectNode,
            getFitnessAlgorithm(getGeneticParameterValue(gaParams, "fitness.algorithm")),
            buffer.toString());

        return String.format(GA_NODE, generation, goal, tmp);
    }


    private String getGaNodeSteadyState(SpecificSimulation specificSimulation, int generation, float goal) {
        long simulationId = specificSimulation.getBelong().getId();

        List<PopulationCombinationOrder> populationCombinationOrders = populationCombinationOrderRepository.findAllByBelongIdOrderByIndexOrder(specificSimulation.getId());
        PopulationCombination populationCombination = populationCombinationOrders.get(0).getUse();

        List<PopulationParam> populationParams = populationParamRepository.findAllByBelongBelongBelongId(simulationId);
        String geneticOperationsNode = getGeneticOperations(populationParams);

        String tmp = String.format(SS,
            String.format("%s/%d.xml",
                String.format(populationCombinationService.getPopulationPathname(), populationCombination.getBelong().getId()),
                populationCombination.getId()),
            geneticOperationsNode);

        return String.format(GA_NODE, generation, goal, tmp);
    }

    public String getGaNode(SpecificSimulation specificSimulation) {
        ProblemType problemType = specificSimulation.getBelong().getProblem();

        long simulationId = specificSimulation.getBelong().getId();
        int generation = 1;
        float goal = 0f;

        List<GaParam> gaParams = gaParamRepository.findAllByBelongBelongId(simulationId);
        for (GaParam gp: gaParams) {
            if (gp.equals("generation"))
                generation = new Integer(gp.getValue());
            else if (gp.equals("goal"))
                goal = new Float(gp.getValue());
        }

        switch (problemType.getNumPopulation()) {
            case 0:
                return "";
            case 1:
                return getGaNodeSteadyState(specificSimulation, generation, goal);
            default:
                return getGaNodeCooperative(specificSimulation, generation, goal);
        }
    }


    private String getGeneticOperations(List<PopulationParam> populationParams) {
        int motherAlgorithm    = getSelectionAlgorithm(getParameterValue(populationParams, "mother.algorithm"));
        int fatherAlgorithm    = getSelectionAlgorithm(getParameterValue(populationParams, "father.algorithm"));
        int crossoverAlgorithm = getCrossoverAlgorithm(getParameterValue(populationParams, "crossover.algorithm"));
        int mutationAlgorithm  = getMutationAlgorithm(getParameterValue(populationParams, "mutation.algorithm"));
        int replaceAlgorithm   = getReplaceAlgorithm(getParameterValue(populationParams, "replace.algorithm"));

        float crossoverRate = new Float(getParameterValue(populationParams, "crossover.rate"));
        float mutationRate  = new Float(getParameterValue(populationParams, "mutation.rate"));

        StringBuffer buffer = new StringBuffer();
        String tmp;

        tmp = "";
        if (motherAlgorithm == SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getId()) {
            tmp = String.format(SELECT_PARAM_WINDOW, new Integer(getParameterValue(populationParams, "mother.window")));
        } else if (motherAlgorithm == SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getId()) {
            tmp = String.format(SELECT_PARAM_WINDOW_PROB,
                new Integer(getParameterValue(populationParams, "mother.window")),
                new Float(getParameterValue(populationParams, "mother.probability")));
        }
        buffer.append(String.format(SELECT_MOTHER, motherAlgorithm, tmp));

        tmp = "";
        if (fatherAlgorithm == SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getId()) {
//log.debug(">> {}", populationParams);
//log.debug(">>>> {}", getParameterValue(populationParams, "father.window"));
            tmp = String.format(SELECT_PARAM_WINDOW, new Integer(getParameterValue(populationParams, "father.window")));
        } else if (fatherAlgorithm == SelectAlgorithm.SELECT_TOURNAMENT_PROBABILISTIC.getId()) {
            tmp = String.format(SELECT_PARAM_WINDOW_PROB,
                new Integer(getParameterValue(populationParams, "father.window")),
                new Float(getParameterValue(populationParams, "father.probability")));
        }
        buffer.append(String.format(SELECT_FATHER, fatherAlgorithm, tmp));

        tmp = "";
        if (crossoverAlgorithm == CrossoverAlgorithm.REAL_SBX.getId()) {
            tmp = String.format(CROSS_SBX, new Float(getParameterValue(populationParams, "crossover.mu")));
        }
        buffer.append(String.format(CROSSOVER, crossoverAlgorithm, crossoverRate, tmp));

        buffer.append(String.format(MUTATION, mutationAlgorithm, mutationRate));

        buffer.append(String.format(REPLACE, replaceAlgorithm));


        return buffer.toString();
    }



}
