/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.gaParameter;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 27/7/16.
 */
@Component
public class GaParameterFactory {
    private final Logger log = LoggerFactory.getLogger(GaParameterFactory.class);

    private static final String LAST_VERSION = "1.0";

    @Inject @Qualifier("gaParameter1_0")
    private GaParameter gaParameter1_0;

    public GaParameter getGaParameter(String version) {
        GaParameter gaParameter = null;

        if (version.equals("1.0")) {
            gaParameter = gaParameter1_0;
        } else
            throw new NotImplementedException("No implemented the way to create ga node for version='"+version+"'");

        return gaParameter;
    }

    public GaParameter getLastGaParameter() {
        return getGaParameter(LAST_VERSION);
    }
}
