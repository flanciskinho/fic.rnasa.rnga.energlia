/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.xmlParameter;

import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.domain.SimulationParam;
import es.udc.tic.rnasa.type.NeuralNetwork.ActivationFunction;
import es.udc.tic.rnasa.type.NeuralNetwork.GliaAlgorithm;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

/**
 * Created by flanciskinho on 28/7/16.
 */
public interface ProblemParameter {

    Logger logger = LoggerFactory.getLogger(ProblemFactory.class);

    String getProblemParameter(Simulation simulation);

    default String getParameterValue(List<SimulationParam> list, String parameter) {
        Optional<SimulationParam> sp =  list.stream().filter(p -> p.getName().equals(parameter)).findFirst();

        return sp.isPresent()? sp.get().getValue(): "";
    }


    default int getOutErrorType(Simulation simulation) {
        String tmp = simulation.getMinimize().getName();
        if (tmp.equals("MSE")) {
            return 0;
        } else if (tmp.equals("MAE")) {
            return 1;
        } else
            throw new NotImplementedException("No implemented angn to use ='"+tmp+"' error");
    }



    default int getOutputType(Simulation simulation) {
        String tmp = simulation.getUse().getAnalyze().getName();

        if (tmp.equals("CLASSIFICATION")) {
            return 0;
        } else if (tmp.equals("REGRESSION")) {
            return 1;
        } else
            throw new NotImplementedException("No implemented angn to use ='"+tmp+"' data");
    }

    default int getActivationFunction(String name) {
        ActivationFunction tmp = ActivationFunction.getByName(name);
        if (tmp == null) {
            String str = "Not implemented activation function with name '" + name + "'";
            logger.warn(str);
            throw new org.apache.commons.lang3.NotImplementedException(str);
        }

        return tmp.getId();
    }

    default int getGliaAlgorithm(String name) {
        GliaAlgorithm tmp = GliaAlgorithm.getByName(name);
        if (tmp == null) {
            String str = "Not implemented glia algorithm with name '" + name + "'";
            logger.warn(str);
            throw new org.apache.commons.lang3.NotImplementedException(str);
        }

        return tmp.getId();
    }
}
