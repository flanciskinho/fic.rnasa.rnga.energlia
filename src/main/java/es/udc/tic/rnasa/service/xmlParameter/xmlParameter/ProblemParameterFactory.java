/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.xmlParameter;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 28/7/16.
 */
@Component
public class ProblemParameterFactory {

    @Inject @Qualifier("nuSvmParameterFactory")
    private ProblemFactory nuSvmParameterFactory;

    @Inject @Qualifier("angnSearchGliaParameterFactory")
    private ProblemFactory angnSearchGliaParameterFactory;

    @Inject @Qualifier("annFixedGliaParameterFactory")
    private ProblemFactory annFixedGliaParameterFactory;

    public ProblemFactory getProblemParameter(String problemName) {
        ProblemFactory problemFactory = null;

        if (problemName.equals("nuSVM")) {
            problemFactory = nuSvmParameterFactory;
        } else if (problemName.equals("AnnFixedGlia")) {
            problemFactory = annFixedGliaParameterFactory;
        } else if (problemName.equals("AngnSearchGlia")) {
            problemFactory = angnSearchGliaParameterFactory;
        } else
            throw new NotImplementedException("No implemented the way to create parameter node for problem='"+problemName+"'");

        return problemFactory;
    }
}
