/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.xmlParameter.angnSearchGlia;

import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.domain.SimulationParam;
import es.udc.tic.rnasa.repository.SimulationParamRepository;
import es.udc.tic.rnasa.service.xmlParameter.xmlParameter.ProblemParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by flanciskinho on 28/7/16.
 */
@Component
public class AngnSearchGliaParameter1_0 implements ProblemParameter {

    private final Logger log = LoggerFactory.getLogger(AngnSearchGliaParameter1_0.class);

    @Inject
    private SimulationParamRepository simulationParamRepository;

    private static final String PROBLEM_NODE =
        "<problem>" +
            "<angnSearchGlia version='1.0' num_neurons='%d' num_layers='%d'>" +
                "%s%s%s%s%s" +
            "</angnSearchGlia>" +
        "</problem>";

    private static final String LAYER_PATTERN  = "<layer id='%d' value='%d'/>";
    private static final String NEURON_PATTERN = "<neuron id='%d' activation_function='%d'/>";

    private static final String OUTERROR = "<outerror type='%d'/>";

    private static final String OUTPUT = "<output type='%d'>%s</output>";
    private static final String CLASSIFY_OUTPUT = "<classify step='%f' classes='%d'/>";

    private static final String CONSTRAINTS =
        "<constraints>" +
            "<weight min='%f' max='%f'/>" +
            "<same_glia_algorithm value='%d'/>" +
            "<iteration min='%f' max='%f'/>" +
            "<activation min='%f' max='%f'/>" +
            "<per_increase min='%d' max='%d'/>" +
            "<per_decrease min='%d' max='%d'/>" +
            "<per_decrease_greather_than_per_increase value='%d'/>" +
        "</constraints>";

    public String getProblemParameter(Simulation simulation) {
        Long simulationId = simulation.getId();


        List<SimulationParam> simulationParams = simulationParamRepository.findAllByBelongId(simulationId);

        String tmp;
        int cnt, auxNeuron, aux;
        int numLayers = new Integer(getParameterValue(simulationParams, "layer"));
        int numNeurons = 0;

        StringBuffer layerData = new StringBuffer();
        for (cnt = 0; cnt < numLayers; cnt++) {
            tmp = getParameterValue(simulationParams, String.format("layer[%d].neuron", cnt));
            layerData.append(String.format(LAYER_PATTERN, cnt, new Integer(tmp)));
            numNeurons += new Integer(tmp);
        }

        StringBuffer neuronData = new StringBuffer();
        for (aux = cnt = 0; cnt < numLayers; cnt++) {

            for (auxNeuron = 0; auxNeuron < new Integer(getParameterValue(simulationParams, String.format("layer[%d].neuron", cnt))) ; auxNeuron++) {
                neuronData.append(String.format(NEURON_PATTERN, aux,
                    getActivationFunction(
                        getParameterValue(simulationParams, String.format("layer[%d].activationFunction", cnt))
                    )));
                aux++;
            }
        }

        String outErrorData = String.format(OUTERROR, getOutErrorType(simulation));

        int outputType = getOutputType(simulation);
        tmp = "";
        if (outputType == 0) {
            tmp = String.format(CLASSIFY_OUTPUT, 0.5f, simulation.getUse().getNumClass());
        }
        String outputData = String.format(OUTPUT, outputType, tmp);

        String constraints = String.format(CONSTRAINTS,
            new Float(getParameterValue(simulationParams, "constraint.weight.min")), new Float(getParameterValue(simulationParams, "constraint.weight.max")),
            new Boolean(getParameterValue(simulationParams, "constraint.sameGlia")).booleanValue()?1:0,
            new Float(getParameterValue(simulationParams, "constraint.iteration.min")), new Float(getParameterValue(simulationParams, "constraint.iteration.max")),
            new Float(getParameterValue(simulationParams, "constraint.activation.min")), new Float(getParameterValue(simulationParams, "constraint.activation.max")),
            (int) (new Float(getParameterValue(simulationParams, "constraint.increase.min"))*10), (int) (new Float(getParameterValue(simulationParams, "constraint.increase.max"))*10),
            (int) (new Float(getParameterValue(simulationParams, "constraint.decrease.min"))*10), (int) (new Float(getParameterValue(simulationParams, "constraint.decrease.max"))*10),
            new Boolean(getParameterValue(simulationParams, "constraint.decreaseGtIncrease")).booleanValue()?1:0
            );

        return String.format(PROBLEM_NODE, numNeurons, numLayers,
            layerData.toString(), neuronData.toString(),
            outputData, outErrorData, constraints
        );
    }



}
