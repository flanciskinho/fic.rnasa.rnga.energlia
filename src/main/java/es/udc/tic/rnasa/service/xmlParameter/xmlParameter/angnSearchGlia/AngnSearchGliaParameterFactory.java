/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.xmlParameter.angnSearchGlia;

import es.udc.tic.rnasa.service.xmlParameter.xmlParameter.ProblemFactory;
import es.udc.tic.rnasa.service.xmlParameter.xmlParameter.ProblemParameter;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by flanciskinho on 28/7/16.
 */
@Component
public class AngnSearchGliaParameterFactory implements ProblemFactory {

    private final Logger log = LoggerFactory.getLogger(AngnSearchGliaParameterFactory.class);

    private static final String LAST_VERSION = "1.0";

    @Inject @Qualifier("angnSearchGliaParameter1_0")
    private ProblemParameter angnSearchGliaParameter1_0;

    public ProblemParameter getProblemParameter(String version) {
        ProblemParameter problemParameter = null;

        if (version.equals("1.0")) {
            problemParameter = angnSearchGliaParameter1_0;
        } else
            throw new NotImplementedException("No implemented the way to create angn-search-glia parameter node for version='"+version+"'");

        return problemParameter;
    }

    public ProblemParameter getLastProblemParameter() {
        return getProblemParameter(LAST_VERSION);
    }


}
