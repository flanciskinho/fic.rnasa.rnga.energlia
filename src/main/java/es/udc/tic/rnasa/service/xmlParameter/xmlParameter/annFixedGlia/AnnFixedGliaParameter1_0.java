/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.xmlParameter.annFixedGlia;

import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.domain.SimulationParam;
import es.udc.tic.rnasa.repository.SimulationParamRepository;
import es.udc.tic.rnasa.service.xmlParameter.xmlParameter.ProblemParameter;
import es.udc.tic.rnasa.service.xmlParameter.xmlParameter.angnSearchGlia.AngnSearchGliaParameter1_0;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by flanciskinho on 28/7/16.
 */
@Component
public class AnnFixedGliaParameter1_0 implements ProblemParameter {

    private final Logger log = LoggerFactory.getLogger(AngnSearchGliaParameter1_0.class);

    @Inject
    private SimulationParamRepository simulationParamRepository;

    private static final String PROBLEM_NODE =
        "<problem>" +
            "<annFixedGlia version='1.0' num_neurons='%d' num_layers='%d' num_activation='%d' num_iteration='%d'>" +
                "%s%s%s%s%s" +
            "</annFixedGlia>" +
        "</problem>";

    private static final String LAYER_PATTERN  = "<layer id='%d' value='%d'/>";
    private static final String NEURON_PATTERN =
        "<neuron id='%d' activation_function='%d'>" +
            "<astrocyte algorithm='%d' per_increase='%f' per_decrease='%f'/>" +
        "</neuron>";

    private static final String OUTERROR = "<outerror type='%d'/>";

    private static final String OUTPUT = "<output type='%d'>%s</output>";
    private static final String CLASSIFY_OUTPUT = "<classify step='%f' classes='%d'/>";

    private static final String CONSTRAINTS =
        "<constraints>" +
            "<weight min='%f' max='%f'/>" +
        "</constraints>";

    public String getProblemParameter(Simulation simulation) {
        Long simulationId = simulation.getId();

        List<SimulationParam> simulationParams = simulationParamRepository.findAllByBelongId(simulationId);

        String tmp;
        int cnt, auxNeuron, aux;
        int numLayers = new Integer(getParameterValue(simulationParams, "layer"));
        int numNeurons = 0;
        int numActivation = new Integer(getParameterValue(simulationParams, "activation"));
        int numIteration  = new Integer(getParameterValue(simulationParams, "iteration"));

        StringBuffer layerData = new StringBuffer();
        for (cnt = 0; cnt < numLayers; cnt++) {
            tmp = getParameterValue(simulationParams, String.format("layer[%d].neuron", cnt));
            layerData.append(String.format(LAYER_PATTERN, cnt, new Integer(tmp)));
            numNeurons += new Integer(tmp);
        }

        StringBuffer neuronData = new StringBuffer();
        for (aux = cnt = 0; cnt < numLayers; cnt++) {
//log.debug("Increase[{}/{}] {}", cnt, numLayers, getParameterValue(simulationParams, String.format("layer[%d].increase", cnt)));
//log.debug("Decrease[{}/{}] {}", cnt, numLayers, getParameterValue(simulationParams, String.format("layer[%d].decrease", cnt)));

            for (auxNeuron = 0; auxNeuron < new Integer(getParameterValue(simulationParams, String.format("layer[%d].neuron", cnt))) ; auxNeuron++) {

                neuronData.append(String.format(NEURON_PATTERN, aux,
                    getActivationFunction(
                        getParameterValue(simulationParams, String.format("layer[%d].activationFunction", cnt))
                    ),
                    getGliaAlgorithm(
                        getParameterValue(simulationParams, String.format("layer[%d].gliaAlgorithm", cnt))
                    ),
                    new Float(getParameterValue(simulationParams, String.format("layer[%d].increase", cnt))),
                    new Float(getParameterValue(simulationParams, String.format("layer[%d].decrease", cnt)))
                ));
                aux++;
            }
        }

        String outErrorData = String.format(OUTERROR, getOutErrorType(simulation));

        int outputType = getOutputType(simulation);
        tmp = "";
        if (outputType == 0) {
            tmp = String.format(CLASSIFY_OUTPUT, 0.5f, simulation.getUse().getNumClass());
        }
        String outputData = String.format(OUTPUT, outputType, tmp);

        String constraints = String.format(CONSTRAINTS,
            new Float(getParameterValue(simulationParams, "constraint.weight.min")),
            new Float(getParameterValue(simulationParams, "constraint.weight.max"))
        );

        return String.format(PROBLEM_NODE, numNeurons, numLayers, numActivation, numIteration,
            layerData.toString(), neuronData.toString(),
            outputData, outErrorData, constraints
        );
    }



}
