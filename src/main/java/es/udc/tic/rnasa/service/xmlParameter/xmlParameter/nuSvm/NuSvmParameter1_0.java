/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.xmlParameter.xmlParameter.nuSvm;

import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.domain.SimulationParam;
import es.udc.tic.rnasa.repository.SimulationParamRepository;
import es.udc.tic.rnasa.service.xmlParameter.xmlParameter.ProblemParameter;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by flanciskinho on 28/7/16.
 */
@Component
public class NuSvmParameter1_0 implements ProblemParameter {

    private final Logger log = LoggerFactory.getLogger(NuSvmParameter1_0.class);

    @Inject
    private SimulationParamRepository simulationParamRepository;

    private static final String PROBLEM_NODE =
        "<problem>" +
            "<svm version='1.0' type='%d'>" +
                "<constraints>" +
                    "<mu min='%f' max='%f'/>" +
                    "<tolerance min='%f' max='%f'/>" +
                    "<gamma min='%f' max='%f'/>" +
                    "<coef min='%f' max='%f'/>" +
                    "<degree min='%d' max='%d'/>" +
                "</constraints>" +
            "</svm>" +
        "</problem>";

    public String getProblemParameter(Simulation simulation) {
        Long simulationId = simulation.getId();

        int problemType = -1;
        if (simulation.getUse().getAnalyze().getName().equals("CLASSIFICATION"))
            problemType = 0;
        else if (simulation.getUse().getAnalyze().getName().equals("REGRESSION"))
            problemType = 1;
        else
            throw new NotImplementedException("No implemented svm for '"+simulation.getUse().getAnalyze().getName()+"'");

        List<SimulationParam> simulationParams = simulationParamRepository.findAllByBelongId(simulationId);

        return String.format(PROBLEM_NODE, problemType,
            new Float(getParameterValue(simulationParams, "constraint.nu.min")), new Float(getParameterValue(simulationParams, "constraint.nu.max")),
            new Float(getParameterValue(simulationParams, "constraint.tolerance.min")), new Float(getParameterValue(simulationParams, "constraint.tolerance.max")),
            new Float(getParameterValue(simulationParams, "constraint.gamma.min")), new Float(getParameterValue(simulationParams, "constraint.gamma.max")),
            new Float(getParameterValue(simulationParams, "constraint.coef.min")), new Float(getParameterValue(simulationParams, "constraint.coef.max")),
            new Integer(getParameterValue(simulationParams, "constraint.degree.min")), new Integer(getParameterValue(simulationParams, "constraint.degree.max"))
        );
    }


}
