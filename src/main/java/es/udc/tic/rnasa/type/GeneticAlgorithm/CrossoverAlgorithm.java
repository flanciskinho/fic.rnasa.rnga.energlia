/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.type.GeneticAlgorithm;

/**
 * Created by flanciskinho on 11/8/16.
 */
public enum CrossoverAlgorithm {

    REAL_SPX   (10, "REAL_SPX"),
    REAL_DPX   (11, "REAL_DPX"),
    REAL_UPX   (12, "REAL_UPX"),
    REAL_SBX   (13, "REAL_SBX");


    private Integer id;
    private String name;

    CrossoverAlgorithm(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static CrossoverAlgorithm getById(int id) {
        CrossoverAlgorithm aux[] = CrossoverAlgorithm.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].id == id)
                return aux[cnt];

        return null;
    }

    public static CrossoverAlgorithm getByName(String name) {
        CrossoverAlgorithm aux[] = CrossoverAlgorithm.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].name.equals(name))
                return aux[cnt];

        return null;
    }

}
