/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.type.GeneticAlgorithm;

/**
 * Created by flanciskinho on 11/8/16.
 */
public enum FitnessAlgorithm {
    FITNESS_MIN (1, "FITNESS_MIN"),
    FITNESS_AVG (2, "FITNESS_AVG"),
    FITNESS_MAX (3, "FITNESS_MAX");

    private Integer id;
    private String name;

    FitnessAlgorithm(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static FitnessAlgorithm getById(int id) {
        FitnessAlgorithm aux[] = FitnessAlgorithm.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].id == id)
                return aux[cnt];

        return null;
    }

    public static FitnessAlgorithm getByName(String name) {
        FitnessAlgorithm aux[] = FitnessAlgorithm.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].name.equals(name))
                return aux[cnt];

        return null;
    }

}
