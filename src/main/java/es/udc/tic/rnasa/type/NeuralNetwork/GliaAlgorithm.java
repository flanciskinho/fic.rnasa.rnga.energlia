/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.type.NeuralNetwork;

/**
 * Created by flanciskinho on 11/8/16.
 */
public enum GliaAlgorithm {

    ATTENUATED_EFFECT_ASTROCYTE(0, "ATTENUATED_EFFECT_ASTROCYTE"),
    NON_CONSECUTIVE_UNLIMITED(1, "NON_CONSECUTIVE_UNLIMITED"),
    GLOBAL_PROCESSING_EFFECT(2, "GLOBAL_PROCESSING_EFFECT"),
    CONSECUTIVE_LIMITED(3, "CONSECUTIVE_LIMITED"),
    NON_CONSECUTIVE_LIMITED(4, "NON_CONSECUTIVE_LIMITED"),
    CONSECUTIVE_UNLIMITED(5, "CONSECUTIVE_UNLIMITED"),
    DEPRESSION(6, "DEPRESSION"),
    CANNABINOIDES(7, "CANNABINOIDES");

    private Integer id;
    private String name;

    GliaAlgorithm(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public static GliaAlgorithm getById(int id) {
        GliaAlgorithm aux[] = GliaAlgorithm.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].id == id)
                return aux[cnt];

        return null;
    }

    public static GliaAlgorithm getByName(String name) {
        GliaAlgorithm aux[] = GliaAlgorithm.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].name.equals(name))
                return aux[cnt];

        return null;
    }

}



