/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.type.NeuralNetwork;

/**
 * Created by flanciskinho on 11/8/16.
 */
public enum OutputNetwork {
    OUTPUT_TYPE_CLASSIFY    (0, "CLASSIFY"),
    OUTPUT_TYPE_REGRESSION  (1, "REGRESSION"),
    OUTPUT_TYPE_COMPETITIVE (2, "COMPETITIVE");

    private Integer id;
    private String name;

    OutputNetwork(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static OutputNetwork getById(int id) {
        OutputNetwork aux[] = OutputNetwork.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].id == id)
                return aux[cnt];

        return null;
    }

    public static OutputNetwork getByName(String name) {
        OutputNetwork aux[] = OutputNetwork.values();

        for (int cnt = 0; cnt < aux.length; cnt++)
            if (aux[cnt].name.equals(name))
                return aux[cnt];

        return null;
    }
}
