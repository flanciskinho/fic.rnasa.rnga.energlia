/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.AnalyzeType;
import es.udc.tic.rnasa.repository.AnalyzeTypeRepository;
import es.udc.tic.rnasa.service.AnalyzeTypeService;
import es.udc.tic.rnasa.web.rest.dto.AnalyzeTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.AnalyzeTypeMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AnalyzeType.
 */
@RestController
@RequestMapping("/api")
public class AnalyzeTypeResource {

    private final Logger log = LoggerFactory.getLogger(AnalyzeTypeResource.class);

    @Inject
    private AnalyzeTypeService analyzeTypeService;

    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    @Inject
    private AnalyzeTypeMapper analyzeTypeMapper;

    /**
     * POST  /analyze-types : Create a new analyzeType.
     *
     * @param analyzeTypeDTO the analyzeTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new analyzeTypeDTO, or with status 400 (Bad Request) if the analyzeType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/analyze-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AnalyzeTypeDTO> createAnalyzeType(@Valid @RequestBody AnalyzeTypeDTO analyzeTypeDTO) throws URISyntaxException {
        log.debug("REST request to save AnalyzeType : {}", analyzeTypeDTO);
        if (analyzeTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("analyzeType", "idexists", "A new analyzeType cannot already have an ID")).body(null);
        }
        AnalyzeTypeDTO result = analyzeTypeService.save(analyzeTypeDTO);
        return ResponseEntity.created(new URI("/api/analyze-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("analyzeType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /analyze-types : Updates an existing analyzeType.
     *
     * @param analyzeTypeDTO the analyzeTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated analyzeTypeDTO,
     * or with status 400 (Bad Request) if the analyzeTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the analyzeTypeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/analyze-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AnalyzeTypeDTO> updateAnalyzeType(@Valid @RequestBody AnalyzeTypeDTO analyzeTypeDTO) throws URISyntaxException {
        log.debug("REST request to update AnalyzeType : {}", analyzeTypeDTO);
        if (analyzeTypeDTO.getId() == null) {
            return createAnalyzeType(analyzeTypeDTO);
        }
        AnalyzeTypeDTO result = analyzeTypeService.save(analyzeTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("analyzeType", analyzeTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /analyze-types : get all the analyzeTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of analyzeTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/analyze-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<AnalyzeTypeDTO>> getAllAnalyzeTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of AnalyzeTypes");
        Page<AnalyzeType> page = analyzeTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/analyze-types");
        return new ResponseEntity<>(analyzeTypeMapper.analyzeTypesToAnalyzeTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /analyze-types/:id : get the "id" analyzeType.
     *
     * @param id the id of the analyzeTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the analyzeTypeDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/analyze-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AnalyzeTypeDTO> getAnalyzeType(@PathVariable Long id) {
        log.debug("REST request to get AnalyzeType : {}", id);
        AnalyzeTypeDTO analyzeTypeDTO = analyzeTypeService.findOne(id);
        return Optional.ofNullable(analyzeTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /analyze-types/:id : delete the "id" analyzeType.
     *
     * @param id the id of the analyzeTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/analyze-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAnalyzeType(@PathVariable Long id) {
        log.debug("REST request to delete AnalyzeType : {}", id);
        analyzeTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("analyzeType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/analyze-types?query=:query : search for the analyzeType corresponding
     * to the query.
     *
     * @param query the query of the analyzeType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/analyze-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<AnalyzeTypeDTO>> searchAnalyzeTypes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of AnalyzeTypes for query {}", query);
        Page<AnalyzeType> page = analyzeTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/analyze-types");
        return new ResponseEntity<>(analyzeTypeMapper.analyzeTypesToAnalyzeTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
