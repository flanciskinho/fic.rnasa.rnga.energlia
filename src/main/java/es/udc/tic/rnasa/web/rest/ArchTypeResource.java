/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.ArchType;
import es.udc.tic.rnasa.service.ArchTypeService;
import es.udc.tic.rnasa.web.rest.dto.ArchTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ArchTypeMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ArchType.
 */
@RestController
@RequestMapping("/api")
public class ArchTypeResource {

    private final Logger log = LoggerFactory.getLogger(ArchTypeResource.class);

    @Inject
    private ArchTypeService archTypeService;

    @Inject
    private ArchTypeMapper archTypeMapper;



    /**
     * GET  /arch-types-by-belong/:id : get all the archTypes by belong id.
     *
     * @param id belong id information
     * @return the ResponseEntity with status 200 (OK) and the list of archTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/arch-types-by-belong/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<ArchTypeDTO> getAllArchTypes(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("REST request to get all ArchTypes by belong id {}", id);

        List<ArchTypeDTO> result = archTypeService.findAllByBelongIdAndActivatedIsTrue(id);

        log.debug("result {}", result);

        return result;
    }




    /**
     * POST  /arch-types : Create a new archType.
     *
     * @param archTypeDTO the archTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new archTypeDTO, or with status 400 (Bad Request) if the archType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/arch-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ArchTypeDTO> createArchType(@Valid @RequestBody ArchTypeDTO archTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ArchType : {}", archTypeDTO);
        if (archTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("archType", "idexists", "A new archType cannot already have an ID")).body(null);
        }
        ArchTypeDTO result = archTypeService.save(archTypeDTO);
        return ResponseEntity.created(new URI("/api/arch-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("archType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /arch-types : Updates an existing archType.
     *
     * @param archTypeDTO the archTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated archTypeDTO,
     * or with status 400 (Bad Request) if the archTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the archTypeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/arch-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ArchTypeDTO> updateArchType(@Valid @RequestBody ArchTypeDTO archTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ArchType : {}", archTypeDTO);
        if (archTypeDTO.getId() == null) {
            return createArchType(archTypeDTO);
        }
        ArchTypeDTO result = archTypeService.save(archTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("archType", archTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /arch-types : get all the archTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of archTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/arch-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ArchTypeDTO>> getAllArchTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ArchTypes");
        Page<ArchType> page = archTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/arch-types");
        return new ResponseEntity<>(archTypeMapper.archTypesToArchTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /arch-types/:id : get the "id" archType.
     *
     * @param id the id of the archTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the archTypeDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/arch-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ArchTypeDTO> getArchType(@PathVariable Long id) {
        log.debug("REST request to get ArchType : {}", id);
        ArchTypeDTO archTypeDTO = archTypeService.findOne(id);
        return Optional.ofNullable(archTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /arch-types/:id : delete the "id" archType.
     *
     * @param id the id of the archTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/arch-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteArchType(@PathVariable Long id) {
        log.debug("REST request to delete ArchType : {}", id);
        archTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("archType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/arch-types?query=:query : search for the archType corresponding
     * to the query.
     *
     * @param query the query of the archType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/arch-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ArchTypeDTO>> searchArchTypes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ArchTypes for query {}", query);
        Page<ArchType> page = archTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/arch-types");
        return new ResponseEntity<>(archTypeMapper.archTypesToArchTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
