/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.CompiledApp;
import es.udc.tic.rnasa.service.CompiledAppService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.CompiledAppDTO;
import es.udc.tic.rnasa.web.rest.mapper.CompiledAppMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CompiledApp.
 */
@RestController
@RequestMapping("/api")
public class CompiledAppResource {

    private final Logger log = LoggerFactory.getLogger(CompiledAppResource.class);
        
    @Inject
    private CompiledAppService compiledAppService;
    
    @Inject
    private CompiledAppMapper compiledAppMapper;
    
    /**
     * POST  /compiled-apps : Create a new compiledApp.
     *
     * @param compiledAppDTO the compiledAppDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new compiledAppDTO, or with status 400 (Bad Request) if the compiledApp has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/compiled-apps",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CompiledAppDTO> createCompiledApp(@Valid @RequestBody CompiledAppDTO compiledAppDTO) throws URISyntaxException {
        log.debug("REST request to save CompiledApp : {}", compiledAppDTO);
        if (compiledAppDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("compiledApp", "idexists", "A new compiledApp cannot already have an ID")).body(null);
        }
        CompiledAppDTO result = compiledAppService.save(compiledAppDTO);
        return ResponseEntity.created(new URI("/api/compiled-apps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("compiledApp", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /compiled-apps : Updates an existing compiledApp.
     *
     * @param compiledAppDTO the compiledAppDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated compiledAppDTO,
     * or with status 400 (Bad Request) if the compiledAppDTO is not valid,
     * or with status 500 (Internal Server Error) if the compiledAppDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/compiled-apps",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CompiledAppDTO> updateCompiledApp(@Valid @RequestBody CompiledAppDTO compiledAppDTO) throws URISyntaxException {
        log.debug("REST request to update CompiledApp : {}", compiledAppDTO);
        if (compiledAppDTO.getId() == null) {
            return createCompiledApp(compiledAppDTO);
        }
        CompiledAppDTO result = compiledAppService.save(compiledAppDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("compiledApp", compiledAppDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /compiled-apps : get all the compiledApps.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of compiledApps in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/compiled-apps",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<CompiledAppDTO>> getAllCompiledApps(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of CompiledApps");
        Page<CompiledApp> page = compiledAppService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/compiled-apps");
        return new ResponseEntity<>(compiledAppMapper.compiledAppsToCompiledAppDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /compiled-apps/:id : get the "id" compiledApp.
     *
     * @param id the id of the compiledAppDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the compiledAppDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/compiled-apps/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CompiledAppDTO> getCompiledApp(@PathVariable Long id) {
        log.debug("REST request to get CompiledApp : {}", id);
        CompiledAppDTO compiledAppDTO = compiledAppService.findOne(id);
        return Optional.ofNullable(compiledAppDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /compiled-apps/:id : delete the "id" compiledApp.
     *
     * @param id the id of the compiledAppDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/compiled-apps/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCompiledApp(@PathVariable Long id) {
        log.debug("REST request to delete CompiledApp : {}", id);
        compiledAppService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("compiledApp", id.toString())).build();
    }

    /**
     * SEARCH  /_search/compiled-apps?query=:query : search for the compiledApp corresponding
     * to the query.
     *
     * @param query the query of the compiledApp search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/compiled-apps",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<CompiledAppDTO>> searchCompiledApps(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of CompiledApps for query {}", query);
        Page<CompiledApp> page = compiledAppService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/compiled-apps");
        return new ResponseEntity<>(compiledAppMapper.compiledAppsToCompiledAppDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
