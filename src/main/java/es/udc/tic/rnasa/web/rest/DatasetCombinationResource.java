/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.DatasetCombination;
import es.udc.tic.rnasa.repository.DatasetCombinationRepository;
import es.udc.tic.rnasa.service.DatasetCombinationService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.DatasetCombinationDTO;
import es.udc.tic.rnasa.web.rest.mapper.DatasetCombinationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DatasetCombination.
 */
@RestController
@RequestMapping("/api")
public class DatasetCombinationResource {

    private final Logger log = LoggerFactory.getLogger(DatasetCombinationResource.class);

    @Inject
    private DatasetCombinationService datasetCombinationService;

    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private DatasetCombinationMapper datasetCombinationMapper;

    /**
     * POST  /dataset-combinations : Create a new datasetCombination.
     *
     * @param datasetCombinationDTO the datasetCombinationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new datasetCombinationDTO, or with status 400 (Bad Request) if the datasetCombination has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dataset-combinations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetCombinationDTO> createDatasetCombination(@Valid @RequestBody DatasetCombinationDTO datasetCombinationDTO) throws URISyntaxException {
        log.debug("REST request to save DatasetCombination : {}", datasetCombinationDTO);
        if (datasetCombinationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("datasetCombination", "idexists", "A new datasetCombination cannot already have an ID")).body(null);
        }
        DatasetCombinationDTO result = datasetCombinationService.save(datasetCombinationDTO);
        return ResponseEntity.created(new URI("/api/dataset-combinations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("datasetCombination", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dataset-combinations : Updates an existing datasetCombination.
     *
     * @param datasetCombinationDTO the datasetCombinationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated datasetCombinationDTO,
     * or with status 400 (Bad Request) if the datasetCombinationDTO is not valid,
     * or with status 500 (Internal Server Error) if the datasetCombinationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dataset-combinations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetCombinationDTO> updateDatasetCombination(@Valid @RequestBody DatasetCombinationDTO datasetCombinationDTO) throws URISyntaxException {
        log.debug("REST request to update DatasetCombination : {}", datasetCombinationDTO);
        if (datasetCombinationDTO.getId() == null) {
            return createDatasetCombination(datasetCombinationDTO);
        }
        DatasetCombinationDTO result = datasetCombinationService.save(datasetCombinationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("datasetCombination", datasetCombinationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dataset-combinations : get all the datasetCombinations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of datasetCombinations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/dataset-combinations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<DatasetCombinationDTO>> getAllDatasetCombinations(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DatasetCombinations");
        Page<DatasetCombination> page = datasetCombinationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dataset-combinations");
        return new ResponseEntity<>(datasetCombinationMapper.datasetCombinationsToDatasetCombinationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /dataset-combinations/:id : get the "id" datasetCombination.
     *
     * @param id the id of the datasetCombinationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the datasetCombinationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dataset-combinations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetCombinationDTO> getDatasetCombination(@PathVariable Long id) {
        log.debug("REST request to get DatasetCombination : {}", id);
        DatasetCombinationDTO datasetCombinationDTO = datasetCombinationService.findOne(id);
        return Optional.ofNullable(datasetCombinationDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dataset-combinations/:id : delete the "id" datasetCombination.
     *
     * @param id the id of the datasetCombinationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/dataset-combinations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDatasetCombination(@PathVariable Long id) {
        log.debug("REST request to delete DatasetCombination : {}", id);
        datasetCombinationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("datasetCombination", id.toString())).build();
    }

    /**
     * SEARCH  /_search/dataset-combinations?query=:query : search for the datasetCombination corresponding
     * to the query.
     *
     * @param query the query of the datasetCombination search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/dataset-combinations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<DatasetCombinationDTO>> searchDatasetCombinations(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of DatasetCombinations for query {}", query);
        Page<DatasetCombination> page = datasetCombinationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dataset-combinations");
        return new ResponseEntity<>(datasetCombinationMapper.datasetCombinationsToDatasetCombinationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
