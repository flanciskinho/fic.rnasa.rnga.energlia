/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.AnalyzeType;
import es.udc.tic.rnasa.domain.Dataset;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.AnalyzeTypeRepository;
import es.udc.tic.rnasa.repository.DatasetRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.security.SecurityUtils;
import es.udc.tic.rnasa.service.DatasetService;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.web.rest.dto.CustomDatasetDTO;
import es.udc.tic.rnasa.web.rest.dto.CustomDescriptionDTO;
import es.udc.tic.rnasa.web.rest.dto.CustomShareResourceDTO;
import es.udc.tic.rnasa.web.rest.dto.DatasetDTO;
import es.udc.tic.rnasa.web.rest.mapper.DatasetMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Dataset.
 */
@RestController
@RequestMapping("/api")
public class DatasetResource {

    private final Logger log = LoggerFactory.getLogger(DatasetResource.class);

    @Inject
    private DatasetService datasetService;

    @Inject
    private DatasetRepository datasetRepository;

    @Inject
    private UserMapper userMapper;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    @Inject
    private DatasetMapper datasetMapper;

    /**
     * POST  /custom-datasets : Create a new dataset.
     *
     * @param customDatasetDTO the customDatasetDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new datasetDTO, or with status 400 (Bad Request) if the dataset has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/custom-datasets",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetDTO> createCustomDataset(@Valid @RequestBody CustomDatasetDTO customDatasetDTO) throws URISyntaxException {
        try {
            log.debug("REST request to save Custom Dataset : {}", customDatasetDTO);
            if (customDatasetDTO.getId() != null) {
                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("dataset", "idexists", "A new dataset cannot already have an ID")).body(null);
            }

            /*
            call here the service
             */
            User owner = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
            AnalyzeType analyzeType = analyzeTypeRepository.findOne(customDatasetDTO.getAnalyzeId());


            List<byte []> l1 = customDatasetDTO.getTrainfilesin();
            List<byte []> l2 = customDatasetDTO.getTrainfilesout();

            List<byte []> l3 = customDatasetDTO.getValidationfilesin();
            List<byte []> l4 = customDatasetDTO.getValidationfilesout();

            List<byte []> l5 = customDatasetDTO.getTestfilesin();
            List<byte []> l6 = customDatasetDTO.getTestfilesout();

            String name = customDatasetDTO.getName();
            String desc = customDatasetDTO.getDescription();

            log.debug("customDatasetDTO: {}", customDatasetDTO);
            log.debug("owner: {}", owner);

            Dataset dataset = datasetService.createDataset(owner, name, desc, analyzeType, l1, l2, l3, l4, l5, l6);
            DatasetDTO result = datasetMapper.datasetToDatasetDTO(dataset);

            return ResponseEntity.created(new URI("/api/datasets/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("dataset", result.getName()))
                .body(result);


        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * PUT  /custom-datasets : Updates an existing dataset.
     *
     * @param customDescriptionDTO the customDescriptionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated datasetDTO,
     * or with status 400 (Bad Request) if the datasetDTO is not valid,
     * or with status 500 (Internal Server Error) if the datasetDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/custom-datasets",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetDTO> updateCustomDataset(@Valid @RequestBody CustomDescriptionDTO customDescriptionDTO) throws URISyntaxException {
        try {
            log.debug("REST request to update Dataset : {}", customDescriptionDTO);

            DatasetDTO result = datasetService.updateDataset(customDescriptionDTO);

            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("dataset", result.getName()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * POST  /dataset-share : Share a dataset with other users.
     *
     * @param customShareResourceDTO resource and user to share it
     * @return the ResponseEntity with status 201 (Created) and with body the datasetDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dataset-share",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetDTO> shareDataset(@Valid @RequestBody CustomShareResourceDTO customShareResourceDTO) throws URISyntaxException {
        log.debug("REST request to save Dataset : {}", customShareResourceDTO);

        try {
            DatasetDTO result = datasetService.share(customShareResourceDTO.getResourceId(), customShareResourceDTO.getLoginname());
            return ResponseEntity.created(new URI("/api/datasets/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("dataset", result.getId().toString()))
                .body(result);

        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //
    // below this point have a auto-generated resources
    //


    /**
     * POST  /datasets : Create a new dataset.
     *
     * @param datasetDTO the datasetDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new datasetDTO, or with status 400 (Bad Request) if the dataset has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/datasets",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetDTO> createDataset(@Valid @RequestBody DatasetDTO datasetDTO) throws URISyntaxException {
        try {
            log.debug("REST request to save Dataset : {}", datasetDTO);
            if (datasetDTO.getId() != null) {
                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("dataset", "idexists", "A new dataset cannot already have an ID")).body(null);
            }

            DatasetDTO result = datasetService.save(datasetDTO);
            return ResponseEntity.created(new URI("/api/datasets/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("dataset", result.getId().toString()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * PUT  /datasets : Updates an existing dataset.
     *
     * @param datasetDTO the datasetDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated datasetDTO,
     * or with status 400 (Bad Request) if the datasetDTO is not valid,
     * or with status 500 (Internal Server Error) if the datasetDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/datasets",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetDTO> updateDataset(@Valid @RequestBody DatasetDTO datasetDTO) throws URISyntaxException {
        try {
            log.debug("REST request to update Dataset : {}", datasetDTO);
            if (datasetDTO.getId() == null) {
                return createDataset(datasetDTO);
            }
            DatasetDTO result = datasetService.save(datasetDTO);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("dataset", datasetDTO.getId().toString()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * GET  /datasets : get all the datasets.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of datasets in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/datasets",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<DatasetDTO>> getAllDatasets(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Datasets");

        try {
            Page<Dataset> page = datasetService.findAll(pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/datasets");
            return new ResponseEntity<>(datasetMapper.datasetsToDatasetDTOs(page.getContent()), headers, HttpStatus.OK);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        }
    }

    /**
     * GET  /datasets/:id : get the "id" dataset.
     *
     * @param id the id of the datasetDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the datasetDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/datasets/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DatasetDTO> getDataset(@PathVariable Long id) {
        try {
            log.debug("REST request to get Dataset : {}", id);
            DatasetDTO datasetDTO = datasetService.findOne(id);

            return Optional.ofNullable(datasetDTO)
                .map(result -> new ResponseEntity<>(
                    result,
                    HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * DELETE  /datasets/:id : delete the "id" dataset.
     *
     * @param id the id of the datasetDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/datasets/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDataset(@PathVariable Long id) {
        try {
            log.debug("REST request to delete Dataset : {}", id);
            datasetService.delete(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("dataset", id.toString())).build();
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * SEARCH  /_search/datasets?query=:query : search for the dataset corresponding
     * to the query.
     *
     * @param query the query of the dataset search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/datasets",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<DatasetDTO>> searchDatasets(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        try {
            log.debug("REST request to search for a page of Datasets for query {}", query);
            Page<Dataset> page = datasetService.search(query, pageable);
            HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/datasets");
            return new ResponseEntity<>(datasetMapper.datasetsToDatasetDTOs(page.getContent()), headers, HttpStatus.OK);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
