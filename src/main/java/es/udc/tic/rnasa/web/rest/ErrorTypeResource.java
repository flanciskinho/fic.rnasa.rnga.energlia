/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.ErrorType;
import es.udc.tic.rnasa.service.ErrorTypeService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.ErrorTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ErrorTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ErrorType.
 */
@RestController
@RequestMapping("/api")
public class ErrorTypeResource {

    private final Logger log = LoggerFactory.getLogger(ErrorTypeResource.class);
        
    @Inject
    private ErrorTypeService errorTypeService;
    
    @Inject
    private ErrorTypeMapper errorTypeMapper;
    
    /**
     * POST  /error-types : Create a new errorType.
     *
     * @param errorTypeDTO the errorTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new errorTypeDTO, or with status 400 (Bad Request) if the errorType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/error-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErrorTypeDTO> createErrorType(@Valid @RequestBody ErrorTypeDTO errorTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ErrorType : {}", errorTypeDTO);
        if (errorTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("errorType", "idexists", "A new errorType cannot already have an ID")).body(null);
        }
        ErrorTypeDTO result = errorTypeService.save(errorTypeDTO);
        return ResponseEntity.created(new URI("/api/error-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("errorType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /error-types : Updates an existing errorType.
     *
     * @param errorTypeDTO the errorTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated errorTypeDTO,
     * or with status 400 (Bad Request) if the errorTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the errorTypeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/error-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErrorTypeDTO> updateErrorType(@Valid @RequestBody ErrorTypeDTO errorTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ErrorType : {}", errorTypeDTO);
        if (errorTypeDTO.getId() == null) {
            return createErrorType(errorTypeDTO);
        }
        ErrorTypeDTO result = errorTypeService.save(errorTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("errorType", errorTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /error-types : get all the errorTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of errorTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/error-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ErrorTypeDTO>> getAllErrorTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ErrorTypes");
        Page<ErrorType> page = errorTypeService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/error-types");
        return new ResponseEntity<>(errorTypeMapper.errorTypesToErrorTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /error-types/:id : get the "id" errorType.
     *
     * @param id the id of the errorTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the errorTypeDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/error-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErrorTypeDTO> getErrorType(@PathVariable Long id) {
        log.debug("REST request to get ErrorType : {}", id);
        ErrorTypeDTO errorTypeDTO = errorTypeService.findOne(id);
        return Optional.ofNullable(errorTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /error-types/:id : delete the "id" errorType.
     *
     * @param id the id of the errorTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/error-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteErrorType(@PathVariable Long id) {
        log.debug("REST request to delete ErrorType : {}", id);
        errorTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("errorType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/error-types?query=:query : search for the errorType corresponding
     * to the query.
     *
     * @param query the query of the errorType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/error-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ErrorTypeDTO>> searchErrorTypes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ErrorTypes for query {}", query);
        Page<ErrorType> page = errorTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/error-types");
        return new ResponseEntity<>(errorTypeMapper.errorTypesToErrorTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
