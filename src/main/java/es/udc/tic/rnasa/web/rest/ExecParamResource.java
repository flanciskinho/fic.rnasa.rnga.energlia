/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.ExecParam;
import es.udc.tic.rnasa.service.ExecParamService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.ExecParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.ExecParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ExecParam.
 */
@RestController
@RequestMapping("/api")
public class ExecParamResource {

    private final Logger log = LoggerFactory.getLogger(ExecParamResource.class);
        
    @Inject
    private ExecParamService execParamService;
    
    @Inject
    private ExecParamMapper execParamMapper;
    
    /**
     * POST  /exec-params : Create a new execParam.
     *
     * @param execParamDTO the execParamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new execParamDTO, or with status 400 (Bad Request) if the execParam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/exec-params",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExecParamDTO> createExecParam(@Valid @RequestBody ExecParamDTO execParamDTO) throws URISyntaxException {
        log.debug("REST request to save ExecParam : {}", execParamDTO);
        if (execParamDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("execParam", "idexists", "A new execParam cannot already have an ID")).body(null);
        }
        ExecParamDTO result = execParamService.save(execParamDTO);
        return ResponseEntity.created(new URI("/api/exec-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("execParam", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /exec-params : Updates an existing execParam.
     *
     * @param execParamDTO the execParamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated execParamDTO,
     * or with status 400 (Bad Request) if the execParamDTO is not valid,
     * or with status 500 (Internal Server Error) if the execParamDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/exec-params",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExecParamDTO> updateExecParam(@Valid @RequestBody ExecParamDTO execParamDTO) throws URISyntaxException {
        log.debug("REST request to update ExecParam : {}", execParamDTO);
        if (execParamDTO.getId() == null) {
            return createExecParam(execParamDTO);
        }
        ExecParamDTO result = execParamService.save(execParamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("execParam", execParamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /exec-params : get all the execParams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of execParams in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/exec-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ExecParamDTO>> getAllExecParams(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ExecParams");
        Page<ExecParam> page = execParamService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/exec-params");
        return new ResponseEntity<>(execParamMapper.execParamsToExecParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /exec-params/:id : get the "id" execParam.
     *
     * @param id the id of the execParamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the execParamDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/exec-params/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExecParamDTO> getExecParam(@PathVariable Long id) {
        log.debug("REST request to get ExecParam : {}", id);
        ExecParamDTO execParamDTO = execParamService.findOne(id);
        return Optional.ofNullable(execParamDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /exec-params/:id : delete the "id" execParam.
     *
     * @param id the id of the execParamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/exec-params/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteExecParam(@PathVariable Long id) {
        log.debug("REST request to delete ExecParam : {}", id);
        execParamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("execParam", id.toString())).build();
    }

    /**
     * SEARCH  /_search/exec-params?query=:query : search for the execParam corresponding
     * to the query.
     *
     * @param query the query of the execParam search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/exec-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ExecParamDTO>> searchExecParams(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ExecParams for query {}", query);
        Page<ExecParam> page = execParamService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/exec-params");
        return new ResponseEntity<>(execParamMapper.execParamsToExecParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
