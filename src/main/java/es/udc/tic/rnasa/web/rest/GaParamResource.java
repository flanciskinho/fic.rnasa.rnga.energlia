/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.GaParam;
import es.udc.tic.rnasa.service.GaParamService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.GaParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GaParam.
 */
@RestController
@RequestMapping("/api")
public class GaParamResource {

    private final Logger log = LoggerFactory.getLogger(GaParamResource.class);
        
    @Inject
    private GaParamService gaParamService;
    
    @Inject
    private GaParamMapper gaParamMapper;
    
    /**
     * POST  /ga-params : Create a new gaParam.
     *
     * @param gaParamDTO the gaParamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gaParamDTO, or with status 400 (Bad Request) if the gaParam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ga-params",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaParamDTO> createGaParam(@Valid @RequestBody GaParamDTO gaParamDTO) throws URISyntaxException {
        log.debug("REST request to save GaParam : {}", gaParamDTO);
        if (gaParamDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("gaParam", "idexists", "A new gaParam cannot already have an ID")).body(null);
        }
        GaParamDTO result = gaParamService.save(gaParamDTO);
        return ResponseEntity.created(new URI("/api/ga-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("gaParam", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ga-params : Updates an existing gaParam.
     *
     * @param gaParamDTO the gaParamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gaParamDTO,
     * or with status 400 (Bad Request) if the gaParamDTO is not valid,
     * or with status 500 (Internal Server Error) if the gaParamDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ga-params",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaParamDTO> updateGaParam(@Valid @RequestBody GaParamDTO gaParamDTO) throws URISyntaxException {
        log.debug("REST request to update GaParam : {}", gaParamDTO);
        if (gaParamDTO.getId() == null) {
            return createGaParam(gaParamDTO);
        }
        GaParamDTO result = gaParamService.save(gaParamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("gaParam", gaParamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ga-params : get all the gaParams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gaParams in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/ga-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<GaParamDTO>> getAllGaParams(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of GaParams");
        Page<GaParam> page = gaParamService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ga-params");
        return new ResponseEntity<>(gaParamMapper.gaParamsToGaParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /ga-params/:id : get the "id" gaParam.
     *
     * @param id the id of the gaParamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gaParamDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/ga-params/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaParamDTO> getGaParam(@PathVariable Long id) {
        log.debug("REST request to get GaParam : {}", id);
        GaParamDTO gaParamDTO = gaParamService.findOne(id);
        return Optional.ofNullable(gaParamDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /ga-params/:id : delete the "id" gaParam.
     *
     * @param id the id of the gaParamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/ga-params/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGaParam(@PathVariable Long id) {
        log.debug("REST request to delete GaParam : {}", id);
        gaParamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("gaParam", id.toString())).build();
    }

    /**
     * SEARCH  /_search/ga-params?query=:query : search for the gaParam corresponding
     * to the query.
     *
     * @param query the query of the gaParam search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/ga-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<GaParamDTO>> searchGaParams(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of GaParams for query {}", query);
        Page<GaParam> page = gaParamService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/ga-params");
        return new ResponseEntity<>(gaParamMapper.gaParamsToGaParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
