/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.Ga;
import es.udc.tic.rnasa.service.GaService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.GaDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Ga.
 */
@RestController
@RequestMapping("/api")
public class GaResource {

    private final Logger log = LoggerFactory.getLogger(GaResource.class);
        
    @Inject
    private GaService gaService;
    
    @Inject
    private GaMapper gaMapper;
    
    /**
     * POST  /gas : Create a new ga.
     *
     * @param gaDTO the gaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gaDTO, or with status 400 (Bad Request) if the ga has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gas",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaDTO> createGa(@Valid @RequestBody GaDTO gaDTO) throws URISyntaxException {
        log.debug("REST request to save Ga : {}", gaDTO);
        if (gaDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("ga", "idexists", "A new ga cannot already have an ID")).body(null);
        }
        GaDTO result = gaService.save(gaDTO);
        return ResponseEntity.created(new URI("/api/gas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("ga", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gas : Updates an existing ga.
     *
     * @param gaDTO the gaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gaDTO,
     * or with status 400 (Bad Request) if the gaDTO is not valid,
     * or with status 500 (Internal Server Error) if the gaDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaDTO> updateGa(@Valid @RequestBody GaDTO gaDTO) throws URISyntaxException {
        log.debug("REST request to update Ga : {}", gaDTO);
        if (gaDTO.getId() == null) {
            return createGa(gaDTO);
        }
        GaDTO result = gaService.save(gaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("ga", gaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gas : get all the gas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gas in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/gas",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<GaDTO>> getAllGas(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Gas");
        Page<Ga> page = gaService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gas");
        return new ResponseEntity<>(gaMapper.gasToGaDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /gas/:id : get the "id" ga.
     *
     * @param id the id of the gaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gaDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/gas/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaDTO> getGa(@PathVariable Long id) {
        log.debug("REST request to get Ga : {}", id);
        GaDTO gaDTO = gaService.findOne(id);
        return Optional.ofNullable(gaDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /gas/:id : delete the "id" ga.
     *
     * @param id the id of the gaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/gas/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGa(@PathVariable Long id) {
        log.debug("REST request to delete Ga : {}", id);
        gaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("ga", id.toString())).build();
    }

    /**
     * SEARCH  /_search/gas?query=:query : search for the ga corresponding
     * to the query.
     *
     * @param query the query of the ga search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/gas",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<GaDTO>> searchGas(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Gas for query {}", query);
        Page<Ga> page = gaService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/gas");
        return new ResponseEntity<>(gaMapper.gasToGaDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
