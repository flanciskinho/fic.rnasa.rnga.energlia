/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.GaType;
import es.udc.tic.rnasa.service.GaTypeService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.GaTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GaType.
 */
@RestController
@RequestMapping("/api")
public class GaTypeResource {

    private final Logger log = LoggerFactory.getLogger(GaTypeResource.class);
        
    @Inject
    private GaTypeService gaTypeService;
    
    @Inject
    private GaTypeMapper gaTypeMapper;
    
    /**
     * POST  /ga-types : Create a new gaType.
     *
     * @param gaTypeDTO the gaTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gaTypeDTO, or with status 400 (Bad Request) if the gaType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ga-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaTypeDTO> createGaType(@Valid @RequestBody GaTypeDTO gaTypeDTO) throws URISyntaxException {
        log.debug("REST request to save GaType : {}", gaTypeDTO);
        if (gaTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("gaType", "idexists", "A new gaType cannot already have an ID")).body(null);
        }
        GaTypeDTO result = gaTypeService.save(gaTypeDTO);
        return ResponseEntity.created(new URI("/api/ga-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("gaType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ga-types : Updates an existing gaType.
     *
     * @param gaTypeDTO the gaTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gaTypeDTO,
     * or with status 400 (Bad Request) if the gaTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the gaTypeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ga-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaTypeDTO> updateGaType(@Valid @RequestBody GaTypeDTO gaTypeDTO) throws URISyntaxException {
        log.debug("REST request to update GaType : {}", gaTypeDTO);
        if (gaTypeDTO.getId() == null) {
            return createGaType(gaTypeDTO);
        }
        GaTypeDTO result = gaTypeService.save(gaTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("gaType", gaTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ga-types : get all the gaTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gaTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/ga-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<GaTypeDTO>> getAllGaTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of GaTypes");
        Page<GaType> page = gaTypeService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ga-types");
        return new ResponseEntity<>(gaTypeMapper.gaTypesToGaTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /ga-types/:id : get the "id" gaType.
     *
     * @param id the id of the gaTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gaTypeDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/ga-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GaTypeDTO> getGaType(@PathVariable Long id) {
        log.debug("REST request to get GaType : {}", id);
        GaTypeDTO gaTypeDTO = gaTypeService.findOne(id);
        return Optional.ofNullable(gaTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /ga-types/:id : delete the "id" gaType.
     *
     * @param id the id of the gaTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/ga-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGaType(@PathVariable Long id) {
        log.debug("REST request to delete GaType : {}", id);
        gaTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("gaType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/ga-types?query=:query : search for the gaType corresponding
     * to the query.
     *
     * @param query the query of the gaType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/ga-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<GaTypeDTO>> searchGaTypes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of GaTypes for query {}", query);
        Page<GaType> page = gaTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/ga-types");
        return new ResponseEntity<>(gaTypeMapper.gaTypesToGaTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
