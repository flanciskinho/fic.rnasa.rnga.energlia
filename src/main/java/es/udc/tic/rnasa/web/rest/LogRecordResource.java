/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.LogRecord;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.service.LogRecordService;
import es.udc.tic.rnasa.service.SimulationService;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.service.util.CheckPermission;
import es.udc.tic.rnasa.web.rest.dto.ForLogRecordDTO.LogRecordPointDTO;
import es.udc.tic.rnasa.web.rest.dto.LogRecordDTO;
import es.udc.tic.rnasa.web.rest.mapper.LogRecordMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LogRecord.
 */
@RestController
@RequestMapping("/api")
public class LogRecordResource {

    private final Logger log = LoggerFactory.getLogger(LogRecordResource.class);

    @Inject
    private LogRecordService logRecordService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private SimulationService simulationService;

    @Inject
    private LogRecordMapper logRecordMapper;

    /**
     * POST  /log-records : Create a new logRecord.
     *
     * @param logRecordDTO the logRecordDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new logRecordDTO, or with status 400 (Bad Request) if the logRecord has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/log-records",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LogRecordDTO> createLogRecord(@Valid @RequestBody LogRecordDTO logRecordDTO) throws URISyntaxException {
        log.debug("REST request to save LogRecord : {}", logRecordDTO);
        if (logRecordDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("logRecord", "idexists", "A new logRecord cannot already have an ID")).body(null);
        }
        LogRecordDTO result = logRecordService.save(logRecordDTO);
        return ResponseEntity.created(new URI("/api/log-records/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("logRecord", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /log-records : Updates an existing logRecord.
     *
     * @param logRecordDTO the logRecordDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated logRecordDTO,
     * or with status 400 (Bad Request) if the logRecordDTO is not valid,
     * or with status 500 (Internal Server Error) if the logRecordDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/log-records",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LogRecordDTO> updateLogRecord(@Valid @RequestBody LogRecordDTO logRecordDTO) throws URISyntaxException {
        log.debug("REST request to update LogRecord : {}", logRecordDTO);
        if (logRecordDTO.getId() == null) {
            return createLogRecord(logRecordDTO);
        }
        LogRecordDTO result = logRecordService.save(logRecordDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("logRecord", logRecordDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /log-records : get all the logRecords.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of logRecords in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/log-records",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<LogRecordDTO>> getAllLogRecords(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of LogRecords");
        Page<LogRecord> page = logRecordService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/log-records");
        return new ResponseEntity<>(logRecordMapper.logRecordsToLogRecordDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /log-records/:id : get the "id" logRecord.
     *
     * @param id the id of the logRecordDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the logRecordDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/log-records/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LogRecordDTO> getLogRecord(@PathVariable Long id) {
        log.debug("REST request to get LogRecord : {}", id);
        LogRecordDTO logRecordDTO = logRecordService.findOne(id);
        return Optional.ofNullable(logRecordDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /log-records/:id : delete the "id" logRecord.
     *
     * @param id the id of the logRecordDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/log-records/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteLogRecord(@PathVariable Long id) {
        log.debug("REST request to delete LogRecord : {}", id);
        logRecordService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("logRecord", id.toString())).build();
    }

    /**
     * SEARCH  /_search/log-records?query=:query : search for the logRecord corresponding
     * to the query.
     *
     * @param query the query of the logRecord search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/log-records",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<LogRecordDTO>> searchLogRecords(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of LogRecords for query {}", query);
        Page<LogRecord> page = logRecordService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/log-records");
        return new ResponseEntity<>(logRecordMapper.logRecordsToLogRecordDTOs(page.getContent()), headers, HttpStatus.OK);
    }



    /**
     * GET  /get-log-average/:simulationId : get average list of records for simulation
     *
     * @param simulationId the id of the logRecordDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the logRecordPointDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/get-log-average/{simulationId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<LogRecordPointDTO>> getLogAverage(@PathVariable Long simulationId) {
        log.debug("REST request to get log average : {}", simulationId);

        try {
            new CheckPermission(userRepository).checkPermission(simulationService.findOne(simulationId));

            List<LogRecordPointDTO> list = logRecordService.getLogsAverage(simulationId, 40);

            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (HttpExceptionDetails httpExceptionDetails) {
            return new ResponseEntity<>(httpExceptionDetails.getStatus());
        }

    }



}
