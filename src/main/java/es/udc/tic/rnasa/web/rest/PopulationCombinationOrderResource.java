/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.PopulationCombinationOrder;
import es.udc.tic.rnasa.service.PopulationCombinationOrderService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.PopulationCombinationOrderDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationCombinationOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PopulationCombinationOrder.
 */
@RestController
@RequestMapping("/api")
public class PopulationCombinationOrderResource {

    private final Logger log = LoggerFactory.getLogger(PopulationCombinationOrderResource.class);
        
    @Inject
    private PopulationCombinationOrderService populationCombinationOrderService;
    
    @Inject
    private PopulationCombinationOrderMapper populationCombinationOrderMapper;
    
    /**
     * POST  /population-combination-orders : Create a new populationCombinationOrder.
     *
     * @param populationCombinationOrderDTO the populationCombinationOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new populationCombinationOrderDTO, or with status 400 (Bad Request) if the populationCombinationOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-combination-orders",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationCombinationOrderDTO> createPopulationCombinationOrder(@Valid @RequestBody PopulationCombinationOrderDTO populationCombinationOrderDTO) throws URISyntaxException {
        log.debug("REST request to save PopulationCombinationOrder : {}", populationCombinationOrderDTO);
        if (populationCombinationOrderDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("populationCombinationOrder", "idexists", "A new populationCombinationOrder cannot already have an ID")).body(null);
        }
        PopulationCombinationOrderDTO result = populationCombinationOrderService.save(populationCombinationOrderDTO);
        return ResponseEntity.created(new URI("/api/population-combination-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("populationCombinationOrder", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /population-combination-orders : Updates an existing populationCombinationOrder.
     *
     * @param populationCombinationOrderDTO the populationCombinationOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated populationCombinationOrderDTO,
     * or with status 400 (Bad Request) if the populationCombinationOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the populationCombinationOrderDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-combination-orders",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationCombinationOrderDTO> updatePopulationCombinationOrder(@Valid @RequestBody PopulationCombinationOrderDTO populationCombinationOrderDTO) throws URISyntaxException {
        log.debug("REST request to update PopulationCombinationOrder : {}", populationCombinationOrderDTO);
        if (populationCombinationOrderDTO.getId() == null) {
            return createPopulationCombinationOrder(populationCombinationOrderDTO);
        }
        PopulationCombinationOrderDTO result = populationCombinationOrderService.save(populationCombinationOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("populationCombinationOrder", populationCombinationOrderDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /population-combination-orders : get all the populationCombinationOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of populationCombinationOrders in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/population-combination-orders",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationCombinationOrderDTO>> getAllPopulationCombinationOrders(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PopulationCombinationOrders");
        Page<PopulationCombinationOrder> page = populationCombinationOrderService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/population-combination-orders");
        return new ResponseEntity<>(populationCombinationOrderMapper.populationCombinationOrdersToPopulationCombinationOrderDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /population-combination-orders/:id : get the "id" populationCombinationOrder.
     *
     * @param id the id of the populationCombinationOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the populationCombinationOrderDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/population-combination-orders/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationCombinationOrderDTO> getPopulationCombinationOrder(@PathVariable Long id) {
        log.debug("REST request to get PopulationCombinationOrder : {}", id);
        PopulationCombinationOrderDTO populationCombinationOrderDTO = populationCombinationOrderService.findOne(id);
        return Optional.ofNullable(populationCombinationOrderDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /population-combination-orders/:id : delete the "id" populationCombinationOrder.
     *
     * @param id the id of the populationCombinationOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/population-combination-orders/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePopulationCombinationOrder(@PathVariable Long id) {
        log.debug("REST request to delete PopulationCombinationOrder : {}", id);
        populationCombinationOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("populationCombinationOrder", id.toString())).build();
    }

    /**
     * SEARCH  /_search/population-combination-orders?query=:query : search for the populationCombinationOrder corresponding
     * to the query.
     *
     * @param query the query of the populationCombinationOrder search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/population-combination-orders",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationCombinationOrderDTO>> searchPopulationCombinationOrders(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of PopulationCombinationOrders for query {}", query);
        Page<PopulationCombinationOrder> page = populationCombinationOrderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/population-combination-orders");
        return new ResponseEntity<>(populationCombinationOrderMapper.populationCombinationOrdersToPopulationCombinationOrderDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
