/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.PopulationCombination;
import es.udc.tic.rnasa.service.PopulationCombinationService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.PopulationCombinationDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationCombinationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PopulationCombination.
 */
@RestController
@RequestMapping("/api")
public class PopulationCombinationResource {

    private final Logger log = LoggerFactory.getLogger(PopulationCombinationResource.class);
        
    @Inject
    private PopulationCombinationService populationCombinationService;
    
    @Inject
    private PopulationCombinationMapper populationCombinationMapper;
    
    /**
     * POST  /population-combinations : Create a new populationCombination.
     *
     * @param populationCombinationDTO the populationCombinationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new populationCombinationDTO, or with status 400 (Bad Request) if the populationCombination has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-combinations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationCombinationDTO> createPopulationCombination(@Valid @RequestBody PopulationCombinationDTO populationCombinationDTO) throws URISyntaxException {
        log.debug("REST request to save PopulationCombination : {}", populationCombinationDTO);
        if (populationCombinationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("populationCombination", "idexists", "A new populationCombination cannot already have an ID")).body(null);
        }
        PopulationCombinationDTO result = populationCombinationService.save(populationCombinationDTO);
        return ResponseEntity.created(new URI("/api/population-combinations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("populationCombination", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /population-combinations : Updates an existing populationCombination.
     *
     * @param populationCombinationDTO the populationCombinationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated populationCombinationDTO,
     * or with status 400 (Bad Request) if the populationCombinationDTO is not valid,
     * or with status 500 (Internal Server Error) if the populationCombinationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-combinations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationCombinationDTO> updatePopulationCombination(@Valid @RequestBody PopulationCombinationDTO populationCombinationDTO) throws URISyntaxException {
        log.debug("REST request to update PopulationCombination : {}", populationCombinationDTO);
        if (populationCombinationDTO.getId() == null) {
            return createPopulationCombination(populationCombinationDTO);
        }
        PopulationCombinationDTO result = populationCombinationService.save(populationCombinationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("populationCombination", populationCombinationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /population-combinations : get all the populationCombinations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of populationCombinations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/population-combinations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationCombinationDTO>> getAllPopulationCombinations(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PopulationCombinations");
        Page<PopulationCombination> page = populationCombinationService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/population-combinations");
        return new ResponseEntity<>(populationCombinationMapper.populationCombinationsToPopulationCombinationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /population-combinations/:id : get the "id" populationCombination.
     *
     * @param id the id of the populationCombinationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the populationCombinationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/population-combinations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationCombinationDTO> getPopulationCombination(@PathVariable Long id) {
        log.debug("REST request to get PopulationCombination : {}", id);
        PopulationCombinationDTO populationCombinationDTO = populationCombinationService.findOne(id);
        return Optional.ofNullable(populationCombinationDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /population-combinations/:id : delete the "id" populationCombination.
     *
     * @param id the id of the populationCombinationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/population-combinations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePopulationCombination(@PathVariable Long id) {
        log.debug("REST request to delete PopulationCombination : {}", id);
        populationCombinationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("populationCombination", id.toString())).build();
    }

    /**
     * SEARCH  /_search/population-combinations?query=:query : search for the populationCombination corresponding
     * to the query.
     *
     * @param query the query of the populationCombination search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/population-combinations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationCombinationDTO>> searchPopulationCombinations(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of PopulationCombinations for query {}", query);
        Page<PopulationCombination> page = populationCombinationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/population-combinations");
        return new ResponseEntity<>(populationCombinationMapper.populationCombinationsToPopulationCombinationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
