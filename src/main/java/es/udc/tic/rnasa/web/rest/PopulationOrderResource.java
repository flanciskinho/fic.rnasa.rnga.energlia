/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.PopulationOrder;
import es.udc.tic.rnasa.service.PopulationOrderService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.PopulationOrderDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PopulationOrder.
 */
@RestController
@RequestMapping("/api")
public class PopulationOrderResource {

    private final Logger log = LoggerFactory.getLogger(PopulationOrderResource.class);
        
    @Inject
    private PopulationOrderService populationOrderService;
    
    @Inject
    private PopulationOrderMapper populationOrderMapper;
    
    /**
     * POST  /population-orders : Create a new populationOrder.
     *
     * @param populationOrderDTO the populationOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new populationOrderDTO, or with status 400 (Bad Request) if the populationOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-orders",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationOrderDTO> createPopulationOrder(@Valid @RequestBody PopulationOrderDTO populationOrderDTO) throws URISyntaxException {
        log.debug("REST request to save PopulationOrder : {}", populationOrderDTO);
        if (populationOrderDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("populationOrder", "idexists", "A new populationOrder cannot already have an ID")).body(null);
        }
        PopulationOrderDTO result = populationOrderService.save(populationOrderDTO);
        return ResponseEntity.created(new URI("/api/population-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("populationOrder", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /population-orders : Updates an existing populationOrder.
     *
     * @param populationOrderDTO the populationOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated populationOrderDTO,
     * or with status 400 (Bad Request) if the populationOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the populationOrderDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-orders",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationOrderDTO> updatePopulationOrder(@Valid @RequestBody PopulationOrderDTO populationOrderDTO) throws URISyntaxException {
        log.debug("REST request to update PopulationOrder : {}", populationOrderDTO);
        if (populationOrderDTO.getId() == null) {
            return createPopulationOrder(populationOrderDTO);
        }
        PopulationOrderDTO result = populationOrderService.save(populationOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("populationOrder", populationOrderDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /population-orders : get all the populationOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of populationOrders in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/population-orders",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationOrderDTO>> getAllPopulationOrders(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PopulationOrders");
        Page<PopulationOrder> page = populationOrderService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/population-orders");
        return new ResponseEntity<>(populationOrderMapper.populationOrdersToPopulationOrderDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /population-orders/:id : get the "id" populationOrder.
     *
     * @param id the id of the populationOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the populationOrderDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/population-orders/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationOrderDTO> getPopulationOrder(@PathVariable Long id) {
        log.debug("REST request to get PopulationOrder : {}", id);
        PopulationOrderDTO populationOrderDTO = populationOrderService.findOne(id);
        return Optional.ofNullable(populationOrderDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /population-orders/:id : delete the "id" populationOrder.
     *
     * @param id the id of the populationOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/population-orders/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePopulationOrder(@PathVariable Long id) {
        log.debug("REST request to delete PopulationOrder : {}", id);
        populationOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("populationOrder", id.toString())).build();
    }

    /**
     * SEARCH  /_search/population-orders?query=:query : search for the populationOrder corresponding
     * to the query.
     *
     * @param query the query of the populationOrder search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/population-orders",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationOrderDTO>> searchPopulationOrders(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of PopulationOrders for query {}", query);
        Page<PopulationOrder> page = populationOrderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/population-orders");
        return new ResponseEntity<>(populationOrderMapper.populationOrdersToPopulationOrderDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
