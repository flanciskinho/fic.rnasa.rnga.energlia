/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.PopulationParam;
import es.udc.tic.rnasa.service.PopulationParamService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.PopulationParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PopulationParam.
 */
@RestController
@RequestMapping("/api")
public class PopulationParamResource {

    private final Logger log = LoggerFactory.getLogger(PopulationParamResource.class);
        
    @Inject
    private PopulationParamService populationParamService;
    
    @Inject
    private PopulationParamMapper populationParamMapper;
    
    /**
     * POST  /population-params : Create a new populationParam.
     *
     * @param populationParamDTO the populationParamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new populationParamDTO, or with status 400 (Bad Request) if the populationParam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-params",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationParamDTO> createPopulationParam(@Valid @RequestBody PopulationParamDTO populationParamDTO) throws URISyntaxException {
        log.debug("REST request to save PopulationParam : {}", populationParamDTO);
        if (populationParamDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("populationParam", "idexists", "A new populationParam cannot already have an ID")).body(null);
        }
        PopulationParamDTO result = populationParamService.save(populationParamDTO);
        return ResponseEntity.created(new URI("/api/population-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("populationParam", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /population-params : Updates an existing populationParam.
     *
     * @param populationParamDTO the populationParamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated populationParamDTO,
     * or with status 400 (Bad Request) if the populationParamDTO is not valid,
     * or with status 500 (Internal Server Error) if the populationParamDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-params",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationParamDTO> updatePopulationParam(@Valid @RequestBody PopulationParamDTO populationParamDTO) throws URISyntaxException {
        log.debug("REST request to update PopulationParam : {}", populationParamDTO);
        if (populationParamDTO.getId() == null) {
            return createPopulationParam(populationParamDTO);
        }
        PopulationParamDTO result = populationParamService.save(populationParamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("populationParam", populationParamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /population-params : get all the populationParams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of populationParams in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/population-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationParamDTO>> getAllPopulationParams(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PopulationParams");
        Page<PopulationParam> page = populationParamService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/population-params");
        return new ResponseEntity<>(populationParamMapper.populationParamsToPopulationParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /population-params/:id : get the "id" populationParam.
     *
     * @param id the id of the populationParamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the populationParamDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/population-params/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationParamDTO> getPopulationParam(@PathVariable Long id) {
        log.debug("REST request to get PopulationParam : {}", id);
        PopulationParamDTO populationParamDTO = populationParamService.findOne(id);
        return Optional.ofNullable(populationParamDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /population-params/:id : delete the "id" populationParam.
     *
     * @param id the id of the populationParamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/population-params/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePopulationParam(@PathVariable Long id) {
        log.debug("REST request to delete PopulationParam : {}", id);
        populationParamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("populationParam", id.toString())).build();
    }

    /**
     * SEARCH  /_search/population-params?query=:query : search for the populationParam corresponding
     * to the query.
     *
     * @param query the query of the populationParam search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/population-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationParamDTO>> searchPopulationParams(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of PopulationParams for query {}", query);
        Page<PopulationParam> page = populationParamService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/population-params");
        return new ResponseEntity<>(populationParamMapper.populationParamsToPopulationParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
