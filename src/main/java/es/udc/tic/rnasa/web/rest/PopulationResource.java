/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.Population;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.security.SecurityUtils;
import es.udc.tic.rnasa.service.PopulationService;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.web.rest.dto.*;
import es.udc.tic.rnasa.web.rest.mapper.PopulationMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Population.
 */
@RestController
@RequestMapping("/api")
public class PopulationResource {

    private final Logger log = LoggerFactory.getLogger(PopulationResource.class);

    @Inject
    private PopulationService populationService;

    @Inject
    private PopulationMapper populationMapper;

    @Inject
    private UserRepository userRepository;

    /**
     * POST  /custom-populations : Create a new population.
     *
     * @param customPopulationDTO the populationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new populationDTO, or with status 400 (Bad Request) if the population has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/custom-populations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationDTO> createPopulation(@Valid @RequestBody CustomPopulationDTO customPopulationDTO) throws URISyntaxException {
        log.debug("REST request to save Population : {}", customPopulationDTO);
        if (customPopulationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("population", "idexists", "A new population cannot already have an ID")).body(null);
        }

        try {
            //call here the service
            User owner = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();

            String name = customPopulationDTO.getName();
            String desc = customPopulationDTO.getDescription();
            List<byte []> files = customPopulationDTO.getFiles();

            log.debug("customDatasetDTO: {}", customPopulationDTO);
            log.debug("owner: {}", owner);

            Population population = populationService.createPopulation(owner, name, desc, files);

            PopulationDTO result = populationMapper.populationToPopulationDTO(population);
            return ResponseEntity.created(new URI("/api/populations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("population", result.getName()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * PUT  /custom-populations : Updates an existing population.
     *
     * @param customDescriptionDTO the customDescriptionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated populationDTO,
     * or with status 400 (Bad Request) if the populationDTO is not valid,
     * or with status 500 (Internal Server Error) if the populationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/custom-populations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationDTO> updatePopulation(@Valid @RequestBody CustomDescriptionDTO customDescriptionDTO) throws URISyntaxException {
        log.debug("REST request to update Population : {}", customDescriptionDTO);

        try {
            PopulationDTO result = populationService.update(customDescriptionDTO);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("population", result.getName()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    /**
     * GET  /populations-size : get all the populations.
     *
     * @param pageable the pagination information
     * @param sizeShort short size
     * @param sizeFloat float size
     * @param sizeBool bool size
     * @return the ResponseEntity with status 200 (OK) and the list of populations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/populations-size",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationDTO>> getAllPopulations(Pageable pageable, @RequestParam int sizeShort, @RequestParam int sizeFloat, @RequestParam int sizeBool)
        throws URISyntaxException {
        log.debug("\n\nREST request to get a page of Populations:\n\tPage: {}\n\t{}", pageable, new CustomSizeParamDTO(sizeShort, sizeFloat, sizeBool));
        try {
            Page<Population> page = populationService.findAllBySize(pageable, sizeShort, sizeFloat, sizeBool);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/populations");
            return new ResponseEntity<>(populationMapper.populationsToPopulationDTOs(page.getContent()), headers, HttpStatus.OK);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        }
    }

    /**
     * POST  /population-share : Share a population with other users.
     *
     * @param customShareResourceDTO resource and user to share it
     * @return the ResponseEntity with status 201 (Created) and with body the populationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/population-share",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationDTO> sharePopulation(@Valid @RequestBody CustomShareResourceDTO customShareResourceDTO) throws URISyntaxException {
        log.debug("REST request to save Population : {}", customShareResourceDTO);

        try {
            PopulationDTO result = populationService.share(customShareResourceDTO.getResourceId(), customShareResourceDTO.getLoginname());
            return ResponseEntity.created(new URI("/api/populations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("population", result.getId().toString()))
                .body(result);

        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //
    // below this point have a auto-generated resources
    //

    /**
     * POST  /populations : Create a new population.
     *
     * @param populationDTO the populationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new populationDTO, or with status 400 (Bad Request) if the population has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/populations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationDTO> createPopulation(@Valid @RequestBody PopulationDTO populationDTO) throws URISyntaxException {
        log.debug("REST request to save Population : {}", populationDTO);
        if (populationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("population", "idexists", "A new population cannot already have an ID")).body(null);
        }

        try {
            PopulationDTO result = populationService.save(populationDTO);
            return ResponseEntity.created(new URI("/api/populations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("population", result.getId().toString()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * PUT  /populations : Updates an existing population.
     *
     * @param populationDTO the populationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated populationDTO,
     * or with status 400 (Bad Request) if the populationDTO is not valid,
     * or with status 500 (Internal Server Error) if the populationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/populations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationDTO> updatePopulation(@Valid @RequestBody PopulationDTO populationDTO) throws URISyntaxException {
        log.debug("REST request to update Population : {}", populationDTO);
        if (populationDTO.getId() == null) {
            return createPopulation(populationDTO);
        }

        try {
            PopulationDTO result = populationService.save(populationDTO);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("population", populationDTO.getId().toString()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * GET  /populations : get all the populations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of populations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/populations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationDTO>> getAllPopulations(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Populations");
        try {
            Page<Population> page = populationService.findAll(pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/populations");
            return new ResponseEntity<>(populationMapper.populationsToPopulationDTOs(page.getContent()), headers, HttpStatus.OK);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        }
    }

    /**
     * GET  /populations/:id : get the "id" population.
     *
     * @param id the id of the populationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the populationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/populations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PopulationDTO> getPopulation(@PathVariable Long id) {
        log.debug("REST request to get Population : {}", id);

        try {
            PopulationDTO populationDTO = populationService.findOne(id);
            return Optional.ofNullable(populationDTO)
                .map(result -> new ResponseEntity<>(
                    result,
                    HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * DELETE  /populations/:id : delete the "id" population.
     *
     * @param id the id of the populationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/populations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePopulation(@PathVariable Long id) {
        log.debug("REST request to delete Population : {}", id);

        try {
            populationService.delete(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("population", id.toString())).build();
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * SEARCH  /_search/populations?query=:query : search for the population corresponding
     * to the query.
     *
     * @param query the query of the population search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/populations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PopulationDTO>> searchPopulations(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Populations for query {}", query);

        try {
            Page<Population> page = populationService.search(query, pageable);
            HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/populations");
            return new ResponseEntity<>(populationMapper.populationsToPopulationDTOs(page.getContent()), headers, HttpStatus.OK);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
