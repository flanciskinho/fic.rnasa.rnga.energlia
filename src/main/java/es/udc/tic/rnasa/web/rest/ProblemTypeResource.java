/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.ProblemType;
import es.udc.tic.rnasa.service.ProblemTypeService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.ProblemTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ProblemTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProblemType.
 */
@RestController
@RequestMapping("/api")
public class ProblemTypeResource {

    private final Logger log = LoggerFactory.getLogger(ProblemTypeResource.class);
        
    @Inject
    private ProblemTypeService problemTypeService;
    
    @Inject
    private ProblemTypeMapper problemTypeMapper;
    
    /**
     * POST  /problem-types : Create a new problemType.
     *
     * @param problemTypeDTO the problemTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new problemTypeDTO, or with status 400 (Bad Request) if the problemType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/problem-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProblemTypeDTO> createProblemType(@Valid @RequestBody ProblemTypeDTO problemTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ProblemType : {}", problemTypeDTO);
        if (problemTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("problemType", "idexists", "A new problemType cannot already have an ID")).body(null);
        }
        ProblemTypeDTO result = problemTypeService.save(problemTypeDTO);
        return ResponseEntity.created(new URI("/api/problem-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("problemType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /problem-types : Updates an existing problemType.
     *
     * @param problemTypeDTO the problemTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated problemTypeDTO,
     * or with status 400 (Bad Request) if the problemTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the problemTypeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/problem-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProblemTypeDTO> updateProblemType(@Valid @RequestBody ProblemTypeDTO problemTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ProblemType : {}", problemTypeDTO);
        if (problemTypeDTO.getId() == null) {
            return createProblemType(problemTypeDTO);
        }
        ProblemTypeDTO result = problemTypeService.save(problemTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("problemType", problemTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /problem-types : get all the problemTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of problemTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/problem-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ProblemTypeDTO>> getAllProblemTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ProblemTypes");
        Page<ProblemType> page = problemTypeService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/problem-types");
        return new ResponseEntity<>(problemTypeMapper.problemTypesToProblemTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /problem-types/:id : get the "id" problemType.
     *
     * @param id the id of the problemTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the problemTypeDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/problem-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProblemTypeDTO> getProblemType(@PathVariable Long id) {
        log.debug("REST request to get ProblemType : {}", id);
        ProblemTypeDTO problemTypeDTO = problemTypeService.findOne(id);
        return Optional.ofNullable(problemTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /problem-types/:id : delete the "id" problemType.
     *
     * @param id the id of the problemTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/problem-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteProblemType(@PathVariable Long id) {
        log.debug("REST request to delete ProblemType : {}", id);
        problemTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("problemType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/problem-types?query=:query : search for the problemType corresponding
     * to the query.
     *
     * @param query the query of the problemType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/problem-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ProblemTypeDTO>> searchProblemTypes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ProblemTypes for query {}", query);
        Page<ProblemType> page = problemTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/problem-types");
        return new ResponseEntity<>(problemTypeMapper.problemTypesToProblemTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
