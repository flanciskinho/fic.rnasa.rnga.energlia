/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.QueueType;
import es.udc.tic.rnasa.service.QueueTypeService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing QueueType.
 */
@RestController
@RequestMapping("/api")
public class QueueTypeResource {

    private final Logger log = LoggerFactory.getLogger(QueueTypeResource.class);
        
    @Inject
    private QueueTypeService queueTypeService;
    
    /**
     * POST  /queue-types : Create a new queueType.
     *
     * @param queueType the queueType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new queueType, or with status 400 (Bad Request) if the queueType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/queue-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QueueType> createQueueType(@Valid @RequestBody QueueType queueType) throws URISyntaxException {
        log.debug("REST request to save QueueType : {}", queueType);
        if (queueType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("queueType", "idexists", "A new queueType cannot already have an ID")).body(null);
        }
        QueueType result = queueTypeService.save(queueType);
        return ResponseEntity.created(new URI("/api/queue-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("queueType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /queue-types : Updates an existing queueType.
     *
     * @param queueType the queueType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated queueType,
     * or with status 400 (Bad Request) if the queueType is not valid,
     * or with status 500 (Internal Server Error) if the queueType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/queue-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QueueType> updateQueueType(@Valid @RequestBody QueueType queueType) throws URISyntaxException {
        log.debug("REST request to update QueueType : {}", queueType);
        if (queueType.getId() == null) {
            return createQueueType(queueType);
        }
        QueueType result = queueTypeService.save(queueType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("queueType", queueType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /queue-types : get all the queueTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of queueTypes in body
     */
    @RequestMapping(value = "/queue-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<QueueType> getAllQueueTypes() {
        log.debug("REST request to get all QueueTypes");
        return queueTypeService.findAll();
    }

    /**
     * GET  /queue-types/:id : get the "id" queueType.
     *
     * @param id the id of the queueType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the queueType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/queue-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QueueType> getQueueType(@PathVariable Long id) {
        log.debug("REST request to get QueueType : {}", id);
        QueueType queueType = queueTypeService.findOne(id);
        return Optional.ofNullable(queueType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /queue-types/:id : delete the "id" queueType.
     *
     * @param id the id of the queueType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/queue-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteQueueType(@PathVariable Long id) {
        log.debug("REST request to delete QueueType : {}", id);
        queueTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("queueType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/queue-types?query=:query : search for the queueType corresponding
     * to the query.
     *
     * @param query the query of the queueType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/queue-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<QueueType> searchQueueTypes(@RequestParam String query) {
        log.debug("REST request to search QueueTypes for query {}", query);
        return queueTypeService.search(query);
    }

}
