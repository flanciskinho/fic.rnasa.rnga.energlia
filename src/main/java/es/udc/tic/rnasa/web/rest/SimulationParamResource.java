/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.SimulationParam;
import es.udc.tic.rnasa.service.SimulationParamService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.SimulationParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SimulationParam.
 */
@RestController
@RequestMapping("/api")
public class SimulationParamResource {

    private final Logger log = LoggerFactory.getLogger(SimulationParamResource.class);
        
    @Inject
    private SimulationParamService simulationParamService;
    
    @Inject
    private SimulationParamMapper simulationParamMapper;
    
    /**
     * POST  /simulation-params : Create a new simulationParam.
     *
     * @param simulationParamDTO the simulationParamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simulationParamDTO, or with status 400 (Bad Request) if the simulationParam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulation-params",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationParamDTO> createSimulationParam(@Valid @RequestBody SimulationParamDTO simulationParamDTO) throws URISyntaxException {
        log.debug("REST request to save SimulationParam : {}", simulationParamDTO);
        if (simulationParamDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("simulationParam", "idexists", "A new simulationParam cannot already have an ID")).body(null);
        }
        SimulationParamDTO result = simulationParamService.save(simulationParamDTO);
        return ResponseEntity.created(new URI("/api/simulation-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("simulationParam", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /simulation-params : Updates an existing simulationParam.
     *
     * @param simulationParamDTO the simulationParamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simulationParamDTO,
     * or with status 400 (Bad Request) if the simulationParamDTO is not valid,
     * or with status 500 (Internal Server Error) if the simulationParamDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulation-params",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationParamDTO> updateSimulationParam(@Valid @RequestBody SimulationParamDTO simulationParamDTO) throws URISyntaxException {
        log.debug("REST request to update SimulationParam : {}", simulationParamDTO);
        if (simulationParamDTO.getId() == null) {
            return createSimulationParam(simulationParamDTO);
        }
        SimulationParamDTO result = simulationParamService.save(simulationParamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("simulationParam", simulationParamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /simulation-params : get all the simulationParams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of simulationParams in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/simulation-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SimulationParamDTO>> getAllSimulationParams(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SimulationParams");
        Page<SimulationParam> page = simulationParamService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/simulation-params");
        return new ResponseEntity<>(simulationParamMapper.simulationParamsToSimulationParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /simulation-params/:id : get the "id" simulationParam.
     *
     * @param id the id of the simulationParamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the simulationParamDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/simulation-params/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationParamDTO> getSimulationParam(@PathVariable Long id) {
        log.debug("REST request to get SimulationParam : {}", id);
        SimulationParamDTO simulationParamDTO = simulationParamService.findOne(id);
        return Optional.ofNullable(simulationParamDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /simulation-params/:id : delete the "id" simulationParam.
     *
     * @param id the id of the simulationParamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/simulation-params/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSimulationParam(@PathVariable Long id) {
        log.debug("REST request to delete SimulationParam : {}", id);
        simulationParamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("simulationParam", id.toString())).build();
    }

    /**
     * SEARCH  /_search/simulation-params?query=:query : search for the simulationParam corresponding
     * to the query.
     *
     * @param query the query of the simulationParam search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/simulation-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SimulationParamDTO>> searchSimulationParams(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SimulationParams for query {}", query);
        Page<SimulationParam> page = simulationParamService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/simulation-params");
        return new ResponseEntity<>(simulationParamMapper.simulationParamsToSimulationParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
