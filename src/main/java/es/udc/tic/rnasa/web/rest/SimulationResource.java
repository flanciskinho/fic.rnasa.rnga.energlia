/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.Simulation;
import es.udc.tic.rnasa.service.SimulationService;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.web.rest.dto.CustomDescriptionDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.CustomAngnSearchGliaDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.CustomAnnFixedGliaDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.CustomNuSvmDTO;
import es.udc.tic.rnasa.web.rest.dto.SimulationDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Simulation.
 */
@RestController
@RequestMapping("/api")
public class SimulationResource {

    private final Logger log = LoggerFactory.getLogger(SimulationResource.class);

    @Inject
    private SimulationService simulationService;

    @Inject
    private SimulationMapper simulationMapper;


    /**
     * PUT  /custom-simulations : Updates an existing simulation.
     *
     * @param customDescriptionDTO the customDescriptionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simulationDTO,
     * or with status 400 (Bad Request) if the simulationDTO is not valid,
     * or with status 500 (Internal Server Error) if the simulationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/custom-simulations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationDTO> updateCustomSimulation(@Valid @RequestBody CustomDescriptionDTO customDescriptionDTO) throws URISyntaxException {
        try {
            log.debug("REST request to update Simulation : {}", customDescriptionDTO);

            SimulationDTO result = simulationService.updateSimulation(customDescriptionDTO);

            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("simulation", customDescriptionDTO.getId().toString()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * POST  /simulations-ann-fixed-glia : Create a new simulation.
     *
     * @param customAnnFixedGliaDTO the customAnnFixedGliaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simulationDTO, or with status 400 (Bad Request) if the simulation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulations-ann-fixed-glia",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationDTO> createSimulationAnnFixedGlia(@Valid @RequestBody CustomAnnFixedGliaDTO customAnnFixedGliaDTO) throws URISyntaxException {
        log.debug("REST request to save Simulation : {}", customAnnFixedGliaDTO);

        try {

            SimulationDTO result = simulationService.save(customAnnFixedGliaDTO);
            return ResponseEntity.created(new URI("/api/simulations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("simulation", result.getName()))
                .body(result);

        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * POST  /simulations-angn-search-glia : Create a new simulation.
     *
     * @param customAngnSearchGliaDTO the customAnnFixedGliaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simulationDTO, or with status 400 (Bad Request) if the simulation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulations-angn-search-glia",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationDTO> createSimulationAngnSearchGlia(@Valid @RequestBody CustomAngnSearchGliaDTO customAngnSearchGliaDTO) throws URISyntaxException {
        log.debug("REST request to save Simulation : {}", customAngnSearchGliaDTO);

        try {

            SimulationDTO result = simulationService.save(customAngnSearchGliaDTO);

            return ResponseEntity.created(new URI("/api/simulations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("simulation", result.getId().toString()))
                .body(result);

        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * POST  /simulations-nu-svm : Create a new simulation.
     *
     * @param customNuSvmDTO the customAnnFixedGliaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simulationDTO, or with status 400 (Bad Request) if the simulation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulations-nu-svm",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationDTO> createSimulation(@Valid @RequestBody CustomNuSvmDTO customNuSvmDTO) throws URISyntaxException {
        log.debug("REST request to save Simulation : {}", customNuSvmDTO);

        try {

            SimulationDTO result = simulationService.save(customNuSvmDTO);
            return ResponseEntity.created(new URI("/api/simulations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("simulation", result.getId().toString()))
                .body(result);

        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * POST  /simulations : Create a new simulation.
     *
     * @param simulationDTO the simulationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simulationDTO, or with status 400 (Bad Request) if the simulation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationDTO> createSimulation(@Valid @RequestBody SimulationDTO simulationDTO) throws URISyntaxException {
        log.debug("REST request to save Simulation : {}", simulationDTO);
        if (simulationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("simulation", "idexists", "A new simulation cannot already have an ID")).body(null);
        }

        try {
            SimulationDTO result = simulationService.save(simulationDTO);
            return ResponseEntity.created(new URI("/api/simulations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("simulation", result.getId().toString()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * PUT  /simulations : Updates an existing simulation.
     *
     * @param simulationDTO the simulationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simulationDTO,
     * or with status 400 (Bad Request) if the simulationDTO is not valid,
     * or with status 500 (Internal Server Error) if the simulationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationDTO> updateSimulation(@Valid @RequestBody SimulationDTO simulationDTO) throws URISyntaxException {
        log.debug("REST request to update Simulation : {}", simulationDTO);
        if (simulationDTO.getId() == null) {
            return createSimulation(simulationDTO);
        }

        try {
            SimulationDTO result = simulationService.save(simulationDTO);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("simulation", simulationDTO.getName()))
                .body(result);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * GET  /simulations : get all the simulations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of simulations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/simulations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SimulationDTO>> getAllSimulations(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Simulations");
        Page<Simulation> page = simulationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/simulations");
        return new ResponseEntity<>(simulationMapper.simulationsToSimulationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /simulations/:id : get the "id" simulation.
     *
     * @param id the id of the simulationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the simulationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/simulations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationDTO> getSimulation(@PathVariable Long id) {
        log.debug("REST request to get Simulation : {}", id);

        try {
            SimulationDTO simulationDTO = simulationService.findOne(id);
            return Optional.ofNullable(simulationDTO)
                .map(result -> new ResponseEntity<>(
                    result,
                    HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * DELETE  /simulations/:id : delete the "id" simulation.
     *
     * @param id the id of the simulationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/simulations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSimulation(@PathVariable Long id) {
        log.debug("REST request to delete Simulation : {}", id);

        try {
            simulationService.delete(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("simulation", id.toString())).build();
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * SEARCH  /_search/simulations?query=:query : search for the simulation corresponding
     * to the query.
     *
     * @param query the query of the simulation search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/simulations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SimulationDTO>> searchSimulations(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Simulations for query {}", query);
        try {
            Page<Simulation> page = simulationService.search(query, pageable);
            HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/simulations");
            return new ResponseEntity<>(simulationMapper.simulationsToSimulationDTOs(page.getContent()), headers, HttpStatus.OK);
        } catch (HttpExceptionDetails hed) {
            return new ResponseEntity<>(hed.getStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
