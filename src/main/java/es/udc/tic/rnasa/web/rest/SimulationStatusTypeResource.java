/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.SimulationStatusType;
import es.udc.tic.rnasa.service.SimulationStatusTypeService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.SimulationStatusTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationStatusTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SimulationStatusType.
 */
@RestController
@RequestMapping("/api")
public class SimulationStatusTypeResource {

    private final Logger log = LoggerFactory.getLogger(SimulationStatusTypeResource.class);
        
    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;
    
    @Inject
    private SimulationStatusTypeMapper simulationStatusTypeMapper;
    
    /**
     * POST  /simulation-status-types : Create a new simulationStatusType.
     *
     * @param simulationStatusTypeDTO the simulationStatusTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simulationStatusTypeDTO, or with status 400 (Bad Request) if the simulationStatusType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulation-status-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationStatusTypeDTO> createSimulationStatusType(@Valid @RequestBody SimulationStatusTypeDTO simulationStatusTypeDTO) throws URISyntaxException {
        log.debug("REST request to save SimulationStatusType : {}", simulationStatusTypeDTO);
        if (simulationStatusTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("simulationStatusType", "idexists", "A new simulationStatusType cannot already have an ID")).body(null);
        }
        SimulationStatusTypeDTO result = simulationStatusTypeService.save(simulationStatusTypeDTO);
        return ResponseEntity.created(new URI("/api/simulation-status-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("simulationStatusType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /simulation-status-types : Updates an existing simulationStatusType.
     *
     * @param simulationStatusTypeDTO the simulationStatusTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simulationStatusTypeDTO,
     * or with status 400 (Bad Request) if the simulationStatusTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the simulationStatusTypeDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/simulation-status-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationStatusTypeDTO> updateSimulationStatusType(@Valid @RequestBody SimulationStatusTypeDTO simulationStatusTypeDTO) throws URISyntaxException {
        log.debug("REST request to update SimulationStatusType : {}", simulationStatusTypeDTO);
        if (simulationStatusTypeDTO.getId() == null) {
            return createSimulationStatusType(simulationStatusTypeDTO);
        }
        SimulationStatusTypeDTO result = simulationStatusTypeService.save(simulationStatusTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("simulationStatusType", simulationStatusTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /simulation-status-types : get all the simulationStatusTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of simulationStatusTypes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/simulation-status-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SimulationStatusTypeDTO>> getAllSimulationStatusTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SimulationStatusTypes");
        Page<SimulationStatusType> page = simulationStatusTypeService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/simulation-status-types");
        return new ResponseEntity<>(simulationStatusTypeMapper.simulationStatusTypesToSimulationStatusTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /simulation-status-types/:id : get the "id" simulationStatusType.
     *
     * @param id the id of the simulationStatusTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the simulationStatusTypeDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/simulation-status-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SimulationStatusTypeDTO> getSimulationStatusType(@PathVariable Long id) {
        log.debug("REST request to get SimulationStatusType : {}", id);
        SimulationStatusTypeDTO simulationStatusTypeDTO = simulationStatusTypeService.findOne(id);
        return Optional.ofNullable(simulationStatusTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /simulation-status-types/:id : delete the "id" simulationStatusType.
     *
     * @param id the id of the simulationStatusTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/simulation-status-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSimulationStatusType(@PathVariable Long id) {
        log.debug("REST request to delete SimulationStatusType : {}", id);
        simulationStatusTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("simulationStatusType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/simulation-status-types?query=:query : search for the simulationStatusType corresponding
     * to the query.
     *
     * @param query the query of the simulationStatusType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/simulation-status-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SimulationStatusTypeDTO>> searchSimulationStatusTypes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SimulationStatusTypes for query {}", query);
        Page<SimulationStatusType> page = simulationStatusTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/simulation-status-types");
        return new ResponseEntity<>(simulationStatusTypeMapper.simulationStatusTypesToSimulationStatusTypeDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
