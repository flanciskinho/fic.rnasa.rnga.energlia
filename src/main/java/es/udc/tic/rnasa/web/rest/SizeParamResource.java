/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.SizeParam;
import es.udc.tic.rnasa.service.SizeParamService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.SizeParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.SizeParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SizeParam.
 */
@RestController
@RequestMapping("/api")
public class SizeParamResource {

    private final Logger log = LoggerFactory.getLogger(SizeParamResource.class);
        
    @Inject
    private SizeParamService sizeParamService;
    
    @Inject
    private SizeParamMapper sizeParamMapper;
    
    /**
     * POST  /size-params : Create a new sizeParam.
     *
     * @param sizeParamDTO the sizeParamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sizeParamDTO, or with status 400 (Bad Request) if the sizeParam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/size-params",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SizeParamDTO> createSizeParam(@Valid @RequestBody SizeParamDTO sizeParamDTO) throws URISyntaxException {
        log.debug("REST request to save SizeParam : {}", sizeParamDTO);
        if (sizeParamDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("sizeParam", "idexists", "A new sizeParam cannot already have an ID")).body(null);
        }
        SizeParamDTO result = sizeParamService.save(sizeParamDTO);
        return ResponseEntity.created(new URI("/api/size-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("sizeParam", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /size-params : Updates an existing sizeParam.
     *
     * @param sizeParamDTO the sizeParamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sizeParamDTO,
     * or with status 400 (Bad Request) if the sizeParamDTO is not valid,
     * or with status 500 (Internal Server Error) if the sizeParamDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/size-params",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SizeParamDTO> updateSizeParam(@Valid @RequestBody SizeParamDTO sizeParamDTO) throws URISyntaxException {
        log.debug("REST request to update SizeParam : {}", sizeParamDTO);
        if (sizeParamDTO.getId() == null) {
            return createSizeParam(sizeParamDTO);
        }
        SizeParamDTO result = sizeParamService.save(sizeParamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("sizeParam", sizeParamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /size-params : get all the sizeParams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sizeParams in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/size-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SizeParamDTO>> getAllSizeParams(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SizeParams");
        Page<SizeParam> page = sizeParamService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/size-params");
        return new ResponseEntity<>(sizeParamMapper.sizeParamsToSizeParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /size-params/:id : get the "id" sizeParam.
     *
     * @param id the id of the sizeParamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sizeParamDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/size-params/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SizeParamDTO> getSizeParam(@PathVariable Long id) {
        log.debug("REST request to get SizeParam : {}", id);
        SizeParamDTO sizeParamDTO = sizeParamService.findOne(id);
        return Optional.ofNullable(sizeParamDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /size-params/:id : delete the "id" sizeParam.
     *
     * @param id the id of the sizeParamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/size-params/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSizeParam(@PathVariable Long id) {
        log.debug("REST request to delete SizeParam : {}", id);
        sizeParamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("sizeParam", id.toString())).build();
    }

    /**
     * SEARCH  /_search/size-params?query=:query : search for the sizeParam corresponding
     * to the query.
     *
     * @param query the query of the sizeParam search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/size-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SizeParamDTO>> searchSizeParams(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SizeParams for query {}", query);
        Page<SizeParam> page = sizeParamService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/size-params");
        return new ResponseEntity<>(sizeParamMapper.sizeParamsToSizeParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
