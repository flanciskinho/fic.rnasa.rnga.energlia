/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.SizeType;
import es.udc.tic.rnasa.service.SizeTypeService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SizeType.
 */
@RestController
@RequestMapping("/api")
public class SizeTypeResource {

    private final Logger log = LoggerFactory.getLogger(SizeTypeResource.class);
        
    @Inject
    private SizeTypeService sizeTypeService;
    
    /**
     * POST  /size-types : Create a new sizeType.
     *
     * @param sizeType the sizeType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sizeType, or with status 400 (Bad Request) if the sizeType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/size-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SizeType> createSizeType(@Valid @RequestBody SizeType sizeType) throws URISyntaxException {
        log.debug("REST request to save SizeType : {}", sizeType);
        if (sizeType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("sizeType", "idexists", "A new sizeType cannot already have an ID")).body(null);
        }
        SizeType result = sizeTypeService.save(sizeType);
        return ResponseEntity.created(new URI("/api/size-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("sizeType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /size-types : Updates an existing sizeType.
     *
     * @param sizeType the sizeType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sizeType,
     * or with status 400 (Bad Request) if the sizeType is not valid,
     * or with status 500 (Internal Server Error) if the sizeType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/size-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SizeType> updateSizeType(@Valid @RequestBody SizeType sizeType) throws URISyntaxException {
        log.debug("REST request to update SizeType : {}", sizeType);
        if (sizeType.getId() == null) {
            return createSizeType(sizeType);
        }
        SizeType result = sizeTypeService.save(sizeType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("sizeType", sizeType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /size-types : get all the sizeTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sizeTypes in body
     */
    @RequestMapping(value = "/size-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SizeType> getAllSizeTypes() {
        log.debug("REST request to get all SizeTypes");
        return sizeTypeService.findAll();
    }

    /**
     * GET  /size-types/:id : get the "id" sizeType.
     *
     * @param id the id of the sizeType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sizeType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/size-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SizeType> getSizeType(@PathVariable Long id) {
        log.debug("REST request to get SizeType : {}", id);
        SizeType sizeType = sizeTypeService.findOne(id);
        return Optional.ofNullable(sizeType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /size-types/:id : delete the "id" sizeType.
     *
     * @param id the id of the sizeType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/size-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSizeType(@PathVariable Long id) {
        log.debug("REST request to delete SizeType : {}", id);
        sizeTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("sizeType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/size-types?query=:query : search for the sizeType corresponding
     * to the query.
     *
     * @param query the query of the sizeType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/size-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SizeType> searchSizeTypes(@RequestParam String query) {
        log.debug("REST request to search SizeTypes for query {}", query);
        return sizeTypeService.search(query);
    }

}
