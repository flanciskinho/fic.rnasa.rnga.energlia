/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.SpecificSimulation;
import es.udc.tic.rnasa.service.SpecificSimulationService;
import es.udc.tic.rnasa.web.rest.dto.SpecificSimulationDTO;
import es.udc.tic.rnasa.web.rest.mapper.SpecificSimulationMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SpecificSimulation.
 */
@RestController
@RequestMapping("/api")
public class SpecificSimulationResource {

    private final Logger log = LoggerFactory.getLogger(SpecificSimulationResource.class);

    @Inject
    private SpecificSimulationService specificSimulationService;

    @Inject
    private SpecificSimulationMapper specificSimulationMapper;

    /**
     * POST  /specific-simulations : Create a new specificSimulation.
     *
     * @param specificSimulationDTO the specificSimulationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new specificSimulationDTO, or with status 400 (Bad Request) if the specificSimulation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/specific-simulations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SpecificSimulationDTO> createSpecificSimulation(@RequestBody SpecificSimulationDTO specificSimulationDTO) throws URISyntaxException {
        log.debug("REST request to save SpecificSimulation : {}", specificSimulationDTO);
        if (specificSimulationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("specificSimulation", "idexists", "A new specificSimulation cannot already have an ID")).body(null);
        }
        SpecificSimulationDTO result = specificSimulationService.save(specificSimulationDTO);
        return ResponseEntity.created(new URI("/api/specific-simulations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("specificSimulation", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /specific-simulations : Updates an existing specificSimulation.
     *
     * @param specificSimulationDTO the specificSimulationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated specificSimulationDTO,
     * or with status 400 (Bad Request) if the specificSimulationDTO is not valid,
     * or with status 500 (Internal Server Error) if the specificSimulationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/specific-simulations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SpecificSimulationDTO> updateSpecificSimulation(@RequestBody SpecificSimulationDTO specificSimulationDTO) throws URISyntaxException {
        log.debug("REST request to update SpecificSimulation : {}", specificSimulationDTO);
        if (specificSimulationDTO.getId() == null) {
            return createSpecificSimulation(specificSimulationDTO);
        }
        SpecificSimulationDTO result = specificSimulationService.save(specificSimulationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("specificSimulation", specificSimulationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /specific-simulations : get all the specificSimulations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of specificSimulations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/specific-simulations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SpecificSimulationDTO>> getAllSpecificSimulations(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SpecificSimulations");
        Page<SpecificSimulation> page = specificSimulationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/specific-simulations");
        return new ResponseEntity<>(specificSimulationMapper.specificSimulationsToSpecificSimulationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /specific-simulations-by-status/:id : get all the specificSimulations filter by status
     *
     * @param id the id of status
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of specificSimulations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/specific-simulations-by-status/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SpecificSimulationDTO>> getAllSpecificSimulationsByStatusId(@PathVariable Long id, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SpecificSimulations");
        Page<SpecificSimulation> page = specificSimulationService.findAllByStatusIdOrderById(id,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/specific-simulations");
        return new ResponseEntity<>(specificSimulationMapper.specificSimulationsToSpecificSimulationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /specific-simulations/:id : get the "id" specificSimulation.
     *
     * @param id the id of the specificSimulationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the specificSimulationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/specific-simulations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SpecificSimulationDTO> getSpecificSimulation(@PathVariable Long id) {
        log.debug("REST request to get SpecificSimulation : {}", id);
        SpecificSimulationDTO specificSimulationDTO = specificSimulationService.findOne(id);
        return Optional.ofNullable(specificSimulationDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /specific-simulations/:id : delete the "id" specificSimulation.
     *
     * @param id the id of the specificSimulationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/specific-simulations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSpecificSimulation(@PathVariable Long id) {
        log.debug("REST request to delete SpecificSimulation : {}", id);
        specificSimulationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("specificSimulation", id.toString())).build();
    }

    /**
     * SEARCH  /_search/specific-simulations?query=:query : search for the specificSimulation corresponding
     * to the query.
     *
     * @param query the query of the specificSimulation search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/specific-simulations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SpecificSimulationDTO>> searchSpecificSimulations(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SpecificSimulations for query {}", query);
        Page<SpecificSimulation> page = specificSimulationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/specific-simulations");
        return new ResponseEntity<>(specificSimulationMapper.specificSimulationsToSpecificSimulationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
