/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.SshAccount;
import es.udc.tic.rnasa.service.SshAccountService;
import es.udc.tic.rnasa.web.rest.dto.SshAccountDTO;
import es.udc.tic.rnasa.web.rest.mapper.SshAccountMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SshAccount.
 */
@RestController
@RequestMapping("/api")
public class SshAccountResource {

    private final Logger log = LoggerFactory.getLogger(SshAccountResource.class);

    @Inject
    private SshAccountService sshAccountService;

    @Inject
    private SshAccountMapper sshAccountMapper;

    /**
     * POST  /ssh-accounts : Create a new sshAccount.
     *
     * @param sshAccountDTO the sshAccountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sshAccountDTO, or with status 400 (Bad Request) if the sshAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ssh-accounts",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SshAccountDTO> createSshAccount(@Valid @RequestBody SshAccountDTO sshAccountDTO) throws URISyntaxException {
        log.debug("REST request to save SshAccount : {}", sshAccountDTO);
        if (sshAccountDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("sshAccount", "idexists", "A new sshAccount cannot already have an ID")).body(null);
        }
        SshAccountDTO result = sshAccountService.save(sshAccountDTO);
        return ResponseEntity.created(new URI("/api/ssh-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("sshAccount", result.getUsername()))
            .body(result);
    }

    /**
     * PUT  /ssh-accounts : Updates an existing sshAccount.
     *
     * @param sshAccountDTO the sshAccountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sshAccountDTO,
     * or with status 400 (Bad Request) if the sshAccountDTO is not valid,
     * or with status 500 (Internal Server Error) if the sshAccountDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ssh-accounts",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SshAccountDTO> updateSshAccount(@Valid @RequestBody SshAccountDTO sshAccountDTO) throws URISyntaxException {
        log.debug("REST request to update SshAccount : {}", sshAccountDTO);
        if (sshAccountDTO.getId() == null) {
            return createSshAccount(sshAccountDTO);
        }
        SshAccountDTO result = sshAccountService.save(sshAccountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("sshAccount", sshAccountDTO.getUsername()))
            .body(result);
    }

    /**
     * GET  /ssh-accounts : get all the sshAccounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sshAccounts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/ssh-accounts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SshAccountDTO>> getAllSshAccounts(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SshAccounts");
        Page<SshAccount> page = sshAccountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ssh-accounts");
        return new ResponseEntity<>(sshAccountMapper.sshAccountsToSshAccountDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /ssh-accounts/:id : get the "id" sshAccount.
     *
     * @param id the id of the sshAccountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sshAccountDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/ssh-accounts/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SshAccountDTO> getSshAccount(@PathVariable Long id) {
        log.debug("REST request to get SshAccount : {}", id);
        SshAccountDTO sshAccountDTO = sshAccountService.findOne(id);
        return Optional.ofNullable(sshAccountDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /ssh-accounts/:id : delete the "id" sshAccount.
     *
     * @param id the id of the sshAccountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/ssh-accounts/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSshAccount(@PathVariable Long id) {
        log.debug("REST request to delete SshAccount : {}", id);
        sshAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("sshAccount", id.toString())).build();
    }

    /**
     * SEARCH  /_search/ssh-accounts?query=:query : search for the sshAccount corresponding
     * to the query.
     *
     * @param query the query of the sshAccount search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/ssh-accounts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SshAccountDTO>> searchSshAccounts(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SshAccounts for query {}", query);
        Page<SshAccount> page = sshAccountService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/ssh-accounts");
        return new ResponseEntity<>(sshAccountMapper.sshAccountsToSshAccountDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
