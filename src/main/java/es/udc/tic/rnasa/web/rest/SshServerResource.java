/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.service.SshServerService;
import es.udc.tic.rnasa.web.rest.dto.SshServerDTO;
import es.udc.tic.rnasa.web.rest.mapper.SshServerMapper;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SshServer.
 */
@RestController
@RequestMapping("/api")
public class SshServerResource {

    private final Logger log = LoggerFactory.getLogger(SshServerResource.class);

    @Inject
    private SshServerService sshServerService;

    @Inject
    private SshServerMapper sshServerMapper;

    /**
     * POST  /ssh-servers : Create a new sshServer.
     *
     * @param sshServerDTO the sshServerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sshServerDTO, or with status 400 (Bad Request) if the sshServer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ssh-servers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SshServerDTO> createSshServer(@Valid @RequestBody SshServerDTO sshServerDTO) throws URISyntaxException {
        log.debug("REST request to save SshServer : {}", sshServerDTO);
        if (sshServerDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("sshServer", "idexists", "A new sshServer cannot already have an ID")).body(null);
        }
        SshServerDTO result = sshServerService.save(sshServerDTO);
        return ResponseEntity.created(new URI("/api/ssh-servers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("sshServer", result.getDnsname()))
            .body(result);
    }

    /**
     * PUT  /ssh-servers : Updates an existing sshServer.
     *
     * @param sshServerDTO the sshServerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sshServerDTO,
     * or with status 400 (Bad Request) if the sshServerDTO is not valid,
     * or with status 500 (Internal Server Error) if the sshServerDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/ssh-servers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SshServerDTO> updateSshServer(@Valid @RequestBody SshServerDTO sshServerDTO) throws URISyntaxException {
        log.debug("REST request to update SshServer : {}", sshServerDTO);
        if (sshServerDTO.getId() == null) {
            return createSshServer(sshServerDTO);
        }
        SshServerDTO result = sshServerService.save(sshServerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("sshServer", sshServerDTO.getDnsname()))
            .body(result);
    }

    /**
     * GET  /ssh-servers : get all the sshServers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sshServers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/ssh-servers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SshServerDTO>> getAllSshServers(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SshServers");
        Page<SshServer> page = sshServerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ssh-servers");
        return new ResponseEntity<>(sshServerMapper.sshServersToSshServerDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /ssh-servers-activated : get all the sshServers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sshServers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/ssh-servers-activated",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SshServerDTO>> getAllSshServersActivated(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SshServers");
        Page<SshServer> page = sshServerService.findAllByActiveIsTrue(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ssh-servers-activated");
        return new ResponseEntity<>(sshServerMapper.sshServersToSshServerDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /ssh-servers/:id : get the "id" sshServer.
     *
     * @param id the id of the sshServerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sshServerDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/ssh-servers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SshServerDTO> getSshServer(@PathVariable Long id) {
        log.debug("REST request to get SshServer : {}", id);
        SshServerDTO sshServerDTO = sshServerService.findOne(id);
        return Optional.ofNullable(sshServerDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /ssh-servers/:id : delete the "id" sshServer.
     *
     * @param id the id of the sshServerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/ssh-servers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSshServer(@PathVariable Long id) {
        log.debug("REST request to delete SshServer : {}", id);
        sshServerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("sshServer", id.toString())).build();
    }

    /**
     * SEARCH  /_search/ssh-servers?query=:query : search for the sshServer corresponding
     * to the query.
     *
     * @param query the query of the sshServer search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/ssh-servers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SshServerDTO>> searchSshServers(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of SshServers for query {}", query);
        Page<SshServer> page = sshServerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/ssh-servers");
        return new ResponseEntity<>(sshServerMapper.sshServersToSshServerDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
