/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.tic.rnasa.domain.TechniqueParam;
import es.udc.tic.rnasa.service.TechniqueParamService;
import es.udc.tic.rnasa.web.rest.util.HeaderUtil;
import es.udc.tic.rnasa.web.rest.util.PaginationUtil;
import es.udc.tic.rnasa.web.rest.dto.TechniqueParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.TechniqueParamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TechniqueParam.
 */
@RestController
@RequestMapping("/api")
public class TechniqueParamResource {

    private final Logger log = LoggerFactory.getLogger(TechniqueParamResource.class);
        
    @Inject
    private TechniqueParamService techniqueParamService;
    
    @Inject
    private TechniqueParamMapper techniqueParamMapper;
    
    /**
     * POST  /technique-params : Create a new techniqueParam.
     *
     * @param techniqueParamDTO the techniqueParamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new techniqueParamDTO, or with status 400 (Bad Request) if the techniqueParam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/technique-params",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TechniqueParamDTO> createTechniqueParam(@Valid @RequestBody TechniqueParamDTO techniqueParamDTO) throws URISyntaxException {
        log.debug("REST request to save TechniqueParam : {}", techniqueParamDTO);
        if (techniqueParamDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("techniqueParam", "idexists", "A new techniqueParam cannot already have an ID")).body(null);
        }
        TechniqueParamDTO result = techniqueParamService.save(techniqueParamDTO);
        return ResponseEntity.created(new URI("/api/technique-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("techniqueParam", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /technique-params : Updates an existing techniqueParam.
     *
     * @param techniqueParamDTO the techniqueParamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated techniqueParamDTO,
     * or with status 400 (Bad Request) if the techniqueParamDTO is not valid,
     * or with status 500 (Internal Server Error) if the techniqueParamDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/technique-params",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TechniqueParamDTO> updateTechniqueParam(@Valid @RequestBody TechniqueParamDTO techniqueParamDTO) throws URISyntaxException {
        log.debug("REST request to update TechniqueParam : {}", techniqueParamDTO);
        if (techniqueParamDTO.getId() == null) {
            return createTechniqueParam(techniqueParamDTO);
        }
        TechniqueParamDTO result = techniqueParamService.save(techniqueParamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("techniqueParam", techniqueParamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /technique-params : get all the techniqueParams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of techniqueParams in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/technique-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<TechniqueParamDTO>> getAllTechniqueParams(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TechniqueParams");
        Page<TechniqueParam> page = techniqueParamService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/technique-params");
        return new ResponseEntity<>(techniqueParamMapper.techniqueParamsToTechniqueParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /technique-params/:id : get the "id" techniqueParam.
     *
     * @param id the id of the techniqueParamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the techniqueParamDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/technique-params/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TechniqueParamDTO> getTechniqueParam(@PathVariable Long id) {
        log.debug("REST request to get TechniqueParam : {}", id);
        TechniqueParamDTO techniqueParamDTO = techniqueParamService.findOne(id);
        return Optional.ofNullable(techniqueParamDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /technique-params/:id : delete the "id" techniqueParam.
     *
     * @param id the id of the techniqueParamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/technique-params/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTechniqueParam(@PathVariable Long id) {
        log.debug("REST request to delete TechniqueParam : {}", id);
        techniqueParamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("techniqueParam", id.toString())).build();
    }

    /**
     * SEARCH  /_search/technique-params?query=:query : search for the techniqueParam corresponding
     * to the query.
     *
     * @param query the query of the techniqueParam search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/technique-params",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<TechniqueParamDTO>> searchTechniqueParams(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TechniqueParams for query {}", query);
        Page<TechniqueParam> page = techniqueParamService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/technique-params");
        return new ResponseEntity<>(techniqueParamMapper.techniqueParamsToTechniqueParamDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
