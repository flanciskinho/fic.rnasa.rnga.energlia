/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the ArchType entity.
 */
public class ArchTypeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;


    @NotNull
    private Boolean activated;


    private Long belongId;

    private String belongDnsname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long sshServerId) {
        this.belongId = sshServerId;
    }

    public String getBelongDnsname() {
        return belongDnsname;
    }

    public void setBelongDnsname(String belongDnsname) {
        this.belongDnsname = belongDnsname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ArchTypeDTO archTypeDTO = (ArchTypeDTO) o;

        if ( ! Objects.equals(id, archTypeDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ArchTypeDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", activated='" + activated + "'" +
            '}';
    }
}
