/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the CompiledApp entity.
 */
public class CompiledAppDTO implements Serializable {

    private Long id;

    @NotNull
    @Lob
    private byte[] executable;

    private String executableContentType;

    private Long belongId;
    private String belongName;
    private Long useId;
    private String useDnsname;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public byte[] getExecutable() {
        return executable;
    }

    public void setExecutable(byte[] executable) {
        this.executable = executable;
    }

    public String getExecutableContentType() {
        return executableContentType;
    }

    public void setExecutableContentType(String executableContentType) {
        this.executableContentType = executableContentType;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long applicationId) {
        this.belongId = applicationId;
    }

    public String getBelongName() {
        return belongName;
    }

    public void setBelongName(String belongName) {
        this.belongName = belongName;
    }

    public String getUseDnsname() {
        return useDnsname;
    }

    public void setUseDnsname(String useDnsname) {
        this.useDnsname = useDnsname;
    }

    public Long getUseId() {
        return useId;
    }

    public void setUseId(Long sshServerId) {
        this.useId = sshServerId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CompiledAppDTO compiledAppDTO = (CompiledAppDTO) o;

        if ( ! Objects.equals(id, compiledAppDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CompiledAppDTO{" +
            "id=" + id +
            ", executable='" + executable + "'" +
            '}';
    }
}
