/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import es.udc.tic.rnasa.domain.util.Recognizable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flanciskinho on 10/5/16.
 */
public class CustomDatasetDTO implements Recognizable {
    private Long id;

    private String name;
    private String description;

    private Long analyzeId;

    private List<byte[]> trainfilesin;
    private List<byte[]> trainfilesout;

    private List<byte[]> validationfilesin;
    private List<byte[]> validationfilesout;

    private List<byte[]> testfilesin;
    private List<byte[]> testfilesout;


    public CustomDatasetDTO() {
    }

    public CustomDatasetDTO(Long id, String name, String description, Long analizeId, List<byte[]> trainfilesin, List<byte[]> trainfilesout) {
        this(id, name, description, analizeId, trainfilesin, trainfilesout, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList());
    }

    public CustomDatasetDTO(Long id, String name, String description, Long analizeId,List<byte[]> trainfilesin, List<byte[]> trainfilesout, List<byte[]> validationfilesin, List<byte[]> validationfilesout) {
        this(id, name, description, analizeId, trainfilesin, trainfilesout, validationfilesin, validationfilesout, new ArrayList<>(), new ArrayList());
    }

    public CustomDatasetDTO(Long id, String name, String description, Long analizeId, List<byte[]> trainfilesin, List<byte[]> trainfilesout, List<byte[]> validationfilesin, List<byte[]> validationfilesout, List<byte[]> testfilesin, List<byte[]> testfilesout) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.analyzeId = analizeId;
        this.trainfilesin = trainfilesin;
        this.trainfilesout = trainfilesout;
        this.validationfilesin = validationfilesin;
        this.validationfilesout = validationfilesout;
        this.testfilesin = testfilesin;
        this.testfilesout = testfilesout;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAnalyzeId() {
        return analyzeId;
    }

    public void setAnalyzeId(Long analyzeId) {
        this.analyzeId = analyzeId;
    }

    public List<byte[]> getTrainfilesin() {
        return trainfilesin;
    }

    public void setTrainfilesin(List<byte[]> trainfilesin) {
        this.trainfilesin = trainfilesin;
    }

    public List<byte[]> getTrainfilesout() {
        return trainfilesout;
    }

    public void setTrainfilesout(List<byte[]> trainfilesout) {
        this.trainfilesout = trainfilesout;
    }

    public List<byte[]> getValidationfilesin() {
        return validationfilesin;
    }

    public void setValidationfilesin(List<byte[]> validationfilesin) {
        this.validationfilesin = validationfilesin;
    }

    public List<byte[]> getValidationfilesout() {
        return validationfilesout;
    }

    public void setValidationfilesout(List<byte[]> validationfilesout) {
        this.validationfilesout = validationfilesout;
    }

    public List<byte[]> getTestfilesin() {
        return testfilesin;
    }

    public void setTestfilesin(List<byte[]> testfilesin) {
        this.testfilesin = testfilesin;
    }

    public List<byte[]> getTestfilesout() {
        return testfilesout;
    }

    public void setTestfilesout(List<byte[]> testfilesout) {
        this.testfilesout = testfilesout;
    }


    @Override
    public String toString() {
        return "CustomDatasetDTO{" +
            "id=" + id +  "'" +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", analyzeId='" + analyzeId + "'" +
            ", trainfilesin=" + trainfilesin +
            ", trainfilesout=" + trainfilesout +
            ", validationfilesin=" + validationfilesin +
            ", validationfilesout=" + validationfilesout +
            ", testfilesin=" + testfilesin +
            ", testfilesout=" + testfilesout +
            '}';
    }
}
