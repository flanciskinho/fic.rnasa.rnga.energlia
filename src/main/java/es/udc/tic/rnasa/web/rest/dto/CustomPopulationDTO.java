/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import es.udc.tic.rnasa.domain.util.Recognizable;

import java.util.List;

/**
 * Created by flanciskinho on 15/5/16.
 */
public class CustomPopulationDTO implements Recognizable {
    private Long id;

    private String name;
    private String description;

    private List<byte[]> files;

    public CustomPopulationDTO() {
    }

    public CustomPopulationDTO(Long id, String name, String description, List<byte[]> files) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.files = files;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<byte[]> getFiles() {
        return files;
    }

    public void setFiles(List<byte[]> files) {
        this.files = files;
    }

    @Override
    public String toString() {
        String str = "";

        int cnt;
        for (cnt = 0; cnt < files.size(); cnt++) {
            str = str + new String(files.get(cnt));
        }

        return "CustomPopulationDTO{" +
            "description='" + description + '\'' +
            ", name='" + name + '\'' +
            ", id=" + id +
            ", files= [" + str + "]" +
            '}';
    }
}
