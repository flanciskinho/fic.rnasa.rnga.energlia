/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 9/9/16.
 */
public class CustomShareResourceDTO {

    @NotNull
    private Long resourceId;
    @NotNull
    private String loginname;

    public CustomShareResourceDTO() {}

    public CustomShareResourceDTO(Long resourceId, String loginname) {
        this.resourceId = resourceId;
        this.loginname = loginname;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    @Override
    public String toString() {
        return "CustomShareResourceDTO{" +
            "resourceId=" + resourceId +
            ", loginname='" + loginname + '\'' +
            '}';
    }
}
