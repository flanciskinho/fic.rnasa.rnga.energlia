/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

/**
 * Created by flanciskinho on 31/5/16.
 */
public class CustomSizeParamDTO {
    private int sizeShort;
    private int sizeFloat;

    private int sizeBool;

    public CustomSizeParamDTO() {
    }

    public CustomSizeParamDTO(int sizeShort, int sizeFloat, int sizeBool) {
        this.sizeShort = sizeShort;
        this.sizeFloat = sizeFloat;
        this.sizeBool = sizeBool;
    }

    public int getSizeShort() {
        return sizeShort;
    }

    public void setSizeShort(int sizeShort) {
        this.sizeShort = sizeShort;
    }

    public int getSizeFloat() {
        return sizeFloat;
    }

    public void setSizeFloat(int sizeFloat) {
        this.sizeFloat = sizeFloat;
    }

    public int getSizeBool() {
        return sizeBool;
    }

    public void setSizeBool(int sizeBool) {
        this.sizeBool = sizeBool;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomSizeParamDTO that = (CustomSizeParamDTO) o;

        if (sizeShort != that.sizeShort) return false;
        if (sizeFloat != that.sizeFloat) return false;
        return sizeBool == that.sizeBool;

    }

    @Override
    public int hashCode() {
        int result = sizeShort;
        result = 31 * result + sizeFloat;
        result = 31 * result + sizeBool;
        return result;
    }


    @Override
    public String toString() {
        return "CustomSizeParamDTO{" +
            "sizeShort=" + sizeShort +
            ", sizeFloat=" + sizeFloat +
            ", sizeBool=" + sizeBool +
            '}';
    }
}
