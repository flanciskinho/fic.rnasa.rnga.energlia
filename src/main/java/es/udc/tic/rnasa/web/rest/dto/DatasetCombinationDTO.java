/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the DatasetCombination entity.
 */
public class DatasetCombinationDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer indexCombination;


    @NotNull
    @Lob
    private byte[] trainfilein;

    private String trainfileinContentType;

    @NotNull
    @Lob
    private byte[] trainfileout;

    private String trainfileoutContentType;

    @Lob
    private byte[] validationfilein;

    private String validationfileinContentType;

    @Lob
    private byte[] validationfileout;

    private String validationfileoutContentType;

    @Lob
    private byte[] testfilein;

    private String testfileinContentType;

    @Lob
    private byte[] testfileout;

    private String testfileoutContentType;

    private Long belongId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getIndexCombination() {
        return indexCombination;
    }

    public void setIndexCombination(Integer indexCombination) {
        this.indexCombination = indexCombination;
    }
    public byte[] getTrainfilein() {
        return trainfilein;
    }

    public void setTrainfilein(byte[] trainfilein) {
        this.trainfilein = trainfilein;
    }

    public String getTrainfileinContentType() {
        return trainfileinContentType;
    }

    public void setTrainfileinContentType(String trainfileinContentType) {
        this.trainfileinContentType = trainfileinContentType;
    }
    public byte[] getTrainfileout() {
        return trainfileout;
    }

    public void setTrainfileout(byte[] trainfileout) {
        this.trainfileout = trainfileout;
    }

    public String getTrainfileoutContentType() {
        return trainfileoutContentType;
    }

    public void setTrainfileoutContentType(String trainfileoutContentType) {
        this.trainfileoutContentType = trainfileoutContentType;
    }
    public byte[] getValidationfilein() {
        return validationfilein;
    }

    public void setValidationfilein(byte[] validationfilein) {
        this.validationfilein = validationfilein;
    }

    public String getValidationfileinContentType() {
        return validationfileinContentType;
    }

    public void setValidationfileinContentType(String validationfileinContentType) {
        this.validationfileinContentType = validationfileinContentType;
    }
    public byte[] getValidationfileout() {
        return validationfileout;
    }

    public void setValidationfileout(byte[] validationfileout) {
        this.validationfileout = validationfileout;
    }

    public String getValidationfileoutContentType() {
        return validationfileoutContentType;
    }

    public void setValidationfileoutContentType(String validationfileoutContentType) {
        this.validationfileoutContentType = validationfileoutContentType;
    }
    public byte[] getTestfilein() {
        return testfilein;
    }

    public void setTestfilein(byte[] testfilein) {
        this.testfilein = testfilein;
    }

    public String getTestfileinContentType() {
        return testfileinContentType;
    }

    public void setTestfileinContentType(String testfileinContentType) {
        this.testfileinContentType = testfileinContentType;
    }
    public byte[] getTestfileout() {
        return testfileout;
    }

    public void setTestfileout(byte[] testfileout) {
        this.testfileout = testfileout;
    }

    public String getTestfileoutContentType() {
        return testfileoutContentType;
    }

    public void setTestfileoutContentType(String testfileoutContentType) {
        this.testfileoutContentType = testfileoutContentType;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long datasetId) {
        this.belongId = datasetId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DatasetCombinationDTO datasetCombinationDTO = (DatasetCombinationDTO) o;

        if ( ! Objects.equals(id, datasetCombinationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DatasetCombinationDTO{" +
            "id=" + id +
            ", indexCombination='" + indexCombination + "'" +
            ", trainfilein='" + trainfilein + "'" +
            ", trainfileout='" + trainfileout + "'" +
            ", validationfilein='" + validationfilein + "'" +
            ", validationfileout='" + validationfileout + "'" +
            ", testfilein='" + testfilein + "'" +
            ", testfileout='" + testfileout + "'" +
            '}';
    }
}
