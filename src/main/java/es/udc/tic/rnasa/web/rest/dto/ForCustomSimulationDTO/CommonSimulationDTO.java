/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 4/7/16.
 */
public class CommonSimulationDTO {

    @NotNull
    private String name;

    private String description;

    @NotNull
    private Long datasetId;

    @NotNull
    private Long problemId;

    @NotNull
    private Long errorId;

    @NotNull
    private Long serverId;

    public CommonSimulationDTO(){}

    public CommonSimulationDTO(String name, String description, Long datasetId, Long problemId, Long errorId, Long serverId) {
        this.name = name;
        this.description = description;
        this.datasetId = datasetId;
        this.problemId = problemId;
        this.errorId = errorId;
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(Long datasetId) {
        this.datasetId = datasetId;
    }

    public Long getProblemId() {
        return problemId;
    }

    public void setProblemId(Long problemId) {
        this.problemId = problemId;
    }

    public Long getErrorId() {
        return errorId;
    }

    public void setErrorId(Long errorId) {
        this.errorId = errorId;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    @Override
    public String toString() {
        return "CommonSimulationDTO{" +
            "name='" + name + '\'' +
            ((description == null)? "":", description='" + description + '\'') +
            ", datasetId=" + datasetId +
            ", problemId=" + problemId +
            ", errorId=" + errorId +
            ", serverId=" + serverId +
            '}';
    }
}
