/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import es.udc.tic.rnasa.domain.util.Recognizable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by flanciskinho on 9/6/16.
 */
public class CustomAngnSearchGliaDTO implements Recognizable, CustomSimulationVariablesDTO{

    // Common
    private Long id;

    @NotNull
    private CommonSimulationDTO detail;

    @NotNull
    private SimExecDTO execution;

    // Specific
    @NotNull
    private Integer layer;

    @NotNull
    @Size(min = 1)
    private List<Integer> neuron;
    @NotNull
    @Size(min = 1)
    private List<String> activationFunction;

    @NotNull
    private Boolean sameGliaAlgorithm;
    @NotNull
    private Boolean decreaseGreatherThanIncrease;

    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coWeight;       //min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Integer> coIteration;  //min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Integer> coActivation; //min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coIncrease;     //min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coDecrease;     //min, max


    // GA
    @NotNull
    private SimGaBasicDTO gaBasic;

    @NotNull
    private SimGaDTO ga;

    // GA
    @NotNull
    @Size(min = 2, max = 2)
    private SimPopulationDTO populations[];


    public SimGaBasicDTO getGaBasic() {
        return gaBasic;
    }

    public void setGaBasic(SimGaBasicDTO gaBasic) {
        this.gaBasic = gaBasic;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CommonSimulationDTO getDetail() {
        return detail;
    }

    public void setDetail(CommonSimulationDTO detail) {
        this.detail = detail;
    }

    public SimExecDTO getExecution() {
        return execution;
    }

    public void setExecution(SimExecDTO execution) {
        this.execution = execution;
    }

    public Integer getLayer() {
        return layer;
    }

    public void setLayer(Integer layer) {
        this.layer = layer;
    }

    public List<Integer> getNeuron() {
        return neuron;
    }

    public void setNeuron(List<Integer> neuron) {
        this.neuron = neuron;
    }

    public List<String> getActivationFunction() {
        return activationFunction;
    }

    public void setActivationFunction(List<String> activationFunction) {
        this.activationFunction = activationFunction;
    }

    public Boolean getSameGliaAlgorithm() {
        return sameGliaAlgorithm;
    }

    public void setSameGliaAlgorithm(Boolean sameGliaAlgorithm) {
        this.sameGliaAlgorithm = sameGliaAlgorithm;
    }

    public Boolean getDecreaseGreatherThanIncrease() {
        return decreaseGreatherThanIncrease;
    }

    public void setDecreaseGreatherThanIncrease(Boolean decreaseGreatherThanIncrease) {
        this.decreaseGreatherThanIncrease = decreaseGreatherThanIncrease;
    }

    public List<Float> getCoWeight() {
        return coWeight;
    }

    public void setCoWeight(List<Float> coWeight) {
        this.coWeight = coWeight;
    }

    public List<Integer> getCoIteration() {
        return coIteration;
    }

    public void setCoIteration(List<Integer> coIteration) {
        this.coIteration = coIteration;
    }

    public List<Integer> getCoActivation() {
        return coActivation;
    }

    public void setCoActivation(List<Integer> coActivation) {
        this.coActivation = coActivation;
    }

    public List<Float> getCoIncrease() {
        return coIncrease;
    }

    public void setCoIncrease(List<Float> coIncrease) {
        this.coIncrease = coIncrease;
    }

    public List<Float> getCoDecrease() {
        return coDecrease;
    }

    public void setCoDecrease(List<Float> coDecrease) {
        this.coDecrease = coDecrease;
    }

    public SimGaDTO getGa() {
        return ga;
    }

    public void setGa(SimGaDTO ga) {
        this.ga = ga;
    }

    public SimPopulationDTO[] getPopulations() {
        return populations;
    }

    public void setPopulations(SimPopulationDTO[] populations) {
        Arrays.sort(populations, new Comparator<SimPopulationDTO>() {
            @Override
            public int compare(SimPopulationDTO o1, SimPopulationDTO o2) {
                return new Integer(o1.getIndexOrder()).compareTo(new Integer(o2.getIndexOrder()));
            }
        });

        this.populations = populations;
    }

    @Override
    public String toString() {
        return "CustomAngnSearchGliaDTO{" +
            "id=" + id +
            ", detail=" + detail +
            ", execution=" + execution +
            ", gaBasic=" + gaBasic +
            ", layer=" + layer +
            ", neuron=" + neuron +
            ", activationFunction=" + activationFunction +
            ", sameGliaAlgorithm=" + sameGliaAlgorithm +
            ", decreaseGreatherThanIncrease=" + decreaseGreatherThanIncrease +
            ", coWeight=" + coWeight +
            ", coIteration=" + coIteration +
            ", coActivation=" + coActivation +
            ", coIncrease=" + coIncrease +
            ", coDecrease=" + coDecrease +
            ", ga=" + ga +
            ", populations=" + Arrays.toString(populations) +
            '}';
    }
}
