/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import es.udc.tic.rnasa.domain.util.Recognizable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.List;

/**
 * Created by flanciskinho on 9/6/16.
 */
public class CustomAnnFixedGliaDTO implements Recognizable, CustomSimulationVariablesDTO {

    // Common
    private Long id;

    @NotNull
    private CommonSimulationDTO detail;

    @NotNull
    private SimExecDTO execution;

    // GA
    @NotNull
    private SimGaBasicDTO gaBasic;

    @NotNull
    @Size(min = 1, max = 1)
    private SimPopulationDTO populations[];


    // Specific
    @NotNull
    private Integer layer;
    @NotNull
    private Integer iteration;
    @NotNull
    private Integer activation;

    @NotNull
    @Size(min = 1)
    private List<Integer> neuron;
    @NotNull
    @Size(min = 1)
    private List<String> activationFunction;
    @NotNull
    @Size(min = 1)
    private List<String> gliaAlgorithm;
    @NotNull
    @Size(min = 1)
    private List<Float> increase;
    @NotNull
    @Size(min = 1)
    private List<Float> decrease;
    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coWeight; // min max

    public CustomAnnFixedGliaDTO() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SimGaDTO getGa() {
        return null;
    }

    public void setGa(SimGaDTO ga) {

    }

    public SimGaBasicDTO getGaBasic() {
        return gaBasic;
    }

    public void setGaBasic(SimGaBasicDTO gaBasic) {
        this.gaBasic = gaBasic;
    }

    public CommonSimulationDTO getDetail() {
        return detail;
    }

    public void setDetail(CommonSimulationDTO detail) {
        this.detail = detail;
    }

    public SimExecDTO getExecution() {
        return execution;
    }

    public void setExecution(SimExecDTO execution) {
        this.execution = execution;
    }

    public SimPopulationDTO[] getPopulations() {
        return populations;
    }

    public void setPopulations(SimPopulationDTO[] populations) {
        this.populations = populations;
    }

    public Integer getLayer() {
        return layer;
    }

    public void setLayer(Integer layer) {
        this.layer = layer;
    }

    public Integer getIteration() {
        return iteration;
    }

    public void setIteration(Integer iteration) {
        this.iteration = iteration;
    }

    public Integer getActivation() {
        return activation;
    }

    public void setActivation(Integer activation) {
        this.activation = activation;
    }

    public List<Integer> getNeuron() {
        return neuron;
    }

    public void setNeuron(List<Integer> neuron) {
        this.neuron = neuron;
    }

    public List<String> getActivationFunction() {
        return activationFunction;
    }

    public void setActivationFunction(List<String> activationFunction) {
        this.activationFunction = activationFunction;
    }

    public List<String> getGliaAlgorithm() {
        return gliaAlgorithm;
    }

    public void setGliaAlgorithm(List<String> gliaAlgorithm) {
        this.gliaAlgorithm = gliaAlgorithm;
    }

    public List<Float> getIncrease() {
        return increase;
    }

    public void setIncrease(List<Float> increase) {
        this.increase = increase;
    }

    public List<Float> getDecrease() {
        return decrease;
    }

    public void setDecrease(List<Float> decrease) {
        this.decrease = decrease;
    }

    public List<Float> getCoWeight() {
        return coWeight;
    }

    public void setCoWeight(List<Float> coWeight) {
        this.coWeight = coWeight;
    }

    @Override
    public String toString() {
        return "CustomAnnFixedGliaDTO{" +
            "id=" + id +
            ", detail=" + detail +
            ", execution=" + execution +
            ", gaBasic=" + gaBasic +
            ", populations=" + Arrays.toString(populations) +
            ", layer=" + layer +
            ", iteration=" + iteration +
            ", activation=" + activation +
            ", neuron=" + neuron +
            ", activationFunction=" + activationFunction +
            ", gliaAlgorithm=" + gliaAlgorithm +
            ", increase=" + increase +
            ", decrease=" + decrease +
            ", coWeight=" + coWeight +
            '}';
    }
}
