/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import es.udc.tic.rnasa.domain.util.Recognizable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.List;

/**
 * Created by flanciskinho on 9/6/16.
 */
public class CustomNuSvmDTO implements Recognizable, CustomSimulationVariablesDTO {

    // Common
    private Long id;

    @NotNull
    private CommonSimulationDTO detail;

    @NotNull
    private SimExecDTO execution;

    // GA
    @NotNull
    private SimGaBasicDTO gaBasic;

    @NotNull
    @Size(min = 1, max = 1)
    private SimPopulationDTO populations[];

    // Specific
    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coNu;        // min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coTolerance; // min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Integer> coDegree;  // min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coGamma;     // min, max
    @NotNull
    @Size(min = 2, max = 2)
    private List<Float> coCoef;      // min, max



    public CustomNuSvmDTO() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SimGaDTO getGa() {
        return null;
    }

    public void setGa(SimGaDTO ga){
    }


    public SimGaBasicDTO getGaBasic() {
        return gaBasic;
    }

    public void setGaBasic(SimGaBasicDTO gaBasic) {
        this.gaBasic = gaBasic;
    }

    public CommonSimulationDTO getDetail() {
        return detail;
    }

    public void setDetail(CommonSimulationDTO detail) {
        this.detail = detail;
    }

    public SimExecDTO getExecution() {
        return execution;
    }

    public void setExecution(SimExecDTO execution) {
        this.execution = execution;
    }

    public SimPopulationDTO[] getPopulations() {
        return populations;
    }

    public void setPopulations(SimPopulationDTO[] populations) {
        this.populations = populations;
    }

    public List<Float> getCoNu() {
        return coNu;
    }

    public void setCoNu(List<Float> coNu) {
        this.coNu = coNu;
    }

    public List<Float> getCoTolerance() {
        return coTolerance;
    }

    public void setCoTolerance(List<Float> coTolerance) {
        this.coTolerance = coTolerance;
    }

    public List<Integer> getCoDegree() {
        return coDegree;
    }

    public void setCoDegree(List<Integer> coDegree) {
        this.coDegree = coDegree;
    }

    public List<Float> getCoGamma() {
        return coGamma;
    }

    public void setCoGamma(List<Float> coGamma) {
        this.coGamma = coGamma;
    }

    public List<Float> getCoCoef() {
        return coCoef;
    }

    public void setCoCoef(List<Float> coCoef) {
        this.coCoef = coCoef;
    }


    @Override
    public String toString() {
        return "CustomNuSvmDTO{" +
            "id=" + id +
            ", detail=" + detail +
            ", execution=" + execution +
            ", gaBasic=" + gaBasic +
            ", populations=" + Arrays.toString(populations) +
            ", coNu=" + coNu +
            ", coTolerance=" + coTolerance +
            ", coDegree=" + coDegree +
            ", coGamma=" + coGamma +
            ", coCoef=" + coCoef +
            '}';
    }
}
