/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

/**
 * Created by flanciskinho on 26/7/16.
 */
public interface CustomSimulationVariablesDTO {

    void setId(Long id);

    Long getId();

    CommonSimulationDTO getDetail();

    void setDetail(CommonSimulationDTO detail);

    SimExecDTO getExecution();

    void setExecution(SimExecDTO execution);

    SimPopulationDTO[] getPopulations();

    void setPopulations(SimPopulationDTO[] populations);

    SimGaDTO getGa();

    void setGa(SimGaDTO ga);

    SimGaBasicDTO getGaBasic();

    void setGaBasic(SimGaBasicDTO gaBasic);
}
