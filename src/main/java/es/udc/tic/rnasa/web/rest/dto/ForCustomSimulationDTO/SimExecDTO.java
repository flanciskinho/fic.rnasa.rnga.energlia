/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 7/7/16.
 */
public class SimExecDTO {

    @NotNull
    private String arch;

    @NotNull
    @Min(1)
    private Integer proc;

    @NotNull
    @Min(1)
    private Long memory;

    @NotNull
    @Min(1)
    private Long time;

    public SimExecDTO() {}

    public SimExecDTO(String arch, Integer proc, Long memory, Long time) {
        this.arch = arch;
        this.proc = proc;
        this.memory = memory;
        this.time = time;
    }

    public String getArch() {
        return arch;
    }

    public void setArch(String arch) {
        this.arch = arch;
    }

    public Integer getProc() {
        return proc;
    }

    public void setProc(Integer proc) {
        this.proc = proc;
    }

    public Long getMemory() {
        return memory;
    }

    public void setMemory(Long memory) {
        this.memory = memory;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "SimExecDTO{" +
            "arch='" + arch + '\'' +
            ", proc=" + proc +
            ", memory=" + memory +
            ", time=" + time +
            '}';
    }
}
