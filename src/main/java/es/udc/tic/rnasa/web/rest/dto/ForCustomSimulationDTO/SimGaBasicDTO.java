/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 27/7/16.
 */
public class SimGaBasicDTO {

    @NotNull
    @Min(1)
    private Integer generation;

    @NotNull
    @Min(0)
    private Float goal;

    public SimGaBasicDTO() {}

    public SimGaBasicDTO(Integer generation, Float goal) {
        this.generation = generation;
        this.goal = goal;
    }

    public Integer getGeneration() {
        return generation;
    }

    public void setGeneration(Integer generation) {
        this.generation = generation;
    }

    public Float getGoal() {
        return goal;
    }

    public void setGoal(Float goal) {
        this.goal = goal;
    }

    @Override
    public String toString() {
        return "SimGaBasicDTO{" +
            "generation=" + generation +
            ", goal=" + goal +
            '}';
    }
}
