/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 4/7/16.
 */
public class SimGaDTO {

    private String algorithm;

    @NotNull
    @Min(0)
    @Max(10)
    private Short best;

    @NotNull
    @Min(0)
    @Max(10)
    private Short random;

    @NotNull
    @Min(0)
    @Max(10)
    private Short function;

    private SimPopSelectDTO select;

    public SimGaDTO() {}

    public SimGaDTO(String algorithm, Short best, Short random) {
        this.algorithm = algorithm;
        this.best = best;
        this.random = random;
        this.function = 0;
        this.select = null;
    }

    public SimGaDTO(String algorithm, Short best, Short random, Short function, SimPopSelectDTO select) {
        this.algorithm = algorithm;
        this.best = best;
        this.random = random;
        this.function = function;
        this.select = select;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public Short getBest() {
        return best;
    }

    public void setBest(Short best) {
        this.best = best;
    }

    public Short getRandom() {
        return random;
    }

    public void setRandom(Short random) {
        this.random = random;
    }

    public Short getFunction() {
        return function;
    }

    public void setFunction(Short function) {
        this.function = function;
    }

    public SimPopSelectDTO getSelect() {
        return select;
    }

    public void setSelect(SimPopSelectDTO select) {
        this.select = select;
    }

    @Override
    public String toString() {
        return "SimGaDTO{" +
            "algorithm=" + algorithm +
            ", best=" + best +
            ", random=" + random +
            ", function=" + function +
            ", select=" + select +
            '}';
    }
}
