/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 4/7/16.
 */
public class SimPopCrossoverDTO {

    @NotNull
    private String algorithm;

    @NotNull
    @Min(0)
    @Max(1)
    private Float rate;

    private Float mu;

    public SimPopCrossoverDTO() {}

    public SimPopCrossoverDTO(String algorithm, Float rate) {
        this.algorithm = new String(algorithm);
        this.rate = rate;
    }

    public SimPopCrossoverDTO(String algorithm, Float rate, Float mu) {
        this(algorithm, rate);
        this.mu = mu;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Float getMu() {
        return mu;
    }

    public void setMu(Float mu) {
        this.mu = mu;
    }

    @Override
    public String toString() {
        return "SimPopCrossoverDTO{" +
            "algorithm=" + algorithm +
            ", rate=" + rate +
            ", mu=" + mu +
            '}';
    }
}
