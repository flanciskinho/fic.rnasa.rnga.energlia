/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 4/7/16.
 */
public class SimPopSelectDTO {

    @NotNull
    private String algorithm;
    private Short window;
    private Float probability;

    public SimPopSelectDTO() {}

    public SimPopSelectDTO(String algorithm) {
        this.algorithm = new String(algorithm);
    }

    public SimPopSelectDTO(String algorithm, Short window) {
        this(algorithm);
        this.window = window;
    }

    public SimPopSelectDTO(String algorithm, Short window, Float probability) {
        this(algorithm, window);
        this.probability = probability;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public Short getWindow() {
        return window;
    }

    public void setWindow(Short window) {
        this.window = window;
    }

    public Float getProbability() {
        return probability;
    }

    public void setProbability(Float probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "SimPopSelectDTO{" +
            "algorithm=" + algorithm +
            ", window=" + window +
            ", probability=" + probability +
            '}';
    }
}
