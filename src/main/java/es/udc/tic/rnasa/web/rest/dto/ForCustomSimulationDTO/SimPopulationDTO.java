/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO;

import javax.validation.constraints.NotNull;

/**
 * Created by flanciskinho on 4/7/16.
 */
public class SimPopulationDTO {

    @NotNull
    private Integer indexOrder;

    private Long populationId;

    @NotNull
    private SimPopSelectDTO mother;

    @NotNull
    private SimPopSelectDTO father;

    @NotNull
    private SimPopMutationDTO mutation;

    @NotNull
    private SimPopCrossoverDTO crossover;

    @NotNull
    private SimPopReplaceDTO replace;


    public SimPopulationDTO(){}


    public SimPopulationDTO(Integer indexOrder, Long populationId, SimPopSelectDTO mother, SimPopSelectDTO father, SimPopMutationDTO mutation, SimPopCrossoverDTO crossover, SimPopReplaceDTO replace) {
        this.indexOrder = indexOrder;
        this.populationId = populationId;
        this.mother = mother;
        this.father = father;
        this.mutation = mutation;
        this.crossover = crossover;
        this.replace = replace;
    }

    public Integer getIndexOrder() {
        return indexOrder;
    }

    public void setIndexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
    }

    public Long getPopulationId() {
        return populationId;
    }

    public void setPopulationId(Long populationId) {
        this.populationId = populationId;
    }

    public SimPopSelectDTO getMother() {
        return mother;
    }

    public void setMother(SimPopSelectDTO mother) {
        this.mother = mother;
    }

    public SimPopSelectDTO getFather() {
        return father;
    }

    public void setFather(SimPopSelectDTO father) {
        this.father = father;
    }

    public SimPopMutationDTO getMutation() {
        return mutation;
    }

    public void setMutation(SimPopMutationDTO mutation) {
        this.mutation = mutation;
    }

    public SimPopCrossoverDTO getCrossover() {
        return crossover;
    }

    public void setCrossover(SimPopCrossoverDTO crossover) {
        this.crossover = crossover;
    }

    public SimPopReplaceDTO getReplace() {
        return replace;
    }

    public void setReplace(SimPopReplaceDTO replace) {
        this.replace = replace;
    }

    @Override
    public String toString() {
        return "SimPopulationDTO{" +
            "populationId=" + populationId +
            ", mother=" + mother +
            ", father=" + father +
            ", mutation=" + mutation +
            ", crossover=" + crossover +
            ", replace=" + replace +
            '}';
    }
}
