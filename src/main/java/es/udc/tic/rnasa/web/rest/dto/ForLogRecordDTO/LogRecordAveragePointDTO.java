/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForLogRecordDTO;

/**
 * Created by flanciskinho on 16/8/16.
 */
public class LogRecordAveragePointDTO {

    private Long indexRecord;

    private Long timestamp;

    private Double errorTrain, errorValidation, errorTest;

    public LogRecordAveragePointDTO() {}

    public LogRecordAveragePointDTO(Long indexRecord, Long timestamp, Double errorTrain, Double errorValidation, Double errorTest) {
        this.indexRecord = indexRecord;
        this.timestamp = timestamp;
        this.errorTrain = errorTrain;
        this.errorValidation = errorValidation;
        this.errorTest = errorTest;
    }

    public Long getIndexRecord() {
        return indexRecord;
    }

    public void setIndexRecord(Long indexRecord) {
        this.indexRecord = indexRecord;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getErrorTrain() {
        return errorTrain;
    }

    public void setErrorTrain(Double errorTrain) {
        this.errorTrain = errorTrain;
    }

    public Double getErrorValidation() {
        return errorValidation;
    }

    public void setErrorValidation(Double errorValidation) {
        this.errorValidation = errorValidation;
    }

    public Double getErrorTest() {
        return errorTest;
    }

    public void setErrorTest(Double errorTest) {
        this.errorTest = errorTest;
    }


    @Override
    public String toString() {
        return "LogRecordAveragePointDTO{" +
            "indexRecord=" + indexRecord +
            ", timestamp=" + timestamp +
            ", errorTrain=" + errorTrain +
            ", errorValidation=" + errorValidation +
            ", errorTest=" + errorTest +
            '}';
    }
}
