/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto.ForLogRecordDTO;

/**
 * Created by flanciskinho on 16/8/16.
 */
public class LogRecordPointDTO {

    private Long indexRecord;

    private Long timestamp;
    private Long desTimestampPlus, desTimestampSub;

    private Double errorTrain, errorValidation, errorTest;
    private Double desTrainPlus, desValidationPlus, desTestPlus;
    private Double desTrainSub,  desValidationSub,  desTestSub;

    public LogRecordPointDTO() {}

    public LogRecordPointDTO(Long indexRecord, Long timestamp, Double errorTrain, Double errorValidation, Double errorTest, Long desTimestamp, Double desTrain, Double desValidation, Double desTest) {
        this.indexRecord = indexRecord;

        this.timestamp = timestamp;

        this.errorTrain = errorTrain;
        this.errorValidation = errorValidation;
        this.errorTest = errorTest;

        this.desTimestampPlus = timestamp + desTimestamp;
        this.desTimestampSub  = timestamp - desTimestamp;

        if (errorTrain != null || desTrain != null) {
            this.desTrainPlus = errorTrain + desTrain;
            this.desTrainSub  = errorTrain - desTrain;
        } else {
            this.desTrainPlus = this.desTrainSub = null;
        }

        if (errorValidation != null || desValidation != null) {
            this.desValidationPlus = errorValidation + desValidation;
            this.desValidationSub  = errorValidation - desValidation;
        } else {
            this.desValidationPlus = this.desValidationSub = null;
        }

        if (errorTest != null || desTest != null) {
            this.desTestPlus = errorTest + desTest;
            this.desTestSub  = errorTest - desTest;
        } else {
            this.desTestPlus = this.desTestSub = null;
        }
    }

    public Long getIndexRecord() {
        return indexRecord;
    }

    public void setIndexRecord(Long indexRecord) {
        this.indexRecord = indexRecord;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getErrorTrain() {
        return errorTrain;
    }

    public void setErrorTrain(Double errorTrain) {
        this.errorTrain = errorTrain;
    }

    public Double getErrorValidation() {
        return errorValidation;
    }

    public void setErrorValidation(Double errorValidation) {
        this.errorValidation = errorValidation;
    }

    public Double getErrorTest() {
        return errorTest;
    }

    public void setErrorTest(Double errorTest) {
        this.errorTest = errorTest;
    }

    public Double getDesTrainPlus() {
        return desTrainPlus;
    }

    public void setDesTrainPlus(Double desTrainPlus) {
        this.desTrainPlus = desTrainPlus;
    }

    public Double getDesValidationPlus() {
        return desValidationPlus;
    }

    public void setDesValidationPlus(Double desValidationPlus) {
        this.desValidationPlus = desValidationPlus;
    }

    public Double getDesTestPlus() {
        return desTestPlus;
    }

    public void setDesTestPlus(Double desTestPlus) {
        this.desTestPlus = desTestPlus;
    }

    public Double getDesTrainSub() {
        return desTrainSub;
    }

    public void setDesTrainSub(Double desTrainSub) {
        this.desTrainSub = desTrainSub;
    }

    public Double getDesValidationSub() {
        return desValidationSub;
    }

    public void setDesValidationSub(Double desValidationSub) {
        this.desValidationSub = desValidationSub;
    }

    public Double getDesTestSub() {
        return desTestSub;
    }

    public void setDesTestSub(Double desTestSub) {
        this.desTestSub = desTestSub;
    }

    public Long getDesTimestampPlus() {
        return desTimestampPlus;
    }

    public void setDesTimestampPlus(Long desTimestampPlus) {
        this.desTimestampPlus = desTimestampPlus;
    }

    public Long getDesTimestampSub() {
        return desTimestampSub;
    }

    public void setDesTimestampSub(Long desTimestampSub) {
        this.desTimestampSub = desTimestampSub;
    }

    @Override
    public String toString() {
        return "LogRecordPointDTO{" +
            "indexRecord=" + indexRecord +
            ", timestamp=" + timestamp +
            ", desTimestampPlus=" + desTimestampPlus +
            ", desTimestampSub=" + desTimestampSub +
            ", errorTrain=" + errorTrain +
            ", errorValidation=" + errorValidation +
            ", errorTest=" + errorTest +
            ", desTrainPlus=" + desTrainPlus +
            ", desValidationPlus=" + desValidationPlus +
            ", desTestPlus=" + desTestPlus +
            ", desTrainSub=" + desTrainSub +
            ", desValidationSub=" + desValidationSub +
            ", desTestSub=" + desTestSub +
            '}';
    }
}
