/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the LogRecord entity.
 */
public class LogRecordDTO implements Serializable {

    private Long id;

    @NotNull
    private Long indexRecord;


    @NotNull
    private Long timestamp;


    @NotNull
    private Double errorTrain;


    private Double errorValidation;


    private Double errorTest;


    private Long belongId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getIndexRecord() {
        return indexRecord;
    }

    public void setIndexRecord(Long indexRecord) {
        this.indexRecord = indexRecord;
    }
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
    public Double getErrorTrain() {
        return errorTrain;
    }

    public void setErrorTrain(Double errorTrain) {
        this.errorTrain = errorTrain;
    }
    public Double getErrorValidation() {
        return errorValidation;
    }

    public void setErrorValidation(Double errorValidation) {
        this.errorValidation = errorValidation;
    }
    public Double getErrorTest() {
        return errorTest;
    }

    public void setErrorTest(Double errorTest) {
        this.errorTest = errorTest;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long specificSimulationId) {
        this.belongId = specificSimulationId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LogRecordDTO logRecordDTO = (LogRecordDTO) o;

        if ( ! Objects.equals(id, logRecordDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "LogRecordDTO{" +
            "id=" + id +
            ", indexRecord='" + indexRecord + "'" +
            ", timestamp='" + timestamp + "'" +
            ", errorTrain='" + errorTrain + "'" +
            ", errorValidation='" + errorValidation + "'" +
            ", errorTest='" + errorTest + "'" +
            '}';
    }
}
