/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the PopulationCombination entity.
 */
public class PopulationCombinationDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer indexCombination;


    @NotNull
    @Lob
    private byte[] file;

    private String fileContentType;

    private Long belongId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getIndexCombination() {
        return indexCombination;
    }

    public void setIndexCombination(Integer indexCombination) {
        this.indexCombination = indexCombination;
    }
    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long populationId) {
        this.belongId = populationId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PopulationCombinationDTO populationCombinationDTO = (PopulationCombinationDTO) o;

        if ( ! Objects.equals(id, populationCombinationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PopulationCombinationDTO{" +
            "id=" + id +
            ", indexCombination='" + indexCombination + "'" +
            ", file='" + file + "'" +
            '}';
    }
}
