/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the PopulationCombinationOrder entity.
 */
public class PopulationCombinationOrderDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer indexOrder;


    private Long belongId;
    private Long useId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getIndexOrder() {
        return indexOrder;
    }

    public void setIndexOrder(Integer indexOrder) {
        this.indexOrder = indexOrder;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long specificSimulationId) {
        this.belongId = specificSimulationId;
    }
    public Long getUseId() {
        return useId;
    }

    public void setUseId(Long populationCombinationId) {
        this.useId = populationCombinationId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PopulationCombinationOrderDTO populationCombinationOrderDTO = (PopulationCombinationOrderDTO) o;

        if ( ! Objects.equals(id, populationCombinationOrderDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PopulationCombinationOrderDTO{" +
            "id=" + id +
            ", indexOrder='" + indexOrder + "'" +
            '}';
    }
}
