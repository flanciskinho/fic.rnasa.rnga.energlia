/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import es.udc.tic.rnasa.domain.util.Recognizable;
import es.udc.tic.rnasa.service.util.HasOwnerToPrivacy;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Population entity.
 */
public class PopulationDTO implements Serializable, HasOwnerToPrivacy, Recognizable {

    private Long id;

    @NotNull
    private String name;


    private String description;


    @NotNull
    private ZonedDateTime timestamp;


    private Integer numIndividual;


    private Integer numCombination;


    private Integer sizeGenotype;


    private Long ownerId;
    private String ownerLogin;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }
    public Integer getNumIndividual() {
        return numIndividual;
    }

    public void setNumIndividual(Integer numIndividual) {
        this.numIndividual = numIndividual;
    }
    public Integer getNumCombination() {
        return numCombination;
    }

    public void setNumCombination(Integer numCombination) {
        this.numCombination = numCombination;
    }
    public Integer getSizeGenotype() {
        return sizeGenotype;
    }

    public void setSizeGenotype(Integer sizeGenotype) {
        this.sizeGenotype = sizeGenotype;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long userId) {
        this.ownerId = userId;
    }

    public String getOwnerLogin() {
        return ownerLogin;
    }

    public void setOwnerLogin(String ownerLogin) {
        this.ownerLogin = ownerLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PopulationDTO populationDTO = (PopulationDTO) o;

        if ( ! Objects.equals(id, populationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PopulationDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", timestamp='" + timestamp + "'" +
            ", numIndividual='" + numIndividual + "'" +
            ", numCombination='" + numCombination + "'" +
            ", sizeGenotype='" + sizeGenotype + "'" +
            ", ownerId='" + ownerId + "'" +
            '}';
    }
}
