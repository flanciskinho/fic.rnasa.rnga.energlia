/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * A DTO for the ProblemType entity.
 */
public class ProblemTypeDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 100)
    private String name;

    private Integer outputFeatureConstraint;

    @NotNull
    private Integer numPopulation;

    private Set<ErrorTypeDTO> minimizes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOutputFeatureConstraint() {
        return outputFeatureConstraint;
    }

    public void setOutputFeatureConstraint(Integer outputFeatureConstraint) {
        this.outputFeatureConstraint = outputFeatureConstraint;
    }

    public Integer getNumPopulation() {
        return numPopulation;
    }

    public void setNumPopulation(Integer numPopulation) {
        this.numPopulation = numPopulation;
    }

    public Set<ErrorTypeDTO> getMinimizes() {
        return minimizes;
    }

    public void setMinimizes(Set<ErrorTypeDTO> errorTypes) {
        this.minimizes = errorTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProblemTypeDTO problemTypeDTO = (ProblemTypeDTO) o;

        if ( ! Objects.equals(id, problemTypeDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProblemTypeDTO{" +
            "id=" + id + "'" +
            ", name='" + name +  "'" +
            ", outputFeatureConstraint=" + outputFeatureConstraint + "'" +
            ", numPopulation=" + numPopulation + "'" +
            '}';
    }
}
