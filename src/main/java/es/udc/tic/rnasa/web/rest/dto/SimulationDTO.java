/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import es.udc.tic.rnasa.domain.util.Recognizable;
import es.udc.tic.rnasa.service.util.HasOwnerToPrivacy;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;


/**
 * A DTO for the Simulation entity.
 */
public class SimulationDTO implements Serializable, HasOwnerToPrivacy, Recognizable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime timestamp;

    private String description;


    private Long belongId;
    private String belongLogin;
    private Long useId;
    private String useName;
    private Long exeId;
    private Long problemId;
    private String problemName;
    private Long minimizeId;
    private String minimizeName;
    private Long launchId;
    private Integer taskTotal = 0;
    private Integer taskDone = 0;
    private Integer taskFail = 0;
    private Integer taskQueue = 0;
    private Integer taskServer = 0;

    public SimulationDTO() {
        taskTotal = taskDone = taskFail = taskServer = taskQueue = 0;
    }

    public SimulationDTO(Long id, String name, ZonedDateTime timestamp, String description, Long belongId, String belongLogin, Long useId, Long exeId, Long problemId, Long minimizeId, Long launchId) {
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.description = description;
        this.belongId = belongId;
        this.belongLogin = belongLogin;
        this.useId = useId;
        this.exeId = exeId;
        this.problemId = problemId;
        this.minimizeId = minimizeId;
        this.launchId = launchId;
        taskTotal = taskDone = taskFail = taskServer = taskQueue = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(Integer taskTotal) {
        this.taskTotal = taskTotal;
    }

    public Integer getTaskDone() {
        return taskDone;
    }

    public void setTaskDone(Integer taskDone) {
        this.taskDone = taskDone;
    }

    public Integer getTaskFail() {
        return taskFail;
    }

    public void setTaskFail(Integer taskFail) {
        this.taskFail = taskFail;
    }

    public Integer getTaskQueue() {
        return taskQueue;
    }

    public void setTaskQueue(Integer taskQueue) {
        this.taskQueue = taskQueue;
    }

    public Integer getTaskServer() {
        return taskServer;
    }

    public void setTaskServer(Integer taskServer) {
        this.taskServer = taskServer;
    }

    public Long getOwnerId() { return belongId; }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long userId) {
        this.belongId = userId;
    }

    public String getBelongLogin() {
        return belongLogin;
    }

    public void setBelongLogin(String belongLogin) {
        this.belongLogin = belongLogin;
    }

    public Long getUseId() {
        return useId;
    }

    public void setUseId(Long datasetId) {
        this.useId = datasetId;
    }

    public String getUseName() {
        return useName;
    }

    public void setUseName(String useName) {
        this.useName = useName;
    }

    public Long getExeId() {
        return exeId;
    }

    public void setExeId(Long applicationId) {
        this.exeId = applicationId;
    }
    public Long getProblemId() {
        return problemId;
    }

    public void setProblemId(Long problemTypeId) {
        this.problemId = problemTypeId;
    }
    public Long getMinimizeId() {
        return minimizeId;
    }

    public void setMinimizeId(Long errorTypeId) {
        this.minimizeId = errorTypeId;
    }

    public Long getLaunchId() {
        return launchId;
    }

    public void setLaunchId(Long launchId) {
        this.launchId = launchId;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getMinimizeName() {
        return minimizeName;
    }

    public void setMinimizeName(String minimizeName) {
        this.minimizeName = minimizeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimulationDTO simulationDTO = (SimulationDTO) o;

        if ( ! Objects.equals(id, simulationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SimulationDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", timestamp=" + timestamp +
            ", belongLogin='" + belongLogin + '\'' +
            ", taskTotal=" + taskTotal +
            ", taskDone=" + taskDone +
            ", taskFail=" + taskFail +
            ", taskQueue=" + taskQueue +
            ", taskServer=" + taskServer +
            '}';
    }
}
