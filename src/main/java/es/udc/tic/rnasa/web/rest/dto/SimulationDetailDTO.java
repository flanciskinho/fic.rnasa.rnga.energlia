/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import es.udc.tic.rnasa.domain.*;

import java.util.List;

/**
 * Created by flanciskinho on 19/8/16.
 */
public class SimulationDetailDTO {

    private Long id;
    private String name;
    private String description;

    private Long problemId;
    private String problemName;

    private Long minimizeId;
    private String minimizeName;

    private Long datasetId;
    private String datasetName;

    private Long execId;
    private String execDnsname;

    private Long populationId[];
    private String populationName[];


    public void setSimulation(Simulation simulation) {
        this.id = simulation.getId();
        this.name = simulation.getName();
        this.description = simulation.getDescription();
    }

    public void setDataset(Dataset dataset) {
        this.datasetId = dataset.getId();
        this.datasetName = dataset.getName();
    }

    public void setProblem(ProblemType problem) {
        this.problemId = problem.getId();
        this.problemName = problem.getName();
    }

    public void setError(ErrorType error) {
        this.minimizeId = error.getId();
        this.minimizeName = error.getName();
    }

    public void setExec(SshServer sshServer) {
        this.execId = sshServer.getId();
        this.execDnsname = sshServer.getDnsname();
    }

    public void setPopulation(List<Population> populations) {
        populationId = new Long[populations.size()];
        populationName = new String[populations.size()];

        Population population;
        for (int cnt = 0; cnt < populations.size(); cnt++) {
            population = populations.get(cnt);
            populationId[cnt]   = population.getId();
            populationName[cnt] = population.getName();
        }

    }

}
