/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the SizeParam entity.
 */
public class SizeParamDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer size;


    private Long belongId;
    private Long useId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long populationId) {
        this.belongId = populationId;
    }
    public Long getUseId() {
        return useId;
    }

    public void setUseId(Long sizeTypeId) {
        this.useId = sizeTypeId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SizeParamDTO sizeParamDTO = (SizeParamDTO) o;

        if ( ! Objects.equals(id, sizeParamDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SizeParamDTO{" +
            "id=" + id +
            ", size='" + size + "'" +
            '}';
    }
}
