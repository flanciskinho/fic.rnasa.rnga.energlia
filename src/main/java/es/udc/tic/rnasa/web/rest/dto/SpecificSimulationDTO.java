/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the SpecificSimulation entity.
 */
public class SpecificSimulationDTO implements Serializable {

    private Long id;

    private String logVersion;


    @Lob
    private byte[] filelog;

    private String filelogContentType;

    private String nodeVersion;


    private Long jobid;


    private Long statusId;
    private Long belongId;
    private Long launchById;
    private Long useDataId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getLogVersion() {
        return logVersion;
    }

    public void setLogVersion(String logVersion) {
        this.logVersion = logVersion;
    }
    public byte[] getFilelog() {
        return filelog;
    }

    public void setFilelog(byte[] filelog) {
        this.filelog = filelog;
    }

    public String getFilelogContentType() {
        return filelogContentType;
    }

    public void setFilelogContentType(String filelogContentType) {
        this.filelogContentType = filelogContentType;
    }
    public String getNodeVersion() {
        return nodeVersion;
    }

    public void setNodeVersion(String nodeVersion) {
        this.nodeVersion = nodeVersion;
    }
    public Long getJobid() {
        return jobid;
    }

    public void setJobid(Long jobid) {
        this.jobid = jobid;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long simulationStatusTypeId) {
        this.statusId = simulationStatusTypeId;
    }
    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long simulationId) {
        this.belongId = simulationId;
    }
    public Long getLaunchById() {
        return launchById;
    }

    public void setLaunchById(Long sshAccountId) {
        this.launchById = sshAccountId;
    }
    public Long getUseDataId() {
        return useDataId;
    }

    public void setUseDataId(Long datasetCombinationId) {
        this.useDataId = datasetCombinationId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpecificSimulationDTO specificSimulationDTO = (SpecificSimulationDTO) o;

        if ( ! Objects.equals(id, specificSimulationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SpecificSimulationDTO{" +
            "id=" + id +
            ", logVersion='" + logVersion + "'" +
            ", filelog='" + filelog + "'" +
            ", nodeVersion='" + nodeVersion + "'" +
            ", jobid='" + jobid + "'" +
            '}';
    }
}
