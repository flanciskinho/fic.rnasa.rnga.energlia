/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the SshAccount entity.
 */
public class SshAccountDTO implements Serializable {

    private Long id;

    @NotNull
    private String username;


    @NotNull
    private String key;

    private Integer numJob;

    private Long version;

    @NotNull
    private Boolean activated;


    private Long belongId;
    private String belongDnsname;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getNumJob() {
        return numJob;
    }

    public void setNumJob(Integer numJob) {
        this.numJob = numJob;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Long getBelongId() {
        return belongId;
    }

    public void setBelongId(Long sshServerId) {
        this.belongId = sshServerId;
    }

    public String getBelongDnsname() {
        return belongDnsname;
    }

    public void setBelongDnsname(String belongDnsname) {
        this.belongDnsname = belongDnsname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SshAccountDTO sshAccountDTO = (SshAccountDTO) o;

        if ( ! Objects.equals(id, sshAccountDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SshAccountDTO{" +
            "id=" + id +
            ", username='" + username + "'" +
            //", key='" + key + "'" +
            ", numJob='" + numJob + "'" +
            ", activated='" + activated + "'" +
            '}';
    }
}
