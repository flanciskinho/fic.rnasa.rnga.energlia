/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the SshServer entity.
 */
public class SshServerDTO implements Serializable {

    private Long id;

    @NotNull
    private String dnsname;

    @NotNull
    private Integer port;

    @NotNull
    @Size(min = 47, max = 47)
    private String fingerprint;


    @NotNull
    private Integer maxJob;


    @NotNull
    private Integer maxProc;

    @NotNull
    private Long maxTime;

    @NotNull
    private Long maxSpace;


    @NotNull
    private Long maxMemory;


    @NotNull
    private Boolean activated;

    @NotNull
    private Integer numConnection;

    @NotNull
    private Integer maxConnection;

    private Long useId;
    private String useName;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getDnsname() {
        return dnsname;
    }

    public void setDnsname(String dnsname) {
        this.dnsname = dnsname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }
    public Integer getMaxJob() {
        return maxJob;
    }

    public void setMaxJob(Integer maxJob) {
        this.maxJob = maxJob;
    }

    public Integer getMaxProc() {
        return maxProc;
    }

    public void setMaxProc(Integer maxProc) {
        this.maxProc = maxProc;
    }

    public Long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(Long maxTime) {
        this.maxTime = maxTime;
    }

    public Long getMaxSpace() {
        return maxSpace;
    }

    public void setMaxSpace(Long maxSpace) {
        this.maxSpace = maxSpace;
    }
    public Long getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(Long maxMemory) {
        this.maxMemory = maxMemory;
    }
    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Long getUseId() {
        return useId;
    }

    public void setUseId(Long queueTypeId) {
        this.useId = queueTypeId;
    }

    public String getUseName() {
        return useName;
    }

    public void setUseName(String useName) {
        this.useName = useName;
    }

    public Integer getMaxConnection() {
        return maxConnection;
    }

    public void setMaxConnection(Integer maxConnection) {
        this.maxConnection = maxConnection;
    }

    public Integer getNumConnection() {
        return numConnection;
    }

    public void setNumConnection(Integer numConnection) {
        this.numConnection = numConnection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SshServerDTO sshServerDTO = (SshServerDTO) o;

        if ( ! Objects.equals(id, sshServerDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SshServerDTO{" +
            "id=" + id +
            ", dnsname='" + dnsname + '\'' +
            ", port=" + port +
            ", fingerprint='" + fingerprint + '\'' +
            ", maxJob=" + maxJob +
            ", maxProc=" + maxProc +
            ", maxTime=" + maxTime +
            ", maxSpace=" + maxSpace +
            ", maxMemory=" + maxMemory +
            ", activated=" + activated +
            ", maxConnection=" + maxConnection +
            ", useId=" + useId +
            ", useName='" + useName + '\'' +
            '}';
    }
}
