/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.AnalyzeTypeDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity AnalyzeType and its DTO AnalyzeTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AnalyzeTypeMapper {

    AnalyzeTypeDTO analyzeTypeToAnalyzeTypeDTO(AnalyzeType analyzeType);

    List<AnalyzeTypeDTO> analyzeTypesToAnalyzeTypeDTOs(List<AnalyzeType> analyzeTypes);

    AnalyzeType analyzeTypeDTOToAnalyzeType(AnalyzeTypeDTO analyzeTypeDTO);

    List<AnalyzeType> analyzeTypeDTOsToAnalyzeTypes(List<AnalyzeTypeDTO> analyzeTypeDTOs);
}
