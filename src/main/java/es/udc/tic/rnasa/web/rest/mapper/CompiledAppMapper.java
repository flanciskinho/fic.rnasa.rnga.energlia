/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.CompiledAppDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity CompiledApp and its DTO CompiledAppDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CompiledAppMapper {

    @Mapping(source = "belong.id", target = "belongId")
    @Mapping(source = "belong.name", target = "belongName")
    @Mapping(source = "use.id", target = "useId")
    @Mapping(source = "use.dnsname", target = "useDnsname")
    CompiledAppDTO compiledAppToCompiledAppDTO(CompiledApp compiledApp);

    List<CompiledAppDTO> compiledAppsToCompiledAppDTOs(List<CompiledApp> compiledApps);

    @Mapping(source = "belongId", target = "belong")
    @Mapping(source = "useId", target = "use")
    CompiledApp compiledAppDTOToCompiledApp(CompiledAppDTO compiledAppDTO);

    List<CompiledApp> compiledAppDTOsToCompiledApps(List<CompiledAppDTO> compiledAppDTOs);

    default Application applicationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Application application = new Application();
        application.setId(id);
        return application;
    }

    default SshServer sshServerFromId(Long id) {
        if (id == null) {
            return null;
        }
        SshServer sshServer = new SshServer();
        sshServer.setId(id);
        return sshServer;
    }
}
