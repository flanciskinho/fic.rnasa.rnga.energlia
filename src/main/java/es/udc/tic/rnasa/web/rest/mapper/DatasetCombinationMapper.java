/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.DatasetCombinationDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity DatasetCombination and its DTO DatasetCombinationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DatasetCombinationMapper {

    @Mapping(source = "belong.id", target = "belongId")
    DatasetCombinationDTO datasetCombinationToDatasetCombinationDTO(DatasetCombination datasetCombination);

    List<DatasetCombinationDTO> datasetCombinationsToDatasetCombinationDTOs(List<DatasetCombination> datasetCombinations);

    @Mapping(source = "belongId", target = "belong")
    DatasetCombination datasetCombinationDTOToDatasetCombination(DatasetCombinationDTO datasetCombinationDTO);

    List<DatasetCombination> datasetCombinationDTOsToDatasetCombinations(List<DatasetCombinationDTO> datasetCombinationDTOs);

    default Dataset datasetFromId(Long id) {
        if (id == null) {
            return null;
        }
        Dataset dataset = new Dataset();
        dataset.setId(id);
        return dataset;
    }
}
