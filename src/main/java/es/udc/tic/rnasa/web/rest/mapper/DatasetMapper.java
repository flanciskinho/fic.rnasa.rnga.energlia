/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.AnalyzeType;
import es.udc.tic.rnasa.domain.Dataset;
import es.udc.tic.rnasa.web.rest.dto.DatasetDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * Mapper for the entity Dataset and its DTO DatasetDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface DatasetMapper {

    @Mapping(source = "owner.id", target = "ownerId")
    @Mapping(source = "owner.login", target = "ownerLogin")
    @Mapping(source = "analyze.id", target = "analyzeId")
    @Mapping(source = "analyze.name", target = "analyzeName")
    DatasetDTO datasetToDatasetDTO(Dataset dataset);

    List<DatasetDTO> datasetsToDatasetDTOs(List<Dataset> datasets);

    @Mapping(source = "ownerId", target = "owner")
    @Mapping(source = "analyzeId", target = "analyze")
    @Mapping(target = "shares", ignore = true)
    Dataset datasetDTOToDataset(DatasetDTO datasetDTO);

    List<Dataset> datasetDTOsToDatasets(List<DatasetDTO> datasetDTOs);

    default AnalyzeType analyzeTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        AnalyzeType analyze = new AnalyzeType();
        analyze.setId(id);
        return analyze;
    }
}
