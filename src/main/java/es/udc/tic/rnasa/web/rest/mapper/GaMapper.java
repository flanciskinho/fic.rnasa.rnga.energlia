/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.GaDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Ga and its DTO GaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GaMapper {

    @Mapping(source = "belong.id", target = "belongId")
    @Mapping(source = "use.id", target = "useId")
    GaDTO gaToGaDTO(Ga ga);

    List<GaDTO> gasToGaDTOs(List<Ga> gas);

    @Mapping(source = "belongId", target = "belong")
    @Mapping(source = "useId", target = "use")
    Ga gaDTOToGa(GaDTO gaDTO);

    List<Ga> gaDTOsToGas(List<GaDTO> gaDTOs);

    default Simulation simulationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Simulation simulation = new Simulation();
        simulation.setId(id);
        return simulation;
    }

    default GaType gaTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        GaType gaType = new GaType();
        gaType.setId(id);
        return gaType;
    }
}
