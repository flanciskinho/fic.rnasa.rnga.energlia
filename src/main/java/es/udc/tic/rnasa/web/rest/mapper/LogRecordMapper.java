/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.LogRecordDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity LogRecord and its DTO LogRecordDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LogRecordMapper {

    @Mapping(source = "belong.id", target = "belongId")
    LogRecordDTO logRecordToLogRecordDTO(LogRecord logRecord);

    List<LogRecordDTO> logRecordsToLogRecordDTOs(List<LogRecord> logRecords);

    @Mapping(source = "belongId", target = "belong")
    LogRecord logRecordDTOToLogRecord(LogRecordDTO logRecordDTO);

    List<LogRecord> logRecordDTOsToLogRecords(List<LogRecordDTO> logRecordDTOs);

    default SpecificSimulation specificSimulationFromId(Long id) {
        if (id == null) {
            return null;
        }
        SpecificSimulation specificSimulation = new SpecificSimulation();
        specificSimulation.setId(id);
        return specificSimulation;
    }
}
