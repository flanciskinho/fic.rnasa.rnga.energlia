/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.PopulationCombinationOrderDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity PopulationCombinationOrder and its DTO PopulationCombinationOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PopulationCombinationOrderMapper {

    @Mapping(source = "belong.id", target = "belongId")
    @Mapping(source = "use.id", target = "useId")
    PopulationCombinationOrderDTO populationCombinationOrderToPopulationCombinationOrderDTO(PopulationCombinationOrder populationCombinationOrder);

    List<PopulationCombinationOrderDTO> populationCombinationOrdersToPopulationCombinationOrderDTOs(List<PopulationCombinationOrder> populationCombinationOrders);

    @Mapping(source = "belongId", target = "belong")
    @Mapping(source = "useId", target = "use")
    PopulationCombinationOrder populationCombinationOrderDTOToPopulationCombinationOrder(PopulationCombinationOrderDTO populationCombinationOrderDTO);

    List<PopulationCombinationOrder> populationCombinationOrderDTOsToPopulationCombinationOrders(List<PopulationCombinationOrderDTO> populationCombinationOrderDTOs);

    default SpecificSimulation specificSimulationFromId(Long id) {
        if (id == null) {
            return null;
        }
        SpecificSimulation specificSimulation = new SpecificSimulation();
        specificSimulation.setId(id);
        return specificSimulation;
    }

    default PopulationCombination populationCombinationFromId(Long id) {
        if (id == null) {
            return null;
        }
        PopulationCombination populationCombination = new PopulationCombination();
        populationCombination.setId(id);
        return populationCombination;
    }
}
