/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.PopulationOrderDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity PopulationOrder and its DTO PopulationOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PopulationOrderMapper {

    @Mapping(source = "belong.id", target = "belongId")
    @Mapping(source = "use.id", target = "useId")
    PopulationOrderDTO populationOrderToPopulationOrderDTO(PopulationOrder populationOrder);

    List<PopulationOrderDTO> populationOrdersToPopulationOrderDTOs(List<PopulationOrder> populationOrders);

    @Mapping(source = "belongId", target = "belong")
    @Mapping(source = "useId", target = "use")
    PopulationOrder populationOrderDTOToPopulationOrder(PopulationOrderDTO populationOrderDTO);

    List<PopulationOrder> populationOrderDTOsToPopulationOrders(List<PopulationOrderDTO> populationOrderDTOs);

    default Ga gaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Ga ga = new Ga();
        ga.setId(id);
        return ga;
    }

    default Population populationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Population population = new Population();
        population.setId(id);
        return population;
    }
}
