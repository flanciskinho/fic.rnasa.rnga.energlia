/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.ProblemTypeDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ProblemType and its DTO ProblemTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {ErrorTypeMapper.class, })
public interface ProblemTypeMapper {

    ProblemTypeDTO problemTypeToProblemTypeDTO(ProblemType problemType);

    List<ProblemTypeDTO> problemTypesToProblemTypeDTOs(List<ProblemType> problemTypes);

    ProblemType problemTypeDTOToProblemType(ProblemTypeDTO problemTypeDTO);

    List<ProblemType> problemTypeDTOsToProblemTypes(List<ProblemTypeDTO> problemTypeDTOs);

    default ErrorType errorTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        ErrorType errorType = new ErrorType();
        errorType.setId(id);
        return errorType;
    }
}
