/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.SimulationDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Simulation and its DTO SimulationDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface SimulationMapper {

    @Mapping(source = "belong.id", target = "belongId")
    @Mapping(source = "belong.login", target = "belongLogin")
    @Mapping(source = "use.id", target = "useId")
    @Mapping(source = "use.name", target = "useName")
    @Mapping(source = "exe.id", target = "exeId")
    @Mapping(source = "problem.id", target = "problemId")
    @Mapping(source = "problem.name", target = "problemName")
    @Mapping(source = "minimize.id", target = "minimizeId")
    @Mapping(source = "minimize.name", target = "minimizeName")
    @Mapping(source = "launch.id", target = "launchId")
    SimulationDTO simulationToSimulationDTO(Simulation simulation);

    List<SimulationDTO> simulationsToSimulationDTOs(List<Simulation> simulations);

    @Mapping(source = "belongId", target = "belong")
    @Mapping(source = "useId", target = "use")
    @Mapping(source = "exeId", target = "exe")
    @Mapping(source = "problemId", target = "problem")
    @Mapping(source = "minimizeId", target = "minimize")
    @Mapping(source = "launchId", target = "launch")
    Simulation simulationDTOToSimulation(SimulationDTO simulationDTO);

    List<Simulation> simulationDTOsToSimulations(List<SimulationDTO> simulationDTOs);

    default Dataset datasetFromId(Long id) {
        if (id == null) {
            return null;
        }
        Dataset dataset = new Dataset();
        dataset.setId(id);
        return dataset;
    }

    default Application applicationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Application application = new Application();
        application.setId(id);
        return application;
    }

    default ProblemType problemTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        ProblemType problemType = new ProblemType();
        problemType.setId(id);
        return problemType;
    }

    default ErrorType errorTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        ErrorType errorType = new ErrorType();
        errorType.setId(id);
        return errorType;
    }

    default SshServer sshServerFromId(Long id) {
        if (id == null) {
            return null;
        }
        SshServer sshServer = new SshServer();
        sshServer.setId(id);
        return sshServer;
    }
}
