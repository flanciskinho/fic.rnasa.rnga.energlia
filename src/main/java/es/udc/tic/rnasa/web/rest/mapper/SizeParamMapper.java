/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.SizeParamDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity SizeParam and its DTO SizeParamDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SizeParamMapper {

    @Mapping(source = "belong.id", target = "belongId")
    @Mapping(source = "use.id", target = "useId")
    SizeParamDTO sizeParamToSizeParamDTO(SizeParam sizeParam);

    List<SizeParamDTO> sizeParamsToSizeParamDTOs(List<SizeParam> sizeParams);

    @Mapping(source = "belongId", target = "belong")
    @Mapping(source = "useId", target = "use")
    SizeParam sizeParamDTOToSizeParam(SizeParamDTO sizeParamDTO);

    List<SizeParam> sizeParamDTOsToSizeParams(List<SizeParamDTO> sizeParamDTOs);

    default Population populationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Population population = new Population();
        population.setId(id);
        return population;
    }

    default SizeType sizeTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        SizeType sizeType = new SizeType();
        sizeType.setId(id);
        return sizeType;
    }
}
