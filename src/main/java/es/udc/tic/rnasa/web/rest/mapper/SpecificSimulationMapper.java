/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.SpecificSimulationDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity SpecificSimulation and its DTO SpecificSimulationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SpecificSimulationMapper {

    @Mapping(source = "status.id", target = "statusId")
    @Mapping(source = "belong.id", target = "belongId")
    @Mapping(source = "launchBy.id", target = "launchById")
    @Mapping(source = "useData.id", target = "useDataId")
    SpecificSimulationDTO specificSimulationToSpecificSimulationDTO(SpecificSimulation specificSimulation);

    List<SpecificSimulationDTO> specificSimulationsToSpecificSimulationDTOs(List<SpecificSimulation> specificSimulations);

    @Mapping(source = "statusId", target = "status")
    @Mapping(source = "belongId", target = "belong")
    @Mapping(source = "launchById", target = "launchBy")
    @Mapping(source = "useDataId", target = "useData")
    SpecificSimulation specificSimulationDTOToSpecificSimulation(SpecificSimulationDTO specificSimulationDTO);

    List<SpecificSimulation> specificSimulationDTOsToSpecificSimulations(List<SpecificSimulationDTO> specificSimulationDTOs);

    default SimulationStatusType simulationStatusTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        SimulationStatusType simulationStatusType = new SimulationStatusType();
        simulationStatusType.setId(id);
        return simulationStatusType;
    }

    default Simulation simulationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Simulation simulation = new Simulation();
        simulation.setId(id);
        return simulation;
    }

    default SshAccount sshAccountFromId(Long id) {
        if (id == null) {
            return null;
        }
        SshAccount sshAccount = new SshAccount();
        sshAccount.setId(id);
        return sshAccount;
    }

    default DatasetCombination datasetCombinationFromId(Long id) {
        if (id == null) {
            return null;
        }
        DatasetCombination datasetCombination = new DatasetCombination();
        datasetCombination.setId(id);
        return datasetCombination;
    }
}
