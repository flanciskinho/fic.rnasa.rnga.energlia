/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.SshServerDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity SshServer and its DTO SshServerDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SshServerMapper {

    @Mapping(source = "use.id", target = "useId")
    @Mapping(source = "use.name", target = "useName")
    SshServerDTO sshServerToSshServerDTO(SshServer sshServer);

    List<SshServerDTO> sshServersToSshServerDTOs(List<SshServer> sshServers);

    @Mapping(target = "version", ignore = true)
    @Mapping(source = "useId", target = "use")
    SshServer sshServerDTOToSshServer(SshServerDTO sshServerDTO);

    List<SshServer> sshServerDTOsToSshServers(List<SshServerDTO> sshServerDTOs);

    default QueueType queueTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        QueueType queueType = new QueueType();
        queueType.setId(id);
        return queueType;
    }
}
