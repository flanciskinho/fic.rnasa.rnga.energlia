/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.mapper;

import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.web.rest.dto.TechniqueParamDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity TechniqueParam and its DTO TechniqueParamDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TechniqueParamMapper {

    @Mapping(source = "belong.id", target = "belongId")
    TechniqueParamDTO techniqueParamToTechniqueParamDTO(TechniqueParam techniqueParam);

    List<TechniqueParamDTO> techniqueParamsToTechniqueParamDTOs(List<TechniqueParam> techniqueParams);

    @Mapping(source = "belongId", target = "belong")
    TechniqueParam techniqueParamDTOToTechniqueParam(TechniqueParamDTO techniqueParamDTO);

    List<TechniqueParam> techniqueParamDTOsToTechniqueParams(List<TechniqueParamDTO> techniqueParamDTOs);

    default LogRecord logRecordFromId(Long id) {
        if (id == null) {
            return null;
        }
        LogRecord logRecord = new LogRecord();
        logRecord.setId(id);
        return logRecord;
    }
}
