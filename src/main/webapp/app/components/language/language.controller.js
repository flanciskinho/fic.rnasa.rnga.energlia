/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('JhiLanguageController', JhiLanguageController);

    JhiLanguageController.$inject = ['$translate', 'JhiLanguageService', 'tmhDynamicLocale'];

    function JhiLanguageController ($translate, JhiLanguageService, tmhDynamicLocale) {
        var vm = this;

        vm.changeLanguage = changeLanguage;
        vm.languages = null;

        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function changeLanguage (languageKey) {
            $translate.use(languageKey);
            tmhDynamicLocale.set(languageKey);
        }


        vm.getEmojiFlag = function(lang) {
            var tmp = '';
            switch (lang) {
                case 'es': tmp = 'em-es'; break;
                case 'da': tmp = 'em-da'; break;
                case 'en': tmp = 'em-uk'; break;
                case 'it': tmp = 'em-it'; break;
                case 'fr': tmp = 'em-fr'; break;
                case 'ja': tmp = 'em-jp'; break;
                case 'ru': tmp = 'em-ru'; break;
                case 'en': tmp = 'em-uk'; break;// 'em-us', 'em-gb
                case 'zh-cn':
                case 'zh-tw': tmp = 'em-cn';  break;
                case 'de': tmp = 'em-de'; break;
            }

             return tmp;
        };
    }

})();
