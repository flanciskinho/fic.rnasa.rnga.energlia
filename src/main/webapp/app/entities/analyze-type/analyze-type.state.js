/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('analyze-type', {
            parent: 'entity',
            url: '/analyze-type?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.analyzeType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/analyze-type/analyze-types.html',
                    controller: 'AnalyzeTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('analyzeType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('analyze-type-detail', {
            parent: 'entity',
            url: '/analyze-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.analyzeType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/analyze-type/analyze-type-detail.html',
                    controller: 'AnalyzeTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('analyzeType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AnalyzeType', function($stateParams, AnalyzeType) {
                    return AnalyzeType.get({id : $stateParams.id});
                }]
            }
        })
        .state('analyze-type.new', {
            parent: 'analyze-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/analyze-type/analyze-type-dialog.html',
                    controller: 'AnalyzeTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('analyze-type', null, { reload: true });
                }, function() {
                    $state.go('analyze-type');
                });
            }]
        })
        .state('analyze-type.edit', {
            parent: 'analyze-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/analyze-type/analyze-type-dialog.html',
                    controller: 'AnalyzeTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AnalyzeType', function(AnalyzeType) {
                            return AnalyzeType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('analyze-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('analyze-type.delete', {
            parent: 'analyze-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/analyze-type/analyze-type-delete-dialog.html',
                    controller: 'AnalyzeTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AnalyzeType', function(AnalyzeType) {
                            return AnalyzeType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('analyze-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
