/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('ArchTypeDetailController', ArchTypeDetailController);

    ArchTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ArchType', 'SshServer'];

    function ArchTypeDetailController($scope, $rootScope, $stateParams, entity, ArchType, SshServer) {
        var vm = this;
        vm.archType = entity;
        vm.load = function (id) {
            ArchType.get({id: id}, function(result) {
                vm.archType = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:archTypeUpdate', function(event, result) {
            vm.archType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
