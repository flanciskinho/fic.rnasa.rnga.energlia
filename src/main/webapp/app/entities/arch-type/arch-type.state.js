/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('arch-type', {
            parent: 'entity',
            url: '/arch-type?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.archType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/arch-type/arch-types.html',
                    controller: 'ArchTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('archType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('arch-type-detail', {
            parent: 'entity',
            url: '/arch-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.archType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/arch-type/arch-type-detail.html',
                    controller: 'ArchTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('archType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ArchType', function($stateParams, ArchType) {
                    return ArchType.get({id : $stateParams.id});
                }]
            }
        })
        .state('arch-type.new', {
            parent: 'arch-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/arch-type/arch-type-dialog.html',
                    controller: 'ArchTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                activated: false,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('arch-type', null, { reload: true });
                }, function() {
                    $state.go('arch-type');
                });
            }]
        })
        .state('arch-type.edit', {
            parent: 'arch-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/arch-type/arch-type-dialog.html',
                    controller: 'ArchTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ArchType', function(ArchType) {
                            return ArchType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('arch-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('arch-type.delete', {
            parent: 'arch-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/arch-type/arch-type-delete-dialog.html',
                    controller: 'ArchTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ArchType', function(ArchType) {
                            return ArchType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('arch-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
