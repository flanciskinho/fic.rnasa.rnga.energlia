/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('CompiledAppDialogController', CompiledAppDialogController);

    CompiledAppDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'CompiledApp', 'Application', 'SshServer'];

    function CompiledAppDialogController ($scope, $stateParams, $uibModalInstance, DataUtils, entity, CompiledApp, Application, SshServer) {
        var vm = this;
        vm.compiledApp = entity;
        vm.applications = Application.query();
        vm.sshservers = SshServer.query();
        vm.load = function(id) {
            CompiledApp.get({id : id}, function(result) {
                vm.compiledApp = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:compiledAppUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.compiledApp.id !== null) {
                CompiledApp.update(vm.compiledApp, onSaveSuccess, onSaveError);
            } else {
                CompiledApp.save(vm.compiledApp, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.setExecutable = function ($file, compiledApp) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        compiledApp.executable = base64Data;
                        compiledApp.executableContentType = $file.type;
                    });
                });
            }
        };

        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
    }
})();
