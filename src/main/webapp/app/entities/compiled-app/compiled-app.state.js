/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('compiled-app', {
            parent: 'entity',
            url: '/compiled-app?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.compiledApp.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/compiled-app/compiled-apps.html',
                    controller: 'CompiledAppController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('compiledApp');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('compiled-app-detail', {
            parent: 'entity',
            url: '/compiled-app/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.compiledApp.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/compiled-app/compiled-app-detail.html',
                    controller: 'CompiledAppDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('compiledApp');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CompiledApp', function($stateParams, CompiledApp) {
                    return CompiledApp.get({id : $stateParams.id});
                }]
            }
        })
        .state('compiled-app.new', {
            parent: 'compiled-app',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/compiled-app/compiled-app-dialog.html',
                    controller: 'CompiledAppDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                executable: null,
                                executableContentType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('compiled-app', null, { reload: true });
                }, function() {
                    $state.go('compiled-app');
                });
            }]
        })
        .state('compiled-app.edit', {
            parent: 'compiled-app',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/compiled-app/compiled-app-dialog.html',
                    controller: 'CompiledAppDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CompiledApp', function(CompiledApp) {
                            return CompiledApp.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('compiled-app', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('compiled-app.delete', {
            parent: 'compiled-app',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/compiled-app/compiled-app-delete-dialog.html',
                    controller: 'CompiledAppDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CompiledApp', function(CompiledApp) {
                            return CompiledApp.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('compiled-app', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
