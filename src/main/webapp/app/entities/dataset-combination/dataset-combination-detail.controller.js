/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('DatasetCombinationDetailController', DatasetCombinationDetailController);

    DatasetCombinationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'DataUtils', 'entity', 'DatasetCombination', 'Dataset'];

    function DatasetCombinationDetailController($scope, $rootScope, $stateParams, DataUtils, entity, DatasetCombination, Dataset) {
        var vm = this;
        vm.datasetCombination = entity;
        vm.load = function (id) {
            DatasetCombination.get({id: id}, function(result) {
                vm.datasetCombination = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:datasetCombinationUpdate', function(event, result) {
            vm.datasetCombination = result;
        });
        $scope.$on('$destroy', unsubscribe);

        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
    }
})();
