/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('DatasetCombinationDialogController', DatasetCombinationDialogController);

    DatasetCombinationDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'DatasetCombination', 'Dataset'];

    function DatasetCombinationDialogController ($scope, $stateParams, $uibModalInstance, DataUtils, entity, DatasetCombination, Dataset) {
        var vm = this;
        vm.datasetCombination = entity;
        vm.datasets = Dataset.query();
        vm.load = function(id) {
            DatasetCombination.get({id : id}, function(result) {
                vm.datasetCombination = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:datasetCombinationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.datasetCombination.id !== null) {
                DatasetCombination.update(vm.datasetCombination, onSaveSuccess, onSaveError);
            } else {
                DatasetCombination.save(vm.datasetCombination, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.setTrainfilein = function ($file, datasetCombination) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        datasetCombination.trainfilein = base64Data;
                        datasetCombination.trainfileinContentType = $file.type;
                    });
                });
            }
        };

        vm.setTrainfileout = function ($file, datasetCombination) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        datasetCombination.trainfileout = base64Data;
                        datasetCombination.trainfileoutContentType = $file.type;
                    });
                });
            }
        };

        vm.setValidationfilein = function ($file, datasetCombination) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        datasetCombination.validationfilein = base64Data;
                        datasetCombination.validationfileinContentType = $file.type;
                    });
                });
            }
        };

        vm.setValidationfileout = function ($file, datasetCombination) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        datasetCombination.validationfileout = base64Data;
                        datasetCombination.validationfileoutContentType = $file.type;
                    });
                });
            }
        };

        vm.setTestfilein = function ($file, datasetCombination) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        datasetCombination.testfilein = base64Data;
                        datasetCombination.testfileinContentType = $file.type;
                    });
                });
            }
        };

        vm.setTestfileout = function ($file, datasetCombination) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        datasetCombination.testfileout = base64Data;
                        datasetCombination.testfileoutContentType = $file.type;
                    });
                });
            }
        };

        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
    }
})();
