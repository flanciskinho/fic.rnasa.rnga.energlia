/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('DatasetDeleteController',DatasetDeleteController);

    DatasetDeleteController.$inject = ['$uibModalInstance', 'entity', 'Dataset'];

    function DatasetDeleteController($uibModalInstance, entity, Dataset) {
        var vm = this;
        vm.dataset = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Dataset.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
