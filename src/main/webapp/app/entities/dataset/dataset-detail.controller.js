/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('DatasetDetailController', DatasetDetailController);

    DatasetDetailController.$inject = ['$scope', '$rootScope', '$stateParams', '$http', '$filter', 'entity', 'Dataset', 'User', 'AnalyzeType'];

    function DatasetDetailController($scope, $rootScope, $stateParams, $http, $filter, entity, Dataset, User, AnalyzeType) {
        var vm = this;
        vm.dataset = entity;

        vm.share = false;
        vm.loginname = "";
        vm.edit = false;
        vm.editDescription = vm.dataset.description;

        vm.timeout=5000;
        vm.alerts = [];

        vm.shareDataset = function() {
            var info = {
                resourceId: vm.dataset.id,
                loginname: vm.loginname
            };

            $http.post('api/dataset-share', info)
                .success(function(data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                    console.log(headers);
                    console.log(config);
                    vm.addAlert('entity.update.share');
                    vm.loginname = "";
                    vm.share = false;
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                    console.log(headers);
                    console.log(config);
                    vm.addAlert('entity.update.notShare', 'danger');
                });
        };

        vm.addAlert = function(message, type) {
            console.log("add Alert");

            message = typeof message !== 'undefined'? message: 'entity.update.description';
            type    = typeof type    !== 'undefined'? type: 'success';

            vm.alerts.push({msg: $filter('translate')(message), type: type});
        };

        vm.closeAlert = function(index) {
            vm.alerts.splice(index, 1);
        };

        vm.update = function () {
            var info = {
                id : vm.dataset.id,
                description : vm.editDescription
            };

            console.log(info);

            $http.put('api/custom-datasets', info).
            success(function(data, status, headers, config) {
                console.log(data);
                console.log(status);
                //console.log(headers);
                //console.log(config);
                vm.dataset.description = data.description;
                vm.addAlert();
                vm.edit = false;
            }).error(function (data, status, headers, config) {
                console.log("error");
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
            });
        };

        vm.changeEdit = function () {
            console.log("change edit function");
            vm.edit = !vm.edit;
        };



        vm.load = function (id) {
            Dataset.get({id: id}, function(result) {
                vm.dataset = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:datasetUpdate', function(event, result) {
            vm.dataset = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
