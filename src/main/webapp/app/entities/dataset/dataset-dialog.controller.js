/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('DatasetDialogController', DatasetDialogController);

    DatasetDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$http', 'DataUtils', 'entity', 'Dataset', 'User', 'AnalyzeType'];

    function DatasetDialogController ($scope, $stateParams, $uibModalInstance, $http, DataUtils, entity, Dataset, User, AnalyzeType) {
        var vm = this;

        vm.checkTrain = true;
        vm.checkValidation = false;
        vm.checkTest = false;

        vm.traininfile  = [];
        vm.trainoutfile = [];

        vm.validationinfile  = [];
        vm.validationoutfile = [];

        vm.testinfile  = [];
        vm.testoutfile = [];

        vm.dataset = entity;
        vm.users = User.query();
        vm.analyzetypes = AnalyzeType.query();

        vm.load = function(id) {
            Dataset.get({id : id}, function(result) {
                vm.dataset = result;
            });
        };

        vm.updateArray = function($file, array, index) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Date) {
                    $scope.$apply(function() {
                        array[index] = base64Date;
                    });
                });
            }
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:datasetUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function() {
            vm.isSaving = true;

            var info = {
                name : vm.dataset.name,
                description : vm.dataset.description,
                analyzeId: vm.dataset.analyzeId,
                trainfilesin  : (vm.checkTrain) ? vm.traininfile : [],
                trainfilesout : (vm.checkTrain) ? vm.trainoutfile: [],
                validationfilesin  : (vm.checkTrain && vm.checkValidation) ? vm.validationinfile : [],
                validationfilesout : (vm.checkTrain && vm.checkValidation) ? vm.validationoutfile: [],
                testfilesin  : (vm.checkTrain && vm.checkValidation && vm.checkTest) ? vm.testinfile : [],
                testfilesout : (vm.checkTrain && vm.checkValidation && vm.checkTest) ? vm.testoutfile: []
            };

            $http.post('api/custom-datasets', info).
                success(function(data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                    //console.log(headers);
                    //console.log(config);
                    $uibModalInstance.close(true);
                }).error(function (data, status, headers, config) {
                    console.log("error");
                    console.log(data);
                    console.log(status);
                    console.log(headers);
                    console.log(config);
                });

        };

        /*
        vm.save = function () {
            vm.isSaving = true;
            if (vm.dataset.id !== null) {
                Dataset.update(vm.dataset, onSaveSuccess, onSaveError);
            } else {
                Dataset.save(vm.dataset, onSaveSuccess, onSaveError);
            }
        };
        */

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.timestamp = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
