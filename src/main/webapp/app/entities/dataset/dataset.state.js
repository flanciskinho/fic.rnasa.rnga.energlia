/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dataset', {
            parent: 'entity',
            url: '/dataset?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.dataset.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dataset/datasets.html',
                    controller: 'DatasetController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'timestamp,desc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dataset');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('dataset-detail', {
            parent: 'entity',
            url: '/dataset/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.dataset.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dataset/dataset-detail.html',
                    controller: 'DatasetDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dataset');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Dataset', function($stateParams, Dataset) {
                    return Dataset.get({id : $stateParams.id});
                }]
            }
        })
        .state('dataset.new', {
            parent: 'dataset',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dataset/dataset-dialog.html',
                    controller: 'DatasetDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                timestamp: null,
                                numPattern: null,
                                outputFeature: null,
                                inputFeature: null,
                                numCombination: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dataset', null, { reload: true });
                }, function() {
                    $state.go('dataset');
                });
            }]
        })
        .state('dataset.delete', {
            parent: 'dataset',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dataset/dataset-delete-dialog.html',
                    controller: 'DatasetDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Dataset', function(Dataset) {
                            return Dataset.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('dataset', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
