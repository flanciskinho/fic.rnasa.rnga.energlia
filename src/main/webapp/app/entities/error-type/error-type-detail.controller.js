/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('ErrorTypeDetailController', ErrorTypeDetailController);

    ErrorTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ErrorType'];

    function ErrorTypeDetailController($scope, $rootScope, $stateParams, entity, ErrorType) {
        var vm = this;
        vm.errorType = entity;
        vm.load = function (id) {
            ErrorType.get({id: id}, function(result) {
                vm.errorType = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:errorTypeUpdate', function(event, result) {
            vm.errorType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
