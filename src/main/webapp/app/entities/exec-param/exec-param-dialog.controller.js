/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('ExecParamDialogController', ExecParamDialogController);

    ExecParamDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ExecParam', 'Simulation'];

    function ExecParamDialogController ($scope, $stateParams, $uibModalInstance, entity, ExecParam, Simulation) {
        var vm = this;
        vm.execParam = entity;
        vm.simulations = Simulation.query();
        vm.load = function(id) {
            ExecParam.get({id : id}, function(result) {
                vm.execParam = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:execParamUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.execParam.id !== null) {
                ExecParam.update(vm.execParam, onSaveSuccess, onSaveError);
            } else {
                ExecParam.save(vm.execParam, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
