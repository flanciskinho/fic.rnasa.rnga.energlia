/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('ga-param', {
            parent: 'entity',
            url: '/ga-param?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.gaParam.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ga-param/ga-params.html',
                    controller: 'GaParamController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gaParam');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('ga-param-detail', {
            parent: 'entity',
            url: '/ga-param/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.gaParam.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ga-param/ga-param-detail.html',
                    controller: 'GaParamDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gaParam');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'GaParam', function($stateParams, GaParam) {
                    return GaParam.get({id : $stateParams.id});
                }]
            }
        })
        .state('ga-param.new', {
            parent: 'ga-param',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga-param/ga-param-dialog.html',
                    controller: 'GaParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                value: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('ga-param', null, { reload: true });
                }, function() {
                    $state.go('ga-param');
                });
            }]
        })
        .state('ga-param.edit', {
            parent: 'ga-param',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga-param/ga-param-dialog.html',
                    controller: 'GaParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GaParam', function(GaParam) {
                            return GaParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ga-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('ga-param.delete', {
            parent: 'ga-param',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga-param/ga-param-delete-dialog.html',
                    controller: 'GaParamDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GaParam', function(GaParam) {
                            return GaParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ga-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
