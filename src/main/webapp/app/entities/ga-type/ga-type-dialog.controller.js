/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('GaTypeDialogController', GaTypeDialogController);

    GaTypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'GaType'];

    function GaTypeDialogController ($scope, $stateParams, $uibModalInstance, entity, GaType) {
        var vm = this;
        vm.gaType = entity;
        vm.load = function(id) {
            GaType.get({id : id}, function(result) {
                vm.gaType = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:gaTypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.gaType.id !== null) {
                GaType.update(vm.gaType, onSaveSuccess, onSaveError);
            } else {
                GaType.save(vm.gaType, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
