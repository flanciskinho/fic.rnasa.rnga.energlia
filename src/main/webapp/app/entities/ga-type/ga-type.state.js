/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('ga-type', {
            parent: 'entity',
            url: '/ga-type?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.gaType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ga-type/ga-types.html',
                    controller: 'GaTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gaType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('ga-type-detail', {
            parent: 'entity',
            url: '/ga-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.gaType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ga-type/ga-type-detail.html',
                    controller: 'GaTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gaType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'GaType', function($stateParams, GaType) {
                    return GaType.get({id : $stateParams.id});
                }]
            }
        })
        .state('ga-type.new', {
            parent: 'ga-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga-type/ga-type-dialog.html',
                    controller: 'GaTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('ga-type', null, { reload: true });
                }, function() {
                    $state.go('ga-type');
                });
            }]
        })
        .state('ga-type.edit', {
            parent: 'ga-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga-type/ga-type-dialog.html',
                    controller: 'GaTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GaType', function(GaType) {
                            return GaType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ga-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('ga-type.delete', {
            parent: 'ga-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga-type/ga-type-delete-dialog.html',
                    controller: 'GaTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GaType', function(GaType) {
                            return GaType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ga-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
