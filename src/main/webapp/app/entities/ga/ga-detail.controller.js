/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('GaDetailController', GaDetailController);

    GaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Ga', 'Simulation', 'GaType'];

    function GaDetailController($scope, $rootScope, $stateParams, entity, Ga, Simulation, GaType) {
        var vm = this;
        vm.ga = entity;
        vm.load = function (id) {
            Ga.get({id: id}, function(result) {
                vm.ga = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:gaUpdate', function(event, result) {
            vm.ga = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
