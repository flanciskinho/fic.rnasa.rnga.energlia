/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('GaDialogController', GaDialogController);

    GaDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Ga', 'Simulation', 'GaType'];

    function GaDialogController ($scope, $stateParams, $uibModalInstance, $q, entity, Ga, Simulation, GaType) {
        var vm = this;
        vm.ga = entity;
        vm.belongs = Simulation.query({filter: 'ga-is-null'});
        $q.all([vm.ga.$promise, vm.belongs.$promise]).then(function() {
            if (!vm.ga.belongId) {
                return $q.reject();
            }
            return Simulation.get({id : vm.ga.belongId}).$promise;
        }).then(function(belong) {
            vm.belongs.push(belong);
        });
        vm.gatypes = GaType.query();
        vm.load = function(id) {
            Ga.get({id : id}, function(result) {
                vm.ga = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:gaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.ga.id !== null) {
                Ga.update(vm.ga, onSaveSuccess, onSaveError);
            } else {
                Ga.save(vm.ga, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
