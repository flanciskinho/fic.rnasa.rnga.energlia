/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('ga', {
            parent: 'entity',
            url: '/ga?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.ga.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ga/gas.html',
                    controller: 'GaController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('ga');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('ga-detail', {
            parent: 'entity',
            url: '/ga/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.ga.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ga/ga-detail.html',
                    controller: 'GaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('ga');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Ga', function($stateParams, Ga) {
                    return Ga.get({id : $stateParams.id});
                }]
            }
        })
        .state('ga.new', {
            parent: 'ga',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga/ga-dialog.html',
                    controller: 'GaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                version: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('ga', null, { reload: true });
                }, function() {
                    $state.go('ga');
                });
            }]
        })
        .state('ga.edit', {
            parent: 'ga',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga/ga-dialog.html',
                    controller: 'GaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Ga', function(Ga) {
                            return Ga.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ga', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('ga.delete', {
            parent: 'ga',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ga/ga-delete-dialog.html',
                    controller: 'GaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Ga', function(Ga) {
                            return Ga.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ga', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
