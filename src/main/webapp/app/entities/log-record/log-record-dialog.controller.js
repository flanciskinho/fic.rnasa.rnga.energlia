/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('LogRecordDialogController', LogRecordDialogController);

    LogRecordDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'LogRecord', 'SpecificSimulation'];

    function LogRecordDialogController ($scope, $stateParams, $uibModalInstance, entity, LogRecord, SpecificSimulation) {
        var vm = this;
        vm.logRecord = entity;
        vm.specificsimulations = SpecificSimulation.query();
        vm.load = function(id) {
            LogRecord.get({id : id}, function(result) {
                vm.logRecord = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:logRecordUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.logRecord.id !== null) {
                LogRecord.update(vm.logRecord, onSaveSuccess, onSaveError);
            } else {
                LogRecord.save(vm.logRecord, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
