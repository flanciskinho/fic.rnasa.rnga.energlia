/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('log-record', {
            parent: 'entity',
            url: '/log-record?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.logRecord.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/log-record/log-records.html',
                    controller: 'LogRecordController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('logRecord');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('log-record-detail', {
            parent: 'entity',
            url: '/log-record/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.logRecord.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/log-record/log-record-detail.html',
                    controller: 'LogRecordDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('logRecord');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'LogRecord', function($stateParams, LogRecord) {
                    return LogRecord.get({id : $stateParams.id});
                }]
            }
        })
        .state('log-record.new', {
            parent: 'log-record',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/log-record/log-record-dialog.html',
                    controller: 'LogRecordDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                indexRecord: null,
                                timestamp: null,
                                errorTrain: null,
                                errorValidation: null,
                                errorTest: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('log-record', null, { reload: true });
                }, function() {
                    $state.go('log-record');
                });
            }]
        })
        .state('log-record.edit', {
            parent: 'log-record',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/log-record/log-record-dialog.html',
                    controller: 'LogRecordDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['LogRecord', function(LogRecord) {
                            return LogRecord.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('log-record', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('log-record.delete', {
            parent: 'log-record',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/log-record/log-record-delete-dialog.html',
                    controller: 'LogRecordDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['LogRecord', function(LogRecord) {
                            return LogRecord.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('log-record', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
