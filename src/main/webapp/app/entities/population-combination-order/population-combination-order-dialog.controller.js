/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('PopulationCombinationOrderDialogController', PopulationCombinationOrderDialogController);

    PopulationCombinationOrderDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'PopulationCombinationOrder', 'SpecificSimulation', 'PopulationCombination'];

    function PopulationCombinationOrderDialogController ($scope, $stateParams, $uibModalInstance, entity, PopulationCombinationOrder, SpecificSimulation, PopulationCombination) {
        var vm = this;
        vm.populationCombinationOrder = entity;
        vm.specificsimulations = SpecificSimulation.query();
        vm.populationcombinations = PopulationCombination.query();
        vm.load = function(id) {
            PopulationCombinationOrder.get({id : id}, function(result) {
                vm.populationCombinationOrder = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:populationCombinationOrderUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.populationCombinationOrder.id !== null) {
                PopulationCombinationOrder.update(vm.populationCombinationOrder, onSaveSuccess, onSaveError);
            } else {
                PopulationCombinationOrder.save(vm.populationCombinationOrder, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
