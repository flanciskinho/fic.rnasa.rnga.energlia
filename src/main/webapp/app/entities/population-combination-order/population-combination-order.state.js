/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('population-combination-order', {
            parent: 'entity',
            url: '/population-combination-order?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationCombinationOrder.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-combination-order/population-combination-orders.html',
                    controller: 'PopulationCombinationOrderController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationCombinationOrder');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('population-combination-order-detail', {
            parent: 'entity',
            url: '/population-combination-order/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationCombinationOrder.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-combination-order/population-combination-order-detail.html',
                    controller: 'PopulationCombinationOrderDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationCombinationOrder');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PopulationCombinationOrder', function($stateParams, PopulationCombinationOrder) {
                    return PopulationCombinationOrder.get({id : $stateParams.id});
                }]
            }
        })
        .state('population-combination-order.new', {
            parent: 'population-combination-order',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-combination-order/population-combination-order-dialog.html',
                    controller: 'PopulationCombinationOrderDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                indexOrder: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('population-combination-order', null, { reload: true });
                }, function() {
                    $state.go('population-combination-order');
                });
            }]
        })
        .state('population-combination-order.edit', {
            parent: 'population-combination-order',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-combination-order/population-combination-order-dialog.html',
                    controller: 'PopulationCombinationOrderDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PopulationCombinationOrder', function(PopulationCombinationOrder) {
                            return PopulationCombinationOrder.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-combination-order', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('population-combination-order.delete', {
            parent: 'population-combination-order',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-combination-order/population-combination-order-delete-dialog.html',
                    controller: 'PopulationCombinationOrderDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PopulationCombinationOrder', function(PopulationCombinationOrder) {
                            return PopulationCombinationOrder.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-combination-order', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
