/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('PopulationCombinationDetailController', PopulationCombinationDetailController);

    PopulationCombinationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'DataUtils', 'entity', 'PopulationCombination', 'Population'];

    function PopulationCombinationDetailController($scope, $rootScope, $stateParams, DataUtils, entity, PopulationCombination, Population) {
        var vm = this;
        vm.populationCombination = entity;
        vm.load = function (id) {
            PopulationCombination.get({id: id}, function(result) {
                vm.populationCombination = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:populationCombinationUpdate', function(event, result) {
            vm.populationCombination = result;
        });
        $scope.$on('$destroy', unsubscribe);

        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
    }
})();
