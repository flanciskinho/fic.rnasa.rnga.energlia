/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('PopulationCombinationDialogController', PopulationCombinationDialogController);

    PopulationCombinationDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'PopulationCombination', 'Population'];

    function PopulationCombinationDialogController ($scope, $stateParams, $uibModalInstance, DataUtils, entity, PopulationCombination, Population) {
        var vm = this;
        vm.populationCombination = entity;
        vm.populations = Population.query();
        vm.load = function(id) {
            PopulationCombination.get({id : id}, function(result) {
                vm.populationCombination = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:populationCombinationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.populationCombination.id !== null) {
                PopulationCombination.update(vm.populationCombination, onSaveSuccess, onSaveError);
            } else {
                PopulationCombination.save(vm.populationCombination, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.setFile = function ($file, populationCombination) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        populationCombination.file = base64Data;
                        populationCombination.fileContentType = $file.type;
                    });
                });
            }
        };

        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
    }
})();
