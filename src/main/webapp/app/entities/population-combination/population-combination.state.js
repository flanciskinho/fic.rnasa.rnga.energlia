/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('population-combination', {
            parent: 'entity',
            url: '/population-combination?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationCombination.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-combination/population-combinations.html',
                    controller: 'PopulationCombinationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationCombination');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('population-combination-detail', {
            parent: 'entity',
            url: '/population-combination/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationCombination.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-combination/population-combination-detail.html',
                    controller: 'PopulationCombinationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationCombination');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PopulationCombination', function($stateParams, PopulationCombination) {
                    return PopulationCombination.get({id : $stateParams.id});
                }]
            }
        })
        .state('population-combination.new', {
            parent: 'population-combination',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-combination/population-combination-dialog.html',
                    controller: 'PopulationCombinationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                index: null,
                                file: null,
                                fileContentType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('population-combination', null, { reload: true });
                }, function() {
                    $state.go('population-combination');
                });
            }]
        })
        .state('population-combination.edit', {
            parent: 'population-combination',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-combination/population-combination-dialog.html',
                    controller: 'PopulationCombinationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PopulationCombination', function(PopulationCombination) {
                            return PopulationCombination.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-combination', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('population-combination.delete', {
            parent: 'population-combination',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-combination/population-combination-delete-dialog.html',
                    controller: 'PopulationCombinationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PopulationCombination', function(PopulationCombination) {
                            return PopulationCombination.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-combination', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
