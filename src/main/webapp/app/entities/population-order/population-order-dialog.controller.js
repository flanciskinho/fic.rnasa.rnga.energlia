/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('PopulationOrderDialogController', PopulationOrderDialogController);

    PopulationOrderDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'PopulationOrder', 'Ga', 'Population'];

    function PopulationOrderDialogController ($scope, $stateParams, $uibModalInstance, entity, PopulationOrder, Ga, Population) {
        var vm = this;
        vm.populationOrder = entity;
        vm.gas = Ga.query();
        vm.populations = Population.query();
        vm.load = function(id) {
            PopulationOrder.get({id : id}, function(result) {
                vm.populationOrder = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:populationOrderUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.populationOrder.id !== null) {
                PopulationOrder.update(vm.populationOrder, onSaveSuccess, onSaveError);
            } else {
                PopulationOrder.save(vm.populationOrder, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
