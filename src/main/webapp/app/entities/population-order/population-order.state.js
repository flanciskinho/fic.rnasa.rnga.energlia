/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('population-order', {
            parent: 'entity',
            url: '/population-order?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationOrder.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-order/population-orders.html',
                    controller: 'PopulationOrderController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationOrder');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('population-order-detail', {
            parent: 'entity',
            url: '/population-order/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationOrder.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-order/population-order-detail.html',
                    controller: 'PopulationOrderDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationOrder');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PopulationOrder', function($stateParams, PopulationOrder) {
                    return PopulationOrder.get({id : $stateParams.id});
                }]
            }
        })
        .state('population-order.new', {
            parent: 'population-order',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-order/population-order-dialog.html',
                    controller: 'PopulationOrderDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                indexOrder: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('population-order', null, { reload: true });
                }, function() {
                    $state.go('population-order');
                });
            }]
        })
        .state('population-order.edit', {
            parent: 'population-order',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-order/population-order-dialog.html',
                    controller: 'PopulationOrderDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PopulationOrder', function(PopulationOrder) {
                            return PopulationOrder.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-order', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('population-order.delete', {
            parent: 'population-order',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-order/population-order-delete-dialog.html',
                    controller: 'PopulationOrderDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PopulationOrder', function(PopulationOrder) {
                            return PopulationOrder.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-order', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
