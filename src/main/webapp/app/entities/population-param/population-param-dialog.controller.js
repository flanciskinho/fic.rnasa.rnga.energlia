/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('PopulationParamDialogController', PopulationParamDialogController);

    PopulationParamDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'PopulationParam', 'PopulationOrder'];

    function PopulationParamDialogController ($scope, $stateParams, $uibModalInstance, entity, PopulationParam, PopulationOrder) {
        var vm = this;
        vm.populationParam = entity;
        vm.populationorders = PopulationOrder.query();
        vm.load = function(id) {
            PopulationParam.get({id : id}, function(result) {
                vm.populationParam = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:populationParamUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.populationParam.id !== null) {
                PopulationParam.update(vm.populationParam, onSaveSuccess, onSaveError);
            } else {
                PopulationParam.save(vm.populationParam, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
