/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('population-param', {
            parent: 'entity',
            url: '/population-param?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationParam.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-param/population-params.html',
                    controller: 'PopulationParamController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationParam');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('population-param-detail', {
            parent: 'entity',
            url: '/population-param/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.populationParam.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population-param/population-param-detail.html',
                    controller: 'PopulationParamDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('populationParam');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PopulationParam', function($stateParams, PopulationParam) {
                    return PopulationParam.get({id : $stateParams.id});
                }]
            }
        })
        .state('population-param.new', {
            parent: 'population-param',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-param/population-param-dialog.html',
                    controller: 'PopulationParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                value: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('population-param', null, { reload: true });
                }, function() {
                    $state.go('population-param');
                });
            }]
        })
        .state('population-param.edit', {
            parent: 'population-param',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-param/population-param-dialog.html',
                    controller: 'PopulationParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PopulationParam', function(PopulationParam) {
                            return PopulationParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('population-param.delete', {
            parent: 'population-param',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population-param/population-param-delete-dialog.html',
                    controller: 'PopulationParamDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PopulationParam', function(PopulationParam) {
                            return PopulationParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
