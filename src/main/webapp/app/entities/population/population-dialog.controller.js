/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('PopulationDialogController', PopulationDialogController);

    PopulationDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$http', 'DataUtils', 'entity', 'Population', 'User'];

    function PopulationDialogController ($scope, $stateParams, $uibModalInstance, $http, DataUtils, entity, Population, User) {
        var vm = this;
        vm.population = entity;
        vm.users = User.query();

        vm.files = [];

        vm.load = function(id) {
            Population.get({id : id}, function(result) {
                vm.population = result;
            });
        };

        vm.updateArray = function($file, array, index) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Date) {
                    $scope.$apply(function() {
                        array[index] = base64Date;
                    });
                });
            }
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:populationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };


        vm.save = function() {
            vm.isSaving = true;

            var info = {
                name : vm.population.name,
                description : vm.population.description,
                files : vm.files
            };

            console.log(info);


            $http.post('api/custom-populations', info).
            success(function(data, status, headers, config) {
                console.log(data);
                console.log(status);
                //console.log(headers);
                //console.log(config);
                $uibModalInstance.close(true);
            }).error(function (data, status, headers, config) {
                console.log("error");
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
            });

        };

        /*
        vm.save = function () {
            vm.isSaving = true;
            if (vm.population.id !== null) {
                Population.update(vm.population, onSaveSuccess, onSaveError);
            } else {
                Population.save(vm.population, onSaveSuccess, onSaveError);
            }
        };
        */

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.timestamp = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
