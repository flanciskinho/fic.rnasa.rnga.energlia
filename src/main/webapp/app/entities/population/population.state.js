/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('population', {
            parent: 'entity',
            url: '/population?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.population.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population/populations.html',
                    controller: 'PopulationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'timestamp,desc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('population');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('population-detail', {
            parent: 'entity',
            url: '/population/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.population.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/population/population-detail.html',
                    controller: 'PopulationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('population');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Population', function($stateParams, Population) {
                    return Population.get({id : $stateParams.id});
                }]
            }
        })
        .state('population.new', {
            parent: 'population',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population/population-dialog.html',
                    controller: 'PopulationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                timestamp: null,
                                numIndividual: null,
                                numCombination: null,
                                sizeGenotype: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('population', null, { reload: true });
                }, function() {
                    $state.go('population');
                });
            }]
        })
        .state('population.delete', {
            parent: 'population',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/population/population-delete-dialog.html',
                    controller: 'PopulationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Population', function(Population) {
                            return Population.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('population', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
