/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('ProblemTypeDeleteController',ProblemTypeDeleteController);

    ProblemTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProblemType'];

    function ProblemTypeDeleteController($uibModalInstance, entity, ProblemType) {
        var vm = this;
        vm.problemType = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            ProblemType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
