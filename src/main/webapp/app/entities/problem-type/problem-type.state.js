/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('problem-type', {
            parent: 'entity',
            url: '/problem-type?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.problemType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/problem-type/problem-types.html',
                    controller: 'ProblemTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('problemType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('problem-type-detail', {
            parent: 'entity',
            url: '/problem-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.problemType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/problem-type/problem-type-detail.html',
                    controller: 'ProblemTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('problemType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ProblemType', function($stateParams, ProblemType) {
                    return ProblemType.get({id : $stateParams.id});
                }]
            }
        })
        .state('problem-type.new', {
            parent: 'problem-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/problem-type/problem-type-dialog.html',
                    controller: 'ProblemTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                outputFeatureConstraint: null,
                                numPopulation: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('problem-type', null, { reload: true });
                }, function() {
                    $state.go('problem-type');
                });
            }]
        })
        .state('problem-type.edit', {
            parent: 'problem-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/problem-type/problem-type-dialog.html',
                    controller: 'ProblemTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ProblemType', function(ProblemType) {
                            return ProblemType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('problem-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('problem-type.delete', {
            parent: 'problem-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/problem-type/problem-type-delete-dialog.html',
                    controller: 'ProblemTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ProblemType', function(ProblemType) {
                            return ProblemType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('problem-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
