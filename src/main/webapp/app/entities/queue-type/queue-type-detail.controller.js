/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('QueueTypeDetailController', QueueTypeDetailController);

    QueueTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'QueueType'];

    function QueueTypeDetailController($scope, $rootScope, $stateParams, entity, QueueType) {
        var vm = this;
        vm.queueType = entity;
        vm.load = function (id) {
            QueueType.get({id: id}, function(result) {
                vm.queueType = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:queueTypeUpdate', function(event, result) {
            vm.queueType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
