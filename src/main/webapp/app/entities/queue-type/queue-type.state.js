/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('queue-type', {
            parent: 'entity',
            url: '/queue-type',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.queueType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/queue-type/queue-types.html',
                    controller: 'QueueTypeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('queueType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('queue-type-detail', {
            parent: 'entity',
            url: '/queue-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.queueType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/queue-type/queue-type-detail.html',
                    controller: 'QueueTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('queueType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'QueueType', function($stateParams, QueueType) {
                    return QueueType.get({id : $stateParams.id});
                }]
            }
        })
        .state('queue-type.new', {
            parent: 'queue-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/queue-type/queue-type-dialog.html',
                    controller: 'QueueTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('queue-type', null, { reload: true });
                }, function() {
                    $state.go('queue-type');
                });
            }]
        })
        .state('queue-type.edit', {
            parent: 'queue-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/queue-type/queue-type-dialog.html',
                    controller: 'QueueTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['QueueType', function(QueueType) {
                            return QueueType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('queue-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('queue-type.delete', {
            parent: 'queue-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/queue-type/queue-type-delete-dialog.html',
                    controller: 'QueueTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['QueueType', function(QueueType) {
                            return QueueType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('queue-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
