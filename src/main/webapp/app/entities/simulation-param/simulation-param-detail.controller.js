/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SimulationParamDetailController', SimulationParamDetailController);

    SimulationParamDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'SimulationParam', 'Simulation'];

    function SimulationParamDetailController($scope, $rootScope, $stateParams, entity, SimulationParam, Simulation) {
        var vm = this;
        vm.simulationParam = entity;
        vm.load = function (id) {
            SimulationParam.get({id: id}, function(result) {
                vm.simulationParam = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:simulationParamUpdate', function(event, result) {
            vm.simulationParam = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
