/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SimulationParamDialogController', SimulationParamDialogController);

    SimulationParamDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'SimulationParam', 'Simulation'];

    function SimulationParamDialogController ($scope, $stateParams, $uibModalInstance, entity, SimulationParam, Simulation) {
        var vm = this;
        vm.simulationParam = entity;
        vm.simulations = Simulation.query();
        vm.load = function(id) {
            SimulationParam.get({id : id}, function(result) {
                vm.simulationParam = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:simulationParamUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.simulationParam.id !== null) {
                SimulationParam.update(vm.simulationParam, onSaveSuccess, onSaveError);
            } else {
                SimulationParam.save(vm.simulationParam, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
