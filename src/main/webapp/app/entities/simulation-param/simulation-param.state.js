/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('simulation-param', {
            parent: 'entity',
            url: '/simulation-param?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.simulationParam.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/simulation-param/simulation-params.html',
                    controller: 'SimulationParamController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('simulationParam');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('simulation-param-detail', {
            parent: 'entity',
            url: '/simulation-param/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.simulationParam.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/simulation-param/simulation-param-detail.html',
                    controller: 'SimulationParamDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('simulationParam');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SimulationParam', function($stateParams, SimulationParam) {
                    return SimulationParam.get({id : $stateParams.id});
                }]
            }
        })
        .state('simulation-param.new', {
            parent: 'simulation-param',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation-param/simulation-param-dialog.html',
                    controller: 'SimulationParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                value: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('simulation-param', null, { reload: true });
                }, function() {
                    $state.go('simulation-param');
                });
            }]
        })
        .state('simulation-param.edit', {
            parent: 'simulation-param',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation-param/simulation-param-dialog.html',
                    controller: 'SimulationParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SimulationParam', function(SimulationParam) {
                            return SimulationParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('simulation-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('simulation-param.delete', {
            parent: 'simulation-param',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation-param/simulation-param-delete-dialog.html',
                    controller: 'SimulationParamDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SimulationParam', function(SimulationParam) {
                            return SimulationParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('simulation-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
