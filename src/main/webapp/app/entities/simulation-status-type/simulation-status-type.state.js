/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('simulation-status-type', {
            parent: 'entity',
            url: '/simulation-status-type?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.simulationStatusType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/simulation-status-type/simulation-status-types.html',
                    controller: 'SimulationStatusTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('simulationStatusType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('simulation-status-type-detail', {
            parent: 'entity',
            url: '/simulation-status-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.simulationStatusType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/simulation-status-type/simulation-status-type-detail.html',
                    controller: 'SimulationStatusTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('simulationStatusType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SimulationStatusType', function($stateParams, SimulationStatusType) {
                    return SimulationStatusType.get({id : $stateParams.id});
                }]
            }
        })
        .state('simulation-status-type.new', {
            parent: 'simulation-status-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation-status-type/simulation-status-type-dialog.html',
                    controller: 'SimulationStatusTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('simulation-status-type', null, { reload: true });
                }, function() {
                    $state.go('simulation-status-type');
                });
            }]
        })
        .state('simulation-status-type.edit', {
            parent: 'simulation-status-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation-status-type/simulation-status-type-dialog.html',
                    controller: 'SimulationStatusTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SimulationStatusType', function(SimulationStatusType) {
                            return SimulationStatusType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('simulation-status-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('simulation-status-type.delete', {
            parent: 'simulation-status-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation-status-type/simulation-status-type-delete-dialog.html',
                    controller: 'SimulationStatusTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SimulationStatusType', function(SimulationStatusType) {
                            return SimulationStatusType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('simulation-status-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
