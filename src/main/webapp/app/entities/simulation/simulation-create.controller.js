/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SimulationCreateController', SimulationCreateController);

    SimulationCreateController.$inject = ['$scope', '$state', '$rootScope', '$stateParams', '$http', '$filter', 'ParseLinks',  'AlertService', 'entity', 'Simulation', 'Dataset', 'DatasetSearch', 'PopulationSize', 'Population', 'PopulationSearch', 'SshServer', 'SshServerSearch', 'Application', 'ProblemType', 'ErrorType', 'AnalyzeType', 'ArchType', 'pagingParams', 'paginationConstants'];

    function SimulationCreateController($scope, $state, $rootScope, $stateParams, $http, $filter, ParseLinks, AlertService, entity, Simulation, Dataset, DatasetSearch, PopulationSize, Population, PopulationSearch, SshServer, SshServerSearch, Application, ProblemType, ErrorType, AnalyzeType, ArchType, pagingParams, paginationConstants) {
        var vm = this;
        vm.simulation = entity;

        vm.SIMULATION_STATUS = {
            DETAIL: 1,
            DATASET: 2,
            PROBLEM: 3,
            POPULATION: 4,
            GA: 5,
            SERVER: 6,
            EXECUTION: 7
        };

        vm.simulationStatus = vm.SIMULATION_STATUS.DETAIL;

        // To show execution
        vm.simulation.execution = {};
        vm.simulation.execution.time = {};
        vm.simulation.execution.time.minute = 0;
        vm.simulation.execution.time.hour   = 0;
        vm.simulation.execution.time.day    = 0;

        vm.archtypes = [];

        vm.getArchitecture = function(idSshServer) {
            ArchType.getActiveByBelong({id: idSshServer}, function(result) {
                vm.archtypes = result;
                //console.log(result);
            });
        };

        // To show analyzeTypes
        vm.analyzetypes = AnalyzeType.query();

        vm.getAnalyzeName = function(analyzeId) {
            var result = vm.analyzetypes.filter(function(obj){
                return obj.id == analyzeId;
            });

            return result[0].name;
        };

        // To show errorTypes
        vm.errortypes = ErrorType.query();
        vm.problemtype;

        vm.setProblemType  = function (idProblemType) {
            ProblemType.get({id : idProblemType}, function(result) {
                vm.problemtype = result;
                vm.numPopulation = result.numPopulation;
            });
        };

        // To show probemType
        vm.problemtypes = ProblemType.query();

        // To ssh server
        // To show paginate sshServer
        vm.getSshServer = function(id) {
            console.log("getSshServer("+id+") called");
            vm.sshServer = SshServer.get({id : id});
        };


        vm.sshServerLoadAll = sshServerLoadAll;
        vm.sshServerLoadPage = sshServerLoadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.sshServerTransition = sshServerTransition;
        vm.sshServerClear = sshServerClear;
        vm.sshServerSearch = sshServerSearch;

        vm.sshServerSearchQuery = pagingParams.search;
        vm.sshServerCurrentSearch = pagingParams.search;

        //vm.sshServerLoadAll();

        function sshServerLoadAll() {
            if (vm.simulationStatus != vm.SIMULATION_STATUS.SERVER) {
                return;
            }

            if (vm.sshServerCurrentSearch) {
                vm.sshServers = SshServerSearch.query({
                    query: vm.sshServerCurrentSearch,
                    page: vm.sshServerPage - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                vm.sshServerPage = (vm.sshServerPage === undefined) ? 1 : vm.sshServerPage;

                vm.sshServers = SshServer.queryActivated({
                    page: vm.sshServerPage - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalSshServerItems = headers('X-Total-Count');
                vm.querySshServerCount = vm.totalSshServerItems;
                vm.simulations = data;
                vm.sshServerPage = vm.sshServerPage;

                if (vm.totalSshServerItems == 0) {
                    vm.addAlert("energliaApp.sshServer.unavailable", "warning");
                }
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function sshServerLoadPage (page) {
            vm.sshServerPage = page;
            vm.sshServerTransition();
        }

        function sshServerTransition () {
            vm.sshServerLoadAll();
        }

        function sshServerSearch (searchQuery) {
            if (!searchQuery){
                return vm.sshServerClear();
            }
            vm.links = null;
            vm.sshServerPage = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.sshServerCurrentSearch = searchQuery;
            vm.sshServerTransition();
        }

        function sshServerClear () {
            vm.links = null;
            vm.sshServerPage = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.sshServerCurrentSearch = null;
            vm.sshServerTransition();
        }

        // To population
        vm.currentPopulation = 0;
        vm.sizePopulation = [];

        vm.selectPopulation = [];
        vm.population = [];

        // To show paginate population
        vm.getPopulation = function(id) {
            console.log("getPopulation("+id+") called");
            vm.population[vm.currentPopulation] = Population.get({id : id});
        };

        //vm.populations = PopulationSize.query();


        vm.populationLoadAll = populationLoadAll;
        vm.populationLoadPage = populationLoadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.populationTransition = populationTransition;
        vm.populationClear = populationClear;
        vm.populationSearch = populationSearch;

        vm.populationSearchQuery = pagingParams.search;
        vm.populationCurrentSearch = pagingParams.search;

        vm.populationLoadAll();

        function populationLoadAll() {
            if (vm.sizePopulation.length == 0) {
                return;
            }

            if (vm.populationCurrentSearch) {
                vm.populations = PopulationSearch.query({
                    query: vm.populationCurrentSearch,
                    page: vm.populationPage - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                vm.populationPage = (vm.populationPage === undefined) ? 1 : vm.populationPage;

                var info = {
                    sizeShort: vm.sizePopulation[vm.currentPopulation].sizeShort,
                    sizeFloat: vm.sizePopulation[vm.currentPopulation].sizeFloat,
                    sizeBool : vm.sizePopulation[vm.currentPopulation].sizeBool,

                    page: vm.populationPage - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                };

                vm.populations = PopulationSize.list(info,  onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalPopulationItems = headers('X-Total-Count');
                vm.queryPopulationCount = vm.totalPopulationItems;
                vm.simulations = data;
                vm.populationPage = vm.populationPage;

                if (vm.totalPopulationItems == 0) {
                    vm.addAlert("energliaApp.population.unavailable", "warning");
                    vm.selectPopulation[vm.currentPopulation] = undefined;
                }
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function populationLoadPage (page) {
            vm.populationPage = page;
            vm.populationTransition();
        }

        function populationTransition () {
            vm.populationLoadAll();
        }

        function populationSearch (searchQuery) {
            if (!searchQuery){
                return vm.populationClear();
            }
            vm.links = null;
            vm.populationPage = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.populationCurrentSearch = searchQuery;
            vm.populationTransition();
        }

        function populationClear () {
            vm.links = null;
            vm.populationPage = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.populationCurrentSearch = null;
            vm.populationTransition();
        }

        // To nuSVM
        vm.nuSvmParams = {};
        vm.nuSvmParams.constraints = {};
        vm.nuSvmParams.constraints.mu = {};
        vm.nuSvmParams.constraints.tolerance = {};
        vm.nuSvmParams.constraints.degree = {};
        vm.nuSvmParams.constraints.gamma = {};
        vm.nuSvmParams.constraints.coef = {};

        // To angnSearchGlia
        vm.angnSearchGlia = {};
        vm.angnSearchGlia.layer = 2;
        vm.angnSearchGlia.layerParams = [];
        vm.angnSearchGlia.constraints = {};

        vm.angnSearchGlia.constraints.sameGliaAlgorithm = true;
        vm.angnSearchGlia.constraints.decreateGreaterThanIncrease = true;

        vm.angnSearchGlia.constraints.weight = {};
        vm.angnSearchGlia.constraints.iteration = {};
        vm.angnSearchGlia.constraints.activation = {};
        vm.angnSearchGlia.constraints.increase = {};
        vm.angnSearchGlia.constraints.decrease = {};


        vm.angnSearchGliaSetLayerParamsLength = function(size) {
            if (size > vm.angnSearchGlia.layerParams.length) {
                while (size > vm.angnSearchGlia.layerParams.length) {
                    vm.angnSearchGlia.layerParams.push({neuron: 1});
                }
            } else if (size < vm.angnSearchGlia.layerParams.length) {
                while (size < vm.angnSearchGlia.layerParams.length) {
                    vm.angnSearchGlia.layerParams.pop();
                }
            }

            vm.angnSearchGlia.layerParams[0].neuron = vm.dataset.inputFeature;
            vm.angnSearchGlia.layerParams[size-1].neuron = vm.dataset.outputFeature;
        };

        vm.angnSearchGliaFillValuesFrom0 = function() {
            vm.angnSearchGliaSetLayerParamsLength(vm.angnSearchGlia.layer);
console.log(vm.angnSearchGlia.layerParams);
            for (var cnt = 1; cnt < vm.angnSearchGlia.layerParams.length; cnt++) {
                var tmp = vm.angnSearchGlia.layerParams[cnt].neuron;
                vm.angnSearchGlia.layerParams[cnt] = angular.copy(vm.angnSearchGlia.layerParams[0]);
                vm.angnSearchGlia.layerParams[cnt].neuron = tmp;
            }

            vm.angnSearchGlia.layerParams[(vm.angnSearchGlia.layerParams.length-1)].neuron = vm.dataset.outputFeature;
            //vm.angnSearchGlia.layerParams[0].neuron = vm.dataset.inputFeature;
        };

        // To annfixedglia
        vm.annFixedGlia = {};
        vm.annFixedGlia.layer = 2;
        vm.annFixedGlia.layerParams = [];
        vm.annFixedGlia.constraints = {};
        vm.annFixedGlia.constraints.weight = {};
        vm.annFixedGlia.constraints.weight.min = null;
        vm.annFixedGlia.constraints.weight.max = null;


        vm.annFixedGliaSetLayerParamsLength = function(size) {
            if (size > vm.annFixedGlia.layerParams.length) {
                while (size > vm.annFixedGlia.layerParams.length) {
                    vm.annFixedGlia.layerParams.push({neuron: 1});
                }
            } else if (size < vm.annFixedGlia.layerParams.length) {
                while (size < vm.annFixedGlia.layerParams.length) {
                   vm.annFixedGlia.layerParams.pop();
                }
            }

            vm.annFixedGlia.layerParams[0].neuron = vm.dataset.inputFeature;
            vm.annFixedGlia.layerParams[size-1].neuron = vm.dataset.outputFeature;
        };

        vm.annFixedGliaFillValuesFrom0 = function() {
            vm.annFixedGliaSetLayerParamsLength(vm.annFixedGlia.layer);
console.log(vm.annFixedGlia.layerParams);
            for (var cnt = 1; cnt < vm.annFixedGlia.layerParams.length; cnt++) {
                var tmp = vm.annFixedGlia.layerParams[cnt].neuron;
                vm.annFixedGlia.layerParams[cnt] = angular.copy(vm.annFixedGlia.layerParams[0]);
                vm.annFixedGlia.layerParams[cnt].neuron = tmp;
            }

            vm.annFixedGlia.layerParams[(vm.annFixedGlia.layerParams.length-1)].neuron = vm.dataset.outputFeature;
            //vm.annFixedGlia.layerParams[0].neuron = vm.dataset.inputFeature;
        };

        // To show paginate dataset
        vm.getDataset = function(id) {
console.log("getDataset("+id+") called");
            vm.dataset = Dataset.get({id : id});
        };


        vm.datasetLoadAll = datasetLoadAll;
        vm.datasetLoadPage = datasetLoadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.datasetTransition = datasetTransition;
        vm.datasetClear = datasetClear;
        vm.datasetSearch = datasetSearch;

        vm.datasetSearchQuery = pagingParams.search;
        vm.datasetCurrentSearch = pagingParams.search;

        //vm.datasetLoadAll();

        function datasetLoadAll() {
            if (vm.simulationStatus != vm.SIMULATION_STATUS.DATASET) {
                return;
            }

            if (vm.datasetCurrentSearch) {
                vm.datasets = DatasetSearch.query({
                    query: vm.datasetCurrentSearch,
                    page: vm.datasetPage - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                vm.datasetPage = (vm.datasetPage === undefined) ? 1 : vm.datasetPage;
                vm.datasets = Dataset.query({
                    page: vm.datasetPage - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalDatasetItems = headers('X-Total-Count');
                vm.queryDatasetCount = vm.totalDatasetItems;
                vm.simulations = data;
                vm.datasetPage = vm.datasetPage;

                if (vm.totalDatasetItems == 0) {
                    vm.addAlert("energliaApp.dataset.unavailable", "warning");
                }
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function datasetLoadPage (page) {
            vm.datasetPage = page;
            vm.datasetTransition();
        }

        function datasetTransition () {
            vm.datasetLoadAll();
        }

        function datasetSearch (searchQuery) {
            if (!searchQuery){
                return vm.datasetClear();
            }
            vm.links = null;
            vm.datasetPage = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.datasetCurrentSearch = searchQuery;
            vm.datasetTransition();
        }

        function datasetClear () {
            vm.links = null;
            vm.datasetPage = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.datasetCurrentSearch = null;
            vm.datasetTransition();
        }

        ///
        /// Next buttons (save)
        ///

        vm.saveDetail = function() {
            vm.simulationStatus = vm.SIMULATION_STATUS.DATASET;

            if (vm.datasets === undefined || vm.datasets == null) {
                vm.datasetLoadAll();
            }
        };

        vm.saveDataset = function() {
            vm.simulationStatus = vm.SIMULATION_STATUS.PROBLEM;

            vm.annFixedGliaFillValuesFrom0();
            vm.angnSearchGliaFillValuesFrom0();
        };

        vm.saveProblem = function() {
            vm.sizePopulation = [];

            var size_genofloat = 0;
            var size_genoshort = 0;
            var size_genobool  = 0;
            var cnt = 0;

            switch (vm.simulation.problemId) {
                case 1:// 1 -> ann fixed glia
                    for (cnt = 1; cnt < vm.annFixedGlia.layer; cnt++) {
                        size_genofloat += vm.annFixedGlia.layerParams[cnt].neuron * vm.annFixedGlia.layerParams[cnt-1].neuron;
                    }

                    vm.sizePopulation.push({
                        sizeFloat: size_genofloat,
                        sizeShort: 0,
                        sizeBool : 0
                    });


                    break;
                case 2:// 2 -> angn search glia
                    for (cnt = 1; cnt < vm.angnSearchGlia.layer; cnt++) {
                        size_genofloat += vm.angnSearchGlia.layerParams[cnt].neuron * vm.angnSearchGlia.layerParams[cnt-1].neuron;
                    }

                    vm.sizePopulation.push({
                        sizeFloat: size_genofloat,
                        sizeShort: 0,
                        sizeBool : 0
                    });

                    size_genoshort = vm.angnSearchGlia.constraints.sameGliaAlgorithm? 5: 2+3*vm.angnSearchGlia.layer;

                    vm.sizePopulation.push({
                        sizeFloat: 0,
                        sizeShort: size_genoshort,
                        sizeBool : 0
                    });


                    break;
                case 3:// 3 -> nu svm
                    size_genofloat = 7;
                    size_genoshort = 3;

                    vm.sizePopulation.push({
                        sizeFloat: size_genofloat,
                        sizeShort: size_genoshort,
                        sizeBool : 0
                    });


                    break;

            }


console.log("population size");
console.log(vm.sizePopulation);

            if (vm.sizePopulation.length == 0) {
                vm.simulationStatus = vm.SIMULATION_STATUS.SERVER;
            } else {
                vm.simulationStatus = vm.SIMULATION_STATUS.POPULATION;
                // Always load population because sizePopulation can be changed
                //if (vm.populations === undefined || vm.populations == null)
                    populationLoadAll();


            }

        };

        vm.savePopulation = function() {
            if (vm.currentPopulation + 1 < vm.sizePopulation.length) {
                vm.currentPopulation++;
                populationLoadAll();

                var tmp = vm.sizePopulation[vm.currentPopulation].sizeBool +
                    vm.sizePopulation[vm.currentPopulation].sizeFloat +
                    vm.sizePopulation[vm.currentPopulation].sizeShort;
                if (vm.population[vm.currentPopulation].sizeGenotype != tmp)
                    vm.population[vm.currentPopulation] = undefined;

                return;
            }

            vm.simulationStatus = vm.SIMULATION_STATUS.GA;

        };

        vm.saveGa = function() {
            console.log('save GA params');

            vm.simulationStatus = vm.SIMULATION_STATUS.SERVER;
            if (vm.sshServers === undefined || vm.sshServers == null) {
                sshServerLoadAll();
            }
        };

        vm.saveSshServer = function() {
            console.log('save sshServer params');

            vm.simulationStatus = vm.SIMULATION_STATUS.EXECUTION;

            if (vm.simulation.execution.time.minute > vm.sshServer.maxTime) {
                vm.simulation.execution.time.minute = 0;
            }
            if (vm.simulation.execution.time.hour > vm.sshServer.maxTime/60 ) {
                vm.simulation.execution.time.hour = 0;
            }
            if (vm.simulation.execution.time.day > vm.sshServer.maxTime/(60*24) ) {
                vm.simulation.execution.time.day = 0;
            }

            if (vm.simulation.execution.proc === undefined)
                vm.simulation.execution.proc = 1;
            vm.getArchitecture(vm.sshServer.id);

            if (vm.simulation.execution.memory === undefined
            || vm.simulation.execution.memory*1024*1024 > vm.sshServer.maxMemory)
                vm.simulation.execution.memory = vm.sshServer.maxMemory / (2*1024*1024);

        };

        vm.saveExecution = function() {
            var detail = {
                name: vm.simulation.name,
                description: (vm.simulation.description === undefined || vm.simulation.description == null)? "": vm.simulation.description,
                datasetId: vm.selectDataset,
                problemId: vm.simulation.problemId,
                errorId: vm.simulation.minimizeId,
                serverId: vm.selectSshServer
            };

            var exec = {
                arch: vm.simulation.execution.selectedArch,
                proc: vm.simulation.execution.proc,
                memory: vm.simulation.execution.memory*1024*1024,
                time: vm.simulation.execution.time.total
            };

            var gaBasic = {
                generation: vm.gaParams.generation,
                goal: vm.gaParams.goal
            };

            var info = {
                detail: detail,
                execution: exec,
                gaBasic: gaBasic
            };

            var mother  = {};
            var father  = {};
            var cross   = {};
            var muta    = {};
            var replace = {};

            var populations = [];

            // Swith for ga parameters
            switch (vm.simulation.problemId) {
                case 1:// 1 -> ann fixed glia
                case 3:// 3 -> nu svm

                    mother = {
                        algorithm: vm.gaParams.mother[0].algorithm,
                        window: vm.gaParams.mother[0].window,
                        probability: vm.gaParams.mother[0].prob
                    };

                    father = {
                        algorithm: vm.gaParams.father[0].algorithm,
                        window: vm.gaParams.father[0].window,
                        probability: vm.gaParams.father[0].prob
                    };

                    cross = {
                        algorithm: vm.gaParams.crossover[0].algorithm,
                        rate: vm.gaParams.crossover[0].rate,
                        mu: vm.gaParams.crossover[0].mu
                    };

                    muta = {
                        algorithm: vm.gaParams.mutation[0].algorithm,
                        rate: vm.gaParams.mutation[0].rate
                    };

                    replace = {algorithm: vm.gaParams.replace[0]};

                    populations[0] = {
                        indexOrder: 0,
                        populationId: vm.selectPopulation[0],
                        mother: mother,
                        father: father,
                        mutation: muta,
                        crossover: cross,
                        replace: replace
                    };

                    info.populations = populations;

                    break;
                case 2:// 2 -> angn search glia
                    var ga = {
                        algorithm: vm.gaParams.fitness.algorithm,
                        best: vm.gaParams.fitness.best,
                        random: vm.gaParams.fitness.random,
                        function: vm.gaParams.fitness.function,
                        select: {
                            algorithm: vm.gaParams.fitness.select.algorithm,
                            window: vm.gaParams.fitness.select.window,
                            probability: vm.gaParams.fitness.select.prob
                        }
                    };

                    for (var cnt = 0; cnt < 2; cnt++) {
                        mother = {
                            algorithm: vm.gaParams.mother[cnt].algorithm,
                            window: vm.gaParams.mother[cnt].window,
                            probability: vm.gaParams.mother[cnt].prob
                        };

                        father = {
                            algorithm: vm.gaParams.father[cnt].algorithm,
                            window: vm.gaParams.father[cnt].window,
                            probability: vm.gaParams.father[cnt].prob
                        };

                        cross = {
                            algorithm: vm.gaParams.crossover[cnt].algorithm,
                            rate: vm.gaParams.crossover[cnt].rate,
                            mu: vm.gaParams.crossover[cnt].mu
                        };

                        muta = {
                            algorithm: vm.gaParams.mutation[cnt].algorithm,
                            rate: vm.gaParams.mutation[cnt].rate
                        };

                        replace = {algorithm: vm.gaParams.replace[cnt]};

                        populations[cnt] = {
                            indexOrder: cnt,
                            populationId: vm.selectPopulation[cnt],
                            mother: mother,
                            father: father,
                            mutation: muta,
                            crossover: cross,
                            replace: replace
                        };
                    }

                    info.ga = ga;
                    info.populations = populations;

                    break;
            }

            switch (vm.simulation.problemId) {
                case 1:// 1 -> ann fixed glia
                    info.layer = vm.annFixedGlia.layer;
                    info.iteration = vm.annFixedGlia.iteration;
                    info.activation = vm.annFixedGlia.activation;

                    info.neuron = [];
                    info.activationFunction = [];
                    info.gliaAlgorithm = [];
                    info.increase = [];
                    info.decrease = [];

                    for (var cnt = 0; cnt < info.layer; cnt++) {
                        info.neuron.push(vm.annFixedGlia.layerParams[cnt].neuron);
                        info.activationFunction.push(vm.annFixedGlia.layerParams[cnt].activation_function);
                        info.gliaAlgorithm.push(vm.annFixedGlia.layerParams[cnt].glia);
                        info.increase.push(vm.annFixedGlia.layerParams[cnt].increase);
                        info.decrease.push(vm.annFixedGlia.layerParams[cnt].decrease);
                    }

                    info.coWeight = [vm.annFixedGlia.constraints.weight.min, vm.annFixedGlia.constraints.weight.max];
                    break;
                case 2:// 2 -> angn search glia

                    info.layer = vm.angnSearchGlia.layer;
                    info.neuron = [];
                    info.activationFunction = [];

                    for (var cnt = 0; cnt < info.layer; cnt++) {
                        info.neuron.push(vm.angnSearchGlia.layerParams[cnt].neuron);
                        info.activationFunction.push(vm.angnSearchGlia.layerParams[cnt].activation_function);
                    }

                    info.sameGliaAlgorithm = vm.angnSearchGlia.constraints.sameGliaAlgorithm;
                    info.decreaseGreatherThanIncrease = vm.angnSearchGlia.constraints.decreateGreaterThanIncrease;
                    info.coWeight = [vm.angnSearchGliaParams.constraints.weight.min, vm.angnSearchGliaParams.constraints.weight.max];
                    info.coIteration = [vm.angnSearchGliaParams.constraints.iteration.min, vm.angnSearchGliaParams.constraints.iteration.max];
                    info.coActivation = [vm.angnSearchGliaParams.constraints.activation.min, vm.angnSearchGliaParams.constraints.activation.max];
                    info.coIncrease = [vm.angnSearchGliaParams.constraints.increase.min, vm.angnSearchGliaParams.constraints.increase.max];
                    info.coDecrease = [vm.angnSearchGliaParams.constraints.decrease.min, vm.angnSearchGliaParams.constraints.decrease.max];

                    break;
                case 3:// 3 -> nu svm
                    info.populationId = vm.selectPopulation[0];
                    info.coNu = [vm.nuSvmParams.constraints.mu.min, vm.nuSvmParams.constraints.mu.max];
                    info.coTolerance = [vm.nuSvmParams.constraints.tolerance.min, vm.nuSvmParams.constraints.tolerance.max];
                    info.coDegree = [vm.nuSvmParams.constraints.degree.min, vm.nuSvmParams.constraints.degree.max];
                    info.coGamma = [vm.nuSvmParams.constraints.gamma.min, vm.nuSvmParams.constraints.gamma.max];
                    info.coCoef = [vm.nuSvmParams.constraints.coef.min, vm.nuSvmParams.constraints.coef.max];
                    break;
            }

            vm.isSaving = true;
            switch (vm.simulation.problemId) {
                case 1:// 1 -> ann fixed glia
                    Simulation.saveAnnFixedGlia(info, onSaveSuccess, onSaveError);
                    break;
                case 2:// 2 -> angn search glia
                    Simulation.saveAngnSearchGlia(info, onSaveSuccess, onSaveError);
                    break;
                case 3: // 3-> nu svm
                    Simulation.saveNuSvm(info, onSaveSuccess, onSaveError);
                    break;
            }

            console.log(info);

        };

        var onSaveSuccess = function (result) {
            vm.isSaving = false;
            $state.go('simulation');
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        /// Previous buttons (clear)
        vm.datasetClear = function() {
            console.log("previous dataset");
            vm.simulationStatus = vm.SIMULATION_STATUS.DETAIL;
        };

        vm.problemClear = function() {
            console.log("previous button");
            vm.simulationStatus = vm.SIMULATION_STATUS.DATASET;
        };

        vm.populationClear = function() {
            console.log("previous population");

            if (vm.currentPopulation > 0) {
                vm.currentPopulation--;
                populationLoadAll();
                return;
            }

            vm.simulationStatus = vm.SIMULATION_STATUS.PROBLEM;
        };

        vm.gaClear = function() {
            console.log("previous ga param");

            vm.simulationStatus = vm.SIMULATION_STATUS.POPULATION;
        };

        vm.sshServerClear = function() {
            console.log("previous ssh server");

            if (vm.sizePopulation.length == 0)
                vm.simulationStatus = vm.SIMULATION_STATUS.PROBLEM;
            else
                vm.simulationStatus = vm.SIMULATION_STATUS.GA;
        };

        vm.executionClear = function() {
            console.log("previous execution");

            vm.simulationStatus = vm.SIMULATION_STATUS.SERVER;
        };

        ///
        /// Code for alerts
        ///
        vm.timeout=10000;
        vm.alerts = [];

        vm.addAlert = function(data, type) {
            if (type === undefined || type == null)
                type = "info";

            vm.alerts.push({msg: $filter('translate')(data), type: type});
        };

        vm.closeAlert = function(index) {
            vm.alerts.splice(index, 1);
        };

        ///
        /// End code for alerts
        ///


        vm.load = function (id) {
            Simulation.get({id: id}, function(result) {
                vm.simulation = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:simulationUpdate', function(event, result) {
            vm.simulation = result;
        });
        $scope.$on('$destroy', unsubscribe);


        ///
        /// Simulation constants
        ///

        var EnumItem = function(params) {
            this.id   = params.id;
            this.name = params.name;
        };

        vm.glia_algorithm = [
            new EnumItem({id: 0, name: 'ATTENUATED_EFFECT_ASTROCYTE'}),
            new EnumItem({id: 1, name: 'NON_CONSECUTIVE_UNLIMITED'}),
            new EnumItem({id: 2, name: 'GLOBAL_PROCESSING_EFFECT'}),
            new EnumItem({id: 3, name: 'CONSECUTIVE_LIMITED'}),
            new EnumItem({id: 4, name: 'NON_CONSECUTIVE_LIMITED'}),
            new EnumItem({id: 5, name: 'CONSECUTIVE_UNLIMITED'}),
            new EnumItem({id: 6, name: 'DEPRESSION'}),
            new EnumItem({id: 7, name: 'CANNABINOIDE'})
        ];

        vm.activation_function = [
            new EnumItem({id: 0, name: 'IDENTITY'}),
            new EnumItem({id: 1, name: 'THRESHOLD'}),
            new EnumItem({id: 2, name: 'HTANGENT'}),
            new EnumItem({id: 3, name: 'SIGMOID'})
        ];

        vm.select_function = [
            new EnumItem({id: 1, name: 'RANDOM'}),
            new EnumItem({id: 2, name: 'ROULETTE'}),
            new EnumItem({id: 3, name: 'DETERMINISTIC_TOURNAMENT'}),
            new EnumItem({id: 4, name: 'PROBABILISTIC_TOURNAMENT'})
        ];

        vm.crossover_function = [
            new EnumItem({id: 10, name: 'REAL_SPX'}),
            new EnumItem({id: 11, name: 'REAL_DPX'}),
            new EnumItem({id: 12, name: 'REAL_UPX'}),
            new EnumItem({id: 13, name: 'REAL_SBX'})
        ];

        vm.mutation_function = [
            new EnumItem({id: 10, name: 'CHANGE_VALUE'}),
            new EnumItem({id: 11, name: 'CHANGE_PERCENT'}),
        ];

        vm.replace_function = [
            new EnumItem({id: 1, name: 'ALWAYS_REPLACE'}),
            new EnumItem({id: 2, name: 'WORST_REPLACING'}),
            new EnumItem({id: 3, name: 'SIMILAR_REPLACE'})
        ];

        vm.fitness_function = [
            new EnumItem({id: 1, name:'FITNESS_MIN'}),
            new EnumItem({id: 2, name:'FITNESS_AVG'}),
            new EnumItem({id: 3, name:'FITNESS_MAX'})
        ];

    }
})();
