/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SimulationDetailController', SimulationDetailController);

    SimulationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', '$http', '$filter', 'entity', 'Simulation', 'User', 'Dataset', 'Application', 'ProblemType', 'ErrorType'];

    function SimulationDetailController($scope, $rootScope, $stateParams, $http, $filter, entity, Simulation, User, Dataset, Application, ProblemType, ErrorType) {
        var vm = this;
        vm.simulation = entity;

        vm.edit = false;
        vm.editDescription = vm.simulation.description;

        vm.timeout=5000;
        vm.alerts = [];
        

        vm.addAlert = function() {
            console.log("add Alert");

            vm.alerts.push({msg: $filter('translate')('entity.update.description')});
        };

        vm.closeAlert = function(index) {
            vm.alerts.splice(index, 1);
        };

        vm.update = function () {
            var info = {
                id : vm.simulation.id,
                description : vm.editDescription
            };

            console.log(info);

            $http.put('api/custom-simulations', info).
            success(function(data, status, headers, config) {
                console.log(data);
                console.log(status);
                //console.log(headers);
                //console.log(config);
                vm.simulation.description = data.description;
                vm.addAlert();
                vm.edit = false;
            }).error(function (data, status, headers, config) {
                console.log("error");
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
            });
        };

        vm.changeEdit = function () {
            console.log("change edit function");
            vm.edit = !vm.edit;
        };




        vm.load = function (id) {
            Simulation.get({id: id}, function(result) {
                vm.simulation = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:simulationUpdate', function(event, result) {
            vm.simulation = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
