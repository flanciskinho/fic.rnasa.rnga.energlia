/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SimulationReportController', SimulationReportController);

    SimulationReportController.$inject = ['$scope', '$location', '$rootScope', '$stateParams', '$http', '$filter', 'entity', 'Simulation'];

    function SimulationReportController($scope, $location, $rootScope, $stateParams, $http, $filter, entity, Simulation) {
        var vm = this;
        vm.simulation = entity;

        vm.logData = {dataset: []};
        vm.loadData = loadData;

        var COLOR_TYPE = {
            TRAIN_ERROR: "#0080FF",
            VALIDATION_ERROR: "#800080",
            TEST_ERROR: "#FF0C1D",

            DES_TRAIN_ERROR: "#30D2FF",
            DES_VALIDATION_ERROR: "#D43ACE",
            DES_TEST_ERROR: "#FF6666"
        };

        var tension = 0.99;

        var tmpOptionValidation = {
            axis: "y",
            dataset: "dataset",
            key: "errorValidation",
            label: $filter('translate')('energliaApp.simulation.report.serie.validation'),
            interpolation: {mode: "bundle", tension: tension},
            color: COLOR_TYPE.VALIDATION_ERROR,
            type: ['line'],
            id: 'myValidation1'
        };
        var tmpOptionTest = {
            axis: "y",
            dataset: "dataset",
            key: "errorTest",
            label: $filter('translate')('energliaApp.simulation.report.serie.test'),
            interpolation: {mode: "bundle", tension: tension},
            color: COLOR_TYPE.TEST_ERROR,
            type: ['line'],
            id: 'myTest1'
        };

        vm.infoSpecific = [];
        vm.infoGeneral = [];

        vm.optionGeneral = {
            series: [
                {
                    axis: "y",
                    dataset: "dataset",
                    key: "errorTrain",
                    label: $filter('translate')('energliaApp.simulation.report.serie.train'),
                    interpolation: {mode: "bundle", tension: tension},
                    color: COLOR_TYPE.TRAIN_ERROR,
                    type: ['line'],
                    id: 'myTrain1'
                }
            ],
            axes: {
                x: {key: "indexRecord"}
            }
        };

        vm.optionTrain = {
            series: [
                {
                    axis: "y",
                    dataset: "dataset",
                    key: "errorTrain",
                    label: $filter('translate')('energliaApp.simulation.report.serie.train'),
                    interpolation: {mode: "bundle", tension: tension},
                    color: COLOR_TYPE.TRAIN_ERROR,
                    type: ['line'],
                    id: 'myTrain0'
                },
                {
                    axis: "y",
                    dataset: "dataset",
                    key: {y0: "desTrainPlus", y1: "desTrainSub"},
                    label: $filter('translate')('energliaApp.simulation.report.serie.desTrain'),
                    interpolation: {mode: "bundle", tension: tension},
                    color: COLOR_TYPE.DES_TRAIN_ERROR,//"hsla(184, 100%, 50%, 0.75)",
                    type: ['area'],
                    id: 'myTrainDes0'}
            ],
            axes: {
                x: {key: "indexRecord"}
            }
        };

        vm.optionValidation = {
            series: [
                {
                    axis: "y",
                    dataset: "dataset",
                    key: "errorValidation",
                    label: $filter('translate')('energliaApp.simulation.report.serie.validation'),
                    interpolation: {mode: "bundle", tension: tension},
                    color: COLOR_TYPE.VALIDATION_ERROR,
                    type: ['line'],
                    id: 'myValidation0'
                },
                {
                    axis: "y",
                    dataset: "dataset",
                    key: {y0: "desValidationPlus", y1: "desValidationSub"},
                    label: $filter('translate')('energliaApp.simulation.report.serie.desValidation'),
                    interpolation: {mode: "bundle", tension: tension},
                    color: COLOR_TYPE.DES_VALIDATION_ERROR,
                    type: ['area'],
                    id: 'myValidationDes0'
                }
            ],
            axes: {
                x: {key: "indexRecord"}
            }
        };

        vm.optionTest = {
            series: [
                {
                    axis: "y",
                    dataset: "dataset",
                    key: "errorTest",
                    label: $filter('translate')('energliaApp.simulation.report.serie.test'),
                    interpolation: {mode: "bundle", tension: tension},
                    color: COLOR_TYPE.TEST_ERROR,
                    type: ['line'],
                    id: 'myTest0'
                },
                {
                    axis: "y",
                    dataset: "dataset",
                    key: {y0: "desTestPlus", y1: "desTestSub"},
                    label: $filter('translate')('energliaApp.simulation.report.serie.desTest'),
                    interpolation: {mode: "bundle", tension: tension},
                    color: COLOR_TYPE.DES_TEST_ERROR,
                    type: ['area'],
                    id: 'myTestDes0'}
            ],
            axes: {
                x: {key: "indexRecord"}
            }
        };


        vm.loadData();

        function loadData() {
            $http.get('api/get-log-average/'+$stateParams.id).success(function(data, status) {
                vm.logData.dataset = data;

                var size = -1;

                if (!(vm.logData.dataset[0].errorTrain === undefined) && vm.logData.dataset[0].errorTrain != null) {
                    vm.infoSpecific.push({class: '', title: 'train', option: vm.optionTrain});
                    size++;
                }

                if (!(vm.logData.dataset[0].errorValidation === undefined) && vm.logData.dataset[0].errorValidation != null) {
                    vm.optionGeneral.series.push(tmpOptionValidation);
                    vm.infoSpecific.push({class: '', title: 'validation', option: vm.optionValidation});
                    size++;
                }

                if (!(vm.logData.dataset[0].errorTest === undefined) && vm.logData.dataset[0].errorTest != null) {
                    vm.optionGeneral.series.push(tmpOptionTest);
                    vm.infoSpecific.push({class: '', title: 'test', option: vm.optionTest});
                    size++;
                }

                if (size > 0)
                    vm.infoGeneral.push({option: vm.optionGeneral});

                var tmpClass = '';
                switch (size) {
                    case 0:
                        tmpClass = 'col-md-12';
                        break;
                    case 1:
                        tmpClass = 'col-md-6';
                        break;
                    case 2:
                        tmpClass = 'col-md-4';
                        break;

                }

                for (var cnt = 0; cnt < vm.infoSpecific.length; cnt++) {
                    vm.infoSpecific[cnt].class = tmpClass;
                }

                console.log(vm.infoSpecific);

            }).error(function(data, status) {
                console.log("bad"+$stateParams.id);
            });


        }




    }
})();

