/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('simulation', {
            parent: 'entity',
            url: '/simulation?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.simulation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/simulation/simulations.html',
                    controller: 'SimulationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'timestamp,desc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('simulation');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
            .state('simulation-create', {
                parent: 'entity',
                url: '/simulation/create',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'energliaApp.simulation.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/simulation/simulation-create.html',
                        controller: 'SimulationCreateController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id',
                        squash: true
                    },
                    sizeShort: null,
                    sizeFloat: null,
                    sizeBool : null,
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('simulation');
                        $translatePartialLoader.addPart('dataset');
                        $translatePartialLoader.addPart('population');
                        $translatePartialLoader.addPart('sshServer');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            name: null,
                            timestamp: null,
                            description: null,
                            id: null,
                            taskTotal: null,
                            taskQueue: null,
                            taskServer: null,
                            taskDone: null,
                            taskFail: null
                        };
                    }
                }
            })
        .state('simulation-detail', {
            parent: 'entity',
            url: '/simulation/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.simulation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/simulation/simulation-detail.html',
                    controller: 'SimulationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('simulation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Simulation', function($stateParams, Simulation) {
                    return Simulation.get({id : $stateParams.id});
                }]
            }
        })
        .state('simulation.edit', {
            parent: 'simulation',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation/simulation-dialog.html',
                    controller: 'SimulationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Simulation', function(Simulation) {
                            return Simulation.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('simulation', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('simulation.delete', {
            parent: 'simulation',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/simulation/simulation-delete-dialog.html',
                    controller: 'SimulationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Simulation', function(Simulation) {
                            return Simulation.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('simulation', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        }).state('simulation-report', {
            parent: 'entity',
            url: '/simulation-report/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.simulation.report.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/simulation/simulation-report.html',
                    controller: 'SimulationReportController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('simulation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Simulation', function($stateParams, Simulation) {
                    return Simulation.get({id : $stateParams.id});
                }]
            }
        });
    }

})();
