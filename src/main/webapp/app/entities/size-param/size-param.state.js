/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('size-param', {
            parent: 'entity',
            url: '/size-param?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sizeParam.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/size-param/size-params.html',
                    controller: 'SizeParamController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sizeParam');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('size-param-detail', {
            parent: 'entity',
            url: '/size-param/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sizeParam.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/size-param/size-param-detail.html',
                    controller: 'SizeParamDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sizeParam');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SizeParam', function($stateParams, SizeParam) {
                    return SizeParam.get({id : $stateParams.id});
                }]
            }
        })
        .state('size-param.new', {
            parent: 'size-param',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/size-param/size-param-dialog.html',
                    controller: 'SizeParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                size: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('size-param', null, { reload: true });
                }, function() {
                    $state.go('size-param');
                });
            }]
        })
        .state('size-param.edit', {
            parent: 'size-param',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/size-param/size-param-dialog.html',
                    controller: 'SizeParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SizeParam', function(SizeParam) {
                            return SizeParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('size-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('size-param.delete', {
            parent: 'size-param',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/size-param/size-param-delete-dialog.html',
                    controller: 'SizeParamDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SizeParam', function(SizeParam) {
                            return SizeParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('size-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
