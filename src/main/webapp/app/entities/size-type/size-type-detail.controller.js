/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SizeTypeDetailController', SizeTypeDetailController);

    SizeTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'SizeType'];

    function SizeTypeDetailController($scope, $rootScope, $stateParams, entity, SizeType) {
        var vm = this;
        vm.sizeType = entity;
        vm.load = function (id) {
            SizeType.get({id: id}, function(result) {
                vm.sizeType = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:sizeTypeUpdate', function(event, result) {
            vm.sizeType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
