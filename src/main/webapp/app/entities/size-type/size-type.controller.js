/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SizeTypeController', SizeTypeController);

    SizeTypeController.$inject = ['$scope', '$state', 'SizeType', 'SizeTypeSearch'];

    function SizeTypeController ($scope, $state, SizeType, SizeTypeSearch) {
        var vm = this;
        vm.sizeTypes = [];
        vm.loadAll = function() {
            SizeType.query(function(result) {
                vm.sizeTypes = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            SizeTypeSearch.query({query: vm.searchQuery}, function(result) {
                vm.sizeTypes = result;
            });
        };
        vm.loadAll();
        
    }
})();
