/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('size-type', {
            parent: 'entity',
            url: '/size-type',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sizeType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/size-type/size-types.html',
                    controller: 'SizeTypeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sizeType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('size-type-detail', {
            parent: 'entity',
            url: '/size-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sizeType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/size-type/size-type-detail.html',
                    controller: 'SizeTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sizeType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SizeType', function($stateParams, SizeType) {
                    return SizeType.get({id : $stateParams.id});
                }]
            }
        })
        .state('size-type.new', {
            parent: 'size-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/size-type/size-type-dialog.html',
                    controller: 'SizeTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('size-type', null, { reload: true });
                }, function() {
                    $state.go('size-type');
                });
            }]
        })
        .state('size-type.edit', {
            parent: 'size-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/size-type/size-type-dialog.html',
                    controller: 'SizeTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SizeType', function(SizeType) {
                            return SizeType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('size-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('size-type.delete', {
            parent: 'size-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/size-type/size-type-delete-dialog.html',
                    controller: 'SizeTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SizeType', function(SizeType) {
                            return SizeType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('size-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
