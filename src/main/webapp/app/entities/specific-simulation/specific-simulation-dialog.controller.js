/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SpecificSimulationDialogController', SpecificSimulationDialogController);

    SpecificSimulationDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'SpecificSimulation', 'SimulationStatusType', 'Simulation', 'SshAccount', 'DatasetCombination'];

    function SpecificSimulationDialogController ($scope, $stateParams, $uibModalInstance, DataUtils, entity, SpecificSimulation, SimulationStatusType, Simulation, SshAccount, DatasetCombination) {
        var vm = this;
        vm.specificSimulation = entity;
        vm.simulationstatustypes = SimulationStatusType.query();
        vm.simulations = Simulation.query();
        vm.sshaccounts = SshAccount.query();
        vm.datasetcombinations = DatasetCombination.query();
        vm.load = function(id) {
            SpecificSimulation.get({id : id}, function(result) {
                vm.specificSimulation = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:specificSimulationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.specificSimulation.id !== null) {
                SpecificSimulation.update(vm.specificSimulation, onSaveSuccess, onSaveError);
            } else {
                SpecificSimulation.save(vm.specificSimulation, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.setFilelog = function ($file, specificSimulation) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        specificSimulation.filelog = base64Data;
                        specificSimulation.filelogContentType = $file.type;
                    });
                });
            }
        };

        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
    }
})();
