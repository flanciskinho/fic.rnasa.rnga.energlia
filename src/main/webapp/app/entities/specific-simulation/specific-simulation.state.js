/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('specific-simulation', {
            parent: 'entity',
            url: '/specific-simulation?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.specificSimulation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/specific-simulation/specific-simulations.html',
                    controller: 'SpecificSimulationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('specificSimulation');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('specific-simulation-detail', {
            parent: 'entity',
            url: '/specific-simulation/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.specificSimulation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/specific-simulation/specific-simulation-detail.html',
                    controller: 'SpecificSimulationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('specificSimulation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SpecificSimulation', function($stateParams, SpecificSimulation) {
                    return SpecificSimulation.get({id : $stateParams.id});
                }]
            }
        })
        .state('specific-simulation.new', {
            parent: 'specific-simulation',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/specific-simulation/specific-simulation-dialog.html',
                    controller: 'SpecificSimulationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                logVersion: null,
                                filelog: null,
                                filelogContentType: null,
                                nodeVersion: null,
                                jobid: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('specific-simulation', null, { reload: true });
                }, function() {
                    $state.go('specific-simulation');
                });
            }]
        })
        .state('specific-simulation.edit', {
            parent: 'specific-simulation',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/specific-simulation/specific-simulation-dialog.html',
                    controller: 'SpecificSimulationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SpecificSimulation', function(SpecificSimulation) {
                            return SpecificSimulation.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('specific-simulation', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('specific-simulation.delete', {
            parent: 'specific-simulation',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/specific-simulation/specific-simulation-delete-dialog.html',
                    controller: 'SpecificSimulationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SpecificSimulation', function(SpecificSimulation) {
                            return SpecificSimulation.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('specific-simulation', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
