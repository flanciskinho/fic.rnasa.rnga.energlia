/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SshAccountDialogController', SshAccountDialogController);

    SshAccountDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'SshAccount', 'SshServer'];

    function SshAccountDialogController ($scope, $stateParams, $uibModalInstance, entity, SshAccount, SshServer) {
        var vm = this;
        vm.sshAccount = entity;
        vm.sshservers = SshServer.query();
        vm.load = function(id) {
            SshAccount.get({id : id}, function(result) {
                vm.sshAccount = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:sshAccountUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.sshAccount.id !== null) {
                SshAccount.update(vm.sshAccount, onSaveSuccess, onSaveError);
            } else {
                SshAccount.save(vm.sshAccount, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
