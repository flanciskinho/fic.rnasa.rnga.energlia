/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('ssh-account', {
            parent: 'entity',
            url: '/ssh-account?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sshAccount.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ssh-account/ssh-accounts.html',
                    controller: 'SshAccountController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sshAccount');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('ssh-account-detail', {
            parent: 'entity',
            url: '/ssh-account/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sshAccount.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ssh-account/ssh-account-detail.html',
                    controller: 'SshAccountDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sshAccount');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SshAccount', function($stateParams, SshAccount) {
                    return SshAccount.get({id : $stateParams.id});
                }]
            }
        })
        .state('ssh-account.new', {
            parent: 'ssh-account',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ssh-account/ssh-account-dialog.html',
                    controller: 'SshAccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                username: null,
                                key: null,
                                activated: false,
                                numJob: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('ssh-account', null, { reload: true });
                }, function() {
                    $state.go('ssh-account');
                });
            }]
        })
        .state('ssh-account.edit', {
            parent: 'ssh-account',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ssh-account/ssh-account-dialog.html',
                    controller: 'SshAccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SshAccount', function(SshAccount) {
                            return SshAccount.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ssh-account', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('ssh-account.delete', {
            parent: 'ssh-account',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ssh-account/ssh-account-delete-dialog.html',
                    controller: 'SshAccountDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SshAccount', function(SshAccount) {
                            return SshAccount.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ssh-account', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
