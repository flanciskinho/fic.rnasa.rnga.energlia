/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('SshServerDialogController', SshServerDialogController);

    SshServerDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'SshServer', 'QueueType'];

    function SshServerDialogController ($scope, $stateParams, $uibModalInstance, entity, SshServer, QueueType) {
        var vm = this;
        vm.sshServer = entity;
        vm.queuetypes = QueueType.query();
        vm.load = function(id) {
            SshServer.get({id : id}, function(result) {
                vm.sshServer = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('energliaApp:sshServerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true
            if (vm.sshServer.numConnection === undefined || vm.sshServer.numConnection == null)
                vm.sshServer.numConnection = 0;
            if (vm.sshServer.id !== null) {
                SshServer.update(vm.sshServer, onSaveSuccess, onSaveError);
            } else {
                SshServer.save(vm.sshServer, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
