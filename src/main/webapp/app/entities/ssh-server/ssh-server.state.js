/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('ssh-server', {
            parent: 'entity',
            url: '/ssh-server?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sshServer.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ssh-server/ssh-servers.html',
                    controller: 'SshServerController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sshServer');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('ssh-server-detail', {
            parent: 'entity',
            url: '/ssh-server/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.sshServer.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/ssh-server/ssh-server-detail.html',
                    controller: 'SshServerDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sshServer');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SshServer', function($stateParams, SshServer) {
                    return SshServer.get({id : $stateParams.id});
                }]
            }
        })
        .state('ssh-server.new', {
            parent: 'ssh-server',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ssh-server/ssh-server-dialog.html',
                    controller: 'SshServerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dnsname: null,
                                fingerprint: null,
                                port: null,
                                maxJob: null,
                                maxProc: null,
                                maxTime: null,
                                maxSpace: null,
                                maxMemory: null,
                                maxConnection: null,
                                numConnection: null,
                                activated: false,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('ssh-server', null, { reload: true });
                }, function() {
                    $state.go('ssh-server');
                });
            }]
        })
        .state('ssh-server.edit', {
            parent: 'ssh-server',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ssh-server/ssh-server-dialog.html',
                    controller: 'SshServerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SshServer', function(SshServer) {
                            return SshServer.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ssh-server', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('ssh-server.delete', {
            parent: 'ssh-server',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/ssh-server/ssh-server-delete-dialog.html',
                    controller: 'SshServerDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SshServer', function(SshServer) {
                            return SshServer.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('ssh-server', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
