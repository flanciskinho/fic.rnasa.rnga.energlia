/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('TechniqueParamDetailController', TechniqueParamDetailController);

    TechniqueParamDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'TechniqueParam', 'LogRecord'];

    function TechniqueParamDetailController($scope, $rootScope, $stateParams, entity, TechniqueParam, LogRecord) {
        var vm = this;
        vm.techniqueParam = entity;
        vm.load = function (id) {
            TechniqueParam.get({id: id}, function(result) {
                vm.techniqueParam = result;
            });
        };
        var unsubscribe = $rootScope.$on('energliaApp:techniqueParamUpdate', function(event, result) {
            vm.techniqueParam = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
