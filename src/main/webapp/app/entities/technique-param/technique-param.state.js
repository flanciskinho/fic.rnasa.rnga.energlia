/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('technique-param', {
            parent: 'entity',
            url: '/technique-param?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.techniqueParam.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/technique-param/technique-params.html',
                    controller: 'TechniqueParamController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('techniqueParam');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('technique-param-detail', {
            parent: 'entity',
            url: '/technique-param/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'energliaApp.techniqueParam.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/technique-param/technique-param-detail.html',
                    controller: 'TechniqueParamDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('techniqueParam');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TechniqueParam', function($stateParams, TechniqueParam) {
                    return TechniqueParam.get({id : $stateParams.id});
                }]
            }
        })
        .state('technique-param.new', {
            parent: 'technique-param',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/technique-param/technique-param-dialog.html',
                    controller: 'TechniqueParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                value: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('technique-param', null, { reload: true });
                }, function() {
                    $state.go('technique-param');
                });
            }]
        })
        .state('technique-param.edit', {
            parent: 'technique-param',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/technique-param/technique-param-dialog.html',
                    controller: 'TechniqueParamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TechniqueParam', function(TechniqueParam) {
                            return TechniqueParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('technique-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('technique-param.delete', {
            parent: 'technique-param',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/technique-param/technique-param-delete-dialog.html',
                    controller: 'TechniqueParamDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TechniqueParam', function(TechniqueParam) {
                            return TechniqueParam.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('technique-param', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
