/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';

    angular
        .module('energliaApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService'];

    function HomeController ($scope, Principal, LoginService) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        vm.userOption = [
            {admin: false, img: "content/images/option/dataset.png",      section: "dataset",      href: "#dataset"},
            {admin: false, img: "content/images/option/population.jpg",   section: "population",   href: "#population"},
            {admin: false, img: "content/images/option/simulation.jpg",   section: "simulation",   href: "#simulation"}
        ];
        vm.adminOption = [
            {admin: true,  img: "content/images/option/ssh_server.png",   section: "ssh_server",   href: "#ssh-server"},
            {admin: true,  img: "content/images/option/ssh_account.png",  section: "ssh_account",  href: "#ssh-account"},

            {admin: true,  img: "content/images/option/app.svg",          section: "app",          href: "#application"},
            {admin: true,  img: "content/images/option/compiled_app.svg", section: "compiled_app", href: "#compiled-app"},
        ];
    }
})();
