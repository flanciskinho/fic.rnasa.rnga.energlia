----------------
-- dataset
----------------
INSERT INTO dataset(id, name, timestamp, input_feature, output_feature, num_class, analyze_id)
VALUES
(1111, 'd1', now(), 4, 2, 2, 1),
(2222, 'd2', now(), 4, 2, 2, 2),
(3333, 'd3', now(), 10, 1, 2, 1),
(4444, 'd4', now(), 10, 1, 2, 1);

INSERT INTO dataset_combination(belong_id, index_combination, trainfilein, trainfilein_content_type, trainfileout, trainfileout_content_type)
VALUES
(1111, 1, x'31', 'txt', x'32', 'txt'),
(1111, 2, x'33', 'txt', x'34', 'txt'),
(1111, 3, x'35', 'txt', x'36', 'txt'),
(2222, 4, x'37', 'txt', x'38', 'txt'),
(2222, 1, x'39', 'txt', x'30', 'txt'),
(2222, 2, x'31', 'txt', x'32', 'txt'),
(2222, 3, x'33', 'txt', x'34', 'txt'),
(3333, 1, x'35', 'txt', x'36', 'txt'),
(3333, 2, x'37', 'txt', x'38', 'txt'),
(4444, 1, x'31', 'txt', x'32', 'txt'),
(4444, 2, x'33', 'txt', x'34', 'txt'),
(4444, 3, x'35', 'txt', x'36', 'txt'),
(4444, 4, x'37', 'txt', x'38', 'txt');

UPDATE dataset d
SET d.owner_id = 1 + (id % 4),
    d.num_pattern = 1 + id,
    d.description = '',
    d.num_combination =
    ( SELECT count(id)
      FROM dataset_combination
      WHERE belong_id = d.id
    )

WHERE owner_id IS NULL;


----------------
-- population
----------------
-- p1
INSERT INTO population(name, timestamp)
VALUES ('p1', now());

SET @lastid = LAST_INSERT_ID();

INSERT INTO size_param(size, belong_id, use_id)
VALUES
(5, @lastid, 2),
(3, @lastid, 3);

INSERT INTO population_combination(belong_id, index_combination, file, file_content_type)
VALUES
(@lastid, 1, x'3C706F7020312F3E', 'txt'),
(@lastid, 2, x'3C706F7020322F3E', 'txt');

-- p2
INSERT INTO population(name, timestamp)
VALUES ('p2', now());

SET @lastid = LAST_INSERT_ID();

INSERT INTO size_param(size, belong_id, use_id)
VALUES
(18, @lastid, 3);

INSERT INTO population_combination(belong_id, index_combination, file, file_content_type)
VALUES
(@lastid, 1, x'3C706F7020312F3E', 'txt'),
(@lastid, 2, x'3C706F7020322F3E', 'txt');

-- p3
INSERT INTO population(name, timestamp)
VALUES ('p3', now());

SET @lastid = LAST_INSERT_ID();

INSERT INTO size_param(size, belong_id, use_id)
VALUES
(5, @lastid, 2);

INSERT INTO population_combination(belong_id, index_combination, file, file_content_type)
VALUES
(@lastid, 1, x'3C706F7020312F3E', 'txt'),
(@lastid, 2, x'3C706F7020322F3E', 'txt');

-- p4
INSERT INTO population(name, timestamp)
VALUES ('p4', now());

SET @lastid = LAST_INSERT_ID();

INSERT INTO size_param(size, belong_id, use_id)
VALUES
(3, @lastid, 2),
(7, @lastid, 3);

INSERT INTO population_combination(belong_id, index_combination, file, file_content_type)
VALUES
(@lastid, 1, x'3C706F7020312F3E', 'txt'),
(@lastid, 2, x'3C706F7020322F3E', 'txt'),
(@lastid, 3, x'3C706F7020332F3E', 'txt');

UPDATE population p
SET p.owner_id = 1 + (id % 4),
    p.num_individual = 1 + id,
    p.num_combination = 1,
    p.description = '',
    p.size_genotype =
    ( SELECT SUM(size)
      FROM size_param
      WHERE belong_id = p.id
    ),
    p.num_combination =
    ( SELECT count(id)
      FROM population_combination
      WHERE belong_id = p.id
    )
WHERE owner_id IS NULL;

----------------
-- sshserver
----------------
INSERT INTO ssh_server (dnsname, port, fingerprint, max_job, max_proc, max_time, max_space, max_memory, activated, max_connection, num_connection, version)
VALUES
('.org', 22, 'AA', 108, 7, 60, 1, 1, true, 1, 0, 1),
('.org', 22, 'BB',   1, 8, 50, 1, 1, true, 1, 0, 1),
('.org', 22, 'CC',   2, 6, 40, 1, 1, true, 1, 0, 1),
('.org', 22, 'DD',   4, 4, 30, 1, 1, true, 1, 0, 1),
('.org', 22, 'EE',   6, 2, 20, 1, 1, true, 1, 0, 1),
('.org', 22, '11',   8, 1, 10, 1, 1, true, 1, 0, 1);

UPDATE ssh_server
SET activated = false,
    max_space  = id*10*1024*1024,
    max_memory = id*1024*1024
WHERE use_id IS NULL
  AND id % 2 = 0;

UPDATE ssh_server
SET use_id = 1 + (id % 2),
    dnsname = CONCAT('example', CONCAT(id, dnsname))
WHERE use_id IS NULL;

----------------
-- simulations
----------------
insert into simulation(name, timestamp, description, belong_id, use_id, exe_id, problem_id, minimize_id, launch_id, task_total, task_done, task_fail, task_queue, task_server) values ('prove', now(), '', 1, 1111, null, 3, 1, 1, 3, 3, 0, 0, 0);

SET @lastid = LAST_INSERT_ID();

insert into specific_simulation(id, status_id, belong_id)
values
(1111, 4, @last_id),
(2222, 4, @last_id),
(3333, 4, @last_id);

insert into log_record(index_record, timestamp, error_train, belong_id)
values
(0, now(), 30/(1+0), 1111),
(2, now(), 30/(1+2), 1111),
(4, now(), 30/(1+4), 1111),
(6, now(), 30/(1+6), 1111),
(8, now(), 30/(1+8), 1111),
(10, now(), 30/(1+10), 1111),
(12, now(), 30/(1+12), 1111),
(14, now(), 30/(1+14), 1111),
(16, now(), 30/(1+16), 1111),
(18, now(), 30/(1+18), 1111),
(20, now(), 30/(1+20), 1111),
(22, now(), 30/(1+22), 1111),
(24, now(), 30/(1+24), 1111),
(26, now(), 30/(1+26), 1111),
(28, now(), 30/(1+28), 1111),
(30, now(), 30/(1+30), 1111),

(0, now(), 100/(1+0.0), 2222),
(4, now(), 100/(1+4.4), 2222),
(8, now(), 100/(1+8.8), 2222),
(12, now(), 100/(1+12.12), 2222),
(16, now(), 100/(1+16.16), 2222),
(20, now(), 100/(1+20.20), 2222),
(24, now(), 100/(1+24.24), 2222),
(28, now(), 100/(1+28.28), 2222),
(32, now(), 100/(1+32.32), 2222),
(36, now(), 100/(1+36.36), 2222),
(40, now(), 100/(1+40.40), 2222),
(44, now(), 100/(1+44.44), 2222),
(48, now(), 100/(1+48.48), 2222),
(52, now(), 100/(1+52.52), 2222),
(56, now(), 100/(1+56.56), 2222),
(60, now(), 100/(1+60.60), 2222),
(64, now(), 100/(1+64.64), 2222),
(68, now(), 100/(1+68.68), 2222),

(0, now(), 20/(1+00.0), 3333),
(3, now(), 20/(1+33.3), 3333),
(6, now(), 20/(1+66.6), 3333),
(9, now(), 20/(1+99.9), 3333),
(12, now(), 20/(1+1212.12), 3333),
(15, now(), 20/(1+1515.15), 3333),
(18, now(), 20/(1+1818.18), 3333),
(21, now(), 20/(1+2121.21), 3333),
(24, now(), 20/(1+2424.24), 3333),
(27, now(), 20/(1+2727.27), 3333),
(30, now(), 20/(1+3030.30), 3333),
(33, now(), 20/(1+3333.33), 3333),
(36, now(), 20/(1+3636.36), 3333),
(39, now(), 20/(1+3939.39), 3333),
(42, now(), 20/(1+4242.42), 3333),
(45, now(), 20/(1+4545.45), 3333),
(48, now(), 20/(1+4848.48), 3333);