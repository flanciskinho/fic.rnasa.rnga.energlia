/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.domain;


import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.*;
import es.udc.tic.rnasa.type.GeneticAlgorithm.*;
import es.udc.tic.rnasa.web.rest.*;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.*;
import es.udc.tic.rnasa.web.rest.dto.SimulationDTO;
import es.udc.tic.rnasa.web.rest.dto.SshAccountDTO;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.*;

@Component

public class CreateEntities {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private SshProperties sshProperties;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;

    @Inject
    private AnalyzeTypeService analyzeTypeService;

    @Inject
    private PopulationService populationService;

    @Inject
    private ProblemTypeService problemTypeService;

    @Inject
    private ProblemTypeRepository problemTypeRepository;

    @Inject
    private ErrorTypeService errorTypeService;

    @Inject
    private ErrorTypeRepository errorTypeRepository;

    @Inject
    private SshServerService sshServerService;
    @Inject
    private SshServerRepository sshServerRepository;

    @Inject
    private SshAccountService sshAccountService;
    @Inject
    private SshAccountRepository sshAccountRepository;

    @Inject
    private ArchTypeService archTypeService;
    @Inject
    private ArchTypeRepository archTypeRepository;


    // Dataset things
    @Inject
    private DatasetService datasetService;
    @Inject
    private DatasetRepository datasetRepository;

    @Inject
    private DatasetCombinationService datasetCombinationService;
    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    // Simulation things
    @Inject
    private SimulationService simulationService;
    @Inject
    private SimulationRepository simulationRepository;

    // Application things
    @Inject
    private QueueTypeService queueTypeService;
    @Inject
    private QueueTypeRepository queueTypeRepository;
    @Inject
    private ApplicationService applicationService;
    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private CompiledAppService compiledAppService;
    @Inject
    private CompiledAppRepository compiledAppRepository;

    private UserDTO ownerDTO;
    private User owner;
    private static final String loginname = "joe";
    private static final String password  = "123456789012345678901234567890123456789012345678901234567890";


    @PostConstruct
    public void config() {
        logger.debug("Running create entities");

        MockitoAnnotations.initMocks(this);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);

        AnalyzeTypeResource analyzeTypeResource = new AnalyzeTypeResource();
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeService", analyzeTypeService);


        // Data set things
        DatasetResource datasetResource = new DatasetResource();
        ReflectionTestUtils.setField(datasetResource, "datasetService", datasetService);
        ReflectionTestUtils.setField(datasetService, "datasetRepository", datasetRepository);

        DatasetCombinationResource datasetCombinationResource = new DatasetCombinationResource();
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationService", datasetCombinationService);
        ReflectionTestUtils.setField(datasetCombinationService, "datasetCombinationRepository", datasetCombinationRepository);

        // Population things
        PopulationResource populationResource = new PopulationResource();
        ReflectionTestUtils.setField(populationResource, "populationService", populationService);

        ProblemTypeResource problemTypeResource = new ProblemTypeResource();
        ReflectionTestUtils.setField(problemTypeResource, "problemTypeService", problemTypeService);
        ReflectionTestUtils.setField(problemTypeService, "problemTypeRepository", problemTypeRepository);

        ErrorTypeResource errorTypeResource = new ErrorTypeResource();
        ReflectionTestUtils.setField(errorTypeResource, "errorTypeService", errorTypeService);
        ReflectionTestUtils.setField(errorTypeService, "errorTypeRepository", errorTypeRepository);

        SshServerResource sshServerResource = new SshServerResource();
        ReflectionTestUtils.setField(sshServerResource, "sshServerService", sshServerService);
        ReflectionTestUtils.setField(sshServerService, "sshServerRepository", sshServerRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
        ReflectionTestUtils.setField(sshAccountService, "sshAccountRepository", sshAccountRepository);

        ArchTypeResource archTypeResource = new ArchTypeResource();
        ReflectionTestUtils.setField(archTypeResource, "archTypeService", archTypeService);
        ReflectionTestUtils.setField(archTypeService, "archTypeRepository", archTypeRepository);

        // Simulation things
        SimulationResource simulationResource = new SimulationResource();
        ReflectionTestUtils.setField(simulationResource, "simulationService", simulationService);
        ReflectionTestUtils.setField(simulationService, "simulationRepository", simulationRepository);


        // App things
        QueueTypeResource queueTypeResource = new QueueTypeResource();
        ReflectionTestUtils.setField(queueTypeResource, "queueTypeService", queueTypeService);
        ReflectionTestUtils.setField(queueTypeService, "queueTypeRepository", queueTypeRepository);

        CompiledAppResource compiledAppResource = new CompiledAppResource();
        ReflectionTestUtils.setField(compiledAppResource, "compiledAppService", compiledAppService);
        ReflectionTestUtils.setField(compiledAppService, "compiledAppRepository", compiledAppRepository);

        ApplicationResource applicationResource = new ApplicationResource();
        ReflectionTestUtils.setField(applicationResource, "applicationService", applicationService);
        ReflectionTestUtils.setField(applicationService, "applicationRepository", applicationRepository);


        logger.debug("userRepository:        {}", userRepository);
        logger.debug("userMapper:            {}", userMapper);
        logger.debug("analyzeTypeService:    {}", analyzeTypeService);
        logger.debug("datasetService:        {}", datasetService);
        logger.debug("populationService:     {}", populationService);
        logger.debug("problemTypeRepository: {}", problemTypeRepository);
        logger.debug("errorTypeRepository:   {}", errorTypeRepository);
        logger.debug("sshServerRepository:   {}", sshServerRepository);
        logger.debug("archTypeRepository:    {}", archTypeRepository);

        ownerDTO = new UserDTO(
            loginname,                  // login
            password,
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin(loginname).isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }

    }

    public User getOwner() {
        return owner;
    }

    private void authenticate() {
        // Authenticate
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.USER));
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(loginname, password, authorities);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Application createApplication(String name, String version, ZonedDateTime time, boolean activated) {
        Application app = new Application();
        app.setActivated(activated);
        app.setFeature("feature".getBytes());
        app.setFeatureContentType("text/markdown");
        app.setVersion(version);
        app.setName(name);
        app.setTimestamp(time);

        return applicationRepository.saveAndFlush(app);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public CompiledApp createCompiledApp(Application app, SshServer use) {
        CompiledApp compiledApp = new CompiledApp();
        compiledApp.setBelong(app);
        compiledApp.setUse(use);

        compiledApp.setExecutable("exe".getBytes());
        compiledApp.setExecutableContentType("exe/exe");

        return compiledAppRepository.saveAndFlush(compiledApp);
    }


    // To get Error
    @Transactional(propagation = Propagation.REQUIRED)
    public ErrorType getErrorType(String name) {
        Optional<ErrorType> op = errorTypeRepository.findOneByName(name);

        return op.orElse(null);
    }

    // To get ProblemType

    @Transactional(propagation = Propagation.REQUIRED)
    public ProblemType getProblemType(String name) {
        return problemTypeRepository.findOneByName(name);
    }

    // To create SshServer


    public SshServer createSshServer() throws Exception {
        return createSshServer(sshProperties.getServer().getFingerprint());
    }

    public SshServer createSshServer(String fingerprint) throws Exception {
        return createSshServer(sshProperties.getServer().getHost(), fingerprint, 1);
    }

    public SshServer createSshServer(String name, int numArch) throws Exception {
        return createSshServer(name, sshProperties.getServer().getFingerprint(), numArch);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SshServer createSshServer(String hostname, String fingerprint, int numArch) throws Exception {

        SshServer sshServer = new SshServer();
        sshServer.setDnsname(hostname);
        sshServer.setFingerprint(fingerprint);
        sshServer.setMaxJob(30);
        sshServer.setMaxProc(32);
        sshServer.setMaxTime(60l);
        sshServer.setMaxSpace(10000l);
        sshServer.setMaxMemory(100l);
        sshServer.setActivated(true);
        sshServer.setMaxConnection(10);
        sshServer.setPort(sshProperties.getServer().getPort());
        sshServer.setNumConnection(0);
        sshServer.setUse(queueTypeRepository.findFirstByName("NQS"));

        sshServer = sshServerRepository.saveAndFlush(sshServer);

        for (int cnt = 0; cnt < numArch; cnt++)
            archTypeRepository.saveAndFlush(new ArchType(String.format("arch %d", cnt), true, sshServer));

        return sshServer;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SshAccount createSshAccount(String username, String key, SshServer sshServer) {
        SshAccountDTO sshAccountDTO = new SshAccountDTO();
        sshAccountDTO.setBelongId(sshServer.getId());
        sshAccountDTO.setBelongDnsname(sshServer.getDnsname());
        sshAccountDTO.setKey(key);
        sshAccountDTO.setNumJob(0);
        sshAccountDTO.setActivated(true);
        sshAccountDTO.setVersion(0l);
        sshAccountDTO.setUsername(username);

        //logger.debug("user: {}", SecurityUtils.getCurrentUserLogin());

        authenticate();

        sshAccountDTO = sshAccountService.save(sshAccountDTO);

        SshAccount sshAccount =  sshAccountRepository.findOne(sshAccountDTO.getId());

        SecurityContextHolder.clearContext();

        return sshAccount;
    }

    public SshAccount createSshAccount(String username, SshServer sshServer) {
        return createSshAccount(username, username+sshServer.getFingerprint(), sshServer);
    }



    // To create ArchType
    @Transactional(propagation = Propagation.REQUIRED)
    public ArchType createArchType(String name, boolean activated, SshServer sshServer) throws Exception {
        ArchType archType = new ArchType();
        archType.setName(name);
        archType.setActivated(activated);
        archType.setBelong(sshServer);

        return archTypeRepository.saveAndFlush(archType);
    }

    // To create Dataset

    @Transactional(propagation = Propagation.REQUIRED)
    public Dataset createDataset() throws Exception {
        authenticate();

        List<byte []> trainInput = new ArrayList<>();
        trainInput.add("0 1 2\n3 4 5".getBytes());
        trainInput.add("6 7 8".getBytes());

        List<byte []> trainOutput = new ArrayList<>();
        trainOutput.add("0\n0".getBytes());
        trainOutput.add("6".getBytes());

        List<byte []> validationInput = new ArrayList<>();
        validationInput.add("6 7 8".getBytes());
        validationInput.add("3 4 5".getBytes());

        List<byte []> validationOutput = new ArrayList<>();
        validationOutput.add("6".getBytes());
        validationOutput.add("0".getBytes());

        List<byte []> testInput = new ArrayList<>();
        testInput.add("9 10 11".getBytes());
        testInput.add("0 1 2\n9 10 11".getBytes());

        List<byte []> testOutput = new ArrayList<>();
        testOutput.add("9".getBytes());
        testOutput.add("0\n9".getBytes());



        Dataset dataset = datasetService.createDataset(owner, "dataset name", "description for dataset", analyzeTypeService.getCLASSIFICATION(),
            trainInput, trainOutput,
            validationInput, validationOutput,
            testInput, testOutput);

        SecurityContextHolder.clearContext();

        return dataset;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public Dataset createDataset(int featuresInput, int featuresOutput, int numCombination, AnalyzeType analyzeType) throws Exception {
        authenticate();

        List<byte []>  input = new ArrayList<>(numCombination);
        List<byte []> output = new ArrayList<>(numCombination);
        StringBuffer buffer;
        int cnt;

        buffer = new StringBuffer();
        for (cnt = 0; cnt < featuresInput; cnt++) {
            buffer.append(cnt+1 == featuresInput?cnt:cnt+" ");
        }
        for (cnt = 0; cnt < numCombination; cnt++)
            input.add(buffer.toString().getBytes());

        buffer = new StringBuffer();
        for (cnt = 0; cnt < featuresOutput; cnt++) {
            buffer.append(cnt+1 == featuresOutput?cnt:cnt+" ");
        }
        for (cnt = 0; cnt < numCombination; cnt++)
            output.add(buffer.toString().getBytes());

        Dataset dataset = datasetService.createDataset(owner, "dataset", "", analyzeType == null?analyzeTypeService.getCLASSIFICATION():analyzeType,
            input, output, input, output, input, output
        );

        SecurityContextHolder.clearContext();

        return dataset;
    }


    // To create population
    @Transactional(propagation = Propagation.REQUIRED)
    public Population createPopulation(int genoFloat, int genoShort, int numCombination) throws Exception {
        authenticate();

        List<byte []> files = new ArrayList<>(numCombination);

        List<Map<Integer, Short>> indShort = getShortIndividual(1, genoShort, 0);
        List<Map<Integer, Float>> indFloat = getFloatIndividual(1, genoFloat, 0);

        String str = getRealPopulationv1_0(indShort, indFloat);

        for (int cnt = 0; cnt < numCombination; cnt++)
            files.add(str.getBytes());

        Population population = populationService.createPopulation(owner, "Population name", "Description for population", files);

        SecurityContextHolder.clearContext();

        return population;
    }

    private List<Map<Integer, Short>> getShortIndividual(int sizeIndividual, int sizeGeno, int startValue) {
        int cnt, tmp;

        List<Map<Integer, Short>> i_short = new ArrayList<>();
        Map<Integer, Short> m_short;

        for (tmp = 0; tmp < sizeIndividual; tmp++) {
            m_short = new HashMap();
            for (cnt = 0; cnt < sizeGeno; cnt++)
                m_short.put(cnt, (short) (cnt + tmp * sizeGeno + startValue));
            i_short.add(m_short);
        }

        return i_short;
    }

    private List<Map<Integer, Float>> getFloatIndividual(int sizeIndividual, int sizeGeno, int startValue) {
        int cnt, tmp;

        List<Map<Integer, Float>> i_float = new ArrayList<>();
        Map<Integer, Float> m_float;

        for (tmp = 0; tmp < sizeIndividual; tmp++) {
            m_float = new HashMap();
            for (cnt = 0; cnt < sizeGeno; cnt++)
                m_float.put(cnt, (float) (cnt + tmp * sizeGeno + startValue) + 0.5f);
            i_float.add(m_float);
        }

        return i_float;
    }

    private String getRealPopulationv1_0(List<Map<Integer, Short>> i_short,List<Map<Integer, Float>> i_float) {
        String frame = "<population version='1.0' encode='2' num_individuals='%d' s_genoshort='%d' s_genofloat='%d'>\n%s</population>";
        String ind = "\t<individual version='1.0' encode='2'>\n%s\t</individual>\n";
        String g_short = "\t\t<genoshort id='%d' value='%d' />\n";
        String g_float = "\t\t<genofloat id='%d' value='%f' />\n";

        StringBuffer str1 = new StringBuffer();
        StringBuffer str2;

        for (int index = 0; index < Math.max(i_short.size(), i_float.size()); index++) {
            str2 = new StringBuffer();


            if (index < i_short.size())
                for (Map.Entry<Integer, Short> entry : i_short.get(index).entrySet()) {
                    str2.append(String.format(g_short, entry.getKey(), entry.getValue()));
                }

            if (index < i_float.size())
                for (Map.Entry<Integer, Float> entry: i_float.get(index).entrySet()) {
                    str2.append(String.format(g_float, entry.getKey(), entry.getValue()));
                }

            str1.append(String.format(ind, str2.toString()));
        }


        int numIndividuals = Math.max(i_short.size(), i_float.size());
        return String.format(frame,
            numIndividuals,
            numIndividuals*(i_short.size() == 0?0:i_short.get(0).size()),
            numIndividuals*(i_float.size() == 0?0:i_float.get(0).size()),
            str1.toString());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Population createPopulation(int sizeGenoShort, int sizeGenoFloat) throws Exception {
        Locale.setDefault(Locale.ENGLISH);
        authenticate();

        int cnt;
        int numIndividual = 2;

        List<Map<Integer, Short>> i1_short = getShortIndividual(numIndividual, sizeGenoShort, 0);
        List<Map<Integer, Float>> i1_float = getFloatIndividual(numIndividual, sizeGenoFloat, 0);

        List<Map<Integer, Short>> i2_short = getShortIndividual(numIndividual, sizeGenoShort, numIndividual*sizeGenoShort);
        List<Map<Integer, Float>> i2_float = getFloatIndividual(numIndividual, sizeGenoFloat, numIndividual*sizeGenoFloat);

        List<byte []> files = new ArrayList<>(2);
        files.add(getRealPopulationv1_0(i1_short, i1_float).getBytes());
        files.add(getRealPopulationv1_0(i2_short, i2_float).getBytes());

        Population population = populationService.createPopulation(owner, "Population name", "Description for population", files);

        SecurityContextHolder.clearContext();

        return population;
    }

    // To create Simulation
    @Transactional(propagation =  Propagation.REQUIRED)
    public Simulation createNuSvmSimulation(String name, Dataset dataset, List<Population> populations, SshServer sshServer) throws Exception {

        authenticate();

        ProblemType problemType = problemTypeRepository.findOneByName("nuSVM");

        if (problemType.getOutputFeatureConstraint() != null && dataset.getOutputFeature() != problemType.getOutputFeatureConstraint())
            throw new Exception("Cannot create a simulation because dataset violate output feature constraints");

        if (problemType.getNumPopulation() != populations.size())
            throw new Exception("Cannot create a simulation because populations violate number of populations");

        CommonSimulationDTO detail = new CommonSimulationDTO();
        detail.setName(name);
        detail.setDescription("Description for "+name);
        detail.setProblemId(problemType.getId());
        detail.setDatasetId(dataset.getId());
        detail.setErrorId(problemType.getMinimizes().iterator().next().getId());
        detail.setServerId(sshServer.getId());

        SimPopSelectDTO popSelectDTO1 = new SimPopSelectDTO(SelectAlgorithm.SELECT_RANDOM.getName());
        SimPopSelectDTO popSelectDTO2 = new SimPopSelectDTO(SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getName(), new Short("5"));
        SimPopMutationDTO popMutationDTO = new SimPopMutationDTO(MutationAlgorithm.CHANGE_PERCENT.getName(), 0.1f);
        SimPopCrossoverDTO popCrossoverDTO = new SimPopCrossoverDTO(CrossoverAlgorithm.REAL_SPX.getName(), 0.01f);
        SimPopReplaceDTO popReplaceDTO = new SimPopReplaceDTO(ReplaceAlgorithm.WORST_REPLACING.getName());

        SimPopulationDTO [] simPopulationDTOs = new SimPopulationDTO[1];
        simPopulationDTOs[0] = new SimPopulationDTO(0, populations.get(0).getId(), popSelectDTO1, popSelectDTO2, popMutationDTO, popCrossoverDTO, popReplaceDTO);

        Float[] arrayFloat = {0.1f, 0.2f};
        List<Float> listFloat = new ArrayList<Float>(Arrays.asList(arrayFloat));

        Integer[] arrayInteger = {1, 2};
        List<Integer> listinteger = new ArrayList<Integer>(Arrays.asList(arrayInteger));

        CustomNuSvmDTO customNuSvmDTO = new CustomNuSvmDTO();
        customNuSvmDTO.setDetail(detail);
        customNuSvmDTO.setGaBasic(new SimGaBasicDTO(10,0.1f));
        customNuSvmDTO.setExecution(new SimExecDTO("arch:"+name, 1, 2l, 3l));
        customNuSvmDTO.setPopulations(simPopulationDTOs);
        customNuSvmDTO.setCoGamma(listFloat);
        customNuSvmDTO.setCoDegree(listinteger);
        customNuSvmDTO.setCoCoef(listFloat);
        customNuSvmDTO.setCoNu(listFloat);
        customNuSvmDTO.setCoTolerance(listFloat);

        SimulationDTO simulationDTO = simulationService.save(customNuSvmDTO);

        SecurityContextHolder.clearContext();

        return simulationRepository.findOne(simulationDTO.getId());
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public Simulation createAnnFixedGliaSimulation(String name, List<Integer> neurons, Dataset dataset, List<Population> populations, SshServer sshServer) throws Exception {

        authenticate();

        //int numNeurons = neurons.stream().mapToInt(Integer::intValue).sum();
        int numLayers  = neurons.size();

        ProblemType problemType = problemTypeRepository.findOneByName("AnnFixedGlia");

        if (problemType.getOutputFeatureConstraint() != null && dataset.getOutputFeature() != problemType.getOutputFeatureConstraint())
            throw new Exception("Cannot create a simulation because dataset violate output feature constraints");

        if (dataset.getInputFeature() != neurons.get(0))
            throw new Exception("Cannot create a simulation because dataset violate input feature constraint");

        if (dataset.getOutputFeature() != neurons.get(numLayers-1))
            throw new Exception("Cannot create a simulation because dataset violate output feature constraint");

        if (problemType.getNumPopulation() != populations.size())
            throw new Exception("Cannot create a simulation because populations violate number of populations");

        CommonSimulationDTO detail = new CommonSimulationDTO();
        detail.setName(name);
        detail.setDescription("Description for "+name);
        detail.setProblemId(problemType.getId());
        detail.setDatasetId(dataset.getId());
        detail.setErrorId(problemType.getMinimizes().iterator().next().getId());
        detail.setServerId(sshServer.getId());

        SimPopSelectDTO popSelectDTO1 = new SimPopSelectDTO(SelectAlgorithm.SELECT_RANDOM.getName());
        SimPopSelectDTO popSelectDTO2 = new SimPopSelectDTO(SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getName(), new Short("5"));
        SimPopMutationDTO popMutationDTO = new SimPopMutationDTO(MutationAlgorithm.CHANGE_PERCENT.getName(), 0.1f);
        SimPopCrossoverDTO popCrossoverDTO = new SimPopCrossoverDTO(CrossoverAlgorithm.REAL_SPX.getName(), 0.01f);
        SimPopReplaceDTO popReplaceDTO = new SimPopReplaceDTO(ReplaceAlgorithm.WORST_REPLACING.getName());

        SimPopulationDTO [] simPopulationDTOs = new SimPopulationDTO[1];
        simPopulationDTOs[0] = new SimPopulationDTO(0, populations.get(0).getId(), popSelectDTO1, popSelectDTO2, popMutationDTO, popCrossoverDTO, popReplaceDTO);

        Float[] arrayFloat = {0.1f, 0.2f};
        List<Float> listFloat = new ArrayList<Float>(Arrays.asList(arrayFloat));

        CustomAnnFixedGliaDTO annFixedGliaDTO = new CustomAnnFixedGliaDTO();
        annFixedGliaDTO.setDetail(detail);
        annFixedGliaDTO.setExecution(new SimExecDTO("arch:"+name, 1, 2l, 3l));
        annFixedGliaDTO.setPopulations(simPopulationDTOs);

        List<Float> tmpFloat = new ArrayList<>(neurons.size());
        List<String> activationFunctions = new ArrayList<>(neurons.size());
        List<String> gliaAlgorithms = new ArrayList<>(neurons.size());
        for (int cnt = 0; cnt < neurons.size(); cnt++) {
            tmpFloat.add(0.1f * cnt);
            activationFunctions.add("IDENTITY");
            gliaAlgorithms.add("DEPRESSION");
        }

        annFixedGliaDTO.setGaBasic(new SimGaBasicDTO(10,0.1f));

        annFixedGliaDTO.setLayer(neurons.size());
        annFixedGliaDTO.setActivation(2);
        annFixedGliaDTO.setIteration(4);

        annFixedGliaDTO.setNeuron(neurons);
        annFixedGliaDTO.setIncrease(tmpFloat);
        annFixedGliaDTO.setDecrease(tmpFloat);
        annFixedGliaDTO.setActivationFunction(activationFunctions);
        annFixedGliaDTO.setGliaAlgorithm(gliaAlgorithms);

        annFixedGliaDTO.setCoWeight(listFloat);

        SimulationDTO simulationDTO = simulationService.save(annFixedGliaDTO);

        SecurityContextHolder.clearContext();

        return simulationRepository.findOne(simulationDTO.getId());
    }

    @Transactional(propagation =  Propagation.REQUIRED)
    public Simulation createAngnSearchGliaSimulation(String name, List<Integer> neurons, Dataset dataset, List<Population> populations, SshServer sshServer) throws Exception {

        authenticate();

        //int numNeurons = neurons.stream().mapToInt(Integer::intValue).sum();
        int numLayers  = neurons.size();

        ProblemType problemType = problemTypeRepository.findOneByName("AnnFixedGlia");

        if (problemType.getOutputFeatureConstraint() != null && dataset.getOutputFeature() != problemType.getOutputFeatureConstraint())
            throw new Exception("Cannot create a simulation because dataset violate output feature constraints");

        if (dataset.getInputFeature() != neurons.get(0))
            throw new Exception("Cannot create a simulation because dataset violate input feature constraint");

        if (dataset.getOutputFeature() != neurons.get(numLayers-1))
            throw new Exception("Cannot create a simulation because dataset violate output feature constraint");

        if (problemType.getNumPopulation() != populations.size())
            throw new Exception("Cannot create a simulation because populations violate number of populations");

        CommonSimulationDTO detail = new CommonSimulationDTO();
        detail.setName(name);
        detail.setDescription("Description for "+name);
        detail.setProblemId(problemType.getId());
        detail.setDatasetId(dataset.getId());
        detail.setErrorId(problemType.getMinimizes().iterator().next().getId());
        detail.setServerId(sshServer.getId());

        SimPopulationDTO [] simPopulationDTOs = new SimPopulationDTO[populations.size()];
        for (int cnt = 0; cnt < populations.size(); cnt++)
            simPopulationDTOs[cnt] = new SimPopulationDTO(cnt, populations.get(cnt).getId(),
                new SimPopSelectDTO(SelectAlgorithm.SELECT_RANDOM.getName()),
                new SimPopSelectDTO(SelectAlgorithm.SELECT_TOURNAMENT_DETERMINISTIC.getName(), new Short("5")),
                new SimPopMutationDTO(MutationAlgorithm.CHANGE_PERCENT.getName(), 0.1f),
                new SimPopCrossoverDTO(CrossoverAlgorithm.REAL_SPX.getName(), 0.01f),
                new SimPopReplaceDTO(ReplaceAlgorithm.WORST_REPLACING.getName()));

        Float[] arrayFloat = {0.1f, 0.2f};
        List<Float> listFloat = new ArrayList<Float>(Arrays.asList(arrayFloat));

        Integer[] arrayInteger = {1, 2};
        List<Integer> listInteger = new ArrayList<Integer>(Arrays.asList(arrayInteger));

        CustomAngnSearchGliaDTO customAngnSearchGliaDTO = new CustomAngnSearchGliaDTO();
        customAngnSearchGliaDTO.setDetail(detail);
        customAngnSearchGliaDTO.setExecution(new SimExecDTO("arch:"+name, 1, 2l, 3l));
        customAngnSearchGliaDTO.setPopulations(simPopulationDTOs);

        customAngnSearchGliaDTO.setSameGliaAlgorithm(true);
        customAngnSearchGliaDTO.setDecreaseGreatherThanIncrease(true);

        List<String> activationFunctions = new ArrayList<>(neurons.size());
        for (int cnt = 0; cnt < neurons.size(); cnt++)
            activationFunctions.add("IDENTITY");


        customAngnSearchGliaDTO.setGaBasic(new SimGaBasicDTO(10,0.1f));

        customAngnSearchGliaDTO.setGa(new SimGaDTO(FitnessAlgorithm.FITNESS_AVG.getName(), new Short("1"), new Short("2")));

        customAngnSearchGliaDTO.setLayer(neurons.size());
        customAngnSearchGliaDTO.setNeuron(neurons);
        customAngnSearchGliaDTO.setActivationFunction(activationFunctions);

        customAngnSearchGliaDTO.setCoWeight(listFloat);
        customAngnSearchGliaDTO.setCoIteration(listInteger);
        customAngnSearchGliaDTO.setCoActivation(listInteger);
        customAngnSearchGliaDTO.setCoIncrease(listFloat);
        customAngnSearchGliaDTO.setCoDecrease(listFloat);

        SimulationDTO simulationDTO = simulationService.save(customAngnSearchGliaDTO);

        SecurityContextHolder.clearContext();

        return simulationRepository.findOne(simulationDTO.getId());
    }

    public String getLogSvm() {
        return getLogSvm("1.0", "1.0", "1.0", 1);
    }

    public String getLogSvm(String logVersion, String codeVersion, String nodeVersion, int elements) {
        String root = "<logs version='"+logVersion+"' code_version='"+codeVersion+"'>%s</logs>";
        String log  = "<log generation='%d' train='%f' validation='%f' test='%f' ms='%f'> %s </log> ";

        String svmNode =
            "<svm_model version='"+nodeVersion+"'><![CDATA[svm_type nu_svr\n" +
                "kernel_type rbf\n" +
                "gamma 1\n" +
                "nr_class 2\n" +
                "total_sv 2\n" +
                "rho -0.5\n" +
                "SV\n" +
                "-0.5 1:1.1 2:2.2 3:3.3 4:4.4000001 \n" +
                "0.5 1:5.5 2:6.5999999 3:7.6999998 4:8.8000002 \n" +
            "]]></svm_model>\n";

        StringBuffer buffer = new StringBuffer();
        int cnt, tmp;
        for (cnt = 0; cnt < elements; cnt++) {
            tmp = elements-cnt+1;
            buffer.append(
                String.format(log, cnt,
                    0.3f*tmp, 0.2f*tmp, 0.1f*tmp,
                    cnt*0.1f, svmNode)
            );
        }

        String logSvm = String.format(root, buffer.toString());

        return logSvm;
    }

    public String getLogAngn() {
        return getLogAngn("1.0", "1.1", "1,0", 1);
    }

    public String getLogAngn(String logVersion, String codeVersion, String nodeVersion, int elements) {
        String root = "<logs version='"+logVersion+"' code_version='"+codeVersion+"'>%s</logs>";
        String log  = "<log generation='%d' train='%f' validation='%f' test='%f' ms='%f'> %s </log> ";

        String angnNode =
            "<angn version='"+nodeVersion+"' type='feedforward' num_neurons='9' num_layers='3' activations='2' iterations='5'>" +
                "<output type='0'><classify step='0.5' class='2' /></output>" +
                "<neuron id='0' layer='0' activation_function='0' per_increase='0.1' per_decrease='0.09' algorithm='1' />" +
                "<neuron id='1' layer='0' activation_function='1' per_increase='0.2' per_decrease='0.08' algorithm='2' />" +
                "<neuron id='2' layer='0' activation_function='2' per_increase='0.3' per_decrease='0.07' algorithm='3' />" +
                "<neuron id='3' layer='0' activation_function='3' per_increase='0.4' per_decrease='0.06' algorithm='4' />" +
                "<neuron id='4' layer='1' activation_function='0' per_increase='0.5' per_decrease='0.05' algorithm='5' />" +
                "<neuron id='5' layer='1' activation_function='1' per_increase='0.6' per_decrease='0.04' algorithm='6' />" +
                "<neuron id='6' layer='1' activation_function='2' per_increase='0.7' per_decrease='0.03' algorithm='7' />" +
                "<neuron id='7' layer='2' activation_function='3' per_increase='0.8' per_decrease='0.02' algorithm='0' />" +
                "<neuron id='8' layer='2' activation_function='0' per_increase='0.9' per_decrease='0.01' algorithm='1' />" +
                "<layers>" +
                    "<layer id='0' value='4' />" +
                    "<layer id='1' value='3' />" +
                    "<layer id='2' value='2' />" +
                "</layers>" +
                "<weights num='18'><weight id='0' value='2.3052389622'/><weight id='1' value='-4.8575549126'/><weight id='2' value='2.6673409939'/><weight id='3' value='-9.3132295609'/><weight id='4' value='1.1571389437'/><weight id='5' value='7.6756539345'/><weight id='6' value='-1.3945209980'/><weight id='7' value='5.5473971367'/><weight id='8' value='-7.2331190109'/><weight id='9' value='6.9334201813'/><weight id='10' value='-4.9391288757'/><weight id='11' value='-8.5944976807'/><weight id='12' value='0.9358519912'/><weight id='13' value='4.5891571045'/><weight id='14' value='8.7754211426'/><weight id='15' value='0.6043789983'/><weight id='16' value='1.7954900265'/><weight id='17' value='-3.6644139290'/></weights>" +
            "</angn>\n";

        StringBuffer buffer = new StringBuffer();
        int cnt, tmp;
        for (cnt = 0; cnt < elements; cnt++) {
            tmp = elements-cnt+1;
            buffer.append(
                String.format(log, cnt,
                    0.3f*tmp, 0.2f*tmp, 0.1f*tmp,
                    cnt*0.1f, angnNode)
            );
        }

        String logAngn = String.format(root, buffer.toString());

        return logAngn;
    }

}
