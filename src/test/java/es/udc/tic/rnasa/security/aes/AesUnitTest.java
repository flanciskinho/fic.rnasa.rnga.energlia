/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.security.aes;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by flanciskinho on 29/4/16.
 */
public class AesUnitTest {

    private String salt  = "6530389635564f6464e8e3a47d593e19";
    private String key   = "s3cr3tp3ssw0rd!!!";
    private String plain = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

    private void testEncryptionDecryption(AesLengthConstants keyLength) throws Exception {
        Aes aes = new Aes(salt, key, keyLength);

        String en = aes.encrypt(plain);
        String de = aes.decrypt(en);

        assertThat(plain).isEqualTo(de);
    }

    @Test
    public void testEncryptionDecryption() throws Exception {
        testEncryptionDecryption(AesLengthConstants.AES_128);
        testEncryptionDecryption(AesLengthConstants.AES_192);
        testEncryptionDecryption(AesLengthConstants.AES_256);
    }

    @Test
    public void testDifferentObjects() throws Exception {
        int iterations = 10;
        Aes aes;
        List<String> en = new ArrayList<>();

        for (int cnt = 0; cnt < iterations; cnt++) {
            aes = new Aes(salt, key, AesLengthConstants.AES_256);
            en.add(aes.encrypt(plain));
        }

        for (String str: en) {
            aes = new Aes(salt, key, AesLengthConstants.AES_256);
            assertThat(plain).isEqualTo(aes.decrypt(str));
        }
    }

    @Test
    public void testProveWrongPassword() throws Exception {
        Aes aes = new Aes(salt, key, AesLengthConstants.AES_256);
        String en = aes.encrypt(plain);

        aes = new Aes(salt, key.replace(key.charAt(0), (char) (key.charAt(0)+1)), AesLengthConstants.AES_256);
        String de = aes.decrypt(en);

        assertThat(plain).isNotEqualTo(de);
    }

}
