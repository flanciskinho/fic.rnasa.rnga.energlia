/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;


// Created by flanciskinho on 27/5/16.

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.repository.AnalyzeTypeRepository;
import es.udc.tic.rnasa.web.rest.AnalyzeTypeResource;
import es.udc.tic.rnasa.web.rest.mapper.AnalyzeTypeMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Test class for the AnalyzeTypeService
 *
 * @see AnalyzeTypeService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class AnalyzeTypeServiceIntTest {

    @Inject
    private AnalyzeTypeService analyzeTypeService;

    @Inject
    private AnalyzeTypeMapper analyzeTypeMapper;

    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    @Before
    public void init() {
        AnalyzeTypeResource analyzeTypeResource = new AnalyzeTypeResource();
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeRepository", analyzeTypeRepository);
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeMapper",     analyzeTypeMapper);
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeService", analyzeTypeService);

    }

    @Test
    public void testCheckType() {
        assertThat(analyzeTypeService.getCLASSIFICATION().getId()).isEqualTo(1l);
        assertThat(analyzeTypeService.getREGRESSION().getId()).isEqualTo(2l);

        assertThat(analyzeTypeService.getCLASSIFICATION().getName()).isEqualTo("CLASSIFICATION");
        assertThat(analyzeTypeService.getREGRESSION().getName()).isEqualTo("REGRESSION");
    }

}
