/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.ApplicationRepository;
import es.udc.tic.rnasa.repository.CompiledAppRepository;
import es.udc.tic.rnasa.repository.SshAccountRepository;
import es.udc.tic.rnasa.repository.SshServerRepository;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.web.rest.ApplicationResource;
import es.udc.tic.rnasa.web.rest.CompiledAppResource;
import es.udc.tic.rnasa.web.rest.SshAccountResource;
import es.udc.tic.rnasa.web.rest.SshServerResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by flanciskinho on 26/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class CompiledAppServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(CompiledAppServiceIntTest.class);

    @Inject
    private CreateEntities createEntities;

    @Inject
    private SshProperties sshProperties;

    @Inject
    private CompiledAppService compiledAppService;
    @Inject
    private CompiledAppRepository compiledAppRepository;

    @Inject
    private ApplicationService applicationService;
    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private SshServerService sshServerService;
    @Inject
    private SshServerRepository sshServerRepository;

    @Inject
    private SshAccountService sshAccountService;
    @Inject
    private SshAccountRepository sshAccountRepository;

    @Before
    public void init() {
        CompiledAppResource compiledAppResource = new CompiledAppResource();
        ReflectionTestUtils.setField(compiledAppResource, "compiledAppService", compiledAppService);
        ReflectionTestUtils.setField(compiledAppService, "compiledAppRepository", compiledAppRepository);

        ApplicationResource applicationResource = new ApplicationResource();
        ReflectionTestUtils.setField(applicationResource, "applicationService", applicationService);
        ReflectionTestUtils.setField(applicationService, "applicationRepository", applicationRepository);

        SshServerResource sshServerResource = new SshServerResource();
        ReflectionTestUtils.setField(sshServerResource, "sshServerService", sshServerService);
        ReflectionTestUtils.setField(sshServerService, "sshServerRepository", sshServerRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
        ReflectionTestUtils.setField(sshAccountService, "sshAccountRepository", sshAccountRepository);
    }

    @PostConstruct
    public void setup() {

    }


    @Test
    public void sendCompiledApp() throws Exception {

        List<SshServer> sshServers = new ArrayList<>(2);
        sshServers.add(createEntities.createSshServer(sshProperties.getServer().getHost(), sshProperties.getServer().getFingerprint(), 5));
        sshServers.add(createEntities.createSshServer("pepiños.pruebas", 1));

        SshAccount sshAccount = createEntities.createSshAccount(sshProperties.getServer().getUsername().get(0), sshProperties.getServer().getKey(), sshServers.get(0));
        createEntities.createSshAccount(sshProperties.getServer().getUsername().get(1), sshProperties.getServer().getKey(), sshServers.get(0));

        createEntities.createSshAccount("pepiño", sshServers.get(1));

        List<Application> applications = new ArrayList<>(3);
        applications.add(createEntities.createApplication("app1", "0.0", ZonedDateTime.now(), true));
        applications.add(createEntities.createApplication("app1", "0.0", ZonedDateTime.now(), true));
        applications.add(createEntities.createApplication("app1", "0.0", ZonedDateTime.now(), false));

        createEntities.createCompiledApp(applications.get(0), sshServers.get(0));
        createEntities.createCompiledApp(applications.get(0), sshServers.get(1));

        CompiledApp compiledApp = createEntities.createCompiledApp(applications.get(1), sshServers.get(0));
        createEntities.createCompiledApp(applications.get(1), sshServers.get(1));

        createEntities.createCompiledApp(applications.get(2), sshServers.get(0));
        createEntities.createCompiledApp(applications.get(2), sshServers.get(1));

        CompiledApp result = compiledAppRepository.findFirstByUseIdAndBelongActivatedIsTrueOrderByBelongIdDesc(sshAccount.getBelong().getId());

        assertThat(result.equals(compiledApp)).isTrue();



        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;
        boolean isTrue = true;
        try {
            exec     = (ExecConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);
            transfer = (TransferConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.SFTP);

            compiledAppService.sendCompiledApp(sshAccount.getBelong(), exec, transfer);

            String pathname = String.format("%s/%d.out", applicationService.getAppPathname(), result.getId());
            isTrue = sshAccountService.exists(exec, pathname);

            exec.exec(String.format("rm -rf %s", pathname));

        } catch (Exception e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
            if (transfer != null)
                sshAccountService.closeConnection(transfer);

            throw e;
        }

        assertThat(isTrue).isTrue();
    }

}
