/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.ConnectionServer;

/**
 * Created by flanciskinho on 3/5/16.
 */

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.service.SshAccountService;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotOpenConnectionException;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotTransferConnectionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the ExecConnectionServer and TransferConnectionServer.
 *
 * @see es.udc.tic.rnasa.service.connectionServer
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class ConnectionServerIntTest {

    @Inject
    private SshProperties sshProperties;

    private final Logger log = LoggerFactory.getLogger(ConnectionServerIntTest.class);

    private String username = "";
    private String password = "";
    private String hostname = "";
    private String fingerprint = "";
    private int    port     = 0;

    private int    timeout  = 0;

    private String fakeFingerprint;

    @Before
    public void init() {
        ReflectionTestUtils.setField(new SshAccountService(), "sshProperties", sshProperties);
    }

    @PostConstruct
    public void setup() {
        username = sshProperties.getServer().getUsername().get(0);
        password = sshProperties.getServer().getKey();
        hostname = sshProperties.getServer().getHost();
        fingerprint = sshProperties.getServer().getFingerprint();
        port     = sshProperties.getServer().getPort();

        timeout  = sshProperties.getConfig().getTimeout();

        fakeFingerprint = new String(fingerprint);
        int cnt;
        for (cnt = 0; cnt < 9; cnt++)
            fakeFingerprint = fakeFingerprint.replace((char) ((int)'1' + cnt), '0');

        for (cnt = 0; cnt < 6; cnt++)
            fakeFingerprint = fakeFingerprint.replace((char) ((int)'a' + cnt), '0');
    }

    @Test(expected = CannotOpenConnectionException.class)
    public void testInvalidFingerprintExec() throws CannotOpenConnectionException {
        new ExecConnectionServer(username, password, hostname, fakeFingerprint, port, timeout).open();
    }

    @Test(expected = CannotOpenConnectionException.class)
    public void testInvalidFingerprintTransfer() throws CannotOpenConnectionException {
        new TransferConnectionServer(username, password, hostname, fakeFingerprint, port, timeout).open();
    }

    @Test(expected = CannotOpenConnectionException.class)
    public void testInvalidHostnameExec() throws CannotOpenConnectionException {
        new ExecConnectionServer(username, password, "0.0.0.0", fingerprint, port, timeout).open();
    }

    @Test(expected = CannotOpenConnectionException.class)
    public void testInvalidHostnameTransfer() throws CannotOpenConnectionException {
        new TransferConnectionServer(username, password, "0.0.0.0", fingerprint, port, timeout).open();
    }

    @Test(expected = CannotOpenConnectionException.class)
    public void testInvalidPort() throws CannotOpenConnectionException {
        new ExecConnectionServer(username, password, hostname, fingerprint, 0, timeout).open();
    }

    @Test(expected = CannotOpenConnectionException.class)
    public void testInvalidUsername() throws CannotOpenConnectionException {
        new ExecConnectionServer("", password, hostname, fingerprint, 0, timeout).open();
    }

    @Test(expected = CannotOpenConnectionException.class)
    public void testInvalidPassword() throws CannotOpenConnectionException {
        new ExecConnectionServer(username, password+hostname, hostname, fingerprint, 0, timeout).open();
    }

    @Test
    public void testExecCommands() throws CannotOpenConnectionException, CannotExecConnectionException{
        int max = 5;
        int cnt;
        List<String> commands = new ArrayList<>();
        String str1, str2;

        for (cnt = 0; cnt < max; cnt++) {
            commands.add(String.format("echo \"`whoami` %d\"", cnt));
        }

        log.debug("\n\n\nusername: {}\n password: {}\n hostname: {}\n fingerprint: {}\n port: {}\n timeout: {}\n\n\n",
            username, password, hostname, fingerprint, port, timeout);

        ExecConnectionServer ecs = new ExecConnectionServer(username, password, hostname, fingerprint, port, timeout);

        ecs.open();

        for (cnt = 0; cnt < max; cnt++) {
            str1 = ecs.exec(commands.get(cnt));
            str2 = String.format("%s %d\n", username, cnt);
            assertThat(str1).isEqualTo(str2);
        }

        ecs.close();
    }

    @Test
    public void testTransferFile() throws CannotOpenConnectionException, CannotTransferConnectionException, CannotExecConnectionException {
        int max = 5;
        int cnt;

        String pathname_msc = EnergliaApp.class.getCanonicalName()+"_f%d.txt";
        String content_msc  = "Content for file %d\n\t:D";

        List<String> pathnames = new ArrayList<>();
        String str;

        for (cnt = 0; cnt < max; cnt++) {
            pathnames.add(String.format(pathname_msc, cnt));
        }

        TransferConnectionServer tcs = new TransferConnectionServer(username, password, hostname, fingerprint, port, timeout);

        tcs.open();

        for (cnt = 0; cnt < max; cnt++) {
            tcs.sendData(String.format(content_msc, cnt), pathnames.get(cnt));

            str = tcs.receiveData(pathnames.get(cnt));

            assertThat(str).isEqualTo(String.format(content_msc, cnt));
        }

        tcs.close();


        ExecConnectionServer ecs = new ExecConnectionServer(username, password, hostname, fingerprint, port, timeout);
        ecs.open();
        for (cnt = 0; cnt < max; cnt++)
            ecs.exec("rm "+pathnames.get(cnt));

        ecs.close();
    }

}
