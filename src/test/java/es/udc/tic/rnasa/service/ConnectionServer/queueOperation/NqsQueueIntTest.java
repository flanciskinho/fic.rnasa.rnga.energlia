/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service.ConnectionServer.queueOperation;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.domain.SimulationStatusType;
import es.udc.tic.rnasa.service.SpecificSimulationService;
import es.udc.tic.rnasa.service.queueOperations.QueueOperation;
import es.udc.tic.rnasa.service.queueServer.NqsQueueService;
import es.udc.tic.rnasa.service.SimulationStatusTypeService;
import es.udc.tic.rnasa.service.SshAccountService;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.queueServer.QueueFactory;
import es.udc.tic.rnasa.web.rest.SimulationStatusTypeResource;
import es.udc.tic.rnasa.web.rest.SpecificSimulationResource;
import es.udc.tic.rnasa.web.rest.SshAccountResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by flanciskinho on 14/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class NqsQueueIntTest {

    private final Logger log = LoggerFactory.getLogger(NqsQueueIntTest.class);

    private final int TIME_FOR_REFRESH = 2000;

    @Inject
    private SshProperties sshProperties;

    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;

    @Inject
    private SpecificSimulationService specificSimulationService;

    @Inject
    private SshAccountService sshAccountService;
    @Inject
    private QueueFactory queueFactory;

    private QueueOperation nqsQueueService;

    private String username = "";
    private String password = "";
    private String hostname = "";
    private String fingerprint = "";
    private int    port     = 0;

    private int    timeout  = 0;

    private String script =
        "#!/bin/bash\n" +
            "sleep 120\n" +
            "echo \"hello `whoami`\"\n" +
            "echo \"you are on `hostname`\"\n";

    private String pathname = "energlia.test.script.sh";

    @Before
    public void init() {
        ReflectionTestUtils.setField(new SshAccountService(), "sshProperties", sshProperties);
        ReflectionTestUtils.setField(new SimulationStatusTypeResource(), "simulationStatusTypeService", simulationStatusTypeService);

        SpecificSimulationResource specificSimulationResource = new SpecificSimulationResource();
        ReflectionTestUtils.setField(specificSimulationResource, "specificSimulationService", specificSimulationService);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
        ReflectionTestUtils.setField(sshAccountService, "queueFactory", queueFactory);
    }

    @PostConstruct
    public void setup() {
        username = sshProperties.getServer().getUsername().get(0);
        password = sshProperties.getServer().getKey();
        hostname = sshProperties.getServer().getHost();
        fingerprint = sshProperties.getServer().getFingerprint();
        port     = sshProperties.getServer().getPort();

        timeout  = sshProperties.getConfig().getTimeout();
    }

    private ExecConnectionServer getExec() {
        ExecConnectionServer execConnectionServer = new ExecConnectionServer(username, password, hostname, fingerprint, port, timeout);

        return execConnectionServer;
    }

    private TransferConnectionServer getTransfer() {
        TransferConnectionServer transferConnectionServer = new TransferConnectionServer(username, password, hostname, fingerprint, port, timeout);

        return transferConnectionServer;
    }

    @Test
    public void getCurrentJobs() throws Exception {
        ExecConnectionServer exec = getExec();
        exec.open();

        nqsQueueService = queueFactory.getQueueOperation("NQS", exec);

        try {
            Thread.sleep(TIME_FOR_REFRESH);

            nqsQueueService.setExecConnectionServer(exec);

            long jobs = nqsQueueService.getCurrentJobs();

            exec.close();

            assertThat(jobs).isEqualTo(0l);
        } catch (Exception e) {
            exec.close();
            throw e;
        }
    }

    @Test
    public void launchJob() throws Exception {
        ExecConnectionServer exec = getExec();
        TransferConnectionServer transfer = getTransfer();
        transfer.open();
        exec.open();


        nqsQueueService = queueFactory.getQueueOperation("NQS", exec);

        try {
            transfer.sendData(script,pathname);

            nqsQueueService.setExecConnectionServer(exec);

            long nJob0 = nqsQueueService.getCurrentJobs();
            long jobid = nqsQueueService.launchJob(1, 2l, 1024l, "amd", pathname );

            Thread.sleep(TIME_FOR_REFRESH);
            long nJob1 = nqsQueueService.getCurrentJobs();

            Map<Long, SimulationStatusType> map = nqsQueueService.getStatus();

            exec.exec(String.format("qdel %d", jobid));
            exec.exec(String.format("rm %s", pathname));
            exec.exec(String.format("rm %s*%d", pathname, jobid));

            exec.close();
            transfer.close();

            assertThat(nJob0).isEqualTo(0l);
            assertThat(jobid).isGreaterThan(1);
            assertThat(nJob1).isEqualTo(1l);

            assertThat(map).hasSize(1);
            assertThat(map).containsKey(jobid);
            assertThat(map.get(jobid)).isEqualTo(simulationStatusTypeService.getWAITING_ON_QUEUE());

        } catch (Exception e) {
            transfer.close();
            exec.close();
            throw e;
        }
    }


}
