/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.DatasetCombinationRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.web.rest.DatasetCombinationResource;
import es.udc.tic.rnasa.web.rest.SshAccountResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by flanciskinho on 26/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class DatasetCombinationServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(DatasetCombinationServiceIntTest.class);

    @Inject
    private CreateEntities createEntities;

    @Inject
    private SshProperties sshProperties;

    @Inject
    private DatasetCombinationService datasetCombinationService;
    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private SshAccountService sshAccountService;


    private SshServer sshServer;
    private SshAccount sshAccount;

    private String loginname = "login";
    private String password  = "s3cr3t" ;

    @Before
    public void init() {
        DatasetCombinationResource datasetCombinationResource = new DatasetCombinationResource();
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationService", datasetCombinationService);
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationRepository", datasetCombinationRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
    }

    @PostConstruct
    public void setup() {

    }

    private boolean checkFile(ExecConnectionServer exec, DatasetCombination datasetCombination) throws CannotExecConnectionException {
        Long id = datasetCombination.getId();
        String pathname = String.format(datasetCombinationService.getDatasetPathname(), datasetCombination.getBelong().getId());
        if (!sshAccountService.exists(exec, String.format("%s/%d.train.in", pathname, id)))
            return false;
        if (!sshAccountService.exists(exec, String.format("%s/%d.train.out", pathname, id)))
            return false;

        if (datasetCombination.getValidationfilein() != null) {
            if (!sshAccountService.exists(exec, String.format("%s/%d.validation.in", pathname, id)))
                return false;
            if (!sshAccountService.exists(exec, String.format("%s/%d.validation.out", pathname, id)))
                return false;

            if (datasetCombination.getTrainfilein() != null) {
                if (!sshAccountService.exists(exec, String.format("%s/%d.test.in", pathname, id)))
                    return false;
                if (!sshAccountService.exists(exec, String.format("%s/%d.test.out", pathname, id)))
                    return false;
            }
        }

        return true;
    }

    @Test
    public void checkSendData() throws Exception {
        int comb = 5;
        Dataset dataset = createEntities.createDataset(10, 4, comb, null);

        sshServer  = createEntities.createSshServer(sshProperties.getServer().getHost(), sshProperties.getServer().getFingerprint(), 5);
        sshAccount = createEntities.createSshAccount(sshProperties.getServer().getUsername().get(0), sshProperties.getServer().getKey(), sshServer);

        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;
        boolean isTrue = true;
        try {

            exec     = (ExecConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);
            transfer = (TransferConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.SFTP);

            datasetCombinationService.sendData(dataset, exec, transfer);

            List<DatasetCombination> list = datasetCombinationRepository.findAllByBelongId(dataset.getId());
            assertThat(list).hasSize(comb);

            for (DatasetCombination dc : list) {
                if (!checkFile(exec, dc)) {
                    log.debug("dc false id: {}, belong: {}, index: {}", dc.getId(), dc.getBelong(), dc.getIndexCombination());
                    isTrue = false;
                }
            }

            exec.exec(String.format("rm -rf %s", String.format(datasetCombinationService.getDatasetPathname(), dataset.getId())));

        } catch (Exception e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
            if (transfer != null)
                sshAccountService.closeConnection(transfer);

            throw e;
        }

        assertThat(isTrue).isTrue();
    }
}
