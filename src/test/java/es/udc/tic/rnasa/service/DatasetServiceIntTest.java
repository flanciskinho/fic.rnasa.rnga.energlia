/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

// Created by flanciskinho on 14/4/16.

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.web.rest.*;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.AnalyzeTypeMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the PatternTypeService.
 *
 * @see PatternTypeService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class DatasetServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(DatasetServiceIntTest.class);

    @Inject
    private UserMapper userMapper;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    @Inject
    private AnalyzeTypeMapper analyzeTypeMapper;

    @Inject
    private AnalyzeTypeService analyzeTypeService;


    private UserDTO ownerDTO;
    private User owner;
    private AnalyzeType analyzeType;

    private List<byte[]> trainInput = new ArrayList<>();
    private List<byte[]> trainOutput = new ArrayList<>();
    private List<byte[]> validationInput = new ArrayList<>();
    private List<byte[]> validationOutput = new ArrayList<>();
    private List<byte[]> testInput = new ArrayList<>();
    private List<byte[]> testOutput = new ArrayList<>();

    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private DatasetService datasetService;

    @Inject
    private DatasetRepository datasetRepository;

    private static final String loginname = "joe";
    private static final String password  = "123456789012345678901234567890123456789012345678901234567890";

    @Before
    public void init() {
        DatasetResource datasetResource = new DatasetResource();
        ReflectionTestUtils.setField(datasetResource, "datasetService", datasetService);
        ReflectionTestUtils.setField(datasetResource, "datasetRepository", datasetRepository);

        DatasetCombinationResource datasetCombinationResource = new DatasetCombinationResource();
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationRepository", datasetCombinationRepository);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);

        AnalyzeTypeResource analyzeTypeResource = new AnalyzeTypeResource();
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeRepository", analyzeTypeRepository);
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeMapper",     analyzeTypeMapper);
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeService",    analyzeTypeService);
    }

    @PostConstruct
    public void setup() {
        datasetCombinationRepository.deleteAll();
        datasetRepository.deleteAll();

        ownerDTO = new UserDTO(
            loginname,                  // login
            password,
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin(loginname).isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }

        if (!analyzeTypeRepository.findOneByName("AnalyzeType").isPresent()) {
            analyzeType = new AnalyzeType();
            analyzeType.setName("AnalyzeType");
            analyzeType = analyzeTypeRepository.saveAndFlush(analyzeType);
        } else {
            analyzeType = analyzeTypeRepository.findOneByName("AnalyzeType").get();
        }

        //TRAIN
        trainInput.add("0.0000 0.1234 0.5678 0.8901\n1.0000 1.1234 1.5678 1.8901\n2.0000 2.1234 2.5678 2.8901".getBytes());
        trainInput.add("2.0000 2.1234 2.5678 2.8901\n3.0000 3.1234 3.5678 3.8901\n4.0000 4.1234 4.5678 4.8901".getBytes());
        trainInput.add("4.0000 4.1234 4.5678 4.8901\n6.0000 6.1234 6.5678 6.8901\n8.0000 8.1234 8.5678 8.8901\n0.0000 0.1234 0.5678 0.8901".getBytes());

        trainOutput.add("0.12 0.34\n1.12 1.34\n2.12 2.34".getBytes());
        trainOutput.add("2.12 2.34\n3.12 3.34\n4.12 4.34".getBytes());
        trainOutput.add("4.12 4.34\n6.12 6.34\n8.12 8.34\n0.12 0.34".getBytes());

        //VALIDATION
        validationInput.add("3.0000 3.1234 3.5678 3.8901\n4.0000 4.1234 4.5678 4.8901\n5.0000 5.1234 5.5678 5.8901".getBytes());
        validationInput.add("5.0000 5.1234 5.5678 5.8901\n6.0000 6.1234 6.5678 6.8901\n7.0000 7.1234 7.5678 7.8901\n8.0000 8.1234 8.5678 8.8901".getBytes());
        validationInput.add("2.0000 2.1234 2.5678 2.8901\n1.0000 1.1234 1.5678 1.8901\n3.0000 3.1234 3.5678 3.8901".getBytes());


        validationOutput.add("3.12 3.34\n4.12 4.34\n5.12 5.34".getBytes());
        validationOutput.add("5.12 5.34\n6.12 6.34\n7.12 7.34\n8.12 8.34".getBytes());
        validationOutput.add("2.12 2.34\n1.12 1.34\n3.12 3.34".getBytes());

        //TEST
        testInput.add("6.0000 6.1234 6.5678 6.8901\n7.0000 7.1234 7.5678 7.8901\n8.0000 8.1234 8.5678 8.8901\n9.0000 9.1234 9.5678 9.8901".getBytes());
        testInput.add("9.0000 9.1234 9.5678 9.8901\n0.0000 0.1234 0.5678 0.8901\n1.0000 1.1234 1.5678 1.8901".getBytes());
        testInput.add("5.0000 5.1234 5.5678 5.8901\n7.0000 7.1234 7.5678 7.8901\n9.0000 9.1234 9.5678 9.8901".getBytes());

        testOutput.add("6.12 6.34\n7.12 7.34\n8.12 8.34\n9.12 9.34".getBytes());
        testOutput.add("9.12 9.34\n0.12 0.34\n1.12 1.34".getBytes());
        testOutput.add("5.12 5.34\n7.12 7.34\n9.12 9.34".getBytes());

        // Authenticate
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.USER));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(loginname, password, authorities);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void testCheckMustBeAuthenticated() throws HttpExceptionDetails {
        SecurityContextHolder.clearContext();

        Dataset dataset = datasetService.createDataset(owner, "dataset", "description", analyzeType,
            trainInput, trainOutput,
            validationInput, validationOutput,
            testInput, testOutput);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckEmptyDataset() throws  Exception {
        datasetService.createDataset(null, "name", "description", analyzeType,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckEmptyDatasetTrainInput() throws  Exception {
        List<byte[]> list = new ArrayList<>(1);
        list.add("info".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            list, new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckEmptyDatasetTrainOutput() throws  Exception {
        List<byte[]> list = new ArrayList<>(1);
        list.add("info".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            new ArrayList<>(), list,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentNumberPatternsTrainInput() throws  Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0\n1".getBytes());
        trout.add("0\n1".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());
    }


    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentNumberPatternsTrainOutput() throws  Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0\n1 0".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0\n1".getBytes());
        trout.add("0".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentNumberPatternsTrainValiationInput() throws  Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0\n1 0".getBytes());

        List<byte[]> vain = new ArrayList<>(1);
        vain.add("1 0\n1 0".getBytes());
        vain.add("1 0".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0\n1".getBytes());
        trout.add("0\n1".getBytes());

        List<byte[]> vaout = new ArrayList<>(1);
        vaout.add("0\n1".getBytes());
        vaout.add("0\n1".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            vain, vaout,
            new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentNumberPatternsTrainValiationOutput() throws  Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0\n1 0".getBytes());

        List<byte[]> vain = new ArrayList<>(1);
        vain.add("1 0\n1 0".getBytes());
        vain.add("1 0\n1 0".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0\n1".getBytes());
        trout.add("0\n1".getBytes());

        List<byte[]> vaout = new ArrayList<>(1);
        vaout.add("0\n1".getBytes());
        vaout.add("0".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            vain, vaout,
            new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentNumberPatternsTrainValiationTestOutput() throws  Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0\n1 0".getBytes());

        List<byte[]> vain = new ArrayList<>(1);
        vain.add("1 0\n1 0".getBytes());
        vain.add("1 0\n1 0".getBytes());

        List<byte[]> tein = new ArrayList<>(1);
        vain.add("1 0\n1 0".getBytes());
        vain.add("1 0\n1 0".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0\n1".getBytes());
        trout.add("0\n1".getBytes());

        List<byte[]> vaout = new ArrayList<>(1);
        vaout.add("0\n1".getBytes());
        vaout.add("0\n1".getBytes());

        List<byte[]> teout = new ArrayList<>(1);
        vaout.add("0\n1".getBytes());
        vaout.add("0".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            vain, vaout,
            tein, teout);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentNumberOfFeaturesInput() throws Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0 2\n1 0".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0 0\n1".getBytes());
        trout.add("0 0\n1".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());

    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentNumberOfFeaturesOutput() throws Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0\n1 0".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0 0\n1".getBytes());
        trout.add("0\n1".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());

    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckDifferentPatterns() throws Exception {
        List<byte[]> trin = new ArrayList<>(1);
        trin.add("1 0\n1 0".getBytes());
        trin.add("1 0\n1 2".getBytes());

        List<byte[]> trout = new ArrayList<>(1);
        trout.add("0 0\n1 1".getBytes());
        trout.add("0 0\n1 1".getBytes());

        datasetService.createDataset(null, "name", "description", analyzeType,
            trin, trout,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());
    }


    @Test
    public void testCreateDatasetTrain() throws Exception {
        trainInput = new ArrayList<>();
        trainInput.add("0 1 2\n3 4 5".getBytes());
        trainInput.add("3 4 5\n0 1 2".getBytes());

        trainOutput = new ArrayList<>();
        trainOutput.add("0\n3".getBytes());
        trainOutput.add("3\n0".getBytes());

        Dataset dataset = datasetService.createDataset(owner, "dataset", "description", analyzeType,
            trainInput, trainOutput,
            new ArrayList<>(), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>());

        SecurityContextHolder.clearContext();

        Dataset testDataset = datasetRepository.findOne(dataset.getId());


        assertThat(dataset).isEqualTo(testDataset);

        datasetCombinationRepository.findAll().stream().forEach(
            dc -> {
                assertThat(dc.getTrainfilein()).isNotNull();
                assertThat(dc.getTrainfileout()).isNotNull();
                assertThat(dc.getValidationfilein()).isNull();
                assertThat(dc.getValidationfileout()).isNull();
                assertThat(dc.getTestfilein()).isNull();
                assertThat(dc.getTestfileout()).isNull();
                assertThat(dc.getIndexCombination()).isBetween(0,1);
            }
        );
    }

    @Test
    public void testCreateDatasetTrainValidation() throws Exception {
        trainInput = new ArrayList<>();
        trainInput.add("0 1 2\n3 4 5".getBytes());
        trainInput.add("6 7 8".getBytes());

        trainOutput = new ArrayList<>();
        trainOutput.add("0\n3".getBytes());
        trainOutput.add("6".getBytes());

        validationInput = new ArrayList<>();
        validationInput.add("6 7 8".getBytes());
        validationInput.add("3 4 5\n0 1 2".getBytes());

        validationOutput = new ArrayList<>();
        validationOutput.add("6".getBytes());
        validationOutput.add("3\n0".getBytes());

        Dataset dataset = datasetService.createDataset(owner, "dataset", "description", analyzeType,
            trainInput, trainOutput,
            validationInput, validationOutput,
            new ArrayList<>(), new ArrayList<>());

        SecurityContextHolder.clearContext();

        Dataset testDataset = datasetRepository.findOne(dataset.getId());

        assertThat(dataset).isEqualTo(testDataset);

        datasetCombinationRepository.findAll().stream().forEach(
            dc -> {
                assertThat(dc.getTrainfilein()).isNotNull();
                assertThat(dc.getTrainfileout()).isNotNull();
                assertThat(dc.getValidationfilein()).isNotNull();
                assertThat(dc.getValidationfileout()).isNotNull();
                assertThat(dc.getTestfilein()).isNull();
                assertThat(dc.getTestfileout()).isNull();
                assertThat(dc.getIndexCombination()).isBetween(0,1);
            }
        );
    }



    @Test
    public void testCreateDatasetTrainValidationTest() throws Exception {
        trainInput = new ArrayList<>();
        trainInput.add("0 1 2\n3 4 5".getBytes());
        trainInput.add("6 7 8".getBytes());

        trainOutput = new ArrayList<>();
        trainOutput.add("0\n3".getBytes());
        trainOutput.add("6".getBytes());

        validationInput = new ArrayList<>();
        validationInput.add("6 7 8".getBytes());
        validationInput.add("3 4 5".getBytes());

        validationOutput = new ArrayList<>();
        validationOutput.add("6".getBytes());
        validationOutput.add("3".getBytes());

        testInput = new ArrayList<>();
        testInput.add("9 10 11".getBytes());
        testInput.add("0 1 2\n9 10 11".getBytes());

        testOutput = new ArrayList<>();
        testOutput.add("9".getBytes());
        testOutput.add("0\n9".getBytes());

        Dataset dataset = datasetService.createDataset(owner, "dataset", "description", analyzeType,
            trainInput, trainOutput,
            validationInput, validationOutput,
            testInput, testOutput);

        SecurityContextHolder.clearContext();

        Dataset testDataset = datasetRepository.findOne(dataset.getId());

        assertThat(dataset).isEqualTo(testDataset);

        datasetCombinationRepository.findAll().stream().forEach(
            dc -> {
                assertThat(dc.getTrainfilein()).isNotNull();
                assertThat(dc.getTrainfileout()).isNotNull();
                assertThat(dc.getValidationfilein()).isNotNull();
                assertThat(dc.getValidationfileout()).isNotNull();
                assertThat(dc.getTestfilein()).isNotNull();
                assertThat(dc.getTestfileout()).isNotNull();
                assertThat(dc.getIndexCombination()).isBetween(0,1);
            }
        );

    }



    @Test
    public void testCheckNumClasses() throws HttpExceptionDetails {
        trainInput = new ArrayList<>();
        trainInput.add("0 1 2\n3 4 5".getBytes());
        trainInput.add("6 7 8".getBytes());

        trainOutput = new ArrayList<>();
        trainOutput.add("0\n0".getBytes());
        trainOutput.add("6".getBytes());

        validationInput = new ArrayList<>();
        validationInput.add("6 7 8".getBytes());
        validationInput.add("3 4 5".getBytes());

        validationOutput = new ArrayList<>();
        validationOutput.add("6".getBytes());
        validationOutput.add("0".getBytes());

        testInput = new ArrayList<>();
        testInput.add("9 10 11".getBytes());
        testInput.add("0 1 2\n9 10 11".getBytes());

        testOutput = new ArrayList<>();
        testOutput.add("9".getBytes());
        testOutput.add("0\n9".getBytes());

        Dataset dataset = datasetService.createDataset(owner, "dataset", "description", analyzeTypeService.getCLASSIFICATION(),
            trainInput, trainOutput,
            validationInput, validationOutput,
            testInput, testOutput);

        SecurityContextHolder.clearContext();

        Dataset testDataset = datasetRepository.findOne(dataset.getId());

        assertThat(dataset).isEqualTo(testDataset);

        assertThat(dataset.getNumClass()).isEqualTo(3);
    }

}
