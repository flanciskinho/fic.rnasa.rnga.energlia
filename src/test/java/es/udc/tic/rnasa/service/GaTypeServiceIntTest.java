/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

/**
 * Created by flanciskinho on 1/7/16.
 */

import es.udc.tic.rnasa.EnergliaApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Test class for the GaTypeService
 *
 * @see GaTypeService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional

public class GaTypeServiceIntTest {

    @Inject
    private GaTypeService gaTypeService;

    @Test
    public void testCheckType() {
        assertThat(gaTypeService.getSteadyState().getId()).isEqualTo(1l);
        assertThat(gaTypeService.getCooperativeCoevolutiveSteadyState().getId()).isEqualTo(2l);

        assertThat(gaTypeService.getSteadyState().getName()).isEqualTo("STEADY_STATE");
        assertThat(gaTypeService.getCooperativeCoevolutiveSteadyState().getName()).isEqualTo("COOPERATIVE_COEVOLUTIVE_STEADY_STATE");
    }
}
