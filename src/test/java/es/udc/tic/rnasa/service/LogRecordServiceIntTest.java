/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.service.util.ParseUtil;
import es.udc.tic.rnasa.web.rest.*;
import es.udc.tic.rnasa.web.rest.dto.ForLogRecordDTO.LogRecordPointDTO;
import org.apache.commons.lang.NotImplementedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by flanciskinho on 10/8/16.
 */


/**
 * Test class for the LogRecordService.
 *
 * @see LogRecordService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class LogRecordServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(LogRecordServiceIntTest.class);

    @Inject
    private CreateEntities createEntities;

    @Inject
    private SimulationService simulationService;

    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;

    @Inject
    private SpecificSimulationService specificSimulationService;
    @Inject
    private SpecificSimulationRepository specificSimulationRepository;

    @Inject
    private DatasetService datasetService;
    @Inject
    private DatasetRepository datasetRepository;

    @Inject
    private PopulationService populationService;
    @Inject
    private PopulationRepository populationRepository;

    @Inject
    private SshAccountService sshAccountService;

    @Inject
    private LogRecordService logRecordService;
    @Inject
    private LogRecordRepository logRecordRepository;

    @Inject
    private TechniqueParamService techniqueParamService;
    @Inject
    private TechniqueParamRepository techniqueParamRepository;


    @Before
    public void init() {
        DatasetResource datasetResource = new DatasetResource();
        ReflectionTestUtils.setField(datasetResource, "datasetService", datasetService);
        ReflectionTestUtils.setField(datasetService, "datasetRepository", datasetRepository);

        PopulationResource populationResource = new PopulationResource();
        ReflectionTestUtils.setField(populationResource, "populationService", populationService);
        ReflectionTestUtils.setField(populationService, "populationRepository", populationRepository);

        SimulationStatusTypeResource simulationStatusTypeResource = new SimulationStatusTypeResource();
        ReflectionTestUtils.setField(simulationStatusTypeResource, "simulationStatusTypeService", simulationStatusTypeService);

        SimulationResource simulationResource = new SimulationResource();
        ReflectionTestUtils.setField(simulationResource, "simulationService", simulationService);

        SpecificSimulationResource specificSimulationResource = new SpecificSimulationResource();
        ReflectionTestUtils.setField(specificSimulationResource, "specificSimulationService", specificSimulationService);
        ReflectionTestUtils.setField(specificSimulationService, "specificSimulationRepository", specificSimulationRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);

        LogRecordResource logRecordResource = new LogRecordResource();
        ReflectionTestUtils.setField(logRecordResource, "logRecordService", logRecordService);
        ReflectionTestUtils.setField(logRecordService, "logRecordRepository", logRecordRepository);

        TechniqueParamResource techniqueParamResource = new TechniqueParamResource();
        ReflectionTestUtils.setField(techniqueParamResource, "techniqueParamService", techniqueParamService);
        ReflectionTestUtils.setField(techniqueParamService, "techniqueParamRepository", techniqueParamRepository);

    }


    @PostConstruct
    public void setup() {
        datasetRepository.deleteAll();
        populationRepository.deleteAll();

        Locale.setDefault(Locale.ENGLISH);
    }

    private Simulation createSvmSimulation() throws Exception {
        return createSvmSimulation(1,1);
    }

    private Simulation createSvmSimulation(int combD, int combP) throws Exception {

        Population population = createEntities.createPopulation(10,0,combP);
        SshServer sshServer  = createEntities.createSshServer("sshserver1.energlia", 1);

        createEntities.createCompiledApp(
            createEntities.createApplication("proves", "0.0.1", ZonedDateTime.now(), true),
            sshServer);

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        Simulation simulation = createEntities.createNuSvmSimulation(
            "Simulation",
            createEntities.createDataset(3, 1, combD, null),
            populations,
            sshServer);

        return simulation;
    }

    private Simulation createAngnSimulation() throws Exception {
        int combD = 1;
        int combP = 1;

        Population population = createEntities.createPopulation(18,0,combP);
        SshServer sshServer  = createEntities.createSshServer("sshserver1.energlia", 1);

        createEntities.createCompiledApp(
            createEntities.createApplication("proves", "0.0.1", ZonedDateTime.now(), true),
            sshServer);

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        List<Integer> neurons = new ArrayList<>(3);
        neurons.add(4);
        neurons.add(3);
        neurons.add(2);

        Simulation simulation = createEntities.createAnnFixedGliaSimulation(
            "Simulation",
            neurons,
            createEntities.createDataset(neurons.get(0), neurons.get(neurons.size()-1), combD, null),
            populations,
            sshServer);

        return simulation;
    }

    @Test(expected = NotImplementedException.class)
    public void testReadSvmLogInvalidVersion() throws Exception {
        Simulation simulation = createSvmSimulation();

        SpecificSimulation specificSimulation = specificSimulationRepository.findAllByBelongId(simulation.getId()).get(0);
        specificSimulation.setStatus(simulationStatusTypeService.getFINISHED_FAILURE());
        specificSimulation.setLaunchBy(createEntities.createSshAccount("ulctifcs", simulation.getLaunch()));

        String logVersion = "1.0";
        String codeVersion = "1.1";
        String nodeVersion = "fakeVersion";
        int elements = 3;

        String resultContent = createEntities.getLogSvm(logVersion, codeVersion, nodeVersion, elements);
        Document d = ParseUtil.StringToXmlDocument(resultContent);

        logRecordService.saveLogs(specificSimulation, d);
    }

    @Test
    public void testReadSvmLog() throws Exception {
        Simulation simulation = createSvmSimulation();

        SpecificSimulation specificSimulation = specificSimulationRepository.findAllByBelongId(simulation.getId()).get(0);
        specificSimulation.setStatus(simulationStatusTypeService.getFINISHED_FAILURE());
        specificSimulation.setLaunchBy(createEntities.createSshAccount("ulctifcs", simulation.getLaunch()));

        String logVersion = "1.0";
        String codeVersion = "1.0";
        String nodeVersion = "1.0";
        int elements = 3;

        String resultContent = createEntities.getLogSvm(logVersion, codeVersion, nodeVersion, elements);
        log.debug("resultContent :{}", resultContent);
        Document d = ParseUtil.StringToXmlDocument(resultContent);

        logRecordService.saveLogs(specificSimulation, d);

        assertThat(logVersion).isEqualTo(specificSimulation.getLogVersion());
        assertThat(nodeVersion).isEqualTo(specificSimulation.getNodeVersion());

        List<LogRecord> logRecords = logRecordRepository.findAllByBelongId(specificSimulation.getId());
        assertThat(logRecords).hasSize(elements);

        List<TechniqueParam> techniqueParams = techniqueParamRepository.findAllByBelongBelongId(specificSimulation.getId());

        techniqueParams.stream().forEach(tp -> assertThat(logRecords).contains(tp.getBelong()));

        logRecords.stream().forEach(l -> {
            assertThat(l.getBelong()).isEqualTo(specificSimulation);
            assertThat(l.getIndexRecord().intValue()).isBetween(0, elements);
            assertThat(l.getId()).isNotNull();
            assertThat(l.getErrorTrain()).isNotNull();
            List<TechniqueParam> tps = techniqueParamRepository.findAllByBelongId(l.getId());
            assertThat(tps).isNotEmpty();
            assertThat((int) tps.stream().filter(tp -> tp.getName().startsWith("SV")).count()).isGreaterThan(0);
            assertThat((int) tps.stream().filter(tp -> tp.getName().equals("svm_type") ||tp.getName().equals("kernel_type")).count()).isEqualTo(2);
        });


    }

    @Test
    public void testReadAngnLog() throws Exception {
        Simulation simulation = createAngnSimulation();

        SpecificSimulation specificSimulation = specificSimulationRepository.findAllByBelongId(simulation.getId()).get(0);
        specificSimulation.setStatus(simulationStatusTypeService.getFINISHED_FAILURE());
        specificSimulation.setLaunchBy(createEntities.createSshAccount("ulctifcs", simulation.getLaunch()));

        String logVersion = "1.0";
        String codeVersion = "1.0";
        String nodeVersion = "1.0";
        int elements = 3;

        String resultContent = createEntities.getLogAngn(logVersion, codeVersion, nodeVersion, elements);
        log.debug("resultContent :{}", resultContent);
        Document d = ParseUtil.StringToXmlDocument(resultContent);

        logRecordService.saveLogs(specificSimulation, d);

        assertThat(logVersion).isEqualTo(specificSimulation.getLogVersion());
        assertThat(nodeVersion).isEqualTo(specificSimulation.getNodeVersion());

        List<LogRecord> logRecords = logRecordRepository.findAllByBelongId(specificSimulation.getId());
        assertThat(logRecords).hasSize(elements);

        List<TechniqueParam> techniqueParams = techniqueParamRepository.findAllByBelongBelongId(specificSimulation.getId());

        techniqueParams.stream().forEach(tp -> assertThat(logRecords).contains(tp.getBelong()));

        logRecords.stream().forEach(l -> {
            assertThat(l.getBelong()).isEqualTo(specificSimulation);
            assertThat(l.getIndexRecord().intValue()).isBetween(0, elements);
            assertThat(l.getId()).isNotNull();
            assertThat(l.getErrorTrain()).isNotNull();
            List<TechniqueParam> tps = techniqueParamRepository.findAllByBelongId(l.getId());
            assertThat(tps).isNotEmpty();

            assertThat((int) tps.stream().filter(tp -> tp.getName().startsWith("neuron")).count()).isGreaterThan(0);
            assertThat((int) tps.stream().filter(tp -> tp.getName().startsWith("layer")).count()).isGreaterThan(0);
            assertThat((int) tps.stream().filter(tp -> tp.getName().startsWith("weight")).count()).isGreaterThan(0);
        });


        techniqueParamRepository.findAll().stream().forEach(tp -> log.debug(">>>b {} n {} v {}", tp.getBelong().getId(), tp.getName(), tp.getValue()));

    }

    @Test
    public void testLogAverage() throws Exception {
        Simulation simulation = createSvmSimulation(3,1);

        List<SpecificSimulation> specificSimulations = specificSimulationRepository.findAllByBelongId(simulation.getId());


        int cnt;
        List<LogRecord> logRecords = new ArrayList();
        SpecificSimulation specificSimulation;

        specificSimulation = specificSimulations.get(0);
        for (cnt = 0; cnt < 1000; cnt += 10)
            logRecords.add(new LogRecord(new Long(cnt), cnt*10l, new Double(cnt), null, null, specificSimulation));
        specificSimulation = specificSimulations.get(1);
        for (cnt = 0; cnt < 500; cnt += 15)
            logRecords.add(new LogRecord(new Long(cnt), cnt*10l, new Double(cnt), null, null, specificSimulation));
        specificSimulation = specificSimulations.get(2);
        for (cnt = 0; cnt < 750; cnt += 20)
            logRecords.add(new LogRecord(new Long(cnt), cnt*10l, new Double(cnt), null, null, specificSimulation));

        logRecordRepository.save(logRecords);

        List<LogRecordPointDTO> points = logRecordService.getLogsAverage(simulation, 10);

        LogRecordPointDTO logRecordPointDTO;
        Double des;
        for (cnt = 0; cnt < points.size(); cnt++) {
            logRecordPointDTO = points.get(cnt);
            if (cnt > 0)
                assertThat(logRecordPointDTO.getIndexRecord()).isGreaterThan(points.get(cnt-1).getIndexRecord());

            if (logRecordPointDTO.getIndexRecord() <= 495) {
                assertThat(logRecordPointDTO.getErrorTrain()).isEqualTo(new Double(logRecordPointDTO.getIndexRecord()));
                des = 0.0;
            } else if (logRecordPointDTO.getIndexRecord() > 495 && logRecordPointDTO.getIndexRecord() <= 740) {
                assertThat(logRecordPointDTO.getErrorTrain()).isEqualTo((logRecordPointDTO.getIndexRecord()*2+495)/3.0);
                des = (Math.abs(logRecordPointDTO.getErrorTrain()-495) + 2 * Math.abs(logRecordPointDTO.getErrorTrain() - logRecordPointDTO.getIndexRecord()) ) /3.0;
            } else {
                assertThat(logRecordPointDTO.getErrorTrain()).isEqualTo((logRecordPointDTO.getIndexRecord()+740+495)/3.0);
                des = Math.abs(Math.abs(logRecordPointDTO.getErrorTrain() - 740) + Math.abs(logRecordPointDTO.getErrorTrain() - 495) + Math.abs(logRecordPointDTO.getErrorTrain() - logRecordPointDTO.getIndexRecord()) )/3.0;
            }
            assertThat(logRecordPointDTO.getErrorTrain() + des).isEqualTo(logRecordPointDTO.getDesTrainPlus());
            assertThat(logRecordPointDTO.getErrorTrain() - des).isEqualTo(logRecordPointDTO.getDesTrainSub());
        }

    }


}
