/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.PopulationCombinationRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.web.rest.PopulationCombinationResource;
import es.udc.tic.rnasa.web.rest.SshAccountResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by flanciskinho on 26/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class PopulationCombinationServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(PopulationCombinationServiceIntTest.class);

    @Inject
    private CreateEntities createEntities;

    @Inject
    private SshProperties sshProperties;

    @Inject
    private PopulationCombinationService populationCombinationService;
    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @Inject
    private SshAccountService sshAccountService;


    private SshServer sshServer;
    private SshAccount sshAccount;

    private String loginname = "login";
    private String password  = "s3cr3t" ;

    @Before
    public void init() {

        PopulationCombinationResource populationCombinationResource = new PopulationCombinationResource();
        ReflectionTestUtils.setField(populationCombinationResource, "populationCombinationService", populationCombinationService);
        ReflectionTestUtils.setField(populationCombinationService, "populationCombinationRepository", populationCombinationRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
    }

    @PostConstruct
    public void setup() {
        Locale.setDefault(Locale.ENGLISH);
    }

    private boolean checkFile(ExecConnectionServer exec, PopulationCombination populationCombination) throws CannotExecConnectionException {
        Long id = populationCombination.getId();
        String pathname = String.format(populationCombinationService.getPopulationPathname(), populationCombination.getBelong().getId());

        log.debug("pathname {}", String.format("%s/%d.xml", pathname, id));

        return sshAccountService.exists(exec, String.format("%s/%d.xml", pathname, id));
    }

    @Test
    public void checkSendData() throws Exception {
        int comb = 5;
        Population population = createEntities.createPopulation(10, 5, comb);

        sshServer  = createEntities.createSshServer(sshProperties.getServer().getHost(), sshProperties.getServer().getFingerprint(), 5);
        sshAccount = createEntities.createSshAccount(sshProperties.getServer().getUsername().get(0), sshProperties.getServer().getKey(), sshServer);

        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;
        boolean isTrue = true;
        try {
            exec     = (ExecConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);
            transfer = (TransferConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.SFTP);

            populationCombinationService.sendData(population, exec, transfer);

            List<PopulationCombination> list = populationCombinationRepository.findAllByBelongId(population.getId());
            assertThat(list).hasSize(comb);


            for (PopulationCombination pc : list) {
                if (!checkFile(exec, pc)) {
                    log.debug("dc false id: {}, belong: {}, index: {}", pc.getId(), pc.getBelong(), pc.getIndexCombination());
                    isTrue = false;
                }
            }

            exec.exec(String.format("rm -rf %s", String.format(populationCombinationService.getPopulationPathname(), population.getId())));

        } catch (Exception e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);
            if (transfer != null)
                sshAccountService.closeConnection(transfer);

            throw e;
        }

        assertThat(isTrue).isTrue();
    }
}
