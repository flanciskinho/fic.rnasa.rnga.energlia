/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.exceptions.HttpExceptionDetails;
import es.udc.tic.rnasa.service.exceptions.NoXmlAttributeException;
import es.udc.tic.rnasa.web.rest.PopulationResource;
import es.udc.tic.rnasa.web.rest.UserResource;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * Created by flanciskinho on 25/4/16.
 *
 * @see PopulationService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class PopulationServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(PopulationServiceIntTest.class);

    @Inject
    private UserMapper userMapper;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    private UserDTO ownerDTO;
    private User owner;

    private static final String loginname = "joe";
    private static final String password  = "123456789012345678901234567890123456789012345678901234567890";


    @Inject
    private PopulationService populationService;

    @Inject
    private PopulationRepository populationRepository;

    @Inject
    private SizeParamRepository sizeParamRepository;

    @Inject
    private SizeTypeService sizeTypeService;

    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @Before
    public void init() {
        Locale.setDefault(Locale.ENGLISH);

        PopulationResource populationResource = new PopulationResource();
        ReflectionTestUtils.setField(populationResource, "populationService", populationService);
        ReflectionTestUtils.setField(populationService, "populationRepository", populationRepository);
        ReflectionTestUtils.setField(populationService, "populationCombinationRepository", populationCombinationRepository);
        ReflectionTestUtils.setField(populationService, "sizeParamRepository", sizeParamRepository);
        ReflectionTestUtils.setField(populationService, "sizeTypeService", sizeTypeService);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userService", userService);
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);
    }

    @PostConstruct
    public void setup() {
        populationRepository.deleteAll();
        populationCombinationRepository.deleteAll();
        sizeParamRepository.deleteAll();


        ownerDTO = new UserDTO(
            loginname,                  // login
            password,
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin(loginname).isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }


        // Authenticate
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.USER));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(loginname, password, authorities);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void testCheckMustBeAuthenticated() throws Exception {
        // sign out
        SecurityContextHolder.clearContext();

        populationService.createPopulation(owner, "name", "desc", new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckEmptyPopulation() throws Exception {
        populationService.createPopulation(null, "name", "desc", new ArrayList<>());
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckEmptyFile() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("".getBytes());

        populationService.createPopulation(null, "name", "desc", list);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckBadXmlFormat() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("<root><alskd></root>".getBytes());

        populationService.createPopulation(null, "name", "desc", list);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckIncorrectFile() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("<root></root>".getBytes());

        populationService.createPopulation(null, "name", "desc", list);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckNoVersionNoEncode() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("<population></population>".getBytes());

        populationService.createPopulation(null, "name", "desc", list);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckNoEncode() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("<population version='1.0'></population>".getBytes());

        populationService.createPopulation(null, "name", "desc", list);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckNoVersion() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("<population encode='1'></population>".getBytes());

        populationService.createPopulation(null, "name", "desc", list);
    }

    @Test(expected = HttpExceptionDetails.class)
    public void testCheckNoNumIndividuals() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("<population version='1.0' encode='1'></population>".getBytes());

        populationService.createPopulation(null, "name", "desc", list);
    }

    @Test(expected = org.apache.commons.lang3.NotImplementedException.class)
    public void testCheckNoIndividuals() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add("<population version='1.0' encode='1' num_individuals='0' s_genobool='0'></population>".getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = org.apache.commons.lang3.NotImplementedException.class)
    public void testCheckIndividualNoVersionNoEncode() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='1' num_individuals='1' s_genobool='0'>" +
                "<individual />" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = org.apache.commons.lang3.NotImplementedException.class)
    public void testCheckIndividualBoolNoVersion() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='1' num_individuals='1' s_genobool='0'>" +
                "<individual encode='1'/>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = org.apache.commons.lang3.NotImplementedException.class)
    public void testCheckIndividualRealNoVersion() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='2' num_individuals='1' s_genoshort='0' s_genofloat='0'>" +
                "<individual encode='2'/>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = org.apache.commons.lang3.NotImplementedException.class)
    public void testCheckIndividualNoEncode() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='1' num_individuals='1' s_genobool='0'>" +
                "<individual version='1.0'/>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = NoXmlAttributeException.class)
    public void testCheckGenoBoolNoIdNoValue() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='1' num_individuals='1' s_genobool='0'>" +
                "<individual version='1.0' encode='1'>" +
                    "<genobool />" +
                "</individual>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = NoXmlAttributeException.class)
    public void testCheckGenoBoolNoId() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='1' num_individuals='1' s_genobool='0'>" +
                "<individual version='1.0' encode='1'>" +
                    "<genobool value='1'/>" +
                "</individual>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = NoXmlAttributeException.class)
    public void testCheckGenoBoolNoValue() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='1' num_individuals='1' s_genobool='0'>" +
                "<individual version='1.0' encode='1'>" +
                    "<genobool id='1'/>" +
                "</individual>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = NoXmlAttributeException.class)
    public void testCheckGenoRealNoIdNoValue() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='2' num_individuals='1' s_genofloat='1' s_genoshort='0'>" +
                "<individual version='1.0' encode='2'>" +
                    "<genofloat />" +
                "</individual>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = NoXmlAttributeException.class)
    public void testCheckGenoRealNoId() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='2' num_individuals='1' s_genofloat='1' s_genoshort='0'>" +
                "<individual version='1.0' encode='2'>" +
                    "<genofloat value='1'/>" +
                "</individual>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    @Test(expected = NoXmlAttributeException.class)
    public void testCheckGenoRealNoValue() throws Exception {
        List<byte []> list = new ArrayList<>(1);
        list.add((
            "<population version='1.0' encode='2' num_individuals='1' s_genofloat='1' s_genoshort='0'>" +
                "<individual version='1.0' encode='2'>" +
                    "<genofloat id='1'/>" +
                "</individual>" +
            "</population>").getBytes());

        populationService.createPopulation(owner, "name", "desc", list);
    }

    private String getBinaryPopulationv1_0(List<Map<Integer, Boolean>> individuals) {
        String frame = "<population version='1.0' encode='1' num_individuals='%d' s_genobool='%d'>\n%s</population>";
        String ind = "\t<individual version='1.0' encode='1'>\n%s\t</individual>\n";
        String gen = "\t\t<genobool id='%d' value='%d' />\n";

        StringBuffer str1 = new StringBuffer();


        individuals.stream().forEach(individual -> {
            StringBuffer str2 = new StringBuffer();
            individual.entrySet().stream().forEach( genobool -> {
                str2.append(String.format(gen, genobool.getKey(), genobool.getValue()?1:0));
            });
            str1.append(String.format(ind, str2.toString()));

        });

        return String.format(frame, individuals.size(), individuals.size()*individuals.get(0).size(), str1.toString());
    }

    private String getRealPopulationv1_0(List<Map<Integer, Short>> i_short,List<Map<Integer, Float>> i_float) {
        String frame = "<population version='1.0' encode='2' num_individuals='%d' s_genoshort='%d' s_genofloat='%d'>\n%s</population>";
        String ind = "\t<individual version='1.0' encode='2'>\n%s\t</individual>\n";
        String g_short = "\t\t<genoshort id='%d' value='%d' />\n";
        String g_float = "\t\t<genofloat id='%d' value='%f' />\n";

        StringBuffer str1 = new StringBuffer();
        StringBuffer str2;

        for (int index = 0; index < Math.max(i_short.size(), i_float.size()); index++) {
            str2 = new StringBuffer();


            if (index < i_short.size())
                for (Map.Entry<Integer, Short> entry : i_short.get(index).entrySet()) {
                    str2.append(String.format(g_short, entry.getKey(), entry.getValue()));
                }

            if (index < i_float.size())
                for (Map.Entry<Integer, Float> entry: i_float.get(index).entrySet()) {
                    str2.append(String.format(g_float, entry.getKey(), entry.getValue()));
                }

            str1.append(String.format(ind, str2.toString()));
        }


        int numIndividuals = Math.max(i_short.size(), i_float.size());
        return String.format(frame,
            numIndividuals,
            numIndividuals*(i_short.size() == 0?0:i_short.get(0).size()),
            numIndividuals*(i_float.size()== 0?0:i_float.get(0).size()),
            str1.toString());
    }

    private void checkOnePopulation(Population population, byte [] file, List<Map<Integer, Boolean>> i_bool, List<Map<Integer, Short>> i_short, List<Map<Integer, Float>> i_float) {
        assertThat(population).isEqualTo(populationRepository.findOne(population.getId()));

        List<PopulationCombination> lpc = populationCombinationRepository.findAll();
        assertThat(lpc).hasSize(1);

        PopulationCombination pc = lpc.get(0);
        assertThat(pc.getBelong()).isEqualTo(population);
        assertThat(pc.getFile()).isEqualTo(file);
        assertThat(pc.getIndexCombination()).isEqualTo(0);

    }



    public List<Map<Integer, Short>> getShortIndividual(int sizeIndividual, int sizeGeno, int startValue) {
        int cnt, tmp;

        List<Map<Integer, Short>> i_short = new ArrayList<>();
        Map<Integer, Short> m_short;

        for (tmp = 0; tmp < sizeIndividual; tmp++) {
            m_short = new HashMap();
            for (cnt = 0; cnt < sizeGeno; cnt++)
                m_short.put(cnt, (short) (cnt + tmp * sizeGeno + startValue));
            i_short.add(m_short);
        }

        return i_short;
    }

    public List<Map<Integer, Float>> getFloatIndividual(int sizeIndividual, int sizeGeno, int startValue) {
        int cnt, tmp;

        List<Map<Integer, Float>> i_float = new ArrayList<>();
        Map<Integer, Float> m_float;

        for (tmp = 0; tmp < sizeIndividual; tmp++) {
            m_float = new HashMap();
            for (cnt = 0; cnt < sizeGeno; cnt++)
                m_float.put(cnt, ((float) (cnt + tmp * sizeGeno + startValue) + 0.5f) * (cnt % 2 == 0? -1.0f: 1.0f) );
            i_float.add(m_float);
        }

        return i_float;
    }

    @Test
    public void testCheckGenoBool() throws Exception {
        int cnt, tmp;
        int numIndividuals = 3;
        int sizeGenoBool   = 5;
        List<Map<Integer, Boolean>> individuals = new ArrayList<>();
        Map<Integer, Boolean> m;
        for (cnt = 0; cnt < numIndividuals; cnt++) {
            m = new HashMap();
            for (tmp = 0; tmp < sizeGenoBool; tmp++) {
                m.put(tmp, tmp % 2 == 0);
            }
            individuals.add(m);
        }

        List<byte []> list = new ArrayList<>(1);
        list.add(getBinaryPopulationv1_0(individuals).getBytes());

        list.stream().forEach(f -> log.debug("file[{}]:\n{}", list.indexOf(f), new String(f)));

        Population pop = populationService.createPopulation(owner, "name", "desc", list);

        checkOnePopulation(pop, list.get(0), individuals, new ArrayList<>(), new ArrayList<>());
    }

    @Test
    public void testCheckGenoFloat() throws Exception {
        List<Map<Integer, Short>> i_short = new ArrayList<>();
        List<Map<Integer, Float>> i_float = getFloatIndividual(3, 7, 0);

        List<byte []> list = new ArrayList<>(1);
        list.add(getRealPopulationv1_0(i_short, i_float).getBytes());

        list.stream().forEach(f -> log.debug("file[{}]:\n{}", list.indexOf(f), new String(f)));

        Population pop = populationService.createPopulation(owner, "name", "desc", list);

        checkOnePopulation(pop, list.get(0), new ArrayList<>(), i_short, i_float);
    }

    @Test
    public void testCheckGenoShort() throws Exception {
        List<Map<Integer, Short>> i_short = getShortIndividual(3, 7, 0);
        List<Map<Integer, Float>> i_float = new ArrayList<>();

        List<byte []> list = new ArrayList<>(1);
        list.add(getRealPopulationv1_0(i_short, i_float).getBytes());

        list.stream().forEach(f -> log.debug("file[{}]:\n{}", list.indexOf(f), new String(f)));

        Population pop = populationService.createPopulation(owner, "name", "desc", list);

        checkOnePopulation(pop, list.get(0), new ArrayList<>(), i_short, i_float);
    }

    @Test
    public void testCheckGenoReal() throws Exception {
        int numIndividuals = 3;
        int sizeGenoShort  = 10;
        int sizeGenoFloat  = 7;

        List<Map<Integer, Short>> i_short = getShortIndividual(numIndividuals, sizeGenoShort, 0);
        List<Map<Integer, Float>> i_float = getFloatIndividual(numIndividuals, sizeGenoFloat, 0);

        List<byte []> list = new ArrayList<>(1);
        list.add(getRealPopulationv1_0(i_short, i_float).getBytes());

        list.stream().forEach(f -> log.debug("file[{}]:\n{}", list.indexOf(f), new String(f)));

        Population pop = populationService.createPopulation(owner, "name", "desc", list);

        checkOnePopulation(pop, list.get(0), new ArrayList<>(), i_short, i_float);
    }

    private void saveSizeParam(Population belong, Integer size, SizeType type) {
        if (size == null || size == 0)
            return;

        SizeParam sizeParam = new SizeParam();
        sizeParam.setBelong(belong);
        sizeParam.setSize(size);
        sizeParam.setUse(type);

        sizeParamRepository.saveAndFlush(sizeParam);
    }

    private void checkFindBySize(int ss, int sf, int sb, int numPopulations) throws HttpExceptionDetails{
        final int total = ss + sf + sb;

        Pageable pageRequest = new PageRequest(0, 100);

        Page<Population> page = populationService.findAllBySize(pageRequest, ss, sf, sb);
//        page.forEach(p -> log.debug("\n\n\t{}: {}", p.getId(), p.getName()));
        assertThat(page).hasSize(numPopulations);
        page.forEach(p -> assertThat(p.getSizeGenotype()).isEqualTo(total));
    }

    @Test
    public void testFindBySize() throws Exception {
        SecurityContextHolder.clearContext();

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(loginname, password, authorities);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        int [] s_short = {30,  0,  0,  5,  5,  4,  3,  9, 11, 11, 10,  9,  3,  3, 70};
        int [] s_float = {40, 70, 70,  5,  0,  1,  2,  2,  0,  0,  0,  1,  7,  7, 70};
        int [] s_bool  = { 0,  0,  0,  5,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 70};
        int cnt, size;

        String template = "p%2d size = %3d (%2d,%2d,%2d)";
        Population population;


        // create populations
        for (cnt = 0; cnt < s_short.length; cnt++) {
            size = s_short[cnt] + s_float[cnt] + s_bool[cnt];

            population = new Population();
            population.setName(String.format(template, cnt, size, s_short[cnt], s_float[cnt], s_bool[cnt]));
            population.setTimestamp(ZonedDateTime.now());
            population.setNumIndividual (100+cnt);
            population.setNumCombination(1000+cnt);
            population.setSizeGenotype(size);

            population = populationRepository.saveAndFlush(population);

            // short
            saveSizeParam(population, s_short[cnt], sizeTypeService.getSHORT());
            saveSizeParam(population, s_float[cnt], sizeTypeService.getREAL());
            saveSizeParam(population, s_bool [cnt], sizeTypeService.getBINARY());
        }

        checkFindBySize(  0, 70,  0, 2);
        checkFindBySize( 11,  0,  0, 2);
        checkFindBySize(  3,  7,  0, 2);
        checkFindBySize( 70, 70, 70, 1);
        checkFindBySize(  5,  5,  5, 1);
    }

    @Test
    public void testFindBySizeWithShare() throws Exception {
        SecurityContextHolder.clearContext();

        String loginname = "pepe";
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.USER));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(loginname, password, authorities);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        int [] s_short = { 0,  0};
        int [] s_float = {70, 70};
        int [] s_bool  = { 0,  0};
        int cnt, size;

        String template = "p%2d size = %3d (%2d,%2d,%2d)";
        List<Population> populations = new ArrayList<>();
        Population population;

        // create populations
        for (cnt = 0; cnt < s_short.length; cnt++) {
            size = s_short[cnt] + s_float[cnt] + s_bool[cnt];

            population = new Population();
            population.setName(String.format(template, cnt, size, s_short[cnt], s_float[cnt], s_bool[cnt]));
            population.setTimestamp(ZonedDateTime.now());
            population.setNumIndividual (100+cnt);
            population.setNumCombination(1000+cnt);
            population.setSizeGenotype(size);

            populations.add(populationRepository.saveAndFlush(population));

            // short
            saveSizeParam(population, s_short[cnt], sizeTypeService.getSHORT());
            saveSizeParam(population, s_float[cnt], sizeTypeService.getREAL());
            saveSizeParam(population, s_bool [cnt], sizeTypeService.getBINARY());
        }

        User user1 = userService.createUserInformation(loginname, password, "Pepe", "Pepiño", "Pepe@localhost", "en");
        User user2 = userService.createUserInformation("alba", password, "Alba", "Albiña", "alba@localhost", "en");
        user1.setActivated(true);
        user2.setActivated(true);
        userRepository.saveAndFlush(user1);
        userRepository.saveAndFlush(user2);

        population = populations.get(0);
        population.setOwner(user1);
        populationRepository.saveAndFlush(population);

        population = populations.get(1);
        population.setOwner(user2);
        Set<User> set = population.getShares();
        set.add(user1);
        population.setShares(set);
        populationRepository.saveAndFlush(population);

        int ss = 0;
        int sf = 70;
        int sb = 0;

        final int total = ss + sf + sb;

        Pageable pageRequest = new PageRequest(0, 100);

        Page<Population> page = populationService.findAllBySize(pageRequest, ss, sf, sb);
        assertThat(page).hasSize(2);
        page.forEach(p -> assertThat(p.getSizeGenotype()).isEqualTo(total));


    }


}
