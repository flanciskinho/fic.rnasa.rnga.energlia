/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.DatasetCombinationRepository;
import es.udc.tic.rnasa.repository.PopulationCombinationRepository;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.exceptions.CannotExecConnectionException;
import es.udc.tic.rnasa.web.rest.DatasetCombinationResource;
import es.udc.tic.rnasa.web.rest.PopulationCombinationResource;
import es.udc.tic.rnasa.web.rest.SimulationResource;
import es.udc.tic.rnasa.web.rest.SshAccountResource;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by flanciskinho on 26/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class SimulationServiceIntTest {
    private final Logger log = LoggerFactory.getLogger(PopulationCombinationServiceIntTest.class);

    @Inject
    private CreateEntities createEntities;

    @Inject
    private SshProperties sshProperties;

    @Inject
    private PopulationCombinationService populationCombinationService;
    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @Inject
    private DatasetCombinationService datasetCombinationService;
    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private SshAccountService sshAccountService;

    @Inject
    private SimulationService simulationService;

    private SshServer sshServer;
    private SshAccount sshAccount;

    @Before
    public void init() {
        DatasetCombinationResource datasetCombinationResource = new DatasetCombinationResource();
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationService", datasetCombinationService);
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationRepository", datasetCombinationRepository);

        PopulationCombinationResource populationCombinationResource = new PopulationCombinationResource();
        ReflectionTestUtils.setField(populationCombinationResource, "populationCombinationService", populationCombinationService);
        ReflectionTestUtils.setField(populationCombinationService, "populationCombinationRepository", populationCombinationRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);

        SimulationResource simulationResource = new SimulationResource();
        ReflectionTestUtils.setField(simulationResource, "simulationService", simulationService);
    }

    @PostConstruct
    public void setup() {
        Locale.setDefault(Locale.ENGLISH);
    }

    private boolean checkDir(ExecConnectionServer exec, String pathname) throws CannotExecConnectionException {
        boolean result = sshAccountService.exists(exec, pathname);
        if (!result)
            log.debug("Not found {}", pathname);

        exec.exec(String.format("rm -rf %s", pathname));


        return result;
    }

    @Test
    public void createDir() throws Exception {
        int combP = 5;
        int combD = 3;

        Population population = createEntities.createPopulation(10, 0, combP);
        Dataset dataset = createEntities.createDataset(3, 1, combD, null);

        sshServer  = createEntities.createSshServer(sshProperties.getServer().getHost(), sshProperties.getServer().getFingerprint(), 5);
        sshAccount = createEntities.createSshAccount(sshProperties.getServer().getUsername().get(0), sshProperties.getServer().getKey(), sshServer);

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        Simulation simulation = createEntities.createNuSvmSimulation("svm", dataset, populations, sshServer);

        assertThat(populationCombinationRepository.findAllByBelongId(population.getId())).hasSize(combP);
        assertThat(datasetCombinationRepository.findAllByBelongId(dataset.getId())).hasSize(combD);

        CompiledApp compiledApp = createEntities.createCompiledApp(
            createEntities.createApplication("app", "0.0.0", ZonedDateTime.now(), true),
            sshServer);

        ExecConnectionServer exec = null;
        boolean isTrue = true;
        try {
            exec = (ExecConnectionServer) sshAccountService.openConnection(sshAccount.getId(), ChannelType.EXEC);

            simulationService.createDirSimulation(simulation, sshAccount);

            if (!checkDir(exec, String.format(simulationService.getScriptPathname(), simulation.getId())))
                isTrue = false;
            if (!checkDir(exec, String.format(simulationService.getConfigPathname(), simulation.getId())))
                isTrue = false;
            if (!checkDir(exec, String.format(simulationService.getResultPathname(), simulation.getId())))
                isTrue = false;


            exec.exec("rm -rf .energlia");

        } catch (Exception e) {
            if (exec != null)
                sshAccountService.closeConnection(exec);

            throw e;
        }

        AssertionsForClassTypes.assertThat(isTrue).isTrue();
    }
}
