/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.TransferConnectionServer;
import es.udc.tic.rnasa.web.rest.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Test class for the SpecificSimulationService.
 *
 * @see SpecificSimulationService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class SpecificSimulationServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(SshAccountServiceIntTest.class);

    @Inject
    private CreateEntities createEntities;

    @Inject
    private SshServerService sshServerService;
    @Inject
    private SshServerRepository sshServerRepository;

    @Inject
    private SshAccountService sshAccountService;
    @Inject
    private SshAccountRepository sshAccountRepository;

    @Inject
    private DatasetService datasetService;
    @Inject
    private DatasetRepository datasetRepository;

    @Inject
    private SimulationService simulationService;

    @Inject
    private PopulationService populationService;
    @Inject
    private PopulationRepository populationRepository;

    @Inject
    private SpecificSimulationService specificSimulationService;
    @Inject
    private SpecificSimulationRepository specificSimulationRepository;

    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;

    private static final String loginname = "login";
    private static final String password = "s3cr3t";

    private static final String DEFAULT_FINGERPRINT = "76:21:24:40:16:9f:55:c9:56:2a:05:f9:18:fc:41:47";
//    private static final String PUB_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCx8f99PJ04K+Mjc32QEU47UnHYbFL3BFCiUEUS/aCNSnSXEbYUvrrYDdyAnqk7K+1CMSlzTtTHDj57PATa6QeuNCyVQX/nI2i2CNRqHxH0p2ZzXlGSsvbQIYm1vYlFVI9x8qQVV8vddgX6B6mB8E1YjocsqbUEq1LNiKCrbQ9s/BdCyxgyclCCgqcNGaxITyH9u88OOwLXOfXbdHbB6O9an71mCoFgmG7gH+K7YvyEwltwd8Ewm0/QWGlZ4ZnrCTavUUH5KVelQ4om3tp/ev6yGlFqZlhlqwQLafN81WuKbca7Py6+hyNMAyAJ16nz2MlJczDcsmTITvdbTLBBsfm3 flanciskinho@rnasa-H55M-S2V";
    private static final String DEFAULT_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
        "MIIEpAIBAAKCAQEAsfH/fTydOCvjI3N9kBFOO1Jx2GxS9wRQolBFEv2gjUp0lxG2\n" +
        "FL662A3cgJ6pOyvtQjEpc07Uxw4+ezwE2ukHrjQslUF/5yNotgjUah8R9Kdmc15R\n" +
        "krL20CGJtb2JRVSPcfKkFVfL3XYF+gepgfBNWI6HLKm1BKtSzYigq20PbPwXQssY\n" +
        "MnJQgoKnDRmsSE8h/bvPDjsC1zn123R2wejvWp+9ZgqBYJhu4B/iu2L8hMJbcHfB\n" +
        "MJtP0FhpWeGZ6wk2r1FB+SlXpUOKJt7af3r+shpRamZYZasEC2nzfNVrim3Guz8u\n" +
        "vocjTAMgCdep89jJSXMw3LJkyE73W0ywQbH5twIDAQABAoIBAQCkew4wEvE4VMjV\n" +
        "ohE0M+reRqzY4fgmQCTQg+X1Ud8v+tyeL07hChPnVZi2gvHmsgnTH2IzJXDktjPm\n" +
        "8WmDd9cTnUKAjFTPsBk/+hVuKl4MiRfYGBzKvwYJCJckGJhkL2+QYjptSAQ3JrsL\n" +
        "jD2Q0DCOZOBp1kxujnYxadMp4dtaXnf67GVg5hjKZ0wvhfRvwyPF4+hb2TNBmHYz\n" +
        "AmkaiZ4Myzmw5jHo93FNrELEdvXUcpHA56XnR71AgUCOu+/c8w1cSSQynkTHMRF3\n" +
        "H8LJz6qluNY0mjTPxaBiN4KukEOuqQf719DWMltqxJWbR0AChR+Ba/JBkEbLkWLF\n" +
        "3f1GLYeRAoGBANjAJPRzqQnFTxsXL/6bmYX6MmvNsP536x+kS+CHz6a2iytSr1Sn\n" +
        "c9ayec1VCymDAWgSmrK98pQfqj0KtJv9ggnPZrtso9X6d9fA07PykPokAdPS8hjm\n" +
        "52zWk5Iwepapuja86adMSMKJ5NoMAB8Aa07rbcOKen5s5onTy2/4SUv7AoGBANIq\n" +
        "951jX2AKv04Y+qzl9L0dmGOINGt9oEwUYoHe6HRekQGOSiqA/+dPEcjSh1kdqQW+\n" +
        "Sz2Y4VMuZFl+RrM54PVqgAELIz0ZXUDA3NWgDB6MGmr9x5hGL2CS6oaP+6dNFfLP\n" +
        "P9SE6tdEKM2LP09XLB6Gx1UKd8LPGNFd0RvBwsB1AoGAKNeD3yAQIQwxzLwAyiwN\n" +
        "sKYlbBTVHg4AmvS9a3CL2zEYZjBtYzkuZa05sEwvD0JlOHQQ/E84b1rMHDZxM9d/\n" +
        "8lNHW1esQ3yvqlLmUUkKsmeohH6CKdkQkHyaT/ickNkognn0WRbRuv56xe9u8miE\n" +
        "z9ki1K66SCpDAoOLGO0i+pMCgYAus5OQaruP2POHxC836mWG/KOXQnX7iM/s9/6n\n" +
        "w0O+UCFCyj6a0U2MNdrK2FVxLREZEJ8hke5IEKAF7vSA8RSXFFJbaQEsm41ITVMF\n" +
        "OWpYA2P/cGNA6B+xAxSQUEBofc2pxfn2HI8exKuYeR/ZXpKUOfjjYc8Y//IQ3BsS\n" +
        "Yu0G3QKBgQCYskPm2sRgsdKfFBkE15QmUZo0qoXSDxxEkBVPkuA7Q3kLJTqvor9b\n" +
        "Qw49ySJxakyGJC5FxSAsoAs8QiJDKc1GjxJnpFkrjXvki5l5dJGhZcNdsgbMCglb\n" +
        "nMm8KywoCg5cqLsSRsymrJUplH/2Lvn2r5Odn6++B/Lm/otVITbD8w==\n" +
        "-----END RSA PRIVATE KEY-----";

    @Before
    public void init() {

        SshServerResource sshServerResource = new SshServerResource();
        ReflectionTestUtils.setField(sshServerResource, "sshServerService", sshServerService);
        ReflectionTestUtils.setField(sshServerService, "sshServerRepository", sshServerRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
        ReflectionTestUtils.setField(sshAccountService, "sshAccountRepository", sshAccountRepository);

        DatasetResource datasetResource = new DatasetResource();
        ReflectionTestUtils.setField(datasetResource, "datasetService", datasetService);
        ReflectionTestUtils.setField(datasetService, "datasetRepository", datasetRepository);

        PopulationResource populationResource = new PopulationResource();
        ReflectionTestUtils.setField(populationResource, "populationService", populationService);
        ReflectionTestUtils.setField(populationService, "populationRepository", populationRepository);

        SimulationResource simulationResource = new SimulationResource();
        ReflectionTestUtils.setField(simulationResource, "simulationService", simulationService);

        SpecificSimulationResource specificSimulationResource = new SpecificSimulationResource();
        ReflectionTestUtils.setField(specificSimulationResource, "specificSimulationService", specificSimulationService);
        ReflectionTestUtils.setField(specificSimulationService, "specificSimulationRepository", specificSimulationRepository);

        SimulationStatusTypeResource simulationStatusTypeResource = new SimulationStatusTypeResource();
        ReflectionTestUtils.setField(simulationStatusTypeResource, "simulationStatusTypeService", simulationStatusTypeService);
    }

    @PostConstruct
    public void setup() {
        datasetRepository.deleteAll();
        populationRepository.deleteAll();

        Locale.setDefault(Locale.ENGLISH);
    }


    @Test
    public void testJobOnQueue() throws Exception {
        datasetRepository.deleteAll();
        populationRepository.deleteAll();
        specificSimulationRepository.deleteAll();

        int combD = 3;
        int combP = 4;

        Dataset dataset       = createEntities.createDataset(7, 1, combD, null);
        Population population = createEntities.createPopulation(10,0,combP);
        SshServer sshServer1  = createEntities.createSshServer("sshserver1.energlia", DEFAULT_FINGERPRINT, 2);
        sshServer1.setMaxJob(7);

        sshServerRepository.saveAndFlush(sshServer1);

        List<SshAccount> sshAccountList1 = new ArrayList<>();
        sshAccountList1.add(createEntities.createSshAccount("ulctifcs", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctinvb", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctiefb", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctilpr", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctiapp", DEFAULT_KEY, sshServer1));

        createEntities.createCompiledApp(
            createEntities.createApplication("proves", "0.0.1", ZonedDateTime.now(), true),
            sshServer1);

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        int maxS = 2;
        int cnt;

        List<Simulation> simulations = new ArrayList<>(maxS);
        for (cnt = 0; cnt < maxS; cnt++)
            simulations.add(createEntities.createNuSvmSimulation("s"+cnt, dataset, populations, sshServer1));

        simulations.stream().forEach(simulation -> log.debug("simulation {} launchByServer {}", simulation, simulation.getLaunch()));

        specificSimulationService.launchSpecificSimulation();
        log.debug("launchSpecificSimulations");

        ExecConnectionServer exec = null;
        TransferConnectionServer transfer = null;

        int sizePage = combD*combP*maxS;
        Page<SpecificSimulation> page =  specificSimulationRepository.findAllByStatusIdOrderById(simulationStatusTypeService.getWAITING_ON_QUEUE().getId(), new PageRequest(0, sizePage));

        page.iterator().forEachRemaining(ss -> {
            if (ss.getId() % 2 == 0) {
                ss.setStatus(simulationStatusTypeService.getRUNNING());
            }
        });

        List<Long> statusOnQueue = new ArrayList<>(2);
        statusOnQueue.add(simulationStatusTypeService.getRUNNING().getId());
        statusOnQueue.add(simulationStatusTypeService.getWAITING_ON_QUEUE().getId());

        sizePage = 12;
        List<SpecificSimulation> tmp = specificSimulationRepository.findAllByStatusIdIn(statusOnQueue, new PageRequest(0, sizePage));

        String pathname = null;
        cnt = 0;
        try {
            for (SpecificSimulation ss: tmp) {
                exec = (ExecConnectionServer) sshAccountService.openConnection(ss.getLaunchBy().getId(), ChannelType.EXEC);
                transfer = (TransferConnectionServer) sshAccountService.openConnection(ss.getLaunchBy().getId(), ChannelType.SFTP);
log.debug("qdel {}", ss.getJobid());
                exec.exec(String.format("qdel %d", ss.getJobid()));

                pathname = String.format("%s/%d.xml", String.format(simulationService.getResultPathname(), ss.getBelong().getId()), ss.getId());
log.debug("create file {}", pathname);
                if (cnt % 2 == 0) {
                    if (cnt % 4 == 0) {
                        transfer.sendData("<logs>Invalid xml file<logs>",pathname);
                    }
                } else {
                    transfer.sendData(createEntities.getLogSvm(), pathname);
                }

                sshAccountService.closeConnection(exec);
                sshAccountService.closeConnection(transfer);
                exec = null;
                cnt++;
            }
        } catch (Exception e) {
            log.debug("Exception e {}", e.getMessage());
            if (exec == null)
                sshAccountService.closeConnection(exec);
            if (transfer == null)
                sshAccountService.closeConnection(transfer);
        }

        specificSimulationService.checkJobOnQueue();

        specificSimulationRepository.findAll().stream().forEach(ss -> log.debug("ss id {}, jobid {} launch {} status {}", ss.getId(), ss.getJobid(), ss.getLaunchBy() == null?"null":ss.getLaunchBy().getUsername(), ss.getStatus()==null?"null":ss.getStatus().getName()));

        assertThat((int) specificSimulationRepository.findAll().stream().filter(ss -> ss.getStatus().equals(simulationStatusTypeService.getFINISHED_FAILURE())).count()).isEqualTo(sizePage/2);
        assertThat((int) specificSimulationRepository.findAll().stream().filter(ss -> ss.getStatus().equals(simulationStatusTypeService.getFINISHED_SUCCESS())).count()).isEqualTo(sizePage/2);

        deleteJobsOnCpd(specificSimulationRepository.findAll());
    }

    @Test
    public void testUpdateStatus() throws Exception {
        int combD = 3;
        int combP = 4;

        Dataset dataset       = createEntities.createDataset(7, 1, combD, null);
        Population population = createEntities.createPopulation(10,0,combP);
        SshServer sshServer1  = createEntities.createSshServer("sshserver1.energlia", DEFAULT_FINGERPRINT, 2);
        SshServer sshServer2  = createEntities.createSshServer("sshserver2.energlia", DEFAULT_FINGERPRINT, 4);

        List<SshAccount> sshAccountList1 = new ArrayList<>();
        List<SshAccount> sshAccountList2 = new ArrayList<>();
        // Configure ssh server and add users
        sshServer1.setMaxJob(5);
        sshServer2.setMaxJob(10);
        sshServerRepository.saveAndFlush(sshServer1);
        sshServerRepository.saveAndFlush(sshServer2);

        sshAccountList1.add(createEntities.createSshAccount("ulctifcs", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctinvb", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctiefb", DEFAULT_KEY, sshServer1));

        sshAccountList2.add(createEntities.createSshAccount("ulctilpr", DEFAULT_KEY, sshServer2));
        sshAccountList2.add(createEntities.createSshAccount("ulctiapp", DEFAULT_KEY, sshServer2));

        Application app = createEntities.createApplication("proves", "0.0.1", ZonedDateTime.now(), true);
        createEntities.createCompiledApp(app, sshServer1);
        createEntities.createCompiledApp(app, sshServer2);

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        int maxS = 2;
        int cnt;

        List<Simulation> simulations = new ArrayList<>(maxS);
        for (cnt = 0; cnt < maxS; cnt++)
            simulations.add(createEntities.createNuSvmSimulation("s"+cnt, dataset, populations, (cnt % 2 == 0? sshServer1: sshServer2)));

        simulations.stream().forEach(simulation -> log.debug("simulation {} launchByServer {}", simulation, simulation.getLaunch()));

        specificSimulationService.launchSpecificSimulation();

        int pageSize = 15;
        List<SpecificSimulation> tmp = new ArrayList();
        specificSimulationRepository.findAllByStatusIdOrderById(simulationStatusTypeService.getRUNNING().getId(), new PageRequest(0, pageSize)).iterator().forEachRemaining(tmp::add);

        assertThat(tmp).hasSize(0);

        tmp = new ArrayList<>(pageSize);
        specificSimulationRepository.findAllByStatusIdOrderById(simulationStatusTypeService.getWAITING_ON_QUEUE().getId(), new PageRequest(0, pageSize)).iterator().forEachRemaining(tmp::add);

        tmp.sort((o1, o2) -> o1.getLaunchBy().getId().compareTo(o2.getLaunchBy().getId()));


            ExecConnectionServer exec = null;
            try {
                exec = (ExecConnectionServer) sshAccountService.openConnection(tmp.get(0).getLaunchBy().getId(), ChannelType.EXEC);

                for (SpecificSimulation ss: tmp) {
                    exec.exec(String.format("qdebug %d %c", ss.getJobid(), 'r'));
                }

                sshAccountService.closeConnection(exec);
                exec = null;
            } catch (Exception e) {
                log.debug("Exception e {}", e.getMessage());
                if (exec == null)
                    sshAccountService.closeConnection(exec);
            }


        specificSimulationService.checkSimulationStatus();

        tmp = new ArrayList<>();
        specificSimulationService.findAllByStatusIdOrderById(simulationStatusTypeService.getRUNNING().getId(), new PageRequest(0, pageSize*2)).iterator().forEachRemaining(tmp::add);

        assertThat(tmp).hasSize(pageSize);

        tmp = new ArrayList();
        specificSimulationRepository.findAll().iterator().forEachRemaining(tmp::add);
        setToFinish(tmp);
    }

    @Test
    public void testVirtualQueue() throws Exception {
        int combD = 4;
        int combP = 5;

        Dataset dataset       = createEntities.createDataset(7, 1, combD, null);
        Population population = createEntities.createPopulation(10,0,combP);
        SshServer sshServer1  = createEntities.createSshServer("sshserver1.energlia", DEFAULT_FINGERPRINT, 2);
        SshServer sshServer2  = createEntities.createSshServer("sshserver2.energlia", DEFAULT_FINGERPRINT, 4);

        List<SshAccount> sshAccountList1 = new ArrayList<>();
        List<SshAccount> sshAccountList2 = new ArrayList<>();
        // Configure ssh server and add users
        sshServer1.setMaxJob(3);
        sshServer2.setMaxJob(7);
        sshServerRepository.saveAndFlush(sshServer1);
        sshServerRepository.saveAndFlush(sshServer2);

        sshAccountList1.add(createEntities.createSshAccount("ulctifcs", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctinvb", DEFAULT_KEY, sshServer1));
        sshAccountList1.add(createEntities.createSshAccount("ulctiefb", DEFAULT_KEY, sshServer1));

        sshAccountList2.add(createEntities.createSshAccount("ulctilpr", DEFAULT_KEY, sshServer2));
        sshAccountList2.add(createEntities.createSshAccount("ulctiapp", DEFAULT_KEY, sshServer2));

        Application app = createEntities.createApplication("proves", "0.0.1", ZonedDateTime.now(), true);
        createEntities.createCompiledApp(app, sshServer1);
        createEntities.createCompiledApp(app, sshServer2);

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        int maxS = 3;
        int cnt;

        List<Simulation> simulations = new ArrayList<>(maxS);
        for (cnt = 0; cnt < maxS; cnt++)
            simulations.add(createEntities.createNuSvmSimulation("s"+cnt, dataset, populations, (cnt % 2 == 0? sshServer1: sshServer2)));

        simulations.stream().forEach(simulation -> log.debug("simulation {} launchByServer {}", simulation, simulation.getLaunch()));

        Long s12 [] = {simulations.get(0).getId(), simulations.get(1).getId()};


        // de la simulacion 1,  9 de los 20 trabajos se envián a la cola
        // de la simulation 2, 14 de los 20 trabajos se envián a la cola
        specificSimulationService.launchSpecificSimulation();

        Page<SpecificSimulation> specificSimulations = specificSimulationRepository.findAllByStatusIdOrderById(simulationStatusTypeService.getWAITING_ON_QUEUE().getId(), new PageRequest(0, maxS*combD*combP));

        assertThat(specificSimulations.getNumberOfElements()).isEqualTo(3*3+2*7);
        specificSimulations.forEach(
            specificSimulation -> {
                assertThat(s12).contains(specificSimulation.getBelong().getId());
            }
        );

        checkSimulation(simulations.get(0).getId(), 11,  9,  0);
        checkSimulation(simulations.get(1).getId(),  6, 14,  0);
        checkSimulation(simulations.get(2).getId(), 20,  0,  0);

        // marcamos de la simulacion 2, 2 trabajos de usuarios distintos los marcamos como acabados
        // marcamos de la simulacion 1, 1 trabajo del usuario 2 y 2 trabajos del usuario 3 como acabados
        List<SpecificSimulation> toFinish = new ArrayList<>();
        List<SpecificSimulation> aux = specificSimulationRepository.findAllByBelongId(simulations.get(1).getId());

        setToFinish(aux.stream().filter(ss -> ss.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(ss -> ss.getLaunchBy().equals(sshAccountList2.get(0)))
            .limit(1).collect(Collectors.toList()));
        setToFinish(aux.stream().filter(ss -> ss.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(ss -> ss.getLaunchBy().equals(sshAccountList2.get(1)))
            .limit(1).collect(Collectors.toList()));

        setToFinish(specificSimulationRepository.findAllByBelongId(simulations.get(0).getId()).stream()
            .filter(specificSimulation -> specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(specificSimulation -> specificSimulation.getLaunchBy().equals(sshAccountList1.get(1)))
            .limit(1).collect(Collectors.toList()));
        setToFinish(specificSimulationRepository.findAllByBelongId(simulations.get(0).getId()).stream()
            .filter(specificSimulation -> specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(specificSimulation -> specificSimulation.getLaunchBy().equals(sshAccountList1.get(2)))
            .limit(2).collect(Collectors.toList()));

        setToFinish(toFinish);


        checkSimulation(simulations.get(0).getId(), 11,  6,  3);
        checkSimulation(simulations.get(1).getId(),  6, 12,  2);
        checkSimulation(simulations.get(2).getId(), 20,  0,  0);


        // De la simulation 1 se enviaran los trabajos 10, 11 y 12 a la cola
        // De la simulation 2 se enviaran los trabajos 35 y 36 a la cola
        specificSimulationService.launchSpecificSimulation();

        specificSimulations = specificSimulationRepository.findAllByStatusIdOrderById(simulationStatusTypeService.getWAITING_ON_QUEUE().getId(), new PageRequest(0, maxS*combD*combP));

        assertThat(specificSimulations.getNumberOfElements()).isEqualTo(3*3+2*7);
        specificSimulations.forEach(
            specificSimulation -> {
                assertThat(s12).contains(specificSimulation.getBelong().getId());
            }
        );

        checkSimulation(simulations.get(0).getId(),  8,  9,  3);
        checkSimulation(simulations.get(1).getId(),  4, 14,  2);
        checkSimulation(simulations.get(2).getId(), 20,  0,  0);

        // Marcamos todos los trabajos de la simulacion 1 de los usuarios 2 y 3 para acabar
        // Marcamos todos los trabajos de la simulacion 2 para acabar
        toFinish = new ArrayList<>();
        for (SpecificSimulation ss :specificSimulationRepository.findAllByBelongId(simulations.get(0).getId())) {
            if (!ss.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
                continue;
            if (ss.getLaunchBy().equals(sshAccountList1.get(0)))
                continue;
            toFinish.add(ss);
        }
        for (SpecificSimulation ss :specificSimulationRepository.findAllByBelongId(simulations.get(1).getId()))
            if (ss.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
                toFinish.add(ss);


        setToFinish(toFinish);

        checkSimulation(simulations.get(0).getId(),  8,  3,  9);
        checkSimulation(simulations.get(1).getId(),  4,  0, 16);
        checkSimulation(simulations.get(2).getId(), 20,  0,  0);

        // De la simulation 1 se enviaran los trabajos 13-18 a la cola
        // De la simulation 2 se enviaran los trabajos 27-28 y 39-40 a la cola
        specificSimulationService.launchSpecificSimulation();

        specificSimulations = specificSimulationRepository.findAllByStatusIdOrderById(simulationStatusTypeService.getWAITING_ON_QUEUE().getId(), new PageRequest(0, maxS*combD*combP));

        assertThat(specificSimulations.getNumberOfElements()).isEqualTo(3*3+4);
        specificSimulations.forEach(
            specificSimulation -> {
                assertThat(s12).contains(specificSimulation.getBelong().getId());
            }
        );

        checkSimulation(simulations.get(0).getId(),  2,  9,  9);
        checkSimulation(simulations.get(1).getId(),  0,  4, 16);
        checkSimulation(simulations.get(2).getId(), 20,  0,  0);

        // De la simulacion 1 marcamos como acabados 1 trabajo del usuario 1
        // De la simulacion 1 marcamos como acabados 2 trabajo del usuario 2
        // De la simulacion 1 marcamos como acabados 2 trabajo del usuario 3
        // De la simulacion 2 marcamos como acabados 2 trabajo del usuario 1
        setToFinish(specificSimulationRepository.findAllByBelongId(simulations.get(0).getId()).stream()
            .filter(specificSimulation -> specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(specificSimulation -> specificSimulation.getLaunchBy().equals(sshAccountList1.get(0)))
            .limit(1).collect(Collectors.toList()));
        setToFinish(specificSimulationRepository.findAllByBelongId(simulations.get(0).getId()).stream()
            .filter(specificSimulation -> specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(specificSimulation -> specificSimulation.getLaunchBy().equals(sshAccountList1.get(1)))
            .limit(2).collect(Collectors.toList()));
        setToFinish(specificSimulationRepository.findAllByBelongId(simulations.get(0).getId()).stream()
            .filter(specificSimulation -> specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(specificSimulation -> specificSimulation.getLaunchBy().equals(sshAccountList1.get(2)))
            .limit(2).collect(Collectors.toList()));
        setToFinish(specificSimulationRepository.findAllByBelongId(simulations.get(1).getId()).stream()
            .filter(specificSimulation -> specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE()))
            .filter(specificSimulation -> specificSimulation.getLaunchBy().equals(sshAccountList2.get(0)))
            .limit(2).collect(Collectors.toList()));

        checkSimulation(simulations.get(0).getId(),  2,  4, 14);
        checkSimulation(simulations.get(1).getId(),  0,  2, 18);
        checkSimulation(simulations.get(2).getId(), 20,  0,  0);

        // De la simulation 1 se enviaran los trabajos 19-20 a la cola
        // De la simulation 3 se enviaran los trabajos 41-44 a la cola
        specificSimulationService.launchSpecificSimulation();

        specificSimulations = specificSimulationRepository.findAllByStatusIdOrderById(simulationStatusTypeService.getWAITING_ON_QUEUE().getId(), new PageRequest(0, maxS*combD*combP));

        Long s123 [] = {simulations.get(0).getId(), simulations.get(1).getId(), simulations.get(2).getId()};

        assertThat(specificSimulations.getNumberOfElements()).isEqualTo(3*3+2);
        specificSimulations.forEach(
            specificSimulation -> {
                assertThat(s123).contains(specificSimulation.getBelong().getId());
            }
        );

        checkSimulation(simulations.get(0).getId(),  0,  6, 14);
        checkSimulation(simulations.get(1).getId(),  0,  2, 18);
        checkSimulation(simulations.get(2).getId(), 17,  3,  0);


        List<SshAccount> accounts = new ArrayList<>(sshAccountList1.size() + sshAccountList2.size());
        for (SshAccount account: sshAccountList1)
            accounts.add(account);
        for (SshAccount account: sshAccountList2)
            accounts.add(account);

        accounts.stream().forEach(ss -> {
            ExecConnectionServer tmpExec = null;
            try {
                tmpExec = (ExecConnectionServer) sshAccountService.openConnection(ss.getId(), ChannelType.EXEC);
                tmpExec.exec("qdel -u "+ss.getUsername());
                tmpExec.exec("rm -rf .energlia");
                sshAccountService.closeConnection(tmpExec);
            } catch (Exception e) {
                if (tmpExec != null)
                    sshAccountService.closeConnection(tmpExec);
            }
        });

        List<SpecificSimulation> tmp = new ArrayList();
        specificSimulationRepository.findAll().iterator().forEachRemaining(tmp::add);
        setToFinish(tmp);

    }

    private void checkSimulation(Long id, int forQueue, int onQueue, int finish) {
        List<SpecificSimulation> list = specificSimulationRepository.findAllByBelongId(id);

        assertThat(list.stream().filter(specificSimulation ->
            specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_FOR_QUEUE())
        )).hasSize(forQueue);

        assertThat(list.stream().filter(specificSimulation ->
            specificSimulation.getStatus().equals(simulationStatusTypeService.getWAITING_ON_QUEUE())
        )).hasSize(onQueue);

        assertThat(list.stream().filter(specificSimulation ->
            specificSimulation.getStatus().equals(simulationStatusTypeService.getFINISHED_SUCCESS()) ||
            specificSimulation.getStatus().equals(simulationStatusTypeService.getFINISHED_FAILURE())
        )).hasSize(finish);
    }

    private void deleteJobsOnCpd(List<SpecificSimulation> list) {
        list.stream().forEach(specificSimulation -> {
            ExecConnectionServer exec = null;

            try {
                exec = (ExecConnectionServer) sshAccountService.openConnection(specificSimulation.getLaunchBy().getId(), ChannelType.EXEC);
                exec.exec("qdel "+specificSimulation.getJobid());

                log.debug("job {} (ss.id={}) toDelete qdel {}", specificSimulation.getJobid() , specificSimulation.getId(), specificSimulation.getJobid());

                exec.exec(String.format("rm %s/%d.xml", String.format(simulationService.getResultPathname(), specificSimulation.getBelong().getId()), specificSimulation.getId()));

                sshAccountService.closeConnection(exec);
            } catch (Exception e) {
                if (exec != null)
                    sshAccountService.closeConnection(exec);
            }
        });
    }

    private void setToFinish(List<SpecificSimulation> list) {
        list.stream().forEach(specificSimulation -> {
            ExecConnectionServer exec = null;
            String pathname;
            try {
                exec = (ExecConnectionServer) sshAccountService.openConnection(specificSimulation.getLaunchBy().getId(), ChannelType.EXEC);
                exec.exec("qdel "+specificSimulation.getJobid());

                log.debug("specificSimulation {} setToFinish qdel {}", specificSimulation.getId(), specificSimulation.getJobid());

                pathname = String.format("%s/%d.xml", String.format(simulationService.getResultPathname(), specificSimulation.getBelong().getId()), specificSimulation.getId());
                exec.exec("rm "+pathname);

                sshAccountService.closeConnection(exec);
            } catch (Exception e) {
                if (exec != null)
                    sshAccountService.closeConnection(exec);
            }

            try {
                specificSimulation.setStatus(simulationStatusTypeService.getFINISHED_SUCCESS());
                specificSimulation.getLaunchBy().setNumJob(specificSimulation.getLaunchBy().getNumJob() - 1);
                specificSimulationRepository.saveAndFlush(specificSimulation);
                sshAccountRepository.saveAndFlush(specificSimulation.getLaunchBy());
            } catch (Exception e) { }
        });
    }



}
