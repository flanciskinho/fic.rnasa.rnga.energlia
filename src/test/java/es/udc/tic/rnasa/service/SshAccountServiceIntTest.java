/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.service;

/**
 * Created by flanciskinho on 17/5/16.
 */

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.config.ssh.SshProperties;
import es.udc.tic.rnasa.domain.SshAccount;
import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.repository.ArchTypeRepository;
import es.udc.tic.rnasa.repository.SshAccountRepository;
import es.udc.tic.rnasa.repository.SshServerRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.connectionServer.ChannelType;
import es.udc.tic.rnasa.service.connectionServer.ConnectionServer;
import es.udc.tic.rnasa.service.connectionServer.ExecConnectionServer;
import es.udc.tic.rnasa.service.exceptions.TooMuchConnectionException;
import es.udc.tic.rnasa.web.rest.SshAccountResource;
import es.udc.tic.rnasa.web.rest.SshServerResource;
import es.udc.tic.rnasa.web.rest.dto.SshAccountDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Test class for the SshAccountService.
 *
 * @see SshAccountService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class SshAccountServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(SshAccountServiceIntTest.class);

    private static final Integer DEFAULT_MAX_CONNECTION = 3;

    @Inject
    private SshProperties sshProperties;

    @Inject
    private ArchTypeRepository archTypeRepository;

    @Inject
    private SshServerService sshServerService;

    @Inject
    private SshServerRepository sshServerRepository;

    @Inject
    private SshAccountService sshAccountService;

    @Inject
    private SshAccountRepository sshAccountRepository;


    private SshServer sshServer;
    private SshAccountDTO sshAccountDTO;
    private SshAccount sshAccount;

    private String loginname = "login";
    private String password  = "s3cr3t" ;

    @Before
    public void init() {
        SshServerResource sshServerResource = new SshServerResource();
        ReflectionTestUtils.setField(sshServerResource, "sshServerService", sshServerService);

        ReflectionTestUtils.setField(sshServerService, "sshServerRepository", sshServerRepository);

        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
        ReflectionTestUtils.setField(sshAccountService, "sshAccountRepository", sshAccountRepository);
        ReflectionTestUtils.setField(sshAccountService, "sshProperties", sshProperties);

    }

    @PostConstruct
    public void setup() {
        archTypeRepository.deleteAll();
        sshAccountRepository.deleteAll();
        sshServerRepository.deleteAll();

        // Authenticate
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN));
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(loginname, password, authorities);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // Authenticate (end)

        sshServer = new SshServer();
        sshServer.setDnsname(sshProperties.getServer().getHost());
        sshServer.setFingerprint(sshProperties.getServer().getFingerprint());
        sshServer.setPort(sshProperties.getServer().getPort());
        sshServer.setActivated(true);
        sshServer.setMaxProc(10);
        sshServer.setMaxTime(60l);
        sshServer.setMaxJob(10);
        sshServer.setMaxMemory(10l);
        sshServer.setMaxSpace(10l);
        sshServer.setMaxConnection(DEFAULT_MAX_CONNECTION);
        sshServer.setNumConnection(0);

        sshServerRepository.saveAndFlush(sshServer);

        sshAccountDTO = new SshAccountDTO();
        sshAccountDTO.setUsername(sshProperties.getServer().getUsername().get(0));
        sshAccountDTO.setKey(sshProperties.getServer().getKey());
        sshAccountDTO.setActivated(true);
        sshAccountDTO.setBelongDnsname(sshProperties.getServer().getHost());
        sshAccountDTO.setBelongId(sshServer.getId());

        sshAccountDTO = sshAccountService.save(sshAccountDTO);
    }


    @Test
    public void testExecCommand() throws Exception {
        ExecConnectionServer connection = (ExecConnectionServer) sshAccountService.openConnection(sshAccountDTO.getId(), ChannelType.EXEC);

        String output = sshAccountService.launchCommand(connection, "whoami").trim();

        assertThat(output).isEqualTo(sshProperties.getServer().getUsername().get(0));

        sshAccountService.closeConnection(connection);

        SecurityContextHolder.clearContext();
    }


    @Test
    public void testTooMuchExecConnection() throws Exception {
        List<ConnectionServer> list = new ArrayList<>();
        ConnectionServer tmp = null;

        boolean checksum = false;
        try {
            for (int cnt = 0; cnt <= DEFAULT_MAX_CONNECTION; cnt++) {
                log.info("\n\n\tTrying connection {}/{}", cnt, DEFAULT_MAX_CONNECTION);
                log.info("{} has {} connections",
                    sshServerRepository.findOne(sshAccountDTO.getBelongId()).getDnsname(),
                    sshServerRepository.findOne(sshAccountDTO.getBelongId()).getNumConnection());
                try {
                    tmp = sshAccountService.openConnection(sshAccountDTO.getId(), ChannelType.EXEC);
                } catch (TooMuchConnectionException e) {
                    checksum = true;
                    break;
                }
                tmp.open();

                list.add(tmp);
            }
        } catch (Exception e) {
            log.info("get Exception: {}", e.getClass().getName());
        }

        list.stream().forEach(c -> sshAccountService.closeConnection(c));

        assertThat(list).hasSize(DEFAULT_MAX_CONNECTION);
        assertThat(checksum).isTrue();

        SecurityContextHolder.clearContext();
    }

}
