/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.AnalyzeType;
import es.udc.tic.rnasa.repository.AnalyzeTypeRepository;
import es.udc.tic.rnasa.service.AnalyzeTypeService;
import es.udc.tic.rnasa.repository.search.AnalyzeTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.AnalyzeTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.AnalyzeTypeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the AnalyzeTypeResource REST controller.
 *
 * @see AnalyzeTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class AnalyzeTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    @Inject
    private AnalyzeTypeMapper analyzeTypeMapper;

    @Inject
    private AnalyzeTypeService analyzeTypeService;

    @Inject
    private AnalyzeTypeSearchRepository analyzeTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restAnalyzeTypeMockMvc;

    private AnalyzeType analyzeType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AnalyzeTypeResource analyzeTypeResource = new AnalyzeTypeResource();
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeService", analyzeTypeService);
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeMapper", analyzeTypeMapper);
        this.restAnalyzeTypeMockMvc = MockMvcBuilders.standaloneSetup(analyzeTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        analyzeTypeSearchRepository.deleteAll();
        analyzeType = new AnalyzeType();
        analyzeType.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createAnalyzeType() throws Exception {
        int databaseSizeBeforeCreate = analyzeTypeRepository.findAll().size();

        // Create the AnalyzeType
        AnalyzeTypeDTO analyzeTypeDTO = analyzeTypeMapper.analyzeTypeToAnalyzeTypeDTO(analyzeType);

        restAnalyzeTypeMockMvc.perform(post("/api/analyze-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(analyzeTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the AnalyzeType in the database
        List<AnalyzeType> analyzeTypes = analyzeTypeRepository.findAll();
        assertThat(analyzeTypes).hasSize(databaseSizeBeforeCreate + 1);
        AnalyzeType testAnalyzeType = analyzeTypes.get(analyzeTypes.size() - 1);
        assertThat(testAnalyzeType.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the AnalyzeType in ElasticSearch
        AnalyzeType analyzeTypeEs = analyzeTypeSearchRepository.findOne(testAnalyzeType.getId());
        assertThat(analyzeTypeEs).isEqualToComparingFieldByField(testAnalyzeType);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = analyzeTypeRepository.findAll().size();
        // set the field null
        analyzeType.setName(null);

        // Create the AnalyzeType, which fails.
        AnalyzeTypeDTO analyzeTypeDTO = analyzeTypeMapper.analyzeTypeToAnalyzeTypeDTO(analyzeType);

        restAnalyzeTypeMockMvc.perform(post("/api/analyze-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(analyzeTypeDTO)))
                .andExpect(status().isBadRequest());

        List<AnalyzeType> analyzeTypes = analyzeTypeRepository.findAll();
        assertThat(analyzeTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAnalyzeTypes() throws Exception {
        // Initialize the database
        analyzeTypeRepository.saveAndFlush(analyzeType);

        // Get all the analyzeTypes
        restAnalyzeTypeMockMvc.perform(get("/api/analyze-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(analyzeType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAnalyzeType() throws Exception {
        // Initialize the database
        analyzeTypeRepository.saveAndFlush(analyzeType);

        // Get the analyzeType
        restAnalyzeTypeMockMvc.perform(get("/api/analyze-types/{id}", analyzeType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(analyzeType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAnalyzeType() throws Exception {
        // Get the analyzeType
        restAnalyzeTypeMockMvc.perform(get("/api/analyze-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnalyzeType() throws Exception {
        // Initialize the database
        analyzeTypeRepository.saveAndFlush(analyzeType);
        analyzeTypeSearchRepository.save(analyzeType);
        int databaseSizeBeforeUpdate = analyzeTypeRepository.findAll().size();

        // Update the analyzeType
        AnalyzeType updatedAnalyzeType = new AnalyzeType();
        updatedAnalyzeType.setId(analyzeType.getId());
        updatedAnalyzeType.setName(UPDATED_NAME);
        AnalyzeTypeDTO analyzeTypeDTO = analyzeTypeMapper.analyzeTypeToAnalyzeTypeDTO(updatedAnalyzeType);

        restAnalyzeTypeMockMvc.perform(put("/api/analyze-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(analyzeTypeDTO)))
                .andExpect(status().isOk());

        // Validate the AnalyzeType in the database
        List<AnalyzeType> analyzeTypes = analyzeTypeRepository.findAll();
        assertThat(analyzeTypes).hasSize(databaseSizeBeforeUpdate);
        AnalyzeType testAnalyzeType = analyzeTypes.get(analyzeTypes.size() - 1);
        assertThat(testAnalyzeType.getName()).isEqualTo(UPDATED_NAME);

        // Validate the AnalyzeType in ElasticSearch
        AnalyzeType analyzeTypeEs = analyzeTypeSearchRepository.findOne(testAnalyzeType.getId());
        assertThat(analyzeTypeEs).isEqualToComparingFieldByField(testAnalyzeType);
    }

    @Test
    @Transactional
    public void deleteAnalyzeType() throws Exception {
        // Initialize the database
        analyzeTypeRepository.saveAndFlush(analyzeType);
        analyzeTypeSearchRepository.save(analyzeType);
        int databaseSizeBeforeDelete = analyzeTypeRepository.findAll().size();

        // Get the analyzeType
        restAnalyzeTypeMockMvc.perform(delete("/api/analyze-types/{id}", analyzeType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean analyzeTypeExistsInEs = analyzeTypeSearchRepository.exists(analyzeType.getId());
        assertThat(analyzeTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<AnalyzeType> analyzeTypes = analyzeTypeRepository.findAll();
        assertThat(analyzeTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAnalyzeType() throws Exception {
        // Initialize the database
        analyzeTypeRepository.saveAndFlush(analyzeType);
        analyzeTypeSearchRepository.save(analyzeType);

        // Search the analyzeType
        restAnalyzeTypeMockMvc.perform(get("/api/_search/analyze-types?query=id:" + analyzeType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(analyzeType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
}
