/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.Application;
import es.udc.tic.rnasa.repository.ApplicationRepository;
import es.udc.tic.rnasa.service.ApplicationService;
import es.udc.tic.rnasa.repository.search.ApplicationSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ApplicationDTO;
import es.udc.tic.rnasa.web.rest.mapper.ApplicationMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ApplicationResource REST controller.
 *
 * @see ApplicationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class ApplicationResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_VERSION = "AAAAA";
    private static final String UPDATED_VERSION = "BBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TIMESTAMP_STR = dateTimeFormatter.format(DEFAULT_TIMESTAMP);

    private static final byte[] DEFAULT_FEATURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FEATURE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_FEATURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FEATURE_CONTENT_TYPE = "image/png";

    private static final Boolean DEFAULT_ACTIVATED = false;
    private static final Boolean UPDATED_ACTIVATED = true;

    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private ApplicationMapper applicationMapper;

    @Inject
    private ApplicationService applicationService;

    @Inject
    private ApplicationSearchRepository applicationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restApplicationMockMvc;

    private Application application;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ApplicationResource applicationResource = new ApplicationResource();
        ReflectionTestUtils.setField(applicationResource, "applicationService", applicationService);
        ReflectionTestUtils.setField(applicationResource, "applicationMapper", applicationMapper);
        this.restApplicationMockMvc = MockMvcBuilders.standaloneSetup(applicationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        applicationSearchRepository.deleteAll();
        application = new Application();
        application.setName(DEFAULT_NAME);
        application.setVersion(DEFAULT_VERSION);
        application.setTimestamp(DEFAULT_TIMESTAMP);
        application.setFeature(DEFAULT_FEATURE);
        application.setFeatureContentType(DEFAULT_FEATURE_CONTENT_TYPE);
        application.setActivated(DEFAULT_ACTIVATED);
    }

    @Test
    @Transactional
    public void createApplication() throws Exception {
        int databaseSizeBeforeCreate = applicationRepository.findAll().size();

        // Create the Application
        ApplicationDTO applicationDTO = applicationMapper.applicationToApplicationDTO(application);

        ZonedDateTime current = ZonedDateTime.now();

        restApplicationMockMvc.perform(post("/api/applications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(applicationDTO)))
                .andExpect(status().isCreated());

        // Validate the Application in the database
        List<Application> applications = applicationRepository.findAll();
        assertThat(applications).hasSize(databaseSizeBeforeCreate + 1);
        Application testApplication = applications.get(applications.size() - 1);
        assertThat(testApplication.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testApplication.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testApplication.getTimestamp()).isAfter(current);
        assertThat(testApplication.getFeature()).isEqualTo(DEFAULT_FEATURE);
        assertThat(testApplication.getFeatureContentType()).isEqualTo(DEFAULT_FEATURE_CONTENT_TYPE);
        assertThat(testApplication.isActivated()).isEqualTo(DEFAULT_ACTIVATED);

        // Validate the Application in ElasticSearch
        //Application applicationEs = applicationSearchRepository.findOne(testApplication.getId());
        //assertThat(applicationEs).isEqualToComparingFieldByField(testApplication);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicationRepository.findAll().size();
        // set the field null
        application.setName(null);

        // Create the Application, which fails.
        ApplicationDTO applicationDTO = applicationMapper.applicationToApplicationDTO(application);

        restApplicationMockMvc.perform(post("/api/applications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(applicationDTO)))
                .andExpect(status().isBadRequest());

        List<Application> applications = applicationRepository.findAll();
        assertThat(applications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVersionIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicationRepository.findAll().size();
        // set the field null
        application.setVersion(null);

        // Create the Application, which fails.
        ApplicationDTO applicationDTO = applicationMapper.applicationToApplicationDTO(application);

        restApplicationMockMvc.perform(post("/api/applications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(applicationDTO)))
                .andExpect(status().isBadRequest());

        List<Application> applications = applicationRepository.findAll();
        assertThat(applications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFeatureIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicationRepository.findAll().size();
        // set the field null
        application.setFeature(null);

        // Create the Application, which fails.
        ApplicationDTO applicationDTO = applicationMapper.applicationToApplicationDTO(application);

        restApplicationMockMvc.perform(post("/api/applications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(applicationDTO)))
                .andExpect(status().isBadRequest());

        List<Application> applications = applicationRepository.findAll();
        assertThat(applications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActivatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicationRepository.findAll().size();
        // set the field null
        application.setActivated(null);

        // Create the Application, which fails.
        ApplicationDTO applicationDTO = applicationMapper.applicationToApplicationDTO(application);

        restApplicationMockMvc.perform(post("/api/applications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(applicationDTO)))
                .andExpect(status().isBadRequest());

        List<Application> applications = applicationRepository.findAll();
        assertThat(applications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllApplications() throws Exception {
        // Initialize the database
        applicationRepository.saveAndFlush(application);

        // Get all the applications
        restApplicationMockMvc.perform(get("/api/applications?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(application.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())))
                .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
                .andExpect(jsonPath("$.[*].featureContentType").value(hasItem(DEFAULT_FEATURE_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].feature").value(hasItem(Base64Utils.encodeToString(DEFAULT_FEATURE))))
                .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())));
    }

    @Test
    @Transactional
    public void getApplication() throws Exception {
        // Initialize the database
        applicationRepository.saveAndFlush(application);

        // Get the application
        restApplicationMockMvc.perform(get("/api/applications/{id}", application.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(application.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP_STR))
            .andExpect(jsonPath("$.featureContentType").value(DEFAULT_FEATURE_CONTENT_TYPE))
            .andExpect(jsonPath("$.feature").value(Base64Utils.encodeToString(DEFAULT_FEATURE)))
            .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingApplication() throws Exception {
        // Get the application
        restApplicationMockMvc.perform(get("/api/applications/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateApplication() throws Exception {
        // Initialize the database
        applicationRepository.saveAndFlush(application);
        applicationSearchRepository.save(application);
        int databaseSizeBeforeUpdate = applicationRepository.findAll().size();


        // Update the application
        Application updatedApplication = new Application();
        updatedApplication.setId(application.getId());
        updatedApplication.setName(UPDATED_NAME);
        updatedApplication.setVersion(UPDATED_VERSION);
        updatedApplication.setTimestamp(UPDATED_TIMESTAMP);
        updatedApplication.setFeature(UPDATED_FEATURE);
        updatedApplication.setFeatureContentType(UPDATED_FEATURE_CONTENT_TYPE);
        updatedApplication.setActivated(UPDATED_ACTIVATED);
        ApplicationDTO applicationDTO = applicationMapper.applicationToApplicationDTO(updatedApplication);

        restApplicationMockMvc.perform(put("/api/applications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(applicationDTO)))
                .andExpect(status().isOk());

        // Validate the Application in the database
        List<Application> applications = applicationRepository.findAll();
        assertThat(applications).hasSize(databaseSizeBeforeUpdate);
        Application testApplication = applications.get(applications.size() - 1);
        assertThat(testApplication.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testApplication.getVersion()).isEqualTo(UPDATED_VERSION);
        //assertThat(testApplication.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testApplication.getFeature()).isEqualTo(UPDATED_FEATURE);
        assertThat(testApplication.getFeatureContentType()).isEqualTo(UPDATED_FEATURE_CONTENT_TYPE);
        assertThat(testApplication.isActivated()).isEqualTo(UPDATED_ACTIVATED);

        // Validate the Application in ElasticSearch
        //Application applicationEs = applicationSearchRepository.findOne(testApplication.getId());
        //assertThat(applicationEs).isEqualToComparingFieldByField(testApplication);
    }

    @Test
    @Transactional
    public void deleteApplication() throws Exception {
        // Initialize the database
        applicationRepository.saveAndFlush(application);
        applicationSearchRepository.save(application);
        int databaseSizeBeforeDelete = applicationRepository.findAll().size();

        // Get the application
        restApplicationMockMvc.perform(delete("/api/applications/{id}", application.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean applicationExistsInEs = applicationSearchRepository.exists(application.getId());
        assertThat(applicationExistsInEs).isFalse();

        // Validate the database is empty
        List<Application> applications = applicationRepository.findAll();
        assertThat(applications).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchApplication() throws Exception {
        // Initialize the database
        applicationRepository.saveAndFlush(application);
        applicationSearchRepository.save(application);

        // Search the application
        restApplicationMockMvc.perform(get("/api/_search/applications?query=id:" + application.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(application.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].featureContentType").value(hasItem(DEFAULT_FEATURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].feature").value(hasItem(Base64Utils.encodeToString(DEFAULT_FEATURE))))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())));
    }
}
