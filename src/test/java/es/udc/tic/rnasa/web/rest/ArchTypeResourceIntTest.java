/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.ArchType;
import es.udc.tic.rnasa.domain.CreateEntities;
import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.repository.ArchTypeRepository;
import es.udc.tic.rnasa.repository.search.ArchTypeSearchRepository;
import es.udc.tic.rnasa.service.ArchTypeService;
import es.udc.tic.rnasa.web.rest.dto.ArchTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ArchTypeMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ArchTypeResource REST controller.
 *
 * @see ArchTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class ArchTypeResourceIntTest {


    @Inject
    private CreateEntities createEntities;

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    private static final Boolean DEFAULT_ACTIVATED = false;
    private static final Boolean UPDATED_ACTIVATED = true;

    @Inject
    private ArchTypeRepository archTypeRepository;

    @Inject
    private ArchTypeMapper archTypeMapper;

    @Inject
    private ArchTypeService archTypeService;

    @Inject
    private ArchTypeSearchRepository archTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restArchTypeMockMvc;

    private ArchType archType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ArchTypeResource archTypeResource = new ArchTypeResource();
        ReflectionTestUtils.setField(archTypeResource, "archTypeService", archTypeService);
        ReflectionTestUtils.setField(archTypeResource, "archTypeMapper", archTypeMapper);
        this.restArchTypeMockMvc = MockMvcBuilders.standaloneSetup(archTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        archTypeSearchRepository.deleteAll();
        archType = new ArchType();
        archType.setName(DEFAULT_NAME);
        archType.setActivated(DEFAULT_ACTIVATED);
    }

    @Test
    @Transactional
    public void createArchType() throws Exception {
        int databaseSizeBeforeCreate = archTypeRepository.findAll().size();

        // Create the ArchType
        ArchTypeDTO archTypeDTO = archTypeMapper.archTypeToArchTypeDTO(archType);

        restArchTypeMockMvc.perform(post("/api/arch-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(archTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the ArchType in the database
        List<ArchType> archTypes = archTypeRepository.findAll();
        assertThat(archTypes).hasSize(databaseSizeBeforeCreate + 1);
        ArchType testArchType = archTypes.get(archTypes.size() - 1);
        assertThat(testArchType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testArchType.isActivated()).isEqualTo(DEFAULT_ACTIVATED);

        // Validate the ArchType in ElasticSearch
        ArchType archTypeEs = archTypeSearchRepository.findOne(testArchType.getId());
        assertThat(archTypeEs).isEqualToComparingFieldByField(testArchType);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = archTypeRepository.findAll().size();
        // set the field null
        archType.setName(null);

        // Create the ArchType, which fails.
        ArchTypeDTO archTypeDTO = archTypeMapper.archTypeToArchTypeDTO(archType);

        restArchTypeMockMvc.perform(post("/api/arch-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(archTypeDTO)))
                .andExpect(status().isBadRequest());

        List<ArchType> archTypes = archTypeRepository.findAll();
        assertThat(archTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActivatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = archTypeRepository.findAll().size();
        // set the field null
        archType.setActivated(null);

        // Create the ArchType, which fails.
        ArchTypeDTO archTypeDTO = archTypeMapper.archTypeToArchTypeDTO(archType);

        restArchTypeMockMvc.perform(post("/api/arch-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(archTypeDTO)))
                .andExpect(status().isBadRequest());

        List<ArchType> archTypes = archTypeRepository.findAll();
        assertThat(archTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllArchTypes() throws Exception {
        // Initialize the database
        archTypeRepository.saveAndFlush(archType);

        // Get all the archTypes
        restArchTypeMockMvc.perform(get("/api/arch-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(archType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())));
    }


    @Test
    @Transactional
    public void getAllArchTypesByBelongId() throws Exception {
        SshServer s1 = createEntities.createSshServer("example1", 0);
        SshServer s2 = createEntities.createSshServer("example2", 0);

        int max = 5;
        List<ArchType> l1 = new ArrayList<>(max);
        List<ArchType> l2 = new ArrayList<>(max);


        for (int cnt = 0; cnt < max; cnt++) {
            l1.add(createEntities.createArchType(s1.getDnsname()+"-a"+cnt, cnt % 2 == 0, s1));
            l2.add(createEntities.createArchType(s2.getDnsname()+"-b"+cnt, cnt % 2 == 0, s2));
        }

        int size = (int) l1.stream().filter(a -> a.isActivated()).count();

        // Get all the archTypes
        ResultActions result = restArchTypeMockMvc.perform(get("/api/arch-types-by-belong/{id}",s1.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasSize(size)));

        for (int cnt = 0; cnt < max; cnt++) {
            if (l1.get(cnt).isActivated()) {
                result.andExpect(jsonPath("$.[*].id").value(hasItem(l1.get(cnt).getId().intValue())))
                    .andExpect(jsonPath("$.[*].name").value(hasItem(l1.get(cnt).getName())))
                    .andExpect(jsonPath("$.[*].activated").value(hasItem(true)))
                    .andExpect(jsonPath("$.[*].belongId").value(hasItem(l1.get(cnt).getBelong().getId().intValue())));
            }
        }
    }

    @Test
    @Transactional
    public void getArchType() throws Exception {
        // Initialize the database
        archTypeRepository.saveAndFlush(archType);

        // Get the archType
        restArchTypeMockMvc.perform(get("/api/arch-types/{id}", archType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(archType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingArchType() throws Exception {
        // Get the archType
        restArchTypeMockMvc.perform(get("/api/arch-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateArchType() throws Exception {
        // Initialize the database
        archTypeRepository.saveAndFlush(archType);
        archTypeSearchRepository.save(archType);
        int databaseSizeBeforeUpdate = archTypeRepository.findAll().size();

        // Update the archType
        ArchType updatedArchType = new ArchType();
        updatedArchType.setId(archType.getId());
        updatedArchType.setName(UPDATED_NAME);
        updatedArchType.setActivated(UPDATED_ACTIVATED);
        ArchTypeDTO archTypeDTO = archTypeMapper.archTypeToArchTypeDTO(updatedArchType);

        restArchTypeMockMvc.perform(put("/api/arch-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(archTypeDTO)))
                .andExpect(status().isOk());

        // Validate the ArchType in the database
        List<ArchType> archTypes = archTypeRepository.findAll();
        assertThat(archTypes).hasSize(databaseSizeBeforeUpdate);
        ArchType testArchType = archTypes.get(archTypes.size() - 1);
        assertThat(testArchType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testArchType.isActivated()).isEqualTo(UPDATED_ACTIVATED);

        // Validate the ArchType in ElasticSearch
        ArchType archTypeEs = archTypeSearchRepository.findOne(testArchType.getId());
        assertThat(archTypeEs).isEqualToComparingFieldByField(testArchType);
    }

    @Test
    @Transactional
    public void deleteArchType() throws Exception {
        // Initialize the database
        archTypeRepository.saveAndFlush(archType);
        archTypeSearchRepository.save(archType);
        int databaseSizeBeforeDelete = archTypeRepository.findAll().size();

        // Get the archType
        restArchTypeMockMvc.perform(delete("/api/arch-types/{id}", archType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean archTypeExistsInEs = archTypeSearchRepository.exists(archType.getId());
        assertThat(archTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<ArchType> archTypes = archTypeRepository.findAll();
        assertThat(archTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchArchType() throws Exception {
        // Initialize the database
        archTypeRepository.saveAndFlush(archType);
        archTypeSearchRepository.save(archType);

        // Search the archType
        restArchTypeMockMvc.perform(get("/api/_search/arch-types?query=id:" + archType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(archType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())));
    }
}
