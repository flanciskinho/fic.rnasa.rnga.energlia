/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.CompiledApp;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.CompiledAppRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.CompiledAppService;
import es.udc.tic.rnasa.repository.search.CompiledAppSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.CompiledAppDTO;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.CompiledAppMapper;

import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import es.udc.tic.rnasa.web.rest.util.TestUtilPrivacy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CompiledAppResource REST controller.
 *
 * @see CompiledAppResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class CompiledAppResourceIntTest {


    private static final byte[] DEFAULT_EXECUTABLE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_EXECUTABLE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_EXECUTABLE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_EXECUTABLE_CONTENT_TYPE = "image/png";

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;

    @Inject
    private CompiledAppRepository compiledAppRepository;

    @Inject
    private CompiledAppMapper compiledAppMapper;

    @Inject
    private CompiledAppService compiledAppService;

    @Inject
    private CompiledAppSearchRepository compiledAppSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCompiledAppMockMvc;

    private CompiledApp compiledApp;

    private User owner;
    private UserDTO ownerDTO;

    @Inject
    private WebApplicationContext context;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CompiledAppResource compiledAppResource = new CompiledAppResource();
        ReflectionTestUtils.setField(compiledAppResource, "compiledAppService", compiledAppService);
        ReflectionTestUtils.setField(compiledAppResource, "compiledAppMapper", compiledAppMapper);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);

/*
        this.restCompiledAppMockMvc = MockMvcBuilders.standaloneSetup(compiledAppResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
*/
        this.restCompiledAppMockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Before
    public void initTest() {
        compiledAppSearchRepository.deleteAll();

        ownerDTO = new UserDTO(
            "joe",                  // login
            "123456789012345678901234567890123456789012345678901234567890",
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin("joe").isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }


        compiledApp = new CompiledApp();
        compiledApp.setExecutable(DEFAULT_EXECUTABLE);
        compiledApp.setExecutableContentType(DEFAULT_EXECUTABLE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createCompiledAppUnauthenticated() throws Exception {
        int databaseSizeBeforeCreate = compiledAppRepository.findAll().size();

        // Create the CompiledApp
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(compiledApp);

        restCompiledAppMockMvc.perform(MockMvcRequestBuilders.post("/api/compiled-apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compiledAppDTO)))
            .andExpect(status().is4xxClientError())
        ;

        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeCreate);

        // Validate the CompiledApp in ElasticSearch
        //CompiledApp compiledAppEs = compiledAppSearchRepository.findOne(testCompiledApp.getId());
        //assertThat(compiledAppEs).isEqualToComparingFieldByField(testCompiledApp);
    }

    @Test
    @Transactional
    public void createCompiledAppNoAdmin() throws Exception {
        int databaseSizeBeforeCreate = compiledAppRepository.findAll().size();

        // Create the CompiledApp
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(compiledApp);

        restCompiledAppMockMvc.perform(MockMvcRequestBuilders.post("/api/compiled-apps")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compiledAppDTO)))
            .andExpect(status().isForbidden())
        ;

        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeCreate);

        // Validate the CompiledApp in ElasticSearch
        //CompiledApp compiledAppEs = compiledAppSearchRepository.findOne(testCompiledApp.getId());
        //assertThat(compiledAppEs).isEqualToComparingFieldByField(testCompiledApp);
    }

    @Test
    @Transactional
    public void createCompiledApp() throws Exception {
        int databaseSizeBeforeCreate = compiledAppRepository.findAll().size();

        // Create the CompiledApp
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(compiledApp);

        restCompiledAppMockMvc.perform(MockMvcRequestBuilders.post("/api/compiled-apps")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compiledAppDTO)))
            .andExpect(status().is(HttpStatus.CREATED.value()))
        ;

        // Validate the CompiledApp in the database
        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeCreate + 1);
        CompiledApp testCompiledApp = compiledApps.get(compiledApps.size() - 1);
        assertThat(testCompiledApp.getExecutable()).isEqualTo(DEFAULT_EXECUTABLE);
        assertThat(testCompiledApp.getExecutableContentType()).isEqualTo(DEFAULT_EXECUTABLE_CONTENT_TYPE);

        // Validate the CompiledApp in ElasticSearch
        //CompiledApp compiledAppEs = compiledAppSearchRepository.findOne(testCompiledApp.getId());
        //assertThat(compiledAppEs).isEqualToComparingFieldByField(testCompiledApp);
    }

    @Test
    @Transactional
    public void getAllCompiledAppsUnauthorized() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);

        // Get all the compiledApps
        restCompiledAppMockMvc.perform(get("/api/compiled-apps?sort=id,desc"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void getAllCompiledAppsNonAdmin() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);

        // Get all the compiledApps
        restCompiledAppMockMvc.perform(get("/api/compiled-apps?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void getAllCompiledApps() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);

        // Get all the compiledApps
        restCompiledAppMockMvc.perform(get("/api/compiled-apps?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compiledApp.getId().intValue())))
            .andExpect(jsonPath("$.[*].executableContentType").value(hasItem(DEFAULT_EXECUTABLE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].executable").value(hasItem(Base64Utils.encodeToString(DEFAULT_EXECUTABLE))));
    }

    @Test
    @Transactional
    public void getCompiledAppUnauthenticated() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);

        // Get the compiledApp
        restCompiledAppMockMvc.perform(get("/api/compiled-apps/{id}", compiledApp.getId()))
            .andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void getCompiledAppNonAdmin() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);

        // Get the compiledApp
        restCompiledAppMockMvc.perform(get("/api/compiled-apps/{id}", compiledApp.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void getCompiledAppAdmin() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);

        // Get the compiledApp
        restCompiledAppMockMvc.perform(get("/api/compiled-apps/{id}", compiledApp.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(compiledApp.getId().intValue()))
            .andExpect(jsonPath("$.executableContentType").value(DEFAULT_EXECUTABLE_CONTENT_TYPE))
            .andExpect(jsonPath("$.executable").value(Base64Utils.encodeToString(DEFAULT_EXECUTABLE)));
    }

    @Test
    @Transactional
    public void getNonExistingCompiledApp() throws Exception {

        // Get the compiledApp
        restCompiledAppMockMvc.perform(get("/api/compiled-apps/{id}", Long.MAX_VALUE)
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompiledAppUnauthenticated() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);
        compiledAppSearchRepository.save(compiledApp);
        int databaseSizeBeforeUpdate = compiledAppRepository.findAll().size();

        // Update the compiledApp
        CompiledApp updatedCompiledApp = new CompiledApp();
        updatedCompiledApp.setId(compiledApp.getId());
        updatedCompiledApp.setExecutable(UPDATED_EXECUTABLE);
        updatedCompiledApp.setExecutableContentType(UPDATED_EXECUTABLE_CONTENT_TYPE);
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(updatedCompiledApp);

        restCompiledAppMockMvc.perform(put("/api/compiled-apps")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(compiledAppDTO)))
                .andExpect(status().is4xxClientError());

        // Validate the CompiledApp in the database
        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateCompiledAppNonAdmin() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);
        compiledAppSearchRepository.save(compiledApp);
        int databaseSizeBeforeUpdate = compiledAppRepository.findAll().size();

        // Update the compiledApp
        CompiledApp updatedCompiledApp = new CompiledApp();
        updatedCompiledApp.setId(compiledApp.getId());
        updatedCompiledApp.setExecutable(UPDATED_EXECUTABLE);
        updatedCompiledApp.setExecutableContentType(UPDATED_EXECUTABLE_CONTENT_TYPE);
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(updatedCompiledApp);

        restCompiledAppMockMvc.perform(put("/api/compiled-apps")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compiledAppDTO)))
            .andExpect(status().isForbidden());

        // Validate the CompiledApp in the database
        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateCompiledApp() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);
        compiledAppSearchRepository.save(compiledApp);
        int databaseSizeBeforeUpdate = compiledAppRepository.findAll().size();

        // Update the compiledApp
        CompiledApp updatedCompiledApp = new CompiledApp();
        updatedCompiledApp.setId(compiledApp.getId());
        updatedCompiledApp.setExecutable(UPDATED_EXECUTABLE);
        updatedCompiledApp.setExecutableContentType(UPDATED_EXECUTABLE_CONTENT_TYPE);
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(updatedCompiledApp);

        restCompiledAppMockMvc.perform(put("/api/compiled-apps")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compiledAppDTO)))
            .andExpect(status().isOk());

        // Validate the CompiledApp in the database
        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeUpdate);
        CompiledApp testCompiledApp = compiledApps.get(compiledApps.size() - 1);
        assertThat(testCompiledApp.getExecutable()).isEqualTo(UPDATED_EXECUTABLE);
        assertThat(testCompiledApp.getExecutableContentType()).isEqualTo(UPDATED_EXECUTABLE_CONTENT_TYPE);

        // Validate the CompiledApp in ElasticSearch
        CompiledApp compiledAppEs = compiledAppSearchRepository.findOne(testCompiledApp.getId());
        assertThat(compiledAppEs).isEqualToComparingFieldByField(testCompiledApp);
    }

    @Test
    @Transactional
    public void deleteCompiledAppNonAuthenticated() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);
        compiledAppSearchRepository.save(compiledApp);
        int databaseSizeBeforeDelete = compiledAppRepository.findAll().size();

        // Get the compiledApp
        restCompiledAppMockMvc.perform(delete("/api/compiled-apps/{id}", compiledApp.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError());

        // Validate ElasticSearch is empty
        boolean compiledAppExistsInEs = compiledAppSearchRepository.exists(compiledApp.getId());
        assertThat(compiledAppExistsInEs).isTrue();

        // Validate the database is empty
        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteCompiledAppNonAdmin() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);
        compiledAppSearchRepository.save(compiledApp);
        int databaseSizeBeforeDelete = compiledAppRepository.findAll().size();

        // Get the compiledApp
        restCompiledAppMockMvc.perform(delete("/api/compiled-apps/{id}", compiledApp.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isForbidden());

        // Validate ElasticSearch is empty
        boolean compiledAppExistsInEs = compiledAppSearchRepository.exists(compiledApp.getId());
        assertThat(compiledAppExistsInEs).isTrue();

        // Validate the database is empty
        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteCompiledApp() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);
        compiledAppSearchRepository.save(compiledApp);
        int databaseSizeBeforeDelete = compiledAppRepository.findAll().size();

        // Get the compiledApp
        restCompiledAppMockMvc.perform(delete("/api/compiled-apps/{id}", compiledApp.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean compiledAppExistsInEs = compiledAppSearchRepository.exists(compiledApp.getId());
        assertThat(compiledAppExistsInEs).isFalse();

        // Validate the database is empty
        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeDelete - 1);
    }

/*
    @Test
    @Transactional
    public void searchCompiledApp() throws Exception {
        // Initialize the database
        compiledAppRepository.saveAndFlush(compiledApp);
        compiledAppSearchRepository.save(compiledApp);

        // Search the compiledApp
        restCompiledAppMockMvc.perform(get("/api/_search/compiled-apps?query=id:" + compiledApp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compiledApp.getId().intValue())))
            .andExpect(jsonPath("$.[*].executableContentType").value(hasItem(DEFAULT_EXECUTABLE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].executable").value(hasItem(Base64Utils.encodeToString(DEFAULT_EXECUTABLE))));
    }
*/

    @Test
    @Transactional
    public void checkExecutableIsRequired() throws Exception {
        int databaseSizeBeforeTest = compiledAppRepository.findAll().size();
        // set the field null
        compiledApp.setExecutable(null);

        // Create the CompiledApp, which fails.
        CompiledAppDTO compiledAppDTO = compiledAppMapper.compiledAppToCompiledAppDTO(compiledApp);

        restCompiledAppMockMvc.perform(post("/api/compiled-apps")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compiledAppDTO)))
            .andExpect(status().isBadRequest());

        List<CompiledApp> compiledApps = compiledAppRepository.findAll();
        assertThat(compiledApps).hasSize(databaseSizeBeforeTest);
    }
}
