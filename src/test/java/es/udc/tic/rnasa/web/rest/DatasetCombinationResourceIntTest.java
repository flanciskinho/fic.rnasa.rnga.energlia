/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.DatasetCombination;
import es.udc.tic.rnasa.repository.DatasetCombinationRepository;
import es.udc.tic.rnasa.service.DatasetCombinationService;
import es.udc.tic.rnasa.repository.search.DatasetCombinationSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.DatasetCombinationDTO;
import es.udc.tic.rnasa.web.rest.mapper.DatasetCombinationMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the DatasetCombinationResource REST controller.
 *
 * @see DatasetCombinationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class DatasetCombinationResourceIntTest {


    private static final Integer DEFAULT_INDEX_COMBINATION = 1;
    private static final Integer UPDATED_INDEX_COMBINATION = 2;

    private static final byte[] DEFAULT_TRAINFILEIN = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_TRAINFILEIN = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_TRAINFILEIN_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_TRAINFILEIN_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_TRAINFILEOUT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_TRAINFILEOUT = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_TRAINFILEOUT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_TRAINFILEOUT_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_VALIDATIONFILEIN = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_VALIDATIONFILEIN = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_VALIDATIONFILEIN_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_VALIDATIONFILEIN_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_VALIDATIONFILEOUT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_VALIDATIONFILEOUT = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_VALIDATIONFILEOUT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_VALIDATIONFILEOUT_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_TESTFILEIN = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_TESTFILEIN = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_TESTFILEIN_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_TESTFILEIN_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_TESTFILEOUT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_TESTFILEOUT = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_TESTFILEOUT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_TESTFILEOUT_CONTENT_TYPE = "image/png";

    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private DatasetCombinationMapper datasetCombinationMapper;

    @Inject
    private DatasetCombinationService datasetCombinationService;

    @Inject
    private DatasetCombinationSearchRepository datasetCombinationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restDatasetCombinationMockMvc;

    private DatasetCombination datasetCombination;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DatasetCombinationResource datasetCombinationResource = new DatasetCombinationResource();
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationService", datasetCombinationService);
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationMapper", datasetCombinationMapper);
        this.restDatasetCombinationMockMvc = MockMvcBuilders.standaloneSetup(datasetCombinationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        datasetCombinationSearchRepository.deleteAll();
        datasetCombination = new DatasetCombination();
        datasetCombination.setIndexCombination(DEFAULT_INDEX_COMBINATION);
        datasetCombination.setTrainfilein(DEFAULT_TRAINFILEIN);
        datasetCombination.setTrainfileinContentType(DEFAULT_TRAINFILEIN_CONTENT_TYPE);
        datasetCombination.setTrainfileout(DEFAULT_TRAINFILEOUT);
        datasetCombination.setTrainfileoutContentType(DEFAULT_TRAINFILEOUT_CONTENT_TYPE);
        datasetCombination.setValidationfilein(DEFAULT_VALIDATIONFILEIN);
        datasetCombination.setValidationfileinContentType(DEFAULT_VALIDATIONFILEIN_CONTENT_TYPE);
        datasetCombination.setValidationfileout(DEFAULT_VALIDATIONFILEOUT);
        datasetCombination.setValidationfileoutContentType(DEFAULT_VALIDATIONFILEOUT_CONTENT_TYPE);
        datasetCombination.setTestfilein(DEFAULT_TESTFILEIN);
        datasetCombination.setTestfileinContentType(DEFAULT_TESTFILEIN_CONTENT_TYPE);
        datasetCombination.setTestfileout(DEFAULT_TESTFILEOUT);
        datasetCombination.setTestfileoutContentType(DEFAULT_TESTFILEOUT_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createDatasetCombination() throws Exception {
        int databaseSizeBeforeCreate = datasetCombinationRepository.findAll().size();

        // Create the DatasetCombination
        DatasetCombinationDTO datasetCombinationDTO = datasetCombinationMapper.datasetCombinationToDatasetCombinationDTO(datasetCombination);

        restDatasetCombinationMockMvc.perform(post("/api/dataset-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(datasetCombinationDTO)))
                .andExpect(status().isCreated());

        // Validate the DatasetCombination in the database
        List<DatasetCombination> datasetCombinations = datasetCombinationRepository.findAll();
        assertThat(datasetCombinations).hasSize(databaseSizeBeforeCreate + 1);
        DatasetCombination testDatasetCombination = datasetCombinations.get(datasetCombinations.size() - 1);
        assertThat(testDatasetCombination.getIndexCombination()).isEqualTo(DEFAULT_INDEX_COMBINATION);
        assertThat(testDatasetCombination.getTrainfilein()).isEqualTo(DEFAULT_TRAINFILEIN);
        assertThat(testDatasetCombination.getTrainfileinContentType()).isEqualTo(DEFAULT_TRAINFILEIN_CONTENT_TYPE);
        assertThat(testDatasetCombination.getTrainfileout()).isEqualTo(DEFAULT_TRAINFILEOUT);
        assertThat(testDatasetCombination.getTrainfileoutContentType()).isEqualTo(DEFAULT_TRAINFILEOUT_CONTENT_TYPE);
        assertThat(testDatasetCombination.getValidationfilein()).isEqualTo(DEFAULT_VALIDATIONFILEIN);
        assertThat(testDatasetCombination.getValidationfileinContentType()).isEqualTo(DEFAULT_VALIDATIONFILEIN_CONTENT_TYPE);
        assertThat(testDatasetCombination.getValidationfileout()).isEqualTo(DEFAULT_VALIDATIONFILEOUT);
        assertThat(testDatasetCombination.getValidationfileoutContentType()).isEqualTo(DEFAULT_VALIDATIONFILEOUT_CONTENT_TYPE);
        assertThat(testDatasetCombination.getTestfilein()).isEqualTo(DEFAULT_TESTFILEIN);
        assertThat(testDatasetCombination.getTestfileinContentType()).isEqualTo(DEFAULT_TESTFILEIN_CONTENT_TYPE);
        assertThat(testDatasetCombination.getTestfileout()).isEqualTo(DEFAULT_TESTFILEOUT);
        assertThat(testDatasetCombination.getTestfileoutContentType()).isEqualTo(DEFAULT_TESTFILEOUT_CONTENT_TYPE);

        // Validate the DatasetCombination in ElasticSearch
        DatasetCombination datasetCombinationEs = datasetCombinationSearchRepository.findOne(testDatasetCombination.getId());
        assertThat(datasetCombinationEs).isEqualToComparingFieldByField(testDatasetCombination);
    }

    @Test
    @Transactional
    public void checkIndexCombinationIsRequired() throws Exception {
        int databaseSizeBeforeTest = datasetCombinationRepository.findAll().size();
        // set the field null
        datasetCombination.setIndexCombination(null);

        // Create the DatasetCombination, which fails.
        DatasetCombinationDTO datasetCombinationDTO = datasetCombinationMapper.datasetCombinationToDatasetCombinationDTO(datasetCombination);

        restDatasetCombinationMockMvc.perform(post("/api/dataset-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(datasetCombinationDTO)))
                .andExpect(status().isBadRequest());

        List<DatasetCombination> datasetCombinations = datasetCombinationRepository.findAll();
        assertThat(datasetCombinations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTrainfileinIsRequired() throws Exception {
        int databaseSizeBeforeTest = datasetCombinationRepository.findAll().size();
        // set the field null
        datasetCombination.setTrainfilein(null);

        // Create the DatasetCombination, which fails.
        DatasetCombinationDTO datasetCombinationDTO = datasetCombinationMapper.datasetCombinationToDatasetCombinationDTO(datasetCombination);

        restDatasetCombinationMockMvc.perform(post("/api/dataset-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(datasetCombinationDTO)))
                .andExpect(status().isBadRequest());

        List<DatasetCombination> datasetCombinations = datasetCombinationRepository.findAll();
        assertThat(datasetCombinations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTrainfileoutIsRequired() throws Exception {
        int databaseSizeBeforeTest = datasetCombinationRepository.findAll().size();
        // set the field null
        datasetCombination.setTrainfileout(null);

        // Create the DatasetCombination, which fails.
        DatasetCombinationDTO datasetCombinationDTO = datasetCombinationMapper.datasetCombinationToDatasetCombinationDTO(datasetCombination);

        restDatasetCombinationMockMvc.perform(post("/api/dataset-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(datasetCombinationDTO)))
                .andExpect(status().isBadRequest());

        List<DatasetCombination> datasetCombinations = datasetCombinationRepository.findAll();
        assertThat(datasetCombinations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDatasetCombinations() throws Exception {
        // Initialize the database
        datasetCombinationRepository.saveAndFlush(datasetCombination);

        // Get all the datasetCombinations
        restDatasetCombinationMockMvc.perform(get("/api/dataset-combinations?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(datasetCombination.getId().intValue())))
                .andExpect(jsonPath("$.[*].indexCombination").value(hasItem(DEFAULT_INDEX_COMBINATION)))
                .andExpect(jsonPath("$.[*].trainfileinContentType").value(hasItem(DEFAULT_TRAINFILEIN_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].trainfilein").value(hasItem(Base64Utils.encodeToString(DEFAULT_TRAINFILEIN))))
                .andExpect(jsonPath("$.[*].trainfileoutContentType").value(hasItem(DEFAULT_TRAINFILEOUT_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].trainfileout").value(hasItem(Base64Utils.encodeToString(DEFAULT_TRAINFILEOUT))))
                .andExpect(jsonPath("$.[*].validationfileinContentType").value(hasItem(DEFAULT_VALIDATIONFILEIN_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].validationfilein").value(hasItem(Base64Utils.encodeToString(DEFAULT_VALIDATIONFILEIN))))
                .andExpect(jsonPath("$.[*].validationfileoutContentType").value(hasItem(DEFAULT_VALIDATIONFILEOUT_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].validationfileout").value(hasItem(Base64Utils.encodeToString(DEFAULT_VALIDATIONFILEOUT))))
                .andExpect(jsonPath("$.[*].testfileinContentType").value(hasItem(DEFAULT_TESTFILEIN_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].testfilein").value(hasItem(Base64Utils.encodeToString(DEFAULT_TESTFILEIN))))
                .andExpect(jsonPath("$.[*].testfileoutContentType").value(hasItem(DEFAULT_TESTFILEOUT_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].testfileout").value(hasItem(Base64Utils.encodeToString(DEFAULT_TESTFILEOUT))));
    }

    @Test
    @Transactional
    public void getDatasetCombination() throws Exception {
        // Initialize the database
        datasetCombinationRepository.saveAndFlush(datasetCombination);

        // Get the datasetCombination
        restDatasetCombinationMockMvc.perform(get("/api/dataset-combinations/{id}", datasetCombination.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(datasetCombination.getId().intValue()))
            .andExpect(jsonPath("$.indexCombination").value(DEFAULT_INDEX_COMBINATION))
            .andExpect(jsonPath("$.trainfileinContentType").value(DEFAULT_TRAINFILEIN_CONTENT_TYPE))
            .andExpect(jsonPath("$.trainfilein").value(Base64Utils.encodeToString(DEFAULT_TRAINFILEIN)))
            .andExpect(jsonPath("$.trainfileoutContentType").value(DEFAULT_TRAINFILEOUT_CONTENT_TYPE))
            .andExpect(jsonPath("$.trainfileout").value(Base64Utils.encodeToString(DEFAULT_TRAINFILEOUT)))
            .andExpect(jsonPath("$.validationfileinContentType").value(DEFAULT_VALIDATIONFILEIN_CONTENT_TYPE))
            .andExpect(jsonPath("$.validationfilein").value(Base64Utils.encodeToString(DEFAULT_VALIDATIONFILEIN)))
            .andExpect(jsonPath("$.validationfileoutContentType").value(DEFAULT_VALIDATIONFILEOUT_CONTENT_TYPE))
            .andExpect(jsonPath("$.validationfileout").value(Base64Utils.encodeToString(DEFAULT_VALIDATIONFILEOUT)))
            .andExpect(jsonPath("$.testfileinContentType").value(DEFAULT_TESTFILEIN_CONTENT_TYPE))
            .andExpect(jsonPath("$.testfilein").value(Base64Utils.encodeToString(DEFAULT_TESTFILEIN)))
            .andExpect(jsonPath("$.testfileoutContentType").value(DEFAULT_TESTFILEOUT_CONTENT_TYPE))
            .andExpect(jsonPath("$.testfileout").value(Base64Utils.encodeToString(DEFAULT_TESTFILEOUT)));
    }

    @Test
    @Transactional
    public void getNonExistingDatasetCombination() throws Exception {
        // Get the datasetCombination
        restDatasetCombinationMockMvc.perform(get("/api/dataset-combinations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDatasetCombination() throws Exception {
        // Initialize the database
        datasetCombinationRepository.saveAndFlush(datasetCombination);
        datasetCombinationSearchRepository.save(datasetCombination);
        int databaseSizeBeforeUpdate = datasetCombinationRepository.findAll().size();

        // Update the datasetCombination
        DatasetCombination updatedDatasetCombination = new DatasetCombination();
        updatedDatasetCombination.setId(datasetCombination.getId());
        updatedDatasetCombination.setIndexCombination(UPDATED_INDEX_COMBINATION);
        updatedDatasetCombination.setTrainfilein(UPDATED_TRAINFILEIN);
        updatedDatasetCombination.setTrainfileinContentType(UPDATED_TRAINFILEIN_CONTENT_TYPE);
        updatedDatasetCombination.setTrainfileout(UPDATED_TRAINFILEOUT);
        updatedDatasetCombination.setTrainfileoutContentType(UPDATED_TRAINFILEOUT_CONTENT_TYPE);
        updatedDatasetCombination.setValidationfilein(UPDATED_VALIDATIONFILEIN);
        updatedDatasetCombination.setValidationfileinContentType(UPDATED_VALIDATIONFILEIN_CONTENT_TYPE);
        updatedDatasetCombination.setValidationfileout(UPDATED_VALIDATIONFILEOUT);
        updatedDatasetCombination.setValidationfileoutContentType(UPDATED_VALIDATIONFILEOUT_CONTENT_TYPE);
        updatedDatasetCombination.setTestfilein(UPDATED_TESTFILEIN);
        updatedDatasetCombination.setTestfileinContentType(UPDATED_TESTFILEIN_CONTENT_TYPE);
        updatedDatasetCombination.setTestfileout(UPDATED_TESTFILEOUT);
        updatedDatasetCombination.setTestfileoutContentType(UPDATED_TESTFILEOUT_CONTENT_TYPE);
        DatasetCombinationDTO datasetCombinationDTO = datasetCombinationMapper.datasetCombinationToDatasetCombinationDTO(updatedDatasetCombination);

        restDatasetCombinationMockMvc.perform(put("/api/dataset-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(datasetCombinationDTO)))
                .andExpect(status().isOk());

        // Validate the DatasetCombination in the database
        List<DatasetCombination> datasetCombinations = datasetCombinationRepository.findAll();
        assertThat(datasetCombinations).hasSize(databaseSizeBeforeUpdate);
        DatasetCombination testDatasetCombination = datasetCombinations.get(datasetCombinations.size() - 1);
        assertThat(testDatasetCombination.getIndexCombination()).isEqualTo(UPDATED_INDEX_COMBINATION);
        assertThat(testDatasetCombination.getTrainfilein()).isEqualTo(UPDATED_TRAINFILEIN);
        assertThat(testDatasetCombination.getTrainfileinContentType()).isEqualTo(UPDATED_TRAINFILEIN_CONTENT_TYPE);
        assertThat(testDatasetCombination.getTrainfileout()).isEqualTo(UPDATED_TRAINFILEOUT);
        assertThat(testDatasetCombination.getTrainfileoutContentType()).isEqualTo(UPDATED_TRAINFILEOUT_CONTENT_TYPE);
        assertThat(testDatasetCombination.getValidationfilein()).isEqualTo(UPDATED_VALIDATIONFILEIN);
        assertThat(testDatasetCombination.getValidationfileinContentType()).isEqualTo(UPDATED_VALIDATIONFILEIN_CONTENT_TYPE);
        assertThat(testDatasetCombination.getValidationfileout()).isEqualTo(UPDATED_VALIDATIONFILEOUT);
        assertThat(testDatasetCombination.getValidationfileoutContentType()).isEqualTo(UPDATED_VALIDATIONFILEOUT_CONTENT_TYPE);
        assertThat(testDatasetCombination.getTestfilein()).isEqualTo(UPDATED_TESTFILEIN);
        assertThat(testDatasetCombination.getTestfileinContentType()).isEqualTo(UPDATED_TESTFILEIN_CONTENT_TYPE);
        assertThat(testDatasetCombination.getTestfileout()).isEqualTo(UPDATED_TESTFILEOUT);
        assertThat(testDatasetCombination.getTestfileoutContentType()).isEqualTo(UPDATED_TESTFILEOUT_CONTENT_TYPE);

        // Validate the DatasetCombination in ElasticSearch
        DatasetCombination datasetCombinationEs = datasetCombinationSearchRepository.findOne(testDatasetCombination.getId());
        assertThat(datasetCombinationEs).isEqualToComparingFieldByField(testDatasetCombination);
    }

    @Test
    @Transactional
    public void deleteDatasetCombination() throws Exception {
        // Initialize the database
        datasetCombinationRepository.saveAndFlush(datasetCombination);
        datasetCombinationSearchRepository.save(datasetCombination);
        int databaseSizeBeforeDelete = datasetCombinationRepository.findAll().size();

        // Get the datasetCombination
        restDatasetCombinationMockMvc.perform(delete("/api/dataset-combinations/{id}", datasetCombination.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean datasetCombinationExistsInEs = datasetCombinationSearchRepository.exists(datasetCombination.getId());
        assertThat(datasetCombinationExistsInEs).isFalse();

        // Validate the database is empty
        List<DatasetCombination> datasetCombinations = datasetCombinationRepository.findAll();
        assertThat(datasetCombinations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDatasetCombination() throws Exception {
        // Initialize the database
        datasetCombinationRepository.saveAndFlush(datasetCombination);
        datasetCombinationSearchRepository.save(datasetCombination);

        // Search the datasetCombination
        restDatasetCombinationMockMvc.perform(get("/api/_search/dataset-combinations?query=id:" + datasetCombination.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(datasetCombination.getId().intValue())))
            .andExpect(jsonPath("$.[*].indexCombination").value(hasItem(DEFAULT_INDEX_COMBINATION)))
            .andExpect(jsonPath("$.[*].trainfileinContentType").value(hasItem(DEFAULT_TRAINFILEIN_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].trainfilein").value(hasItem(Base64Utils.encodeToString(DEFAULT_TRAINFILEIN))))
            .andExpect(jsonPath("$.[*].trainfileoutContentType").value(hasItem(DEFAULT_TRAINFILEOUT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].trainfileout").value(hasItem(Base64Utils.encodeToString(DEFAULT_TRAINFILEOUT))))
            .andExpect(jsonPath("$.[*].validationfileinContentType").value(hasItem(DEFAULT_VALIDATIONFILEIN_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].validationfilein").value(hasItem(Base64Utils.encodeToString(DEFAULT_VALIDATIONFILEIN))))
            .andExpect(jsonPath("$.[*].validationfileoutContentType").value(hasItem(DEFAULT_VALIDATIONFILEOUT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].validationfileout").value(hasItem(Base64Utils.encodeToString(DEFAULT_VALIDATIONFILEOUT))))
            .andExpect(jsonPath("$.[*].testfileinContentType").value(hasItem(DEFAULT_TESTFILEIN_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].testfilein").value(hasItem(Base64Utils.encodeToString(DEFAULT_TESTFILEIN))))
            .andExpect(jsonPath("$.[*].testfileoutContentType").value(hasItem(DEFAULT_TESTFILEOUT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].testfileout").value(hasItem(Base64Utils.encodeToString(DEFAULT_TESTFILEOUT))));
    }
}
