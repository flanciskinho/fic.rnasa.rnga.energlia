/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.AnalyzeType;
import es.udc.tic.rnasa.domain.CreateEntities;
import es.udc.tic.rnasa.domain.Dataset;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.AnalyzeTypeRepository;
import es.udc.tic.rnasa.repository.DatasetRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.repository.search.DatasetSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.DatasetService;
import es.udc.tic.rnasa.service.UserService;
import es.udc.tic.rnasa.web.rest.dto.*;
import es.udc.tic.rnasa.web.rest.mapper.AnalyzeTypeMapper;
import es.udc.tic.rnasa.web.rest.mapper.DatasetMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import es.udc.tic.rnasa.web.rest.util.TestUtilPrivacy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Test class for the DatasetResource REST controller.
 *
 * @see DatasetResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class DatasetResourceIntTest {

    private final Logger log = LoggerFactory.getLogger(DatasetResourceIntTest.class);

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TIMESTAMP_STR = dateTimeFormatter.format(DEFAULT_TIMESTAMP);

    private static final Integer DEFAULT_NUM_PATTERN = 1;
    private static final Integer UPDATED_NUM_PATTERN = 2;

    private static final Integer DEFAULT_OUTPUT_FEATURE = 1;
    private static final Integer UPDATED_OUTPUT_FEATURE = 2;

    private static final Integer DEFAULT_INPUT_FEATURE = 1;
    private static final Integer UPDATED_INPUT_FEATURE = 2;

    private static final Integer DEFAULT_NUM_COMBINATION = 1;
    private static final Integer UPDATED_NUM_COMBINATION = 2;


    @Inject
    private CreateEntities createEntities;

    @Inject
    private DatasetRepository datasetRepository;

    @Inject
    private DatasetMapper datasetMapper;

    @Inject
    private DatasetService datasetService;

    @Inject
    private DatasetSearchRepository datasetSearchRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;

    @Inject
    private UserService userService;

    @Inject
    private AnalyzeTypeRepository analyzeTypeRepository;

    @Inject
    private AnalyzeTypeMapper analyzeTypeMapper;


    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restDatasetMockMvc;

    private Dataset dataset;
    private DatasetDTO datasetDTO;
    private User owner;
    private UserDTO ownerDTO;
    private AnalyzeType analyzeType;

    @Inject
    private WebApplicationContext context;

    private TestUtilPrivacy testUtilPrivacy;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DatasetResource datasetResource = new DatasetResource();
        ReflectionTestUtils.setField(datasetResource, "datasetService", datasetService);
        ReflectionTestUtils.setField(datasetResource, "datasetRepository", datasetRepository);
        ReflectionTestUtils.setField(datasetResource, "datasetMapper", datasetMapper);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userService", userService);
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper",     userMapper);

        AnalyzeTypeResource analyzeTypeResource = new AnalyzeTypeResource();
        ReflectionTestUtils.setField(analyzeTypeResource, "analyzeTypeMapper",     analyzeTypeMapper);
        ReflectionTestUtils.setField(analyzeTypeResource,  "analyzeTypeRepository", analyzeTypeRepository);

        this.restDatasetMockMvc = MockMvcBuilders.standaloneSetup(datasetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        datasetSearchRepository.deleteAll();

        ownerDTO = new UserDTO(
            "joe",                  // login
            "123456789012345678901234567890123456789012345678901234567890",
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin("joe").isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }


        if (!analyzeTypeRepository.findOneByName("AnalyzeType").isPresent()) {
            analyzeType = new AnalyzeType();
            analyzeType.setName("AnalyzeType");
            analyzeType = analyzeTypeRepository.saveAndFlush(analyzeType);
        } else {
            analyzeType = analyzeTypeRepository.findOneByName("AnalyzeType").get();
        }

        dataset = new Dataset();
        dataset.setName(DEFAULT_NAME);
        dataset.setDescription(DEFAULT_DESCRIPTION);
        dataset.setTimestamp(DEFAULT_TIMESTAMP);
        dataset.setNumPattern(DEFAULT_NUM_PATTERN);
        dataset.setOutputFeature(DEFAULT_OUTPUT_FEATURE);
        dataset.setInputFeature(DEFAULT_INPUT_FEATURE);
        dataset.setNumCombination(DEFAULT_NUM_COMBINATION);
        dataset.setOwner(owner);
        dataset.setAnalyze(analyzeType);

        datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);

        testUtilPrivacy = new TestUtilPrivacy(datasetRepository, context,
            "/api/datasets/{id}",
            "/api/datasets",
            "/api/datasets",
            "/api/datasets/{id}",
            "/api/datasets?sort=id,desc");
    }

    //
    // Custom-resource
    //

    @Test
    @Transactional
    public void createCustomDataset() throws Exception {
        int databaseSizeBeforeCreate = datasetRepository.findAll().size();

        testUtilPrivacy = new TestUtilPrivacy(datasetRepository, context,
            "/api/custom-datasets/{id}",
            "/api/custom-datasets",
            "/api/custom-datasets",
            "/api/custom-datasets/{id}",
            "/api/custom-datasets?sort=id,desc");


        ZonedDateTime zonedDateTime = ZonedDateTime.now();

        List<byte []> trainInput = new ArrayList<>();
        trainInput.add("0 1 2\n3 4 5".getBytes());
        trainInput.add("3 4 5\n0 1 2".getBytes());

        List<byte []> trainOutput = new ArrayList<>();
        trainOutput.add("0\n3".getBytes());
        trainOutput.add("3\n0".getBytes());

        CustomDatasetDTO customDatasetDTO = new CustomDatasetDTO(null, datasetDTO.getName(), datasetDTO.getDescription(), analyzeType.getId(), trainInput, trainOutput);

        ResultActions resultActions = testUtilPrivacy.createEntity(customDatasetDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        log.debug("json: {}", new String(TestUtil.convertObjectToJsonBytes(customDatasetDTO)));
        log.debug("resultActions: {}",resultActions.andReturn().getResponse().getContentAsString());

        List<Dataset> datasets = datasetRepository.findAll();

        assertThat(datasets).hasSize(databaseSizeBeforeCreate + 1);
        Dataset testDataset = datasets.get(datasets.size() - 1);
        assertThat(testDataset.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDataset.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDataset.getTimestamp()).isAfter(zonedDateTime);
        assertThat(testDataset.getNumPattern()).isEqualTo(2);
        assertThat(testDataset.getOutputFeature()).isEqualTo(1);
        assertThat(testDataset.getInputFeature()).isEqualTo(3);
        assertThat(testDataset.getNumCombination()).isEqualTo(2);
        assertThat(testDataset.getOwner()).isEqualTo(owner);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }


    @Test
    @Transactional
    public void updateCustomDataset() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeUpdate = datasetRepository.findAll().size();

        testUtilPrivacy = new TestUtilPrivacy(datasetRepository, context,
            "/api/custom-datasets/{id}",
            "/api/custom-datasets",
            "/api/custom-datasets",
            "/api/custom-datasets/{id}",
            "/api/custom-datasets?sort=id,desc");

        // Update the dataset
        CustomDescriptionDTO customDescriptionDTO = new CustomDescriptionDTO(dataset.getId(), UPDATED_DESCRIPTION);
        testUtilPrivacy.updateEntity(datasetDTO, customDescriptionDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);


        // Validate the Dataset in the database
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeUpdate);
        Dataset testDataset = datasets.get(datasets.size() - 1);
        assertThat(testDataset.getName()).isEqualTo(dataset.getName());
        assertThat(testDataset.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDataset.getTimestamp()).isEqualTo(dataset.getTimestamp());
        assertThat(testDataset.getNumPattern()).isEqualTo(dataset.getNumPattern());
        assertThat(testDataset.getOutputFeature()).isEqualTo(dataset.getOutputFeature());
        assertThat(testDataset.getInputFeature()).isEqualTo(dataset.getInputFeature());
        assertThat(testDataset.getNumCombination()).isEqualTo(dataset.getNumCombination());

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }

    //
    // End of custom resource
    //

    @Test
    @Transactional
    public void createDatasetOwner() throws Exception {
        int databaseSizeBeforeCreate = datasetRepository.findAll().size();

        testUtilPrivacy.createEntity(datasetDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        List<Dataset> datasets = datasetRepository.findAll();


        assertThat(datasets).hasSize(databaseSizeBeforeCreate + 1);
        Dataset testDataset = datasets.get(datasets.size() - 1);
        assertThat(testDataset.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDataset.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDataset.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testDataset.getNumPattern()).isEqualTo(DEFAULT_NUM_PATTERN);
        assertThat(testDataset.getOutputFeature()).isEqualTo(DEFAULT_OUTPUT_FEATURE);
        assertThat(testDataset.getInputFeature()).isEqualTo(DEFAULT_INPUT_FEATURE);
        assertThat(testDataset.getNumCombination()).isEqualTo(DEFAULT_NUM_COMBINATION);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }


    @Test
    @Transactional
    public void createDatasetAdmin() throws Exception {
        int databaseSizeBeforeCreate = datasetRepository.findAll().size();

        testUtilPrivacy.createEntity(datasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.CREATED);

        List<Dataset> datasets = datasetRepository.findAll();


        assertThat(datasets).hasSize(databaseSizeBeforeCreate + 1);
        Dataset testDataset = datasets.get(datasets.size() - 1);
        assertThat(testDataset.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDataset.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDataset.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testDataset.getNumPattern()).isEqualTo(DEFAULT_NUM_PATTERN);
        assertThat(testDataset.getOutputFeature()).isEqualTo(DEFAULT_OUTPUT_FEATURE);
        assertThat(testDataset.getInputFeature()).isEqualTo(DEFAULT_INPUT_FEATURE);
        assertThat(testDataset.getNumCombination()).isEqualTo(DEFAULT_NUM_COMBINATION);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }

    @Test
    @Transactional
    public void createDataset403() throws Exception {
        int databaseSizeBeforeCreate = datasetRepository.findAll().size();

        testUtilPrivacy.createEntity(datasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);

        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeCreate);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset); // check not contains
    }

    @Test
    @Transactional
    public void createDataset401() throws Exception {
        int databaseSizeBeforeCreate = datasetRepository.findAll().size();

        testUtilPrivacy.createEntityUnauthorized(datasetDTO);

        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeCreate);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset); // check not contains
    }

    @Test
    @Transactional
    public void deleteDatasetOwner() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeDelete = datasetRepository.findAll().size();

        // Get the dataset
        datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);
        testUtilPrivacy.deleteEntity(datasetDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);

        // Validate ElasticSearch is empty
        //boolean datasetExistsInEs = datasetSearchRepository.exists(dataset.getId());
        //assertThat(datasetExistsInEs).isFalse();

        // Validate the database is empty
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void deleteDatasetAdmin() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeDelete = datasetRepository.findAll().size();

        // Get the dataset
        datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);
        testUtilPrivacy.deleteEntity(datasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK);

        // Validate ElasticSearch is empty
        //boolean datasetExistsInEs = datasetSearchRepository.exists(dataset.getId());
        //assertThat(datasetExistsInEs).isFalse();

        // Validate the database is empty
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void deleteDataset403() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeDelete = datasetRepository.findAll().size();

        // Get the dataset
        datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);
        testUtilPrivacy.deleteEntity(datasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);

        // Validate ElasticSearch is empty
        //boolean datasetExistsInEs = datasetSearchRepository.exists(dataset.getId());
        //assertThat(datasetExistsInEs).isFalse();

        // Validate the database is empty
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeDelete);
    }



    @Test
    @Transactional
    public void deleteDataset404() throws Exception {
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeDelete = datasetRepository.findAll().size();

        // Get the dataset
        testUtilPrivacy.deleteEntityNotFound(Long.MAX_VALUE, owner.getLogin(), AuthoritiesConstants.ADMIN);

        // Validate ElasticSearch is empty
        //boolean datasetExistsInEs = datasetSearchRepository.exists(dataset.getId());
        //assertThat(datasetExistsInEs).isFalse();

        // Validate the database is empty
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeDelete );
    }

    @Test
    @Transactional
    public void deleteDataset401() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeDelete = datasetRepository.findAll().size();

        // Get the dataset
        datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);
        testUtilPrivacy.deleteEntityUnauthorized(datasetDTO);

        // Validate ElasticSearch is empty
        //boolean datasetExistsInEs = datasetSearchRepository.exists(dataset.getId());
        //assertThat(datasetExistsInEs).isFalse();

        // Validate the database is empty
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeDelete);
    }



    @Test
    @Transactional
    public void getAllDatasetsOwner() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);

        // Get all the datasets
        testUtilPrivacy.listEntities(owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK)
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataset.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].numPattern").value(hasItem(DEFAULT_NUM_PATTERN)))
            .andExpect(jsonPath("$.[*].outputFeature").value(hasItem(DEFAULT_OUTPUT_FEATURE)))
            .andExpect(jsonPath("$.[*].inputFeature").value(hasItem(DEFAULT_INPUT_FEATURE)))
            .andExpect(jsonPath("$.[*].numCombination").value(hasItem(DEFAULT_NUM_COMBINATION)))
            .andExpect(jsonPath("$.[*].ownerId").value(hasItem(owner.getId().intValue())))
            .andExpect(jsonPath("$.[*].ownerLogin").value(hasItem(owner.getLogin())));
    }

    @Test
    @Transactional
    public void getAllDatasetsAdmin() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);

        // Get all the datasets
        testUtilPrivacy.listEntities("+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK)
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataset.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].numPattern").value(hasItem(DEFAULT_NUM_PATTERN)))
            .andExpect(jsonPath("$.[*].outputFeature").value(hasItem(DEFAULT_OUTPUT_FEATURE)))
            .andExpect(jsonPath("$.[*].inputFeature").value(hasItem(DEFAULT_INPUT_FEATURE)))
            .andExpect(jsonPath("$.[*].numCombination").value(hasItem(DEFAULT_NUM_COMBINATION)))
            .andExpect(jsonPath("$.[*].ownerId").value(hasItem(owner.getId().intValue())))
            .andExpect(jsonPath("$.[*].ownerLogin").value(hasItem(owner.getLogin())));
    }

    @Test
    @Transactional
    public void shareDataset() throws Exception {
        datasetRepository.saveAndFlush(dataset);

        User user = userService.createUserInformation("leia", "leiai", "leia", "leia", "leia@localhost", "en");
        user.setActivated(true);
        userRepository.saveAndFlush(user);

        CustomShareResourceDTO customShareResourceDTO = new CustomShareResourceDTO(dataset.getId(), user.getLogin());

        MockMvc restEntityMock = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();

        ResultActions result =  restEntityMock.perform(MockMvcRequestBuilders.post("/api/dataset-share")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customShareResourceDTO)));

        assertThat(datasetRepository.findOne(dataset.getId()).getShares().contains(user)).isTrue();
    }

    @Test
    @Transactional
    public void getAllDatasetsOwnerShare() throws Exception {
        // Initialize the database
        User user = userService.createUserInformation("leia", "leiai", "leia", "leia", "leia@localhost", "en");
        user.setActivated(true);

        Dataset p1 = createEntities.createDataset(1, 2, 1, null);
        Dataset p2 = createEntities.createDataset(1, 2, 1, null);
        p2.setOwner(user);
        Set<User> set  = p2.getShares();
        set.add(owner);
        p2.setShares(set);
        datasetRepository.saveAndFlush(p1);
        datasetRepository.saveAndFlush(p2);

        // Get all the datasets
        ResultActions resultActions = testUtilPrivacy.listEntities(owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);
        resultActions.andExpect(jsonPath("$").value(hasSize(2)));

        //log.debug(resultActions.andReturn().getResponse().getContentAsString());
    }

    @Test
    @Transactional
    public void getAllDatasetsUserWithoutData() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);

        // Get all the datasets
        testUtilPrivacy.listEntities("+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.BAD_REQUEST);
    }

    @Test
    @Transactional
    public void getDatasetOwner() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);

        // Get the dataset
        testUtilPrivacy.getEntity(datasetDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);
    }

    @Test
    @Transactional
    public void getDatasetAdmin() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);

        // Get the dataset
        testUtilPrivacy.getEntity(datasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK);
    }

    @Test
    @Transactional
    public void getDataset403() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);

        // Get the dataset
        testUtilPrivacy.getEntity(datasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);
    }

    @Test
    @Transactional
    public void getDataset401() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);


        // Get the dataset
        testUtilPrivacy.getEntityUnauthorized(datasetDTO);
    }

    @Test
    @Transactional
    public void getDataset404() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);

        // Get the dataset
        testUtilPrivacy.getEntityNotFound(Long.MAX_VALUE, owner.getLogin(), AuthoritiesConstants.ADMIN);
    }

    @Test
    @Transactional
    public void updateDatasetOwner() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeUpdate = datasetRepository.findAll().size();

        // Update the dataset
        Dataset updatedDataset = new Dataset();
        updatedDataset.setId(dataset.getId());
        updatedDataset.setName(UPDATED_NAME);
        updatedDataset.setDescription(UPDATED_DESCRIPTION);
        updatedDataset.setTimestamp(UPDATED_TIMESTAMP);
        updatedDataset.setNumPattern(UPDATED_NUM_PATTERN);
        updatedDataset.setOutputFeature(UPDATED_OUTPUT_FEATURE);
        updatedDataset.setInputFeature(UPDATED_INPUT_FEATURE);
        updatedDataset.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedDataset.setOwner(owner);
        updatedDataset.setAnalyze(analyzeType);
        DatasetDTO updatedDatasetDTO = datasetMapper.datasetToDatasetDTO(updatedDataset);

        testUtilPrivacy.updateEntity(datasetDTO, updatedDatasetDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);


        // Validate the Dataset in the database
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeUpdate);
        Dataset testDataset = datasets.get(datasets.size() - 1);
        assertThat(testDataset.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDataset.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDataset.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testDataset.getNumPattern()).isEqualTo(UPDATED_NUM_PATTERN);
        assertThat(testDataset.getOutputFeature()).isEqualTo(UPDATED_OUTPUT_FEATURE);
        assertThat(testDataset.getInputFeature()).isEqualTo(UPDATED_INPUT_FEATURE);
        assertThat(testDataset.getNumCombination()).isEqualTo(UPDATED_NUM_COMBINATION);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }

    @Test
    @Transactional
    public void updateDatasetAdmin() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeUpdate = datasetRepository.findAll().size();

        // Update the dataset
        Dataset updatedDataset = new Dataset();
        updatedDataset.setId(dataset.getId());
        updatedDataset.setName(UPDATED_NAME);
        updatedDataset.setDescription(UPDATED_DESCRIPTION);
        updatedDataset.setTimestamp(UPDATED_TIMESTAMP);
        updatedDataset.setNumPattern(UPDATED_NUM_PATTERN);
        updatedDataset.setOutputFeature(UPDATED_OUTPUT_FEATURE);
        updatedDataset.setInputFeature(UPDATED_INPUT_FEATURE);
        updatedDataset.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedDataset.setOwner(owner);
        updatedDataset.setAnalyze(analyzeType);
        DatasetDTO updatedDatasetDTO = datasetMapper.datasetToDatasetDTO(updatedDataset);

        testUtilPrivacy.updateEntity(datasetDTO, updatedDatasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK);


        // Validate the Dataset in the database
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeUpdate);
        Dataset testDataset = datasets.get(datasets.size() - 1);
        assertThat(testDataset.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDataset.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDataset.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testDataset.getNumPattern()).isEqualTo(UPDATED_NUM_PATTERN);
        assertThat(testDataset.getOutputFeature()).isEqualTo(UPDATED_OUTPUT_FEATURE);
        assertThat(testDataset.getInputFeature()).isEqualTo(UPDATED_INPUT_FEATURE);
        assertThat(testDataset.getNumCombination()).isEqualTo(UPDATED_NUM_COMBINATION);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }


    @Test
    @Transactional
    public void updateDataset403() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeUpdate = datasetRepository.findAll().size();

        // Update the dataset
        Dataset updatedDataset = new Dataset();
        updatedDataset.setId(dataset.getId());
        updatedDataset.setName(UPDATED_NAME);
        updatedDataset.setDescription(UPDATED_DESCRIPTION);
        updatedDataset.setTimestamp(UPDATED_TIMESTAMP);
        updatedDataset.setNumPattern(UPDATED_NUM_PATTERN);
        updatedDataset.setOutputFeature(UPDATED_OUTPUT_FEATURE);
        updatedDataset.setInputFeature(UPDATED_INPUT_FEATURE);
        updatedDataset.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedDataset.setOwner(owner);
        DatasetDTO updatedDatasetDTO = datasetMapper.datasetToDatasetDTO(updatedDataset);

        testUtilPrivacy.updateEntity(datasetDTO, updatedDatasetDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);


        // Validate the Dataset in the database
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeUpdate);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }


    @Test
    @Transactional
    public void updateDataset401() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        //datasetSearchRepository.save(dataset);
        int databaseSizeBeforeUpdate = datasetRepository.findAll().size();

        // Update the dataset
        Dataset updatedDataset = new Dataset();
        updatedDataset.setId(dataset.getId());
        updatedDataset.setName(UPDATED_NAME);
        updatedDataset.setDescription(UPDATED_DESCRIPTION);
        updatedDataset.setTimestamp(UPDATED_TIMESTAMP);
        updatedDataset.setNumPattern(UPDATED_NUM_PATTERN);
        updatedDataset.setOutputFeature(UPDATED_OUTPUT_FEATURE);
        updatedDataset.setInputFeature(UPDATED_INPUT_FEATURE);
        updatedDataset.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedDataset.setOwner(owner);
        DatasetDTO updatedDatasetDTO = datasetMapper.datasetToDatasetDTO(updatedDataset);

        testUtilPrivacy.updateEntityUnauthorized(datasetDTO, updatedDatasetDTO);


        // Validate the Dataset in the database
        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeUpdate);

        // Validate the Dataset in ElasticSearch
        //Dataset datasetEs = datasetSearchRepository.findOne(testDataset.getId());
        //assertThat(datasetEs).isEqualToComparingFieldByField(testDataset);
    }


/*
    @Test
    @Transactional
    public void searchDataset() throws Exception {
        // Initialize the database
        datasetRepository.saveAndFlush(dataset);
        datasetSearchRepository.save(dataset);

        // Search the dataset
        restDatasetMockMvc.perform(get("/api/_search/datasets?query=id:" + dataset.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataset.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].numPattern").value(hasItem(DEFAULT_NUM_PATTERN)))
            .andExpect(jsonPath("$.[*].outputFeature").value(hasItem(DEFAULT_OUTPUT_FEATURE)))
            .andExpect(jsonPath("$.[*].inputFeature").value(hasItem(DEFAULT_INPUT_FEATURE)))
            .andExpect(jsonPath("$.[*].numCombination").value(hasItem(DEFAULT_NUM_COMBINATION)))
            .andExpect(jsonPath("$.[*].useTo").value(hasItem(DEFAULT_USE_TO)));
    }
*/


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = datasetRepository.findAll().size();
        // set the field null
        dataset.setName(null);

        // Create the Dataset, which fails.
        DatasetDTO datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);

        restDatasetMockMvc.perform(post("/api/datasets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasetDTO)))
            .andExpect(status().isBadRequest());

        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTimestampIsRequired() throws Exception {
        int databaseSizeBeforeTest = datasetRepository.findAll().size();
        // set the field null
        dataset.setTimestamp(null);

        // Create the Dataset, which fails.
        DatasetDTO datasetDTO = datasetMapper.datasetToDatasetDTO(dataset);

        restDatasetMockMvc.perform(post("/api/datasets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datasetDTO)))
            .andExpect(status().isBadRequest());

        List<Dataset> datasets = datasetRepository.findAll();
        assertThat(datasets).hasSize(databaseSizeBeforeTest);
    }
}
