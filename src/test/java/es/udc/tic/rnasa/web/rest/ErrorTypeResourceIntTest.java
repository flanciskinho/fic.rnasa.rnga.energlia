/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.ErrorType;
import es.udc.tic.rnasa.repository.ErrorTypeRepository;
import es.udc.tic.rnasa.service.ErrorTypeService;
import es.udc.tic.rnasa.repository.search.ErrorTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ErrorTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ErrorTypeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ErrorTypeResource REST controller.
 *
 * @see ErrorTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class ErrorTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    @Inject
    private ErrorTypeRepository errorTypeRepository;

    @Inject
    private ErrorTypeMapper errorTypeMapper;

    @Inject
    private ErrorTypeService errorTypeService;

    @Inject
    private ErrorTypeSearchRepository errorTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restErrorTypeMockMvc;

    private ErrorType errorType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ErrorTypeResource errorTypeResource = new ErrorTypeResource();
        ReflectionTestUtils.setField(errorTypeResource, "errorTypeService", errorTypeService);
        ReflectionTestUtils.setField(errorTypeResource, "errorTypeMapper", errorTypeMapper);
        this.restErrorTypeMockMvc = MockMvcBuilders.standaloneSetup(errorTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        errorTypeSearchRepository.deleteAll();
        errorType = new ErrorType();
        errorType.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createErrorType() throws Exception {
        int databaseSizeBeforeCreate = errorTypeRepository.findAll().size();

        // Create the ErrorType
        ErrorTypeDTO errorTypeDTO = errorTypeMapper.errorTypeToErrorTypeDTO(errorType);

        restErrorTypeMockMvc.perform(post("/api/error-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(errorTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the ErrorType in the database
        List<ErrorType> errorTypes = errorTypeRepository.findAll();
        assertThat(errorTypes).hasSize(databaseSizeBeforeCreate + 1);
        ErrorType testErrorType = errorTypes.get(errorTypes.size() - 1);
        assertThat(testErrorType.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the ErrorType in ElasticSearch
        //ErrorType errorTypeEs = errorTypeSearchRepository.findOne(testErrorType.getId());
        //assertThat(errorTypeEs).isEqualToComparingFieldByField(testErrorType);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = errorTypeRepository.findAll().size();
        // set the field null
        errorType.setName(null);

        // Create the ErrorType, which fails.
        ErrorTypeDTO errorTypeDTO = errorTypeMapper.errorTypeToErrorTypeDTO(errorType);

        restErrorTypeMockMvc.perform(post("/api/error-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(errorTypeDTO)))
                .andExpect(status().isBadRequest());

        List<ErrorType> errorTypes = errorTypeRepository.findAll();
        assertThat(errorTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllErrorTypes() throws Exception {
        // Initialize the database
        errorTypeRepository.saveAndFlush(errorType);

        // Get all the errorTypes
        restErrorTypeMockMvc.perform(get("/api/error-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(errorType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getErrorType() throws Exception {
        // Initialize the database
        errorTypeRepository.saveAndFlush(errorType);

        // Get the errorType
        restErrorTypeMockMvc.perform(get("/api/error-types/{id}", errorType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(errorType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingErrorType() throws Exception {
        // Get the errorType
        restErrorTypeMockMvc.perform(get("/api/error-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateErrorType() throws Exception {
        // Initialize the database
        errorTypeRepository.saveAndFlush(errorType);
        errorTypeSearchRepository.save(errorType);
        int databaseSizeBeforeUpdate = errorTypeRepository.findAll().size();

        // Update the errorType
        ErrorType updatedErrorType = new ErrorType();
        updatedErrorType.setId(errorType.getId());
        updatedErrorType.setName(UPDATED_NAME);
        ErrorTypeDTO errorTypeDTO = errorTypeMapper.errorTypeToErrorTypeDTO(updatedErrorType);

        restErrorTypeMockMvc.perform(put("/api/error-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(errorTypeDTO)))
                .andExpect(status().isOk());

        // Validate the ErrorType in the database
        List<ErrorType> errorTypes = errorTypeRepository.findAll();
        assertThat(errorTypes).hasSize(databaseSizeBeforeUpdate);
        ErrorType testErrorType = errorTypes.get(errorTypes.size() - 1);
        assertThat(testErrorType.getName()).isEqualTo(UPDATED_NAME);

        // Validate the ErrorType in ElasticSearch
        //ErrorType errorTypeEs = errorTypeSearchRepository.findOne(testErrorType.getId());
        //assertThat(errorTypeEs).isEqualToComparingFieldByField(testErrorType);
    }

    @Test
    @Transactional
    public void deleteErrorType() throws Exception {
        // Initialize the database
        errorTypeRepository.saveAndFlush(errorType);
        errorTypeSearchRepository.save(errorType);
        int databaseSizeBeforeDelete = errorTypeRepository.findAll().size();

        // Get the errorType
        restErrorTypeMockMvc.perform(delete("/api/error-types/{id}", errorType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        //boolean errorTypeExistsInEs = errorTypeSearchRepository.exists(errorType.getId());
        //assertThat(errorTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<ErrorType> errorTypes = errorTypeRepository.findAll();
        assertThat(errorTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchErrorType() throws Exception {
        // Initialize the database
        errorTypeRepository.saveAndFlush(errorType);
        errorTypeSearchRepository.save(errorType);

        // Search the errorType
        restErrorTypeMockMvc.perform(get("/api/_search/error-types?query=id:" + errorType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(errorType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
}
