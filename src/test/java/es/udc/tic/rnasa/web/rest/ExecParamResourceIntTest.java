/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.ExecParam;
import es.udc.tic.rnasa.repository.ExecParamRepository;
import es.udc.tic.rnasa.service.ExecParamService;
import es.udc.tic.rnasa.repository.search.ExecParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ExecParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.ExecParamMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ExecParamResource REST controller.
 *
 * @see ExecParamResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class ExecParamResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_VALUE = "AAAAA";
    private static final String UPDATED_VALUE = "BBBBB";

    @Inject
    private ExecParamRepository execParamRepository;

    @Inject
    private ExecParamMapper execParamMapper;

    @Inject
    private ExecParamService execParamService;

    @Inject
    private ExecParamSearchRepository execParamSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restExecParamMockMvc;

    private ExecParam execParam;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ExecParamResource execParamResource = new ExecParamResource();
        ReflectionTestUtils.setField(execParamResource, "execParamService", execParamService);
        ReflectionTestUtils.setField(execParamResource, "execParamMapper", execParamMapper);
        this.restExecParamMockMvc = MockMvcBuilders.standaloneSetup(execParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        execParamSearchRepository.deleteAll();
        execParam = new ExecParam();
        execParam.setName(DEFAULT_NAME);
        execParam.setValue(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createExecParam() throws Exception {
        int databaseSizeBeforeCreate = execParamRepository.findAll().size();

        // Create the ExecParam
        ExecParamDTO execParamDTO = execParamMapper.execParamToExecParamDTO(execParam);

        restExecParamMockMvc.perform(post("/api/exec-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(execParamDTO)))
                .andExpect(status().isCreated());

        // Validate the ExecParam in the database
        List<ExecParam> execParams = execParamRepository.findAll();
        assertThat(execParams).hasSize(databaseSizeBeforeCreate + 1);
        ExecParam testExecParam = execParams.get(execParams.size() - 1);
        assertThat(testExecParam.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testExecParam.getValue()).isEqualTo(DEFAULT_VALUE);

        // Validate the ExecParam in ElasticSearch
        ExecParam execParamEs = execParamSearchRepository.findOne(testExecParam.getId());
        assertThat(execParamEs).isEqualToComparingFieldByField(testExecParam);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = execParamRepository.findAll().size();
        // set the field null
        execParam.setName(null);

        // Create the ExecParam, which fails.
        ExecParamDTO execParamDTO = execParamMapper.execParamToExecParamDTO(execParam);

        restExecParamMockMvc.perform(post("/api/exec-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(execParamDTO)))
                .andExpect(status().isBadRequest());

        List<ExecParam> execParams = execParamRepository.findAll();
        assertThat(execParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = execParamRepository.findAll().size();
        // set the field null
        execParam.setValue(null);

        // Create the ExecParam, which fails.
        ExecParamDTO execParamDTO = execParamMapper.execParamToExecParamDTO(execParam);

        restExecParamMockMvc.perform(post("/api/exec-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(execParamDTO)))
                .andExpect(status().isBadRequest());

        List<ExecParam> execParams = execParamRepository.findAll();
        assertThat(execParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllExecParams() throws Exception {
        // Initialize the database
        execParamRepository.saveAndFlush(execParam);

        // Get all the execParams
        restExecParamMockMvc.perform(get("/api/exec-params?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(execParam.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getExecParam() throws Exception {
        // Initialize the database
        execParamRepository.saveAndFlush(execParam);

        // Get the execParam
        restExecParamMockMvc.perform(get("/api/exec-params/{id}", execParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(execParam.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExecParam() throws Exception {
        // Get the execParam
        restExecParamMockMvc.perform(get("/api/exec-params/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExecParam() throws Exception {
        // Initialize the database
        execParamRepository.saveAndFlush(execParam);
        execParamSearchRepository.save(execParam);
        int databaseSizeBeforeUpdate = execParamRepository.findAll().size();

        // Update the execParam
        ExecParam updatedExecParam = new ExecParam();
        updatedExecParam.setId(execParam.getId());
        updatedExecParam.setName(UPDATED_NAME);
        updatedExecParam.setValue(UPDATED_VALUE);
        ExecParamDTO execParamDTO = execParamMapper.execParamToExecParamDTO(updatedExecParam);

        restExecParamMockMvc.perform(put("/api/exec-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(execParamDTO)))
                .andExpect(status().isOk());

        // Validate the ExecParam in the database
        List<ExecParam> execParams = execParamRepository.findAll();
        assertThat(execParams).hasSize(databaseSizeBeforeUpdate);
        ExecParam testExecParam = execParams.get(execParams.size() - 1);
        assertThat(testExecParam.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testExecParam.getValue()).isEqualTo(UPDATED_VALUE);

        // Validate the ExecParam in ElasticSearch
        ExecParam execParamEs = execParamSearchRepository.findOne(testExecParam.getId());
        assertThat(execParamEs).isEqualToComparingFieldByField(testExecParam);
    }

    @Test
    @Transactional
    public void deleteExecParam() throws Exception {
        // Initialize the database
        execParamRepository.saveAndFlush(execParam);
        execParamSearchRepository.save(execParam);
        int databaseSizeBeforeDelete = execParamRepository.findAll().size();

        // Get the execParam
        restExecParamMockMvc.perform(delete("/api/exec-params/{id}", execParam.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean execParamExistsInEs = execParamSearchRepository.exists(execParam.getId());
        assertThat(execParamExistsInEs).isFalse();

        // Validate the database is empty
        List<ExecParam> execParams = execParamRepository.findAll();
        assertThat(execParams).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchExecParam() throws Exception {
        // Initialize the database
        execParamRepository.saveAndFlush(execParam);
        execParamSearchRepository.save(execParam);

        // Search the execParam
        restExecParamMockMvc.perform(get("/api/_search/exec-params?query=id:" + execParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(execParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }
}
