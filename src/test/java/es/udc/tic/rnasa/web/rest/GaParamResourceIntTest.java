/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.GaParam;
import es.udc.tic.rnasa.repository.GaParamRepository;
import es.udc.tic.rnasa.service.GaParamService;
import es.udc.tic.rnasa.repository.search.GaParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.GaParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaParamMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the GaParamResource REST controller.
 *
 * @see GaParamResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class GaParamResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_VALUE = "AAAAA";
    private static final String UPDATED_VALUE = "BBBBB";

    @Inject
    private GaParamRepository gaParamRepository;

    @Inject
    private GaParamMapper gaParamMapper;

    @Inject
    private GaParamService gaParamService;

    @Inject
    private GaParamSearchRepository gaParamSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGaParamMockMvc;

    private GaParam gaParam;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GaParamResource gaParamResource = new GaParamResource();
        ReflectionTestUtils.setField(gaParamResource, "gaParamService", gaParamService);
        ReflectionTestUtils.setField(gaParamResource, "gaParamMapper", gaParamMapper);
        this.restGaParamMockMvc = MockMvcBuilders.standaloneSetup(gaParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        gaParamSearchRepository.deleteAll();
        gaParam = new GaParam();
        gaParam.setName(DEFAULT_NAME);
        gaParam.setValue(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createGaParam() throws Exception {
        int databaseSizeBeforeCreate = gaParamRepository.findAll().size();

        // Create the GaParam
        GaParamDTO gaParamDTO = gaParamMapper.gaParamToGaParamDTO(gaParam);

        restGaParamMockMvc.perform(post("/api/ga-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaParamDTO)))
                .andExpect(status().isCreated());

        // Validate the GaParam in the database
        List<GaParam> gaParams = gaParamRepository.findAll();
        assertThat(gaParams).hasSize(databaseSizeBeforeCreate + 1);
        GaParam testGaParam = gaParams.get(gaParams.size() - 1);
        assertThat(testGaParam.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testGaParam.getValue()).isEqualTo(DEFAULT_VALUE);

        // Validate the GaParam in ElasticSearch
        GaParam gaParamEs = gaParamSearchRepository.findOne(testGaParam.getId());
        assertThat(gaParamEs).isEqualToComparingFieldByField(testGaParam);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = gaParamRepository.findAll().size();
        // set the field null
        gaParam.setName(null);

        // Create the GaParam, which fails.
        GaParamDTO gaParamDTO = gaParamMapper.gaParamToGaParamDTO(gaParam);

        restGaParamMockMvc.perform(post("/api/ga-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaParamDTO)))
                .andExpect(status().isBadRequest());

        List<GaParam> gaParams = gaParamRepository.findAll();
        assertThat(gaParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = gaParamRepository.findAll().size();
        // set the field null
        gaParam.setValue(null);

        // Create the GaParam, which fails.
        GaParamDTO gaParamDTO = gaParamMapper.gaParamToGaParamDTO(gaParam);

        restGaParamMockMvc.perform(post("/api/ga-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaParamDTO)))
                .andExpect(status().isBadRequest());

        List<GaParam> gaParams = gaParamRepository.findAll();
        assertThat(gaParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGaParams() throws Exception {
        // Initialize the database
        gaParamRepository.saveAndFlush(gaParam);

        // Get all the gaParams
        restGaParamMockMvc.perform(get("/api/ga-params?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(gaParam.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getGaParam() throws Exception {
        // Initialize the database
        gaParamRepository.saveAndFlush(gaParam);

        // Get the gaParam
        restGaParamMockMvc.perform(get("/api/ga-params/{id}", gaParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(gaParam.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGaParam() throws Exception {
        // Get the gaParam
        restGaParamMockMvc.perform(get("/api/ga-params/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGaParam() throws Exception {
        // Initialize the database
        gaParamRepository.saveAndFlush(gaParam);
        gaParamSearchRepository.save(gaParam);
        int databaseSizeBeforeUpdate = gaParamRepository.findAll().size();

        // Update the gaParam
        GaParam updatedGaParam = new GaParam();
        updatedGaParam.setId(gaParam.getId());
        updatedGaParam.setName(UPDATED_NAME);
        updatedGaParam.setValue(UPDATED_VALUE);
        GaParamDTO gaParamDTO = gaParamMapper.gaParamToGaParamDTO(updatedGaParam);

        restGaParamMockMvc.perform(put("/api/ga-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaParamDTO)))
                .andExpect(status().isOk());

        // Validate the GaParam in the database
        List<GaParam> gaParams = gaParamRepository.findAll();
        assertThat(gaParams).hasSize(databaseSizeBeforeUpdate);
        GaParam testGaParam = gaParams.get(gaParams.size() - 1);
        assertThat(testGaParam.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGaParam.getValue()).isEqualTo(UPDATED_VALUE);

        // Validate the GaParam in ElasticSearch
        GaParam gaParamEs = gaParamSearchRepository.findOne(testGaParam.getId());
        assertThat(gaParamEs).isEqualToComparingFieldByField(testGaParam);
    }

    @Test
    @Transactional
    public void deleteGaParam() throws Exception {
        // Initialize the database
        gaParamRepository.saveAndFlush(gaParam);
        gaParamSearchRepository.save(gaParam);
        int databaseSizeBeforeDelete = gaParamRepository.findAll().size();

        // Get the gaParam
        restGaParamMockMvc.perform(delete("/api/ga-params/{id}", gaParam.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean gaParamExistsInEs = gaParamSearchRepository.exists(gaParam.getId());
        assertThat(gaParamExistsInEs).isFalse();

        // Validate the database is empty
        List<GaParam> gaParams = gaParamRepository.findAll();
        assertThat(gaParams).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGaParam() throws Exception {
        // Initialize the database
        gaParamRepository.saveAndFlush(gaParam);
        gaParamSearchRepository.save(gaParam);

        // Search the gaParam
        restGaParamMockMvc.perform(get("/api/_search/ga-params?query=id:" + gaParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gaParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }
}
