/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.Ga;
import es.udc.tic.rnasa.repository.GaRepository;
import es.udc.tic.rnasa.service.GaService;
import es.udc.tic.rnasa.repository.search.GaSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.GaDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the GaResource REST controller.
 *
 * @see GaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class GaResourceIntTest {

    private static final String DEFAULT_VERSION = "AAAAA";
    private static final String UPDATED_VERSION = "BBBBB";

    @Inject
    private GaRepository gaRepository;

    @Inject
    private GaMapper gaMapper;

    @Inject
    private GaService gaService;

    @Inject
    private GaSearchRepository gaSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGaMockMvc;

    private Ga ga;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GaResource gaResource = new GaResource();
        ReflectionTestUtils.setField(gaResource, "gaService", gaService);
        ReflectionTestUtils.setField(gaResource, "gaMapper", gaMapper);
        this.restGaMockMvc = MockMvcBuilders.standaloneSetup(gaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        gaSearchRepository.deleteAll();
        ga = new Ga();
        ga.setVersion(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    public void createGa() throws Exception {
        int databaseSizeBeforeCreate = gaRepository.findAll().size();

        // Create the Ga
        GaDTO gaDTO = gaMapper.gaToGaDTO(ga);

        restGaMockMvc.perform(post("/api/gas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaDTO)))
                .andExpect(status().isCreated());

        // Validate the Ga in the database
        List<Ga> gas = gaRepository.findAll();
        assertThat(gas).hasSize(databaseSizeBeforeCreate + 1);
        Ga testGa = gas.get(gas.size() - 1);
        assertThat(testGa.getVersion()).isEqualTo(DEFAULT_VERSION);

        // Validate the Ga in ElasticSearch
        Ga gaEs = gaSearchRepository.findOne(testGa.getId());
        assertThat(gaEs).isEqualToComparingFieldByField(testGa);
    }

    @Test
    @Transactional
    public void checkVersionIsRequired() throws Exception {
        int databaseSizeBeforeTest = gaRepository.findAll().size();
        // set the field null
        ga.setVersion(null);

        // Create the Ga, which fails.
        GaDTO gaDTO = gaMapper.gaToGaDTO(ga);

        restGaMockMvc.perform(post("/api/gas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaDTO)))
                .andExpect(status().isBadRequest());

        List<Ga> gas = gaRepository.findAll();
        assertThat(gas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGas() throws Exception {
        // Initialize the database
        gaRepository.saveAndFlush(ga);

        // Get all the gas
        restGaMockMvc.perform(get("/api/gas?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(ga.getId().intValue())))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())));
    }

    @Test
    @Transactional
    public void getGa() throws Exception {
        // Initialize the database
        gaRepository.saveAndFlush(ga);

        // Get the ga
        restGaMockMvc.perform(get("/api/gas/{id}", ga.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(ga.getId().intValue()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGa() throws Exception {
        // Get the ga
        restGaMockMvc.perform(get("/api/gas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGa() throws Exception {
        // Initialize the database
        gaRepository.saveAndFlush(ga);
        gaSearchRepository.save(ga);
        int databaseSizeBeforeUpdate = gaRepository.findAll().size();

        // Update the ga
        Ga updatedGa = new Ga();
        updatedGa.setId(ga.getId());
        updatedGa.setVersion(UPDATED_VERSION);
        GaDTO gaDTO = gaMapper.gaToGaDTO(updatedGa);

        restGaMockMvc.perform(put("/api/gas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaDTO)))
                .andExpect(status().isOk());

        // Validate the Ga in the database
        List<Ga> gas = gaRepository.findAll();
        assertThat(gas).hasSize(databaseSizeBeforeUpdate);
        Ga testGa = gas.get(gas.size() - 1);
        assertThat(testGa.getVersion()).isEqualTo(UPDATED_VERSION);

        // Validate the Ga in ElasticSearch
        Ga gaEs = gaSearchRepository.findOne(testGa.getId());
        assertThat(gaEs).isEqualToComparingFieldByField(testGa);
    }

    @Test
    @Transactional
    public void deleteGa() throws Exception {
        // Initialize the database
        gaRepository.saveAndFlush(ga);
        gaSearchRepository.save(ga);
        int databaseSizeBeforeDelete = gaRepository.findAll().size();

        // Get the ga
        restGaMockMvc.perform(delete("/api/gas/{id}", ga.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean gaExistsInEs = gaSearchRepository.exists(ga.getId());
        assertThat(gaExistsInEs).isFalse();

        // Validate the database is empty
        List<Ga> gas = gaRepository.findAll();
        assertThat(gas).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGa() throws Exception {
        // Initialize the database
        gaRepository.saveAndFlush(ga);
        gaSearchRepository.save(ga);

        // Search the ga
        restGaMockMvc.perform(get("/api/_search/gas?query=id:" + ga.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ga.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())));
    }
}
