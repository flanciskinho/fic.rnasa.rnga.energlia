/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.GaType;
import es.udc.tic.rnasa.repository.GaTypeRepository;
import es.udc.tic.rnasa.service.GaTypeService;
import es.udc.tic.rnasa.repository.search.GaTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.GaTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.GaTypeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the GaTypeResource REST controller.
 *
 * @see GaTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class GaTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private GaTypeRepository gaTypeRepository;

    @Inject
    private GaTypeMapper gaTypeMapper;

    @Inject
    private GaTypeService gaTypeService;

    @Inject
    private GaTypeSearchRepository gaTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGaTypeMockMvc;

    private GaType gaType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GaTypeResource gaTypeResource = new GaTypeResource();
        ReflectionTestUtils.setField(gaTypeResource, "gaTypeService", gaTypeService);
        ReflectionTestUtils.setField(gaTypeResource, "gaTypeMapper", gaTypeMapper);
        this.restGaTypeMockMvc = MockMvcBuilders.standaloneSetup(gaTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        gaTypeSearchRepository.deleteAll();
        gaType = new GaType();
        gaType.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createGaType() throws Exception {
        int databaseSizeBeforeCreate = gaTypeRepository.findAll().size();

        // Create the GaType
        GaTypeDTO gaTypeDTO = gaTypeMapper.gaTypeToGaTypeDTO(gaType);

        restGaTypeMockMvc.perform(post("/api/ga-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the GaType in the database
        List<GaType> gaTypes = gaTypeRepository.findAll();
        assertThat(gaTypes).hasSize(databaseSizeBeforeCreate + 1);
        GaType testGaType = gaTypes.get(gaTypes.size() - 1);
        assertThat(testGaType.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the GaType in ElasticSearch
        GaType gaTypeEs = gaTypeSearchRepository.findOne(testGaType.getId());
        assertThat(gaTypeEs).isEqualToComparingFieldByField(testGaType);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = gaTypeRepository.findAll().size();
        // set the field null
        gaType.setName(null);

        // Create the GaType, which fails.
        GaTypeDTO gaTypeDTO = gaTypeMapper.gaTypeToGaTypeDTO(gaType);

        restGaTypeMockMvc.perform(post("/api/ga-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaTypeDTO)))
                .andExpect(status().isBadRequest());

        List<GaType> gaTypes = gaTypeRepository.findAll();
        assertThat(gaTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGaTypes() throws Exception {
        // Initialize the database
        gaTypeRepository.saveAndFlush(gaType);

        // Get all the gaTypes
        restGaTypeMockMvc.perform(get("/api/ga-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(gaType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getGaType() throws Exception {
        // Initialize the database
        gaTypeRepository.saveAndFlush(gaType);

        // Get the gaType
        restGaTypeMockMvc.perform(get("/api/ga-types/{id}", gaType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(gaType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGaType() throws Exception {
        // Get the gaType
        restGaTypeMockMvc.perform(get("/api/ga-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGaType() throws Exception {
        // Initialize the database
        gaTypeRepository.saveAndFlush(gaType);
        gaTypeSearchRepository.save(gaType);
        int databaseSizeBeforeUpdate = gaTypeRepository.findAll().size();

        // Update the gaType
        GaType updatedGaType = new GaType();
        updatedGaType.setId(gaType.getId());
        updatedGaType.setName(UPDATED_NAME);
        GaTypeDTO gaTypeDTO = gaTypeMapper.gaTypeToGaTypeDTO(updatedGaType);

        restGaTypeMockMvc.perform(put("/api/ga-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gaTypeDTO)))
                .andExpect(status().isOk());

        // Validate the GaType in the database
        List<GaType> gaTypes = gaTypeRepository.findAll();
        assertThat(gaTypes).hasSize(databaseSizeBeforeUpdate);
        GaType testGaType = gaTypes.get(gaTypes.size() - 1);
        assertThat(testGaType.getName()).isEqualTo(UPDATED_NAME);

        // Validate the GaType in ElasticSearch
        GaType gaTypeEs = gaTypeSearchRepository.findOne(testGaType.getId());
        assertThat(gaTypeEs).isEqualToComparingFieldByField(testGaType);
    }

    @Test
    @Transactional
    public void deleteGaType() throws Exception {
        // Initialize the database
        gaTypeRepository.saveAndFlush(gaType);
        gaTypeSearchRepository.save(gaType);
        int databaseSizeBeforeDelete = gaTypeRepository.findAll().size();

        // Get the gaType
        restGaTypeMockMvc.perform(delete("/api/ga-types/{id}", gaType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean gaTypeExistsInEs = gaTypeSearchRepository.exists(gaType.getId());
        assertThat(gaTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<GaType> gaTypes = gaTypeRepository.findAll();
        assertThat(gaTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGaType() throws Exception {
        // Initialize the database
        gaTypeRepository.saveAndFlush(gaType);
        gaTypeSearchRepository.save(gaType);

        // Search the gaType
        restGaTypeMockMvc.perform(get("/api/_search/ga-types?query=id:" + gaType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gaType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
}
