/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.LogRecordRepository;
import es.udc.tic.rnasa.repository.SpecificSimulationRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.repository.search.LogRecordSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.security.SecurityUtils;
import es.udc.tic.rnasa.service.LogRecordService;
import es.udc.tic.rnasa.service.SpecificSimulationService;
import es.udc.tic.rnasa.web.rest.dto.LogRecordDTO;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.LogRecordMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the LogRecordResource REST controller.
 *
 * @see LogRecordResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class LogRecordResourceIntTest {

    private final Logger log = LoggerFactory.getLogger(LogRecordResourceIntTest.class);


    private static final Long DEFAULT_INDEX_RECORD = 1L;
    private static final Long UPDATED_INDEX_RECORD = 2L;

    private static final Long DEFAULT_TIMESTAMP = 1L;
    private static final Long UPDATED_TIMESTAMP = 2L;

    private static final Double DEFAULT_ERROR_TRAIN = 1D;
    private static final Double UPDATED_ERROR_TRAIN = 2D;

    private static final Double DEFAULT_ERROR_VALIDATION = 1D;
    private static final Double UPDATED_ERROR_VALIDATION = 2D;

    private static final Double DEFAULT_ERROR_TEST = 1D;
    private static final Double UPDATED_ERROR_TEST = 2D;

    @Inject
    private CreateEntities createEntities;

    @Inject
    private LogRecordRepository logRecordRepository;

    @Inject
    private LogRecordMapper logRecordMapper;

    @Inject
    private LogRecordService logRecordService;

    @Inject
    private LogRecordSearchRepository logRecordSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restLogRecordMockMvc;

    private LogRecord logRecord;

    @Inject
    private WebApplicationContext context;

    private UserDTO ownerDTO;
    private User owner;
    private String loginname = "joe";
    private String password  = "123456789012345678901234567890123456789012345678901234567890";

    @Inject
    private UserMapper userMapper;
    @Inject
    private UserRepository userRepository;

    @Inject
    private SpecificSimulationService specificSimulationService;
    @Inject
    private SpecificSimulationRepository specificSimulationRepository;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        LogRecordResource logRecordResource = new LogRecordResource();
        ReflectionTestUtils.setField(logRecordResource, "logRecordService", logRecordService);
        ReflectionTestUtils.setField(logRecordResource, "logRecordMapper", logRecordMapper);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);

        SpecificSimulationResource specificSimulationResource = new SpecificSimulationResource();
        ReflectionTestUtils.setField(specificSimulationResource, "specificSimulationService", specificSimulationService);
        ReflectionTestUtils.setField(specificSimulationService, "specificSimulationRepository", specificSimulationRepository);


        this.restLogRecordMockMvc = MockMvcBuilders.standaloneSetup(logRecordResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();


    }

    @Before
    public void initTest() {
        logRecordSearchRepository.deleteAll();
        logRecord = new LogRecord();
        logRecord.setIndexRecord(DEFAULT_INDEX_RECORD);
        logRecord.setTimestamp(DEFAULT_TIMESTAMP);
        logRecord.setErrorTrain(DEFAULT_ERROR_TRAIN);
        logRecord.setErrorValidation(DEFAULT_ERROR_VALIDATION);
        logRecord.setErrorTest(DEFAULT_ERROR_TEST);

        ownerDTO = new UserDTO(
            loginname,                  // login
            password,
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin(loginname).isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }
    }

    @Test
    @Transactional
    public void createLogRecord() throws Exception {
        int databaseSizeBeforeCreate = logRecordRepository.findAll().size();

        // Create the LogRecord
        LogRecordDTO logRecordDTO = logRecordMapper.logRecordToLogRecordDTO(logRecord);

        restLogRecordMockMvc.perform(post("/api/log-records")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(logRecordDTO)))
                .andExpect(status().isCreated());

        // Validate the LogRecord in the database
        List<LogRecord> logRecords = logRecordRepository.findAll();
        assertThat(logRecords).hasSize(databaseSizeBeforeCreate + 1);
        LogRecord testLogRecord = logRecords.get(logRecords.size() - 1);
        assertThat(testLogRecord.getIndexRecord()).isEqualTo(DEFAULT_INDEX_RECORD);
        assertThat(testLogRecord.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testLogRecord.getErrorTrain()).isEqualTo(DEFAULT_ERROR_TRAIN);
        assertThat(testLogRecord.getErrorValidation()).isEqualTo(DEFAULT_ERROR_VALIDATION);
        assertThat(testLogRecord.getErrorTest()).isEqualTo(DEFAULT_ERROR_TEST);

        // Validate the LogRecord in ElasticSearch
        LogRecord logRecordEs = logRecordSearchRepository.findOne(testLogRecord.getId());
        assertThat(logRecordEs).isEqualToComparingFieldByField(testLogRecord);
    }

    @Test
    @Transactional
    public void checkIndexRecordIsRequired() throws Exception {
        int databaseSizeBeforeTest = logRecordRepository.findAll().size();
        // set the field null
        logRecord.setIndexRecord(null);

        // Create the LogRecord, which fails.
        LogRecordDTO logRecordDTO = logRecordMapper.logRecordToLogRecordDTO(logRecord);

        restLogRecordMockMvc.perform(post("/api/log-records")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(logRecordDTO)))
                .andExpect(status().isBadRequest());

        List<LogRecord> logRecords = logRecordRepository.findAll();
        assertThat(logRecords).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTimestampIsRequired() throws Exception {
        int databaseSizeBeforeTest = logRecordRepository.findAll().size();
        // set the field null
        logRecord.setTimestamp(null);

        // Create the LogRecord, which fails.
        LogRecordDTO logRecordDTO = logRecordMapper.logRecordToLogRecordDTO(logRecord);

        restLogRecordMockMvc.perform(post("/api/log-records")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(logRecordDTO)))
                .andExpect(status().isBadRequest());

        List<LogRecord> logRecords = logRecordRepository.findAll();
        assertThat(logRecords).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkErrorTrainIsRequired() throws Exception {
        int databaseSizeBeforeTest = logRecordRepository.findAll().size();
        // set the field null
        logRecord.setErrorTrain(null);

        // Create the LogRecord, which fails.
        LogRecordDTO logRecordDTO = logRecordMapper.logRecordToLogRecordDTO(logRecord);

        restLogRecordMockMvc.perform(post("/api/log-records")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(logRecordDTO)))
                .andExpect(status().isBadRequest());

        List<LogRecord> logRecords = logRecordRepository.findAll();
        assertThat(logRecords).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLogRecords() throws Exception {
        // Initialize the database
        logRecordRepository.saveAndFlush(logRecord);

        // Get all the logRecords
        restLogRecordMockMvc.perform(get("/api/log-records?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(logRecord.getId().intValue())))
                .andExpect(jsonPath("$.[*].indexRecord").value(hasItem(DEFAULT_INDEX_RECORD.intValue())))
                .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
                .andExpect(jsonPath("$.[*].errorTrain").value(hasItem(DEFAULT_ERROR_TRAIN.doubleValue())))
                .andExpect(jsonPath("$.[*].errorValidation").value(hasItem(DEFAULT_ERROR_VALIDATION.doubleValue())))
                .andExpect(jsonPath("$.[*].errorTest").value(hasItem(DEFAULT_ERROR_TEST.doubleValue())));
    }

    @Test
    @Transactional
    public void getLogRecord() throws Exception {
        // Initialize the database
        logRecordRepository.saveAndFlush(logRecord);

        // Get the logRecord
        restLogRecordMockMvc.perform(get("/api/log-records/{id}", logRecord.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(logRecord.getId().intValue()))
            .andExpect(jsonPath("$.indexRecord").value(DEFAULT_INDEX_RECORD.intValue()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.intValue()))
            .andExpect(jsonPath("$.errorTrain").value(DEFAULT_ERROR_TRAIN.doubleValue()))
            .andExpect(jsonPath("$.errorValidation").value(DEFAULT_ERROR_VALIDATION.doubleValue()))
            .andExpect(jsonPath("$.errorTest").value(DEFAULT_ERROR_TEST.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLogRecord() throws Exception {
        // Get the logRecord
        restLogRecordMockMvc.perform(get("/api/log-records/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLogRecord() throws Exception {
        // Initialize the database
        logRecordRepository.saveAndFlush(logRecord);
        logRecordSearchRepository.save(logRecord);
        int databaseSizeBeforeUpdate = logRecordRepository.findAll().size();

        // Update the logRecord
        LogRecord updatedLogRecord = new LogRecord();
        updatedLogRecord.setId(logRecord.getId());
        updatedLogRecord.setIndexRecord(UPDATED_INDEX_RECORD);
        updatedLogRecord.setTimestamp(UPDATED_TIMESTAMP);
        updatedLogRecord.setErrorTrain(UPDATED_ERROR_TRAIN);
        updatedLogRecord.setErrorValidation(UPDATED_ERROR_VALIDATION);
        updatedLogRecord.setErrorTest(UPDATED_ERROR_TEST);
        LogRecordDTO logRecordDTO = logRecordMapper.logRecordToLogRecordDTO(updatedLogRecord);

        restLogRecordMockMvc.perform(put("/api/log-records")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(logRecordDTO)))
                .andExpect(status().isOk());

        // Validate the LogRecord in the database
        List<LogRecord> logRecords = logRecordRepository.findAll();
        assertThat(logRecords).hasSize(databaseSizeBeforeUpdate);
        LogRecord testLogRecord = logRecords.get(logRecords.size() - 1);
        assertThat(testLogRecord.getIndexRecord()).isEqualTo(UPDATED_INDEX_RECORD);
        assertThat(testLogRecord.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testLogRecord.getErrorTrain()).isEqualTo(UPDATED_ERROR_TRAIN);
        assertThat(testLogRecord.getErrorValidation()).isEqualTo(UPDATED_ERROR_VALIDATION);
        assertThat(testLogRecord.getErrorTest()).isEqualTo(UPDATED_ERROR_TEST);

        // Validate the LogRecord in ElasticSearch
        LogRecord logRecordEs = logRecordSearchRepository.findOne(testLogRecord.getId());
        assertThat(logRecordEs).isEqualToComparingFieldByField(testLogRecord);
    }

    @Test
    @Transactional
    public void deleteLogRecord() throws Exception {
        // Initialize the database
        logRecordRepository.saveAndFlush(logRecord);
        logRecordSearchRepository.save(logRecord);
        int databaseSizeBeforeDelete = logRecordRepository.findAll().size();

        // Get the logRecord
        restLogRecordMockMvc.perform(delete("/api/log-records/{id}", logRecord.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean logRecordExistsInEs = logRecordSearchRepository.exists(logRecord.getId());
        assertThat(logRecordExistsInEs).isFalse();

        // Validate the database is empty
        List<LogRecord> logRecords = logRecordRepository.findAll();
        assertThat(logRecords).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLogRecord() throws Exception {
        // Initialize the database
        logRecordRepository.saveAndFlush(logRecord);
        logRecordSearchRepository.save(logRecord);

        // Search the logRecord
        restLogRecordMockMvc.perform(get("/api/_search/log-records?query=id:" + logRecord.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(logRecord.getId().intValue())))
            .andExpect(jsonPath("$.[*].indexRecord").value(hasItem(DEFAULT_INDEX_RECORD.intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.intValue())))
            .andExpect(jsonPath("$.[*].errorTrain").value(hasItem(DEFAULT_ERROR_TRAIN.doubleValue())))
            .andExpect(jsonPath("$.[*].errorValidation").value(hasItem(DEFAULT_ERROR_VALIDATION.doubleValue())))
            .andExpect(jsonPath("$.[*].errorTest").value(hasItem(DEFAULT_ERROR_TEST.doubleValue())));
    }

    @Test
    @Transactional
    public void testAverage() throws Exception {
        Simulation simulation = createSvmSimulation(3,1);

        MockMvc restEntityMock = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();


        log.debug("Simulation {}", simulation);
        log.debug("Simulation Belong {}", simulation.getBelong());

        restEntityMock.perform(get("/api/get-log-average/{simulationId}", simulation.getId())
            .with(user(simulation.getBelong().getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }
    private Simulation createSvmSimulation(int combD, int combP) throws Exception {

        Population population = createEntities.createPopulation(10,0,combP);
        SshServer sshServer  = createEntities.createSshServer("sshserver1.energlia", 1);

        createEntities.createCompiledApp(
            createEntities.createApplication("proves", "0.0.1", ZonedDateTime.now(), true),
            sshServer);

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        Simulation simulation = createEntities.createNuSvmSimulation(
            "Simulation",
            createEntities.createDataset(3, 1, combD, null),
            populations,
            sshServer);

        List<SpecificSimulation> specificSimulations = specificSimulationRepository.findAllByBelongId(simulation.getId());


        int cnt;
        List<LogRecord> logRecords = new ArrayList();
        SpecificSimulation specificSimulation;

        specificSimulation = specificSimulations.get(0);
        for (cnt = 0; cnt < 1000; cnt += 10)
            logRecords.add(new LogRecord(new Long(cnt), cnt*10l, new Double(cnt), null, null, specificSimulation));
        specificSimulation = specificSimulations.get(1);
        for (cnt = 0; cnt < 500; cnt += 15)
            logRecords.add(new LogRecord(new Long(cnt), cnt*10l, new Double(cnt), null, null, specificSimulation));
        specificSimulation = specificSimulations.get(2);
        for (cnt = 0; cnt < 750; cnt += 20)
            logRecords.add(new LogRecord(new Long(cnt), cnt*10l, new Double(cnt), null, null, specificSimulation));

        logRecordRepository.save(logRecords);

        return simulation;
    }

}
