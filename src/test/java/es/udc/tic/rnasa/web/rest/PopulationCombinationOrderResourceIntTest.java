/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.PopulationCombinationOrder;
import es.udc.tic.rnasa.repository.PopulationCombinationOrderRepository;
import es.udc.tic.rnasa.service.PopulationCombinationOrderService;
import es.udc.tic.rnasa.repository.search.PopulationCombinationOrderSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.PopulationCombinationOrderDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationCombinationOrderMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PopulationCombinationOrderResource REST controller.
 *
 * @see PopulationCombinationOrderResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class PopulationCombinationOrderResourceIntTest {


    private static final Integer DEFAULT_INDEX_ORDER = 1;
    private static final Integer UPDATED_INDEX_ORDER = 2;

    @Inject
    private PopulationCombinationOrderRepository populationCombinationOrderRepository;

    @Inject
    private PopulationCombinationOrderMapper populationCombinationOrderMapper;

    @Inject
    private PopulationCombinationOrderService populationCombinationOrderService;

    @Inject
    private PopulationCombinationOrderSearchRepository populationCombinationOrderSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPopulationCombinationOrderMockMvc;

    private PopulationCombinationOrder populationCombinationOrder;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PopulationCombinationOrderResource populationCombinationOrderResource = new PopulationCombinationOrderResource();
        ReflectionTestUtils.setField(populationCombinationOrderResource, "populationCombinationOrderService", populationCombinationOrderService);
        ReflectionTestUtils.setField(populationCombinationOrderResource, "populationCombinationOrderMapper", populationCombinationOrderMapper);
        this.restPopulationCombinationOrderMockMvc = MockMvcBuilders.standaloneSetup(populationCombinationOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        populationCombinationOrderSearchRepository.deleteAll();
        populationCombinationOrder = new PopulationCombinationOrder();
        populationCombinationOrder.setIndexOrder(DEFAULT_INDEX_ORDER);
    }

    @Test
    @Transactional
    public void createPopulationCombinationOrder() throws Exception {
        int databaseSizeBeforeCreate = populationCombinationOrderRepository.findAll().size();

        // Create the PopulationCombinationOrder
        PopulationCombinationOrderDTO populationCombinationOrderDTO = populationCombinationOrderMapper.populationCombinationOrderToPopulationCombinationOrderDTO(populationCombinationOrder);

        restPopulationCombinationOrderMockMvc.perform(post("/api/population-combination-orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationCombinationOrderDTO)))
                .andExpect(status().isCreated());

        // Validate the PopulationCombinationOrder in the database
        List<PopulationCombinationOrder> populationCombinationOrders = populationCombinationOrderRepository.findAll();
        assertThat(populationCombinationOrders).hasSize(databaseSizeBeforeCreate + 1);
        PopulationCombinationOrder testPopulationCombinationOrder = populationCombinationOrders.get(populationCombinationOrders.size() - 1);
        assertThat(testPopulationCombinationOrder.getIndexOrder()).isEqualTo(DEFAULT_INDEX_ORDER);

        // Validate the PopulationCombinationOrder in ElasticSearch
        PopulationCombinationOrder populationCombinationOrderEs = populationCombinationOrderSearchRepository.findOne(testPopulationCombinationOrder.getId());
        assertThat(populationCombinationOrderEs).isEqualToComparingFieldByField(testPopulationCombinationOrder);
    }

    @Test
    @Transactional
    public void checkIndexOrderIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationCombinationOrderRepository.findAll().size();
        // set the field null
        populationCombinationOrder.setIndexOrder(null);

        // Create the PopulationCombinationOrder, which fails.
        PopulationCombinationOrderDTO populationCombinationOrderDTO = populationCombinationOrderMapper.populationCombinationOrderToPopulationCombinationOrderDTO(populationCombinationOrder);

        restPopulationCombinationOrderMockMvc.perform(post("/api/population-combination-orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationCombinationOrderDTO)))
                .andExpect(status().isBadRequest());

        List<PopulationCombinationOrder> populationCombinationOrders = populationCombinationOrderRepository.findAll();
        assertThat(populationCombinationOrders).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPopulationCombinationOrders() throws Exception {
        // Initialize the database
        populationCombinationOrderRepository.saveAndFlush(populationCombinationOrder);

        // Get all the populationCombinationOrders
        restPopulationCombinationOrderMockMvc.perform(get("/api/population-combination-orders?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(populationCombinationOrder.getId().intValue())))
                .andExpect(jsonPath("$.[*].indexOrder").value(hasItem(DEFAULT_INDEX_ORDER)));
    }

    @Test
    @Transactional
    public void getPopulationCombinationOrder() throws Exception {
        // Initialize the database
        populationCombinationOrderRepository.saveAndFlush(populationCombinationOrder);

        // Get the populationCombinationOrder
        restPopulationCombinationOrderMockMvc.perform(get("/api/population-combination-orders/{id}", populationCombinationOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(populationCombinationOrder.getId().intValue()))
            .andExpect(jsonPath("$.indexOrder").value(DEFAULT_INDEX_ORDER));
    }

    @Test
    @Transactional
    public void getNonExistingPopulationCombinationOrder() throws Exception {
        // Get the populationCombinationOrder
        restPopulationCombinationOrderMockMvc.perform(get("/api/population-combination-orders/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePopulationCombinationOrder() throws Exception {
        // Initialize the database
        populationCombinationOrderRepository.saveAndFlush(populationCombinationOrder);
        populationCombinationOrderSearchRepository.save(populationCombinationOrder);
        int databaseSizeBeforeUpdate = populationCombinationOrderRepository.findAll().size();

        // Update the populationCombinationOrder
        PopulationCombinationOrder updatedPopulationCombinationOrder = new PopulationCombinationOrder();
        updatedPopulationCombinationOrder.setId(populationCombinationOrder.getId());
        updatedPopulationCombinationOrder.setIndexOrder(UPDATED_INDEX_ORDER);
        PopulationCombinationOrderDTO populationCombinationOrderDTO = populationCombinationOrderMapper.populationCombinationOrderToPopulationCombinationOrderDTO(updatedPopulationCombinationOrder);

        restPopulationCombinationOrderMockMvc.perform(put("/api/population-combination-orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationCombinationOrderDTO)))
                .andExpect(status().isOk());

        // Validate the PopulationCombinationOrder in the database
        List<PopulationCombinationOrder> populationCombinationOrders = populationCombinationOrderRepository.findAll();
        assertThat(populationCombinationOrders).hasSize(databaseSizeBeforeUpdate);
        PopulationCombinationOrder testPopulationCombinationOrder = populationCombinationOrders.get(populationCombinationOrders.size() - 1);
        assertThat(testPopulationCombinationOrder.getIndexOrder()).isEqualTo(UPDATED_INDEX_ORDER);

        // Validate the PopulationCombinationOrder in ElasticSearch
        PopulationCombinationOrder populationCombinationOrderEs = populationCombinationOrderSearchRepository.findOne(testPopulationCombinationOrder.getId());
        assertThat(populationCombinationOrderEs).isEqualToComparingFieldByField(testPopulationCombinationOrder);
    }

    @Test
    @Transactional
    public void deletePopulationCombinationOrder() throws Exception {
        // Initialize the database
        populationCombinationOrderRepository.saveAndFlush(populationCombinationOrder);
        populationCombinationOrderSearchRepository.save(populationCombinationOrder);
        int databaseSizeBeforeDelete = populationCombinationOrderRepository.findAll().size();

        // Get the populationCombinationOrder
        restPopulationCombinationOrderMockMvc.perform(delete("/api/population-combination-orders/{id}", populationCombinationOrder.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean populationCombinationOrderExistsInEs = populationCombinationOrderSearchRepository.exists(populationCombinationOrder.getId());
        assertThat(populationCombinationOrderExistsInEs).isFalse();

        // Validate the database is empty
        List<PopulationCombinationOrder> populationCombinationOrders = populationCombinationOrderRepository.findAll();
        assertThat(populationCombinationOrders).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPopulationCombinationOrder() throws Exception {
        // Initialize the database
        populationCombinationOrderRepository.saveAndFlush(populationCombinationOrder);
        populationCombinationOrderSearchRepository.save(populationCombinationOrder);

        // Search the populationCombinationOrder
        restPopulationCombinationOrderMockMvc.perform(get("/api/_search/population-combination-orders?query=id:" + populationCombinationOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(populationCombinationOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].indexOrder").value(hasItem(DEFAULT_INDEX_ORDER)));
    }
}
