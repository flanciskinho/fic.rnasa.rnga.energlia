/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.PopulationCombination;
import es.udc.tic.rnasa.repository.PopulationCombinationRepository;
import es.udc.tic.rnasa.repository.search.PopulationCombinationSearchRepository;
import es.udc.tic.rnasa.service.PopulationCombinationService;
import es.udc.tic.rnasa.service.PopulationServiceIntTest;
import es.udc.tic.rnasa.web.rest.dto.PopulationCombinationDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationCombinationMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PopulationCombinationResource REST controller.
 *
 * @see PopulationCombinationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class PopulationCombinationResourceIntTest {

    private final Logger log = LoggerFactory.getLogger(PopulationCombinationResourceIntTest.class);


    private static final Integer DEFAULT_INDEX = 1;
    private static final Integer UPDATED_INDEX = 2;

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @Inject
    private PopulationCombinationMapper populationCombinationMapper;

    @Inject
    private PopulationCombinationService populationCombinationService;

    @Inject
    private PopulationCombinationSearchRepository populationCombinationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPopulationCombinationMockMvc;

    private PopulationCombination populationCombination;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PopulationCombinationResource populationCombinationResource = new PopulationCombinationResource();
        ReflectionTestUtils.setField(populationCombinationResource, "populationCombinationService", populationCombinationService);
        ReflectionTestUtils.setField(populationCombinationResource, "populationCombinationMapper", populationCombinationMapper);
        this.restPopulationCombinationMockMvc = MockMvcBuilders.standaloneSetup(populationCombinationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        populationCombinationSearchRepository.deleteAll();
        populationCombination = new PopulationCombination();
        populationCombination.setIndexCombination(DEFAULT_INDEX);
        populationCombination.setFile(DEFAULT_FILE);
        populationCombination.setFileContentType(DEFAULT_FILE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createPopulationCombination() throws Exception {
        int databaseSizeBeforeCreate = populationCombinationRepository.findAll().size();

        // Create the PopulationCombination
        PopulationCombinationDTO populationCombinationDTO = populationCombinationMapper.populationCombinationToPopulationCombinationDTO(populationCombination);

        restPopulationCombinationMockMvc.perform(post("/api/population-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationCombinationDTO)))
                .andExpect(status().isCreated());

        // Validate the PopulationCombination in the database
        List<PopulationCombination> populationCombinations = populationCombinationRepository.findAll();
        assertThat(populationCombinations).hasSize(databaseSizeBeforeCreate + 1);
        PopulationCombination testPopulationCombination = populationCombinations.get(populationCombinations.size() - 1);
        assertThat(testPopulationCombination.getIndexCombination()).isEqualTo(DEFAULT_INDEX);
        assertThat(testPopulationCombination.getFile()).isEqualTo(DEFAULT_FILE);
        assertThat(testPopulationCombination.getFileContentType()).isEqualTo(DEFAULT_FILE_CONTENT_TYPE);

        // Validate the PopulationCombination in ElasticSearch
        PopulationCombination populationCombinationEs = populationCombinationSearchRepository.findOne(testPopulationCombination.getId());
        assertThat(populationCombinationEs).isEqualToComparingFieldByField(testPopulationCombination);
    }

    @Test
    @Transactional
    public void checkIndexIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationCombinationRepository.findAll().size();
        // set the field null
        populationCombination.setIndexCombination(null);

        // Create the PopulationCombination, which fails.
        PopulationCombinationDTO populationCombinationDTO = populationCombinationMapper.populationCombinationToPopulationCombinationDTO(populationCombination);

        restPopulationCombinationMockMvc.perform(post("/api/population-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationCombinationDTO)))
                .andExpect(status().isBadRequest());

        List<PopulationCombination> populationCombinations = populationCombinationRepository.findAll();
        assertThat(populationCombinations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFileIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationCombinationRepository.findAll().size();
        // set the field null
        populationCombination.setFile(null);

        // Create the PopulationCombination, which fails.
        PopulationCombinationDTO populationCombinationDTO = populationCombinationMapper.populationCombinationToPopulationCombinationDTO(populationCombination);

        restPopulationCombinationMockMvc.perform(post("/api/population-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationCombinationDTO)))
                .andExpect(status().isBadRequest());

        List<PopulationCombination> populationCombinations = populationCombinationRepository.findAll();
        assertThat(populationCombinations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPopulationCombinations() throws Exception {
        // Initialize the database
        populationCombinationRepository.saveAndFlush(populationCombination);

        // Get all the populationCombinations
        restPopulationCombinationMockMvc.perform(get("/api/population-combinations?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(populationCombination.getId().intValue())))
                .andExpect(jsonPath("$.[*].indexCombination").value(hasItem(DEFAULT_INDEX)))
                .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))));
    }

    @Test
    @Transactional
    public void getPopulationCombination() throws Exception {
        // Initialize the database
        populationCombinationRepository.saveAndFlush(populationCombination);

        // Get the populationCombination
        restPopulationCombinationMockMvc.perform(get("/api/population-combinations/{id}", populationCombination.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(populationCombination.getId().intValue()))
            .andExpect(jsonPath("$.indexCombination").value(DEFAULT_INDEX))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)));
    }

    @Test
    @Transactional
    public void getNonExistingPopulationCombination() throws Exception {
        // Get the populationCombination
        restPopulationCombinationMockMvc.perform(get("/api/population-combinations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePopulationCombination() throws Exception {
        // Initialize the database
        populationCombinationRepository.saveAndFlush(populationCombination);
        populationCombinationSearchRepository.save(populationCombination);
        int databaseSizeBeforeUpdate = populationCombinationRepository.findAll().size();

        // Update the populationCombination
        PopulationCombination updatedPopulationCombination = new PopulationCombination();
        updatedPopulationCombination.setId(populationCombination.getId());
        updatedPopulationCombination.setIndexCombination(UPDATED_INDEX);
        updatedPopulationCombination.setFile(UPDATED_FILE);
        updatedPopulationCombination.setFileContentType(UPDATED_FILE_CONTENT_TYPE);
        PopulationCombinationDTO populationCombinationDTO = populationCombinationMapper.populationCombinationToPopulationCombinationDTO(updatedPopulationCombination);

        restPopulationCombinationMockMvc.perform(put("/api/population-combinations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationCombinationDTO)))
                .andExpect(status().isOk());

        // Validate the PopulationCombination in the database
        List<PopulationCombination> populationCombinations = populationCombinationRepository.findAll();
        assertThat(populationCombinations).hasSize(databaseSizeBeforeUpdate);
        PopulationCombination testPopulationCombination = populationCombinations.get(populationCombinations.size() - 1);
        assertThat(testPopulationCombination.getIndexCombination()).isEqualTo(UPDATED_INDEX);
        assertThat(testPopulationCombination.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testPopulationCombination.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);

        // Validate the PopulationCombination in ElasticSearch
        PopulationCombination populationCombinationEs = populationCombinationSearchRepository.findOne(testPopulationCombination.getId());
        assertThat(populationCombinationEs).isEqualToComparingFieldByField(testPopulationCombination);
    }

    @Test
    @Transactional
    public void deletePopulationCombination() throws Exception {
        // Initialize the database
        populationCombinationRepository.saveAndFlush(populationCombination);
        populationCombinationSearchRepository.save(populationCombination);
        int databaseSizeBeforeDelete = populationCombinationRepository.findAll().size();

        // Get the populationCombination
        restPopulationCombinationMockMvc.perform(delete("/api/population-combinations/{id}", populationCombination.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean populationCombinationExistsInEs = populationCombinationSearchRepository.exists(populationCombination.getId());
        assertThat(populationCombinationExistsInEs).isFalse();

        // Validate the database is empty
        List<PopulationCombination> populationCombinations = populationCombinationRepository.findAll();
        assertThat(populationCombinations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPopulationCombination() throws Exception {
        // Initialize the database
        populationCombinationRepository.saveAndFlush(populationCombination);
        populationCombinationSearchRepository.save(populationCombination);

        // Search the populationCombination
        restPopulationCombinationMockMvc.perform(get("/api/_search/population-combinations?query=id:" + populationCombination.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(populationCombination.getId().intValue())))
            .andExpect(jsonPath("$.[*].indexCombination").value(hasItem(DEFAULT_INDEX)))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))));
    }
}
