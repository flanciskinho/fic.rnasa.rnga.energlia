/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.PopulationOrder;
import es.udc.tic.rnasa.repository.PopulationOrderRepository;
import es.udc.tic.rnasa.service.PopulationOrderService;
import es.udc.tic.rnasa.repository.search.PopulationOrderSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.PopulationOrderDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationOrderMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PopulationOrderResource REST controller.
 *
 * @see PopulationOrderResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class PopulationOrderResourceIntTest {


    private static final Integer DEFAULT_INDEX_ORDER = 1;
    private static final Integer UPDATED_INDEX_ORDER = 2;

    @Inject
    private PopulationOrderRepository populationOrderRepository;

    @Inject
    private PopulationOrderMapper populationOrderMapper;

    @Inject
    private PopulationOrderService populationOrderService;

    @Inject
    private PopulationOrderSearchRepository populationOrderSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPopulationOrderMockMvc;

    private PopulationOrder populationOrder;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PopulationOrderResource populationOrderResource = new PopulationOrderResource();
        ReflectionTestUtils.setField(populationOrderResource, "populationOrderService", populationOrderService);
        ReflectionTestUtils.setField(populationOrderResource, "populationOrderMapper", populationOrderMapper);
        this.restPopulationOrderMockMvc = MockMvcBuilders.standaloneSetup(populationOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        populationOrderSearchRepository.deleteAll();
        populationOrder = new PopulationOrder();
        populationOrder.setIndexOrder(DEFAULT_INDEX_ORDER);
    }

    @Test
    @Transactional
    public void createPopulationOrder() throws Exception {
        int databaseSizeBeforeCreate = populationOrderRepository.findAll().size();

        // Create the PopulationOrder
        PopulationOrderDTO populationOrderDTO = populationOrderMapper.populationOrderToPopulationOrderDTO(populationOrder);

        restPopulationOrderMockMvc.perform(post("/api/population-orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationOrderDTO)))
                .andExpect(status().isCreated());

        // Validate the PopulationOrder in the database
        List<PopulationOrder> populationOrders = populationOrderRepository.findAll();
        assertThat(populationOrders).hasSize(databaseSizeBeforeCreate + 1);
        PopulationOrder testPopulationOrder = populationOrders.get(populationOrders.size() - 1);
        assertThat(testPopulationOrder.getIndexOrder()).isEqualTo(DEFAULT_INDEX_ORDER);

        // Validate the PopulationOrder in ElasticSearch
        PopulationOrder populationOrderEs = populationOrderSearchRepository.findOne(testPopulationOrder.getId());
        assertThat(populationOrderEs).isEqualToComparingFieldByField(testPopulationOrder);
    }

    @Test
    @Transactional
    public void checkIndexOrderIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationOrderRepository.findAll().size();
        // set the field null
        populationOrder.setIndexOrder(null);

        // Create the PopulationOrder, which fails.
        PopulationOrderDTO populationOrderDTO = populationOrderMapper.populationOrderToPopulationOrderDTO(populationOrder);

        restPopulationOrderMockMvc.perform(post("/api/population-orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationOrderDTO)))
                .andExpect(status().isBadRequest());

        List<PopulationOrder> populationOrders = populationOrderRepository.findAll();
        assertThat(populationOrders).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPopulationOrders() throws Exception {
        // Initialize the database
        populationOrderRepository.saveAndFlush(populationOrder);

        // Get all the populationOrders
        restPopulationOrderMockMvc.perform(get("/api/population-orders?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(populationOrder.getId().intValue())))
                .andExpect(jsonPath("$.[*].indexOrder").value(hasItem(DEFAULT_INDEX_ORDER)));
    }

    @Test
    @Transactional
    public void getPopulationOrder() throws Exception {
        // Initialize the database
        populationOrderRepository.saveAndFlush(populationOrder);

        // Get the populationOrder
        restPopulationOrderMockMvc.perform(get("/api/population-orders/{id}", populationOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(populationOrder.getId().intValue()))
            .andExpect(jsonPath("$.indexOrder").value(DEFAULT_INDEX_ORDER));
    }

    @Test
    @Transactional
    public void getNonExistingPopulationOrder() throws Exception {
        // Get the populationOrder
        restPopulationOrderMockMvc.perform(get("/api/population-orders/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePopulationOrder() throws Exception {
        // Initialize the database
        populationOrderRepository.saveAndFlush(populationOrder);
        populationOrderSearchRepository.save(populationOrder);
        int databaseSizeBeforeUpdate = populationOrderRepository.findAll().size();

        // Update the populationOrder
        PopulationOrder updatedPopulationOrder = new PopulationOrder();
        updatedPopulationOrder.setId(populationOrder.getId());
        updatedPopulationOrder.setIndexOrder(UPDATED_INDEX_ORDER);
        PopulationOrderDTO populationOrderDTO = populationOrderMapper.populationOrderToPopulationOrderDTO(updatedPopulationOrder);

        restPopulationOrderMockMvc.perform(put("/api/population-orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationOrderDTO)))
                .andExpect(status().isOk());

        // Validate the PopulationOrder in the database
        List<PopulationOrder> populationOrders = populationOrderRepository.findAll();
        assertThat(populationOrders).hasSize(databaseSizeBeforeUpdate);
        PopulationOrder testPopulationOrder = populationOrders.get(populationOrders.size() - 1);
        assertThat(testPopulationOrder.getIndexOrder()).isEqualTo(UPDATED_INDEX_ORDER);

        // Validate the PopulationOrder in ElasticSearch
        PopulationOrder populationOrderEs = populationOrderSearchRepository.findOne(testPopulationOrder.getId());
        assertThat(populationOrderEs).isEqualToComparingFieldByField(testPopulationOrder);
    }

    @Test
    @Transactional
    public void deletePopulationOrder() throws Exception {
        // Initialize the database
        populationOrderRepository.saveAndFlush(populationOrder);
        populationOrderSearchRepository.save(populationOrder);
        int databaseSizeBeforeDelete = populationOrderRepository.findAll().size();

        // Get the populationOrder
        restPopulationOrderMockMvc.perform(delete("/api/population-orders/{id}", populationOrder.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean populationOrderExistsInEs = populationOrderSearchRepository.exists(populationOrder.getId());
        assertThat(populationOrderExistsInEs).isFalse();

        // Validate the database is empty
        List<PopulationOrder> populationOrders = populationOrderRepository.findAll();
        assertThat(populationOrders).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPopulationOrder() throws Exception {
        // Initialize the database
        populationOrderRepository.saveAndFlush(populationOrder);
        populationOrderSearchRepository.save(populationOrder);

        // Search the populationOrder
        restPopulationOrderMockMvc.perform(get("/api/_search/population-orders?query=id:" + populationOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(populationOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].indexOrder").value(hasItem(DEFAULT_INDEX_ORDER)));
    }
}
