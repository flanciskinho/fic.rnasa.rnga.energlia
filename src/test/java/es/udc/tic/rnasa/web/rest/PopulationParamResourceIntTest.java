/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.PopulationParam;
import es.udc.tic.rnasa.repository.PopulationParamRepository;
import es.udc.tic.rnasa.service.PopulationParamService;
import es.udc.tic.rnasa.repository.search.PopulationParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.PopulationParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.PopulationParamMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PopulationParamResource REST controller.
 *
 * @see PopulationParamResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class PopulationParamResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_VALUE = "AAAAA";
    private static final String UPDATED_VALUE = "BBBBB";

    @Inject
    private PopulationParamRepository populationParamRepository;

    @Inject
    private PopulationParamMapper populationParamMapper;

    @Inject
    private PopulationParamService populationParamService;

    @Inject
    private PopulationParamSearchRepository populationParamSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPopulationParamMockMvc;

    private PopulationParam populationParam;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PopulationParamResource populationParamResource = new PopulationParamResource();
        ReflectionTestUtils.setField(populationParamResource, "populationParamService", populationParamService);
        ReflectionTestUtils.setField(populationParamResource, "populationParamMapper", populationParamMapper);
        this.restPopulationParamMockMvc = MockMvcBuilders.standaloneSetup(populationParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        populationParamSearchRepository.deleteAll();
        populationParam = new PopulationParam();
        populationParam.setName(DEFAULT_NAME);
        populationParam.setValue(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createPopulationParam() throws Exception {
        int databaseSizeBeforeCreate = populationParamRepository.findAll().size();

        // Create the PopulationParam
        PopulationParamDTO populationParamDTO = populationParamMapper.populationParamToPopulationParamDTO(populationParam);

        restPopulationParamMockMvc.perform(post("/api/population-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationParamDTO)))
                .andExpect(status().isCreated());

        // Validate the PopulationParam in the database
        List<PopulationParam> populationParams = populationParamRepository.findAll();
        assertThat(populationParams).hasSize(databaseSizeBeforeCreate + 1);
        PopulationParam testPopulationParam = populationParams.get(populationParams.size() - 1);
        assertThat(testPopulationParam.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPopulationParam.getValue()).isEqualTo(DEFAULT_VALUE);

        // Validate the PopulationParam in ElasticSearch
        PopulationParam populationParamEs = populationParamSearchRepository.findOne(testPopulationParam.getId());
        assertThat(populationParamEs).isEqualToComparingFieldByField(testPopulationParam);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationParamRepository.findAll().size();
        // set the field null
        populationParam.setName(null);

        // Create the PopulationParam, which fails.
        PopulationParamDTO populationParamDTO = populationParamMapper.populationParamToPopulationParamDTO(populationParam);

        restPopulationParamMockMvc.perform(post("/api/population-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationParamDTO)))
                .andExpect(status().isBadRequest());

        List<PopulationParam> populationParams = populationParamRepository.findAll();
        assertThat(populationParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationParamRepository.findAll().size();
        // set the field null
        populationParam.setValue(null);

        // Create the PopulationParam, which fails.
        PopulationParamDTO populationParamDTO = populationParamMapper.populationParamToPopulationParamDTO(populationParam);

        restPopulationParamMockMvc.perform(post("/api/population-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationParamDTO)))
                .andExpect(status().isBadRequest());

        List<PopulationParam> populationParams = populationParamRepository.findAll();
        assertThat(populationParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPopulationParams() throws Exception {
        // Initialize the database
        populationParamRepository.saveAndFlush(populationParam);

        // Get all the populationParams
        restPopulationParamMockMvc.perform(get("/api/population-params?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(populationParam.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getPopulationParam() throws Exception {
        // Initialize the database
        populationParamRepository.saveAndFlush(populationParam);

        // Get the populationParam
        restPopulationParamMockMvc.perform(get("/api/population-params/{id}", populationParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(populationParam.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPopulationParam() throws Exception {
        // Get the populationParam
        restPopulationParamMockMvc.perform(get("/api/population-params/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePopulationParam() throws Exception {
        // Initialize the database
        populationParamRepository.saveAndFlush(populationParam);
        populationParamSearchRepository.save(populationParam);
        int databaseSizeBeforeUpdate = populationParamRepository.findAll().size();

        // Update the populationParam
        PopulationParam updatedPopulationParam = new PopulationParam();
        updatedPopulationParam.setId(populationParam.getId());
        updatedPopulationParam.setName(UPDATED_NAME);
        updatedPopulationParam.setValue(UPDATED_VALUE);
        PopulationParamDTO populationParamDTO = populationParamMapper.populationParamToPopulationParamDTO(updatedPopulationParam);

        restPopulationParamMockMvc.perform(put("/api/population-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(populationParamDTO)))
                .andExpect(status().isOk());

        // Validate the PopulationParam in the database
        List<PopulationParam> populationParams = populationParamRepository.findAll();
        assertThat(populationParams).hasSize(databaseSizeBeforeUpdate);
        PopulationParam testPopulationParam = populationParams.get(populationParams.size() - 1);
        assertThat(testPopulationParam.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPopulationParam.getValue()).isEqualTo(UPDATED_VALUE);

        // Validate the PopulationParam in ElasticSearch
        PopulationParam populationParamEs = populationParamSearchRepository.findOne(testPopulationParam.getId());
        assertThat(populationParamEs).isEqualToComparingFieldByField(testPopulationParam);
    }

    @Test
    @Transactional
    public void deletePopulationParam() throws Exception {
        // Initialize the database
        populationParamRepository.saveAndFlush(populationParam);
        populationParamSearchRepository.save(populationParam);
        int databaseSizeBeforeDelete = populationParamRepository.findAll().size();

        // Get the populationParam
        restPopulationParamMockMvc.perform(delete("/api/population-params/{id}", populationParam.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean populationParamExistsInEs = populationParamSearchRepository.exists(populationParam.getId());
        assertThat(populationParamExistsInEs).isFalse();

        // Validate the database is empty
        List<PopulationParam> populationParams = populationParamRepository.findAll();
        assertThat(populationParams).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPopulationParam() throws Exception {
        // Initialize the database
        populationParamRepository.saveAndFlush(populationParam);
        populationParamSearchRepository.save(populationParam);

        // Search the populationParam
        restPopulationParamMockMvc.perform(get("/api/_search/population-params?query=id:" + populationParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(populationParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }
}
