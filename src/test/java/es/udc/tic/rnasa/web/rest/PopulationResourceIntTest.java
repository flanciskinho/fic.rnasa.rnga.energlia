/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.PopulationRepository;
import es.udc.tic.rnasa.repository.SizeParamRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.repository.search.PopulationSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.PopulationService;
import es.udc.tic.rnasa.service.SizeTypeService;
import es.udc.tic.rnasa.service.UserService;
import es.udc.tic.rnasa.web.rest.dto.*;
import es.udc.tic.rnasa.web.rest.mapper.PopulationMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import es.udc.tic.rnasa.web.rest.util.TestUtilPrivacy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Test class for the PopulationResource REST controller.
 *
 * @see PopulationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class PopulationResourceIntTest {

    private final Logger log = LoggerFactory.getLogger(PopulationResourceIntTest.class);

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TIMESTAMP_STR = dateTimeFormatter.format(DEFAULT_TIMESTAMP);

    private static final Integer DEFAULT_NUM_INDIVIDUAL = 1;
    private static final Integer UPDATED_NUM_INDIVIDUAL = 2;

    private static final Integer DEFAULT_NUM_COMBINATION = 1;
    private static final Integer UPDATED_NUM_COMBINATION = 2;

    private static final Integer DEFAULT_SIZE_GENOTYPE = 1;
    private static final Integer UPDATED_SIZE_GENOTYPE = 2;

    @Inject
    private CreateEntities createEntities;

    @Inject
    private SizeParamRepository sizeParamRepository;

    @Inject
    private SizeTypeService sizeTypeService;


    @Inject
    private PopulationRepository populationRepository;

    @Inject
    private PopulationMapper populationMapper;

    @Inject
    private UserService userService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;

    @Inject
    private PopulationService populationService;

    @Inject
    private PopulationSearchRepository populationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private WebApplicationContext context;

    private MockMvc restPopulationMockMvc;

    private Population population;
    private PopulationDTO populationDTO;

    private User owner;
    private UserDTO ownerDTO;

    private TestUtilPrivacy testUtilPrivacy;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);

        PopulationResource populationResource = new PopulationResource();
        ReflectionTestUtils.setField(populationResource, "populationService", populationService);
        ReflectionTestUtils.setField(populationResource, "populationMapper", populationMapper);

        ReflectionTestUtils.setField(populationService, "sizeParamRepository", sizeParamRepository);
        ReflectionTestUtils.setField(populationService, "sizeTypeService", sizeTypeService);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userService", userService);
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);


        this.restPopulationMockMvc = MockMvcBuilders.standaloneSetup(populationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        populationSearchRepository.deleteAll();

        ownerDTO = new UserDTO(
            "joe",                  // login
            "123456789012345678901234567890123456789012345678901234567890",
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin("joe").isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }

        population = new Population();
        population.setName(DEFAULT_NAME);
        population.setDescription(DEFAULT_DESCRIPTION);
        population.setTimestamp(DEFAULT_TIMESTAMP);
        population.setNumIndividual(DEFAULT_NUM_INDIVIDUAL);
        population.setNumCombination(DEFAULT_NUM_COMBINATION);
        population.setSizeGenotype(DEFAULT_SIZE_GENOTYPE);
        population.setOwner(owner);

        populationDTO = populationMapper.populationToPopulationDTO(population);

        testUtilPrivacy = new TestUtilPrivacy(populationRepository, context,
            "/api/populations/{id}",
            "/api/populations",
            "/api/populations",
            "/api/populations/{id}",
            "/api/populations?sort=id,desc");
    }

    //
    // Custom-resource
    //
    private String getRealPopulationv1_0(List<Map<Integer, Short>> i_short, List<Map<Integer, Float>> i_float) {
        String frame = "<population version='1.0' encode='2' num_individuals='%d' s_genoshort='%d' s_genofloat='%d'>\n%s</population>";
        String ind = "\t<individual version='1.0' encode='2'>\n%s\t</individual>\n";
        String g_short = "\t\t<genoshort id='%d' value='%d' />\n";
        String g_float = "\t\t<genofloat id='%d' value='%f' />\n";

        StringBuffer str1 = new StringBuffer();
        StringBuffer str2;

        for (int index = 0; index < Math.max(i_short.size(), i_float.size()); index++) {
            str2 = new StringBuffer();


            if (index < i_short.size())
                for (Map.Entry<Integer, Short> entry : i_short.get(index).entrySet()) {
                    str2.append(String.format(g_short, entry.getKey(), entry.getValue()));
                }

            if (index < i_float.size())
                for (Map.Entry<Integer, Float> entry: i_float.get(index).entrySet()) {
                    str2.append(String.format(g_float, entry.getKey(), entry.getValue()));
                }

            str1.append(String.format(ind, str2.toString()));
        }


        int numIndividuals = Math.max(i_short.size(), i_float.size());
        return String.format(frame,
            numIndividuals,
            numIndividuals*(i_short.size() == 0?0:i_short.get(0).size()),
            numIndividuals*(i_float.size()== 0?0:i_float.get(0).size()),
            str1.toString());
    }

    public List<Map<Integer, Short>> getShortIndividual(int sizeIndividual, int sizeGeno, int startValue) {
        int cnt, tmp;

        List<Map<Integer, Short>> i_short = new ArrayList<>();
        Map<Integer, Short> m_short;

        for (tmp = 0; tmp < sizeIndividual; tmp++) {
            m_short = new HashMap();
            for (cnt = 0; cnt < sizeGeno; cnt++)
                m_short.put(cnt, (short) (cnt + tmp * sizeGeno + startValue));
            i_short.add(m_short);
        }

        return i_short;
    }

    public List<Map<Integer, Float>> getFloatIndividual(int sizeIndividual, int sizeGeno, int startValue) {
        int cnt, tmp;

        List<Map<Integer, Float>> i_float = new ArrayList<>();
        Map<Integer, Float> m_float;

        for (tmp = 0; tmp < sizeIndividual; tmp++) {
            m_float = new HashMap();
            for (cnt = 0; cnt < sizeGeno; cnt++)
                m_float.put(cnt, ((float) (cnt + tmp * sizeGeno + startValue) + 0.5f) * (cnt %2 == 0?1f:-1f));
            i_float.add(m_float);
        }

        return i_float;
    }


    @Test
    @Transactional
    public void createCustomPopulationOnwer() throws Exception {
        Locale.setDefault(Locale.ENGLISH);

        int databaseSizeBeforeCreate = populationRepository.findAll().size();

        testUtilPrivacy = new TestUtilPrivacy(populationRepository, context,
            "/api/custom-populations/{id}",
            "/api/custom-populations",
            "/api/custom-populations",
            "/api/custom-populations/{id}",
            "/api/custom-populations?sort=id,desc");

        int numIndividual = 2;
        int sizeGenoShort = 3;
        int sizeGenoFloat = 4;

        List<Map<Integer, Short>> i1_short = getShortIndividual(numIndividual, sizeGenoShort, 0);
        List<Map<Integer, Float>> i1_float = getFloatIndividual(numIndividual, sizeGenoFloat, 0);

        List<Map<Integer, Short>> i2_short = getShortIndividual(numIndividual, sizeGenoShort, numIndividual*sizeGenoShort);
        List<Map<Integer, Float>> i2_float = getFloatIndividual(numIndividual, sizeGenoFloat, numIndividual*sizeGenoFloat);

        List<byte []> files = new ArrayList<>(2);
        files.add(getRealPopulationv1_0(i1_short, i1_float).getBytes());
        files.add(getRealPopulationv1_0(i2_short, i2_float).getBytes());

        files.stream().forEach(f -> log.debug("file[{}]:\n{}", files.indexOf(f), new String(f)));

        CustomPopulationDTO customPopulationDTO = new CustomPopulationDTO(null, DEFAULT_NAME, DEFAULT_DESCRIPTION, files);

        ZonedDateTime zonedDateTime = ZonedDateTime.now();

        // Create the Population
        testUtilPrivacy.createEntity(customPopulationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeCreate + 1);
        Population testPopulation = populations.get(populations.size() - 1);
        assertThat(testPopulation.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPopulation.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPopulation.getTimestamp()).isAfter(zonedDateTime);
        assertThat(testPopulation.getNumIndividual()).isEqualTo(numIndividual);
        assertThat(testPopulation.getNumCombination()).isEqualTo(files.size());
        assertThat(testPopulation.getSizeGenotype()).isEqualTo((sizeGenoShort+sizeGenoFloat));

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }

    @Test
    @Transactional
    public void updateCustomPopulation() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeUpdate = populationRepository.findAll().size();

        testUtilPrivacy = new TestUtilPrivacy(populationRepository, context,
            "/api/custom-populations/{id}",
            "/api/custom-populations",
            "/api/custom-populations",
            "/api/custom-populations/{id}",
            "/api/custom-populations?sort=id,desc");

        // Update the population
        CustomDescriptionDTO customDescriptionDTO = new CustomDescriptionDTO(population.getId(), UPDATED_DESCRIPTION);
        testUtilPrivacy.updateEntity(populationDTO, customDescriptionDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeUpdate);
        Population testPopulation = populations.get(populations.size() - 1);
        assertThat(testPopulation.getName()).isEqualTo(populationDTO.getName());
        assertThat(testPopulation.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPopulation.getTimestamp()).isEqualTo(populationDTO.getTimestamp());
        assertThat(testPopulation.getNumIndividual()).isEqualTo(populationDTO.getNumIndividual());
        assertThat(testPopulation.getNumCombination()).isEqualTo(populationDTO.getNumCombination());
        assertThat(testPopulation.getSizeGenotype()).isEqualTo(populationDTO.getSizeGenotype());

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }

    private void saveSizeParam(Population belong, Integer size, SizeType type) {
        if (size == null || size == 0)
            return;

        SizeParam sizeParam = new SizeParam();
        sizeParam.setBelong(belong);
        sizeParam.setSize(size);
        sizeParam.setUse(type);

        sizeParamRepository.saveAndFlush(sizeParam);
    }

    @Test
    @Transactional
    public void getPopulationBySize() throws Exception {
        int sizeShort = 10;
        int sizeFloat = 3;
        int sizeBool  = 0;
        int total = sizeShort + sizeFloat + sizeBool;
        population.setSizeGenotype(total);

        population = populationRepository.saveAndFlush(population);
        saveSizeParam(population, sizeShort, sizeTypeService.getSHORT());
        saveSizeParam(population, sizeFloat, sizeTypeService.getREAL());
        saveSizeParam(population, sizeBool , sizeTypeService.getBINARY());

        CustomSizeParamDTO customSizeParamDTO = new CustomSizeParamDTO(sizeShort, sizeFloat, sizeBool);


        log.debug("\n\t CustomDTO:  {}", customSizeParamDTO);
        log.debug("\n\t Population: {}", population);

        restPopulationMockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();


        log.debug("---\n\n\n\n");

        ResultActions resultActions = restPopulationMockMvc.perform(MockMvcRequestBuilders.get("/api/populations-size")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .param("sizeShort", sizeShort+"")
            .param("sizeFloat", sizeFloat+"")
            .param("sizeBool" , sizeBool+"")

            .contentType(TestUtil.APPLICATION_JSON_UTF8))//;

            .andExpect(status().isOk())

            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].numIndividual").value(hasItem(DEFAULT_NUM_INDIVIDUAL)))
            .andExpect(jsonPath("$.[*].numCombination").value(hasItem(DEFAULT_NUM_COMBINATION)))
            .andExpect(jsonPath("$.[*].sizeGenotype").value(hasItem(total)))
            .andExpect(jsonPath("$.[*].ownerId").value(hasItem(owner.getId().intValue())))
            .andExpect(jsonPath("$.[*].ownerLogin").value(hasItem(owner.getLogin())));


        Enumeration<String> e = resultActions.andReturn().getRequest().getParameterNames();
        String tmp = "\n\n\n";
        while (e.hasMoreElements()) {
            String str = e.nextElement();
            tmp = tmp + str + ":"+resultActions.andReturn().getRequest().getParameter(str) + "\n";
        }
        log.debug("tmp: {}\n\n", tmp);

        log.debug("\n\nResponse:\t{}",resultActions.andReturn().getResponse().getContentAsString());
    }

    @Test
    @Transactional
    public void sharePopulation() throws Exception {
        populationRepository.saveAndFlush(population);

        User user = userService.createUserInformation("leia", "leiai", "leia", "leia", "leia@localhost", "en");
        user.setActivated(true);
        userRepository.saveAndFlush(user);

        CustomShareResourceDTO customShareResourceDTO = new CustomShareResourceDTO(population.getId(), user.getLogin());

        MockMvc restEntityMock = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();

        ResultActions result =  restEntityMock.perform(MockMvcRequestBuilders.post("/api/population-share")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customShareResourceDTO)));

        assertThat(populationRepository.findOne(population.getId()).getShares().contains(user)).isTrue();
    }

    //
    // End Custom-resource
    //

    @Test
    @Transactional
    public void createPopulationOnwer() throws Exception {
        int databaseSizeBeforeCreate = populationRepository.findAll().size();

        // Create the Population
        testUtilPrivacy.createEntity(populationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeCreate + 1);
        Population testPopulation = populations.get(populations.size() - 1);
        assertThat(testPopulation.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPopulation.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPopulation.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testPopulation.getNumIndividual()).isEqualTo(DEFAULT_NUM_INDIVIDUAL);
        assertThat(testPopulation.getNumCombination()).isEqualTo(DEFAULT_NUM_COMBINATION);
        assertThat(testPopulation.getSizeGenotype()).isEqualTo(DEFAULT_SIZE_GENOTYPE);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }

    @Test
    @Transactional
    public void createPopulationAdmin() throws Exception {
        int databaseSizeBeforeCreate = populationRepository.findAll().size();

        // Create the Population
        testUtilPrivacy.createEntity(populationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.CREATED);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeCreate + 1);
        Population testPopulation = populations.get(populations.size() - 1);
        assertThat(testPopulation.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPopulation.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPopulation.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testPopulation.getNumIndividual()).isEqualTo(DEFAULT_NUM_INDIVIDUAL);
        assertThat(testPopulation.getNumCombination()).isEqualTo(DEFAULT_NUM_COMBINATION);
        assertThat(testPopulation.getSizeGenotype()).isEqualTo(DEFAULT_SIZE_GENOTYPE);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }

    @Test
    @Transactional
    public void createPopulation403() throws Exception {
        int databaseSizeBeforeCreate = populationRepository.findAll().size();

        // Create the Population
        testUtilPrivacy.createEntity(populationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeCreate);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }

    @Test
    @Transactional
    public void createPopulation401() throws Exception {
        int databaseSizeBeforeCreate = populationRepository.findAll().size();

        // Create the Population
        testUtilPrivacy.createEntityUnauthorized(populationDTO);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeCreate);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }




    @Test
    @Transactional
    public void deletePopulationOwner() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeDelete = populationRepository.findAll().size();

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.deleteEntity(populationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);

        // Validate ElasticSearch is empty
        //boolean populationExistsInEs = populationSearchRepository.exists(population.getId());
        //assertThat(populationExistsInEs).isFalse();

        // Validate the database is empty
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void deletePopulationAdmin() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeDelete = populationRepository.findAll().size();

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.deleteEntity(populationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK);

        // Validate ElasticSearch is empty
        //boolean populationExistsInEs = populationSearchRepository.exists(population.getId());
        //assertThat(populationExistsInEs).isFalse();

        // Validate the database is empty
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void deletePopulation403() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeDelete = populationRepository.findAll().size();

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.deleteEntity(populationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);

        // Validate ElasticSearch is empty
        //boolean populationExistsInEs = populationSearchRepository.exists(population.getId());
        //assertThat(populationExistsInEs).isFalse();

        // Validate the database is empty
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deletePopulation404() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeDelete = populationRepository.findAll().size();

        // Get the population
        testUtilPrivacy.deleteEntityNotFound(Long.MAX_VALUE, owner.getLogin(), AuthoritiesConstants.ADMIN);

        // Validate ElasticSearch is empty
        //boolean populationExistsInEs = populationSearchRepository.exists(population.getId());
        //assertThat(populationExistsInEs).isFalse();

        // Validate the database is empty
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deletePopulation401() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeDelete = populationRepository.findAll().size();

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.deleteEntityUnauthorized(populationDTO);

        // Validate ElasticSearch is empty
        //boolean populationExistsInEs = populationSearchRepository.exists(population.getId());
        //assertThat(populationExistsInEs).isFalse();

        // Validate the database is empty
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeDelete);
    }


    @Test
    @Transactional
    public void getAllPopulationsOwner() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);


        // Get all the populations
        ResultActions resultActions = testUtilPrivacy.listEntities(owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK)
                .andExpect(jsonPath("$.[*].id").value(hasItem(population.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
                .andExpect(jsonPath("$.[*].numIndividual").value(hasItem(DEFAULT_NUM_INDIVIDUAL)))
                .andExpect(jsonPath("$.[*].numCombination").value(hasItem(DEFAULT_NUM_COMBINATION)))
                .andExpect(jsonPath("$.[*].sizeGenotype").value(hasItem(DEFAULT_SIZE_GENOTYPE)))
                .andExpect(jsonPath("$.[*].ownerId").value(hasItem(owner.getId().intValue())))
                .andExpect(jsonPath("$.[*].ownerLogin").value(hasItem(owner.getLogin())));

        //log.debug(resultActions.andReturn().getResponse().getContentAsString());
    }

    @Test
    @Transactional
    public void getAllPopulationsOwnerShare() throws Exception {
        // Initialize the database
        User user = userService.createUserInformation("leia", "leiai", "leia", "leia", "leia@localhost", "en");
        user.setActivated(true);

        Population p1 = createEntities.createPopulation(1, 2);
        Population p2 = createEntities.createPopulation(0, 1);
        p2.setOwner(user);
        Set<User> set  = p2.getShares();
        set.add(owner);
        p2.setShares(set);
        populationRepository.saveAndFlush(p1);
        populationRepository.saveAndFlush(p2);

        // Get all the populations
        ResultActions resultActions = testUtilPrivacy.listEntities(owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);
        resultActions.andExpect(jsonPath("$").value(hasSize(2)));

        //log.debug(resultActions.andReturn().getResponse().getContentAsString());
    }

    @Test
    @Transactional
    public void getAllPopulationsAdmin() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);

        // Get all the populations
        testUtilPrivacy.listEntities("+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK)
            .andExpect(jsonPath("$.[*].id").value(hasItem(population.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].numIndividual").value(hasItem(DEFAULT_NUM_INDIVIDUAL)))
            .andExpect(jsonPath("$.[*].numCombination").value(hasItem(DEFAULT_NUM_COMBINATION)))
            .andExpect(jsonPath("$.[*].sizeGenotype").value(hasItem(DEFAULT_SIZE_GENOTYPE)))
            .andExpect(jsonPath("$.[*].ownerId").value(hasItem(owner.getId().intValue())))
            .andExpect(jsonPath("$.[*].ownerLogin").value(hasItem(owner.getLogin())));
    }



    @Test
    @Transactional
    public void getAllPopulationsUserWithoutData() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);

        // Get all the populations
        testUtilPrivacy.listEntities("+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.BAD_REQUEST);
    }





    @Test
    @Transactional
    public void getPopulationOwner() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.getEntity(populationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK)
            .andExpect(jsonPath("$.id").value(population.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP_STR))
            .andExpect(jsonPath("$.numIndividual").value(DEFAULT_NUM_INDIVIDUAL))
            .andExpect(jsonPath("$.numCombination").value(DEFAULT_NUM_COMBINATION))
            .andExpect(jsonPath("$.sizeGenotype").value(DEFAULT_SIZE_GENOTYPE))
            .andExpect(jsonPath("$.ownerId").value(owner.getId().intValue()))
            .andExpect(jsonPath("$.ownerLogin").value(owner.getLogin()));
    }

    @Test
    @Transactional
    public void getAllPopulationShare() throws Exception {
        // Initialize the database
        User user = userService.createUserInformation("leia", "leiai", "leia", "leia", "leia@localhost", "en");
        user.setActivated(true);

        userRepository.saveAndFlush(user);
        populationRepository.saveAndFlush(population);

        population.setOwner(user);
        Set<User> set  = population.getShares();
        set.add(owner);
        population.setShares(set);
        populationRepository.saveAndFlush(population);

        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.getEntity(populationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK)
            .andExpect(jsonPath("$.id").value(population.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP_STR))
            .andExpect(jsonPath("$.numIndividual").value(DEFAULT_NUM_INDIVIDUAL))
            .andExpect(jsonPath("$.numCombination").value(DEFAULT_NUM_COMBINATION))
            .andExpect(jsonPath("$.sizeGenotype").value(DEFAULT_SIZE_GENOTYPE))
            .andExpect(jsonPath("$.ownerId").value(user.getId().intValue()))
            .andExpect(jsonPath("$.ownerLogin").value(user.getLogin()));
    }

    @Test
    @Transactional
    public void getPopulationAdmin() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        ResultActions resultActions = testUtilPrivacy.getEntity(populationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK)
            .andExpect(jsonPath("$.id").value(population.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP_STR))
            .andExpect(jsonPath("$.numIndividual").value(DEFAULT_NUM_INDIVIDUAL))
            .andExpect(jsonPath("$.numCombination").value(DEFAULT_NUM_COMBINATION))
            .andExpect(jsonPath("$.sizeGenotype").value(DEFAULT_SIZE_GENOTYPE))
            .andExpect(jsonPath("$.ownerId").value(owner.getId().intValue()))
            .andExpect(jsonPath("$.ownerLogin").value(owner.getLogin()));

        //log.debug(resultActions.andReturn().getResponse().getContentAsString());
    }

    @Test
    @Transactional
    public void getPopulation403() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.getEntity(populationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);
    }

    @Test
    @Transactional
    public void getPopulation404() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);

        // Get the population
        testUtilPrivacy.getEntityNotFound(Long.MAX_VALUE, owner.getLogin(), AuthoritiesConstants.ADMIN);
    }

    @Test
    @Transactional
    public void getPopulation401() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);

        // Get the population
        populationDTO = populationMapper.populationToPopulationDTO(population);
        testUtilPrivacy.getEntityUnauthorized(populationDTO);
    }



    @Test
    @Transactional
    public void updatePopulationOwner() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeUpdate = populationRepository.findAll().size();

        // Update the population
        Population updatedPopulation = new Population();
        updatedPopulation.setId(population.getId());
        updatedPopulation.setName(UPDATED_NAME);
        updatedPopulation.setDescription(UPDATED_DESCRIPTION);
        updatedPopulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedPopulation.setNumIndividual(UPDATED_NUM_INDIVIDUAL);
        updatedPopulation.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedPopulation.setSizeGenotype(UPDATED_SIZE_GENOTYPE);
        updatedPopulation.setOwner(owner);
        PopulationDTO updatePopulationDTO = populationMapper.populationToPopulationDTO(updatedPopulation);
        populationDTO = populationMapper.populationToPopulationDTO(population);

        testUtilPrivacy.updateEntity(populationDTO, updatePopulationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeUpdate);
        Population testPopulation = populations.get(populations.size() - 1);
        assertThat(testPopulation.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPopulation.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPopulation.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testPopulation.getNumIndividual()).isEqualTo(UPDATED_NUM_INDIVIDUAL);
        assertThat(testPopulation.getNumCombination()).isEqualTo(UPDATED_NUM_COMBINATION);
        assertThat(testPopulation.getSizeGenotype()).isEqualTo(UPDATED_SIZE_GENOTYPE);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }

    @Test
    @Transactional
    public void updatePopulationAdmin() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeUpdate = populationRepository.findAll().size();

        // Update the population
        Population updatedPopulation = new Population();
        updatedPopulation.setId(population.getId());
        updatedPopulation.setName(UPDATED_NAME);
        updatedPopulation.setDescription(UPDATED_DESCRIPTION);
        updatedPopulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedPopulation.setNumIndividual(UPDATED_NUM_INDIVIDUAL);
        updatedPopulation.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedPopulation.setSizeGenotype(UPDATED_SIZE_GENOTYPE);
        updatedPopulation.setOwner(owner);
        PopulationDTO updatePopulationDTO = populationMapper.populationToPopulationDTO(updatedPopulation);
        populationDTO = populationMapper.populationToPopulationDTO(population);

        testUtilPrivacy.updateEntity(populationDTO, updatePopulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeUpdate);
        Population testPopulation = populations.get(populations.size() - 1);
        assertThat(testPopulation.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPopulation.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPopulation.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testPopulation.getNumIndividual()).isEqualTo(UPDATED_NUM_INDIVIDUAL);
        assertThat(testPopulation.getNumCombination()).isEqualTo(UPDATED_NUM_COMBINATION);
        assertThat(testPopulation.getSizeGenotype()).isEqualTo(UPDATED_SIZE_GENOTYPE);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }

    @Test
    @Transactional
    public void updatePopulation403() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeUpdate = populationRepository.findAll().size();

        // Update the population
        Population updatedPopulation = new Population();
        updatedPopulation.setId(population.getId());
        updatedPopulation.setName(UPDATED_NAME);
        updatedPopulation.setDescription(UPDATED_DESCRIPTION);
        updatedPopulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedPopulation.setNumIndividual(UPDATED_NUM_INDIVIDUAL);
        updatedPopulation.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedPopulation.setSizeGenotype(UPDATED_SIZE_GENOTYPE);
        updatedPopulation.setOwner(owner);
        PopulationDTO updatePopulationDTO = populationMapper.populationToPopulationDTO(updatedPopulation);
        populationDTO = populationMapper.populationToPopulationDTO(population);

        testUtilPrivacy.updateEntity(populationDTO, updatePopulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeUpdate);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }



    @Test
    @Transactional
    public void updatePopulation401() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        //populationSearchRepository.save(population);
        int databaseSizeBeforeUpdate = populationRepository.findAll().size();

        // Update the population
        Population updatedPopulation = new Population();
        updatedPopulation.setId(population.getId());
        updatedPopulation.setName(UPDATED_NAME);
        updatedPopulation.setDescription(UPDATED_DESCRIPTION);
        updatedPopulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedPopulation.setNumIndividual(UPDATED_NUM_INDIVIDUAL);
        updatedPopulation.setNumCombination(UPDATED_NUM_COMBINATION);
        updatedPopulation.setSizeGenotype(UPDATED_SIZE_GENOTYPE);
        updatedPopulation.setOwner(owner);
        PopulationDTO updatePopulationDTO = populationMapper.populationToPopulationDTO(updatedPopulation);
        populationDTO = populationMapper.populationToPopulationDTO(population);

        testUtilPrivacy.updateEntityUnauthorized(populationDTO, updatePopulationDTO);

        // Validate the Population in the database
        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeUpdate);

        // Validate the Population in ElasticSearch
        //Population populationEs = populationSearchRepository.findOne(testPopulation.getId());
        //assertThat(populationEs).isEqualToComparingFieldByField(testPopulation);
    }
/*
    @Test
    @Transactional
    public void searchPopulation() throws Exception {
        // Initialize the database
        populationRepository.saveAndFlush(population);
        populationSearchRepository.save(population);

        // Search the population
        restPopulationMockMvc.perform(get("/api/_search/populations?query=id:" + population.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(population.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].numIndividual").value(hasItem(DEFAULT_NUM_INDIVIDUAL)))
            .andExpect(jsonPath("$.[*].numCombination").value(hasItem(DEFAULT_NUM_COMBINATION)))
            .andExpect(jsonPath("$.[*].sizeGenotype").value(hasItem(DEFAULT_SIZE_GENOTYPE)));
    }
*/
    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationRepository.findAll().size();
        // set the field null
        population.setName(null);

        // Create the Population, which fails.
        PopulationDTO populationDTO = populationMapper.populationToPopulationDTO(population);

        restPopulationMockMvc.perform(post("/api/populations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(populationDTO)))
            .andExpect(status().isBadRequest());

        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTimestampIsRequired() throws Exception {
        int databaseSizeBeforeTest = populationRepository.findAll().size();
        // set the field null
        population.setTimestamp(null);

        // Create the Population, which fails.
        PopulationDTO populationDTO = populationMapper.populationToPopulationDTO(population);

        restPopulationMockMvc.perform(post("/api/populations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(populationDTO)))
            .andExpect(status().isBadRequest());

        List<Population> populations = populationRepository.findAll();
        assertThat(populations).hasSize(databaseSizeBeforeTest);
    }
}
