/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.ProblemType;
import es.udc.tic.rnasa.repository.ProblemTypeRepository;
import es.udc.tic.rnasa.service.ProblemTypeService;
import es.udc.tic.rnasa.repository.search.ProblemTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.ProblemTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.ProblemTypeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ProblemTypeResource REST controller.
 *
 * @see ProblemTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class ProblemTypeResourceIntTest {

    private final Logger log = LoggerFactory.getLogger(ProblemTypeResourceIntTest.class);

    private static final String DEFAULT_NAME = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final Integer DEFAULT_OUTPUT_FEATURE_CONSTRAINT = 1;
    private static final Integer UPDATED_OUTPUT_FEATURE_CONSTRAINT = 2;

    private static final Integer DEFAULT_NUM_POPULATION = 1;
    private static final Integer UPDATED_NUM_POPULATION = 2;

    @Inject
    private ProblemTypeRepository problemTypeRepository;

    @Inject
    private ProblemTypeMapper problemTypeMapper;

    @Inject
    private ProblemTypeService problemTypeService;

    @Inject
    private ProblemTypeSearchRepository problemTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restProblemTypeMockMvc;

    private ProblemType problemType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProblemTypeResource problemTypeResource = new ProblemTypeResource();
        ReflectionTestUtils.setField(problemTypeResource, "problemTypeService", problemTypeService);
        ReflectionTestUtils.setField(problemTypeResource, "problemTypeMapper", problemTypeMapper);
        this.restProblemTypeMockMvc = MockMvcBuilders.standaloneSetup(problemTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        problemTypeSearchRepository.deleteAll();
        problemType = new ProblemType();
        problemType.setName(DEFAULT_NAME);
        problemType.setOutputFeatureConstraint(DEFAULT_OUTPUT_FEATURE_CONSTRAINT);
        problemType.setNumPopulation(DEFAULT_NUM_POPULATION);
    }

    @Test
    @Transactional
    public void createProblemType() throws Exception {
        int databaseSizeBeforeCreate = problemTypeRepository.findAll().size();

        // Create the ProblemType
        ProblemTypeDTO problemTypeDTO = problemTypeMapper.problemTypeToProblemTypeDTO(problemType);

        restProblemTypeMockMvc.perform(post("/api/problem-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(problemTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the ProblemType in the database
        List<ProblemType> problemTypes = problemTypeRepository.findAll();
        assertThat(problemTypes).hasSize(databaseSizeBeforeCreate + 1);
        ProblemType testProblemType = problemTypes.get(problemTypes.size() - 1);
        assertThat(testProblemType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProblemType.getOutputFeatureConstraint()).isEqualTo(DEFAULT_OUTPUT_FEATURE_CONSTRAINT);
        assertThat(testProblemType.getNumPopulation()).isEqualTo(DEFAULT_NUM_POPULATION);

        // Validate the ProblemType in ElasticSearch
        //ProblemType problemTypeEs = problemTypeSearchRepository.findOne(testProblemType.getId());
        //assertThat(problemTypeEs).isEqualToComparingFieldByField(testProblemType);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = problemTypeRepository.findAll().size();
        // set the field null
        problemType.setName(null);

        // Create the ProblemType, which fails.
        ProblemTypeDTO problemTypeDTO = problemTypeMapper.problemTypeToProblemTypeDTO(problemType);

        restProblemTypeMockMvc.perform(post("/api/problem-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(problemTypeDTO)))
                .andExpect(status().isBadRequest());

        List<ProblemType> problemTypes = problemTypeRepository.findAll();
        assertThat(problemTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumPopulationIsRequired() throws Exception {
        int databaseSizeBeforeTest = problemTypeRepository.findAll().size();
        // set the field null
        problemType.setNumPopulation(null);

        // Create the ProblemType, which fails.
        ProblemTypeDTO problemTypeDTO = problemTypeMapper.problemTypeToProblemTypeDTO(problemType);

        restProblemTypeMockMvc.perform(post("/api/problem-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(problemTypeDTO)))
            .andExpect(status().isBadRequest());

        List<ProblemType> problemTypes = problemTypeRepository.findAll();
        assertThat(problemTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProblemTypes() throws Exception {
        // Initialize the database
        problemTypeRepository.saveAndFlush(problemType);

        // Get all the problemTypes
        restProblemTypeMockMvc.perform(get("/api/problem-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(problemType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].outputFeatureConstraint").value(hasItem(DEFAULT_OUTPUT_FEATURE_CONSTRAINT)))
                .andExpect(jsonPath("$.[*].numPopulation").value(hasItem(DEFAULT_NUM_POPULATION)));
    }

    @Test
    @Transactional
    public void getProblemType() throws Exception {
        // Initialize the database
        problemTypeRepository.saveAndFlush(problemType);

        // Get the problemType
        restProblemTypeMockMvc.perform(get("/api/problem-types/{id}", problemType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(problemType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.outputFeatureConstraint").value(DEFAULT_OUTPUT_FEATURE_CONSTRAINT))
            .andExpect(jsonPath("$.numPopulation").value(DEFAULT_NUM_POPULATION));
    }

    @Test
    @Transactional
    public void getNonExistingProblemType() throws Exception {
        // Get the problemType
        restProblemTypeMockMvc.perform(get("/api/problem-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProblemType() throws Exception {
        // Initialize the database
        problemTypeRepository.saveAndFlush(problemType);
        problemTypeSearchRepository.save(problemType);
        int databaseSizeBeforeUpdate = problemTypeRepository.findAll().size();

        // Update the problemType
        ProblemType updatedProblemType = new ProblemType();
        updatedProblemType.setId(problemType.getId());
        updatedProblemType.setName(UPDATED_NAME);
        updatedProblemType.setOutputFeatureConstraint(UPDATED_OUTPUT_FEATURE_CONSTRAINT);
        updatedProblemType.setNumPopulation(UPDATED_NUM_POPULATION);
        ProblemTypeDTO problemTypeDTO = problemTypeMapper.problemTypeToProblemTypeDTO(updatedProblemType);

        restProblemTypeMockMvc.perform(put("/api/problem-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(problemTypeDTO)))
                .andExpect(status().isOk());

        // Validate the ProblemType in the database
        List<ProblemType> problemTypes = problemTypeRepository.findAll();
        assertThat(problemTypes).hasSize(databaseSizeBeforeUpdate);
        ProblemType testProblemType = problemTypes.get(problemTypes.size() - 1);
        assertThat(testProblemType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProblemType.getOutputFeatureConstraint()).isEqualTo(UPDATED_OUTPUT_FEATURE_CONSTRAINT);
        assertThat(testProblemType.getNumPopulation()).isEqualTo(UPDATED_NUM_POPULATION);

        // Validate the ProblemType in ElasticSearch
        //ProblemType problemTypeEs = problemTypeSearchRepository.findOne(testProblemType.getId());
        //assertThat(problemTypeEs).isEqualToComparingFieldByField(testProblemType);
    }

    @Test
    @Transactional
    public void deleteProblemType() throws Exception {
        // Initialize the database
        problemTypeRepository.saveAndFlush(problemType);
        problemTypeSearchRepository.save(problemType);
        int databaseSizeBeforeDelete = problemTypeRepository.findAll().size();

        // Get the problemType
        restProblemTypeMockMvc.perform(delete("/api/problem-types/{id}", problemType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean problemTypeExistsInEs = problemTypeSearchRepository.exists(problemType.getId());
        assertThat(problemTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<ProblemType> problemTypes = problemTypeRepository.findAll();
        assertThat(problemTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchProblemType() throws Exception {
        // Initialize the database
        problemTypeRepository.saveAndFlush(problemType);
        problemTypeSearchRepository.save(problemType);

        // Search the problemType
        restProblemTypeMockMvc.perform(get("/api/_search/problem-types?query=id:" + problemType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(problemType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].outputFeatureConstraint").value(hasItem(DEFAULT_OUTPUT_FEATURE_CONSTRAINT)))
            .andExpect(jsonPath("$.[*].numPopulation").value(hasItem(DEFAULT_NUM_POPULATION)));
    }
}
