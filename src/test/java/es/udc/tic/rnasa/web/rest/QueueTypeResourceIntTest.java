/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.QueueType;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.QueueTypeRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.QueueTypeService;
import es.udc.tic.rnasa.repository.search.QueueTypeSearchRepository;

import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the QueueTypeResource REST controller.
 *
 * @see QueueTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class QueueTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    @Inject
    private QueueTypeRepository queueTypeRepository;

    @Inject
    private QueueTypeService queueTypeService;

    @Inject
    private QueueTypeSearchRepository queueTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restQueueTypeMockMvc;

    private QueueType queueType;
    private User owner;
    private UserDTO ownerDTO;

    @Inject
    private WebApplicationContext context;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        QueueTypeResource queueTypeResource = new QueueTypeResource();
        ReflectionTestUtils.setField(queueTypeResource, "queueTypeService", queueTypeService);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);
/*
        this.restQueueTypeMockMvc = MockMvcBuilders.standaloneSetup(queueTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
*/
        this.restQueueTypeMockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Before
    public void initTest() {
        queueTypeSearchRepository.deleteAll();

        ownerDTO = new UserDTO(
            "joe",                  // login
            "123456789012345678901234567890123456789012345678901234567890",
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin("joe").isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }


        queueType = new QueueType();
        queueType.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createQueueTypeUnauthorized() throws Exception {
        int databaseSizeBeforeCreate = queueTypeRepository.findAll().size();

        // Create the QueueType

        restQueueTypeMockMvc.perform(post("/api/queue-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(queueType)))
            .andExpect(status().is4xxClientError());

        // Validate the QueueType in the database
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createQueueTypeNoAdmin() throws Exception {
        int databaseSizeBeforeCreate = queueTypeRepository.findAll().size();

        // Create the QueueType

        restQueueTypeMockMvc.perform(post("/api/queue-types")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(queueType)))
            .andExpect(status().isForbidden());

        // Validate the QueueType in the database
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createQueueType() throws Exception {
        int databaseSizeBeforeCreate = queueTypeRepository.findAll().size();

        // Create the QueueType

        restQueueTypeMockMvc.perform(post("/api/queue-types")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(queueType)))
            .andExpect(status().isCreated());

        // Validate the QueueType in the database
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeCreate + 1);
        QueueType testQueueType = queueTypes.get(queueTypes.size() - 1);
        assertThat(testQueueType.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the QueueType in ElasticSearch
        //QueueType queueTypeEs = queueTypeSearchRepository.findOne(testQueueType.getId());
        //assertThat(queueTypeEs).isEqualToComparingFieldByField(testQueueType);
    }

    @Test
    @Transactional
    public void getAllQueueTypesUnauthenticated() throws Exception {
        // Initialize the database
        queueTypeRepository.saveAndFlush(queueType);

        // Get all the queueTypes
        restQueueTypeMockMvc.perform(get("/api/queue-types?sort=id,desc"))
            .andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void getAllQueueTypesNoAdmin() throws Exception {
        // Initialize the database
        queueTypeRepository.saveAndFlush(queueType);

        // Get all the queueTypes
        restQueueTypeMockMvc.perform(get("/api/queue-types?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void getAllQueueTypes() throws Exception {
        // Initialize the database
        queueTypeRepository.saveAndFlush(queueType);

        // Get all the queueTypes
        restQueueTypeMockMvc.perform(get("/api/queue-types?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(queueType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getQueueTypeUnauthorized() throws Exception {
        // Initialize the database
        queueTypeRepository.saveAndFlush(queueType);

        // Get the queueType
        restQueueTypeMockMvc.perform(get("/api/queue-types/{id}", queueType.getId()))
            .andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void getQueueTypeNoAdmin() throws Exception {
        // Initialize the database
        queueTypeRepository.saveAndFlush(queueType);

        // Get the queueType
        restQueueTypeMockMvc.perform(get("/api/queue-types/{id}", queueType.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void getQueueType() throws Exception {
        // Initialize the database
        queueTypeRepository.saveAndFlush(queueType);

        // Get the queueType
        restQueueTypeMockMvc.perform(get("/api/queue-types/{id}", queueType.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(queueType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingQueueType() throws Exception {
        // Get the queueType
        restQueueTypeMockMvc.perform(get("/api/queue-types/{id}", Long.MAX_VALUE)
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQueueTypeUnauthorized() throws Exception {
        // Initialize the database
        queueTypeRepository.save(queueType);

        int databaseSizeBeforeUpdate = queueTypeRepository.findAll().size();

        // Update the queueType
        QueueType updatedQueueType = new QueueType();
        updatedQueueType.setId(queueType.getId());
        updatedQueueType.setName(UPDATED_NAME);

        restQueueTypeMockMvc.perform(put("/api/queue-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedQueueType)))
            .andExpect(status().is4xxClientError());

        // Validate the QueueType in the database
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateQueueTypeNoAdmin() throws Exception {
        // Initialize the database
        queueTypeRepository.save(queueType);

        int databaseSizeBeforeUpdate = queueTypeRepository.findAll().size();

        // Update the queueType
        QueueType updatedQueueType = new QueueType();
        updatedQueueType.setId(queueType.getId());
        updatedQueueType.setName(UPDATED_NAME);

        restQueueTypeMockMvc.perform(put("/api/queue-types")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedQueueType)))
            .andExpect(status().isForbidden());

        // Validate the QueueType in the database
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateQueueType() throws Exception {
        // Initialize the database
        queueTypeRepository.save(queueType);

        int databaseSizeBeforeUpdate = queueTypeRepository.findAll().size();

        // Update the queueType
        QueueType updatedQueueType = new QueueType();
        updatedQueueType.setId(queueType.getId());
        updatedQueueType.setName(UPDATED_NAME);

        restQueueTypeMockMvc.perform(put("/api/queue-types")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedQueueType)))
            .andExpect(status().isOk());

        // Validate the QueueType in the database
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeUpdate);
        QueueType testQueueType = queueTypes.get(queueTypes.size() - 1);
        assertThat(testQueueType.getName()).isEqualTo(UPDATED_NAME);

        // Validate the QueueType in ElasticSearch
        //QueueType queueTypeEs = queueTypeSearchRepository.findOne(testQueueType.getId());
        //assertThat(queueTypeEs).isEqualToComparingFieldByField(testQueueType);
    }

    @Test
    @Transactional
    public void deleteQueueTypeUnauthorized() throws Exception {
        // Initialize the database
        queueTypeRepository.save(queueType);

        int databaseSizeBeforeDelete = queueTypeRepository.findAll().size();

        // Get the queueType
        restQueueTypeMockMvc.perform(delete("/api/queue-types/{id}", queueType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().is4xxClientError());

        // Validate the database is empty
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteQueueTypeNoAdmin() throws Exception {
        // Initialize the database
        queueTypeRepository.save(queueType);

        int databaseSizeBeforeDelete = queueTypeRepository.findAll().size();

        // Get the queueType
        restQueueTypeMockMvc.perform(delete("/api/queue-types/{id}", queueType.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isForbidden());

        // Validate the database is empty
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteQueueType() throws Exception {
        // Initialize the database
        queueTypeRepository.save(queueType);

        int databaseSizeBeforeDelete = queueTypeRepository.findAll().size();

        // Get the queueType
        restQueueTypeMockMvc.perform(delete("/api/queue-types/{id}", queueType.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean queueTypeExistsInEs = queueTypeSearchRepository.exists(queueType.getId());
        assertThat(queueTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

/*
    @Test
    @Transactional
    public void searchQueueType() throws Exception {
        // Initialize the database
        queueTypeService.save(queueType);

        // Search the queueType
        restQueueTypeMockMvc.perform(get("/api/_search/queue-types?query=id:" + queueType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(queueType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
*/

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = queueTypeRepository.findAll().size();
        // set the field null
        queueType.setName(null);

        // Create the QueueType, which fails.

        restQueueTypeMockMvc.perform(post("/api/queue-types")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(queueType)))
            .andExpect(status().isBadRequest());

        List<QueueType> queueTypes = queueTypeRepository.findAll();
        assertThat(queueTypes).hasSize(databaseSizeBeforeTest);
    }
}
