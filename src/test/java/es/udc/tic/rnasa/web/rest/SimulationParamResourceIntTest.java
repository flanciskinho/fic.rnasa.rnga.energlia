/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.SimulationParam;
import es.udc.tic.rnasa.repository.SimulationParamRepository;
import es.udc.tic.rnasa.service.SimulationParamService;
import es.udc.tic.rnasa.repository.search.SimulationParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.SimulationParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationParamMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SimulationParamResource REST controller.
 *
 * @see SimulationParamResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SimulationParamResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_VALUE = "AAAAA";
    private static final String UPDATED_VALUE = "BBBBB";

    @Inject
    private SimulationParamRepository simulationParamRepository;

    @Inject
    private SimulationParamMapper simulationParamMapper;

    @Inject
    private SimulationParamService simulationParamService;

    @Inject
    private SimulationParamSearchRepository simulationParamSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSimulationParamMockMvc;

    private SimulationParam simulationParam;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SimulationParamResource simulationParamResource = new SimulationParamResource();
        ReflectionTestUtils.setField(simulationParamResource, "simulationParamService", simulationParamService);
        ReflectionTestUtils.setField(simulationParamResource, "simulationParamMapper", simulationParamMapper);
        this.restSimulationParamMockMvc = MockMvcBuilders.standaloneSetup(simulationParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        simulationParamSearchRepository.deleteAll();
        simulationParam = new SimulationParam();
        simulationParam.setName(DEFAULT_NAME);
        simulationParam.setValue(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createSimulationParam() throws Exception {
        int databaseSizeBeforeCreate = simulationParamRepository.findAll().size();

        // Create the SimulationParam
        SimulationParamDTO simulationParamDTO = simulationParamMapper.simulationParamToSimulationParamDTO(simulationParam);

        restSimulationParamMockMvc.perform(post("/api/simulation-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simulationParamDTO)))
                .andExpect(status().isCreated());

        // Validate the SimulationParam in the database
        List<SimulationParam> simulationParams = simulationParamRepository.findAll();
        assertThat(simulationParams).hasSize(databaseSizeBeforeCreate + 1);
        SimulationParam testSimulationParam = simulationParams.get(simulationParams.size() - 1);
        assertThat(testSimulationParam.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSimulationParam.getValue()).isEqualTo(DEFAULT_VALUE);

        // Validate the SimulationParam in ElasticSearch
        SimulationParam simulationParamEs = simulationParamSearchRepository.findOne(testSimulationParam.getId());
        assertThat(simulationParamEs).isEqualToComparingFieldByField(testSimulationParam);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = simulationParamRepository.findAll().size();
        // set the field null
        simulationParam.setName(null);

        // Create the SimulationParam, which fails.
        SimulationParamDTO simulationParamDTO = simulationParamMapper.simulationParamToSimulationParamDTO(simulationParam);

        restSimulationParamMockMvc.perform(post("/api/simulation-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simulationParamDTO)))
                .andExpect(status().isBadRequest());

        List<SimulationParam> simulationParams = simulationParamRepository.findAll();
        assertThat(simulationParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = simulationParamRepository.findAll().size();
        // set the field null
        simulationParam.setValue(null);

        // Create the SimulationParam, which fails.
        SimulationParamDTO simulationParamDTO = simulationParamMapper.simulationParamToSimulationParamDTO(simulationParam);

        restSimulationParamMockMvc.perform(post("/api/simulation-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simulationParamDTO)))
                .andExpect(status().isBadRequest());

        List<SimulationParam> simulationParams = simulationParamRepository.findAll();
        assertThat(simulationParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSimulationParams() throws Exception {
        // Initialize the database
        simulationParamRepository.saveAndFlush(simulationParam);

        // Get all the simulationParams
        restSimulationParamMockMvc.perform(get("/api/simulation-params?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(simulationParam.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getSimulationParam() throws Exception {
        // Initialize the database
        simulationParamRepository.saveAndFlush(simulationParam);

        // Get the simulationParam
        restSimulationParamMockMvc.perform(get("/api/simulation-params/{id}", simulationParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(simulationParam.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSimulationParam() throws Exception {
        // Get the simulationParam
        restSimulationParamMockMvc.perform(get("/api/simulation-params/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSimulationParam() throws Exception {
        // Initialize the database
        simulationParamRepository.saveAndFlush(simulationParam);
        simulationParamSearchRepository.save(simulationParam);
        int databaseSizeBeforeUpdate = simulationParamRepository.findAll().size();

        // Update the simulationParam
        SimulationParam updatedSimulationParam = new SimulationParam();
        updatedSimulationParam.setId(simulationParam.getId());
        updatedSimulationParam.setName(UPDATED_NAME);
        updatedSimulationParam.setValue(UPDATED_VALUE);
        SimulationParamDTO simulationParamDTO = simulationParamMapper.simulationParamToSimulationParamDTO(updatedSimulationParam);

        restSimulationParamMockMvc.perform(put("/api/simulation-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simulationParamDTO)))
                .andExpect(status().isOk());

        // Validate the SimulationParam in the database
        List<SimulationParam> simulationParams = simulationParamRepository.findAll();
        assertThat(simulationParams).hasSize(databaseSizeBeforeUpdate);
        SimulationParam testSimulationParam = simulationParams.get(simulationParams.size() - 1);
        assertThat(testSimulationParam.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSimulationParam.getValue()).isEqualTo(UPDATED_VALUE);

        // Validate the SimulationParam in ElasticSearch
        SimulationParam simulationParamEs = simulationParamSearchRepository.findOne(testSimulationParam.getId());
        assertThat(simulationParamEs).isEqualToComparingFieldByField(testSimulationParam);
    }

    @Test
    @Transactional
    public void deleteSimulationParam() throws Exception {
        // Initialize the database
        simulationParamRepository.saveAndFlush(simulationParam);
        simulationParamSearchRepository.save(simulationParam);
        int databaseSizeBeforeDelete = simulationParamRepository.findAll().size();

        // Get the simulationParam
        restSimulationParamMockMvc.perform(delete("/api/simulation-params/{id}", simulationParam.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean simulationParamExistsInEs = simulationParamSearchRepository.exists(simulationParam.getId());
        assertThat(simulationParamExistsInEs).isFalse();

        // Validate the database is empty
        List<SimulationParam> simulationParams = simulationParamRepository.findAll();
        assertThat(simulationParams).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSimulationParam() throws Exception {
        // Initialize the database
        simulationParamRepository.saveAndFlush(simulationParam);
        simulationParamSearchRepository.save(simulationParam);

        // Search the simulationParam
        restSimulationParamMockMvc.perform(get("/api/_search/simulation-params?query=id:" + simulationParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(simulationParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }
}
