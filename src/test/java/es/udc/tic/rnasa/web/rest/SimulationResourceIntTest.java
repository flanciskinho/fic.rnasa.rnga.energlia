/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.*;
import es.udc.tic.rnasa.repository.search.SimulationSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.*;
import es.udc.tic.rnasa.web.rest.dto.CustomDescriptionDTO;
import es.udc.tic.rnasa.web.rest.dto.ForCustomSimulationDTO.*;
import es.udc.tic.rnasa.web.rest.dto.SimulationDTO;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import es.udc.tic.rnasa.web.rest.util.TestUtilPrivacy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Test class for the SimulationResource REST controller.
 *
 * @see SimulationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SimulationResourceIntTest {

    @Inject
    private CreateEntities createEntities;

    private final Logger log = LoggerFactory.getLogger(SimulationResourceIntTest.class);

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.now();
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TIMESTAMP_STR = dateTimeFormatter.format(DEFAULT_TIMESTAMP);
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private SimulationRepository simulationRepository;

    @Inject
    private SimulationMapper simulationMapper;

    @Inject
    private SimulationService simulationService;

    @Inject
    private SimulationSearchRepository simulationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSimulationMockMvc;

    private Simulation simulation;
    private SimulationDTO simulationDTO;

    @Inject
    private WebApplicationContext context;

    private User owner;
    private UserDTO ownerDTO;

    private TestUtilPrivacy testUtilPrivacy;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;


    @Inject
    private SpecificSimulationService specificSimulationService;
    @Inject
    private SpecificSimulationRepository specificSimulationRepository;

    @Inject
    private PopulationCombinationOrderService populationCombinationOrderService;
    @Inject
    private PopulationCombinationOrderRepository populationCombinationOrderRepository;

    @Inject
    private DatasetCombinationService datasetCombinationService;
    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private PopulationCombinationService populationCombinationService;
    @Inject
    private PopulationCombinationRepository populationCombinationRepository;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SimulationResource simulationResource = new SimulationResource();
        ReflectionTestUtils.setField(simulationResource, "simulationService", simulationService);
        ReflectionTestUtils.setField(simulationResource, "simulationMapper", simulationMapper);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);

        SpecificSimulationResource specificSimulationResource = new SpecificSimulationResource();
        ReflectionTestUtils.setField(specificSimulationResource, "specificSimulationService", specificSimulationService);
        ReflectionTestUtils.setField(specificSimulationService, "specificSimulationRepository", specificSimulationRepository);

        PopulationCombinationOrderResource populationCombinationOrderResource = new PopulationCombinationOrderResource();
        ReflectionTestUtils.setField(populationCombinationOrderResource, "populationCombinationOrderService", populationCombinationOrderService);
        ReflectionTestUtils.setField(populationCombinationOrderService, "populationCombinationOrderRepository", populationCombinationOrderRepository);

        DatasetCombinationResource datasetCombinationResource = new DatasetCombinationResource();
        ReflectionTestUtils.setField(datasetCombinationResource, "datasetCombinationService", datasetCombinationService);
        ReflectionTestUtils.setField(datasetCombinationService,  "datasetCombinationRepository", datasetCombinationRepository);

        PopulationCombinationResource populationCombinationResource = new PopulationCombinationResource();
        ReflectionTestUtils.setField(populationCombinationResource, "populationCombinationService", populationCombinationService);
        ReflectionTestUtils.setField(populationCombinationService,  "populationCombinationRepository", populationCombinationRepository);

        this.restSimulationMockMvc = MockMvcBuilders.standaloneSetup(simulationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();

    }

    @Before
    public void initTest() {
        Locale.setDefault(Locale.ENGLISH);

        simulationSearchRepository.deleteAll();

        ownerDTO = new UserDTO(
            "joe",                  // login
            "123456789012345678901234567890123456789012345678901234567890",
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin("joe").isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }

        simulation = new Simulation();
        simulation.setTaskTotal(1);
        simulation.setName(DEFAULT_NAME);
        simulation.setTimestamp(DEFAULT_TIMESTAMP);
        simulation.setDescription(DEFAULT_DESCRIPTION);
        simulation.setBelong(owner);

        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);

        testUtilPrivacy = new TestUtilPrivacy(simulationRepository, context,
            "/api/simulations/{id}",
            "/api/simulations",
            "/api/simulations",
            "/api/simulations/{id}",
            "/api/simulations?sort=id,desc");
    }



    //
    // Custom-resource
    //


    @Test
    @Transactional
    public void updateCustomSimulation() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        //simulationSearchRepository.save(simulation);
        int databaseSizeBeforeUpdate = simulationRepository.findAll().size();

        testUtilPrivacy = new TestUtilPrivacy(simulationRepository, context,
            "/api/custom-simulations/{id}",
            "/api/custom-simulations",
            "/api/custom-simulations",
            "/api/custom-simulations/{id}",
            "/api/custom-simulations?sort=id,desc");

        // Update the simulation
        CustomDescriptionDTO customDescriptionDTO = new CustomDescriptionDTO(simulation.getId(), UPDATED_DESCRIPTION);
        testUtilPrivacy.updateEntity(simulationDTO, customDescriptionDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);


        // Validate the Simulation in the database
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeUpdate);
        Simulation testSimulation = simulations.get(simulations.size() - 1);
        assertThat(testSimulation.getName()).isEqualTo(simulation.getName());
        assertThat(testSimulation.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSimulation.getTimestamp()).isEqualTo(simulation.getTimestamp());

        // Validate the Simulation in ElasticSearch
        //Simulation simulationEs = simulationSearchRepository.findOne(testSimulation.getId());
        //assertThat(simulationEs).isEqualToComparingFieldByField(testSimulation);
    }


    ///
    /// Create simulations for problems
    ///

    private SimPopulationDTO[] getSimPopulationDTO(List<Population> populations) {
        String motherAlgorithm[] = {"RANDOM" , "ROULETTE"};
        Short  motherWindow[] = {0 , 0};
        Float  motherProbability[] = {0.0f, 0.0f};
        String fatherAlgorithm[] = {"RANDOM" , "ROULETTE"};
        Short  fatherWindow[] = {0, 0};
        Float  fatherProbability[] = {0.0f, 0.0f};
        String mutationAlgorithm[] = {"CHANGE-VALUE" , "CHANGE-PERCENT"};
        Float  mutationRate[] = {0.05f, 0.1f};
        String crossoverAlgorithm[] = {"SBX" , "SPX"};
        Float  crossoverRate[] = {0.9f, 0.95f};
        Float  mu[] = {0.3f, 0.5f};
        String replaceAlgorithm[] = {"ALWAYS-REPLACE", "WORST-REPLACING"};

        SimPopulationDTO pops[] = new SimPopulationDTO[populations.size()];

        for (int cnt = 0; cnt < populations.size(); cnt++)
            pops[cnt] = new SimPopulationDTO(cnt, populations.get(cnt).getId(),
                new SimPopSelectDTO(motherAlgorithm[cnt % motherAlgorithm.length]), new SimPopSelectDTO(fatherAlgorithm[cnt % fatherAlgorithm.length]),
                new SimPopMutationDTO(mutationAlgorithm[cnt % mutationAlgorithm.length], mutationRate[cnt % mutationRate.length]),
                new SimPopCrossoverDTO(crossoverAlgorithm[cnt % crossoverAlgorithm.length], crossoverRate[cnt % crossoverRate.length], mu[cnt % mu.length]),
                new SimPopReplaceDTO(replaceAlgorithm[cnt % crossoverAlgorithm.length]));

        return pops;
    }

    private CustomAnnFixedGliaDTO getCustomAnnFixedGliaDTO(Integer neuron[], int numCombinationDataset, int numCombinationPopulation) throws Exception{
        //Integer neuron [] = {4, 3, 2};
        String tmpActivationFunction [] = {"SIGMOID", "SIGMOID", "SIGMOID"};
        String tmpGliaAlgorithm[] = {"DEPRESSION", "DEPRESSION", "DEPRESSION"};
        Float coWeight[] = {-1.0f, 10.0f};

        int iteration = 4;
        int activation = 2;

        int numWeight = 0;
        for (int cnt = 0; cnt < neuron.length-1; cnt++)
            numWeight += neuron[cnt] = neuron[cnt + 1];

        String activationFunction[] = new String[neuron.length];
        String gliaAlgorithm[] = new String[neuron.length];
        Float increase[] = new Float[neuron.length];
        Float decrease[] = new Float[neuron.length];

        for (int cnt = 0; cnt < neuron.length; cnt++) {
            activationFunction[cnt] = tmpActivationFunction[cnt % tmpActivationFunction.length];
            gliaAlgorithm[cnt] = tmpGliaAlgorithm[cnt % tmpGliaAlgorithm.length];
            increase[cnt] = cnt / 100f;
            decrease[cnt] = 1f - cnt/100f;
        }

        Dataset dataset         = createEntities.createDataset(neuron[0], neuron[neuron.length-1], numCombinationDataset, null);
        Population population   = createEntities.createPopulation(0, numWeight, numCombinationPopulation);
        ProblemType problemType = createEntities.getProblemType("AnnFixedGlia");
        ErrorType errorType     = createEntities.getErrorType("MSE");
        SshServer sshServer     = createEntities.createSshServer();

        log.debug("Dataset      '{}'", dataset.getName());
        log.debug("Population   '{}'", population.getName());
        log.debug("ProblemType  '{}'", problemType.getName());
        log.debug("ErrorType    '{}'", errorType.getName());
        log.debug("SshServer    '{}'", sshServer.getDnsname());

        CommonSimulationDTO commonSimulationDTO = new CommonSimulationDTO("ann fixed glia DTO", "description", dataset.getId(), problemType.getId(), errorType.getId(), sshServer.getId());

        List<Population> populations = new ArrayList<>();
        populations.add(population);

        SimExecDTO simExecDTO = new SimExecDTO("amd", sshServer.getMaxProc(), sshServer.getMaxMemory(), sshServer.getMaxTime());

        CustomAnnFixedGliaDTO customAnnFixedGliaDTO = new CustomAnnFixedGliaDTO();

        customAnnFixedGliaDTO.setGaBasic(new SimGaBasicDTO(10, 0.2f));

        customAnnFixedGliaDTO.setDetail(commonSimulationDTO);
        customAnnFixedGliaDTO.setExecution(simExecDTO);
        customAnnFixedGliaDTO.setPopulations(getSimPopulationDTO(populations));

        customAnnFixedGliaDTO.setLayer(neuron.length);
        customAnnFixedGliaDTO.setIteration(iteration);
        customAnnFixedGliaDTO.setActivation(activation);
        customAnnFixedGliaDTO.setNeuron(Arrays.asList(neuron));
        customAnnFixedGliaDTO.setActivationFunction(Arrays.asList(activationFunction));
        customAnnFixedGliaDTO.setGliaAlgorithm(Arrays.asList(gliaAlgorithm));
        customAnnFixedGliaDTO.setIncrease(Arrays.asList(increase));
        customAnnFixedGliaDTO.setDecrease(Arrays.asList(decrease));
        customAnnFixedGliaDTO.setCoWeight(Arrays.asList(coWeight));

        return customAnnFixedGliaDTO;
    }

    private CustomAngnSearchGliaDTO getCustomAngnSearchGliaDTO(Integer neuron[], int numCombinationDataset, int numCombinationPopulation) throws Exception {
        String tmpActivationFunction [] = {"SIGMOID", "SIGMOID", "SIGMOID"};
        int sizeShort = 5;
        boolean sameGliaAlgorithm = true;
        boolean decreaseGreatherThanIncrease = true;
        Float coWeight[] = {-1.0f, 10.0f};
        Float coIncrease[] = {0.0f, 1.0f};
        Float coDecrease[] = {0.0f, 1.0f};
        Integer coIteration[] = {4, 8};
        Integer coActivation[] = {2, 6};

        int numWeight = 0;
        for (int cnt = 0; cnt < neuron.length-1; cnt++)
            numWeight += neuron[cnt] = neuron[cnt + 1];

        String activationFunction[] = new String[neuron.length];
        for (int cnt = 0; cnt < neuron.length; cnt++) {
            activationFunction[cnt] = tmpActivationFunction[cnt % tmpActivationFunction.length];
        }

        Dataset dataset         = createEntities.createDataset(neuron[0],neuron[neuron.length-1], numCombinationDataset , null);
        List<Population> populations = new ArrayList<>(2);
        populations.add(createEntities.createPopulation(0, numWeight, numCombinationPopulation));
        populations.add(createEntities.createPopulation(sizeShort, 0, numCombinationPopulation));
        ProblemType problemType = createEntities.getProblemType("AngnSearchGlia");
        ErrorType errorType     = createEntities.getErrorType("MSE");
        SshServer sshServer     = createEntities.createSshServer();

        Short fitnessBest = 1;
        Short fitnessRandom = 2;

        log.debug("Dataset      '{}'", dataset.getName());
        populations.stream().forEach(p ->
            log.debug("Population '{}'", p.getName())
        );
        log.debug("ProblemType  '{}'", problemType.getName());
        log.debug("ErrorType    '{}'", errorType.getName());
        log.debug("SshServer    '{}'", sshServer.getDnsname());

        List<Long> populationsId = new ArrayList<>(populations.size());
        populations.stream().forEach(p -> populationsId.add(p.getId()));

        SimPopulationDTO pops[] = getSimPopulationDTO(populations);

        CommonSimulationDTO commonSimulationDTO = new CommonSimulationDTO("angn search glia DTO", "angn desc", dataset.getId(), problemType.getId(), errorType.getId(), sshServer.getId());

        SimExecDTO simExecDTO = new SimExecDTO("amd", sshServer.getMaxProc(), sshServer.getMaxMemory(), sshServer.getMaxTime());

        CustomAngnSearchGliaDTO customAngnSearchGliaDTO = new CustomAngnSearchGliaDTO();

        customAngnSearchGliaDTO.setGaBasic(new SimGaBasicDTO(10, 0.2f));

        customAngnSearchGliaDTO.setDetail(commonSimulationDTO);
        customAngnSearchGliaDTO.setExecution(simExecDTO);
        customAngnSearchGliaDTO.setGa(new SimGaDTO("MIN", fitnessBest, fitnessRandom));
        customAngnSearchGliaDTO.setPopulations(pops);

        customAngnSearchGliaDTO.setLayer(neuron.length);
        customAngnSearchGliaDTO.setNeuron(Arrays.asList(neuron));
        customAngnSearchGliaDTO.setActivationFunction(Arrays.asList(activationFunction));
        customAngnSearchGliaDTO.setSameGliaAlgorithm(sameGliaAlgorithm);
        customAngnSearchGliaDTO.setDecreaseGreatherThanIncrease(decreaseGreatherThanIncrease);
        customAngnSearchGliaDTO.setCoWeight(Arrays.asList(coWeight));
        customAngnSearchGliaDTO.setCoIteration(Arrays.asList(coIteration));
        customAngnSearchGliaDTO.setCoActivation(Arrays.asList(coActivation));
        customAngnSearchGliaDTO.setCoIncrease(Arrays.asList(coIncrease));
        customAngnSearchGliaDTO.setCoDecrease(Arrays.asList(coDecrease));

        return customAngnSearchGliaDTO;
    }

    private CustomNuSvmDTO getCustomNuSvmDTO(int numCombinationDataset, int numCombinationPopulation) throws Exception {
        int sizeShort = 3;
        int sizeFloat = 7;
        Integer coDegree[] = {4, 8};
        Float coNu[]        = {0.1f, 1.0f};
        Float coTolerance[] = {0.1f, 1.0f};
        Float coGamma[]     = {0.1f, 1.0f};
        Float coCoef[]      = {0.1f, 1.0f};


        Dataset dataset         = createEntities.createDataset(10, 1, numCombinationDataset, null);
        Population population   = createEntities.createPopulation(sizeShort, sizeFloat, numCombinationPopulation);
        ProblemType problemType = createEntities.getProblemType("nuSVM");
        ErrorType errorType     = createEntities.getErrorType("MSE");
        SshServer sshServer     = createEntities.createSshServer();

        log.debug("Dataset      {}", dataset.getName());
        log.debug("Population   {}", population.getName());
        log.debug("ProblemType  {}", problemType.getName());
        log.debug("ErrorType    {}", errorType.getName());
        log.debug("SshServer    {}", sshServer.getDnsname());

        CommonSimulationDTO commonSimulationDTO = new CommonSimulationDTO("nu svm DTO", "description", dataset.getId(), problemType.getId(), errorType.getId(), sshServer.getId());

        List<Population> populations = new ArrayList<>(1);
        populations.add(population);

        SimExecDTO simExecDTO = new SimExecDTO("amd", sshServer.getMaxProc(), sshServer.getMaxMemory(), sshServer.getMaxTime());

        CustomNuSvmDTO customNuSvmDTO = new CustomNuSvmDTO();

        customNuSvmDTO.setGaBasic(new SimGaBasicDTO(10, 0.2f));

        customNuSvmDTO.setDetail(commonSimulationDTO);
        customNuSvmDTO.setExecution(simExecDTO);
        customNuSvmDTO.setPopulations(getSimPopulationDTO(populations));

        customNuSvmDTO.setCoNu(Arrays.asList(coNu));
        customNuSvmDTO.setCoTolerance(Arrays.asList(coTolerance));
        customNuSvmDTO.setCoDegree(Arrays.asList(coDegree));
        customNuSvmDTO.setCoGamma(Arrays.asList(coGamma));
        customNuSvmDTO.setCoCoef(Arrays.asList(coCoef));

        return customNuSvmDTO;
    }

    @Test
    @Transactional
    public void createAnnFixedGlia() throws Exception {
        simulationRepository.deleteAll();
        // add some extra data
        createEntities.createDataset(10, 1, 10, null);
        createEntities.createPopulation(10,10,10);


        Integer neuron[] = {4,3,2};
        int comDataset = 3;
        int comPopulation = 4;
        CustomAnnFixedGliaDTO customAnnFixedGliaDTO = getCustomAnnFixedGliaDTO(neuron, comDataset, comPopulation);

        testUtilPrivacy = new TestUtilPrivacy(simulationRepository, context, "simulations-ann-fixed-glia");

        ResultActions resultActions = testUtilPrivacy.createEntity(customAnnFixedGliaDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        resultActions
            .andExpect(jsonPath("$.name").value(customAnnFixedGliaDTO.getDetail().getName()))
            .andExpect(jsonPath("$.description").value(customAnnFixedGliaDTO.getDetail().getDescription()))
            .andExpect(jsonPath("$.belongId").value(owner.getId().intValue()))
            .andExpect(jsonPath("$.belongLogin").value(owner.getLogin()));


        List<Simulation> list = simulationRepository.findAll();
        assertThat(list).hasSize(1);

        Simulation simulation = list.get(0);

        List<SpecificSimulation> specificSimulationList = specificSimulationRepository.findAllByBelongId(simulation.getId());
        assertThat(specificSimulationList).hasSize(comDataset*comPopulation);

        Set<DatasetCombination> datasetCombinationSet = new LinkedHashSet<>(comDataset);
        Set<PopulationCombination> populationCombinationSet = new LinkedHashSet<>(comPopulation);

        specificSimulationList.stream().forEach(ss -> {
            List<PopulationCombinationOrder> pco = populationCombinationOrderRepository.findAllByBelongIdOrderByIndexOrder(ss.getId());
            assertThat(pco).hasSize(1);
            assertThat(pco.get(0).getIndexOrder()).isEqualTo(0);

            datasetCombinationSet.add(ss.getUseData());
            populationCombinationSet.add(pco.get(0).getUse());
        });

        // Comprobamos si cogemos los dataset correctos
        datasetCombinationRepository.findAll().stream()
            .filter(dc -> dc.getBelong().getId() == customAnnFixedGliaDTO.getDetail().getDatasetId())
            .forEach(dc -> {
                log.debug(">>dc {} belong {}", dc.getIndexCombination(), dc.getBelong().getId());
                assertThat(dc).isIn(datasetCombinationSet);
                assertThat(datasetCombinationSet.remove(dc)).isTrue();
            });
        assertThat(datasetCombinationSet).hasSize(0);

        // Comprobamos si cogemos las poblaciones correctas
        populationCombinationRepository.findAll().stream()
            .filter(pc -> pc.getBelong().getId() == customAnnFixedGliaDTO.getPopulations()[0].getPopulationId())
            .forEach(pc -> {
                log.debug(">>pc {} belong {}", pc.getIndexCombination(), pc.getBelong().getId());
                assertThat(pc).isIn(populationCombinationSet);
                assertThat(populationCombinationSet.remove(pc)).isTrue();
            });
        assertThat(populationCombinationSet).hasSize(0);
    }


    @Test
    @Transactional
    public void createAngnSearchGlia() throws Exception {
        simulationRepository.deleteAll();
        // add some extra data
        createEntities.createDataset(10, 1, 10, null);
        createEntities.createPopulation(10,10,10);

        Integer neuron[] = {4,3,2};
        int comDataset = 3;
        int comPopulation = 4;
        CustomAngnSearchGliaDTO customAngnSearchGliaDTO = getCustomAngnSearchGliaDTO(neuron, comDataset, comPopulation);

        testUtilPrivacy = new TestUtilPrivacy(simulationRepository, context, "simulations-angn-search-glia");

        ResultActions resultActions = testUtilPrivacy.createEntity(customAngnSearchGliaDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        resultActions
            .andExpect(jsonPath("$.name").value(customAngnSearchGliaDTO.getDetail().getName()))
            .andExpect(jsonPath("$.description").value(customAngnSearchGliaDTO.getDetail().getDescription()))
            .andExpect(jsonPath("$.belongId").value(owner.getId().intValue()))
            .andExpect(jsonPath("$.belongLogin").value(owner.getLogin()));

        List<Simulation> list = simulationRepository.findAll();
        assertThat(list).hasSize(1);

        Simulation simulation = list.get(0);

        List<SpecificSimulation> specificSimulationList = specificSimulationRepository.findAllByBelongId(simulation.getId());
        assertThat(specificSimulationList).hasSize(comDataset*comPopulation);

        Set<DatasetCombination> datasetCombinationSet = new LinkedHashSet<>(comDataset);
        Set<PopulationCombination> populationCombinationSet1 = new LinkedHashSet<>(comPopulation);
        Set<PopulationCombination> populationCombinationSet2 = new LinkedHashSet<>(comPopulation);

        specificSimulationList.stream().forEach(ss -> {
            List<PopulationCombinationOrder> pco = populationCombinationOrderRepository.findAllByBelongIdOrderByIndexOrder(ss.getId());
            assertThat(pco).hasSize(2);
            assertThat(pco.get(0).getIndexOrder()).isEqualTo(0);
            assertThat(pco.get(1).getIndexOrder()).isEqualTo(1);
            log.debug("ss\n\t{}\nDC\n\t{}\nPCO\n\t{}", ss.getId(), ss.getUseData().getIndexCombination(), pco);

            datasetCombinationSet.add(ss.getUseData());
            populationCombinationSet1.add(pco.get(0).getUse());
            populationCombinationSet2.add(pco.get(1).getUse());
        });


        // Comprobamos si cogemos los dataset correctos
        datasetCombinationRepository.findAll().stream()
            .filter(dc -> dc.getBelong().getId() == customAngnSearchGliaDTO.getDetail().getDatasetId())
            .forEach(dc -> {
                log.debug(">>dc {} belong {}", dc.getIndexCombination(), dc.getBelong().getId());
                assertThat(dc).isIn(datasetCombinationSet);
                assertThat(datasetCombinationSet.remove(dc)).isTrue();
            });
        assertThat(datasetCombinationSet).hasSize(0);


        List<PopulationCombination> populationCombinationList = populationCombinationRepository.findAll();

        Set<PopulationCombination> pcl[] = new Set[2];
        pcl[0] = populationCombinationSet1;
        pcl[1] = populationCombinationSet2;

        for (int cnt = 0; cnt < pcl.length; cnt++) {
            final int aux = cnt;
            populationCombinationList.stream()
                .filter(pc -> pc.getBelong().getId() == customAngnSearchGliaDTO.getPopulations()[aux].getPopulationId())
                .forEach(pc -> {
                    log.debug(">>({}) pc {} belong {}", aux, pc.getIndexCombination(), pc.getBelong().getId());
                    assertThat(pc).isIn(pcl[aux]);
                    assertThat(pcl[aux].remove(pc)).isTrue();
                });

            assertThat(pcl[aux]).hasSize(0);
        }

    }

    @Test
    @Transactional
    public void createNuSvm() throws Exception {
        simulationRepository.deleteAll();
        // add some extra data
        createEntities.createDataset(10, 1, 10, null);
        createEntities.createPopulation(10,10,10);

        int comDataset = 3;
        int comPopulation = 4;
        CustomNuSvmDTO customNuSvmDTO = getCustomNuSvmDTO(comDataset, comPopulation);

        testUtilPrivacy = new TestUtilPrivacy(simulationRepository, context, "simulations-nu-svm");

        ResultActions resultActions = testUtilPrivacy.createEntity(customNuSvmDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        resultActions
            .andExpect(jsonPath("$.name").value(customNuSvmDTO.getDetail().getName()))
            .andExpect(jsonPath("$.description").value(customNuSvmDTO.getDetail().getDescription()))
            .andExpect(jsonPath("$.belongId").value(owner.getId().intValue()))
            .andExpect(jsonPath("$.belongLogin").value(owner.getLogin()));

        List<Simulation> list = simulationRepository.findAll();
        assertThat(list).hasSize(1);

        Simulation simulation = list.get(0);

        List<SpecificSimulation> specificSimulationList = specificSimulationRepository.findAllByBelongId(simulation.getId());
        assertThat(specificSimulationList).hasSize(comDataset*comPopulation);

        Set<DatasetCombination> datasetCombinationSet = new LinkedHashSet<>(comDataset);
        Set<PopulationCombination> populationCombinationSet = new LinkedHashSet<>(comPopulation);

        specificSimulationList.stream().forEach(ss -> {
            List<PopulationCombinationOrder> pco = populationCombinationOrderRepository.findAllByBelongIdOrderByIndexOrder(ss.getId());
            assertThat(pco).hasSize(1);
            assertThat(pco.get(0).getIndexOrder()).isEqualTo(0);
            log.debug("ss {}. DC {} PCO {}", ss.getId(), ss.getUseData().getIndexCombination(), pco.get(0));

            datasetCombinationSet.add(ss.getUseData());
            populationCombinationSet.add(pco.get(0).getUse());
        });

        // Comprobamos si cogemos los dataset correctos
        datasetCombinationRepository.findAll().stream()
            .filter(dc -> dc.getBelong().getId() == customNuSvmDTO.getDetail().getDatasetId())
            .forEach(dc -> {
                log.debug(">>dc {} belong {}", dc.getIndexCombination(), dc.getBelong().getId());
                assertThat(dc).isIn(datasetCombinationSet);
                assertThat(datasetCombinationSet.remove(dc)).isTrue();
            });
        assertThat(datasetCombinationSet).hasSize(0);

        // Comprobamos si cogemos las poblaciones correctas
        populationCombinationRepository.findAll().stream()
            .filter(pc -> pc.getBelong().getId() == customNuSvmDTO.getPopulations()[0].getPopulationId())
            .forEach(pc -> {
                log.debug(">>pc {} belong {}", pc.getIndexCombination(), pc.getBelong().getId());
                assertThat(pc).isIn(populationCombinationSet);
                assertThat(populationCombinationSet.remove(pc)).isTrue();
            });
        assertThat(populationCombinationSet).hasSize(0);

    }

    //
    // End of custom resource
    //

    @Test
    @Transactional
    public void createSimulationOwner() throws Exception {
        int databaseSizeBeforeCreate = simulationRepository.findAll().size();

        // Create the Simulation
        testUtilPrivacy.createEntity(simulationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.CREATED);

        // Validate the Simulation in the database
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeCreate + 1);
        Simulation testSimulation = simulations.get(simulations.size() - 1);
        assertThat(testSimulation.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSimulation.getTimestamp()).isAfter(DEFAULT_TIMESTAMP);
        assertThat(testSimulation.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Simulation in ElasticSearch
        //Simulation simulationEs = simulationSearchRepository.findOne(testSimulation.getId());
        //assertThat(simulationEs).isEqualToComparingFieldByField(testSimulation);
    }

    @Test
    @Transactional
    public void createSimulationOwnerAdmin() throws Exception {
        int databaseSizeBeforeCreate = simulationRepository.findAll().size();


        // Create the Simulation
        testUtilPrivacy.createEntity(simulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.CREATED);

        // Validate the Simulation in the database
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeCreate + 1);
        Simulation testSimulation = simulations.get(simulations.size() - 1);
        assertThat(testSimulation.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSimulation.getTimestamp()).isAfter(DEFAULT_TIMESTAMP);
        assertThat(testSimulation.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Simulation in ElasticSearch
        //Simulation simulationEs = simulationSearchRepository.findOne(testSimulation.getId());
        //assertThat(simulationEs).isEqualToComparingFieldByField(testSimulation);
    }

    @Test
    @Transactional
    public void createSimulation403() throws Exception {
        int databaseSizeBeforeCreate = simulationRepository.findAll().size();

        // Create the Simulation
        testUtilPrivacy.createEntity(simulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);

        // Validate the Simulation in the database
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createSimulation401() throws Exception {
        int databaseSizeBeforeCreate = simulationRepository.findAll().size();

        // Create the Simulation
        testUtilPrivacy.createEntityUnauthorized(simulationDTO);


        // Validate the Simulation in the database
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeCreate);
    }



    @Test
    @Transactional
    public void deleteSimulationOwner() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);
        int databaseSizeBeforeDelete = simulationRepository.findAll().size();

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.deleteEntity(simulationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);

        // Validate ElasticSearch is empty
        //boolean simulationExistsInEs = simulationSearchRepository.exists(simulation.getId());
        //assertThat(simulationExistsInEs).isFalse();

        // Validate the database is empty
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void deleteSimulationAdmin() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);
        int databaseSizeBeforeDelete = simulationRepository.findAll().size();

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.deleteEntity(simulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK);

        // Validate ElasticSearch is empty
        //boolean simulationExistsInEs = simulationSearchRepository.exists(simulation.getId());
        //assertThat(simulationExistsInEs).isFalse();

        // Validate the database is empty
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void deleteSimulation403() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);
        int databaseSizeBeforeDelete = simulationRepository.findAll().size();

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.deleteEntity(simulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);

        // Validate ElasticSearch is empty
        //boolean simulationExistsInEs = simulationSearchRepository.exists(simulation.getId());
        //assertThat(simulationExistsInEs).isFalse();

        // Validate the database is empty
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeDelete );
    }

    @Test
    @Transactional
    public void deleteSimulation401() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);
        int databaseSizeBeforeDelete = simulationRepository.findAll().size();

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.deleteEntityUnauthorized(simulationDTO);

        // Validate ElasticSearch is empty
        //boolean simulationExistsInEs = simulationSearchRepository.exists(simulation.getId());
        //assertThat(simulationExistsInEs).isFalse();

        // Validate the database is empty
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteSimulation404() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);
        int databaseSizeBeforeDelete = simulationRepository.findAll().size();

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.deleteEntityNotFound(Long.MAX_VALUE, owner.getLogin(), AuthoritiesConstants.ADMIN);

        // Validate ElasticSearch is empty
        //boolean simulationExistsInEs = simulationSearchRepository.exists(simulation.getId());
        //assertThat(simulationExistsInEs).isFalse();

        // Validate the database is empty
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeDelete);
    }



    @Test
    @Transactional
    public void getAllSimulationsOwner() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get all the simulations
        testUtilPrivacy.listEntities(owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK)
                .andExpect(jsonPath("$.[*].id").value(hasItem(simulation.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].belongId").value(hasItem(owner.getId().intValue())))
                .andExpect(jsonPath("$.[*].belongLogin").value(hasItem(owner.getLogin())));
    }

    @Test
    @Transactional
    public void getAllSimulationsAdmin() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get all the simulations
        testUtilPrivacy.listEntities("+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK)
            .andExpect(jsonPath("$.[*].id").value(hasItem(simulation.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].belongId").value(hasItem(owner.getId().intValue())))
            .andExpect(jsonPath("$.[*].belongLogin").value(hasItem(owner.getLogin())));
    }

    @Test
    @Transactional
    public void getAllSimulationsUserWithoutData() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get all the simulations
        testUtilPrivacy.listEntities("+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK)
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$.[*]").isEmpty());
    }



    @Test
    @Transactional
    public void getSimulationOwner() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.getEntity(simulationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK)
            .andExpect(jsonPath("$.id").value(simulation.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP_STR))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.belongId").value(owner.getId().intValue()))
            .andExpect(jsonPath("$.belongLogin").value(owner.getLogin()));
    }

    @Test
    @Transactional
    public void getSimulationAdmin() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.getEntity(simulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK)
            .andExpect(jsonPath("$.id").value(simulation.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP_STR))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.belongId").value(owner.getId().intValue()))
            .andExpect(jsonPath("$.belongLogin").value(owner.getLogin()));
    }

    @Test
    @Transactional
    public void getSimulationUser403() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.getEntity(simulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);
    }

    @Test
    @Transactional
    public void getSimulationUser404() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get the simulation
        testUtilPrivacy.getEntityNotFound(Long.MAX_VALUE, owner.getLogin(), AuthoritiesConstants.ADMIN);
    }

    @Test
    @Transactional
    public void getSimulationUser401() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);

        // Get the simulation
        simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);
        testUtilPrivacy.getEntityUnauthorized(simulationDTO);
    }


    @Test
    @Transactional
    public void updateSimulationOwner() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);
        int databaseSizeBeforeUpdate = simulationRepository.findAll().size();

        // Update the simulation
        Simulation updatedSimulation = new Simulation();
        updatedSimulation.setId(simulation.getId());
        updatedSimulation.setName(UPDATED_NAME);
        updatedSimulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedSimulation.setDescription(UPDATED_DESCRIPTION);
        updatedSimulation.setBelong(owner);
        SimulationDTO updateSimulationDTO = simulationMapper.simulationToSimulationDTO(updatedSimulation);

        testUtilPrivacy.updateEntity(simulationDTO, updateSimulationDTO, owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.OK);

        // Validate the Simulation in the database
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeUpdate);
        Simulation testSimulation = simulations.get(simulations.size() - 1);
        assertThat(testSimulation.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSimulation.getTimestamp()).isAfter(UPDATED_TIMESTAMP);
        assertThat(testSimulation.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Simulation in ElasticSearch
        //Simulation simulationEs = simulationSearchRepository.findOne(testSimulation.getId());
        //assertThat(simulationEs).isEqualToComparingFieldByField(testSimulation);
    }

    @Test
    @Transactional
    public void updateSimulationAdmin() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);
        int databaseSizeBeforeUpdate = simulationRepository.findAll().size();

        // Update the simulation
        Simulation updatedSimulation = new Simulation();
        updatedSimulation.setId(simulation.getId());
        updatedSimulation.setName(UPDATED_NAME);
        updatedSimulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedSimulation.setDescription(UPDATED_DESCRIPTION);
        SimulationDTO simulationDTO = simulationMapper.simulationToSimulationDTO(updatedSimulation);
        updatedSimulation.setBelong(owner);
        SimulationDTO updateSimulationDTO = simulationMapper.simulationToSimulationDTO(updatedSimulation);

        testUtilPrivacy.updateEntity(simulationDTO, updateSimulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.ADMIN, HttpStatus.OK);

        // Validate the Simulation in the database
        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeUpdate);
        Simulation testSimulation = simulations.get(simulations.size() - 1);
        assertThat(testSimulation.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSimulation.getTimestamp()).isAfter(UPDATED_TIMESTAMP);
        assertThat(testSimulation.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Simulation in ElasticSearch
        //Simulation simulationEs = simulationSearchRepository.findOne(testSimulation.getId());
        //assertThat(simulationEs).isEqualToComparingFieldByField(testSimulation);
    }

    @Test
    @Transactional
    public void updateSimulation403() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);

        // Update the simulation
        Simulation updatedSimulation = new Simulation();
        updatedSimulation.setId(simulation.getId());
        updatedSimulation.setName(UPDATED_NAME);
        updatedSimulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedSimulation.setDescription(UPDATED_DESCRIPTION);
        updatedSimulation.setBelong(owner);
        SimulationDTO updateSimulationDTO = simulationMapper.simulationToSimulationDTO(updatedSimulation);

        testUtilPrivacy.updateEntity(simulationDTO, updateSimulationDTO, "+"+owner.getLogin(), AuthoritiesConstants.USER, HttpStatus.FORBIDDEN);
    }

    @Test
    @Transactional
    public void updateSimulation401() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);

        // Update the simulation
        Simulation updatedSimulation = new Simulation();
        updatedSimulation.setId(simulation.getId());
        updatedSimulation.setName(UPDATED_NAME);
        updatedSimulation.setTimestamp(UPDATED_TIMESTAMP);
        updatedSimulation.setDescription(UPDATED_DESCRIPTION);
        updatedSimulation.setBelong(owner);
        SimulationDTO updateSimulationDTO = simulationMapper.simulationToSimulationDTO(updatedSimulation);

        testUtilPrivacy.updateEntityUnauthorized(simulationDTO, updateSimulationDTO);
    }


/*
    @Test
    @Transactional
    public void searchSimulation() throws Exception {
        // Initialize the database
        simulationRepository.saveAndFlush(simulation);
        simulationSearchRepository.save(simulation);

        // Search the simulation
        restSimulationMockMvc.perform(get("/api/_search/simulations?query=id:" + simulation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(simulation.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP_STR)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
*/

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = simulationRepository.findAll().size();
        // set the field null
        simulation.setName(null);

        // Create the Simulation, which fails.
        SimulationDTO simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);

        restSimulationMockMvc.perform(post("/api/simulations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simulationDTO)))
            .andExpect(status().isBadRequest());

        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeTest);
    }

    /*
    @Test
    @Transactional
    public void checkTimestampIsRequired() throws Exception {
        int databaseSizeBeforeTest = simulationRepository.findAll().size();
        // set the field null
        simulation.setTimestamp(null);

        // Create the Simulation, which fails.
        SimulationDTO simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);

        restSimulationMockMvc.perform(post("/api/simulations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simulationDTO)))
            .andExpect(status().isBadRequest());

        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = simulationRepository.findAll().size();
        // set the field null
        simulation.setDescription(null);

        // Create the Simulation, which fails.
        SimulationDTO simulationDTO = simulationMapper.simulationToSimulationDTO(simulation);

        restSimulationMockMvc.perform(post("/api/simulations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simulationDTO)))
            .andExpect(status().isBadRequest());

        List<Simulation> simulations = simulationRepository.findAll();
        assertThat(simulations).hasSize(databaseSizeBeforeTest);
    }
    */
}
