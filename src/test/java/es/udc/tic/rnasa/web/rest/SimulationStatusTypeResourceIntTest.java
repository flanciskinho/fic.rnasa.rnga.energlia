/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.SimulationStatusType;
import es.udc.tic.rnasa.repository.SimulationStatusTypeRepository;
import es.udc.tic.rnasa.service.SimulationStatusTypeService;
import es.udc.tic.rnasa.repository.search.SimulationStatusTypeSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.SimulationStatusTypeDTO;
import es.udc.tic.rnasa.web.rest.mapper.SimulationStatusTypeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SimulationStatusTypeResource REST controller.
 *
 * @see SimulationStatusTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SimulationStatusTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private SimulationStatusTypeRepository simulationStatusTypeRepository;

    @Inject
    private SimulationStatusTypeMapper simulationStatusTypeMapper;

    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;

    @Inject
    private SimulationStatusTypeSearchRepository simulationStatusTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSimulationStatusTypeMockMvc;

    private SimulationStatusType simulationStatusType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SimulationStatusTypeResource simulationStatusTypeResource = new SimulationStatusTypeResource();
        ReflectionTestUtils.setField(simulationStatusTypeResource, "simulationStatusTypeService", simulationStatusTypeService);
        ReflectionTestUtils.setField(simulationStatusTypeResource, "simulationStatusTypeMapper", simulationStatusTypeMapper);
        this.restSimulationStatusTypeMockMvc = MockMvcBuilders.standaloneSetup(simulationStatusTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        simulationStatusTypeSearchRepository.deleteAll();
        simulationStatusType = new SimulationStatusType();
        simulationStatusType.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createSimulationStatusType() throws Exception {
        int databaseSizeBeforeCreate = simulationStatusTypeRepository.findAll().size();

        // Create the SimulationStatusType
        SimulationStatusTypeDTO simulationStatusTypeDTO = simulationStatusTypeMapper.simulationStatusTypeToSimulationStatusTypeDTO(simulationStatusType);

        restSimulationStatusTypeMockMvc.perform(post("/api/simulation-status-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simulationStatusTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the SimulationStatusType in the database
        List<SimulationStatusType> simulationStatusTypes = simulationStatusTypeRepository.findAll();
        assertThat(simulationStatusTypes).hasSize(databaseSizeBeforeCreate + 1);
        SimulationStatusType testSimulationStatusType = simulationStatusTypes.get(simulationStatusTypes.size() - 1);
        assertThat(testSimulationStatusType.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the SimulationStatusType in ElasticSearch
        SimulationStatusType simulationStatusTypeEs = simulationStatusTypeSearchRepository.findOne(testSimulationStatusType.getId());
        assertThat(simulationStatusTypeEs).isEqualToComparingFieldByField(testSimulationStatusType);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = simulationStatusTypeRepository.findAll().size();
        // set the field null
        simulationStatusType.setName(null);

        // Create the SimulationStatusType, which fails.
        SimulationStatusTypeDTO simulationStatusTypeDTO = simulationStatusTypeMapper.simulationStatusTypeToSimulationStatusTypeDTO(simulationStatusType);

        restSimulationStatusTypeMockMvc.perform(post("/api/simulation-status-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simulationStatusTypeDTO)))
                .andExpect(status().isBadRequest());

        List<SimulationStatusType> simulationStatusTypes = simulationStatusTypeRepository.findAll();
        assertThat(simulationStatusTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSimulationStatusTypes() throws Exception {
        // Initialize the database
        simulationStatusTypeRepository.saveAndFlush(simulationStatusType);

        // Get all the simulationStatusTypes
        restSimulationStatusTypeMockMvc.perform(get("/api/simulation-status-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(simulationStatusType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSimulationStatusType() throws Exception {
        // Initialize the database
        simulationStatusTypeRepository.saveAndFlush(simulationStatusType);

        // Get the simulationStatusType
        restSimulationStatusTypeMockMvc.perform(get("/api/simulation-status-types/{id}", simulationStatusType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(simulationStatusType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSimulationStatusType() throws Exception {
        // Get the simulationStatusType
        restSimulationStatusTypeMockMvc.perform(get("/api/simulation-status-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSimulationStatusType() throws Exception {
        // Initialize the database
        simulationStatusTypeRepository.saveAndFlush(simulationStatusType);
        simulationStatusTypeSearchRepository.save(simulationStatusType);
        int databaseSizeBeforeUpdate = simulationStatusTypeRepository.findAll().size();

        // Update the simulationStatusType
        SimulationStatusType updatedSimulationStatusType = new SimulationStatusType();
        updatedSimulationStatusType.setId(simulationStatusType.getId());
        updatedSimulationStatusType.setName(UPDATED_NAME);
        SimulationStatusTypeDTO simulationStatusTypeDTO = simulationStatusTypeMapper.simulationStatusTypeToSimulationStatusTypeDTO(updatedSimulationStatusType);

        restSimulationStatusTypeMockMvc.perform(put("/api/simulation-status-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(simulationStatusTypeDTO)))
                .andExpect(status().isOk());

        // Validate the SimulationStatusType in the database
        List<SimulationStatusType> simulationStatusTypes = simulationStatusTypeRepository.findAll();
        assertThat(simulationStatusTypes).hasSize(databaseSizeBeforeUpdate);
        SimulationStatusType testSimulationStatusType = simulationStatusTypes.get(simulationStatusTypes.size() - 1);
        assertThat(testSimulationStatusType.getName()).isEqualTo(UPDATED_NAME);

        // Validate the SimulationStatusType in ElasticSearch
        SimulationStatusType simulationStatusTypeEs = simulationStatusTypeSearchRepository.findOne(testSimulationStatusType.getId());
        assertThat(simulationStatusTypeEs).isEqualToComparingFieldByField(testSimulationStatusType);
    }

    @Test
    @Transactional
    public void deleteSimulationStatusType() throws Exception {
        // Initialize the database
        simulationStatusTypeRepository.saveAndFlush(simulationStatusType);
        simulationStatusTypeSearchRepository.save(simulationStatusType);
        int databaseSizeBeforeDelete = simulationStatusTypeRepository.findAll().size();

        // Get the simulationStatusType
        restSimulationStatusTypeMockMvc.perform(delete("/api/simulation-status-types/{id}", simulationStatusType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean simulationStatusTypeExistsInEs = simulationStatusTypeSearchRepository.exists(simulationStatusType.getId());
        assertThat(simulationStatusTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<SimulationStatusType> simulationStatusTypes = simulationStatusTypeRepository.findAll();
        assertThat(simulationStatusTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSimulationStatusType() throws Exception {
        // Initialize the database
        simulationStatusTypeRepository.saveAndFlush(simulationStatusType);
        simulationStatusTypeSearchRepository.save(simulationStatusType);

        // Search the simulationStatusType
        restSimulationStatusTypeMockMvc.perform(get("/api/_search/simulation-status-types?query=id:" + simulationStatusType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(simulationStatusType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
}
