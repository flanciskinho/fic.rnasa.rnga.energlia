/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.SizeParam;
import es.udc.tic.rnasa.repository.SizeParamRepository;
import es.udc.tic.rnasa.service.SizeParamService;
import es.udc.tic.rnasa.repository.search.SizeParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.SizeParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.SizeParamMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SizeParamResource REST controller.
 *
 * @see SizeParamResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SizeParamResourceIntTest {


    private static final Integer DEFAULT_SIZE = 1;
    private static final Integer UPDATED_SIZE = 2;

    @Inject
    private SizeParamRepository sizeParamRepository;

    @Inject
    private SizeParamMapper sizeParamMapper;

    @Inject
    private SizeParamService sizeParamService;

    @Inject
    private SizeParamSearchRepository sizeParamSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSizeParamMockMvc;

    private SizeParam sizeParam;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SizeParamResource sizeParamResource = new SizeParamResource();
        ReflectionTestUtils.setField(sizeParamResource, "sizeParamService", sizeParamService);
        ReflectionTestUtils.setField(sizeParamResource, "sizeParamMapper", sizeParamMapper);
        this.restSizeParamMockMvc = MockMvcBuilders.standaloneSetup(sizeParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        sizeParamSearchRepository.deleteAll();
        sizeParam = new SizeParam();
        sizeParam.setSize(DEFAULT_SIZE);
    }

    @Test
    @Transactional
    public void createSizeParam() throws Exception {
        int databaseSizeBeforeCreate = sizeParamRepository.findAll().size();

        // Create the SizeParam
        SizeParamDTO sizeParamDTO = sizeParamMapper.sizeParamToSizeParamDTO(sizeParam);

        restSizeParamMockMvc.perform(post("/api/size-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(sizeParamDTO)))
                .andExpect(status().isCreated());

        // Validate the SizeParam in the database
        List<SizeParam> sizeParams = sizeParamRepository.findAll();
        assertThat(sizeParams).hasSize(databaseSizeBeforeCreate + 1);
        SizeParam testSizeParam = sizeParams.get(sizeParams.size() - 1);
        assertThat(testSizeParam.getSize()).isEqualTo(DEFAULT_SIZE);

        // Validate the SizeParam in ElasticSearch
        SizeParam sizeParamEs = sizeParamSearchRepository.findOne(testSizeParam.getId());
        assertThat(sizeParamEs).isEqualToComparingFieldByField(testSizeParam);
    }

    @Test
    @Transactional
    public void checkSizeIsRequired() throws Exception {
        int databaseSizeBeforeTest = sizeParamRepository.findAll().size();
        // set the field null
        sizeParam.setSize(null);

        // Create the SizeParam, which fails.
        SizeParamDTO sizeParamDTO = sizeParamMapper.sizeParamToSizeParamDTO(sizeParam);

        restSizeParamMockMvc.perform(post("/api/size-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(sizeParamDTO)))
                .andExpect(status().isBadRequest());

        List<SizeParam> sizeParams = sizeParamRepository.findAll();
        assertThat(sizeParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSizeParams() throws Exception {
        // Initialize the database
        sizeParamRepository.saveAndFlush(sizeParam);

        // Get all the sizeParams
        restSizeParamMockMvc.perform(get("/api/size-params?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(sizeParam.getId().intValue())))
                .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE)));
    }

    @Test
    @Transactional
    public void getSizeParam() throws Exception {
        // Initialize the database
        sizeParamRepository.saveAndFlush(sizeParam);

        // Get the sizeParam
        restSizeParamMockMvc.perform(get("/api/size-params/{id}", sizeParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(sizeParam.getId().intValue()))
            .andExpect(jsonPath("$.size").value(DEFAULT_SIZE));
    }

    @Test
    @Transactional
    public void getNonExistingSizeParam() throws Exception {
        // Get the sizeParam
        restSizeParamMockMvc.perform(get("/api/size-params/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSizeParam() throws Exception {
        // Initialize the database
        sizeParamRepository.saveAndFlush(sizeParam);
        sizeParamSearchRepository.save(sizeParam);
        int databaseSizeBeforeUpdate = sizeParamRepository.findAll().size();

        // Update the sizeParam
        SizeParam updatedSizeParam = new SizeParam();
        updatedSizeParam.setId(sizeParam.getId());
        updatedSizeParam.setSize(UPDATED_SIZE);
        SizeParamDTO sizeParamDTO = sizeParamMapper.sizeParamToSizeParamDTO(updatedSizeParam);

        restSizeParamMockMvc.perform(put("/api/size-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(sizeParamDTO)))
                .andExpect(status().isOk());

        // Validate the SizeParam in the database
        List<SizeParam> sizeParams = sizeParamRepository.findAll();
        assertThat(sizeParams).hasSize(databaseSizeBeforeUpdate);
        SizeParam testSizeParam = sizeParams.get(sizeParams.size() - 1);
        assertThat(testSizeParam.getSize()).isEqualTo(UPDATED_SIZE);

        // Validate the SizeParam in ElasticSearch
        SizeParam sizeParamEs = sizeParamSearchRepository.findOne(testSizeParam.getId());
        assertThat(sizeParamEs).isEqualToComparingFieldByField(testSizeParam);
    }

    @Test
    @Transactional
    public void deleteSizeParam() throws Exception {
        // Initialize the database
        sizeParamRepository.saveAndFlush(sizeParam);
        sizeParamSearchRepository.save(sizeParam);
        int databaseSizeBeforeDelete = sizeParamRepository.findAll().size();

        // Get the sizeParam
        restSizeParamMockMvc.perform(delete("/api/size-params/{id}", sizeParam.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean sizeParamExistsInEs = sizeParamSearchRepository.exists(sizeParam.getId());
        assertThat(sizeParamExistsInEs).isFalse();

        // Validate the database is empty
        List<SizeParam> sizeParams = sizeParamRepository.findAll();
        assertThat(sizeParams).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSizeParam() throws Exception {
        // Initialize the database
        sizeParamRepository.saveAndFlush(sizeParam);
        sizeParamSearchRepository.save(sizeParam);

        // Search the sizeParam
        restSizeParamMockMvc.perform(get("/api/_search/size-params?query=id:" + sizeParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sizeParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE)));
    }
}
