/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.SizeType;
import es.udc.tic.rnasa.repository.SizeTypeRepository;
import es.udc.tic.rnasa.service.SizeTypeService;
import es.udc.tic.rnasa.repository.search.SizeTypeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SizeTypeResource REST controller.
 *
 * @see SizeTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SizeTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    @Inject
    private SizeTypeRepository sizeTypeRepository;

    @Inject
    private SizeTypeService sizeTypeService;

    @Inject
    private SizeTypeSearchRepository sizeTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSizeTypeMockMvc;

    private SizeType sizeType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SizeTypeResource sizeTypeResource = new SizeTypeResource();
        ReflectionTestUtils.setField(sizeTypeResource, "sizeTypeService", sizeTypeService);
        this.restSizeTypeMockMvc = MockMvcBuilders.standaloneSetup(sizeTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        sizeTypeSearchRepository.deleteAll();
        sizeType = new SizeType();
        sizeType.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createSizeType() throws Exception {
        int databaseSizeBeforeCreate = sizeTypeRepository.findAll().size();

        // Create the SizeType

        restSizeTypeMockMvc.perform(post("/api/size-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(sizeType)))
                .andExpect(status().isCreated());

        // Validate the SizeType in the database
        List<SizeType> sizeTypes = sizeTypeRepository.findAll();
        assertThat(sizeTypes).hasSize(databaseSizeBeforeCreate + 1);
        SizeType testSizeType = sizeTypes.get(sizeTypes.size() - 1);
        assertThat(testSizeType.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the SizeType in ElasticSearch
        SizeType sizeTypeEs = sizeTypeSearchRepository.findOne(testSizeType.getId());
        assertThat(sizeTypeEs).isEqualToComparingFieldByField(testSizeType);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = sizeTypeRepository.findAll().size();
        // set the field null
        sizeType.setName(null);

        // Create the SizeType, which fails.

        restSizeTypeMockMvc.perform(post("/api/size-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(sizeType)))
                .andExpect(status().isBadRequest());

        List<SizeType> sizeTypes = sizeTypeRepository.findAll();
        assertThat(sizeTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSizeTypes() throws Exception {
        // Initialize the database
        sizeTypeRepository.saveAndFlush(sizeType);

        // Get all the sizeTypes
        restSizeTypeMockMvc.perform(get("/api/size-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(sizeType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSizeType() throws Exception {
        // Initialize the database
        sizeTypeRepository.saveAndFlush(sizeType);

        // Get the sizeType
        restSizeTypeMockMvc.perform(get("/api/size-types/{id}", sizeType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(sizeType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSizeType() throws Exception {
        // Get the sizeType
        restSizeTypeMockMvc.perform(get("/api/size-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSizeType() throws Exception {
        // Initialize the database
        sizeTypeService.save(sizeType);

        int databaseSizeBeforeUpdate = sizeTypeRepository.findAll().size();

        // Update the sizeType
        SizeType updatedSizeType = new SizeType();
        updatedSizeType.setId(sizeType.getId());
        updatedSizeType.setName(UPDATED_NAME);

        restSizeTypeMockMvc.perform(put("/api/size-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedSizeType)))
                .andExpect(status().isOk());

        // Validate the SizeType in the database
        List<SizeType> sizeTypes = sizeTypeRepository.findAll();
        assertThat(sizeTypes).hasSize(databaseSizeBeforeUpdate);
        SizeType testSizeType = sizeTypes.get(sizeTypes.size() - 1);
        assertThat(testSizeType.getName()).isEqualTo(UPDATED_NAME);

        // Validate the SizeType in ElasticSearch
        SizeType sizeTypeEs = sizeTypeSearchRepository.findOne(testSizeType.getId());
        assertThat(sizeTypeEs).isEqualToComparingFieldByField(testSizeType);
    }

    @Test
    @Transactional
    public void deleteSizeType() throws Exception {
        // Initialize the database
        sizeTypeService.save(sizeType);

        int databaseSizeBeforeDelete = sizeTypeRepository.findAll().size();

        // Get the sizeType
        restSizeTypeMockMvc.perform(delete("/api/size-types/{id}", sizeType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean sizeTypeExistsInEs = sizeTypeSearchRepository.exists(sizeType.getId());
        assertThat(sizeTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<SizeType> sizeTypes = sizeTypeRepository.findAll();
        assertThat(sizeTypes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSizeType() throws Exception {
        // Initialize the database
        sizeTypeService.save(sizeType);

        // Search the sizeType
        restSizeTypeMockMvc.perform(get("/api/_search/size-types?query=id:" + sizeType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sizeType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
}
