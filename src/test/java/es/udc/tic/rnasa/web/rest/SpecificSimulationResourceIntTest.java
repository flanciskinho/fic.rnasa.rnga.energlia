/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.*;
import es.udc.tic.rnasa.repository.DatasetCombinationRepository;
import es.udc.tic.rnasa.repository.SpecificSimulationRepository;
import es.udc.tic.rnasa.repository.search.SpecificSimulationSearchRepository;
import es.udc.tic.rnasa.service.SimulationStatusTypeService;
import es.udc.tic.rnasa.service.SpecificSimulationService;
import es.udc.tic.rnasa.web.rest.dto.SpecificSimulationDTO;
import es.udc.tic.rnasa.web.rest.mapper.SpecificSimulationMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SpecificSimulationResource REST controller.
 *
 * @see SpecificSimulationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SpecificSimulationResourceIntTest {

    @Inject
    private CreateEntities createEntities;

    private static final String DEFAULT_LOG_VERSION = "AAAAA";
    private static final String UPDATED_LOG_VERSION = "BBBBB";

    private static final byte[] DEFAULT_FILELOG = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILELOG = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_FILELOG_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILELOG_CONTENT_TYPE = "image/png";
    private static final String DEFAULT_NODE_VERSION = "AAAAA";
    private static final String UPDATED_NODE_VERSION = "BBBBB";

    private static final Long DEFAULT_JOBID = 1L;
    private static final Long UPDATED_JOBID = 2L;

    @Inject
    private SpecificSimulationRepository specificSimulationRepository;

    @Inject
    private SpecificSimulationMapper specificSimulationMapper;

    @Inject
    private SpecificSimulationService specificSimulationService;

    @Inject
    private SpecificSimulationSearchRepository specificSimulationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSpecificSimulationMockMvc;

    private SpecificSimulation specificSimulation;

    @Inject
    private DatasetCombinationRepository datasetCombinationRepository;

    @Inject
    private SimulationStatusTypeService simulationStatusTypeService;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SpecificSimulationResource specificSimulationResource = new SpecificSimulationResource();
        ReflectionTestUtils.setField(specificSimulationResource, "specificSimulationService", specificSimulationService);
        ReflectionTestUtils.setField(specificSimulationResource, "specificSimulationMapper", specificSimulationMapper);

        SimulationStatusTypeResource simulationStatusTypeResource = new SimulationStatusTypeResource();
        ReflectionTestUtils.setField(simulationStatusTypeResource, "simulationStatusTypeService", simulationStatusTypeService);

        this.restSpecificSimulationMockMvc = MockMvcBuilders.standaloneSetup(specificSimulationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        try {
            specificSimulationSearchRepository.deleteAll();
            specificSimulation = new SpecificSimulation();
            specificSimulation.setLogVersion(DEFAULT_LOG_VERSION);
            specificSimulation.setFilelog(DEFAULT_FILELOG);
            specificSimulation.setFilelogContentType(DEFAULT_FILELOG_CONTENT_TYPE);
            specificSimulation.setNodeVersion(DEFAULT_NODE_VERSION);
            specificSimulation.setJobid(DEFAULT_JOBID);

            SshServer sshServer = createEntities.createSshServer();
            Dataset dataset = createEntities.createDataset(10, 1, 5, null);
            List<Population> populations = new ArrayList(1);
            populations.add(createEntities.createPopulation(10, 0, 1));
            Simulation simulation = createEntities.createNuSvmSimulation("nuSvm", dataset, populations, sshServer);

            specificSimulation.setBelong(simulation);
            specificSimulation.setStatus(simulationStatusTypeService.getWAITING_FOR_QUEUE());
            specificSimulation.setUseData(datasetCombinationRepository.findAllByBelongId(dataset.getId()).get(0));

        } catch (Exception e) {}
    }


    @Test
    @Transactional
    public void getAllSpecificSimulationsByStatusId() throws Exception {
        // Initialize the database
        specificSimulationRepository.saveAndFlush(specificSimulation);


        int max = 5;

        SpecificSimulation aux;
        List<SpecificSimulation> list = new ArrayList<>(max);
        for (int cnt = 0; cnt < max; cnt++) {
            aux = new SpecificSimulation(specificSimulation);
            aux.setId(null);
            aux.setStatus(((cnt % 2 == 0) ? simulationStatusTypeService.getWAITING_ON_QUEUE(): simulationStatusTypeService.getRUNNING()) );
            list.add(aux);
        }

        specificSimulationRepository.save(list);
        specificSimulationRepository.flush();

        Long id = simulationStatusTypeService.getWAITING_ON_QUEUE().getId();
        // Get all the specificSimulations
        restSpecificSimulationMockMvc.perform(get("/api/specific-simulations-by-status/{id}", id))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].statusId").value(hasItem(id.intValue())))
            .andExpect(jsonPath("$.[*].logVersion").value(hasItem(DEFAULT_LOG_VERSION.toString())))
            .andExpect(jsonPath("$.[*].filelogContentType").value(hasItem(DEFAULT_FILELOG_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].filelog").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILELOG))))
            .andExpect(jsonPath("$.[*].nodeVersion").value(hasItem(DEFAULT_NODE_VERSION.toString())))
            .andExpect(jsonPath("$.[*].jobid").value(hasItem(DEFAULT_JOBID.intValue())))
        ;
    }

    @Test
    @Transactional
    public void createSpecificSimulation() throws Exception {
        int databaseSizeBeforeCreate = specificSimulationRepository.findAll().size();

        // Create the SpecificSimulation
        SpecificSimulationDTO specificSimulationDTO = specificSimulationMapper.specificSimulationToSpecificSimulationDTO(specificSimulation);

        restSpecificSimulationMockMvc.perform(post("/api/specific-simulations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(specificSimulationDTO)))
                .andExpect(status().isCreated());

        // Validate the SpecificSimulation in the database
        List<SpecificSimulation> specificSimulations = specificSimulationRepository.findAll();
        assertThat(specificSimulations).hasSize(databaseSizeBeforeCreate + 1);
        SpecificSimulation testSpecificSimulation = specificSimulations.get(specificSimulations.size() - 1);
        assertThat(testSpecificSimulation.getLogVersion()).isEqualTo(DEFAULT_LOG_VERSION);
        assertThat(testSpecificSimulation.getFilelog()).isEqualTo(DEFAULT_FILELOG);
        assertThat(testSpecificSimulation.getFilelogContentType()).isEqualTo(DEFAULT_FILELOG_CONTENT_TYPE);
        assertThat(testSpecificSimulation.getNodeVersion()).isEqualTo(DEFAULT_NODE_VERSION);
        assertThat(testSpecificSimulation.getJobid()).isEqualTo(DEFAULT_JOBID);

        // Validate the SpecificSimulation in ElasticSearch
        SpecificSimulation specificSimulationEs = specificSimulationSearchRepository.findOne(testSpecificSimulation.getId());
        assertThat(specificSimulationEs).isEqualToComparingFieldByField(testSpecificSimulation);
    }

    @Test
    @Transactional
    public void getAllSpecificSimulations() throws Exception {
        // Initialize the database
        specificSimulationRepository.saveAndFlush(specificSimulation);

        // Get all the specificSimulations
        restSpecificSimulationMockMvc.perform(get("/api/specific-simulations?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(specificSimulation.getId().intValue())))
                .andExpect(jsonPath("$.[*].logVersion").value(hasItem(DEFAULT_LOG_VERSION.toString())))
                .andExpect(jsonPath("$.[*].filelogContentType").value(hasItem(DEFAULT_FILELOG_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].filelog").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILELOG))))
                .andExpect(jsonPath("$.[*].nodeVersion").value(hasItem(DEFAULT_NODE_VERSION.toString())))
                .andExpect(jsonPath("$.[*].jobid").value(hasItem(DEFAULT_JOBID.intValue())));
    }

    @Test
    @Transactional
    public void getSpecificSimulation() throws Exception {
        // Initialize the database
        specificSimulationRepository.saveAndFlush(specificSimulation);

        // Get the specificSimulation
        restSpecificSimulationMockMvc.perform(get("/api/specific-simulations/{id}", specificSimulation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(specificSimulation.getId().intValue()))
            .andExpect(jsonPath("$.logVersion").value(DEFAULT_LOG_VERSION.toString()))
            .andExpect(jsonPath("$.filelogContentType").value(DEFAULT_FILELOG_CONTENT_TYPE))
            .andExpect(jsonPath("$.filelog").value(Base64Utils.encodeToString(DEFAULT_FILELOG)))
            .andExpect(jsonPath("$.nodeVersion").value(DEFAULT_NODE_VERSION.toString()))
            .andExpect(jsonPath("$.jobid").value(DEFAULT_JOBID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSpecificSimulation() throws Exception {
        // Get the specificSimulation
        restSpecificSimulationMockMvc.perform(get("/api/specific-simulations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpecificSimulation() throws Exception {
        // Initialize the database
        specificSimulationRepository.saveAndFlush(specificSimulation);
        specificSimulationSearchRepository.save(specificSimulation);
        int databaseSizeBeforeUpdate = specificSimulationRepository.findAll().size();

        // Update the specificSimulation
        SpecificSimulation updatedSpecificSimulation = new SpecificSimulation();
        updatedSpecificSimulation.setId(specificSimulation.getId());
        updatedSpecificSimulation.setLogVersion(UPDATED_LOG_VERSION);
        updatedSpecificSimulation.setFilelog(UPDATED_FILELOG);
        updatedSpecificSimulation.setFilelogContentType(UPDATED_FILELOG_CONTENT_TYPE);
        updatedSpecificSimulation.setNodeVersion(UPDATED_NODE_VERSION);
        updatedSpecificSimulation.setJobid(UPDATED_JOBID);
        SpecificSimulationDTO specificSimulationDTO = specificSimulationMapper.specificSimulationToSpecificSimulationDTO(updatedSpecificSimulation);

        restSpecificSimulationMockMvc.perform(put("/api/specific-simulations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(specificSimulationDTO)))
                .andExpect(status().isOk());

        // Validate the SpecificSimulation in the database
        List<SpecificSimulation> specificSimulations = specificSimulationRepository.findAll();
        assertThat(specificSimulations).hasSize(databaseSizeBeforeUpdate);
        SpecificSimulation testSpecificSimulation = specificSimulations.get(specificSimulations.size() - 1);
        assertThat(testSpecificSimulation.getLogVersion()).isEqualTo(UPDATED_LOG_VERSION);
        assertThat(testSpecificSimulation.getFilelog()).isEqualTo(UPDATED_FILELOG);
        assertThat(testSpecificSimulation.getFilelogContentType()).isEqualTo(UPDATED_FILELOG_CONTENT_TYPE);
        assertThat(testSpecificSimulation.getNodeVersion()).isEqualTo(UPDATED_NODE_VERSION);
        assertThat(testSpecificSimulation.getJobid()).isEqualTo(UPDATED_JOBID);

        // Validate the SpecificSimulation in ElasticSearch
        SpecificSimulation specificSimulationEs = specificSimulationSearchRepository.findOne(testSpecificSimulation.getId());
        assertThat(specificSimulationEs).isEqualToComparingFieldByField(testSpecificSimulation);
    }

    @Test
    @Transactional
    public void deleteSpecificSimulation() throws Exception {
        // Initialize the database
        specificSimulationRepository.saveAndFlush(specificSimulation);
        specificSimulationSearchRepository.save(specificSimulation);
        int databaseSizeBeforeDelete = specificSimulationRepository.findAll().size();

        // Get the specificSimulation
        restSpecificSimulationMockMvc.perform(delete("/api/specific-simulations/{id}", specificSimulation.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean specificSimulationExistsInEs = specificSimulationSearchRepository.exists(specificSimulation.getId());
        assertThat(specificSimulationExistsInEs).isFalse();

        // Validate the database is empty
        List<SpecificSimulation> specificSimulations = specificSimulationRepository.findAll();
        assertThat(specificSimulations).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSpecificSimulation() throws Exception {
        // Initialize the database
        specificSimulationRepository.saveAndFlush(specificSimulation);
        specificSimulationSearchRepository.save(specificSimulation);

        // Search the specificSimulation
        restSpecificSimulationMockMvc.perform(get("/api/_search/specific-simulations?query=id:" + specificSimulation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specificSimulation.getId().intValue())))
            .andExpect(jsonPath("$.[*].logVersion").value(hasItem(DEFAULT_LOG_VERSION.toString())))
            .andExpect(jsonPath("$.[*].filelogContentType").value(hasItem(DEFAULT_FILELOG_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].filelog").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILELOG))))
            .andExpect(jsonPath("$.[*].nodeVersion").value(hasItem(DEFAULT_NODE_VERSION.toString())))
            .andExpect(jsonPath("$.[*].jobid").value(hasItem(DEFAULT_JOBID.intValue())));
    }
}
