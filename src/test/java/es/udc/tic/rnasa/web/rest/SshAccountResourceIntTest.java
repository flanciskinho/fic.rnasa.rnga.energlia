/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.SshAccount;
import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.ArchTypeRepository;
import es.udc.tic.rnasa.repository.SshAccountRepository;
import es.udc.tic.rnasa.repository.SshServerRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.repository.search.SshAccountSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.security.random.IntegerRandom;
import es.udc.tic.rnasa.service.SshAccountService;
import es.udc.tic.rnasa.web.rest.dto.SshAccountDTO;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.SshAccountMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SshAccountResource REST controller.
 *
 * @see SshAccountResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SshAccountResourceIntTest {

    private static final String  DEFAULT_DNSNAME = "AAAAA";
    private static final Integer DEFAULT_PORT = 22;
    private static final String  DEFAULT_FINGERPRINT = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final Integer DEFAULT_MAX_JOB = 1;
    private static final Integer DEFAULT_MAX_PROC = 1;
    private static final Long DEFAULT_MAX_TIME = 1l;
    private static final Long DEFAULT_MAX_SPACE = 1l;
    private static final Long DEFAULT_MAX_MEMORY = 1l;
    private static final Integer DEFAULT_MAX_CONNECTION = 0;
    private static final Integer DEFAULT_NUM_CONNECTION = 0;

    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_USERNAME = "BBBBB";
    private static final String DEFAULT_KEY = "AAAAA";
    private static final String UPDATED_KEY = "BBBBB";

    private static final Boolean DEFAULT_ACTIVATED = false;
    private static final Boolean UPDATED_ACTIVATED = true;

    @Inject
    private SshAccountRepository sshAccountRepository;

    @Inject
    private ArchTypeRepository archTypeRepository;

    @Inject
    private SshAccountMapper sshAccountMapper;

    @Inject
    private SshAccountService sshAccountService;

    @Inject
    private SshAccountSearchRepository sshAccountSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSshAccountMockMvc;

    private SshAccount sshAccount;
    private SshServer sshServer;
    private User owner;
    private UserDTO ownerDTO;

    @Inject
    private WebApplicationContext context;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;

    @Inject
    private SshServerRepository sshServerRepository;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SshAccountResource sshAccountResource = new SshAccountResource();
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountService", sshAccountService);
        ReflectionTestUtils.setField(sshAccountResource, "sshAccountMapper", sshAccountMapper);


        ReflectionTestUtils.setField(sshAccountService, "sshAccountRepository", sshAccountRepository);
        ReflectionTestUtils.setField(sshAccountService, "sshServerRepository", sshServerRepository);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);

        /*
        this.restSshAccountMockMvc = MockMvcBuilders.standaloneSetup(sshAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
        */

        this.restSshAccountMockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Before
    public void initTest() {
        archTypeRepository.deleteAll();
        sshAccountRepository.deleteAll();
        sshServerRepository.deleteAll();


        ownerDTO = new UserDTO(
            "joe",                  // login
            "123456789012345678901234567890123456789012345678901234567890",
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin("joe").isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }

        sshServer = new SshServer();
        sshServer.setDnsname(DEFAULT_DNSNAME);
        sshServer.setPort(DEFAULT_PORT);
        sshServer.setFingerprint(DEFAULT_FINGERPRINT);
        sshServer.setMaxJob(DEFAULT_MAX_JOB);
        sshServer.setMaxProc(DEFAULT_MAX_PROC);
        sshServer.setMaxTime(DEFAULT_MAX_TIME);
        sshServer.setMaxSpace(DEFAULT_MAX_SPACE);
        sshServer.setMaxMemory(DEFAULT_MAX_MEMORY);
        sshServer.setActivated(DEFAULT_ACTIVATED);
        sshServer.setMaxConnection(DEFAULT_MAX_CONNECTION);
        sshServer.setNumConnection(DEFAULT_NUM_CONNECTION);

        sshServerRepository.save(sshServer);

        sshAccount = new SshAccount();
        sshAccount.setUsername(DEFAULT_USERNAME);
        sshAccount.setKey(DEFAULT_KEY);
        sshAccount.setActivated(DEFAULT_ACTIVATED);
        sshAccount.setBelong(sshServer);
        sshAccount.setSalt(IntegerRandom.getSecureRandomInteger(50));
    }


    @Test
    @Transactional
    public void createSshAccountUnauthorized() throws Exception {
        int databaseSizeBeforeCreate = sshAccountRepository.findAll().size();

        // Create the SshAccount
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);

        restSshAccountMockMvc.perform(post("/api/ssh-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().is4xxClientError());

        // Validate the SshAccount in the database
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createSshAccountNonAdmin() throws Exception {
        int databaseSizeBeforeCreate = sshAccountRepository.findAll().size();

        // Create the SshAccount
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);

        restSshAccountMockMvc.perform(post("/api/ssh-accounts")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().isForbidden());

        // Validate the SshAccount in the database
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createSshAccount() throws Exception {


        int databaseSizeBeforeCreate = sshAccountRepository.findAll().size();

        // Create the SshAccount
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);

        restSshAccountMockMvc.perform(post("/api/ssh-accounts")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().isCreated());

        // Validate the SshAccount in the database
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeCreate + 1);
        SshAccount testSshAccount = sshAccounts.get(sshAccounts.size() - 1);
        assertThat(testSshAccount.getUsername()).isEqualTo(DEFAULT_USERNAME);
        //assertThat(sshAccountService.getDecryptKey(testSshAccount)).isEqualTo(DEFAULT_KEY);
        assertThat(testSshAccount.isActivated()).isEqualTo(DEFAULT_ACTIVATED);

        // Validate the SshAccount in ElasticSearch
        SshAccount sshAccountEs = sshAccountSearchRepository.findOne(testSshAccount.getId());
        assertThat(sshAccountEs).isEqualToComparingFieldByField(testSshAccount);
    }


    @Test
    @Transactional
    public void getAllSshAccountsUnauthorized() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);

        // Get all the sshAccounts
        restSshAccountMockMvc.perform(get("/api/ssh-accounts?sort=id,desc"))
            .andExpect(status().is4xxClientError());
    }


    @Test
    @Transactional
    public void getAllSshAccountsNoAdmin() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);

        // Get all the sshAccounts
        restSshAccountMockMvc.perform(get("/api/ssh-accounts?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isForbidden());
    }


    @Test
    @Transactional
    public void getAllSshAccounts() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);

        // Get all the sshAccounts
        restSshAccountMockMvc.perform(get("/api/ssh-accounts?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sshAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY.toString())))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())));
    }

    @Test
    @Transactional
    public void getSshAccountUnauthorized() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);

        // Get the sshAccount
        restSshAccountMockMvc.perform(get("/api/ssh-accounts/{id}", sshAccount.getId()))
            .andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void getSshAccountNoAdmin() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);

        // Get the sshAccount
        restSshAccountMockMvc.perform(get("/api/ssh-accounts/{id}", sshAccount.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void getSshAccount() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);

        // Get the sshAccount
        restSshAccountMockMvc.perform(get("/api/ssh-accounts/{id}", sshAccount.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(sshAccount.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.key").value(DEFAULT_KEY.toString()))
            .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSshAccount() throws Exception {
        // Get the sshAccount
        restSshAccountMockMvc.perform(get("/api/ssh-accounts/{id}", Long.MAX_VALUE)
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSshAccountUnauthorized() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);
        sshAccountSearchRepository.save(sshAccount);
        int databaseSizeBeforeUpdate = sshAccountRepository.findAll().size();

        // Update the sshAccount
        SshAccount updatedSshAccount = new SshAccount();
        updatedSshAccount.setId(sshAccount.getId());
        updatedSshAccount.setUsername(UPDATED_USERNAME);
        updatedSshAccount.setKey(UPDATED_KEY);
        updatedSshAccount.setActivated(UPDATED_ACTIVATED);
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(updatedSshAccount);

        restSshAccountMockMvc.perform(put("/api/ssh-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().is4xxClientError());

        // Validate the SshAccount in the database
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateSshAccountNoAdmin() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);
        sshAccountSearchRepository.save(sshAccount);
        int databaseSizeBeforeUpdate = sshAccountRepository.findAll().size();

        // Update the sshAccount
        SshAccount updatedSshAccount = new SshAccount();
        updatedSshAccount.setId(sshAccount.getId());
        updatedSshAccount.setUsername(UPDATED_USERNAME);
        updatedSshAccount.setKey(UPDATED_KEY);
        updatedSshAccount.setActivated(UPDATED_ACTIVATED);
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(updatedSshAccount);

        restSshAccountMockMvc.perform(put("/api/ssh-accounts")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().isForbidden());

        // Validate the SshAccount in the database
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateSshAccount() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);
        sshAccountSearchRepository.save(sshAccount);
        int databaseSizeBeforeUpdate = sshAccountRepository.findAll().size();

        // Update the sshAccount
        SshAccount updatedSshAccount = new SshAccount();
        updatedSshAccount.setId(sshAccount.getId());
        updatedSshAccount.setUsername(UPDATED_USERNAME);
        updatedSshAccount.setKey(UPDATED_KEY);
        updatedSshAccount.setActivated(UPDATED_ACTIVATED);
        updatedSshAccount.setBelong(sshServer);
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(updatedSshAccount);

        restSshAccountMockMvc.perform(put("/api/ssh-accounts")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().isOk());

        // Validate the SshAccount in the database
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeUpdate);
        SshAccount testSshAccount = sshAccounts.get(sshAccounts.size() - 1);
        assertThat(testSshAccount.getUsername()).isEqualTo(UPDATED_USERNAME);
        //assertThat(sshAccountService.getDecryptKey(testSshAccount)).isEqualTo(UPDATED_KEY);
        assertThat(testSshAccount.isActivated()).isEqualTo(UPDATED_ACTIVATED);

        // Validate the SshAccount in ElasticSearch
        //SshAccount sshAccountEs = sshAccountSearchRepository.findOne(testSshAccount.getId());
        //assertThat(sshAccountEs).isEqualToComparingFieldByField(testSshAccount);
    }

    @Test
    @Transactional
    public void deleteSshAccountUnauthorized() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);
        sshAccountSearchRepository.save(sshAccount);
        int databaseSizeBeforeDelete = sshAccountRepository.findAll().size();

        // Get the sshAccount
        restSshAccountMockMvc.perform(delete("/api/ssh-accounts/{id}", sshAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().is4xxClientError());

        // Validate ElasticSearch is empty
        boolean sshAccountExistsInEs = sshAccountSearchRepository.exists(sshAccount.getId());
        assertThat(sshAccountExistsInEs).isTrue();

        // Validate the database is empty
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteSshAccountNoAdmin() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);
        sshAccountSearchRepository.save(sshAccount);
        int databaseSizeBeforeDelete = sshAccountRepository.findAll().size();

        // Get the sshAccount
        restSshAccountMockMvc.perform(delete("/api/ssh-accounts/{id}", sshAccount.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isForbidden());

        // Validate ElasticSearch is empty
        boolean sshAccountExistsInEs = sshAccountSearchRepository.exists(sshAccount.getId());
        assertThat(sshAccountExistsInEs).isTrue();

        // Validate the database is empty
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteSshAccount() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);
        sshAccountSearchRepository.save(sshAccount);
        int databaseSizeBeforeDelete = sshAccountRepository.findAll().size();

        // Get the sshAccount
        restSshAccountMockMvc.perform(delete("/api/ssh-accounts/{id}", sshAccount.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean sshAccountExistsInEs = sshAccountSearchRepository.exists(sshAccount.getId());
        assertThat(sshAccountExistsInEs).isFalse();

        // Validate the database is empty
        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeDelete - 1);
    }

/*
    @Test
    @Transactional
    public void searchSshAccount() throws Exception {
        // Initialize the database
        sshAccountRepository.saveAndFlush(sshAccount);
        sshAccountSearchRepository.save(sshAccount);

        // Search the sshAccount
        restSshAccountMockMvc.perform(get("/api/_search/ssh-accounts?query=id:" + sshAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sshAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY.toString())))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())));
    }
*/

    @Test
    @Transactional
    public void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshAccountRepository.findAll().size();
        // set the field null
        sshAccount.setUsername(null);

        // Create the SshAccount, which fails.
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);

        restSshAccountMockMvc.perform(post("/api/ssh-accounts")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().isBadRequest());

        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkKeyIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshAccountRepository.findAll().size();
        // set the field null
        sshAccount.setKey(null);

        // Create the SshAccount, which fails.
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);

        restSshAccountMockMvc.perform(post("/api/ssh-accounts")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().isBadRequest());

        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActivatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshAccountRepository.findAll().size();
        // set the field null
        sshAccount.setActivated(null);

        // Create the SshAccount, which fails.
        SshAccountDTO sshAccountDTO = sshAccountMapper.sshAccountToSshAccountDTO(sshAccount);

        restSshAccountMockMvc.perform(post("/api/ssh-accounts")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshAccountDTO)))
            .andExpect(status().isBadRequest());

        List<SshAccount> sshAccounts = sshAccountRepository.findAll();
        assertThat(sshAccounts).hasSize(databaseSizeBeforeTest);
    }
}
