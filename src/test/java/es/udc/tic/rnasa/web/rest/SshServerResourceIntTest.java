/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.SshServer;
import es.udc.tic.rnasa.domain.User;
import es.udc.tic.rnasa.repository.SshServerRepository;
import es.udc.tic.rnasa.repository.UserRepository;
import es.udc.tic.rnasa.repository.search.SshServerSearchRepository;
import es.udc.tic.rnasa.security.AuthoritiesConstants;
import es.udc.tic.rnasa.service.SshAccountServiceIntTest;
import es.udc.tic.rnasa.service.SshServerService;
import es.udc.tic.rnasa.web.rest.dto.SshServerDTO;
import es.udc.tic.rnasa.web.rest.dto.UserDTO;
import es.udc.tic.rnasa.web.rest.mapper.SshServerMapper;
import es.udc.tic.rnasa.web.rest.mapper.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SshServerResource REST controller.
 *
 * @see SshServerResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class SshServerResourceIntTest {

    private final Logger log = LoggerFactory.getLogger(SshServerResourceIntTest.class);

    private static final String DEFAULT_DNSNAME = "AAAAA";
    private static final String UPDATED_DNSNAME = "BBBBB";

    private static final Integer DEFAULT_PORT = 22;
    private static final Integer UPDATED_PORT = 222;

    private static final String DEFAULT_FINGERPRINT = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_FINGERPRINT = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final Integer DEFAULT_MAX_JOB = 1;
    private static final Integer UPDATED_MAX_JOB = 2;

    private static final Integer DEFAULT_MAX_PROC = 1;
    private static final Integer UPDATED_MAX_PROC = 2;

    private static final Long DEFAULT_MAX_TIME = 1l;
    private static final Long UPDATED_MAX_TIME = 2l;

    private static final Long DEFAULT_MAX_SPACE = 1l;
    private static final Long UPDATED_MAX_SPACE = 2l;

    private static final Long DEFAULT_MAX_MEMORY = 1l;
    private static final Long UPDATED_MAX_MEMORY = 2l;

    private static final Boolean DEFAULT_ACTIVATED = false;
    private static final Boolean UPDATED_ACTIVATED = true;

    private static final Integer DEFAULT_MAX_CONNECTION = 1;
    private static final Integer UPDATED_MAX_CONNECTION = 2;

    @Inject
    private SshServerRepository sshServerRepository;

    @Inject
    private SshServerMapper sshServerMapper;

    @Inject
    private SshServerService sshServerService;

    @Inject
    private SshServerSearchRepository sshServerSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSshServerMockMvc;

    private SshServer sshServer;
    private User owner;
    private UserDTO ownerDTO;

    @Inject
    private WebApplicationContext context;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserMapper userMapper;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SshServerResource sshServerResource = new SshServerResource();
        ReflectionTestUtils.setField(sshServerResource, "sshServerService", sshServerService);
        ReflectionTestUtils.setField(sshServerResource, "sshServerMapper", sshServerMapper);

        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userMapper", userMapper);
/*
        this.restSshServerMockMvc = MockMvcBuilders.standaloneSetup(sshServerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
*/
        this.restSshServerMockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Before
    public void initTest() {
        //sshServerSearchRepository.deleteAll();

        ownerDTO = new UserDTO(
            "joe",                  // login
            "123456789012345678901234567890123456789012345678901234567890",
            "Joe",                  // firstName
            "Shmoe",                // lastName
            "joe@example.com",      // e-mail
            true,                   // activated
            "en",                   // langKey
            new HashSet<>(Arrays.asList(AuthoritiesConstants.USER))
        );

        if (!userRepository.findOneByLogin("joe").isPresent()) {
            owner = userMapper.userDTOToUser(ownerDTO);
            userRepository.saveAndFlush(owner);
        } else {
            owner = userRepository.findOneByLogin("joe").get();
        }

        sshServer = new SshServer();
        sshServer.setDnsname(DEFAULT_DNSNAME);
        sshServer.setFingerprint(DEFAULT_FINGERPRINT);
        sshServer.setMaxJob(DEFAULT_MAX_JOB);
        sshServer.setMaxProc(DEFAULT_MAX_PROC);
        sshServer.setMaxTime(DEFAULT_MAX_TIME);
        sshServer.setMaxSpace(DEFAULT_MAX_SPACE);
        sshServer.setMaxMemory(DEFAULT_MAX_MEMORY);
        sshServer.setActivated(DEFAULT_ACTIVATED);
        sshServer.setMaxConnection(DEFAULT_MAX_CONNECTION);
        sshServer.setPort(DEFAULT_PORT);
        sshServer.setNumConnection(0);
    }

    ///
    /// custom resources bellow this point
    ///

    @Test
    @Transactional
    public void findAllActivatedSshServer() throws Exception {
        int MAX = 5;

        int numActivated = (int) Math.ceil(MAX/2.0);

        String template = "node%d.example.org";

        List<SshServer> list = new ArrayList<>();
        for (int cnt = 0; cnt< MAX; cnt++) {
            sshServer = new SshServer();
            sshServer.setDnsname(String.format(template, cnt));
            sshServer.setActivated(cnt % 2 == 0);

            sshServer.setFingerprint(DEFAULT_FINGERPRINT);
            sshServer.setMaxJob(DEFAULT_MAX_JOB);
            sshServer.setMaxTime(DEFAULT_MAX_TIME);
            sshServer.setMaxProc(DEFAULT_MAX_PROC);
            sshServer.setMaxSpace(DEFAULT_MAX_SPACE);
            sshServer.setMaxMemory(DEFAULT_MAX_MEMORY);
            sshServer.setMaxConnection(DEFAULT_MAX_CONNECTION);
            sshServer.setPort(DEFAULT_PORT);
            sshServer.setNumConnection(0);

            list.add(sshServer);
        }

        sshServerRepository.save(list);
        sshServerRepository.flush();

        ResultActions resultActions = restSshServerMockMvc.perform(get("/api/ssh-servers-activated?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            //.andExpect(jsonPath("$.[*].activated").value( hasSize(numActivated)))
            ;


        log.debug("\n\n\n\tResponse: {}\n\n\n", resultActions.andReturn().getResponse().getContentAsString());

        list.stream()
            .filter(s -> s.isActivated())
            .forEach(s -> {
                try {
                    resultActions.andExpect(jsonPath("$.[*].port").value(hasItem(s.getPort())))
                        .andExpect(jsonPath("$.[*].dnsname").value(hasItem(s.getDnsname())))
                        .andExpect(jsonPath("$.[*].fingerprint").value(hasItem(s.getFingerprint())))
                        .andExpect(jsonPath("$.[*].maxJob").value(hasItem(s.getMaxJob())))
                        .andExpect(jsonPath("$.[*].maxProc").value(hasItem(s.getMaxProc())))
                        .andExpect(jsonPath("$.[*].maxTime").value(hasItem(s.getMaxTime().intValue())))
                        .andExpect(jsonPath("$.[*].maxSpace").value(hasItem(s.getMaxSpace().intValue())))
                        .andExpect(jsonPath("$.[*].maxMemory").value(hasItem(s.getMaxMemory().intValue())))
                        .andExpect(jsonPath("$.[*].activated").value(hasItem(true)))
                        .andExpect(jsonPath("$.[*].maxConnection").value(hasItem(s.getMaxConnection())));
                } catch (Exception e) {fail("");}
            }

            );
    }

    ///
    /// end custom api rest
    ///

    @Test
    @Transactional
    public void createSshServerUnauthorized() throws Exception {
        int databaseSizeBeforeCreate = sshServerRepository.findAll().size();

        // Create the SshServer
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().is4xxClientError());

        // Validate the SshServer in the database
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createSshServerNoAdmin() throws Exception {
        int databaseSizeBeforeCreate = sshServerRepository.findAll().size();

        // Create the SshServer
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isForbidden());

        // Validate the SshServer in the database
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void createSshServer() throws Exception {
        int databaseSizeBeforeCreate = sshServerRepository.findAll().size();

        // Create the SshServer
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isCreated());

        // Validate the SshServer in the database
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeCreate + 1);
        SshServer testSshServer = sshServers.get(sshServers.size() - 1);
        assertThat(testSshServer.getDnsname()).isEqualTo(DEFAULT_DNSNAME);
        assertThat(testSshServer.getPort()).isEqualTo(DEFAULT_PORT);
        assertThat(testSshServer.getFingerprint()).isEqualTo(DEFAULT_FINGERPRINT);
        assertThat(testSshServer.getMaxJob()).isEqualTo(DEFAULT_MAX_JOB);
        assertThat(testSshServer.getMaxProc()).isEqualTo(DEFAULT_MAX_PROC);
        assertThat(testSshServer.getMaxTime()).isEqualTo(DEFAULT_MAX_TIME);
        assertThat(testSshServer.getMaxSpace()).isEqualTo(DEFAULT_MAX_SPACE);
        assertThat(testSshServer.getMaxMemory()).isEqualTo(DEFAULT_MAX_MEMORY);
        assertThat(testSshServer.isActivated()).isEqualTo(DEFAULT_ACTIVATED);
        assertThat(testSshServer.getMaxConnection()).isEqualTo(DEFAULT_MAX_CONNECTION);
        assertThat(testSshServer.getNumConnection()).isEqualTo(0);

        // Validate the SshServer in ElasticSearch
        //SshServer sshServerEs = sshServerSearchRepository.findOne(testSshServer.getId());
        //assertThat(sshServerEs).isEqualToComparingFieldByField(testSshServer);
    }

    @Test
    @Transactional
    public void getAllSshServersUnauthorized() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);

        // Get all the sshServers
        restSshServerMockMvc.perform(get("/api/ssh-servers?sort=id,desc"))
            .andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void getAllSshServersNoAdmin() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);

        // Get all the sshServers
        restSshServerMockMvc.perform(get("/api/ssh-servers?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sshServer.getId().intValue())))
            .andExpect(jsonPath("$.[*].dnsname").value(hasItem(DEFAULT_DNSNAME.toString())))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT.intValue())))
            .andExpect(jsonPath("$.[*].fingerprint").value(hasItem(DEFAULT_FINGERPRINT.toString())))
            .andExpect(jsonPath("$.[*].maxJob").value(hasItem(DEFAULT_MAX_JOB)))
            .andExpect(jsonPath("$.[*].maxProc").value(hasItem(DEFAULT_MAX_PROC)))
            .andExpect(jsonPath("$.[*].maxTime").value(hasItem(DEFAULT_MAX_TIME.intValue())))
            .andExpect(jsonPath("$.[*].maxSpace").value(hasItem(DEFAULT_MAX_SPACE.intValue())))
            .andExpect(jsonPath("$.[*].maxMemory").value(hasItem(DEFAULT_MAX_MEMORY.intValue())))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())))
            .andExpect(jsonPath("$.[*].maxConnection").value(hasItem(DEFAULT_MAX_CONNECTION)));
    }

    @Test
    @Transactional
    public void getAllSshServers() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);

        // Get all the sshServers
        restSshServerMockMvc.perform(get("/api/ssh-servers?sort=id,desc")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sshServer.getId().intValue())))
            .andExpect(jsonPath("$.[*].dnsname").value(hasItem(DEFAULT_DNSNAME.toString())))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT.intValue())))
            .andExpect(jsonPath("$.[*].fingerprint").value(hasItem(DEFAULT_FINGERPRINT.toString())))
            .andExpect(jsonPath("$.[*].maxJob").value(hasItem(DEFAULT_MAX_JOB)))
            .andExpect(jsonPath("$.[*].maxProc").value(hasItem(DEFAULT_MAX_PROC)))
            .andExpect(jsonPath("$.[*].maxTime").value(hasItem(DEFAULT_MAX_TIME.intValue())))
            .andExpect(jsonPath("$.[*].maxSpace").value(hasItem(DEFAULT_MAX_SPACE.intValue())))
            .andExpect(jsonPath("$.[*].maxMemory").value(hasItem(DEFAULT_MAX_MEMORY.intValue())))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())))
            .andExpect(jsonPath("$.[*].maxConnection").value(hasItem(DEFAULT_MAX_CONNECTION)));
    }

    @Test
    @Transactional
    public void getSshServerUnauthorized() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);

        // Get the sshServer
        restSshServerMockMvc.perform(get("/api/ssh-servers/{id}", sshServer.getId()))
            .andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void getSshServerNoAdmin() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);

        // Get the sshServer
        restSshServerMockMvc.perform(get("/api/ssh-servers/{id}", sshServer.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(sshServer.getId().intValue()))
            .andExpect(jsonPath("$.dnsname").value(DEFAULT_DNSNAME.toString()))
            .andExpect(jsonPath("$.port").value(DEFAULT_PORT.intValue()))
            .andExpect(jsonPath("$.fingerprint").value(DEFAULT_FINGERPRINT.toString()))
            .andExpect(jsonPath("$.maxJob").value(DEFAULT_MAX_JOB))
            .andExpect(jsonPath("$.maxProc").value(DEFAULT_MAX_PROC))
            .andExpect(jsonPath("$.maxTime").value(DEFAULT_MAX_TIME.intValue()))
            .andExpect(jsonPath("$.maxSpace").value(DEFAULT_MAX_SPACE.intValue()))
            .andExpect(jsonPath("$.maxMemory").value(DEFAULT_MAX_MEMORY.intValue()))
            .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()))
            .andExpect(jsonPath("$.maxConnection").value(DEFAULT_MAX_CONNECTION));
    }

    @Test
    @Transactional
    public void getSshServer() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);

        // Get the sshServer
        restSshServerMockMvc.perform(get("/api/ssh-servers/{id}", sshServer.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(sshServer.getId().intValue()))
            .andExpect(jsonPath("$.dnsname").value(DEFAULT_DNSNAME.toString()))
            .andExpect(jsonPath("$.port").value(DEFAULT_PORT.intValue()))
            .andExpect(jsonPath("$.fingerprint").value(DEFAULT_FINGERPRINT.toString()))
            .andExpect(jsonPath("$.maxJob").value(DEFAULT_MAX_JOB))
            .andExpect(jsonPath("$.maxProc").value(DEFAULT_MAX_PROC))
            .andExpect(jsonPath("$.maxTime").value(DEFAULT_MAX_TIME.intValue()))
            .andExpect(jsonPath("$.maxSpace").value(DEFAULT_MAX_SPACE.intValue()))
            .andExpect(jsonPath("$.maxMemory").value(DEFAULT_MAX_MEMORY.intValue()))
            .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()))
            .andExpect(jsonPath("$.maxConnection").value(DEFAULT_MAX_CONNECTION));
    }

    @Test
    @Transactional
    public void getNonExistingSshServer() throws Exception {
        // Get the sshServer
        restSshServerMockMvc.perform(get("/api/ssh-servers/{id}", Long.MAX_VALUE)
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN))))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSshServerUnauthorized() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);
        sshServerSearchRepository.save(sshServer);
        int databaseSizeBeforeUpdate = sshServerRepository.findAll().size();

        // Update the sshServer
        SshServer updatedSshServer = new SshServer();
        updatedSshServer.setId(sshServer.getId());
        updatedSshServer.setDnsname(UPDATED_DNSNAME);
        updatedSshServer.setPort(UPDATED_PORT);
        updatedSshServer.setFingerprint(UPDATED_FINGERPRINT);
        updatedSshServer.setMaxJob(UPDATED_MAX_JOB);
        updatedSshServer.setMaxProc(UPDATED_MAX_PROC);
        updatedSshServer.setMaxTime(UPDATED_MAX_TIME);
        updatedSshServer.setMaxSpace(UPDATED_MAX_SPACE);
        updatedSshServer.setMaxMemory(UPDATED_MAX_MEMORY);
        updatedSshServer.setActivated(UPDATED_ACTIVATED);
        updatedSshServer.setMaxConnection(UPDATED_MAX_CONNECTION);
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(updatedSshServer);

        restSshServerMockMvc.perform(put("/api/ssh-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().is4xxClientError());

        // Validate the SshServer in the database
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateSshServerNoAdmin() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);
        sshServerSearchRepository.save(sshServer);
        int databaseSizeBeforeUpdate = sshServerRepository.findAll().size();

        // Update the sshServer
        SshServer updatedSshServer = new SshServer();
        updatedSshServer.setId(sshServer.getId());
        updatedSshServer.setDnsname(UPDATED_DNSNAME);
        updatedSshServer.setPort(UPDATED_PORT);
        updatedSshServer.setFingerprint(UPDATED_FINGERPRINT);
        updatedSshServer.setMaxJob(UPDATED_MAX_JOB);
        updatedSshServer.setMaxProc(UPDATED_MAX_PROC);
        updatedSshServer.setMaxTime(UPDATED_MAX_TIME);
        updatedSshServer.setMaxSpace(UPDATED_MAX_SPACE);
        updatedSshServer.setMaxMemory(UPDATED_MAX_MEMORY);
        updatedSshServer.setActivated(UPDATED_ACTIVATED);
        updatedSshServer.setMaxConnection(UPDATED_MAX_CONNECTION);
        updatedSshServer.setNumConnection(sshServer.getNumConnection());
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(updatedSshServer);

        restSshServerMockMvc.perform(put("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isForbidden());

        // Validate the SshServer in the database
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void updateSshServer() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);
        sshServerSearchRepository.save(sshServer);
        int databaseSizeBeforeUpdate = sshServerRepository.findAll().size();

        // Update the sshServer
        SshServer updatedSshServer = new SshServer();
        updatedSshServer.setId(sshServer.getId());
        updatedSshServer.setDnsname(UPDATED_DNSNAME);
        updatedSshServer.setPort(UPDATED_PORT);
        updatedSshServer.setFingerprint(UPDATED_FINGERPRINT);
        updatedSshServer.setMaxJob(UPDATED_MAX_JOB);
        updatedSshServer.setMaxProc(UPDATED_MAX_PROC);
        updatedSshServer.setMaxTime(UPDATED_MAX_TIME);
        updatedSshServer.setMaxSpace(UPDATED_MAX_SPACE);
        updatedSshServer.setMaxMemory(UPDATED_MAX_MEMORY);
        updatedSshServer.setActivated(UPDATED_ACTIVATED);
        updatedSshServer.setMaxConnection(UPDATED_MAX_CONNECTION);
        updatedSshServer.setNumConnection(sshServer.getNumConnection());
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(updatedSshServer);

        restSshServerMockMvc.perform(put("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isOk());

        // Validate the SshServer in the database
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeUpdate);
        SshServer testSshServer = sshServers.get(sshServers.size() - 1);
        assertThat(testSshServer.getDnsname()).isEqualTo(UPDATED_DNSNAME);
        assertThat(testSshServer.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testSshServer.getFingerprint()).isEqualTo(UPDATED_FINGERPRINT);
        assertThat(testSshServer.getMaxJob()).isEqualTo(UPDATED_MAX_JOB);
        assertThat(testSshServer.getMaxProc()).isEqualTo(UPDATED_MAX_PROC);
        assertThat(testSshServer.getMaxTime()).isEqualTo(UPDATED_MAX_TIME);
        assertThat(testSshServer.getMaxSpace()).isEqualTo(UPDATED_MAX_SPACE);
        assertThat(testSshServer.getMaxMemory()).isEqualTo(UPDATED_MAX_MEMORY);
        assertThat(testSshServer.isActivated()).isEqualTo(UPDATED_ACTIVATED);
        assertThat(testSshServer.getMaxConnection()).isEqualTo(UPDATED_MAX_CONNECTION);
        assertThat(testSshServer.getNumConnection()).isEqualTo(0);

        // Validate the SshServer in ElasticSearch
        //SshServer sshServerEs = sshServerSearchRepository.findOne(testSshServer.getId());
        //assertThat(sshServerEs).isEqualToComparingFieldByField(testSshServer);
    }

    @Test
    @Transactional
    public void deleteSshServerUnauthenticated() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);
        sshServerSearchRepository.save(sshServer);
        int databaseSizeBeforeDelete = sshServerRepository.findAll().size();

        // Get the sshServer
        restSshServerMockMvc.perform(delete("/api/ssh-servers/{id}", sshServer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().is4xxClientError());

        // Validate ElasticSearch is empty
        boolean sshServerExistsInEs = sshServerSearchRepository.exists(sshServer.getId());
        assertThat(sshServerExistsInEs).isTrue();

        // Validate the database is empty
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteSshServerNoAdmin() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);
        sshServerSearchRepository.save(sshServer);
        int databaseSizeBeforeDelete = sshServerRepository.findAll().size();

        // Get the sshServer
        restSshServerMockMvc.perform(delete("/api/ssh-servers/{id}", sshServer.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.USER)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isForbidden());

        // Validate ElasticSearch is empty
        boolean sshServerExistsInEs = sshServerSearchRepository.exists(sshServer.getId());
        assertThat(sshServerExistsInEs).isTrue();

        // Validate the database is empty
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeDelete);
    }

    @Test
    @Transactional
    public void deleteSshServer() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);
        sshServerSearchRepository.save(sshServer);
        int databaseSizeBeforeDelete = sshServerRepository.findAll().size();

        // Get the sshServer
        restSshServerMockMvc.perform(delete("/api/ssh-servers/{id}", sshServer.getId())
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean sshServerExistsInEs = sshServerSearchRepository.exists(sshServer.getId());
        assertThat(sshServerExistsInEs).isFalse();

        // Validate the database is empty
        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeDelete - 1);
    }

/*
    @Test
    @Transactional
    public void searchSshServer() throws Exception {
        // Initialize the database
        sshServerRepository.saveAndFlush(sshServer);
        sshServerSearchRepository.save(sshServer);

        // Search the sshServer
        restSshServerMockMvc.perform(get("/api/_search/ssh-servers?query=id:" + sshServer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sshServer.getId().intValue())))
            .andExpect(jsonPath("$.[*].dnsname").value(hasItem(DEFAULT_DNSNAME.toString())))
            .andExpect(jsonPath("$.[*].fingerprint").value(hasItem(DEFAULT_FINGERPRINT.toString())))
            .andExpect(jsonPath("$.[*].maxJob").value(hasItem(DEFAULT_MAX_JOB)))
            .andExpect(jsonPath("$.[*].maxFile").value(hasItem(DEFAULT_MAX_FILE)))
            .andExpect(jsonPath("$.[*].maxSpace").value(hasItem(DEFAULT_MAX_SPACE)))
            .andExpect(jsonPath("$.[*].maxMemory").value(hasItem(DEFAULT_MAX_MEMORY)))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())));
    }
*/

    @Test
    @Transactional
    public void checkDnsnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setDnsname(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFingerprintIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setFingerprint(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxJobIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setMaxJob(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setMaxTime(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxProcIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setMaxProc(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxSpaceIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setMaxSpace(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxMemoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setMaxMemory(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActivatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = sshServerRepository.findAll().size();
        // set the field null
        sshServer.setActivated(null);

        // Create the SshServer, which fails.
        SshServerDTO sshServerDTO = sshServerMapper.sshServerToSshServerDTO(sshServer);

        restSshServerMockMvc.perform(post("/api/ssh-servers")
            .with(user(owner.getLogin()).authorities(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sshServerDTO)))
            .andExpect(status().isBadRequest());

        List<SshServer> sshServers = sshServerRepository.findAll();
        assertThat(sshServers).hasSize(databaseSizeBeforeTest);
    }
}
