/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest;

import es.udc.tic.rnasa.EnergliaApp;
import es.udc.tic.rnasa.domain.TechniqueParam;
import es.udc.tic.rnasa.repository.TechniqueParamRepository;
import es.udc.tic.rnasa.service.TechniqueParamService;
import es.udc.tic.rnasa.repository.search.TechniqueParamSearchRepository;
import es.udc.tic.rnasa.web.rest.dto.TechniqueParamDTO;
import es.udc.tic.rnasa.web.rest.mapper.TechniqueParamMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TechniqueParamResource REST controller.
 *
 * @see TechniqueParamResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EnergliaApp.class)
@WebAppConfiguration
@IntegrationTest
public class TechniqueParamResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_VALUE = "AAAAA";
    private static final String UPDATED_VALUE = "BBBBB";

    @Inject
    private TechniqueParamRepository techniqueParamRepository;

    @Inject
    private TechniqueParamMapper techniqueParamMapper;

    @Inject
    private TechniqueParamService techniqueParamService;

    @Inject
    private TechniqueParamSearchRepository techniqueParamSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTechniqueParamMockMvc;

    private TechniqueParam techniqueParam;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TechniqueParamResource techniqueParamResource = new TechniqueParamResource();
        ReflectionTestUtils.setField(techniqueParamResource, "techniqueParamService", techniqueParamService);
        ReflectionTestUtils.setField(techniqueParamResource, "techniqueParamMapper", techniqueParamMapper);
        this.restTechniqueParamMockMvc = MockMvcBuilders.standaloneSetup(techniqueParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        techniqueParamSearchRepository.deleteAll();
        techniqueParam = new TechniqueParam();
        techniqueParam.setName(DEFAULT_NAME);
        techniqueParam.setValue(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createTechniqueParam() throws Exception {
        int databaseSizeBeforeCreate = techniqueParamRepository.findAll().size();

        // Create the TechniqueParam
        TechniqueParamDTO techniqueParamDTO = techniqueParamMapper.techniqueParamToTechniqueParamDTO(techniqueParam);

        restTechniqueParamMockMvc.perform(post("/api/technique-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(techniqueParamDTO)))
                .andExpect(status().isCreated());

        // Validate the TechniqueParam in the database
        List<TechniqueParam> techniqueParams = techniqueParamRepository.findAll();
        assertThat(techniqueParams).hasSize(databaseSizeBeforeCreate + 1);
        TechniqueParam testTechniqueParam = techniqueParams.get(techniqueParams.size() - 1);
        assertThat(testTechniqueParam.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTechniqueParam.getValue()).isEqualTo(DEFAULT_VALUE);

        // Validate the TechniqueParam in ElasticSearch
        TechniqueParam techniqueParamEs = techniqueParamSearchRepository.findOne(testTechniqueParam.getId());
        assertThat(techniqueParamEs).isEqualToComparingFieldByField(testTechniqueParam);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = techniqueParamRepository.findAll().size();
        // set the field null
        techniqueParam.setName(null);

        // Create the TechniqueParam, which fails.
        TechniqueParamDTO techniqueParamDTO = techniqueParamMapper.techniqueParamToTechniqueParamDTO(techniqueParam);

        restTechniqueParamMockMvc.perform(post("/api/technique-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(techniqueParamDTO)))
                .andExpect(status().isBadRequest());

        List<TechniqueParam> techniqueParams = techniqueParamRepository.findAll();
        assertThat(techniqueParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = techniqueParamRepository.findAll().size();
        // set the field null
        techniqueParam.setValue(null);

        // Create the TechniqueParam, which fails.
        TechniqueParamDTO techniqueParamDTO = techniqueParamMapper.techniqueParamToTechniqueParamDTO(techniqueParam);

        restTechniqueParamMockMvc.perform(post("/api/technique-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(techniqueParamDTO)))
                .andExpect(status().isBadRequest());

        List<TechniqueParam> techniqueParams = techniqueParamRepository.findAll();
        assertThat(techniqueParams).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTechniqueParams() throws Exception {
        // Initialize the database
        techniqueParamRepository.saveAndFlush(techniqueParam);

        // Get all the techniqueParams
        restTechniqueParamMockMvc.perform(get("/api/technique-params?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(techniqueParam.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getTechniqueParam() throws Exception {
        // Initialize the database
        techniqueParamRepository.saveAndFlush(techniqueParam);

        // Get the techniqueParam
        restTechniqueParamMockMvc.perform(get("/api/technique-params/{id}", techniqueParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(techniqueParam.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTechniqueParam() throws Exception {
        // Get the techniqueParam
        restTechniqueParamMockMvc.perform(get("/api/technique-params/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTechniqueParam() throws Exception {
        // Initialize the database
        techniqueParamRepository.saveAndFlush(techniqueParam);
        techniqueParamSearchRepository.save(techniqueParam);
        int databaseSizeBeforeUpdate = techniqueParamRepository.findAll().size();

        // Update the techniqueParam
        TechniqueParam updatedTechniqueParam = new TechniqueParam();
        updatedTechniqueParam.setId(techniqueParam.getId());
        updatedTechniqueParam.setName(UPDATED_NAME);
        updatedTechniqueParam.setValue(UPDATED_VALUE);
        TechniqueParamDTO techniqueParamDTO = techniqueParamMapper.techniqueParamToTechniqueParamDTO(updatedTechniqueParam);

        restTechniqueParamMockMvc.perform(put("/api/technique-params")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(techniqueParamDTO)))
                .andExpect(status().isOk());

        // Validate the TechniqueParam in the database
        List<TechniqueParam> techniqueParams = techniqueParamRepository.findAll();
        assertThat(techniqueParams).hasSize(databaseSizeBeforeUpdate);
        TechniqueParam testTechniqueParam = techniqueParams.get(techniqueParams.size() - 1);
        assertThat(testTechniqueParam.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTechniqueParam.getValue()).isEqualTo(UPDATED_VALUE);

        // Validate the TechniqueParam in ElasticSearch
        TechniqueParam techniqueParamEs = techniqueParamSearchRepository.findOne(testTechniqueParam.getId());
        assertThat(techniqueParamEs).isEqualToComparingFieldByField(testTechniqueParam);
    }

    @Test
    @Transactional
    public void deleteTechniqueParam() throws Exception {
        // Initialize the database
        techniqueParamRepository.saveAndFlush(techniqueParam);
        techniqueParamSearchRepository.save(techniqueParam);
        int databaseSizeBeforeDelete = techniqueParamRepository.findAll().size();

        // Get the techniqueParam
        restTechniqueParamMockMvc.perform(delete("/api/technique-params/{id}", techniqueParam.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean techniqueParamExistsInEs = techniqueParamSearchRepository.exists(techniqueParam.getId());
        assertThat(techniqueParamExistsInEs).isFalse();

        // Validate the database is empty
        List<TechniqueParam> techniqueParams = techniqueParamRepository.findAll();
        assertThat(techniqueParams).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTechniqueParam() throws Exception {
        // Initialize the database
        techniqueParamRepository.saveAndFlush(techniqueParam);
        techniqueParamSearchRepository.save(techniqueParam);

        // Search the techniqueParam
        restTechniqueParamMockMvc.perform(get("/api/_search/technique-params?query=id:" + techniqueParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(techniqueParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }
}
