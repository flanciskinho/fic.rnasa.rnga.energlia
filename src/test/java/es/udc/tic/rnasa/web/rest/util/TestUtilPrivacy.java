/**
 * This file is part of Energlia.

 Energlia is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Energlia is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Energlia.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.udc.tic.rnasa.web.rest.util;


import es.udc.tic.rnasa.domain.util.Recognizable;
import es.udc.tic.rnasa.web.rest.TestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by flanciskinho on 12/4/16.
 */

public class TestUtilPrivacy {

    private final Logger log = LoggerFactory.getLogger(TestUtilPrivacy.class);

    private JpaRepository entityRepository;

    private WebApplicationContext context;

    private String urlDelete, urlUpdate, urlCreate, urlGet, urlList;

    private MockMvc restEntityMock;

    /**
     *
     * @param entityRepository repository to do operations with entity
     * @param context WebApplicationContext
     * @param urlDelete url to delete a entity
     * @param urlUpdate url to update a entity
     * @param urlCreate url to create a entity
     * @param urlGet    url to show a entity
     * @param urlList   url to list entities
     */
    public TestUtilPrivacy(JpaRepository entityRepository, WebApplicationContext context, String urlDelete, String urlUpdate, String urlCreate, String urlGet, String urlList) {
        this.entityRepository = entityRepository;
        this.context = context;
        this.urlDelete = urlDelete;
        this.urlUpdate = urlUpdate;
        this.urlCreate = urlCreate;
        this.urlGet = urlGet;
        this.urlList = urlList;

        // create secure-aware mock
        restEntityMock = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    public TestUtilPrivacy(JpaRepository entityRepository, WebApplicationContext context, String specificUrl) {
        this(entityRepository,context,
            "/api/"+specificUrl+"/{id}",
            "/api/"+specificUrl,
            "/api/"+specificUrl,
            "/api/"+specificUrl+"/{id}",
            "/api/"+specificUrl+"?sort=id,desc");
    }


    public ResultActions listEntities(String user, String role, HttpStatus status) throws Exception {
        return restEntityMock.perform(MockMvcRequestBuilders.get(urlList)
            .with(user(user).authorities(new SimpleGrantedAuthority(role))))
            .andExpect(status().is(status.value()));
    }


    public ResultActions listEntitiesUserWithoutData(String user, String role, HttpStatus status) throws Exception{
        restEntityMock = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();

        return restEntityMock.perform(MockMvcRequestBuilders.get(urlList)
            .with(user(user).authorities(new SimpleGrantedAuthority(role))))
            .andExpect(status().is(status.value()))

            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*]").isEmpty());
    }


    public ResultActions listEntitiesUnauthorized() throws Exception{
        MockMvc restEntityMock = MockMvcBuilders.webAppContextSetup(context).build();

        return restEntityMock.perform(MockMvcRequestBuilders.get(urlList))
            .andExpect(status().isUnauthorized());
    }

    public ResultActions getEntity(Recognizable entity, String user, String role, HttpStatus status) throws Exception {
        Serializable id = (Serializable) entity.getId();
        return restEntityMock.perform(MockMvcRequestBuilders.get(urlGet, id)
            .with(user(user).authorities(new SimpleGrantedAuthority(role))))
            .andExpect(status().is(status.value()));
    }


    public ResultActions getEntityNotFound(long id, String user, String role) throws Exception {
        return restEntityMock.perform(MockMvcRequestBuilders.get(urlGet, id)
            .with(user(user).authorities(new SimpleGrantedAuthority(role))))
            .andExpect(status().isNotFound());
    }


    public ResultActions getEntityUnauthorized(Recognizable entity) throws Exception {
        MockMvc restEntityMock = MockMvcBuilders.webAppContextSetup(context).build();

        Serializable id = (Serializable) entity.getId();
        return restEntityMock.perform(MockMvcRequestBuilders.get(urlGet, id))
            .andExpect(status().isUnauthorized());
    }



    public ResultActions createEntityUnauthorized(Recognizable entity) throws Exception {
        log.debug("Entity to save: {}", entity);

        MockMvc restEntityMock = MockMvcBuilders.webAppContextSetup(context).build();

        return restEntityMock.perform(MockMvcRequestBuilders.post(urlCreate)
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entity)))
            .andExpect(status().isUnauthorized());
    }

    public ResultActions createEntity(Recognizable entity, String user, String role, HttpStatus status) throws Exception {

        log.debug("Current user '{}' with role '{}'", user, role);
        log.debug("Expected status: '{}'", status.value());

        log.debug("entity to save: {}", entity);

        restEntityMock = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();

        ResultActions result =  restEntityMock.perform(MockMvcRequestBuilders.post(urlCreate)
            .with(user(user).authorities(new SimpleGrantedAuthority(role)))
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entity)));


        log.debug("response: {}", result.andReturn().getResponse().getContentAsString());
        log.debug("Error message: {}", result.andReturn().getResponse().getErrorMessage());

        return result.andExpect(status().is(status.value()));
    }

    public ResultActions deleteEntity(Recognizable entity, String user, String role, HttpStatus status) throws Exception {
        ResultActions resultActions = restEntityMock.perform(MockMvcRequestBuilders.delete(urlDelete, entity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8)
            .with(user(user).authorities(new SimpleGrantedAuthority(role))))
            .andExpect(status().is(status.value()));


        Serializable id = (Serializable) entity.getId();
        if (status.equals(HttpStatus.OK))
            assertThat(entityRepository.findOne(id)).isNull();
        else
            assertThat(entityRepository.findOne(id)).isNotNull();

        return resultActions;
    }

    public ResultActions deleteEntityUnauthorized(Recognizable entity) throws Exception {
        ResultActions resultActions;

        MockMvc restEntityMock = MockMvcBuilders.webAppContextSetup(context).build();

        resultActions = restEntityMock.perform(MockMvcRequestBuilders.delete(urlDelete, entity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isUnauthorized());

        Serializable id = (Serializable) entity.getId();
        assertThat(entityRepository.findOne(id)).isNotNull();

        return resultActions;
    }

    public ResultActions deleteEntityNotFound(Long id, String user, String role) throws Exception {
        ResultActions resultActions;
        resultActions = restEntityMock.perform(MockMvcRequestBuilders.delete(urlDelete, id)
            .with(user(user).authorities(new SimpleGrantedAuthority(role)))
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNotFound());

        return resultActions;
    }


    public ResultActions updateEntity(Recognizable entity, Recognizable updateEntity, String user, String role, HttpStatus status) throws Exception {
        log.debug("Current user '{}' with role '{}'", user, role);
        log.debug("Expected status: '{}'", status.value());

        log.debug("store entity: {}", entity);
        log.debug("new entity:   {}", updateEntity);

        ResultActions resultActions = restEntityMock.perform(MockMvcRequestBuilders.put(urlUpdate)
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updateEntity))
            .with(user(user).authorities(new SimpleGrantedAuthority(role)))
        ).andExpect(status().is(status.value()));

        Serializable id = (Serializable) updateEntity.getId();
        if (status.equals(HttpStatus.OK) || status.equals(HttpStatus.CREATED)) {
            assertThat(updateEntity.equals(entityRepository.findOne(id)));
        }



        return resultActions;
    }


    public ResultActions updateEntityUnauthorized(Recognizable entity, Recognizable updatedEntity) throws Exception {
        log.debug("store entity: {}", entity);
        log.debug("new entity:   {}", updatedEntity);

        MockMvc restEntityMock = MockMvcBuilders.webAppContextSetup(context).build();

        ResultActions resultActions = restEntityMock.perform(MockMvcRequestBuilders.put(urlUpdate)
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEntity)))
            .andExpect(status().isUnauthorized());

        return resultActions;
    }
}
