'use strict';

describe('Controller Tests', function() {

    describe('AnalyzeType Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockAnalyzeType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockAnalyzeType = jasmine.createSpy('MockAnalyzeType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'AnalyzeType': MockAnalyzeType
            };
            createController = function() {
                $injector.get('$controller')("AnalyzeTypeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:analyzeTypeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
