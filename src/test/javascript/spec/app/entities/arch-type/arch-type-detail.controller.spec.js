'use strict';

describe('Controller Tests', function() {

    describe('ArchType Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockArchType, MockSshServer;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockArchType = jasmine.createSpy('MockArchType');
            MockSshServer = jasmine.createSpy('MockSshServer');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ArchType': MockArchType,
                'SshServer': MockSshServer
            };
            createController = function() {
                $injector.get('$controller')("ArchTypeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:archTypeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
