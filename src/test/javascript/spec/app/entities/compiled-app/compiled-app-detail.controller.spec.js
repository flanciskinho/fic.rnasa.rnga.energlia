'use strict';

describe('Controller Tests', function() {

    describe('CompiledApp Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockCompiledApp, MockApplication, MockSshServer;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockCompiledApp = jasmine.createSpy('MockCompiledApp');
            MockApplication = jasmine.createSpy('MockApplication');
            MockSshServer = jasmine.createSpy('MockSshServer');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'CompiledApp': MockCompiledApp,
                'Application': MockApplication,
                'SshServer': MockSshServer
            };
            createController = function() {
                $injector.get('$controller')("CompiledAppDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:compiledAppUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
