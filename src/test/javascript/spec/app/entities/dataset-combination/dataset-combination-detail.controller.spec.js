'use strict';

describe('Controller Tests', function() {

    describe('DatasetCombination Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockDatasetCombination, MockDataset;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockDatasetCombination = jasmine.createSpy('MockDatasetCombination');
            MockDataset = jasmine.createSpy('MockDataset');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'DatasetCombination': MockDatasetCombination,
                'Dataset': MockDataset
            };
            createController = function() {
                $injector.get('$controller')("DatasetCombinationDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:datasetCombinationUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
