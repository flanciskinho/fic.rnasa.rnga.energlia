'use strict';

describe('Controller Tests', function() {

    describe('ExecParam Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockExecParam, MockSimulation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockExecParam = jasmine.createSpy('MockExecParam');
            MockSimulation = jasmine.createSpy('MockSimulation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ExecParam': MockExecParam,
                'Simulation': MockSimulation
            };
            createController = function() {
                $injector.get('$controller')("ExecParamDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:execParamUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
