'use strict';

describe('Controller Tests', function() {

    describe('GaParam Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockGaParam, MockGa;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockGaParam = jasmine.createSpy('MockGaParam');
            MockGa = jasmine.createSpy('MockGa');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'GaParam': MockGaParam,
                'Ga': MockGa
            };
            createController = function() {
                $injector.get('$controller')("GaParamDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:gaParamUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
