'use strict';

describe('Controller Tests', function() {

    describe('Ga Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockGa, MockSimulation, MockGaType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockGa = jasmine.createSpy('MockGa');
            MockSimulation = jasmine.createSpy('MockSimulation');
            MockGaType = jasmine.createSpy('MockGaType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Ga': MockGa,
                'Simulation': MockSimulation,
                'GaType': MockGaType
            };
            createController = function() {
                $injector.get('$controller')("GaDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:gaUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
