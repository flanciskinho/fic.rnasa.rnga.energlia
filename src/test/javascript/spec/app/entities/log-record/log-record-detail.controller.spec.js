'use strict';

describe('Controller Tests', function() {

    describe('LogRecord Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockLogRecord, MockSpecificSimulation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockLogRecord = jasmine.createSpy('MockLogRecord');
            MockSpecificSimulation = jasmine.createSpy('MockSpecificSimulation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'LogRecord': MockLogRecord,
                'SpecificSimulation': MockSpecificSimulation
            };
            createController = function() {
                $injector.get('$controller')("LogRecordDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:logRecordUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
