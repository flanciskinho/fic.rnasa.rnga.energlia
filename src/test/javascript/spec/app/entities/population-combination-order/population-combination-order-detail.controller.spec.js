'use strict';

describe('Controller Tests', function() {

    describe('PopulationCombinationOrder Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPopulationCombinationOrder, MockSpecificSimulation, MockPopulationCombination;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPopulationCombinationOrder = jasmine.createSpy('MockPopulationCombinationOrder');
            MockSpecificSimulation = jasmine.createSpy('MockSpecificSimulation');
            MockPopulationCombination = jasmine.createSpy('MockPopulationCombination');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'PopulationCombinationOrder': MockPopulationCombinationOrder,
                'SpecificSimulation': MockSpecificSimulation,
                'PopulationCombination': MockPopulationCombination
            };
            createController = function() {
                $injector.get('$controller')("PopulationCombinationOrderDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:populationCombinationOrderUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
