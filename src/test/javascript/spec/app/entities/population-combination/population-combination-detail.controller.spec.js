'use strict';

describe('Controller Tests', function() {

    describe('PopulationCombination Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPopulationCombination, MockPopulation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPopulationCombination = jasmine.createSpy('MockPopulationCombination');
            MockPopulation = jasmine.createSpy('MockPopulation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'PopulationCombination': MockPopulationCombination,
                'Population': MockPopulation
            };
            createController = function() {
                $injector.get('$controller')("PopulationCombinationDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:populationCombinationUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
