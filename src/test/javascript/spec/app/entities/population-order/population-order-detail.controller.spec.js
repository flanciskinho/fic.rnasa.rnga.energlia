'use strict';

describe('Controller Tests', function() {

    describe('PopulationOrder Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPopulationOrder, MockGa, MockPopulation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPopulationOrder = jasmine.createSpy('MockPopulationOrder');
            MockGa = jasmine.createSpy('MockGa');
            MockPopulation = jasmine.createSpy('MockPopulation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'PopulationOrder': MockPopulationOrder,
                'Ga': MockGa,
                'Population': MockPopulation
            };
            createController = function() {
                $injector.get('$controller')("PopulationOrderDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:populationOrderUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
