'use strict';

describe('Controller Tests', function() {

    describe('PopulationParam Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPopulationParam, MockPopulationOrder;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPopulationParam = jasmine.createSpy('MockPopulationParam');
            MockPopulationOrder = jasmine.createSpy('MockPopulationOrder');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'PopulationParam': MockPopulationParam,
                'PopulationOrder': MockPopulationOrder
            };
            createController = function() {
                $injector.get('$controller')("PopulationParamDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:populationParamUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
