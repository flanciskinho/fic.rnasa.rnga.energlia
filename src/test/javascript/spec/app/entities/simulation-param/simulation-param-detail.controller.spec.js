'use strict';

describe('Controller Tests', function() {

    describe('SimulationParam Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSimulationParam, MockSimulation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSimulationParam = jasmine.createSpy('MockSimulationParam');
            MockSimulation = jasmine.createSpy('MockSimulation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SimulationParam': MockSimulationParam,
                'Simulation': MockSimulation
            };
            createController = function() {
                $injector.get('$controller')("SimulationParamDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:simulationParamUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
