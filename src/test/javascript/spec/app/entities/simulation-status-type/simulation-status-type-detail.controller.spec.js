'use strict';

describe('Controller Tests', function() {

    describe('SimulationStatusType Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSimulationStatusType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSimulationStatusType = jasmine.createSpy('MockSimulationStatusType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SimulationStatusType': MockSimulationStatusType
            };
            createController = function() {
                $injector.get('$controller')("SimulationStatusTypeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:simulationStatusTypeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
