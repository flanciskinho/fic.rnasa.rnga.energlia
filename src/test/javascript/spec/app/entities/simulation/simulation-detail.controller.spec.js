'use strict';

describe('Controller Tests', function() {

    describe('Simulation Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSimulation, MockUser, MockDataset, MockApplication, MockProblemType, MockErrorType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSimulation = jasmine.createSpy('MockSimulation');
            MockUser = jasmine.createSpy('MockUser');
            MockDataset = jasmine.createSpy('MockDataset');
            MockApplication = jasmine.createSpy('MockApplication');
            MockProblemType = jasmine.createSpy('MockProblemType');
            MockErrorType = jasmine.createSpy('MockErrorType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Simulation': MockSimulation,
                'User': MockUser,
                'Dataset': MockDataset,
                'Application': MockApplication,
                'ProblemType': MockProblemType,
                'ErrorType': MockErrorType
            };
            createController = function() {
                $injector.get('$controller')("SimulationDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:simulationUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
