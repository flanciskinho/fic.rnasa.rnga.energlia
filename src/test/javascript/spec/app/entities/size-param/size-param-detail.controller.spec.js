'use strict';

describe('Controller Tests', function() {

    describe('SizeParam Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSizeParam, MockPopulation, MockSizeType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSizeParam = jasmine.createSpy('MockSizeParam');
            MockPopulation = jasmine.createSpy('MockPopulation');
            MockSizeType = jasmine.createSpy('MockSizeType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SizeParam': MockSizeParam,
                'Population': MockPopulation,
                'SizeType': MockSizeType
            };
            createController = function() {
                $injector.get('$controller')("SizeParamDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:sizeParamUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
