'use strict';

describe('Controller Tests', function() {

    describe('SpecificSimulation Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSpecificSimulation, MockSimulationStatusType, MockSimulation, MockSshAccount, MockDatasetCombination;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSpecificSimulation = jasmine.createSpy('MockSpecificSimulation');
            MockSimulationStatusType = jasmine.createSpy('MockSimulationStatusType');
            MockSimulation = jasmine.createSpy('MockSimulation');
            MockSshAccount = jasmine.createSpy('MockSshAccount');
            MockDatasetCombination = jasmine.createSpy('MockDatasetCombination');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SpecificSimulation': MockSpecificSimulation,
                'SimulationStatusType': MockSimulationStatusType,
                'Simulation': MockSimulation,
                'SshAccount': MockSshAccount,
                'DatasetCombination': MockDatasetCombination
            };
            createController = function() {
                $injector.get('$controller')("SpecificSimulationDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:specificSimulationUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
