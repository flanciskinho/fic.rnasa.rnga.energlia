'use strict';

describe('Controller Tests', function() {

    describe('SshAccount Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSshAccount, MockSshServer;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSshAccount = jasmine.createSpy('MockSshAccount');
            MockSshServer = jasmine.createSpy('MockSshServer');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SshAccount': MockSshAccount,
                'SshServer': MockSshServer
            };
            createController = function() {
                $injector.get('$controller')("SshAccountDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:sshAccountUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
