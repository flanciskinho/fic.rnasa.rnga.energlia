'use strict';

describe('Controller Tests', function() {

    describe('SshServer Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSshServer, MockQueueType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSshServer = jasmine.createSpy('MockSshServer');
            MockQueueType = jasmine.createSpy('MockQueueType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SshServer': MockSshServer,
                'QueueType': MockQueueType
            };
            createController = function() {
                $injector.get('$controller')("SshServerDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:sshServerUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
