'use strict';

describe('Controller Tests', function() {

    describe('TechniqueParam Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTechniqueParam, MockLogRecord;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTechniqueParam = jasmine.createSpy('MockTechniqueParam');
            MockLogRecord = jasmine.createSpy('MockLogRecord');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'TechniqueParam': MockTechniqueParam,
                'LogRecord': MockLogRecord
            };
            createController = function() {
                $injector.get('$controller')("TechniqueParamDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'energliaApp:techniqueParamUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
